<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.10.8
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

/*
 * You can remove this if you are confident that your PHP version is sufficient.
 */
if (version_compare(PHP_VERSION, '5.5.9') < 0) {
    trigger_error('Your PHP version must be equal or higher than 5.5.9 to use CakePHP.', E_USER_ERROR);
}

/*
 *  You can remove this if you are confident you have intl installed.
 */
if (!extension_loaded('intl')) {
    trigger_error('You must enable the intl extension to use CakePHP.', E_USER_ERROR);
}

/*
 * You can remove this if you are confident you have mbstring installed.
 */
if (!extension_loaded('mbstring')) {
    trigger_error('You must enable the mbstring extension to use CakePHP.', E_USER_ERROR);
}

/*
 * Configure paths required to find CakePHP + general filepath
 * constants
 */
require __DIR__ . '/paths.php';

/*
 * Bootstrap CakePHP.
 *
 * Does the various bits of setup that CakePHP needs to do.
 * This includes:
 *
 * - Registering the CakePHP autoloader.
 * - Setting the default application paths.
 */
require CORE_PATH . 'config' . DS . 'bootstrap.php';

use Cake\Cache\Cache;
use Cake\Console\ConsoleErrorHandler;
use Cake\Core\App;
use Cake\Core\Configure;
use Cake\Core\Configure\Engine\PhpConfig;
use Cake\Core\Plugin;
use Cake\Database\Type;
use Cake\Datasource\ConnectionManager;
use Cake\Error\ErrorHandler;
use Cake\Log\Log;
use Cake\Mailer\Email;
use Cake\Network\Request;
use Cake\Utility\Inflector;
use Cake\Utility\Security;

/*
 * Read configuration file and inject configuration into various
 * CakePHP classes.
 *
 * By default there is only one configuration file. It is often a good
 * idea to create multiple configuration files, and separate the configuration
 * that changes from configuration that does not. This makes deployment simpler.
 */
try {
    Configure::config('default', new PhpConfig());
    Configure::load('app', 'default', false);
} catch (\Exception $e) {
    exit($e->getMessage() . "\n");
}

/*
 * Load an environment local configuration file.
 * You can use a file like app_local.php to provide local overrides to your
 * shared configuration.
 */
//Configure::load('app_local', 'default');

/*
 * When debug = true the metadata cache should only last
 * for a short time.
 */
if (Configure::read('debug')) {
    Configure::write('Cache._cake_model_.duration', '+2 minutes');
    Configure::write('Cache._cake_core_.duration', '+2 minutes');
}

/*
 * Set server timezone to UTC. You can change it to another timezone of your
 * choice but using UTC makes time calculations / conversions easier.
 */
// date_default_timezone_set('UTC');
date_default_timezone_set('Asia/Tokyo');

/*
 * Configure the mbstring extension to use the correct encoding.
 */
mb_internal_encoding(Configure::read('App.encoding'));

/*
 * Set the default locale. This controls how dates, number and currency is
 * formatted and sets the default language to use for translations.
 */
ini_set('intl.default_locale', Configure::read('App.defaultLocale'));

/*
 * Register application error and exception handlers.
 */
$isCli = PHP_SAPI === 'cli';
if ($isCli) {
    (new ConsoleErrorHandler(Configure::read('Error')))->register();
} else {
    (new ErrorHandler(Configure::read('Error')))->register();
}

/*
 * Include the CLI bootstrap overrides.
 */
if ($isCli) {
    require __DIR__ . '/bootstrap_cli.php';
}

/*
 * Set the full base URL.
 * This URL is used as the base of all absolute links.
 *
 * If you define fullBaseUrl in your config file you can remove this.
 */
if (!Configure::read('App.fullBaseUrl')) {
    $s = null;
    if (env('HTTPS')) {
        $s = 's';
    }

    $httpHost = env('HTTP_HOST');
    if (isset($httpHost)) {
        Configure::write('App.fullBaseUrl', 'http' . $s . '://' . $httpHost);
    }
    unset($httpHost, $s);
}

Cache::config(Configure::consume('Cache'));
ConnectionManager::config(Configure::consume('Datasources'));
Email::configTransport(Configure::consume('EmailTransport'));
Email::config(Configure::consume('Email'));
Log::config(Configure::consume('Log'));
Security::salt(Configure::consume('Security.salt'));

/*
 * The default crypto extension in 3.0 is OpenSSL.
 * If you are migrating from 2.x uncomment this code to
 * use a more compatible Mcrypt based implementation
 */
//Security::engine(new \Cake\Utility\Crypto\Mcrypt());

/*
 * Setup detectors for mobile and tablet.
 */
Request::addDetector('mobile', function ($request) {
    $detector = new \Detection\MobileDetect();

    return $detector->isMobile();
});
Request::addDetector('tablet', function ($request) {
    $detector = new \Detection\MobileDetect();

    return $detector->isTablet();
});

/*
 * Enable immutable time objects in the ORM.
 *
 * You can enable default locale format parsing by adding calls
 * to `useLocaleParser()`. This enables the automatic conversion of
 * locale specific date formats. For details see
 * @link http://book.cakephp.org/3.0/en/core-libraries/internationalization-and-localization.html#parsing-localized-datetime-data
 */
Type::build('time')
    ->useImmutable();
Type::build('date')
    ->useImmutable();
Type::build('datetime')
    ->useImmutable();
Type::build('timestamp')
    ->useImmutable();

/*
 * Custom Inflector rules, can be set to correctly pluralize or singularize
 * table, model, controller names or whatever other string is passed to the
 * inflection functions.
 */
//Inflector::rules('plural', ['/^(inflect)or$/i' => '\1ables']);
//Inflector::rules('irregular', ['red' => 'redlings']);
//Inflector::rules('uninflected', ['dontinflectme']);
//Inflector::rules('transliteration', ['/å/' => 'aa']);

/*
 * Plugins need to be loaded manually, you can either load them one by one or all of them in a single call
 * Uncomment one of the lines below, as you need. make sure you read the documentation on Plugin to use more
 * advanced ways of loading plugins
 *
 * Plugin::loadAll(); // Loads all plugins at once
 * Plugin::load('Migrations'); //Loads a single plugin named Migrations
 *
 */
/*
 * Only try to load DebugKit in development mode
 * Debug Kit should not be installed on a production system
 */
if (Configure::read('debug')) {
    Plugin::load('DebugKit', ['bootstrap' => true]);
}

Configure::write('Session', [
    'defaults' => 'php',
    'timeout' => 4320, // 3 days
]);

// CONSTANT
// NUMBER
define('PAGE_NUMBER', 10);
define('EXPIRE_HOUR', 24);
define('PAGE_LIMIT_NUMBER', 20);

define('IS_SUSPENDED', 1);
define('REMOVE_SUSPENDED', 0);

define('MALE', 1);
define('FEMALE', 0);

// PURCHASE ORDER TYPE
// TO DO CHANGE TO THE FOLLOWINGS
// define('PURCHASE_ORDER_MEDIPRO_TO_SUPPLIER', 1);
define('PO_TYPE_MP_TO_S', 1); // purchase order from MediPro to Supplier
define('PO_TYPE_W_TO_S', 2); // purchase order from Wisteria to Supplier
define('PO_TYPE_W_TO_MP', 3); // purchase order from Wisteria to MediPro

// PDF DOCUMENT TYPES
define('PDF_IMPORT_DOC', 'importing_docs');
define('PDF_ACC_EVIDENCE', 'accounting_evidences');
define('PDF_OTHERS', 'others');

// USER STATUSES
define('STATUS_ACTIVE', 'active');
define('STATUS_INACTIVE', 'inactive');
define('STATUS_DELETED', 'deleted');

// USER ROLES AND CLASSES
define('USER_ROLE_ADMIN', 'admin'); // user role or class admin
define('USER_ROLE_CUSTOMER_SUPPORT', 'customer_support');
define('USER_ROLE_LOGISTIC', 'logistic');
define('USER_ROLE_MANAGER_SALE', 'manager_sale');
define('USER_ROLE_SALE', 'sale');

// USER CLASSES
define('USER_CLASS_WISTERIA', 'wisteria');
define('USER_CLASS_MEDIPRO', 'medipro');
define('USER_SYSTEM_ADMIN', 'system_admin');

// TRADE TYPE
define('TRADE_TYPE_SALE_W_STOCK', 'sale_w_stock');                 // Domistic |
define('TRADE_TYPE_SALE_BEHALF_S_IN_JP', 'sale_behalf_s_in_jp');   // Domistic | => 国内取引 (Domestic)
define('TRADE_TYPE_PURCHASE_W_STOCK', 'purchase_w_stock');         // Domistic |
define('TRADE_TYPE_SALE_BEHALF_MP', 'sale_behalf_mp');                      // Over Sea |
define('TRADE_TYPE_SALE_BEHALF_S_OUTSIDE_JP', 'sale_behalf_s_outside_jp');  // Over Sea | => 海外取引 (Oversea)
define('TRADE_TYPE_PURCHASE_MP_STOCK', 'purchase_mp_stock');                // Over Sea |

define('TRADE_TYPE_W_SAMPLE_STOCK_S_IN_JP', 'w_sample_stock_s_in_jp');  // Sample stock
define('TRADE_TYPE_W_SAMPLE_STOCK_S_OUT_JP', 'w_sample_stock_s_out_jp');  // Sample stock
define('TRADE_TYPE_W_SAMPLE_STOCK_OF_MP', 'w_sample_stock_of_mp');  // Sample stock

// FIELD TYPES
define('TYPE_AMOUNT', 'amount');
define('TYPE_QUANTITY', 'quantity');
define('TYPE_PERCENT', 'percent');
define('CUSTOMER_TYPE_NORMAL', 'normal');
define('CUSTOMER_TYPE_NEW', 'new');
define('TYPE_EDIT', 'edit');
define('TYPE_DISABLED', 'disabled');
define('CUSTOMER_TYPE_CONTRACT', 'contract');
define('CUSTOMER_TYPE_BAD', 'bad');

define('TYPE_BEHALF_MP', 'behalf_mp');
define('TYPE_BEHALF_S', 'behalf_s');
define('TYPE_BEHALF_PURCHASE', 'behalf_purchase');
define('TYPE_MP_SAMPLE', 'mp_sample');
define('TYPE_S_SAMPLE', 's_sample');
define('TYPE_SALE_W_STOCK', 'sale_w_stock');
define('TYPE_WISTERIA_STOCK', 'wisteria_stock');
define('TYPE_ORDER_MANAGMENT', 'order_management');

define('TYPE_MEDICINAL_PRODUCT', 'medicinal_product');
define('TYPE_MEDICIAL_DEVICE', 'medical_device');
define('TYPE_AGENT', 'agent');
define('TYPE_BAD', 'bad');
define('TYPE_CONTRACT', 'contract');
define('TYPE_NEW', 'new');
define('TYPE_BRAND', 'brand');
define('TYPE_GENERAL', 'general');
define('TYPE_CLINIC', 'clinic');
define('TYPE_CUSTOMER', 'customer');
define('TYPE_WISTERIA', 'wisteria');
define('TYPE_UNPAID', 'unpaid');
define('TYPE_PAID', 'paid');
define('TYPE_DOCTOR', 'doctor');
define('TYPE_PERSON', 'person');
define('TYPE_GROUP', 'group');
define('TYPE_HQ_CUSTOMER', 'group_customer');
define('TYPE_HQMC', 'group_medical_corporations');
define('TYPE_IMPORT', 'import');
define('TYPE_MANUFACTURER', 'manufacturer');
define('TYPE_MEDICAL_CORP', 'medical_corporations');
define('TYPE_MESSAGE', 'message');
define('TYPE_MP', 'mp');
define('TYPE_ORDER_TO_S', 'order_to_supplier');
define('TYPE_ORDER_TO_MP', 'order_to_mp');
define('TYPE_ORDER_NP', 'order_np');
define('TYPE_PAYMENT', 'payment');
define('TYPE_PERSON_IN_CHARGE', 'person_in_charge');
define('TYPE_PO', 'po');
define('TYPE_PRODUCT', 'product');
define('TYPE_PRODUCT_BRAND', 'product_brand');
define('TYPE_PACKAGE', 'product_package'); //product package
define('TYPE_PRODUCT_DETAILS', 'product_detail');
define('TYPE_PO_TYPE_MP_TO_S', 'po_mp_to_s');
define('TYPE_PO_TYPE_W_TO_MP', 'po_w_to_mp');
define('TYPE_PRICING', 'pricing');
define('TYPE_REPLY', 'reply');
define('TYPE_SAMPLE', 'sample');
define('TYPE_SELLER_BRAND', 'seller_brand');
define('TYPE_SELLER_PRICING', 'seller_pricing');
define('TYPE_SELLER_PRODUCT', 'seller_product');
define('TYPE_STOCK', 'stock');
define('TYPE_SUBSIDIARY', 'subsidiary');
define('TYPE_SUPPLIER', 'supplier');
define('TYPE_FOC_RANGE', 'foc_range');
define('TYPE_DEVICE', 'device');

// Price Type
define('PRICE_TYPE_SALE', 'price_type_sale');
define('PRICE_TYPE_PURCHASE', 'price_type_purchase');
define('PRICE_TYPE_CLINIC', 'price_type_clinic');

define('STANDARD_PRICE_TYPE', 'standard_price');
define('DISCOUNT_PRICE_TYPE', 'discount_price');
define('CLINIC_PRICE_TYPE', 'clinic_price');
define('FOC_PRICE_TYPE', 'foc_price');

// Message type
define('MESSAGE_TYPE_SELLER_STANDARD_PRICE', 'seller_standard_price');
define('MESSAGE_TYPE_FOC', 'foc');
define('MESSAGE_TYPE_W_STANDARD_PRICE', 'w_standard_price');
define('MESSAGE_TYPE_STOCK_DETAIL', 'stock_detail');
define('MESSAGE_TYPE_SELLER_PRICING', 'seller_pricing');
define('MESSAGE_TYPE_PRICING', 'pricing');
define('MESSAGE_TYPE_PRICING_MANAGEMENT', 'pricing_management');

define('STATUS_CANCEL', 'cancel');
define('STATUS_PAID', 'paid');
define('STATUS_PACKED', 'packed');
define('STATUS_SHIPPED', 'shipped');
define('STATUS_RETURNING', 'returning');
define('STATUS_RETURNED', 'returned');
define('STATUS_APPLIED_CC', 'applied_cc');
define('STATUS_REQUESTED_CC', 'requested_cc');
define('STATUS_COMPLETED', 'completed');

define('STATUS_FAXED_ROP', 'faxed_rop');
define('STATUS_FAXED_W_INVOICE', 'faxed_w_invoice');
define('STATUS_EMAILED_PO', 'emailed_po');
define('STATUS_EMAILED_OF', 'emailed_of');
define('STATUS_URGENT', 'urgent');

// ISSUE TYPES
define('ISSUE_TYPE_WRONG', 'wrong product');
define('ISSUE_TYPE_DAMAGE', 'damage');
define('ISSUE_TYPE_LESS_QTY', 'less quantity');

// ADDRESS TYPE
define('JAPANE_ADDRESS', 'japane_address');
define('SINGAPORE_ADDRESS', 'singapore_addess');
define('ENGLISH_ADDRESS', 'english_address');

// Search Type
define('ADVANCE_TYPE', 'advance');
define('NORMAL_TYPE', 'normal');
define('RRODUCT_TYPE', 'product');
// TRANSACTION STATUS (PO STATUS)
define('PO_STATUS_DRAFF', 'draff');
// define('PO_STATUS_ISSUE_INVOICE', 'issue_invoice');     // need to remove
define('PO_STATUS_PAID', 'paid');  // dont use (query logic with payment)
/* define('PO_STATUS_REQUESTED_PACKING', 'requested_packing'); */
define('PO_STATUS_ISSUE_ROP_OR_WINV', 'faxed_rop_or_w_inv');
define('PO_STATUS_REQUESTED_SHIPPING', 'requested_shipping');
define('PO_STATUS_CUSTOM_CLEARANCE', 'applied_custom_clearance');
define('PO_STATUS_REQUESTED_CC', 'requested_cc');
define('PO_STATUS_DELIVERED', 'delivered');
define('PO_STATUS_DELIVERING', 'delivering');
define('PO_STATUS_COMPLETED', 'completed');

// DAMAGE LEVELS
define('LEVEL_A', 'A');
define('LEVEL_B', 'B');
define('LEVEL_C', 'C');
define('LEVEL_F', 'F');

// ACTION BUTTONS
define('BTN_ICON_DELETE', '<span><i class="fa fa-trash" aria-hidden="true"></i></span>'); //BUTTON DELETE
define('BTN_ICON_EDIT', '<span><i class="fa fa-pencil-square-o" aria-hidden="true"></i></span>'); //BUTTON EDIT
define('BTN_ICON_STOP', '<span><i class="fa fa-ban" aria-hidden="true"></i></span>'); //BUTTON STOP
define('BTN_ICON_UNDO', '<span><i class="fa fa-undo" aria-hidden="true"></i></span>'); //BUTTON UNDO OR RECOVER
define('BTN_ICON_VIEW', '<span><i class="fa fa-eye" aria-hidden="true"></i></span>');
define('BTN_ICON_PDF', '<span><i class="fa fa-file-pdf-o" aria-hidden="true"></i></span>');
define('BTN_ICON_TRUCK', '<span><i class="fa fa-truck" aria-hidden="true"></i></span>');
define('BTN_ICON_PLANE', '<span><i class="fa fa-plane" aria-hidden="true"></i></span>');
define('BTN_ICON_REPORT', '<span><i class="fa fa-file-text-o" aria-hidden="true"></i></span>');
define('BTN_ICON_GIFT', '<span><i class="fa fa-gift" aria-hidden="true"></i></span>');
define('BTN_ICON_YEN', '<span><i class="fa fa-jpy" aria-hidden="true"></i></span>');
define('BTN_ICON_MSG', '<span><i class="fa fa-comments-o" aria-hidden="true"></i></span>');

// MESSAGES
define('MSG_SUCCESS', 'success');
define('MSG_WARNING', 'warning');
define('MSG_ERROR', 'error');
define('RECEIVER_TYPE', 'receiver');
define('MESSAGE_TYPE', 'message');

// CURRENCY
define('TYPE_CURRENCY_JPY_CODE_EN', 'JPY');
define('TYPE_CURRENCY_JPY_CODE', '円');

define('TARGET_EDIT', 'edit');
define('TARGET_SUSPEND', 'suspend');
define('TARGET_DELETE', 'delete');
define('TARGET_NEW', 'new');
define('TARGET_RECOVER', 'recover');
define('UPLOAD_PATH', 'img/uploads');

// LOCAL SESSION STORAGE
define('LOCAL_EN', '_en');

define('MUDULE_ACTION_DASHBOARD', 'Home');
define('ROLE', serialize([
    USER_ROLE_ADMIN => USER_ROLE_ADMIN,
    USER_ROLE_SALE => USER_ROLE_SALE
]));

// SYSTEM EMAIL
define('SYSTEM_EMAIL', 'meyreyya.kan.bitex@gmail.com');

// MESSAGE STATUS LIST
define('MESSAGE_STATUS_TYPE', serialize([
    9 => 'STR_COMMENTS_URGENT',
    4 => 'TXT_COMMENTS_NORMAL',
    3 => 'TXT_COMMENTS_COMPLETE',
    0 => 'TXT_DELETE',
]));

// ENTITY TYPE
define('ENTITY_TYPE', serialize([
    TYPE_MANUFACTURER => 'TXT_MANUFACTURERS',
    TYPE_SUPPLIER => 'TXT_SUPPLIERS',
    TYPE_CUSTOMER => 'TXT_CUSTOMERS',
    TYPE_SUBSIDIARY => 'TXT_SUBSIDIARIES',
]));

// GENDER
define('GENDER', serialize([
    MALE => __('STR_MALE'),
    FEMALE => __('STR_FEMALE'),
]));
