<?php if ($payment): ?>
<table class="table table-striped tb-prbr-view" id="tb-detail">
    <tbody>
        <tr>
            <td><?= __('TXT_CLINIC_NAME') ?> :</td>
            <td><?= $payment->customer_name ?></td>
        </tr>
        <tr>
            <td><?= __('TXT_PAYMENT_ID') ?> :</td>
            <td><?= $payment->id ?></td>
        </tr>
        <tr>
            <td><?= __('TXT_PAY_AMOUNT') ?> :</td>
            <td><?= $this->Comment->isJK($payment->sale_receive_payments[0]->sale->currency->code, $payment->amount) ?></td>
        </tr>
        <tr>
            <td><?= __('TXT_PAY_DATE') ?> :</td>
            <td><?php $pd = 'format_date' . $local; echo $payment->$pd; ?></td>
        </tr>
        <tr>
            <td><?= __('TXT_PAYEE_NAME') ?> :</td>
            <td><?= $payment->payee_name ?></td>
        </tr>
        <tr>
            <td><?= __('TXT_RECEIVE_BY') ?> :</td>
            <td><?= $payment->user['firstname' . $local] . ' ' . $payment->user['lastname' . $local] ?></td>
        </tr>
    </tbody>
</table>
<?php endif;