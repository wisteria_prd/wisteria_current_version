
<section class="modal-wrapper">
    <div class="modal fade advance-search" data-backdrop="static" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <?php
                    echo $this->Form->button('<span aria-hidden="true">&times;</span>', [
                        'type' => 'button',
                        'class' => 'close modal-close',
                        'data-dismiss' => 'modal',
                        'aria-label' => 'Close',
                        'escape' => false,
                    ]);
                    ?>
                    <h4 class="modal-title text-center">Payment Receive</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <?php
                            echo $this->Form->create(null, [
                                'url' => ['action' => 'index'],
                                'type' => 'get',
                                'role' => 'form',
                                'class' => 'form-horizontal',
                                'name' => 'advance_search',
                            ]);
                            echo $this->Form->hidden('type', [
                                'value' => ADVANCE_TYPE
                            ]);
                            ?>
                            <div class="form-group">
                                <label class="col-sm-4 control-label">
                                    <?= __('TXT_TO_CUSTOMER') ?>
                                </label>
                                <div class="col-sm-8 custom-select">
                                    <?= $this->Form->select('customer_id', [], [
                                        'class' => 'form-control',
                                        'id' => 'customer-id',
                                    ]) ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label">
                                    <?= __('TXT_SUBSIDIARIES') ?>
                                </label>
                                <div class="col-sm-8 custom-select">
                                    <?= $this->Form->select('subsidiary_id', [], [
                                        'class' => 'form-control',
                                        'id' => 'subsidiary-id',
                                    ]) ?>
                                </div>
                            </div>
                            <?php echo $this->Form->end(); ?>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <?php
                    echo $this->Form->button(__('TXT_CANCEL'), [
                        'type' => 'button',
                        'class' => 'btn btn-default btn-sm btn-width modal-close',
                        'data-dismiss' => 'modal',
                        'aria-label' => 'Close',
                    ]);
                    echo $this->Form->button(__('TXT_REGISTER'), [
                        'type' => 'button',
                        'class' => 'btn btn-primary btn-sm btn-width btn-sumbit',
                    ]);
                    ?>
                </div>
            </div>
        </div>
    </div>

    <script>
        $(function(e) {
            $('#customer-id').select2({
                width: '100%',
                minLength: 0,
                cache: false,
                ajax: {
                    url: '<?= $this->Url->build('/customers/get-autocomplete') ?>',
                    dataType: 'json',
                    delay: 250,
                    cache: false,
                    data: function (params) {
                        var query = { keyword: params.term };
                        return query;
                    },
                    processResults: function (data) {
                        var dataSource = [];
                        if (data.data === null && data.data === 'undefined') {
                            return false;
                        }
                        $.each(data.data, function(i, v) {
                            dataSource.push({
                                id: v.id,
                                text: v.name + ' ' + v.name_en
                            });
                        });
                        return { results: dataSource };
                    }
                }
            });

            $('#subsidiary-id').select2({
                width: '100%',
                minLength: 0,
                cache: false,
                ajax: {
                    url: '<?= $this->Url->build('/customer-subsidiaries/get-autocomplete-by-customer-id') ?>',
                    dataType: 'json',
                    delay: 250,
                    cache: false,
                    data: function (params) {
                        var query = {
                            customer_id: $('#customer-id').val(),
                            keyword: params.term
                        };
                        return query;
                    },
                    processResults: function (data) {
                        var dataSource = [];
                        if (data.data === null && data.data === 'undefined') {
                            return false;
                        }
                        $.each(data.data.data, function(i, v) {
                            var name = v.subsidiary.name + data.data.local;
                            if ((name === '') || (name === null)) {
                                switch(data.data.local) {
                                    case '<?= LOCAL_EN ?>':
                                        name = v.subsidiary.name_en;
                                        break;
                                    default:
                                        name = v.subsidiary.name;
                                        break;
                                }
                            }
                            dataSource.push({
                                id: v.subsidiary_id,
                                text: name
                            });
                        });
                        return { results: dataSource };
                    }
                }
            });

            $('#customer-id').change(function(e) {
                $('body').find('#subsidiary-id').empty();
            });

            $('.btn-sumbit').click(function(e) {
                $('form[name="advance_search"]').submit();
            });
        });
    </script>
</section>
