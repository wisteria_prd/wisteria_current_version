
<section class="modal-wrapper">
    <div class="modal fade modal-status" data-backdrop="static" role="dialog">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <meta name="id" content="<?php echo $data->id; ?>"/>
                    <meta name="status" content="<?php echo $status; ?>"/>
                    <?php
                    switch ($status) {
                        case 0:
                            echo __('MSG_CONFIRM_UNDO_SUSPEND');
                            $button = __('BTN_RECOVER');
                            break;
                        default:
                            echo __('QUESTION_CONFIRM_SUSPEND');
                            $button = __('BTN_SUSPEND');
                    }
                    ?>
                </div>
                <div class="modal-footer">
                    <?php
                    echo $this->Form->button(__('BTN_CANCEL'), [
                        'type' => 'button',
                        'class' => 'btn btn-default btn-sm btn-width modal-close',
                        'data-dismiss' => 'modal',
                        'aria-label' => 'Close',
                    ]);
                    echo $this->Form->button($button, [
                        'type' => 'button',
                        'class' => 'btn btn-primary btn-sm btn-width change-status',
                    ]);
                    ?>
                </div>
            </div>
        </div>
    </div>
</section>
