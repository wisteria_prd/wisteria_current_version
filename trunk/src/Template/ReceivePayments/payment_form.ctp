
<section class="modal-wrapper">
    <div class="modal fade" id="modal-recieve-payment" data-backdrop="static" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <?php
                    echo $this->Form->button('<span aria-hidden="true">&times;</span>', [
                        'type' => 'button',
                        'class' => 'close modal-close',
                        'data-dismiss' => 'modal',
                        'aria-label' => 'Close',
                        'escape' => false,
                    ]);
                    ?>
                    <h4 class="modal-title text-center"><?php echo __('RECEIVE_PAYMENT_TXT_REGISTER_A_PAYMENT_RECEIPT_RECORD'); ?></h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <?php
                            echo $this->Form->create(null, [
                                'role' => 'form',
                                'class' => 'form-horizontal',
                                'name' => 'payment_form'
                            ]);
                            echo $this->Form->hidden('digits', [
                                'id' => 'digits',
                                'value' => 0,
                            ]);
                            echo $this->Form->hidden('receiver_id', [
                                'id' => 'receiver-id',
                                'value' => $sale ? $sale->user_id : '',
                            ]);
                            echo $this->Form->hidden('customer_name', [
                                'id' => 'customer-name',
                                'value' => $sale ? $sale->customer_name : '',
                            ]);
                            echo $this->Form->hidden('customer_id', [
                                'id' => 'customer-id',
                                'value' => $sale ? $sale->customer_id : '',
                            ]);
                            echo $this->Form->hidden('target', [
                                'id' => 'target',
                                'value' => $target,
                            ]);
                            if ($receivePayment) {
                                echo $this->Form->hidden('id', [
                                    'value' => $receivePayment->id,
                                ]);
                            }
                            ?>
                            <div class="form-group">
                                <label class="col-sm-4 control-label"><?= __('RECEIVE_PAYMENT_TXT_RFP') ?>
                                    <?= $this->Comment->formAsterisk() ?>
                                </label>
                                <div class="col-sm-8 custom-select">
                                    <?php
                                    $options[0] = [
                                        'value'=> '',
                                        'text' => '',
                                    ];
                                    if ($sales) {
                                        foreach ($sales as $key => $value) {
                                            $options[] = [
                                                'value' => $value->id,
                                                'text' => $value->rop_number,
                                            ];
                                        }
                                    }
                                    echo $this->Form->select('sale_id', $options, [
                                        'class' => 'form-control',
                                        'id' => 'sale-id',
                                        'default' => $sale ? $sale->id : __('RECEIVE_PAYMENT_TXT_SELECT_RFP'),
                                        'empty' => ['' => __('RECEIVE_PAYMENT_TXT_SELECT_RFP')],
                                    ]);
                                    ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label">
                                    <?= __('RECEIVE_PAYMENT_TXT_AMOUNT_ON_RFP') ?> #
                                </label>
                                <label class="col-sm-8 control-label paid-number" style="text-align: left;">
                                    <?= $sale ? $sale->rop_number : ''; ?>
                                </label>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label">
                                    <?= __('RECEIVE_PAYMENT_TXT_DUE_AMOUNT') ?>
                                </label>
                                <label class="col-sm-8 control-label" style="text-align: left;">
                                    <span class="amount-not-paid">
                                        <?php
                                        if ($sale) {
                                            echo $this->Comment->currencyEnJpFormatS($local, $sale->sale_details[0]->total_amount, $sale->currency->code);
                                        }
                                        ?>
                                    </span>
                                </label>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label"><?= __('RECEIVE_PAYMENT_TXT_RECEIPTED_DATE') ?>
                                    <?= $this->Comment->formAsterisk() ?>
                                </label>
                                <div class="col-sm-8">
                                    <div class="input-group">
                                        <?= $this->Form->text('pay_date', [
                                            'class' => 'form-control',
                                            'id' => 'datepicker',
                                            'placeholder' => '',
                                            'default' => $receivePayment ? date('Y-m-d', strtotime($receivePayment->pay_date)) : date('Y-m-d'),
                                        ]) ?>
                                        <div class="input-group-btn">
                                            <button type="button" class="btn btn-default" aria-label="Calendar" id="trigger-calendar">
                                                <span class="glyphicon glyphicon-calendar"></span>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label"><?= __('RECEIVE_PAYMENT_TXT_RECEIPT_AMOUNT') ?>
                                    <?= $this->Comment->formAsterisk() ?>
                                </label>
                                <div class="col-sm-8">
                                    <?= $this->Form->text('amount', [
                                        'id' => 'amount',
                                        'class' => 'form-control',
                                        'placeholder' => __('RECEIVE_PAYMENT_TXT_ENTER_RECEIPT_AMOUNT'),
                                        'default' => $receivePayment ? $receivePayment->amount : '',
                                    ]) ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label"><?= __('RECEIVE_PAYMENT_TXT_PAYER') ?>
                                    <?= $this->Comment->formAsterisk() ?>
                                </label>
                                <div class="col-sm-8 custom-select">
                                    <?php
                                    $payeeNames = [];
                                    if ($receivePayment) {
                                        $payeeNames[] = [
                                            'text' => $receivePayment->payee_name,
                                            'value' => $receivePayment->payee_name,
                                        ];
                                    }
                                    echo $this->Form->select('payee_name', $payeeNames, [
                                        'class' => 'form-control',
                                        'id' => 'payee-name',
                                        'disabled' => $sale ? false : true,
                                        'default' => $receivePayment ? $receivePayment->payee_name : __('RECEIVE_PAYMENT_TXT_ENTER_PAYER_NAME'),
                                        'empty' => ['' => __('RECEIVE_PAYMENT_TXT_ENTER_PAYER_NAME')],
                                    ]);
                                    ?>
                                </div>
                            </div>
                            <?php echo $this->Form->end(); ?>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <?php
                    echo $this->Form->button(__('TXT_CANCEL'), [
                        'type' => 'button',
                        'class' => 'btn btn-default btn-sm btn-width modal-close',
                        'data-dismiss' => 'modal',
                        'aria-label' => 'Close',
                    ]);
                    echo $this->Form->button(__('TXT_REGISTER'), [
                        'type' => 'button',
                        'class' => 'btn btn-primary btn-sm btn-width btn-update-register',
                    ]);
                    ?>
                </div>
            </div>
        </div>
    </div>

    <script>
        $(function(e) {
            var digits = $('#digits').val();
            $('#sale-id').select2({
                width: '100%',
                minLength: 0,
                ajax: {
                    url: '<?= $this->Url->build('/sales/getAllSales/') ?>',
                    dataType: 'json',
                    delay: 250,
                    data: function (params) {
                        var query = { keyword: params.term };
                        return query;
                    },
                    processResults: function (data) {
                        var dataSource = [];
                        if (data.data === null && data.data === 'undefined') {
                            return;
                        }
                        $.each(data.data, function(i, v) {
                            dataSource.push({
                                id: v.id,
                                text: v.rop_number
                            });
                        });
                        return { results: dataSource };
                    }
                }
            });

            $('#payee-name').select2({
                width: '100%',
                minLength: 0,
                ajax: {
                    url: '<?= $this->Url->build('/customers/get-customer-by-id') ?>',
                    dataType: 'json',
                    delay: 250,
                    data: function (params) {
                        var query = {
                            customer_id: $('body').find('#customer-id').val(),
                            keyword: params.term
                        };
                        return query;
                    },
                    processResults: function (data) {
                        var dataSource = [];
                        if (data.data === null && data.data === 'undefined') {
                            return;
                        }
                        if ((data.data.payee_name === null)) {
                            dataSource.push({
                                id: data.keyword,
                                text: data.keyword
                            });
                        } else {
                            dataSource.push({
                                id: data.data.payee_name,
                                text: data.data.payee_name
                            });
                        }
                        return { results: dataSource };
                    }
                }
            });

            $('#datepicker').datetimepicker({
                format: 'YYYY-MM-DD',
                minDate: '2007-01-01'
            });

            $('#amount').inputmask('decimal', {
                'alias': 'numeric',
                'groupSeparator': ',',
                'autoGroup': true,
                'digits': digits,
                'radixPoint': '.',
                'digitsOptional': false,
                'allowMinus': false,
                'prefix': '',
                'placeholder': convertToInt(digits),
                numericInput: true,
                rightAlign : false
            });

            $('#sale-id').change(function(e) {
                var saleId = $(this).val();
                var options = {
                    type: 'GET',
                    url: '<?= $this->Url->build('/sales/getBySaleId/') ?>',
                    dataType: 'json',
                    data: { sale_id: saleId },
                    beforeSend: function() {
                        $.LoadingOverlay('show');
                        $('.amount-not-paid').inputmask('remove');
                        $('#customer-name').val('');
                        $('#customer-id').val('');
                        $('#receiver-id').val('');
                        $('#digits').val('');
                    }
                };
                ajaxRequest(options, function(data) {
                    var sale = data.data.sale;
                    var totalAmount = 0;
                    if ((sale !== 'undefined') && (sale !== null)) {
                        var saleDetail = sale.sale_details[0];
                        totalAmount = saleDetail ? saleDetail.total_amount : 0;
                    }
                    $('#customer-name').val(sale.customer_name);
                    $('#customer-id').val(sale.customer_id);
                    $('#receiver-id').val(sale.user_id);
                    $('#digits').val(sale.currency.decimal_format);
                    $('.paid-number').text(sale.rop_number);
                    $('.amount-not-paid').text(totalAmount)
                            .inputmask('decimal', {
                                'alias': 'numeric',
                                'groupSeparator': ',',
                                'autoGroup': true,
                                'radixPoint': ' ' + sale.currency.code,
                                'digitsOptional': false,
                                'allowMinus': false,
                                'prefix': ''
                            });
                    $('#payee-name').removeAttr('disabled', true);
                    $('#payee-name').val(null).trigger('change');
                });
            });

            $('.btn-update-register').click(function (e) {
                e.stopPropagation();
                var form = $('form[name="payment_form"]');
                var options = {
                    type: 'POST',
                    url: '<?= $this->Url->build(['action' => 'createAndUpdate']) ?>',
                    dataType: 'json',
                    data: form.serialize(),
                    beforeSend: function() {
                        $.LoadingOverlay('show');
                        $(form).find('.error-message').remove();
                    }
                };
                ajaxRequest(options, function(data) {
                    if (data.message === '<?= MSG_ERROR ?>') {
                        $.each(data.data, function(i, v) {
                            var message = '<label class="error-message">' + v + '</label>';
                            $(form).find('[name="' + i + '"]')
                                    .closest('.form-group')
                                    .find('.col-sm-8')
                                    .append(message);
                        });
                    } else {
                        location.reload();
                    }
                });
            });

            function convertToInt(digits) {
                var str = '';
                if (digits > 0) {
                    str += '0.';
                    for(i = 0; i < digits; i ++) {
                        str += '0';
                    }
                }
                return str;
            }

            function ajaxRequest(params, callback) {
                $.ajax(params).done(function(data) {
                    if (data === null && data === 'undefined') {
                        return false;
                    }
                    if (typeof callback === 'function') {
                        callback(data);
                    }
                }).fail(function(jqXHR, textStatus, errorThrown) {
                    if (errorThrown === 'Forbidden') {
                        if (confirm('<?= __('TXT_SESSION_TIMEOUT') ?>')) {
                            location.reload();
                        }
                    }
                }).always(function(data) {
                    $.LoadingOverlay('hide');
                });
            }
        });
    </script>
</section>
