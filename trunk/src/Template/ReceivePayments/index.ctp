
<?php
echo $this->Form->create('ReceivePayments', [
    'role' => 'form',
    'class' => 'form-inline form-search',
    'type' => 'get',
    'name' => 'form_search'
]);
$this->Form->templates([
    'inputContainer' => '{{content}}'
]);
echo $this->Form->hidden('displays', ['value' => $display]);
?>
<div class="form-group">
    <?php
    echo $this->Form->text('keyword', [
        'placeholder' => __d('receive_payment', 'TXT_ENTER_KEYWORDS'),
        'class' => 'form-control',
        'autocomplete' => 'off',
        'default' => $this->request->query('keyword'),
    ]);
    ?>
</div>
<div class="form-group custom-select" style="margin-left: 7px; width: 160px">
    <?php
    echo $this->Form->select('status', [
        TYPE_PAID => __d('receive_payment', 'TXT_RECEIVED'),
        TYPE_UNPAID => __d('receive_payment', 'TXT_NOT_RECEIVED'),
    ], [
        'empty' => ['' => __d('receive_payment', 'TXT_ALL')],
        'class' => 'form-control change-status',
        'label' => false,
        'default' => $this->request->query('status')
    ]);
    ?>
</div>
<?php  $this->SwitchStatus->render(); ?>
<?php echo $this->Form->end(); ?>
<div class="total-pagination" style="padding-top: 20px; padding-bottom: 20px;">
    <?php echo $this->element('display_info'); ?>
</div>
<div class="row search-section" style="margin-bottom: 20px;">
    <?php echo $this->element('display_number'); ?>
    <div class="col-sm-6 pull-right" style="text-align: right;">
        <?php
        echo $this->Form->button('Search', [
            'type' => 'button',
            'class' => 'btn btn-sm btn-primary btn-width btn-search',
        ]);
        echo '&nbsp;';
        echo $this->Form->button(__('BTN_REGISTER'), [
            'type' => 'button',
            'class' => 'btn btn-sm btn-primary btn-popup btn-width',
            'data-target' => TARGET_NEW,
        ]);
        ?>
    </div>
    <div class="clearfix"></div>
</div>
<div class="data-tb-list">
    <?php if ($receivePayment): ?>
    <table class="table table-striped">
        <thead>
            <tr>
                <th>#</th>
                <th>&nbsp;</th>
                <th class="white-space">
                    <?php echo $this->Paginator->sort('ReceivePayments.customer_name', __d('receive_payment', 'TXT_ORDER')); ?>
                    <i class="fa fa-sort-desc" aria-hidden="true"></i>
                </th>
                <th class="white-space">
                    <?php echo $this->Paginator->sort('Customers.name', __('RECEIVE_PAYMENT_TXT_PAYER')); ?>
                    <i class="fa fa-sort-desc" aria-hidden="true"></i>
                </th>
                <th class="white-space"><?= __('RECEIVE_PAYMENT_TXT_PO') ?></th>
                <th class="white-space"><?= __('RECEIVE_PAYMENT_TXT_RFP') ?></th>
                <th class="white-space"><?= __('RECEIVE_PAYMENT_TXT_RFP_DATE') ?></th>
                <th class="white-space">
                    <?php echo $this->Paginator->sort('ReceivePayments.created', __d('receive_payment', 'TXT_RECEIVED_DATE')); ?>
                    <i class="fa fa-sort-desc" aria-hidden="true"></i>
                </th>
                <th class="white-space"><?= __('RECEIVE_PAYMENT_TXT_RECEIVED_AMOUNT'); ?></th>
                <?php /* ?>
                <th class="white-space">
                    <?php
                    echo __d('receive_payment', 'TXT_BALANCE'); ?>
                </th>
                <?php */ ?>
                <th width="1%">&nbsp;</th>
                <th width="1%">&nbsp;</th>
            </tr>
        </thead>
        <tbody>
            <?php
            $numbering = $this->Paginator->counter('{{start}}');
            foreach ($receivePayment as $key => $value):
            ?>
            <tr data-id="<?php echo $value->id; ?>" data-target="<?php echo TARGET_EDIT ?>" data-payment-id="<?php echo $value->id; ?>">
                <th scope="row"><?= $numbering; $numbering++; ?></th>
                <td style="white-space: nowrap;">
                    <?php
                    echo $this->ActionButtons->disabledText($value->is_suspend); ?>
                </td>
                <td><?php echo h($value->customer_name); ?></td>
                <td>
                    <?php
                    if ($value->subsidiary_id === null) {
                        $obj = $value->customer;
                    } else {
                        $obj = $value->subsidiary;
                    }
                    echo $this->Comment->getFieldByLocal($obj, $local);
                    ?>
                </td>
                <td>
                    <?php
                    if ($value->sale_receive_payments) {
                        echo $value->sale_receive_payments[0]->sale->sale_number;
                    }
                    ?>
                </td>
                <td>
                    <?php
                    if ($value->sale_receive_payments) {
                        echo $value->sale_receive_payments[0]->sale->rop_number;
                    }
                    ?>
                </td>
                <td>
                    <?php
                    if ($value->sale_receive_payments) {
                        $issue_date = $value->sale_receive_payments[0]->sale->rop_issue_date;
                        echo date('Y-m-d', strtotime($issue_date));
                    }
                    ?>
                </td>
                <td>
                    <?php
                    echo date('Y-m-d', strtotime($value->created)); ?>
                </td>
                <td>
                    <?php
                    if ($value->sale_receive_payments) {
                        $currency = $value->sale_receive_payments[0]->sale->currency;
                        echo $this->Comment->isJK($currency->code, $value->amount);
                    }
                    ?>
                </td>
                <?php /* ?>
                <td>&nbsp;</td>
                <?php */ ?>
                <td data-target="<?php echo ($value->is_suspend) ? 0 : 1; ?>">
                    <?php
                    if ($value->is_suspend == 1) {
                        echo $this->Form->button(BTN_ICON_UNDO, [
                            'class' => 'btn btn-recover btn-sm btn-restore',
                            'escape' => false
                        ]);
                    } else {
                        echo $this->Form->button(BTN_ICON_EDIT, [
                            'class' => 'btn btn- btn-sm btn-popup btn-primary',
                            'escape' => false,
                            'data-target' => TARGET_EDIT,
                        ]);
                    }
                    ?>
                </td>
                <td data-target="<?php echo ($value->is_suspend) ? 0 : 1; ?>">
                    <?php
                    if ($value->is_suspend == 1) {
                        echo $this->Form->button(BTN_ICON_DELETE, [
                            'class' => 'btn btn-delete btn-sm',
                            'escape' => false
                        ]);
                    } else {
                        echo $this->Form->button(BTN_ICON_STOP, [
                            'class' => 'btn btn-suspend btn-sm btn-restore',
                            'escape' => false
                        ]);
                    }
                    ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <?php endif; ?>
</div>
<div class="row row-top-space">
    <?php echo $this->element('display_number'); ?>
    <?= $this->element('next_prev') ?>
    <div class="clearfix"></div>
</div>
<?php
echo $this->Html->css([
    '//cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/3.3.4/css/inputmask.min.css',
    'bootstrap-datetimepicker.min',
    'jquery-ui',
    'intlTelInput'
]);
echo $this->Html->script([
    'jquery.inputmask.bundle',
], ['blog' => 'script']);
?>
<script>
    $(function() {
        $('body').on('click', '.table>tbody>tr>td a', function(e) {
            e.stopPropagation();
        });

        $('body').on('click', '.btn-popup', function (e) {
            e.stopPropagation();
            var params = {
                target: $(this).attr('data-target'),
                sale_id: null,
                payment_id: null
            };
            if ($(this).attr('data-target') === '<?= TARGET_EDIT ?>') {
                params.sale_id = $(this).closest('tr').attr('data-id');
                params.payment_id = $(this).closest('tr').attr('data-payment-id');
            }
            var options = {
                type: 'GET',
                url: '<?= $this->Url->build(['action' => 'paymentForm']) ?>',
                data: params,
                beforeSend: function() {
                    $.LoadingOverlay('show');
                }
            };
            ajaxRequest(options, SESSION_TIMEOUT, function(data) {
                $('body').prepend(data);
                $('body').find('#modal-recieve-payment').modal('show');
            });
        });

        $('body').on('click', '.btn-search', function(e) {
            var options = {
                type: 'GET',
                url: '<?= $this->Url->build('/receive-payments/get-form-search') ?>',
                dataType: 'html',
                beforeSend: function() {
                    $.LoadingOverlay('show');
                }
            };
            ajaxRequest(options, SESSION_TIMEOUT, function(data) {
                $('body').prepend(data);
                $('body').find('.advance-search').modal('show');
            });
        });

        $('.btn-restore').click(function (e) {
            e.stopPropagation();
            let params = {
                id: $(this).closest('tr').attr('data-id'),
                status: $(this).closest('td').attr('data-target')
            };
            let options = {
                url: BASE + 'ReceivePayments/get-status',
                type: 'GET',
                data: params,
                beforeSend: function () {
                    $.LoadingOverlay('show');
                }
            };
            ajaxRequest(options, SESSION_TIMEOUT, function (data) {
                $('body').prepend(data);
                let content = $('body').find('.modal-status');
                $(content).modal('show');
                _iframe(content);
            });
        });

    	$('.btn-delete').click(function (e) {
            e.stopPropagation();
            let params = {
                id: $(this).closest('tr').attr('data-id')
            };
            let options = {
                url: BASE + 'ReceivePayments/get-delete',
                type: 'GET',
                data: params,
                beforeSend: function () {
                    $.LoadingOverlay('show');
                }
            };
            ajaxRequest(options, SESSION_TIMEOUT, function (data) {
                $('body').prepend(data);
                let content = $('body').find('.modal-delete');
                $(content).modal('show');
                _iframe(content);
            });
        });

        function _iframe(content)
        {
            $(content).find('.change-status').click(function (e) {
                let params = {
                    id: $(content).find('[name="id"]').attr('content'),
                    status: $(content).find('[name="status"]').attr('content')
                };
                let options = {
                    url: BASE + 'ReceivePayments/change-of-status',
                    type: 'POST',
                    data: params,
                    beforeSend: function () {
                        $.LoadingOverlay('show');
                    }
                };
                ajaxRequest(options, SESSION_TIMEOUT, function (data) {
                    if (data.message === MSG_SUCCESS) {
                        location.reload();
                    }
                });
            });

            $(content).find('.delete-mf').click(function (e) {
                let params = {
                    id: $(content).find('[name="id"]').attr('content')
                };
                let options = {
                    url: BASE + 'ReceivePayments/delete',
                    type: 'POST',
                    data: params,
                    beforeSend: function () {
                        $.LoadingOverlay('show');
                    }
                };
                ajaxRequest(options, SESSION_TIMEOUT, function (data) {
                    if (data.message === MSG_SUCCESS) {
                        location.reload();
                    }
                });
            });
        }

        var maxPage = parseInt($('#maxPage').val());
        updatePagination(maxPage);
    });
</script>
