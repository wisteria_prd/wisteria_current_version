<div class="search-section" style="margin-bottom: 20px;">
    <?php
    echo $this->Form->create('PurchaseConditions', [
        'class' => 'form-inline form-search',
        'type' => 'get',
        'name' => 'form_search',
        'autocomplete' => 'off',
    ]);
    echo $this->Form->hidden('displays', ['value' => $displays]);
    ?>
    <div class="form-group">
        <?php
        echo $this->Form->text('keyword', [
            'placeholder' => __('USER_SEARCH_KEYWORD'),
            'class' => 'form-control',
            'autocomplete' => 'off',
            'default' => $this->request->query('keyword'),
        ]);
        ?>
    </div>
    <div class="form-group custom-select">
        <?php echo $this->Form->select('supplier', [], [
            'label' => false,
            'empty' => ['' => __('TXT_SELECT_SUPPLIER')],
        ]); ?>
    </div>
    <?php echo $this->SwitchStatus->render(); ?>
    <?php echo $this->Form->end(); ?>
</div>
<div class="total-pagination" style="padding-bottom: 20px;">
    <?php echo $this->element('display_info'); ?>
</div>
<div class="row search-section" style="margin-bottom: 20px;">
    <?php echo $this->element('display_number'); ?>
    <div class="col-md-6 col-sm-6 text-center"></div>
    <?= $this->ActionButtons->btnRegisterNew('PurchaseConditions', [], 'createConditionBrand') ?>
    <div class="clearfix"></div>
</div>
<div class="data-tb-list">
    <?php if ($data): ?>
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>#</th>
                    <th>&nbsp;</th>
                    <th>
                        <?php
                        echo $this->Paginator->sort('PurchaseConditions.type', __('PURCHASE_BRAND_ASSOCIATE_TYPE')); ?>
                        <i class="fa fa-sort-desc" aria-hidden="true"></i>
                    </th>
                    <th>
                        <?php
                        echo $this->Paginator->sort('PurchaseConditions.external_id', __('TXT_SUPPLIER')); ?>
                        <i class="fa fa-sort-desc" aria-hidden="true"></i>
                    </th>
                    <th>
                        <?php
                        echo $this->Paginator->sort('Products.code', __('TXT_OLD_CODE')); ?>
                        <i class="fa fa-sort-desc" aria-hidden="true"></i>
                    </th>
                    <th>
                        <?php
                        echo $this->Paginator->sort('ProductBrands.name' . $locale, __('PRODUCTS_TXT_PRODUCT_BRAND')); ?>
                        <i class="fa fa-sort-desc" aria-hidden="true"></i>
                    </th>
                    <th>
                        <?php
                        echo $this->Paginator->sort('min_amount', __d('purchase_condition', 'TXT_MOQ')); ?>
                        <i class="fa fa-sort-desc" aria-hidden="true"></i>
                    </th>
                    <th>
                        <?php
                        echo $this->Paginator->sort('min_quantity', __d('purchase_condition', 'TXT_MOA')); ?>
                        <i class="fa fa-sort-desc" aria-hidden="true"></i>
                    </th>
                    <th width="1%">&nbsp;</th>
                    <th width="1%">&nbsp;</th>
                    <th width="1%">&nbsp;</th>
                </tr>
            </thead>
            <tbody class="normal-cursor">
                <?php
                $numbering = $this->Paginator->counter('{{start}}');
                foreach ($data as $key => $item):
                    $seller_product = $item->seller_brand->seller_products;
                    $product = $seller_product ? $seller_product[0]->product : '';
                ?>
                <tr data-id="<?php echo h($item->id); ?>">
                    <th scope="row" class="td_evt">
                        <?php
                        echo $numbering; $numbering++; ?>
                    </th>
                    <td>
                        <?php
                        echo $this->ActionButtons->disabledText($item->is_suspend) ?>
                    </td>
                    <td>
                        <?php
                        echo __(h($this->Comment->textHumanize($item->seller_brand->seller->type))); ?>
                    </td>
                    <td>
                        <?php
                        $seller = $item->seller_brand->seller;
                        if ($seller->manufacturer) {
                            echo $this->Comment->getFieldByLocal($seller->manufacturer, $locale);
                        }
                        if ($seller->supplier) {
                            echo $this->Comment->getFieldByLocal($seller->supplier, $locale);
                        }
                        ?>
                    </td>
                    <td>
                        <?php
                        echo $seller_product ? h($product->code) : ''; ?>
                    </td>
                    <td>
                        <?php
                        $product_brand = $item->seller_brand->product_brand;
                        if ($product_brand) {
                            echo $this->Comment->getFieldByLocal($product_brand, $locale);
                        }
                        ?>
                    </td>
                    <td>
                        <?php
                        if ($item->is_moq) {
                            echo __d('purchase_condition', 'TXT_YES');
                        } else {
                            echo __d('purchase_condition', 'TXT_NO');
                        }
                        ?>
                    </td>
                    <td>
                        <?php
                        if ($item->is_moa) {
                            echo __d('purchase_condition', 'TXT_YES');
                        } else {
                            echo __d('purchase_condition', 'TXT_NO');
                        }
                        ?>
                    </td>
                    <td>
                        <?php echo $this->Form->button('<i class="fa fa-comments-o" aria-hidden="true"></i>', [
                            'class' => 'btn btn-primary btn-sm btn_comment',
                            'data-id' => $item->id,
                            'escape' => false,
                            'data-type' => TYPE_SELLER_BRAND,
                        ]); ?>
                    </td>
                    <td data-target="<?php echo ($item->is_suspend) ? 0 : 1; ?>">
                        <?php
                        if ($item->is_suspend == 1) {
                            echo $this->Form->button(BTN_ICON_UNDO, [
                                'class' => 'btn btn-recover btn-sm btn-restore',
                                'escape' => false
                            ]);
                        } else {
                            echo $this->Html->link(BTN_ICON_EDIT, [
                                'controller' => 'PurchaseConditions',
                                'action' => 'editBrand', $item->id
                                ], [
                                'class' => 'btn btn-primary btn-sm',
                                'escape' => false
                            ]);
                        }
                        ?>
                    </td>
                    <td data-target="<?php echo ($item->is_suspend) ? 0 : 1; ?>">
                        <?php
                        if ($item->is_suspend == 1) {
                            if (!empty($item->product_brands) || !empty($item->count_person)) {
                                echo $this->Form->button(BTN_ICON_DELETE, [
                                    'class' => 'btn btn-delete btn-sm',
                                    'disabled' => 'disabled',
                                    'escape' => false
                                ]);
                            } else {
                                echo $this->Form->button(BTN_ICON_DELETE, [
                                    'class' => 'btn btn-delete btn-sm',
                                    'escape' => false
                                ]);
                            }
                        } else {
                            echo $this->Form->button(BTN_ICON_STOP, [
                                'class' => 'btn btn-suspend btn-sm btn-restore',
                                'escape' => false
                            ]);
                        }
                        ?>
                    </td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    <?php endif; ?>
</div>
<div class="row row-top-space" style="padding-bottom: 20px;">
    <?php echo $this->element('display_number'); ?>
    <?= $this->element('next_prev') ?>
    <div class="clearfix"></div>
</div>

<?php
echo $this->Html->script([
    'purchase-condition/brand',
], ['blog' => false]);
