
<?php
$standard_prices = $data->seller_standard_prices;
$std_price = [];
$packings = [];
$packing_units = [];
$purchase_prices = [];

if ($standard_prices) {
    foreach ($standard_prices as $key => $item) {
        $prd_detail = $item->product_detail;
        $su = $prd_detail->single_unit;
        $pz = $prd_detail->tb_pack_sizes;
        $std_price[] = [
            'name' => $prd_detail->pack_size . ' ' . $pz->name . ' X ' . $prd_detail->single_unit_size . ' ' . $su->name,
            'name_en' => $prd_detail->pack_size . ' ' . $pz->name_en . ' X ' . $prd_detail->single_unit_size . ' ' . $su->name_en,
        ];
        $packings[] = [
            'name' => $prd_detail->packing->name,
            'name_en' => $prd_detail->packing->name_en,
        ];
        $packing_units[] = [
            'name' => $prd_detail->packing->unit_name,
            'name_en' => $prd_detail->packing->unit_name_en,
        ];
        $purchase_prices[] = $item->purchase_standard_price;
    }
}
?>
<table class="table table-striped" id="tb-detail">
    <tbody>
        <tr>
            <td><?= __('TXT_PRODUCT_NAME') ?> :</td>
            <td><?php echo $data->product->name_en; ?></td>
            <td><?php echo $data->product->name; ?></td>
        </tr>
        <tr>
            <td><?= __('TXT_PRODUCT_DETAILS') ?> :</td>
            <td>
                <?php
                if ($std_price) {
                    echo '<ul class="email-wrap">';
                    foreach ($std_price as $key => $item) {
                        echo '<li>' . $item['name_en'] . '</li>';
                    }
                    echo '</ul>';
                }
                ?>
            </td>
            <td>
                <?php
                if ($std_price) {
                    echo '<ul class="email-wrap">';
                    foreach ($std_price as $key => $item) {
                        echo '<li>' . $item['name'] . '</li>';
                    }
                    echo '</ul>';
                }
                ?>
            </td>
        </tr>
        <tr>
            <td><?= __('TXT_PACKING') ?> :</td>
            <td>
                <?php
                if ($packings) {
                    echo '<ul class="email-wrap">';
                    foreach ($packings as $key => $item) {
                        echo '<li>' . $item['name_en'] . '</li>';
                    }
                    echo '</ul>';
                }
                ?>
            </td>
            <td>
                <?php
                if ($packings) {
                    echo '<ul class="email-wrap">';
                    foreach ($packings as $key => $item) {
                        echo '<li>' . $item['name'] . '</li>';
                    }
                    echo '</ul>';
                }
                ?>
            </td>
        </tr>
        <tr>
            <td><?= __('TXT_PACKING_UNIT') ?> :</td>
            <td>
                <?php
                if ($packing_units) {
                    echo '<ul class="email-wrap">';
                    foreach ($packing_units as $key => $item) {
                        echo '<li>' . $item['name_en'] . '</li>';
                    }
                    echo '</ul>';
                }
                ?>
            </td>
            <td>
                <?php
                if ($packing_units) {
                    echo '<ul class="email-wrap">';
                    foreach ($packing_units as $key => $item) {
                        echo '<li>' . $item['name'] . '</li>';
                    }
                    echo '</ul>';
                }
                ?>
            </td>
        </tr>
        <tr>
            <td><?= __('TXT_STANDARD_PRICE') ?> :</td>
            <td colspan="2">
                <?php
                if ($purchase_prices) {
                    echo '<ul class="email-wrap">';
                    foreach ($purchase_prices as $item) {
                        echo '<li>' . $data->currency->name . ' ' . $this->Comment->floatFormat($item) . '</li>';
                    }
                    echo '</ul>';
                }
                ?>
            </td>
        </tr>
        <tr>
            <td><?= __('TXT_SELLER') ?> :</td>
            <td><?php echo $data->seller_brand->seller->supplier->name_en; ?></td>
            <td><?php echo $data->seller_brand->seller->supplier->name; ?></td>
        </tr>
        <tr>
            <td><?= __('TXT_MANUFACTURERS') ?> :</td>
            <td><?php echo $data->seller_brand->seller->manufacturer->name_en; ?></td>
            <td><?php echo $data->seller_brand->seller->manufacturer->name; ?></td>
        </tr>
        <tr>
            <td><?= __('TXT_CURRENCY') ?> :</td>
            <td colspan="2">
                <?php echo $data->currency->name . '<br>' . $data->currency->code; ?>
            </td>
        </tr>
        <tr>
            <td><?= __('TXT_MIN_QUANTITY') ?> :</td>
            <td colspan="2"><?php echo $data->purchase_conditions[0]->min_quantity; ?></td>
        </tr>
        <tr>
            <td><?= __('TXT_MIN_AMOUNT') ?> :</td>
            <td colspan="2"><?php echo $data->purchase_conditions[0]->min_amount; ?></td>
        </tr>
    </tbody>
</table>
