<?php if ($payment): ?>
<table class="table table-striped tb-prbr-view" id="tb-detail">
    <tbody>
        <tr>
            <td><?php echo __('PAYMENT_SUPPLIER') ?> :</td>
            <td colspan="2">
                <?php
                if ($payment->sale_payments) {
                    $name = 'name' . $local;
                    if ($payment->sale_payments[0]->sale->seller->type == TYPE_MANUFACTURER) {
                        echo $payment->sale_payments[0]->sale->seller->manufacturer->$name;
                    } else {
                        echo $payment->sale_payments[0]->sale->seller->supplier->$name;
                    }
                }
                ?>
            </td>
        </tr>
        <tr>
            <td><?php echo __('PAYMENT_WPO') ?> :</td>
            <td colspan="2">
                <?php
                $sale_number = '';
                foreach ($payment->sale_payments as $sale) {
                    $sale_number .= $sale->sale->sale_number . ',';
                }
                echo rtrim($sale_number, ',');
                ?>
            </td>
        </tr>
        <tr>
            <td><?php echo __('PAYMENT_TRANSFER_AMOUNT') ?> :</td>
            <td colspan="2">
                <?php echo $this->Currency->format($payment->currency->code_en, $payment->paid_amount) ?>
            </td>
        </tr>
        <tr>
            <td><?php echo __('PAYMENT_BANK_CHARGE') ?> :</td>
            <td colspan="2">
                <?php echo $this->Currency->format($payment->currency->code_en, $payment->bank_charge) ?>
            </td>
        </tr>
        <tr>
            <td><?= __('PAYMENT_CURRENCY_TRANSFER_DATE') ?> :</td>
            <td colspan="2">
                <?php echo $this->Currency->format($payment->currency->code_en, $payment->currency_rate_oc_ap) ?>
            </td>
        </tr>
        <?php
        if ($payment->medias):
            $i = 0;
            foreach ($payment->medias as $media):
        ?>
        <tr>
            <td>
                <?php
                $file = DS . 'img' . DS . 'uploads' . DS . 'payments' . DS . $media->file_name;
                if ($i == 0) {
                    echo __('PAYMENT_PDF_FILE');
                } else {
                    echo '&nbsp;';
                }
                ?>
            </td>
            <td colspan="2">
                <?php
                echo $this->Html->link(
                    '<i class="fa fa-file-pdf-o" aria-hidden="true"></i> ' . __($this->Comment->textHumanize($media->document_type)),
                    $file,
                    [
                        'class' => 'btn btn-default btn-sm',
                        'target' => '_blank',
                        'role' => 'button',
                        'escape' => false
                    ]
                );
                ?>
            </td>
        </tr>
        <?php $i++; endforeach; else: ?>
        <tr>
            <td><?= __('PAYMENT_PDF_FILE') ?></td>
            <td colspan="2">&nbsp;</td>
        </tr>
        <?php  endif; ?>
    </tbody>
</table>
<?php endif;
