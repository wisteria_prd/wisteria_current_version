
<?php

echo $this->Form->create('Payments', [
    'role' => 'form',
    'class' => 'form-inline',
    'type' => 'get',
    'name' => 'form_search'
]);
$this->Form->templates([
    'inputContainer' => '{{content}}'
]);
echo $this->Form->hidden('displays', [
    'value' => empty($this->request->query('displays')) ? 10 : $this->request->query('displays')
]);
echo $this->Form->hidden('type', [
    'class' => 'supplier-types',
    'value' => $this->request->query('type')
]);
?>
    <div class="form-group" style="margin-right: 10px;">
        <?php
        echo $this->Form->input('keyword', [
            'name' => 'keyword',
            'placeholder' => __('USER_SEARCH_KEYWORD'),
            'class' => 'form-control',
            'label' => false,
            'value' => $this->request->query('keyword')
        ]);
        ?>
    </div>
    <div class="form-group" style="margin-right: 10px;">
        <select class="form-control select-supplier" name="supplier">
            <?= $this->Comment->sellerDropdownById($suppliers, $local, $this->request->query('supplier'), $this->request->query('type')); ?>
        </select>
    </div>
    <div class="form-group">
        <?php
        echo $this->Form->select('date', $this->Comment->dropdownMonth($months, 'purchase_date'), [
            'empty' => ['' => __('SELLER_BRANDS_TXT_SELECT_PAID_MONTH')],
            'class' => 'form-control select-date',
            'label' => false,
            'default' => $this->request->query('date')
        ]);
        ?>
    </div>
    <?php $this->SwitchStatus->render(); ?>
<?php echo $this->Form->end(); ?>
<div class="total-pagination" style="padding-top: 20px; padding-bottom: 20px;">
    <?php echo $this->element('display_info'); ?>
</div>
<div class="row search-section" style="margin-bottom: 20px;">
    <?php echo $this->element('display_number'); ?>
    <?= $this->ActionButtons->btnRegisterNew('Payments', [], 'create-purchase') ?>
    <div class="clearfix"></div>
</div>
<div class="data-tb-list">
    <?php if ($payments) : ?>
    <table class="table table-striped">
        <thead>
            <tr>
                <th>#</th>
                <th>&nbsp;</th>
                <th class="white-space">
                    <?php echo $this->Paginator->sort('', __('PAYMENT_PURCHASE_TXT_SUPPLIER')); ?>
                    <span><i class="fa fa-sort-desc" aria-hidden="true"></i></span>
                </th>
                <th class="white-space">
                    <?php echo $this->Paginator->sort('Purchases.purchase_number', __('PAYMENT_PURCHASE_TXT_OF')); ?>
                    <span><i class="fa fa-sort-desc" aria-hidden="true"></i></span>
                </th>
                <th class="white-space">
                    <?php echo $this->Paginator->sort('Payments.payment_date', __('PAYMENT_PURCHASE_TXT_PAYMENT_DATE')); ?>
                    <span><i class="fa fa-sort-desc" aria-hidden="true"></i></span>
                </th>
                <th class="white-space">
                    <?php echo $this->Paginator->sort('Purchases.amount', __('PAYMENT_PURCHASE_TXT_PRODUCT_AMOUNT')); ?>
                    <span><i class="fa fa-sort-desc" aria-hidden="true"></i></span>
                </th>
                <th class="white-space">
                    <?php echo $this->Paginator->sort('Payments.paid_amount', __('PAYMENT_PURCHASE_TXT_DUE_AMOUNT')); ?>
                    <span><i class="fa fa-sort-desc" aria-hidden="true"></i></span>
                </th>
                <th colspan="3">&nbsp;</th>
            </tr>
        </thead>
        <?php
        $numbering = $this->Paginator->counter('{{start}}');
        ?>
        <tbody>
            <?php foreach ($payments as $payment):?>
            <tr data-id="<?= $payment->payment->id ?>">
                <th scope="row"><?php echo $numbering; $numbering++; ?></th>
                <td>
                    <?= $this->ActionButtons->disabledText($payment->payment->is_suspend) ?>
                </td>
                <td>
                    <?php
                        if ($payment->purchase->seller->manufacturer) {
                            echo h($payment->purchase->seller->manufacturer->name);
                        } else {
                            echo h($payment->purchase->seller->supplier->name);
                        }
                    ?>
                </td>
                <td><?= h($payment->purchase->purchase_number) ?></td>
                <td><?= date('Y-m-d', strtotime($payment->payment->payment_date)) ?></td>
                <td><?= $this->Currency->format($payment->payment->currency->code_en, $payment->purchase->amount); ?></td>
                <td><?= $this->Currency->format($payment->payment->currency->code_en, $payment->payment->paid_amount) ?></td>
                <td data-target="<?php echo ($payment->payment->is_suspend) ? 1 : 0; ?>">
                    <?php
                    if ($payment->payment->is_suspend == 1) {
                        echo $this->Form->button(BTN_ICON_UNDO, [
                            'class' => 'btn btn-recover btn-sm btn_suspend', //btn_rc_ed
                            'data-name' => 'recover',
                            'escape' => false
                        ]);
                    } else {
                        echo $this->Html->link(BTN_ICON_EDIT, [
                            'controller' => 'sellerBrands',
                            'action' => 'edit', $payment->payment->id
                            ], [
                            'class' => 'btn btn-primary btn-sm',//btn_rc_ed
                            'escape' => false
                        ]);
                    }
                    ?>
                </td>
                <td data-target="<?php echo ($payment->payment->is_suspend) ? 1 : 0; ?>">
                    <?php
                    if ($payment->payment->is_suspend == 1) {
                        echo $this->Form->button(BTN_ICON_DELETE, [
                            'class' => 'btn btn-delete btn-sm',
                            'id' => 'btn_delete',
                            'data-name' => 'delete',
                            'escape' => false
                        ]);
                    } else {
                        echo $this->Form->button(BTN_ICON_STOP, [
                            'class' => 'btn btn-suspend btn-sm btn_suspend',
                            'data-name' => 'suspend',
                            'escape' => false
                        ]);
                    }
                    ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <?php endif; ?>
</div>
<div class="row row-top-space" style="padding-bottom: 20px;">
    <?php echo $this->element('display_number'); ?>
    <?= $this->element('next_prev') ?>
    <div class="clearfix"></div>
</div>

<!--Modal delete-->
<?php echo $this->element('Modal/delete'); ?>

<!--Modal Suspend-->
<?php echo $this->element('Modal/suspend'); ?>

<script>
    (function(e) {
        submitFormSearch('keyword');
        var optionData = [];
        // GetDataList(optionData);
        // typehead_initialize('#keyword', optionData);
        function GetDataList(optionData) {
            $.ajax({
                url: '<?php echo $this->Url->build('/payments/get-purchase-list'); ?>',
                type: 'get',
                dataType: 'json',
                contentType: 'application/json; charset=utf-8'
            }).done(function (data) {
                var response = data.data;
                if (response != null && response != undefined) {
                    $.each(response, function (i, v) {
                        optionData.push(v.purchase.purchase_number);
                        optionData.push(v.payment.payment_code);
                    });
                }
            });
        }

        $('body').on('click', '.tt-selectable', function (e) {
            $('form[name="form_search"]').submit();
        });

        $('body').on('change', '.select-supplier', function () {
            if ($('.supplier-types').val($(this).find('option:selected').attr('data-type'))) {
                $('form[name="form_search"]').submit();
            }
        });

        $('body').on('change', '.select-date', function () {
            $('form[name="form_search"]').submit();
        });

        var maxPage = parseInt($('#maxPage').val());
        updatePagination(maxPage);

        //modal confirm suspend
        $('body .data-tb-list .table > tbody > tr').on('click', '.btn_suspend', function (e) {
            e.stopPropagation();
            var content = $('#modal_suspend');
            var status = $(this).attr('data-name');
            var txt = '';

            switch (status) {
                case 'suspend':
                    $('#btn_suspend_yes').text('<?= __('BTN_SUSPEND') ?>');
                    txt = '<?= __('MSG_CONFIRM_SUSPEND') ?>';
                    break;
                case 'delete':
                    txt = '<?= __('MSG_CONFIRM_DELETE') ?>';
                    $('#btn_suspend_yes').text('<?= __('BTN_DELETE') ?>');
                    break;
                 case 'recover':
                    $('#btn_suspend_yes').text('<?= __('BTN_RECOVER') ?>');
                    txt = '<?= __('MSG_CONFIRM_UNDO_SUSPEND') ?>';
                    break;
            };

            $(content).find('#btn_suspend_yes').data('id', $(this).closest('tr').data('id'));
            $(content).find('#btn_suspend_yes').data('target', $(this).closest('td').data('target'));
            $(content).find('.modal-body p').html(txt);
            $(content).modal('show');
        });

        //modal change suspend status
        $('body').on('click', '#btn_suspend_yes', function (e) {
            var params = {
                id: $(this).data('id'),
                is_suspend: $(this).data('target')
            };
            $.post('<?php echo $this->Url->build('/payments/update-suspend/'); ?>', params, function (data) {
                if (data.message === 'success') {
                    location.reload();
                }
            }, 'json');
        });

        //modal delete confirm
        $('body').on('click', '#btn_delete', function (e) {
            e.stopPropagation();
            var content = $('#modal_delete');
            $(content).find('#btn_delete_yes').data('id', $(this).closest('tr').data('id'));
            $(content).modal('show');
        });

        //modal delete item.
        $('body').on('click', '#btn_delete_yes', function (e) {
            $.post('<?php echo $this->Url->build('/payments/delete/'); ?>', {id: $(this).data('id')}, function (data) {
                if (data.message === 'success') {
                    location.reload();
                }
            }, 'json');
        });
    })();
</script>
