<div class="row row-top-small-space">
    <div class="col-sm-12">
        <?= $this->element('display_info'); ?>
    </div>
</div>
<div class="row row-top-small-space">
    <div class="col-sm-12">
        <p><?= __('TXT_SELECTED_ITEM') ?> <span class="selected-items">0</span></p>
    </div>
</div>
<div class="row row-top-small-space">
    <div class="col-sm-12">
        <p><?= __('TXT_SELECTED_ITEM_AMT') ?> <span class="selected-currency"></span><span class="selected-amounts"></span></p>
    </div>
</div>
<div class="row row-top-small-space">
    <div class="col-sm-12" style="max-height: 400px; overflow-y: scroll;">
        <table class="table table-bordered table-condensed">
            <thead>
                <tr>
                    <th><input type="checkbox" value="all" class="check-alls" /> <?= __('TXT_ALL') ?></th>
                    <th>#</th>
                    <th class="white-space"><?= __('TXT_SALE_CODE') ?></th>
                    <th class="white-space"><?= __('TXT_CLINIC_NAME') ?></th>
                    <th class="white-space"><?= __('TXT_PRODUCT_AMOUNT') ?></th>
                    <th class="white-space"><?= __('TXT_CURRENCY') ?></th>
                </tr>
            </thead>
            <tbody>
                <?php
                if ($sales):
                    $numbering = $this->Paginator->counter('{{start}}');
                    foreach ($sales as $sale):
                    $supplier = '';
                    $type = $sale->seller->type;
                    if ($sale->seller->type == TYPE_SUPPLIER) {
                        $supplier = $sale->seller->supplier->id;
                    } else if ($sale->seller->type == TYPE_MANUFACTURER) {
                        $supplier = $sale->seller->manufacturer->id;
                    }
                ?>
                <tr>
                    <td>
                        <input
                            type="checkbox"
                            class="check-value"
                            id="each-<?= $sale->id ?>"
                            data-sellerid="<?= $sale->seller->id ?>"
                            data-salenumber="<?= $sale->sale_number ?>"
                            data-supplyid="<?= $supplier ?>"
                            data-currency="<?= $sale->currency->id ?>"
                            data-currencycode="<?= $sale->currency->code ?>"
                            data-amount="<?= $sale->amount ?>"
                            data-type="<?= $type ?>"
                            value="<?= $sale->id ?>" />
                    </td>
                    <td><?php echo $numbering; $numbering++; ?></td>
                    <td><?= h($sale->sale_number) ?></td>
                    <td><?= h($sale->customer->$name) ?></td>
                    <td class="value-amount"><?= $sale->amount ?></td>
                    <td class="value-currency"><?= h($sale->currency->code) ?></td>
                </tr>
                <?php endforeach; endif; ?>                
            </tbody>
        </table>
    </div>
</div>