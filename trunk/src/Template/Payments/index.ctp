
<?php
$name = 'name' . $local;
echo $this->Form->create('Payments', [
    'role' => 'form',
    'class' => 'form-inline form-search',
    'type' => 'get',
    'name' => 'form_search'
]);
$this->Form->templates([
    'inputContainer' => '{{content}}'
]);
echo $this->Form->hidden('type', [
    'class' => 'supplier-types',
    'value' => $this->request->query('type')
]);
echo $this->Form->hidden('displays', ['value' => $displays]); ?>
<div class="form-group" style="margin-right: 10px;">
    <?php
    echo $this->Form->text('keyword', [
        'name' => 'keyword',
        'placeholder' => __('PAYMENT_ENTER_KEYWORD'),
        'class' => 'form-control',
        'label' => false,
        'value' => $this->request->query('keyword'),
        'autocomplete' => 'off',
    ]); ?>
</div>
<div class="form-group custom-select" style="margin-right: 10px;">
    <select class="form-control select-supplier" name="supplier">
        <?= $this->Comment->sellerDropdownById($suppliers, $local, $this->request->query('supplier'), $this->request->query('type')); ?>
    </select>
</div>
<div class="form-group custom-select">
    <?php
    echo $this->Form->select('date', $this->Comment->dropdownMonth($months, 'sale_date'), [
        'empty' => ['' => __('PAYMENT_SELECT_MONTH')],
        'class' => 'form-control select-date',
        'label' => false,
        'default' => $this->request->query('date'),
    ]); ?>
</div>
<?php $this->SwitchStatus->render(); ?>
<?php echo $this->Form->end(); ?>
<div class="total-pagination" style="padding-top: 20px; padding-bottom: 20px;">
    <?php echo $this->element('display_info'); ?>
</div>
<div class="row search-section" style="margin-bottom: 20px;">
    <?php echo $this->element('display_number'); ?>
    <?= $this->ActionButtons->btnRegisterNew('Payments', []) ?>
    <div class="clearfix"></div>
</div>
<div class="data-tb-list">
    <?php if ($payments) : ?>
    <table class="table table-striped">
        <thead>
            <tr>
                <th>#</th>
                <th>&nbsp;</th>
                <th class="white-space">
                    <?php echo __('PAYMENT_SUPPLIER') ?>
                </th>
                <th class="white-space">
                    <?php echo __('PAYMENT_WPO'); ?>
                </th>
                <th class="white-space">
                    <?php echo __d('payment', 'TXT_WPO_ISSUE_DATE'); ?>
                </th>
                <th class="white-space">
                    <?php echo $this->Paginator->sort('Payments.payment_date', __('PAYMENT_PURCHASE_TXT_PAYMENT_DATE')); ?>
                    <span><i class="fa fa-sort-desc" aria-hidden="true"></i></span>
                </th>
                <th class="white-space"><?= __('PAYMENT_TRANSFTER_AMOUNT'); ?></th>
                <th colspan="2">&nbsp;</th>
            </tr>
        </thead>
        <?php
        $numbering = $this->Paginator->counter('{{start}}'); ?>
        <tbody>
            <?php
            foreach ($payments as $payment):
            $total_amount = 0;
            $sale_count = 0;
            // get manufacturers or suppliers from seller base on associate
            $seller_assoc = null;
            $sale = null;
            if ($payment->sale_payments) {
                $total_amount = $payment->sale_payments[0]->total_amount;
                $sale_count = $payment->sale_payments[0]->count_record;
                $sale = $payment->sale_payments[0]->sale;
                $seller = $sale->seller;

                if ($seller->manufacturer) {
                    $seller_assoc = $seller->manufacturer;
                } else {
                    $seller_assoc = $seller->supplier;
                }
            }
            ?>
            <tr data-payid="<?= $payment->id ?>" data-id="<?php echo $payment->id; ?>">
                <th scope="row"><?php echo $numbering; $numbering++; ?></th>
                <td>
                    <?php
                    echo $this->ActionButtons->disabledText($payment->is_suspend); ?>
                </td>
                <td>
                    <?php
                    echo $this->Comment->getFieldByLocal($seller_assoc, $local); ?>
                </td>
                <td>
                <?php
                if ($sale_count > 1) {
                    echo $sale_count . ' ' . __('Sales');
                } else {
                    echo $sale ? h($sale->sale_number) : null;
                }
                ?>
                </td>
                <td>
                    <?php
                    echo $sale ? data('Y-m-d', strtotime($sale->rop_issue_date)) : null; ?>
                </td>
                <td>
                    <?php
                    echo date('Y-m-d', strtotime($payment->payment_date)); ?>
                </td>
                <td>
                    <?php
                    echo $this->Currency->format($payment->currency->code_en, $total_amount); ?>
                </td>
                <td width="1%" data-target="<?php echo ($payment->is_suspend) ? 1 : 0; ?>">
                    <?php
                    if ($payment->is_suspend == 1) {
                        echo $this->Form->button(BTN_ICON_UNDO, [
                            'class' => 'btn btn-recover btn-sm btn_suspend',
                            'data-name' => 'recover',
                            'escape' => false
                        ]);
                    } else {
                        echo $this->Html->link(BTN_ICON_EDIT, [
                            'controller' => 'payments',
                            'action' => 'edit', $payment->id,
                            '?' => ['pkg_id' => $this->request->query('pkg_id')]
                            ], [
                            'class' => 'btn btn-primary btn-sm',
                            'escape' => false
                        ]);
                    }
                    ?>
                </td>
                <td width="1%" data-target="<?php echo ($payment->is_suspend) ? 1 : 0; ?>">
                    <?php
                    if ($payment->is_suspend == 1) {
                        echo $this->Form->button(BTN_ICON_DELETE, [
                            'class' => 'btn btn-delete btn-sm',
                            'id' => 'btn_delete',
                            'escape' => false
                        ]);
                    } else {
                        echo $this->Form->button(BTN_ICON_STOP, [
                            'class' => 'btn btn-suspend btn-sm btn_suspend',
                            'data-name' => 'suspend',
                            'escape' => false
                        ]);
                    }
                    ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <?php endif; ?>
</div>
<div class="row row-top-space" style="padding-bottom: 20px;">
    <?php echo $this->element('display_number'); ?>
    <?= $this->element('next_prev') ?>
    <div class="clearfix"></div>
</div>

<!--Modal delete-->
<?php echo $this->element('Modal/delete'); ?>
<!--Modal Detail-->
<?php echo $this->element('Modal/view_detail'); ?>
<!--Modal Suspend-->
<?php echo $this->element('Modal/suspend'); ?>

<script>
    (function(e) {
        $('body').on('change', '.select-supplier', function () {
            if ($('.supplier-types').val($(this).find('option:selected').attr('data-type'))) {
                $('form[name="form_search"]').submit();
            }
        });

        $('body').on('change', '.select-date', function () {
            $('form[name="form_search"]').submit();
        });

        $('body').on('click', '.table>tbody>tr>td a', function(e) {
            e.stopPropagation();
        });

        $('body').on('click', '.data-tb-list .table>tbody>tr', function (e) {
            $.LoadingOverlay('show');
            var id = $(this).closest('tr').data('payid');
            $.get('<?= $this->Url->build('/payments/view/'); ?>' + id, function (data) {
                $.LoadingOverlay('hide');
                $('#modal_detail').find('.modal-body').html(data);
                $('#modal_detail').modal('show');
            }, 'html');
        });

        var maxPage = parseInt($('#maxPage').val());
        updatePagination(maxPage);

        //modal confirm suspend
        $('body').on('click', '.btn_suspend', function (e) {
            e.stopPropagation();
            $('.suspend-item-id').val($(this).attr('data-id'));
            $('.is-suspend-item').val($(this).attr('data-target'));
            var confirm = '';
            if (parseInt($(this).attr('data-target')) === 1) {
                $('#btn_suspend_yes').text('<?= __('BTN_SUSPEND') ?>');
                confirm = '<?= __('MSG_CONFIRM_SUSPEND') ?>';
            } else {
                $('#btn_suspend_yes').text('<?= __('BTN_RECOVER') ?>');
                confirm = '<?= __('MSG_CONFIRM_UNDO_SUSPEND') ?>';
            }
            $('.confirm-suspend-text').text(confirm);
            $('#modal_suspend').modal('show');
        });

         //modal change suspend status
         $('body .data-tb-list .table > tbody > tr').on('click', '.btn_suspend', function (e) {
             e.stopPropagation();
             var content = $('#modal_suspend');
             var status = $(this).attr('data-name');
             var txt = '';

             switch (status) {
                 case 'suspend':
                     $('#btn_suspend_yes').text('<?= __('BTN_SUSPEND') ?>');
                     txt = '<?= __('MSG_CONFIRM_SUSPEND') ?>';
                     break;
                 case 'delete':
                     txt = '<?= __('MSG_CONFIRM_DELETE') ?>';
                     $('#btn_suspend_yes').text('<?= __('BTN_DELETE') ?>');
                     break;
                  case 'recover':
                     $('#btn_suspend_yes').text('<?= __('BTN_RECOVER') ?>');
                     txt = '<?= __('MSG_CONFIRM_UNDO_SUSPEND') ?>';
                     break;
             };
             $(content).find('#btn_suspend_yes').data('id', $(this).closest('tr').data('id'));
             $(content).find('#btn_suspend_yes').data('target', $(this).closest('td').data('target'));
             $(content).find('.modal-body p').html(txt);
             $(content).modal('show');
         });
         $('body').on('click', '#btn_suspend_yes', function (e) {
             var params = {
                 id: $(this).data('id'),
                 is_suspend: $(this).data('target')
             };
             // console.log(params);return false;
             $.post('<?php echo $this->Url->build('/payments/update-suspend/'); ?>', params, function (data) {
                 if (data.message === 'success') {
                     location.reload();
                 }
             }, 'json');
         });

        //modal delete confirm
        $('body').on('click', '.btn-delete-payment', function (e) {
            e.stopPropagation();
            $('#btn_delete_yes').text('<?= __('BTN_DELETE') ?>');
            $('.delete-item').val($(this).attr('data-id'));
            $('#modal_delete').modal('show');
        });

        //modal delete item.
        $('body').on('click', '#btn_delete_yes', function() {
            var data = $('.form-delete').serialize();
            var url = '<?= $this->Url->build('/payments/delete') ?>';
            itemDelete(url, data, '<?php echo __('TXT_SESSION_TIMEOUT') ?>');
        });
    })();
</script>
