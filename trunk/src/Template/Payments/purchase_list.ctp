<div class="row row-top-small-space">
    <div class="col-sm-12">
        <?= $this->element('display_info'); ?>
    </div>
</div>
<div class="row row-top-small-space">
    <div class="col-sm-12">
        <p><?= __('TXT_SELECTED_ITEM') ?> <span class="selected-items">0</span></p>
    </div>
</div>
<div class="row row-top-small-space">
    <div class="col-sm-12">
        <p><?= __('TXT_SELECTED_ITEM_AMT') ?> <span class="selected-currency"></span><span class="selected-amounts"></span></p>
    </div>
</div>
<div class="row row-top-small-space">
    <div class="col-sm-12" style="max-height: 400px; overflow-y: scroll;">
        <table class="table table-bordered table-condensed">
            <thead>
                <tr>
                    <th><input type="checkbox" value="all" class="check-alls" /> <?= __('TXT_ALL') ?></th>
                    <th>#</th>
                    <th class="white-space"><?= __('TXT_PURCHASE_CODE') ?></th>
                    <th class="white-space"><?= __('TXT_PRODUCT_AMOUNT') ?></th>
                    <th class="white-space"><?= __('TXT_CURRENCY') ?></th>
                </tr>
            </thead>
            <tbody>
                <?php
                if ($purchases):
                    $numbering = $this->Paginator->counter('{{start}}');
                    foreach ($purchases as $purchase):
                ?>
                <tr>
                    <td><input type="checkbox" class="check-value" data-purchasenumber="<?= $purchase->purchase_number ?>" value="<?= $purchase->id ?>" /></td>
                    <td><?php echo $numbering; $numbering++; ?></td>
                    <td><?= h($purchase->purchase_number) ?></td>
                    <td class="value-amount"><?= $purchase->amount ?></td>
                    <td class="value-currency"><?= h($purchase->currency->code) ?></td>
                </tr>
                <?php endforeach; endif; ?>                
            </tbody>
        </table>
    </div>
</div>