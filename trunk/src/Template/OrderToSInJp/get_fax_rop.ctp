
<section class="modal-wrapper">
    <div class="modal fade fax-rop" data-backdrop="static" role="dialog">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <?= __('MSG_CONFIRM_CHANGE_DRAFF_STATUS_TO_FAX_ROP') ?>
                </div>
                <div class="modal-footer">
                    <?php
                    echo $this->Form->button(__('TXT_NO'), [
                        'type' => 'button',
                        'class' => 'btn btn-default btn-sm btn-width modal-close',
                        'data-dismiss' => 'modal',
                        'aria-label' => 'Close',
                    ]);
                    echo $this->Form->button(__('TXT_YES'), [
                        'type' => 'button',
                        'class' => 'btn btn-primary btn-sm btn-width btn-yes',
                        'data-id' => $id,
                    ]);
                    ?>
                </div>
            </div>
        </div>
    </div>
</section>

<script>
    $(function(e) {
        $('body').on('click', '.btn-yes', function(e) {
            $.LoadingOverlay('show');
            ajax_request_post('<?= $this->Url->build(['action' => 'changeDraffStatus']) ?>',
            { id: $(this).attr('data-id') },
            function(data) {
                if (data.message === '<?= MSG_SUCCESS ?>') {
                    location.reload();
                }
            }, 'json');
        });
    });
</script>