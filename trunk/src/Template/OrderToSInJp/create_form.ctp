
<section class="modal-wrapper">
    <style>
        .create-form3 {
            z-index: 1051;
        }
    </style>
    <div class="modal fade create-form3" data-backdrop="static" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <?php
                    echo $this->Form->button('<span aria-hidden="true">&times;</span>', [
                        'type' => 'button',
                        'class' => 'close modal-close',
                        'data-dismiss' => 'modal',
                        'aria-label' => 'Close',
                        'escape' => false,
                    ]);
                    ?>
                    <h4 class="modal-title text-center"><?= __('ORDER_MANAGEMENT_TXT_DOMESTIC_TRADE_PURCHASE_ORDER') ?></h4>
                </div>
                <div class="modal-body">
                    <?php
                    $affiliation_class = $this->request->session()->read('user_group.affiliation_class');
                    echo $this->Form->create($data, [
                        'role' => 'form',
                        'class' => 'form-horizontal',
                        'name' => 'order_form',
                        'onsubmit' => 'return false;',
                    ]);
                    echo $this->Form->hidden('type', [
                        'id' => 'type',
                        'value' => TRADE_TYPE_PURCHASE_W_STOCK
                    ]);
                    if (!$data->isNew()) {
                        echo $this->Form->hidden('id');
                        $affiliation_class = $data->affiliation_class;
                    }
                    echo $this->Form->hidden('affiliation_class', ['value' => $affiliation_class]);
                    ?>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">
                            <?= __('TXT_SELLER'); ?><i class="required-indicator">＊</i>
                        </label>
                        <div class="col-sm-9 custom-select">
                            <?php
                            echo $this->Form->select('seller_id', $sellers,[
                                'class' => 'form-control',
                                'label' => false,
                                'empty' => __('ORDER_MANAGEMENT_TXT_SELECT_SUPPLIER'),
                            ]);
                            ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">
                            <?= __('ORDER_MANAGEMENT_TXT_CURRENCY'); ?><i class="required-indicator">＊</i>
                        </label>
                        <div class="col-sm-9 custom-select">
                            <?php
                            echo $this->Form->select('currency_id', $currencies, [
                                'class' => 'form-control',
                                'label' => false,
                                'empty' => __('ORDER_MANAGEMENT_TXT_SELECT_CURRENCY'),
                            ]);
                            ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">
                            <?= __('ORDER_MANAGEMENT_TXT_OF_ISSUE_DATE'); ?><i class="required-indicator">＊</i>
                        </label>
                        <div class="col-sm-9">
                            <div class="input-group">
                                <?php
                                echo $this->Form->text('issue_date', [
                                    'class' => 'form-control',
                                    'label' => false,
                                    'placeholder' => __('TXT_SELECT_DATE'),
                                    'value' => !$data->isNew() ? date('Y-m-d', strtotime($data->issue_date)) : date('Y-m-d'),
                                ]);
                                ?>
                                <div class="input-group-btn">
                                    <button type="button" class="btn btn-default" aria-label="Calendar" id="trigger-calendar">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php echo $this->Form->end(); ?>
                </div>
                <div class="modal-footer">
                    <?php
                    echo $this->Form->button(__('TXT_CANCEL'), [
                        'type' => 'button',
                        'class' => 'btn btn-default btn-sm btn-width modal-close',
                        'data-dismiss' => 'modal',
                        'aria-label' => 'Close',
                    ]);
                    echo $this->Form->button(__('TXT_REGISTER'), [
                        'type' => 'button',
                        'class' => 'btn btn-primary btn-sm btn-width btn-sw-stock',
                        'aria-label' => 'Close',
                        'v-on:click' => 'register',
                    ]);
                    ?>
                </div>
            </div>
        </div>
    </div>

    <script>
        var saleWStock = new Vue({
            el: '.create-form3',
            data: {
                url: '<?= $this->Url->build(['action' => 'registerSale']) ?>'
            },
            methods: {
                register: function(e) {
                    $.LoadingOverlay('show')
                    var form = $('form[name="order_form"]')
                    this.$http.post(this.url, { data: form.serialize() }, { emulateJSON: true, emulateHTTP: true })
                        .then(function(response) {
                            $(form).find('.error-message').remove()
                            this.data = response.body
                            if (this.data.message === MSG_ERROR) {
                                showMessageError(form, this.data.data)
                            } else {
                                location.href = '<?= $this->Url->build(['action' => 'index']) ?>'
                            }
                            $.LoadingOverlay('hide')
                        });
                }
            },
            mounted: function() {
                // datetime initialize
                $('#issue-date').datetimepicker({
                    format: 'YYYY-MM-DD',
                    minDate: '2007-01-01'
                });

                // select2 initialize
                $('body').find('select').select2({ width: '100%' });
            }
        })
    </script>
</section>
