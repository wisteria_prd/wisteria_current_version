
<?php
$local = $this->request->session()->read('tb_field');
$disabled = false;
if ($saleDetail) {
    $status = $saleDetail->sale->status;
    if (($status === PO_STATUS_CUSTOM_CLEARANCE) || ($status === PO_STATUS_DELIVERED) || ($status === PO_STATUS_REQUESTED_CC)) {
        $disabled = true;
    }
    echo $this->Form->hidden('sale_status', ['value' => $status]);
}
?>
<div class="row">
    <div class="col-md-12">
        <?php
        echo $this->Form->create(null, [
                'class' => 'form-po-settings form-horizontal',
                'autocomplete' => 'off',
                'onsubmit' => 'return false;',
            ]
        );
        $this->Form->templates([
            'inputContainer' => '{{content}}'
        ]);
        $stock_ids = [];
        $quantities = [];

        if ($data) {
            foreach ($data as $key => $value) {
                $stock_ids[$key] = $value->stock_id;
                $quantities[$key] = $value->quantity;
            }
        }
        echo $this->Form->hidden('sale_detail_id', ['value' => $saleDetail->id]);
        echo $this->Form->hidden('sale_detail_quantity', ['value' => $saleDetail->quantity]);
        ?>
        <div class="form-group">
            <label for="" class="col-md-4"><?= __('TXT_PRODUCT') ?></label>
            <div class="col-md-8">
                <?php
                $product = '';
                if (!empty($saleDetail->brand_name)) {
                    $product = $saleDetail->brand_name . ' ';
                }

                if (!empty($saleDetail->product_name)) {
                    $product .= $saleDetail->product_name . ' ';
                }

                if (!empty($saleDetail->single_unit_value) && !empty($saleDetail->pack_size_value)) {
                    $sgun = $saleDetail->single_unit_name;
                    $psn = $saleDetail->pack_size_name;
                    if ($saleDetail->pack_size_value > 1) {
                        $psn = $this->Pluralize->pluralize($psn);
                    }
                    $product .= '(' .
                        $saleDetail->pack_size_value .
                        $sgun . ' x ' .
                        $saleDetail->pack_size_value .
                        $psn . ') ';
                } elseif (empty($saleDetail->single_unit_value) && !empty($saleDetail->pack_size_value)) {
                    $psn = $saleDetail->pack_size_name;
                    if ($saleDetail->pack_size > 1) {
                        $psn = $this->Pluralize->pluralize($psn);
                    }
                    $product .= '(' .
                        $saleDetail->pack_size_value .
                        $psn . ') ';
                } elseif (empty($saleDetail->pack_size_value) && !empty($saleDetail->single_unit_value)) {
                    $sgun = $saleDetail->single_unit_name;
                    if ($saleDetail->single_unit_size > 1) {
                        $sgun = $this->Pluralize->pluralize($sgun);
                    }
                    $product .= '(' .
                        $saleDetail->single_unit_value .
                        $sgun . ') ';
                }

                if (!empty($saleDetail->description)) {
                    $product .= '(' . $saleDetail->description . ')';
                }
                echo $product;
                ?>
            </div>
        </div>
        <div class="form-group">
            <label for="" class="col-md-4"><?= __('TXT_QUANTITY') ?></label>
            <div class="col-md-5"><?php echo $saleDetail->quantity; ?></div>
        </div>
        <div class="form-group custom-select">
            <label for="" class="col-md-4"><?= __('TXT_LOT_EXPIRED') ?></label>
            <div class="col-md-3 custom-select no-padding-r">
                <?php
                echo $this->Form->input('stock_id', [
                    'type' => 'select',
                    'name' => 'stock_id[0]',
                    'class' => 'form-control',
                    'label' => false,
                    'options' => $stocks,
                    'disabled' => $disabled,
                    'value' => $stock_ids ? $stock_ids[0] : null,
                ]);
                ?>
            </div>
            <div class="col-md-3 no-padding-r">
                <?php
                echo $this->Form->input('quantity', [
                    'type' => 'number',
                    'name' => 'quantity[0]',
                    'label' => false,
                    'class' => 'form-control quantity',
                    'placeholder' => __('TXT_ENTER_QUANTITY'),
                    'disabled' => $disabled,
                    'value' => $quantities ? $quantities[0] : null,
                ]);
                ?>
            </div>
            <div class="col-md-2">
                <?php
                echo $this->Form->button('<i class="fa fa-plus" aria-hidden="true"></i>', [
                    'type' => 'button',
                    'class' => 'btn btn-primary btn-stock-add',
                    'disabled' => $disabled,
                    'escape' => false,
                ]);
                ?>
            </div>
        </div>
        <div class="main-wrap">
            <?php
            if ($stock_ids) :
                foreach ($stock_ids as $key => $value) :
                if ($key > 0) :
            ?>
            <div class="form-group custom-select">
                <label for="" class="col-md-4"></label>
                <div class="col-md-3 custom-select no-padding-r">
                    <?php
                    echo $this->Form->input('stock_id', [
                        'type' => 'select',
                        'name' => 'stock_id[' . $key . ']',
                        'class' => 'form-control',
                        'label' => false,
                        'options' => $stocks,
                        'disabled' => $disabled,
                        'value' => $value,
                    ]);
                    ?>
                </div>
                <div class="col-md-3 no-padding-r">
                    <?php
                    echo $this->Form->input('quantity', [
                        'type' => 'number',
                        'name' => 'quantity[' . $key . ']',
                        'label' => false,
                        'disabled' => $disabled,
                        'class' => 'form-control quantity',
                        'placeholder' => __('TXT_ENTER_QUANTITY'),
                        'value' => $quantities[$key]
                    ]);
                    ?>
                </div>
                <div class="col-md-2">
                    <?php
                    if ($disabled == false) {
                        echo $this->Form->button('<i class="fa fa-trash" aria-hidden="true"></i>', [
                            'type' => 'button',
                            'class' => 'btn btn-delete btn-stock-rm',
                            'escape' => false,
                        ]);
                    }
                    ?>
                </div>
            </div>
            <?php endif; endforeach; endif; ?>
        </div>
        <?= $this->Form->end() ?>
        <p id="warning-msg" class="text-center error-message"></p>
    </div>
</div>
