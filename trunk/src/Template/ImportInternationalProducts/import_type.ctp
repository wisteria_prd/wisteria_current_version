<?php
    $btn = 'purchase_id';
    $label = __('TXT_PURCHASE_ID');
    $input = $this->Form->select('purchase_imports.0.purchase_id', $data, [
        'label' => false,
        'class' => 'form-control selected-id',
        'empty' => ['' => __('TXT_SELECT_PURCHASE_ID')]
    ]);
    $err = '<span class="help-block help-tips errors" id="error-purchase_id-0"></span>';
    if ($type == TYPE_SAMPLE) {
        $btn = 'sale_id';
        $label = __('TXT_SALE_ID');
        $input = $this->Form->select('purchase_imports.0.sale_id', $data, [
            'label' => false,
            'class' => 'form-control selected-id',
            'empty' => ['' => __('TXT_SELECT_SALE_ID')]
        ]);
        $err = '<span class="help-block help-tips errors" id="error-sale_id-0"></span>';
    }
    $html = '<div class="form-group row-wid">';
        $html .= '<label class="control-label col-md-3 col-md-offset-2" for="">' . $label . $this->Comment->formAsterisk() . '</label>';
        $html .= '<div class="col-md-4">';
            $html .= $input;
            $html .= $err;
        $html .= '</div>';
        $html .= '<div class="col-md-1 field-no-padding-left">';
            $html .= '<button data-type="'.$btn.'" type="button" class="btn btn-sm btn-primary btn-add-more form-btn-add">';
                $html .= '<i class="fa fa-plus" aria-hidden="true"></i>';
            $html .= '</button>';
        $html .= '</div>';
    $html .= '</div>';
    echo $html;