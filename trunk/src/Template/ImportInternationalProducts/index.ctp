<?php
echo $this->Form->create(null, [
    'role' => 'form',
    'class' => 'form-inline form-search',
    'type' => 'get',
    'name' => 'form_search'
]);
$this->Form->templates([
    'inputContainer' => '{{content}}',
]);
echo $this->Form->hidden('displays', [
    'value' => empty($this->request->query('displays')) ? 10 : $this->request->query('displays')
]);
?>
<div class="form-group">
    <?php
    echo $this->Form->input('keyword', [
        'type' => 'text',
        'placeholder' => __d('import_international_product', 'TXT_ENTER_KEYWORD'),
        'class' => 'form-control',
        'label' => false,
        'value' => $this->request->query('keyword'),
        'autocomplete' => 'off',
    ]); ?>
</div>
<?php
$this->SwitchStatus->render();
echo $this->Form->end(); ?>
<div class="total-pagination" style="padding-top: 20px; padding-bottom: 20px;">
    <?php
    echo $this->element('display_info'); ?>
</div>
<div class="row search-section" style="margin-bottom: 20px;">
    <?php
    echo $this->element('display_number'); ?>
    <?php
    echo $this->ActionButtons->btnRegisterNew('ImportInternationalProducts', []) ?>
    <div class="clearfix"></div>
</div>
<div class="data-tb-list">
    <table class="table table-striped">
        <thead>
            <tr>
                <th>#</th>
                <th>&nbsp;</th>
                <th class="white-space">
                    <?php
                    echo $this->Paginator->sort('Imports.type', __d('import_international_product', 'TXT_TYPE')); ?>
                    <i class="fa fa-sort-desc" aria-hidden="true"></i>
                </th>
                <th class="white-space">
                    <?php
                    echo __d('import_international_product', 'TXT_OF#') ?>
                </th>
                <th class="white-space">
                    <?php
                    echo __d('import_international_product', 'TXT_SUPPLIER') ?>
                </th>
                <th class="white-space">
                    <?php
                    echo __d('import_international_product', 'TXT_TRACKING') ?>
                </th>
                <th class="white-space">
                    <?php
                    echo $this->Paginator->sort('Imports.tax_payment_date', __d('import_international_product', 'TXT_TAX_PAYMENT_DATE')); ?>
                    <i class="fa fa-sort-desc" aria-hidden="true"></i>
                </th>
                <th class="white-space">
                    <?php
                    echo $this->Paginator->sort('Imports.arrival_date', __d('import_international_product', 'TXT_EXPECTED_ARRIVAL_DATE')); ?>
                    <i class="fa fa-sort-desc" aria-hidden="true"></i>
                </th>
                <th class="white-space">
                    <?php
                    echo $this->Paginator->sort('Imports.receiving_date', __d('import_international_product', 'TXT_EXPECTED_RECEIVING_DATE')); ?>
                    <i class="fa fa-sort-desc" aria-hidden="true"></i>
                </th>
                <th class="white-space">
                    <?php
                    echo $this->Paginator->sort('Imports.custom_clearerance_date', __d('import_international_product', 'TXT_EXPECTED_CUSTOM_CLEARANCE_DATE')); ?>
                    <i class="fa fa-sort-desc" aria-hidden="true"></i>
                </th>
                <th colspan="4">&nbsp;</th>
            </tr>
        </thead>
        <tbody class="normal-cursor">
            <?php
            if ($imports):
                $numbering = $this->Paginator->counter('{{start}}');
                foreach ($imports as $import): //pr($import); ?>
            <tr data-id="<?php echo $import->id; ?>">
                <th scope="row">
                    <?php
                    echo $numbering; $numbering++; ?>
                </th>
                <td>
                    <?php
                    echo $this->ActionButtons->disabledText($import->is_suspend) ?>
                </td>
                <td>
                    <?php
                    echo $import->type; ?>
                </td>
                <td>
                    <?php
                    $number_ps = ''; //purchase or sale
                    if ($import->purchase_imports) {
                        foreach ($import->purchase_imports as $number) {
                            if ($number->purchase) {
                                $number_ps .= $this->Html->link($number->purchase->purchase_number, [
                                    'controller' => 'SaleWStocks',
                                    'action' => 'po',
                                    $number->purchase->id
                                ]) . '<br />';
                            } else if ($number->sale) {
                                if ($number->sale->type == TYPE_MP_SAMPLE) {
                                    $number_ps .= $this->Html->link($number->sale->sale_number, [
                                        'controller' => 'OrderToMpSample',
                                        'action' => 'po',
                                        $number->sale->id
                                    ]) . '<br />';
                                } else if ($number->sale->type == TYPE_S_SAMPLE) {
                                    $number_ps .= $this->Html->link($number->sale->sale_number, [
                                        'controller' => 'OrderToNpSample',
                                        'action' => 'po',
                                        $number->sale->id
                                    ]) . '<br />';
                                }
                            }
                        }
                    }
                    echo rtrim($number_ps, '<br />');
                    ?>
                </td>
                <td>
                    <?php
                    if ($import->purchase_imports) {
                        $seller = $import->purchase_imports[0]->purchase->seller;
                        if ($seller && $seller->manufacturer) {
                            echo $this->Comment->getFieldByLocal($seller->manufacturer, $locale);
                        }
                        if ($seller && $seller->supplier) {
                            echo $this->Comment->getFieldByLocal($seller->supplier, $locale);
                        }
                    }
                    ?>
                </td>
                <td>
                    <?php
                    echo h($import->tracking); ?>
                </td>
                <td>
                    <?php
                    echo date('Y-m-d', strtotime($import->tax_payment_date)) ?>
                </td>
                <td>
                    <?php
                    echo date('Y-m-d', strtotime($import->arrival_date)) ?>
                </td>
                <td>
                    <?php
                    echo date('Y-m-d', strtotime($import->receiving_date)) ?>
                </td>
                <td>
                    <?php
                    echo date('Y-m-d', strtotime($import->custom_clearerance_date)) ?>
                </td>
                <td width="1%">
                    <?php
                    $action = 'index';
                    $detail = count($import->import_details);
                    if ($detail > 1) {
                        $detail .= ' ' . __('Products');
                    } else {
                        $detail .= ' ' . __('Product');
                        if ($detail == 0) {
                            $action = 'create';
                        }
                    }
                    echo $this->Html->link($detail, [
                        'controller' => 'ImportDetails',
                        'action' => $action,
                        '?' => ['import_id' => $import->id, 'pch_number' => $import->purchase_number]
                    ],
                    [
                        'class' => 'btn btn-primary btn-width',
                        'role' => 'button'
                    ]);
                    ?>
                </td>
                <td width="1%" data-target="<?php echo ($import->is_suspend) ? 1 : 0; ?>">
                    <?php
                    if ($import->is_suspend == 1) {
                        echo $this->Form->button(BTN_ICON_UNDO, [
                            'class' => 'btn btn-recover btn-sm btn_suspend',
                            'data-name' => 'recover',
                            'escape' => false
                        ]);
                    } else {
                        echo $this->Html->link(BTN_ICON_EDIT, [
                            'controller' => 'import-international-products',
                            'action' => 'edit', $import->id,
                            '?' => ['pkg_id' => $this->request->query('pkg_id')]
                            ], [
                            'class' => 'btn btn-primary btn-sm',
                            'escape' => false
                        ]);
                    }
                    ?>
                </td>
                <td width="1%" data-target="<?php echo ($import->is_suspend) ? 1 : 0; ?>">
                    <?php
                    if ($import->is_suspend == 1) {
                        echo $this->Form->button(BTN_ICON_DELETE, [
                            'class' => 'btn btn-delete btn-sm',
                            'id' => 'btn_delete',
                            'escape' => false
                        ]);
                    } else {
                        echo $this->Form->button(BTN_ICON_STOP, [
                            'class' => 'btn btn-suspend btn-sm btn_suspend',
                            'data-name' => 'suspend',
                            'escape' => false
                        ]);
                    }
                    ?>
                </td>
            </tr>
            <?php endforeach; endif; ?>
        </tbody>
    </table>
</div>
<div class="row row-top-space">
    <?php
    echo $this->element('display_number'); ?>
    <?php
    echo $this->element('next_prev') ?>
    <div class="clearfix"></div>
</div>
<!--Modal delete-->
<?php
echo $this->element('Modal/delete'); ?>
<!--Modal Suspend-->
<?php
echo $this->element('Modal/suspend'); ?>
<script>
    (function(e) {
        //auto complete
        $('body').find('#keyword').typeahead({
            minLength: 0,
            autoSelect: true,
            source:  function (query, process) {
                return $.get('<?= $this->Url->build(['action' => 'getPurchaseNumberBySearch']) ?>', {
                        keyword: query
                    }, function (data) {
                        var optionData = [];
                        if (data.data !== null && data.data !== 'undefined') {
                            $.each(data.data, function(i, v) {
                                optionData.push(v.purchase_number);
                            });
                        }
                        return process(optionData);
                    }, 'json');
            }
        });

        $('body').on('click', '.typeahead li', function(e) {
            $('body').find('.form-search').submit();
        });

        //modal confirm suspend
        $('body').on('click', '.btn_suspend', function (e) {
            e.stopPropagation();
            $('.suspend-item-id').val($(this).attr('data-id'));
            $('.is-suspend-item').val($(this).attr('data-target'));
            var confirm = '';
            if (parseInt($(this).attr('data-target')) === 1) {
                $('#btn_suspend_yes').text('<?= __('BTN_SUSPEND') ?>');
                confirm = '<?= __('MSG_CONFIRM_SUSPEND') ?>';
            } else {
                $('#btn_suspend_yes').text('<?= __('BTN_RECOVER') ?>');
                confirm = '<?= __('MSG_CONFIRM_UNDO_SUSPEND') ?>';
            }
            $('.confirm-suspend-text').text(confirm);
            $('#modal_suspend').modal('show');
        });

        //modal change suspend status
        $('body .data-tb-list .table > tbody > tr').on('click', '.btn_suspend', function (e) {
            e.stopPropagation();
            var content = $('#modal_suspend');
            var status = $(this).attr('data-name');
            var txt = '';

            switch (status) {
                case 'suspend':
                    $('#btn_suspend_yes').text('<?= __('BTN_SUSPEND') ?>');
                    txt = '<?= __('MSG_CONFIRM_SUSPEND') ?>';
                    break;
                case 'delete':
                    txt = '<?= __('MSG_CONFIRM_DELETE') ?>';
                    $('#btn_suspend_yes').text('<?= __('BTN_DELETE') ?>');
                    break;
                 case 'recover':
                    $('#btn_suspend_yes').text('<?= __('BTN_RECOVER') ?>');
                    txt = '<?= __('MSG_CONFIRM_UNDO_SUSPEND') ?>';
                    break;
            };
            $(content).find('#btn_suspend_yes').data('id', $(this).closest('tr').data('id'));
            $(content).find('#btn_suspend_yes').data('target', $(this).closest('td').data('target'));
            $(content).find('.modal-body p').html(txt);
            $(content).modal('show');
        });
        $('body').on('click', '#btn_suspend_yes', function (e) {
            var params = {
                id: $(this).data('id'),
                is_suspend: $(this).data('target')
            };
            // console.log(params);return false;
            $.post('<?php echo $this->Url->build('/import-international-products/update-suspend/'); ?>', params, function (data) {
                if (data.message === 'success') {
                    location.reload();
                }
            }, 'json');
        });

        //modal delete confirm
        $('body').on('click', '#btn_delete', function (e) {
            e.stopPropagation();
            $('#btn_delete_yes').text('<?= __('BTN_DELETE') ?>');
            $('.delete-item').val($(this).attr('data-id'));
            $('#modal_delete').modal('show');
        });

        //modal delete item.
        $('body').on('click', '#btn_delete_yes', function() {
            var data = $('.form-delete').serialize();
            var url = '<?= $this->Url->build('/import-international-products/delete') ?>';
            itemDelete(url, data, '<?php echo __('TXT_SESSION_TIMEOUT') ?>');
        });

        pageOutOfRange(<?= $paging ?>, '<?= $this->request->query('page') ?>');
        var maxPage = parseInt($('#maxPage').val());
        updatePagination(maxPage);
    })();
</script>
