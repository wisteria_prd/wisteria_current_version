<?php
$breadcrumb = ['' => $breadcrumbs];
echo $this->Comment->breadCrumb($breadcrumb);
echo $this->Form->create(null, [
    'role' => 'form',
    'class' => 'form-inline form-search',
    'type' => 'get',
    'name' => 'form_search'
]);
$this->Form->templates([
    'inputContainer' => '{{content}}'
]);
echo $this->Form->hidden('displays', [
    'value' => empty($this->request->query('displays')) ? 10 : $this->request->query('displays')
]);
?>
<div class="form-group">
    <?php
    echo $this->Form->input('keyword', [
        'type' => 'text',
        'placeholder' => __('USER_SEARCH_KEYWORD'),
        'class' => 'form-control',
        'label' => false,
        'value' => $this->request->query('keyword')
    ]);
    ?>
</div>
<?php echo $this->Form->end(); ?>

<div class="total-pagination" style="padding-bottom: 20px;">
    <?php echo $this->element('display_info'); ?>
</div>
<div class="row search-section" style="margin-bottom: 20px;">
    <?php echo $this->element('display_number'); ?>
    <?= $this->ActionButtons->btnRegisterNew('ImportInternationalProducts', []) ?>
    <div class="clearfix"></div>
</div>
<div class="data-tb-list">
    <table class="table table-striped">
        <thead>
            <tr>
                <th>#</th>
                <th>&nbsp;</th>
                <th>
                    <?php echo $this->Paginator->sort('', __('TXT_PURCHASE_NUMBER')); ?>
                    <i class="fa fa-sort-desc" aria-hidden="true"></i>
                </th>
                <th class="white-space">
                    <?php echo $this->Paginator->sort('', __('TXT_TAX_PAY_DATE')); ?>
                    <i class="fa fa-sort-desc" aria-hidden="true"></i>
                </th>
                <th class="white-space">
                    <?php echo $this->Paginator->sort('', __('TXT_EXPECTED_ARRIVAL_DATE')); ?>
                    <i class="fa fa-sort-desc" aria-hidden="true"></i>
                </th>
                <th class="white-space">
                    <?php echo $this->Paginator->sort('', __('TXT_EXPECTED_RECEIVING_DATE')); ?>
                    <i class="fa fa-sort-desc" aria-hidden="true"></i>
                </th>
                <th class="white-space">
                    <?php echo $this->Paginator->sort('', __('TXT_EXPECTED_CUSTOM_CLEARANCE_DATE')); ?>
                    <i class="fa fa-sort-desc" aria-hidden="true"></i>
                </th>
                <th colspan="4">&nbsp;</th>
            </tr>
        </thead>
        <tbody>
            <?php
            if ($imports):
                $page = $this->request->params['paging']['PurchaseImports']['page'];
                $limit = $this->request->params['paging']['PurchaseImports']['perPage'];
                $num = ($page - 1) * $limit;
                foreach ($imports as $import):
                    $num++;
            ?>
            <tr>
                <th scope="row"><?= $num; ?></th>
                <td>
                    <?= $this->ActionButtons->disabledText($import->import->is_suspend) ?>
                </td>
                <td>
                    <?php
                    //echo $import->purchase->purchase_number;
                    ?>
                </td>
                <td>
                    <?php echo date('Y-m-d', strtotime($import->import->tax_payment_date)) ?>
                </td>
                <td>
                    <?php echo date('Y-m-d', strtotime($import->import->arrival_date)) ?>
                </td>
                <td>
                    <?php echo date('Y-m-d', strtotime($import->import->receiving_date)) ?>
                </td>
                <td>
                    <?php echo date('Y-m-d', strtotime($import->import->custom_clearerance_date)) ?>
                </td>
<!--                <td width="1%">-->
                    <?php
                    $disabled = false;
                    $no_link = '';
                    if (empty($import->import->import_details)) {
                        $disabled = true;
                        $no_link = 'onclick="return false;"';
                    }
                    $action = 'index';
                    $query_action = 'create';
                    $return = 0;
                    $return_id = '';
                    if ($import->import->import_returns) {
                        $query_action = 'edit';
                        $return = count($import->import->import_returns[0]->import_return_details);
                        $return_id = $import->import->import_returns[0]->id;
                    }

                    if ($return > 1) {
                        $return .= ' ' . __('Returns');
                    } else {
                        $return .= ' ' . __('Return');
                        $action = 'index';
                    }
//                    echo $this->Html->link($return, [
//                        'controller' => 'ImportReturns',
//                        'action' => $action,
//                        '?' => [
//                            'import_id' => $import->import->id,
//                            'return_id' => $return_id,
//                            'action' => $query_action
//                        ]
//                    ],
//                    [
//                        'class' => 'btn btn-primary btn-width',
//                        'role' => 'button',
//                        'disabled' => $disabled,
//                        $no_link
//                    ]);
                    ?>
<!--                </td>-->
                <td width="1%">
                    <?php
                    $action = 'index';
                    $detail = count($import->import->import_details);
                    if ($detail > 1) {
                        $detail .= ' ' . __('Products');
                    } else {
                        $detail .= ' ' . __('Product');
                        if ($detail == 0) {
                            $action = 'create';
                        }
                    }
                    echo $this->Html->link($detail, [
                        'controller' => 'ImportDetails',
                        'action' => $action,
                        '?' => ['import_id' => $import->import->id]
                    ],
                    [
                        'class' => 'btn btn-primary btn-width',
                        'role' => 'button'
                    ]);
                    ?>
                </td>
                <?php
                echo $this->ActionButtons->btnAction($import->import->id, $import->import->is_suspend, 'ImportInternationalProducts');
                ?>
            </tr>
            <?php endforeach; endif; ?>
        </tbody>
    </table>
</div>
<div class="row row-top-space">
    <?php echo $this->element('display_number'); ?>
    <?= $this->element('next_prev') ?>
    <div class="clearfix"></div>
</div>


<!--Modal delete-->
<?php echo $this->element('Modal/delete'); ?>

<!--Modal Suspend-->
<?php echo $this->element('Modal/suspend'); ?>

<script>
    (function(e) {
        //modal confirm suspend
        $('body').on('click', '.btn_suspend', function (e) {
            e.stopPropagation();
            $('.suspend-item-id').val($(this).attr('data-id'));
            $('.is-suspend-item').val($(this).attr('data-target'));
            var confirm = '';
            if (parseInt($(this).attr('data-target')) === 1) {
                $('#btn_suspend_yes').text('<?= __('BTN_SUSPEND') ?>');
                confirm = '<?= __('MSG_CONFIRM_SUSPEND') ?>';
            } else {
                $('#btn_suspend_yes').text('<?= __('BTN_RECOVER') ?>');
                confirm = '<?= __('MSG_CONFIRM_UNDO_SUSPEND') ?>';
            }
            $('.confirm-suspend-text').text(confirm);
            $('#modal_suspend').modal('show');
        });

        //modal change suspend status
        $('body').on('click', '#btn_suspend_yes', function (e) {
            var data = $('.form-suspend-item').serialize();
            var url = '<?php echo $this->Url->build('/import-international-products/update-suspend/'); ?>';
            var txtTimout = '<?php echo __('TXT_SESSION_TIMEOUT') ?>';
            statusSuspend(url, data, txtTimout);
        });

        //modal delete confirm
        $('body').on('click', '#btn_delete', function (e) {
            e.stopPropagation();
            $('#btn_delete_yes').text('<?= __('BTN_DELETE') ?>');
            $('.delete-item').val($(this).attr('data-id'));
            $('#modal_delete').modal('show');
        });

        //modal delete item.
        $('body').on('click', '#btn_delete_yes', function() {
            var data = $('.form-delete').serialize();
            var url = '<?= $this->Url->build('/import-international-products/delete') ?>';
            itemDelete(url, data, '<?php echo __('TXT_SESSION_TIMEOUT') ?>');
        });
    })();
</script>