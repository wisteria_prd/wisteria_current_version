
<div class="search-section" style="margin-bottom: 20px;">
    <?php
    echo $this->Form->create(null, [
        'role' => 'form',
        'class' => 'form-inline form-search',
        'type' => 'get',
        'name' => 'form_search'
    ]);
    $name = 'name' . $locale;
    $this->Form->templates([
        'inputContainer' => '{{content}}'
    ]);
    echo $this->Form->hidden('displays', [
        'value' => empty($this->request->query('displays')) ? 10 : $this->request->query('displays')
    ]);
    echo $this->Form->hidden('import_id', [
        'value' => $this->request->query('import_id'),
    ]);
    ?>
    <div class="form-group">
        <?php
        echo $this->Form->input('keyword', [
            'type' => 'text',
            'placeholder' => __d('import_detail', 'TXT_ENTER_KEYWORD'),
            'class' => 'form-control',
            'label' => false,
            'value' => $this->request->query('keyword')
        ]); ?>
    </div>
    <?php $this->SwitchStatus->render(); ?>
    <?php
    echo $this->Form->end(); ?>
</div>
<div class="total-pagination" style="padding-bottom: 20px;">
    <?php
    echo $this->element('display_info'); ?>
</div>
<div class="row search-section" style="margin-bottom: 20px;">
    <?php
    echo $this->element('display_number'); ?>
    <?php
    echo $this->ActionButtons->btnRegisterNew('ImportDetails', [
        'import_id' => $this->request->query('import_id'),
        'pch_number' => $this->request->query('pch_number')
    ]) ?>
    <div class="clearfix"></div>
</div>
<div class="data-tb-list">
    <table class="table table-striped">
        <thead>
            <tr>
                <th>#</th>
                <th>&nbsp;</th>
                <th class="white-space">
                    <?php
                    echo __d('import_detail', 'TXT_RECEIPT_DATE') ?>
                </th>
                <th class="white-space">
                    <?php
                    echo __d('import_detail', 'TXT_MANUFACTURER') ?>
                </th>
                <th class="white-space">
                    <?php
                    echo __d('import_detail', 'TXT_PRODUCTS') ?>
                </th>
                <th class="white-space">
                    <?php
                    echo $this->Paginator->sort('ImportDetails.lot_number', __d('import_detail', 'TXT_LOT#')); ?>
                    <i class="fa fa-sort-desc" aria-hidden="true"></i>
                </th>
                <th class="white-space">
                    <?php
                    echo $this->Paginator->sort('ImportDetails.expiry_date', __d('import_detail', 'TXT_EXPIRY_DATE')); ?>
                    <i class="fa fa-sort-desc" aria-hidden="true"></i>
                </th>
                <th class="white-space">
                    <?php
                    echo $this->Paginator->sort('ImportDetails.quantity', __d('import_detail', 'TXT_QUANTITY')); ?>
                    <i class="fa fa-sort-desc" aria-hidden="true"></i>
                </th>
                <th class="white-space">
                    <?php
                    echo $this->Paginator->sort('ImportDetails.is_foc', __d('import_detail', 'TXT_FOC')); ?>
                    <i class="fa fa-sort-desc" aria-hidden="true"></i>
                </th>
                <th colspan="2">&nbsp;</th>
            </tr>
        </thead>
        <tbody class="normal-cursor">
            <?php
            if ($imports):
                $numbering = $this->Paginator->counter('{{start}}');
                foreach ($imports as $import): ?>
                    <tr data-id="<?php echo $import->id; ?>">
                        <th scope="row">
                            <?php
                            echo $numbering; $numbering++; ?>
                        </th>
                        <td>
                             <?php
                             echo $this->ActionButtons->disabledText($import->is_suspend) ?>
                        </td>
                        <td>
                            <?php
                            echo date('Y-m-d', strtotime($import->import->receiving_date)); ?>
                        </td>
                        <td>
                            <?php
                            echo $this->Comment->getFieldByLocal($import->product_detail->product->product_brand->manufacturer, $locale); ?>
                        </td>
                        <td>
                            <?php
                            $product_brand = $this->Comment->getFieldByLocal($import->product_detail->product->product_brand, $locale);
                            $product = $this->Comment->getFieldByLocal($import->product_detail->product, $locale);
                            echo h($product_brand) . ' ' . h($product); ?>
                        </td>
                        <td>
                            <?php
                            echo h($import->lot_number) ?>
                        </td>
                        <td>
                            <?php
                            echo date('Y-m-d', strtotime($import->expiry_date)) ?>
                        </td>
                        <td>
                            <?php
                            echo $import->quantity; ?>
                        </td>
                        <td>
                            <?php
                            $txt = $import->is_foc ? __d('import_detail', 'TXT_YES') : __d('import_detail', 'TXT_NO');
                            echo $txt;
                            ?>
                        </td>
                        <td width="1%" data-target="<?php echo ($import->is_suspend) ? 1 : 0; ?>">
                            <?php
                            if ($import->is_suspend == 1) {
                                echo $this->Form->button(BTN_ICON_UNDO, [
                                    'class' => 'btn btn-recover btn-sm btn_suspend',
                                    'data-name' => 'recover',
                                    'escape' => false
                                ]);
                            } else {
                                echo $this->Html->link(BTN_ICON_EDIT, [
                                    'controller' => 'import-details',
                                    'action' => 'edit', $import->id,
                                    '?' => [
                                        'import_id' => $import->import_id,
                                        'pch_number' => $this->request->query('pch_number')
                                    ]
                                    ], [
                                    'class' => 'btn btn-primary btn-sm',
                                    'escape' => false
                                ]);
                            }
                            ?>
                        </td>
                        <td width="1%" data-target="<?php echo ($import->is_suspend) ? 1 : 0; ?>">
                            <?php
                            if ($import->is_suspend == 1) {
                                echo $this->Form->button(BTN_ICON_DELETE, [
                                    'class' => 'btn btn-delete btn-sm',
                                    'id' => 'btn_delete',
                                    'escape' => false
                                ]);
                            } else {
                                echo $this->Form->button(BTN_ICON_STOP, [
                                    'class' => 'btn btn-suspend btn-sm btn_suspend',
                                    'data-name' => 'suspend',
                                    'escape' => false
                                ]);
                            }
                            ?>
                        </td>
                    </tr>
                <?php endforeach;
            endif; ?>
        </tbody>
    </table>
</div>
<div class="row row-top-space">
    <?php echo $this->element('display_number'); ?>
    <?= $this->element('next_prev') ?>
    <div class="clearfix"></div>
</div>


<!--Modal delete-->
<?php echo $this->element('Modal/delete'); ?>

<!--Modal Suspend-->
<?php echo $this->element('Modal/suspend'); ?>

<script>
    (function(e) {
        //modal delete confirm
        $('body').on('click', '#btn_delete', function (e) {
            e.stopPropagation();
            $('#btn_delete_yes').text('<?= __('BTN_DELETE') ?>');
            $('.delete-item').val($(this).attr('data-id'));
            $('#modal_delete').modal('show');
        });

        //modal change suspend status
        $('body .data-tb-list .table > tbody > tr').on('click', '.btn_suspend', function (e) {
            e.stopPropagation();
            var content = $('#modal_suspend');
            var status = $(this).attr('data-name');
            var txt = '';

            switch (status) {
                case 'suspend':
                    $('#btn_suspend_yes').text('<?= __('BTN_SUSPEND') ?>');
                    txt = '<?= __('MSG_CONFIRM_SUSPEND') ?>';
                    break;
                case 'delete':
                    txt = '<?= __('MSG_CONFIRM_DELETE') ?>';
                    $('#btn_suspend_yes').text('<?= __('BTN_DELETE') ?>');
                    break;
                 case 'recover':
                    $('#btn_suspend_yes').text('<?= __('BTN_RECOVER') ?>');
                    txt = '<?= __('MSG_CONFIRM_UNDO_SUSPEND') ?>';
                    break;
            };
            $(content).find('#btn_suspend_yes').data('id', $(this).closest('tr').data('id'));
            $(content).find('#btn_suspend_yes').data('target', $(this).closest('td').data('target'));
            $(content).find('.modal-body p').html(txt);
            $(content).modal('show');
        });
        $('body').on('click', '#btn_suspend_yes', function (e) {
            var params = {
                id: $(this).data('id'),
                is_suspend: $(this).data('target')
            };
            // console.log(params);return false;
            $.post('<?php echo $this->Url->build('/import-details/update-suspend/'); ?>', params, function (data) {
                if (data.message === 'success') {
                    location.reload();
                }
            }, 'json');
        });

        //modal delete item.
        $('body').on('click', '#btn_delete_yes', function() {
            var data = $('.form-delete').serialize();
            var url = '<?= $this->Url->build('/import-details/delete') ?>';
            itemDelete(url, data, '<?php echo __('TXT_SESSION_TIMEOUT') ?>');
        });

        pageOutOfRange(<?= $paging ?>, '<?= $this->request->query('page') ?>');
        var maxPage = parseInt($('#maxPage').val());
        updatePagination(maxPage);
    })();
</script>
