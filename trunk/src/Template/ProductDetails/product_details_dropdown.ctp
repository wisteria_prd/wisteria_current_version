<?php
if ($products):
    $output = '<option value="">' . __('TXT_SELECT_PRODUCT_DETAIL') . '</option>';
    $name = 'name' . $en;
    foreach ($products as $product) {
        $detail = $product_name . ' ';
        $detail .= $this->Comment->sizeAndUnit($product, $en);
        $output .= '<option value="' . $product->id . '">' . $detail . '</option>';
    }
    echo $output;
endif;