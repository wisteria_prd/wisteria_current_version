
<div class="search-section" style="margin-bottom: 20px;">
    <?php
    echo $this->Form->create('ProductDetails', [
        'class' => 'form-inline',
        'type' => 'get',
        'name' => 'form_search',
    ]);
    echo $this->Form->input('keyword', [
        'name' => 'keyword',
        'placeholder' => __('USER_SEARCH_KEYWORD'),
        'class' => 'form-control',
        'label' => false,
        'value' => $this->request->query('keyword') ? $this->request->query('keyword') : '',
        'templates' => [
            'inputContainer' => '<div class="form-group">{{content}}</div>',
        ]
    ]);
    echo $this->Form->hidden('product_id', [
        'value' => $this->request->query('product_id') ? $this->request->query('product_id') : '',
    ]);
    $this->SwitchStatus->render();
    echo $this->Form->submit('submit', [
        'style' => 'display: none',
    ]);
    echo $this->Form->end();
    ?>
</div>
<div class="total-pagination" style="padding-bottom: 20px;">
    <?php echo $this->element('display_info'); ?>
</div>
<div class="row search-section" style="margin-bottom: 20px;">
    <?php echo $this->element('display_number'); ?>
    <div class="col-md-6 col-sm-6 text-center">
        <h2 class="title-page"><?php //echo $product->$name; ?></h2>
    </div>
    <?= $this->ActionButtons->btnRegisterNew('ProductDetails', ['product_id' => $this->request->query('product_id')]) ?>
    <div class="clearfix"></div>
</div>
<div class="data-tb-list">
    <?php if ($data): ?>
    <table class="table table-striped">
        <thead>
            <tr>
                <th>#</th>
                <th>&nbsp;</th>
                <th>
                    <?php
                    echo $this->Paginator->sort('Products.code', __d('product_detail', 'TXT_OLD_CODE')); ?>
                    <i class="fa fa-sort-desc" aria-hidden="true"></i>
                </th>
                <th>
                    <?php
                    echo $this->Paginator->sort('Manufacturers.name', __d('product_detail', 'TXT_MANUFACTURER')); ?>
                    <i class="fa fa-sort-desc" aria-hidden="true"></i>
                </th>
                <th>
                    <?php
                    echo $this->Paginator->sort('ProductDetails.type', __d('product_detail', 'TXT_PRODUCT_CATEGORY')); ?>
                    <i class="fa fa-sort-desc" aria-hidden="true"></i>
                </th>
                <th>
                    <?php
                    echo $this->Paginator->sort('Products.name', __d('product_detail', 'TXT_PRODUCT')); ?>
                    <i class="fa fa-sort-desc" aria-hidden="true"></i>
                </th>
                <th>
                    <?php
                    echo $this->Paginator->sort('details', __('PRODUCT_DETAILS_TXT_PRODUCT_DETAIL')); ?>
                    <i class="fa fa-sort-desc" aria-hidden="true"></i>
                </th>
                <?php if (false) : ?>
                <th>
                    <?php
                    echo $this->Paginator->sort('description', __('PRODUCT_DETAILS_TXT_DESCRIPTION')); ?>
                    <i class="fa fa-sort-desc" aria-hidden="true"></i>
                </th>
                <?php endif; ?>
                <th>
                    <?php
                    echo $this->Paginator->sort('purchase_flag', __('PRODUCT_DETAILS_TXT_PURCHASE_AVAILABILITY')); ?>
                    <i class="fa fa-sort-desc" aria-hidden="true"></i>
                </th>
                <th>
                    <?php
                    echo $this->Paginator->sort('sale_flag', __('PRODUCT_DETAILS_TXT_SALE_AVAILABILITY')); ?>
                    <i class="fa fa-sort-desc" aria-hidden="true"></i>
                </th>
                <th width="1%">&nbsp;</th>
                <th width="1%">&nbsp;</th>
            </tr>
        </thead>
        <tbody>
            <?php
            $numbering = $this->Paginator->counter('{{start}}');
            foreach ($data as $item):
            ?>
            <tr data-id="<?php echo h($item->id); ?>">
                <th scope="row">
                    <?php
                    echo $numbering; $numbering++; ?>
                </th>
                <td>
                    <?php
                    echo $this->ActionButtons->disabledText($item->is_suspend) ?>
                </td>
                <td>
                    <?php
                    echo h($item->product->code); ?>
                </td>
                <td>
                    <?php
                    echo $this->Comment->getFieldByLocal($item->product->product_brand->manufacturer, $locale); ?>
                </td>
                <td>
                    <?php
                    echo h($item->type); ?>
                </td>
                <td>
                    <?php
                    echo $this->Comment->getFieldByLocal($item->product->product_brand, $locale); ?>
                </td>
                <td>
                    <?php
                    echo $this->Comment->productAndDetails($item, $locale) ?>
                </td>
                <?php if (false) : ?>
                <td>
                    <?php
                    echo h($item->description) ?>
                </td>
                <?php endif; ?>
                <td><?php echo $item->is_purchase_flag; ?></td>
                <td><?php echo $item->is_sell_flag; ?></td>
                <td data-target="<?php echo ($item->is_suspend) ? 1 : 0; ?>">
                    <?php
                    if ($item->is_suspend == 1) {
                        echo $this->Form->button(BTN_ICON_UNDO, [
                            'class' => 'btn btn-recover btn-sm btn_suspend',
                            'data-name' => 'recover',
                            'escape' => false
                        ]);
                    } else {
                        echo $this->Html->link(BTN_ICON_EDIT, [
                            'controller' => 'product-details',
                            'action' => 'edit', $item->id,
                            '?' => ['product_id' => $this->request->query('product_id')]
                            ], [
                            'class' => 'btn btn-primary btn-sm',
                            'escape' => false
                        ]);
                    }
                    ?>
                </td>
                <td data-target="<?php echo ($item->is_suspend) ? 1 : 0; ?>">
                    <?php
                    if ($item->is_suspend == 1) {
                        if (!empty($item->product_brands) || !empty($item->person_in_charges)) {
                            echo $this->Form->button(BTN_ICON_DELETE, [
                                'class' => 'btn btn-delete btn-sm',
                                'id' => 'btn_delete',
                                'disabled' => 'disabled',
                                'escape' => false
                            ]);
                        } else {
                            echo $this->Form->button(BTN_ICON_DELETE, [
                                'class' => 'btn btn-delete btn-sm',
                                'id' => 'btn_delete',
                                'escape' => false
                            ]);
                        }
                    } else {
                        echo $this->Form->button(BTN_ICON_STOP, [
                            'class' => 'btn btn-suspend btn-sm btn_suspend',
                            'data-name' => 'suspend',
                            'escape' => false
                        ]);
                    }
                    ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <?php endif; ?>
</div>
<div class="row row-top-space" style="padding-bottom: 20px;">
    <?php echo $this->element('display_number'); ?>
    <?= $this->element('next_prev') ?>
    <div class="clearfix"></div>
</div>

<!--Modal Suspend-->
<?php echo $this->element('Modal/suspend'); ?>
<!--Modal delete-->
<?php echo $this->element('Modal/delete'); ?>
<!--Modal Detail-->
<?php echo $this->element('Modal/view_detail'); ?>

<script>
    (function(e) {
        var optionData = [];
        get_product_detail_list();
        // typehead_initialize('#keyword', optionData);

        $('body').on('click', '.btn_suspend', function (e) {
            e.stopPropagation();
            var content = $('#modal_suspend');
            var txt = '';
            if ($(this).data('name') === 'recover') {
                $('#btn_suspend_yes').text('<?= __('BTN_RECOVER') ?>');
                txt = '<?= __('MSG_CONFIRM_UNDO_SUSPEND') ?>';
            } else {
                $('#btn_suspend_yes').text('<?= __('BTN_SUSPEND') ?>');
                txt = '<?= __('MSG_CONFIRM_SUSPEND') ?>';
            }

            $(content).find('#btn_suspend_yes').data('id', $(this).closest('tr').data('id'));
            $(content).find('#btn_suspend_yes').data('target', $(this).closest('td').data('target'));
            $(content).find('.modal-body p').html(txt);
            $(content).modal('show');
        });

        $('body').on('click', '#btn_suspend_yes', function (e) {
            var params = {
                id: $(this).data('id'),
                is_suspend: $(this).data('target')
            };
            $.post('<?php echo $this->Url->build('/product-details/update-suspend/'); ?>', params, function (data) {
                if (data.message === 'success') {
                    location.reload();
                }
            }, 'json');
        });

        $('body').on('click', '#btn_delete', function (e) {
            e.stopPropagation();
            var content = $('#modal_delete');
            $(content).find('#btn_delete_yes').attr('data-id', $(this).closest('tr').data('id'));
            $(content).modal('show');
        });

        $('body').on('click', '.table>tbody>tr>td a', function(e) {
            e.stopPropagation();
        });

        $('body').on('click', '.data-tb-list .table>tbody>tr', function (e) {
            $.LoadingOverlay('show');
            var id = $(this).closest('tr').data('id');
            $.get('<?php echo $this->Url->build('/product-details/view/'); ?>' + id, function (data) {
                $.LoadingOverlay('hide');
                $('#modal_detail').find('.modal-body').html(data);
                $('#modal_detail').modal('show');
            }, 'html');
        });

        $('body').on('click', '#btn_delete_yes', function (e) {
            $.post('<?php echo $this->Url->build('/product-details/delete/'); ?>', {id: $(this).data('id')}, function (data) {
                if (data.message === 'success') {
                    location.reload();
                }
            }, 'json');
        });

        $('body').on('click', '.tt-selectable', function (e) {
            $('form[name="form_search"]').submit();
        });

        function get_product_detail_list()
        {
            $.get('<?php echo $this->Url->build('/product-details/get-list'); ?>', function(data) {
                if (data != null && data != undefined) {
                    $.each(data.data, function (i, v) {
                        if (v.description) {
                            optionData.push(v.description);
                        }
                        optionData.push(v.logistic_temperature.name_en);
                        optionData.push(v.logistic_temperature.name);
                        if (v.single_unit) {
                            optionData.push(v.single_unit.name_en);
                            optionData.push(v.single_unit.name);
                        }
                        if (v.tb_pack_sizes) {
                            optionData.push(v.tb_pack_sizes.name_en);
                            optionData.push(v.tb_pack_sizes.name);
                        }
                    });
                }
            }, 'json');
        }

        pageOutOfRange(<?= $paging ?>, '<?= $this->request->query('page') ?>');
        var maxPage = parseInt($('#maxPage').val());
        updatePagination(maxPage);
    })();
</script>
