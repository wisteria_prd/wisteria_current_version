
<table class="table table-striped tb-prbr-view" id="tb-detail">
    <tbody>
        <tr>
            <td>
                <?php
                echo __d('product_detail', 'TXT_MANUFACTURER') ?> :
            </td>
            <td colspan="2">
                <?php
                echo $this->Comment->getFieldByLocal($data->product->product_brand->manufacturer, $locale); ?>
            </td>
        </tr>
        <tr>
            <td>
                <?php
                echo __d('product_detail', 'TXT_PRODUCT_CATEGORY') ?> :
            </td>
            <td colspan="2">
                <?php
                echo $data->type; ?>
            </td>
        </tr>
        <tr>
            <td>
                <?php
                echo __d('product_detail', 'TXT_PRODUCT') ?> :
            </td>
            <td colspan="2">
                <?php
                $product = $this->Comment->getFieldByLocal($data->product->product_brand, $locale);
                $product .= ' ' . $this->Comment->getFieldByLocal($data->product, $locale);
                echo h($product);
                ?>
            </td>
        </tr>
        <tr>
            <td>
                <?php
                echo __d('product_detail', 'TXT_MODAL_PACKAGE_PACK_SIZE') ?> :
            </td>
            <td colspan="2">
                <?php
                $p_size = $data->pack_size . ' ';
                if ($data->tb_pack_sizes) {
                    $p_size .= empty($data->tb_pack_sizes->unit_name_en) ? $data->tb_pack_sizes->unit_name : $data->tb_pack_sizes->unit_name_en;
                }
                echo h($p_size);
                ?>
            </td>
        </tr>
        <tr>
            <td>
                <?php
                echo __d('product_detail', 'TXT_SINGLE_UNIT_SIZE') ?> :
            </td>
            <td colspan="2">
                <?php
                $s_size = $data->single_unit_size . ' ';
                if ($data->single_unit) {
                    $s_size .= $this->Comment->getFieldByLocal($data->single_unit, $locale);
                }
                echo h($s_size);
                ?>
            </td>
        </tr>
        <tr>
            <td>
                <?php
                echo __d('product_detail', 'TXT_LOGISTIC_TEMPERATURE') ?> :
            </td>
            <td colspan="2">
                <?php
                if ($data->logistic_temperature) {
                    echo $data->logistic_temperature->temperature_range . ' ' . $this->Comment->getFieldByLocal($data->logistic_temperature, $locale);
                }
                ?>
            </td>
        </tr>
        <tr>
            <td>
                <?php
                echo __d('product_detail', 'MODAL_PACKAGE_PACKING') ?> :
            </td>
            <td colspan="2">
                <?php
                if ($data->packing) {
                    echo $this->Comment->getFieldByLocal($data->packing, $locale);
                }
                ?>
            </td>
        </tr>
        <tr>
            <td>
                <?php
                echo __d('product_detail', 'TXT_DESCRIPTION') ?> :
            </td>
            <td colspan="2">
                <textarea readonly="readonly" style="background: none;"><?php echo $data->description; ?></textarea>
            </td>
        </tr>
        <tr>
            <td>
                <?php
                echo __d('product_detail', 'TXT_PATIENT_AGREEMENT') ?> :
            </td>
            <td colspan="2">
                <?php
                echo $data->is_patient_agreement ? __d('product_detail', 'TXT_REQUIRED') : ''; ?>
            </td>
        </tr>
        <tr>
            <td>
                <?php
                echo __d('product_detail', 'TXT_IMPORT_CERTIFICATE') ?> :
            </td>
            <td colspan="2">
                <?php
                echo $data->import_certificate ? __d('product_detail', 'TXT_REQUIRED') : ''; ?>
            </td>
        </tr>
        <tr>
            <td>
                <?php
                echo __d('product_detail', 'TXT_PURCHASE_AVAILABILITY') ?> :
            </td>
            <td colspan="2">
                <?php
                echo $data->purchase_flag ? __d('product_detail', 'TXT_YES') : __d('product_detail', 'TXT_NO'); ?>
            </td>
        </tr>
        <tr>
            <td>
                <?php
                echo __d('product_detail', 'TXT_SALE_AVAILABILITY') ?> :
            </td>
            <td colspan="2">
                <?php
                echo $data->sale_flag ? __d('product_detail', 'TXT_YES') : __d('product_detail', 'TXT_NO'); ?>
            </td>
        </tr>
        <tr>
            <td>
                <?php
                echo __d('product_detail', 'TXT_REMARKS') ?> :
            </td>
            <td colspan="2">
                <textarea readonly="readonly" style="background: none;"><?php echo $data->remark; ?></textarea>
            </td>
        </tr>
        <tr>
            <td>
                <?php
                echo __d('product_detail', 'TXT_MODIFIED') ?> :
            </td>
            <td colspan="2">
                <?php
                echo date('Y-m-d', strtotime($data->modified)); ?>
            </td>
        </tr>
        <tr>
            <td>
                <?php
                echo __d('product_detail', 'TXT_REGISTERED') ?> :
            </td>
            <td colspan="2">
                <?php
                echo date('Y-m-d', strtotime($data->created)); ?>
            </td>
        </tr>
    </tbody>
</table>
