
<div class="row">
    <div class="col-md-12">
        <?php
        echo $this->Form->create($purchase_detail ? $purchase_detail : null, [
            'class' => 'form-po-settings form-horizontal',
            'autocomplete' => 'off',
            'onsubmit' => 'return false;',
            'name' => 'purchase_detail_form',
        ]);
        $this->Form->templates(['inputContainer' => '{{content}}']);

        $disabled = false;
        $local = $this->request->session()->read('tb_field');

        if ($purchase_detail) {
            if (empty($purchase_detail->parent_id) && ($childs > 0)) {
                $disabled = true;
            }
            echo $this->Form->hidden('id', ['value' => $purchase_detail->id]);
        }
        echo $this->Form->hidden('purchase_id', ['value' => $purchase_id]);
        echo $this->Form->hidden('brand_name');
        echo $this->Form->hidden('product_name');
        echo $this->Form->hidden('single_unit_value');
        echo $this->Form->hidden('single_unit_name');
        echo $this->Form->hidden('pack_size_value');
        echo $this->Form->hidden('pack_size_name');
        echo $this->Form->hidden('packing_id');
        echo $this->Form->hidden('full_name');
        echo $this->Form->hidden('is_discount');
        echo $this->Form->hidden('description');
        ?>
<!--        <div class="form-group">
            <div class="col-md-3 col-md-offset-1"></div>
            <div class="col-md-5 custom-select">-->
                <?php
//                echo $this->Form->input('parent_id', [
//                    'type' => 'select',
//                    'class' => 'form-control',
//                    'label' => false,
//                    'options' => $parents,
//                    'disabled' => $disabled,
//                ]);
                ?>
<!--            </div>
        </div>-->
        <div class="form-group custom-select">
            <label for="" class="col-md-3 col-md-offset-1"><?= __('TXT_PRODUCT_NAME') ?></label>
            <div class="col-md-5 custom-select">
                <select name="product_detail_id" class="form-control" id="product-detail-id">
                    <?= $this->Comment->productDetailsListWithBrand($local, $products); ?>
                </select>
            </div>
            <div class="col-md-2">
                <button type="button" class="btn btn-primary btn-av-search"><?= __('TXT_ADVANCED_FIND_PRODUCTS') ?></button>
            </div>
        </div>
        <div class="form-group">
            <label for="" class="col-md-3 col-md-offset-1"><?= __('TXT_QUANTITY') ?></label>
            <div class="col-md-5">
                <?php
                echo $this->Form->input('quantity', [
                    'type' => 'number',
                    'label' => false,
                    'class' => 'form-control quantity',
                    'placeholder' => __('TXT_ENTER_QUANTITY')
                ]);
                ?>
            </div>
        </div>
        <div class="form-group">
            <label for="" class="col-md-3 col-md-offset-1"><?= __('TXT_UNIT_PRICE') ?></label>
            <div class="col-md-5">
                <?php
                echo $this->Form->input('unit_price', [
                    'type' => 'number',
                    'label' => false,
                    'readonly' => true,
                    'class' => 'form-control unit-price',
                    'placeholder' => __('TXT_ENTER_UNIT_PRICE')
                ]);
                ?>
            </div>
            <div class="col-md-2">
                <?php
                echo $this->Form->button(__('TXT_VIEW_DISCOUNT'), [
                    'type' => 'button',
                    'class' => 'btn btn-primary popup-discount',
                ]);
                ?>
            </div>
        </div>
        <div class="form-group">
            <div class="col-md-5 col-md-offset-4">
                <div class="checkbox">
                    <label>
                        <?= $this->Form->input('manual', [
                            'type' => 'checkbox',
                            'class' => 'chk-register-foc',
                            'checked' => false,
                            'label' => false]) . __('TXT_MANUAL_REGISTER_UNIT_PRICE') ?>
                    </label>
                </div>
            </div>
        </div>
        <?= $this->Form->end() ?>
    </div>
</div>

<script>
    (function(e) {
        var data = JSON.parse('<?= json_encode($purchase_detail)?>');

        if ($.isEmptyObject(data) === false) {
            $('body').find('#product-detail-id').val(data.product_detail_id);
        }

        $('body').on('click', '.chk-register-foc', function() {
            if ($(this).is(':checked')) {
                if ($('body').find('#parent-id').val() === '') {
                    $('.unit-price').val('');
                }
                $('.unit-price').removeAttr('readonly');
            } else {
                $('.unit-price').val($(this).attr('data-value')).attr('readonly', true);
            }
        });

        $('body').on('change', '#product-detail-id', function(e) {
            var option = $(this).find('option:selected');
            var modal = $('body').find('#modalAddItem');
            var id = $(this).val();

            if (id !== '') {
                $(modal).find('#quantity').removeAttr('disabled', true);
                var params = {
                    id: $(this).val(),
                    quantity: $(modal).find('#quantity').val(),
                    customer_id: data.customer_id
                };
                if (params.quantity !== '') {
                    $.LoadingOverlay('show');
                    getPricing(params);
                }
            } else {
                $(modal).find('#quantity').val('').attr('disabled', true);
            }

            $(modal).find('input[name="brand_name"]').val(option.attr('data-bn'));
            $(modal).find('input[name="product_name"]').val(option.attr('data-pn'));
            $(modal).find('input[name="single_unit_value"]').val(option.attr('data-psuv'));
            $(modal).find('input[name="single_unit_name"]').val(option.attr('data-psun'));
            $(modal).find('input[name="pack_size_value"]').val(option.attr('data-psv'));
            $(modal).find('input[name="pack_size_name"]').val(option.attr('data-psn'));
            $(modal).find('input[name="packing_id"]').val(option.attr('data-pk'));
            $(modal).find('input[name="full_name"]').val(option.attr('data-full-name'));
            $(modal).find('input[name="description"]').val(option.attr('data-dsc'));
        });

        $('body').on('change', '#modalAddItem #quantity', function(e) {
            $.LoadingOverlay('show');
            var params = {
                id: $('#modalAddItem').find('#product-detail-id').val(),
                quantity: $(this).val(),
                customer_id: data.customer_id
            };
            getPricing(params);
        });

        $('body').on('click', '.btn-add-detail', function(e) {
            $.LoadingOverlay('show');
            var form = $('body').find('form[name="purchase_detail_form"]');
            var url = '<?= $this->Url->build(['action' => 'create']) ?>';

            ajax_request_post(url, $(form).serialize(), function(data) {
                $(form).find('.error-message').remove();

                if (data.message === '<?= MSG_ERROR ?>') {
                    displayError(form, data.data);
                } else {
                    location.reload();
                }
            }, '<?= __('TXT_SESSION_TIMEOUT') ?>');
        });

        $('#modalAddItem').on('click', '.btn-av-search', function(e) {
            $.LoadingOverlay('show');
            var url = '<?= $this->Url->build('/order-to-s/advance-search/') ?>';

            ajax_request_get(url, {seller_id: $('#seller-id').attr('data-id')}, function(data) {
                var content = $('body').find('#advance-search');

                $(content).find('.modal-body').html(data);
                $(content).modal('show');

                // Initialize select2
                $(content).find('#manufacturer-id, #brand-id, #product-id').select2({width: '100%'});
            }, 'html', '<?= __('TXT_SESSION_TIMEOUT') ?>');
        });

        $('#modalAddItem').on('click', '.popup-discount', function() {
            $.LoadingOverlay('show');
            var url = '<?= $this->Url->build('/pricing/get-between-quantity/') ?>';
            var params = {
                'type': 'pricing',
                'externalId': $(this).attr('data-id'),
                'quantity': $('body').find('input[name="quantity"]').val()
            };

            ajax_request_get(url, params, function(data) {
                $('body').find('#modalDiscountCondition .table-view-discount tbody').html(data);
                $('body').find('#modalDiscountCondition').modal('show');
                $('body').find('.foc-preview').popover();
            }, 'html', '<?= __('TXT_SESSION_TIMEOUT') ?>');
        });

        function getPricing(params)
        {
            ajax_request_get('<?= $this->Url->build('/sale-details/get-pricing/') ?>', params, function(data) {
                if (data !== null && data !== 'undefined') {
                    var parent = $('body').find('#parent-id').val();
                    var unitPrice = data.data.price;

                    if (parent === '') {
                        $('body').find('.chk-register-foc').attr('data-value', data.data.price);
                        if ($('body').find('.chk-register-foc').is(':checked')) {
                            unitPrice = null;
                        }
                    } else {
                        unitPrice = 0;
                    }
                    $('body').find('#unit-price').val(unitPrice);

                    if (data.data.from === 'Pricing') {
                        $('body').find('input[name="is_discount"]').val('1');
                        $('body').find('.popup-discount').attr('data-id', data.data.id);
                    }
                }
            }, 'json', '<?= __('TXT_SESSION_TIMEOUT') ?>');
        }
    })();
</script>
