
<div class="search-section" style="margin-bottom: 20px;">
    <?php
    echo $this->Form->create(null, [
        'role' => 'form',
        'class' => 'form-inline',
        'type' => 'get',
        'name' => 'form_search'
    ]);
    echo $this->Form->hidden('displays', ['value' => $display]);
    ?>
    <div class="form-group">
        <?php
        echo $this->Form->text('keyword', [
            'placeholder' => __('USER_SEARCH_KEYWORD'),
            'class' => 'form-control',
            'autocomplete' => 'off',
            'default' => $this->request->query('keyword'),
        ]);
        ?>
    </div>
    <?php $this->SwitchStatus->render(); ?>
    <?php echo $this->Form->end(); ?>
</div>
<div class="total-pagination" style="padding-bottom: 20px;">
    <?php echo $this->element('display_info'); ?>
</div>
<div class="row search-section" style="margin-bottom: 20px;">
    <?php
    echo $this->element('display_number');
    echo $this->ActionButtons->btnRegisterNew('Manufacturers', []);
    ?>
    <div class="clearfix"></div>
</div>
<div class="data-tb-list">
    <?php if ($manufacturers): ?>
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>#</th>
                    <th style="white-space: nowrap;">&nbsp;</th>
                    <th>
                        <?php
                        echo $this->Paginator->sort('Manufacturers.code', __d('manufacturer', 'TXT_COLUMN_OLD_CODE')); ?>
                        <span><i class="fa fa-sort-desc" aria-hidden="true"></i></span>
                    </th>
                    <th width="25%">
                        <?php
                        echo $this->Paginator->sort($this->Comment->getFieldName(), __d('manufacturer', 'TXT_COLUMN_MANUFACTURER')); ?>
                        <span><i class="fa fa-sort-desc" aria-hidden="true"></i></span>
                    </th>
                    <th>
                        <?php
                        $country = 'name'. $locale;
                        echo $this->Paginator->sort('Countries.' . $country, __d('manufacturer', 'TXT_COLUMN_COUNTRY')); ?>
                        <span><i class="fa fa-sort-desc" aria-hidden="true"></i></span>
                    </th>
                    <th>
                        <?php
                        echo __d('manufacturer', 'TXT_COLUMN_URL'); ?>
                    </th>
                    <th>
                        <?php
                        echo __d('manufacturer', 'TXT_COLUMN_SHOP_URL'); ?>
                    </th>
                    <th>
                        <?php
                        echo __d('manufacturer', 'TXT_COLUMN_PERSON_IN_CHARGE'); ?>
                    </th>
                    <th>&nbsp;</th>
                    <th width="1%">&nbsp;</th>
                    <th width="1%">&nbsp;</th>
                    <th width="1%">&nbsp;</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $numbering = $this->Paginator->counter('{{start}}');
                foreach ($manufacturers as $item):
                    ?>
                    <tr data-id="<?php echo h($item->id); ?>">
                        <th scope="row" class="td_evt">
                            <?php
                            echo $numbering;
                            $numbering++; ?>
                        </th>
                        <td style="white-space: nowrap;">
                            <?php
                            echo $this->ActionButtons->disabledText($item->is_suspend); ?>
                        </td>
                        <td>
                            <?php
                            echo h($item->code); ?>
                        </td>
                        <td>
                            <?php
                            echo $this->Comment->nameEnOrJp($item->name, $item->name_en, $locale); ?>
                        </td>
                        <td>
                            <?php
                            if ($item->country) {
                                $name = 'name' . $locale;
                                echo h($item->country->$name);
                            } else {
                                echo '&nbsp;';
                            }
                            ?>
                        </td>
                        <td>
                            <?php
                            if ($item->url) {
                                echo $this->Html->link(__d('manufacturer', 'TXT_COLUMN_WEBSITE'), $item->url, [
                                    'class' => 'btn btn-primary btn-sm btn-width',
                                    'target' => '_blank',
                                ]);
                            } else {
                                echo $this->Form->button(__d('manufacturer', 'TXT_COLUMN_WEBSITE'), [
                                    'class' => 'btn btn-primary btn-sm btn-width btn-disabled',
                                    'value' => 'Website',
                                    'disabled' => 'disabled',
                                ]);
                            }
                            ?>
                        </td>
                        <td>
                            <?php
                            if ($item->online_shop_url) {
                                echo $this->Html->link(__d('manufacturer', 'TXT_COLUMN_ONLINE_SHOP'), $item->online_shop_url, [
                                    'class' => 'btn btn-info btn-sm btn-width',
                                    'target' => '_blank',
                                ]);
                            } else {
                                echo $this->Form->button(__d('manufacturer', 'TXT_COLUMN_ONLINE_SHOP'), [
                                    'class' => 'btn btn-info btn-sm btn-width btn-disabled',
                                    'value' => 'online_shop_url',
                                    'disabled' => 'disabled',
                                ]);
                            }
                            ?>
                        </td>
                        <td>
                            <?php
                            $total = $item->person_in_charges ? $item->person_in_charges[0]->total : 0;
                            echo $this->Comment->btnPersonInCharge($item->id, $total, TYPE_MANUFACTURER);
                            ?>
                        </td>
                        <td class="td-btn-wrap">
                            <?php
                            if (isset($item->product_brands[0]['products'])) {
                                echo $this->Html->link(__d('manufacturer', 'TXT_MANUFACTURER_PRODUCTS'), [
                                    'controller' => 'products',
                                    'action' => 'index',
                                    '?' => [
                                        'filter_manufacturer' => $item->id,
                                        'filter_brand' => 0,
                                        'des' => TYPE_MANUFACTURER,
                                    ]
                                ], [
                                    'class' => 'btn btn-primary btn-sm btn-width',
                                ]);
                            } else {
                                echo $this->Form->button(__d('manufacturer', 'TXT_MANUFACTURER_PRODUCTS'), [
                                    'class' => 'btn btn-primary btn-sm btn-width',
                                    'disabled' => 'disabled',
                                ]);
                            }
                            ?>
                        </td>
                        <td>
                            <?php
                            echo $this->Form->button('<i class="fa fa-comments-o" aria-hidden="true"></i>', [
                                'class' => 'btn btn-primary btn-sm btn_comment',
                                'data-id' => $item->id,
                                'escape' => false,
                            ]);
                            ?>
                        </td>
                        <td data-target="<?php echo ($item->is_suspend) ? 0 : 1; ?>">
                            <?php
                            if ($item->is_suspend == 1) {
                                echo $this->Form->button(BTN_ICON_UNDO, [
                                    'class' => 'btn btn-recover btn-sm btn-restore',
                                    'escape' => false
                                ]);
                            } else {
                                echo $this->Html->link(BTN_ICON_EDIT, [
                                    'controller' => 'manufacturers',
                                    'action' => 'edit', $item->id
                                    ], [
                                    'class' => 'btn btn-primary btn-sm',
                                    'escape' => false
                                ]);
                            }
                            ?>
                        </td>
                        <td data-target="<?php echo ($item->is_suspend) ? 0 : 1; ?>">
                            <?php
                            if ($item->is_suspend == 1) {
                                if (!empty($item->product_brands) || !empty($item->count_person)) {
                                    echo $this->Form->button(BTN_ICON_DELETE, [
                                        'class' => 'btn btn-delete btn-sm',
                                        'disabled' => 'disabled',
                                        'escape' => false
                                    ]);
                                } else {
                                    echo $this->Form->button(BTN_ICON_DELETE, [
                                        'class' => 'btn btn-delete btn-sm',
                                        'escape' => false
                                    ]);
                                }
                            } else {
                                echo $this->Form->button(BTN_ICON_STOP, [
                                    'class' => 'btn btn-suspend btn-sm btn-restore',
                                    'escape' => false
                                ]);
                            }
                            ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    <?php endif; ?>
</div>
<div class="row row-top-space" style="padding-bottom: 20px;">
    <?php
    echo $this->element('display_number');
    echo $this->element('next_prev');
    ?>
    <div class="clearfix"></div>
</div>
<?php
echo $this->Html->script([
    'manufacturers/index.js',
], ['blog' => false]);
