
<section class="modal-wrapper">
    <div class="modal fade modal-view" data-backdrop="static" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <table class="table table-striped" id="tb-detail">
                        <tbody>
                            <tr>
                                <td class="white-space">
                                    <?php
                                    echo __d('manufacturer', 'TXT_COLUMN_OLD_CODE'); ?> :
                                </td>
                                <td colspan="2" class="white-space">
                                    <?php
                                    echo h($manufacturer->code); ?>
                                </td>
                            </tr>
                            <tr>
                                <td class="white-space">
                                    <?php
                                    echo __d('manufacturer', 'TXT_COLUMN_MANUFACTURER'); ?> :
                                </td>
                                <td class="white-space">
                                    <?php
                                    echo $manufacturer->name_en; ?>
                                </td>
                                <td class="white-space">
                                    <?php
                                    echo $manufacturer->name; ?>
                                </td>
                            </tr>
                            <tr>
                                <td class="white-space">
                                    <?php
                                    echo __d('manufacturer', 'TXT_NICKNAME'); ?> :
                                </td>
                                <td class="white-space">
                                    <?php
                                    echo $manufacturer->nickname_en; ?>
                                </td>
                                <td class="white-space">
                                    <?php
                                    echo $manufacturer->nickname; ?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <?php
                                    echo __d('manufacturer', 'TXT_ADDRESS'); ?> :
                                </td>
                                <td style="width: 200px;">
                                    <?php
                                    echo $this->Address->format($manufacturer, LOCAL_EN); ?>
                                </td>
                                <td>
                                    <?php
                                    echo $this->Address->format($manufacturer); ?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <?php
                                    echo __d('manufacturer', 'TXT_TEL'); ?> :
                                </td>
                                <td>
                                    <?php
                                    if ($manufacturer->info_details) {
                                        $element = '<ul class="email-wrap">';
                                        foreach ($manufacturer->info_details as $item) {
                                            $element .= '<li>' . $item->tel . '</li>';
                                        }
                                        echo $element . '</ul>';
                                    }
                                    ?>
                                </td>
                                <td>
                                    <?php
                                    if ($manufacturer->info_details) {
                                        $element = '<ul class="email-wrap">';
                                        foreach ($manufacturer->info_details as $item) {
                                            $element .= '<li>' . $item->tel_extension . '</li>';
                                        }
                                        echo $element . '</ul>';
                                    }
                                    ?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <?php
                                    echo __d('manufacturer', 'TXT_FAX'); ?> :
                                </td>
                                <td colspan="2">
                                    <?php
                                    echo $manufacturer->fax; ?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <?php
                                    echo __d('manufacturer', 'TXT_EMAIL'); ?> :
                                </td>
                                <td colspan="2">
                                    <?php
                                    if ($manufacturer->info_details) {
                                        $element = '<ul class="email-wrap">';
                                        foreach ($manufacturer->info_details as $item) {
                                            $element .= '<li><a href="mailto:' . $item->email . '" target="_blank">' .
                                                    $item->email . '</a></li>';
                                        }
                                        echo $element . '</ul>';
                                    }
                                    ?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <?php
                                    echo __d('manufacturer', 'TXT_COLUMN_URL'); ?> :
                                </td>
                                <td colspan="2">
                                    <?php
                                    echo $manufacturer->url; ?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <?php
                                    echo __d('manufacturer', 'TXT_COLUMN_SHOP_URL'); ?> :
                                </td>
                                <td colspan="2">
                                    <?php
                                    echo $manufacturer->online_shop_url; ?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <?php
                                    echo __d('manufacturer', 'TXT_COLUMN_PERSON_IN_CHARGE'); ?> :
                                </td>
                                <td colspan="2">
                                    <?php
                                    $total = 0;
                                    if ($manufacturer->person_in_charges) {
                                        $total = $manufacturer->person_in_charges[0]->total;
                                    }
                                    $TXT_PERSON_IN_CHARGE_NUMBER = '';
                                    if ($total < 2) {
                                        $TXT_PERSON_IN_CHARGE_NUMBER =  __d('manufacturer', 'TXT_PERSON');
                                    } else {
                                        $TXT_PERSON_IN_CHARGE_NUMBER = __d('manufacturer', 'TXT_PEOPLE');
                                    }
                                    echo $this->Html->link($total.' '.$TXT_PERSON_IN_CHARGE_NUMBER, [
                                        'controller' => 'personInCharges',
                                        'action' => ($total ? 'index' : 'add'),
                                        '?' => [
                                            'external_id' => $manufacturer->id,
                                            'filter_by' => TYPE_MANUFACTURER,
                                            'des' => TYPE_MANUFACTURER,
                                        ]
                                    ], [
                                        'class' => 'btn btn-default text-center btn-sm btn-primary',
                                    ]); ?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <?php
                                    echo __d('manufacturer', 'TXT_PRODUCT'); ?> :
                                </td>
                                <td colspan="2">
                                    <?php
                                    $brands = new Cake\Collection\Collection($manufacturer->product_brands);
                                    $total_products = $brands->sumOf(function ($brand) {
                                        if ($brand->products) {
                                            return $brand->products[0]->total;
                                        }
                                    });
                                    if ($total_products < 2) {
                                        $txt_product =  __d('manufacturer', 'TXT_PRODUCT');
                                    } else {
                                        $txt_product = __d('manufacturer', 'TXT_PRODUCTS');
                                    }
                                    echo $this->Form->button($total_products . ' ' . $txt_product, [
                                        'type' => 'button',
                                        'class' => 'btn btn-default text-center btn-sm btn-primary',
                                    ]); ?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <?php
                                    echo __d('manufacturer', 'TXT_DIRECT_SALE'); ?> :
                                </td>
                                <td colspan="2"></td>
                            </tr>
                            <tr>
                                <td>
                                    <?php
                                    echo __d('manufacturer', 'TXT_REMARK'); ?> :
                                </td>
                                <td colspan="2">
                                    <textarea readonly="readonly" style="background: none;">
                                        <?php
                                        echo $manufacturer->remarks; ?>
                                    </textarea>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <?php
                                    echo __d('manufacturer', 'TXT_MODIFIED'); ?> :
                                </td>
                                <td colspan="2">
                                    <?php
                                    echo $manufacturer->modified; ?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <?php
                                    echo __d('manufacturer', 'TXT_REGISTERD'); ?> :
                                </td>
                                <td colspan="2">
                                    <?php
                                    echo $manufacturer->created; ?>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="modal-footer" style="text-align: center !important;">
                    <?php
                    echo $this->Form->button(__('MANUFACTURERS_TXT_CANCEL'), [
                        'type' => 'button',
                        'class' => 'btn btn-default btn-sm  btn-close-modal btn-width modal-close',
                        'data-dismiss' => 'modal',
                        'aria-label' => 'Close',
                    ]);
                    ?>
                </div>
            </div>
        </div>
    </div>
</section>
