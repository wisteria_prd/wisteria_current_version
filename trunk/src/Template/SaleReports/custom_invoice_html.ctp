<div class="page">
    <div class="page-content">
        <h2>Custom Invoice</h2>
        <div class="container">
            <div class="row">
                <div class="col col-6">
                    <p>発行日 01/01/2018</p>
                    <p>支払依頼書番号 1801020F001</p>
                </div>
                <div class="col col-6">
                    <div class="text-center">
                        <?= $this->Html->image('medipro_logo.jpg', ['class' => 'logo']) ?>
                    </div>
                    <h3>Medical Professionals Pte. Ltd.</h3>
                    <p>3791 Jalan Bukit Merah, #10-17 E-Centre@Redhill, Singapore</p>
                    <p>159471</p>
                    <p>Tel: +65-6274-0433 / Fax: +65-6274-0477</p>
                    <p>UEN No. 201403392G</p>
                </div>
            </div>
            <div class="spacer">&nbsp;</div>
            <div class="row">
                <div class="col col-6">
                    <div class="spacer"></div>
                    <p>なおみ皮フ科クリニック</p>
                    <p>, 神田町9-2, 岐阜市岐阜県, , 500-8833</p>
                    <p>電話：Telはありません FAX：058-265-0704</p>
                </div>
            </div>
        </div>
        <p class="text-right">(日本円)</p>
        <table class="table table-data">
            <thead>
                <tr>
                    <th>No.</th>
                    <th>詳細</th>
                    <th>単価</th>
                    <th>数</th>
                    <th width="20%">金額</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td class="text-center">1</td>
                    <td>BALANCE LIDOCAINE 1 シリンジ X 1 ml</td>
                    <td class="text-right">55.00</td>
                    <td class="text-right">50</td>
                    <td class="text-right">3,300.00</td>
                </tr>
                <tr>
                    <td class="text-center">2</td>
                    <td class="product-indent">BALANCE LIDOCAINE ( 1 シリンジ X 1 ml ) ( 34Test56, Exp: Dec-2020 ) x 12</td>
                    <td class="text-right">00.00</td>
                    <td class="text-right">0</td>
                    <td class="text-right">00.00</td>
                </tr>
                <tr>
                    <td class="text-center">3</td>
                    <td class="product-indent">BALANCE LIDOCAINE ( 1 シリンジ X 1 ml ) ( 34Test56, Exp: Dec-2020 ) x 1</td>
                    <td class="text-right">&nbsp;</td>
                    <td class="text-right">&nbsp;</td>
                    <td class="text-right">&nbsp;</td>
                </tr>
                <tr>
                    <td class="text-center">4</td>
                    <td class="product-indent">&nbsp;</td>
                    <td class="text-right">&nbsp;</td>
                    <td class="text-right">&nbsp;</td>
                    <td class="text-right">&nbsp;</td>
                </tr>
                <tr>
                    <td class="text-center">5</td>
                    <td class="product-indent">&nbsp;</td>
                    <td class="text-right">&nbsp;</td>
                    <td class="text-right">&nbsp;</td>
                    <td class="text-right">&nbsp;</td>
                </tr>
                <tr>
                    <td class="text-center">6</td>
                    <td class="product-indent">&nbsp;</td>
                    <td class="text-right">&nbsp;</td>
                    <td class="text-right">&nbsp;</td>
                    <td class="text-right">&nbsp;</td>
                </tr>
                <tr>
                    <td class="text-center">7</td>
                    <td class="product-indent">&nbsp;</td>
                    <td class="text-right">&nbsp;</td>
                    <td class="text-right">&nbsp;</td>
                    <td class="text-right">&nbsp;</td>
                </tr>
                <tr>
                    <td class="text-center">8</td>
                    <td class="product-indent">&nbsp;</td>
                    <td class="text-right">&nbsp;</td>
                    <td class="text-right">&nbsp;</td>
                    <td class="text-right">&nbsp;</td>
                </tr>
                <tr>
                    <td class="text-center">9</td>
                    <td class="product-indent">&nbsp;</td>
                    <td class="text-right">&nbsp;</td>
                    <td class="text-right">&nbsp;</td>
                    <td class="text-right">&nbsp;</td>
                </tr>
                <tr>
                    <td class="text-center">10</td>
                    <td class="product-indent">&nbsp;</td>
                    <td class="text-right">&nbsp;</td>
                    <td class="text-right">&nbsp;</td>
                    <td class="text-right">&nbsp;</td>
                </tr>                
                <tr>
                    <td class="total-bold" colspan="4">Sub Total</td>
                    <td class="text-right total-bold">$0.00</td>
                </tr>
                <tr>
                    <td class="total-bold" colspan="4">&nbsp;</td>
                    <td class="text-right total-bold">&nbsp;</td>
                </tr>
                <tr>
                    <td class="total total-bold" colspan="4">Total</td>
                    <td class="total total-bold text-right">$0.00</td>
                </tr>
            </tbody>
        </table>
        <div class="spacer"></div>
        <div class="container">
            <div class="row">
                <div class="col col-6">
                    <p>備考：</p>
                </div>
                <div class="col col-6">
                    <p>お振込先口座：</p>
                    <div class="spacer"></div>
                    <div class="addr-wrap">
                        <p>Medical Professionals Pte. Ltd.への送金代行用口座です。</p>
                        <div class="spacer"></div>
                        <table class="bank-info">
                            <tbody>
                                <tr>
                                    <td>銀行名</td>
                                    <td>三菱東京UFJ銀行</td>
                                </tr>
                                <tr>
                                    <td>支店名</td>
                                    <td>青山通支店（084）</td>
                                </tr>
                                <tr>
                                    <td>口座番号</td>
                                    <td>普通　0090906</td>
                                </tr>
                                <tr>
                                    <td>口座名義</td>
                                    <td>株式会社ウィステリア</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>