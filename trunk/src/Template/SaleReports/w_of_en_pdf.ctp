
<?php
ini_set('memory_limit', '-1');

// create new PDF document
// $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
$pdf = new TCPDF('', 'mm', 'A4', true, 'UTF-8');
// Reset the encoding forced from tcpdf
mb_internal_encoding('UTF-8');

//$font_jp = TCPDF_FONTS::addTTFfont(WWW_ROOT.'fonts/ipag.ttf');
//$font_jp = TCPDF_FONTS::addTTFfont(WWW_ROOT.'fonts/meiryo-01.ttf');

// test language
$language = 'Helvetica';

ob_clean(); // cleaning the buffer before Output()

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Nick');
$pdf->SetTitle('Wisteria Invoice');
$pdf->SetSubject('Wisteria Invoice');
$pdf->SetKeywords('');

// Configure header and footer
$pdf->SetPrintHeader(false);
$pdf->SetPrintFooter(false);

// set margins
$pdf->SetMargins(13, 13, 13);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
    require_once(dirname(__FILE__).'/lang/eng.php');
    $pdf->setLanguageArray($l);
}
// set default font subsetting mode
$pdf->setFontSubsetting(true);
$pdf->SetFont('Helvetica', '', 8);

// page padding
$page_padding_top = 10;
$page_padding_left = 10;

$pdf->AddPage();

$pdf->SetFillColor(255,255,255);
// Header title of document
$pdf->SetFont($language, 'B', 20);
$pdf->MultiCell($width = 90, $height = 20, $text = 'Order Form', $border = 0, $align = 'C', $fill=true, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = 15, $valign = 'M');
$pdf->ln();

// MediPro Logo
$arrContextOptions=array(
    'ssl' => array(
        'verify_peer' => false,
        'verify_peer_name' => false,
    ),
);
$pathLogo = $this->Url->build('/', true) .'img/wisteria_logo.png';
$data = file_get_contents($pathLogo, false, stream_context_create($arrContextOptions));
$pdf->Image('@' . $data, 150, $page_padding_top, 20, '', 'PNG', '', 'T', false, '', '', false, false, 0, false, false, false);
$pdf->ln();

$issueDate = '';
$invNumber = '';
$registerPhone = '';
if (!empty($sale['sale_tax_invoices'])) {
    $issueDate = $sale['sale_tax_invoices'][0]['issue_date'];
    $issueDate = $issueDate->i18nFormat('dd/MM/yyyy');
    $invNumber = $sale['sale_tax_invoices'][0]['tax_invoice_number'];
    $registerPhone = isset($sale['sale_tax_invoices'][0]['phone']) ? $sale['sale_tax_invoices'][0]['phone'] : '';
}

$pdf->SetFont($language, '', 11);
$date = 'Issue Date              ' . $issueDate . "\n";
$date .= 'Order Form No.      ' . $invNumber;
$pdf->MultiCell($width = 85, $height = '', $text = $date, $border = 0, $align = 'L', $fill=true, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = 20, $valign = 'T');

$pdf->SetFont($language, '', 9);
$address = 'Wisteria Co., Ltd.' . "\n";
$address .= '4F, 1-4-6 Nihonbashi Kayaba-cho, Chuo-ku, Tokyo JAPAN 103-0025' . "\n";
$address .= 'Tel: +81-3-4588-1847  FAX: +81-3-4588-1848';
//$pdf->MultiCell($width = 85, $height = '', $text = '', $border = 0, $align = 'C', $fill=true, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = '', $valign = 'M');
$pdf->MultiCell($width = 110, $height = '', $text = $address, $border = 0, $align = 'L', $fill=true, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = '', $valign = 'M');
$pdf->ln();

// Stamp
$pathStamp = $this->Url->build('/', true) .'img/stamp.png';
$stamp = file_get_contents($pathStamp, false, stream_context_create($arrContextOptions));
$pdf->Image('@' . $stamp, 140, 35, 17, '', 'PNG', '', 'T', false, '', '', false, false, 0, false, false, false);
$pdf->ln();
$pdf->Cell(10, 6, '', 0, false, 'L');
$pdf->ln();

// line break
//$html = '<hr>';
//$pdf->writeHTMLCell(0, 0, '', '', $html, '', 0, 0, true, 'L', true);

// Clinic customer
//$pdf->Cell(10, 6, '', 0, false, 'L');
$pdf->SetFont($language, '', 9);
$customer = $sale['customer'];
$customer_tel = 'Telはありません';
if (isset($customer['info_details']) && !empty($customer['info_details'][0]['tel'])) {
    $customer_tel = $customer['info_details'][0]['tel'];
}

$personInCharge = $customer['name'] . "\n";
$personInCharge .= $customer['building'] . ', ' . $customer['street'] . ', ' . $customer['city'];
$personInCharge .= $customer['prefecture'] . ', ' . $customer['country'] . ', ' . $customer['postal_code'];
$personInCharge .= "\n";
$personInCharge .= '電話：'.$customer_tel.'  FAX：' . $customer['fax'];
$pdf->Cell(13, 4, 'To:  ', 0, false, 'L');
$pdf->MultiCell($width = 75, $height = '', $text = $personInCharge, $border = 0, $align = 'L', $fill=true, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = '', $valign = 'B');

//$pdf->Cell(13, 4, '発送先:', 0, false, 'L');

//$shippingAddress = '〒[suppliers.zipcode]　[suppliers.prefecture][suppliers.city][suppliers.street]' . "\n";
//$shippingAddress .= '電話：03-4588-1847 / FAX：03-4588-1848';
//$pdf->MultiCell($width = 95, $height = '', $text = $shippingAddress, $border = 0, $align = 'L', $fill=true, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = '', $valign = 'B');
//$pdf->ln();
//$pdf->ln();

$pdf->Cell($pdf->GetY(), 5, '', 0, false, 'L');
$pdf->ln();

$pdf->MultiCell($width = 190, $height = '', $text = '(US Dallar)', $border = 0, $align = 'R', $fill=true, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = '', $valign = 'M');
$pdf->ln();

// Products Details
//$pdf->SetFillColor(120,120,120);
$pdf->SetLineStyle(array('width' => 0.1, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(115, 115, 115)));
$pdf->SetFont('Helvetica', '', 11);
$fill = '#F4F4F4';
$pdf->Cell(10, 10, 'No.', 1, false, 'C', $fill);
$pdf->SetFont($language, 'B', 10);
$pdf->Cell(105, 10, 'Description', 1, false, 'C', $fill);
$pdf->Cell(30, 10, 'Unit Price', 1, false, 'C', $fill);
$pdf->Cell(15, 10, 'Qty', 1, false, 'C', $fill);
$pdf->Cell(30, 10, 'Amount', 1, false, 'C', $fill);
$pdf->ln();

//$pdf->SetFillColor(255,255,255);
$pdf->SetFont($language, '', 9);
//$pdf->SetFont('Helvetica', '', 9);

// PDF Content
$numRow = 0;
$totalAmount = 0;
$totalPrice = 0;
$countRecords = count($saleDetails);

if ($countRecords <= 20) {
    $products = '';

    $cellHeight = 6;
    // foreach saleDetails
    foreach ($saleDetails as $key => $value) {
        $numRow += 1;
        $childs = $value->children;
        $saleStockDetails = $value->sale_stock_details;
        //$productDetailName = $value->pack_size_value . ' ' . $value->pack_size_name . ' T ' . $value->single_unit_value . ' ' . $value->single_unit_name;
        //$productName = $value->product_name . ' ' . $productDetailName;
        $productName = $this->Comment->POProductDetailName($value);
        $unitPrice = $this->Comment->numberFormat($value->unit_price) . ' ' . $sale->currency->code_jp;

        $pdf->MultiCell($width = 10, $height = $cellHeight, $text = $numRow, $border = 1, $align = 'C', $fill=true, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = $cellHeight, $valign = 'M');
        $pdf->MultiCell($width = 105, $height = $cellHeight, $text = $productName, $border = 1, $align = 'L', $fill=true, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = $cellHeight, $valign = 'M');
        $pdf->MultiCell($width = 30, $height = $cellHeight, $text = $unitPrice, $border = 1, $align = 'R', $fill=true, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = $cellHeight, $valign = 'M');
        $pdf->MultiCell($width = 15, $height = $cellHeight, $text = $value->quantity, $border = 1, $align = 'R', $fill=true, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = $cellHeight, $valign = 'M');
        $subTotal = $value->unit_price * $value->quantity;
        $totalPrice += $subTotal;
        $pdf->MultiCell($width = 30, $height = $cellHeight, $text = $this->Comment->numberFormat($subTotal) . ' ' . $sale->currency->code_jp, $border = 1, $align = 'R', $fill=true, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = $cellHeight, $valign = 'M');
        $pdf->ln();

        // display sale stock detail
        if ($saleStockDetails) {
            foreach ($saleStockDetails as $key1 => $value1) {
                $numRow += 1;
                //$importDetail = '  ' . $value->product_name . ' ( ' . $productDetailName . ' ) ( ' . $value1->stock->import_detail->lot_number . ', Exp: ' .
                //    date('M-Y', strtotime($value1->stock->import_detail->expiry_date)) . ' ) x ' . $value1->quantity;
                $productDetailName = '  ' . $productName;
                $productDetailName .= ' ( ' . $value1->stock->import_detail->lot_number;
                $productDetailName .= ', Exp: ';
                $productDetailName .= date('M-Y', strtotime($value1->stock->import_detail->expiry_date)) . ' ) ';
                $productDetailName .= ' x ' . $value1->quantity;

                $pdf->Cell(10, 6, $numRow, 1, false, 'C');
                $pdf->Cell(105, 6, $productDetailName , 1, false, 'L');
                $pdf->Cell(30, 6, '', 1, false, 'R');
                $pdf->Cell(15, 6, '', 1, false, 'R');
                $pdf->Cell(30, 6, '', 1, false, 'R');
                $pdf->ln();
            }
        }

        // display child
        if ($childs) {
            foreach ($childs as $key2 => $value2) {
                $numRow += 1;
                $saleStockDetails2 = $value2->sale_stock_details;
                $productDetailName2 = $value2->pack_size_value . ' ' . $value2->pack_size_name . ' X ' . $value2->single_unit_value . ' ' . $value2->single_unit_name;
                $productName2 = '    ' . $value2->product_name . ' ' . $productDetailName2;

                $pdf->Cell(10, 6, $numRow, 1, false, 'C');
                $pdf->Cell(105, 6, $productName2 , 1, false, 'L');
                $pdf->Cell(30, 6, '', 1, false, 'R');
                $pdf->Cell(15, 6, $value->quantity, 1, false, 'R');
                $subTotal = $value2->unit_price * $value2->quantity;
                $totalPrice += $subTotal;
                $pdf->Cell(30, 6, $this->Comment->numberFormat($subTotal) . ' ' . $sale->currency->code_jp, 1, false, 'R');
                $pdf->ln();
            }

            // display sale stock detail for child node
            if ($saleStockDetails) {
                foreach ($saleStockDetails as $key3 => $value3) {
                    $numRow += 1;
                    $importDetail = '      ' . $value2->product_name . ' ( ' . $productDetailName2 . ' ) ( ' . $value3->stock->import_detail->lot_number . ', Exp: ' .
                        date('M-Y', strtotime($value3->stock->import_detail->expiry_date)) . ' ) x ' . $value3->quantity;

                    $pdf->MultiCell($width = 10, $height = 6, $text = $numRow, $border = 1, $align = 'C', $fill=true, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = '', $valign = 'M');
                    $pdf->MultiCell($width = 105, $height = 6, $text = $importDetail, $border = 1, $align = 'L', $fill=true, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = '', $valign = 'M');
                    $pdf->MultiCell($width = 30, $height = 6, $text = '', $border = 1, $align = 'R', $fill=true, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = '', $valign = 'M');
                    $pdf->MultiCell($width = 15, $height = 6, $text = '', $border = 1, $align = 'R', $fill=true, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = '', $valign = 'M');
                    $pdf->MultiCell($width = 30, $height = 6, $text = '', $border = 1, $align = 'R', $fill=true, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = '', $valign = 'M');
                    $pdf->ln();
                }
            }
        }
    }
}

while ($numRow < 10) {
    $pdf->Cell(10, 6, $numRow + 1, 1, false, 'C');
    $pdf->Cell(105, 6, '', 1, false, 'C');
    $pdf->Cell(30, 6, '', 1, false, 'C');
    $pdf->Cell(15, 6, '', 1, false, 'C');
    $pdf->Cell(30, 6, '', 1, false, 'C');
    $pdf->ln();
    $numRow++;
}

$pdf->SetFont($language, '', 10);
$pdf->MultiCell($width = 160, $height = 6, $text = ' Sub Total', $border = 1, $align = 'L', $fill=true, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = 6, $valign = 'M');
$pdf->MultiCell($width = 30, $height = 6, '$'.$text = $this->Comment->numberFormat($totalPrice), $border = 1, $align = 'R', $fill=true, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = 6, $valign = 'M');
$pdf->ln();
$pdf->MultiCell($width = 160, $height = 6, $text = ' -', $border = 1, $align = 'L', $fill=true, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = 6, $valign = 'M');
$colK = '';
$pdf->MultiCell($width = 30, $height = 6, $text = '$'.$this->Comment->numberFormat($colK), $border = 1, $align = 'R', $fill=true, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = 6, $valign = 'M');
$pdf->ln();
$pdf->MultiCell($width = 160, $height = 15, $text = ' Total', $border = 1, $align = 'L', $fill=true, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = 15, $valign = 'M');

$pdf->MultiCell($width = 30, $height = 15, $text = '$'.$this->Comment->numberFormat($totalPrice), $border = 1, $align = 'R', $fill=true, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = 15, $valign = 'M');

$pdf->ln();
$pdf->Cell(100, 1, '', 0, false, 'C');
$pdf->ln();
$pdf->ln();
$pdf->ln();
$pdf->ln();
$pdf->ln();
$pdf->ln();

$pdf->MultiCell($width = 70, $height = 35, $text = 'Remarks:', $border = 0, $align = 'L', $fill = true, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = 35, $valign = 'T');
$pdf->Cell(20, 6, '', 0, false, 'C');
$html = '<hr>';
$pdf->writeHTMLCell(70, 0, '', '', $html, '', 0, 0, true, 'L', true);
$pdf->ln();
//$pdf->Cell(20, 6, 'Miho FUJIMURA, Managing Director, Wisteria Co., Ltd.', 0, false, 'C');

//$pdf->MultiCell($width = 90, $height = 6, $text = 'お振込先口座：', $border = 0, $align = 'L', $fill=true, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = 6, $valign = 'T');
//$pdf->ln();
//$pdf->SetFont($language, 'B', 9);
//$pdf->MultiCell($width = 100, $height = 8, $text = '   Medical Professionals Pte. Ltd.への送金代行用口座です。', $border = 0, $align = 'L', $fill=true, $ln = 0, $x = 103, $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = 6, $valign = 'T');
//$pdf->ln();
$pdf->Output();
