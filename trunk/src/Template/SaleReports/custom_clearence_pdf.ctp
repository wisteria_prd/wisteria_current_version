
<?php
ini_set('memory_limit', '-1');

// create new PDF document
// $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
$pdf = new TCPDF('', 'mm', 'A4', true, 'UTF-8');
// Reset the encoding forced from tcpdf
mb_internal_encoding('UTF-8');

//$font_jp1 = TCPDF_FONTS::addTTFfont(WWW_ROOT.'fonts/ipag.ttf');
$font_jp = TCPDF_FONTS::addTTFfont(WWW_ROOT.'fonts/meiryo-01.ttf');

// language
$local = $this->request->session()->read('tb_field');
if ($local === 'en') {
    $language = 'Helvetica';
} else {
    $language = $font_jp;
}
// test language
$language = $font_jp;

ob_clean(); // cleaning the buffer before Output()

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Nick');
$pdf->SetTitle('Purchase Order Form');
$pdf->SetSubject('Purchase Order Form');
$pdf->SetKeywords('');

// Configure header and footer
$pdf->SetPrintHeader(false);
$pdf->SetPrintFooter(false);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
    require_once(dirname(__FILE__).'/lang/eng.php');
    $pdf->setLanguageArray($l);
}
// set default font subsetting mode
$pdf->setFontSubsetting(true);
$pdf->SetFont('Helvetica', '', 8);

// page padding
$page_padding_left = 20;
$page_padding_top = 25;
$page_padding_right = $page_padding_left;
// header title
$paperWidth = $pdf->GetPageWidth();
$pageWidth = $paperWidth - ($page_padding_left + $page_padding_right);
$pdf->SetMargins($left = $page_padding_left,$top = $page_padding_top,$right = $page_padding_right, true);


//=========== Custom clearence type 1 ===========
$pdf->AddPage();
// header page
$pdf->SetFont($language, '', 8);

$pdf->SetFillColor(255,255,255);
//$pdf->MultiCell($width = 80, $height = 60, $text = 'Main column', $border = 1, $align = 'L', $fill=true, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = 60, $valign = 'T');

$pdf->SetFont($language, '', 18);
$pdf->Cell($pageWidth, 10, '委任状', 0, false, 'C');
$pdf->ln();
$pdf->Cell($pageWidth, 40, '厚生労働大臣　殿', 0, false, 'L');
$pdf->ln();

// default content font size
$fontSize = 11;

$pdf->SetFont($language, '', $fontSize);
$pdf->Cell(22, 4, '【受任者】', 0, false, 'L');
$pdf->ln();

$pdf->Cell(18, 4, '', 0, false, 'L');
$pdf->Cell(15, 5, '所在地：', 0, false, 'L');

$address = '〒103-0025' . "\n";
$address .= '東京都中央区日本橋茅場町1-4-6　木村實業第２ビル４Ｆ' . "\n";
$address .= 'TEL：03-4588-1847　　　　FAX：03-4588-1848' . "\n";
$address .= '株式会社ウィステリア     入月弘美';
$pdf->MultiCell($width = 120, $height = '', $text = $address, $border = 0, $align = 'L', $fill = false, $ln = '', $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = '', $valign = 'M');
$pdf->ln();
$pdf->ln();

$pdf->SetFont($language, '', $fontSize);
$text = '私は上記の者を代理人と定め、医療機器および医薬品等の輸入に必要な薬監証明の取得に関す'. "\n";
$text .= 'る一切の権限を委任します。';
$pdf->MultiCell($width = $pageWidth, $height = '', $text = $text, $border = 0, $align = 'L', $fill = true, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = '', $valign = 'M');
$pdf->ln();
//pr($this->JpDatetimeUtility->wareki(strtotime($pdf_data->sale_custom_clearance_doc->import_date->i18nFormat('yyyy/MM/dd'))));
if (!empty($pdf_data->sale_custom_clearance_doc)) {
    $c_year = date('Y', strtotime($pdf_data->sale_custom_clearance_doc->import_date));
    $c_month = date('m', strtotime($pdf_data->sale_custom_clearance_doc->import_date));
    $c_day = date('d', strtotime($pdf_data->sale_custom_clearance_doc->import_date));
    $clearenceDoc = $pdf_data->sale_custom_clearance_doc;
} else {
    $c_year = '';
    $c_month = '';
    $c_day = '';
    $clearenceDoc = null;
}

//$text = '平成　'.$c_year.'年　'.$c_month.'月　'.$c_day.'日';
if (!empty($pdf_data->sale_custom_clearance_doc)) {
    //$date_n = $this->JpDatetimeUtility->date('J', strtotime($pdf_data->sale_custom_clearance_doc->import_date->i18nFormat('yyyy/MM/dd'))) . ' ';
    $date_n = $this->JpDatetimeUtility->wareki(strtotime($pdf_data->sale_custom_clearance_doc->import_date->i18nFormat('yyyy/MM/dd'))) . ' ';
} else {
    $date_n = ' ';
}

$date_n .= $c_year.'年　'.$c_month.'月　'.$c_day.'日';
$pdf->MultiCell($width = $pageWidth - 5, $height = 10, $text = $date_n, $border = 0, $align = 'R', $fill=true, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = 60, $valign = 'T');
$pdf->ln();

$pdf->Cell(40, 5, '【委任者】', 0, false, 'L');
$pdf->ln();

//$customer_address = $pdf_data->customer->postal_code;
//$customer_address .= $pdf_data->customer->prefecture;
//$customer_address .= $pdf_data->customer->city;
//$customer_address .= $pdf_data->customer->street;
//$customer_address .= $pdf_data->customer->building;

$customerAddressType1 = '〒'.$pdf_data->customer->postal_code . "\n";
$customerAddressType1 .= $pdf_data->customer->prefecture . $pdf_data->customer->city . $pdf_data->customer->street . "\n";
$customerAddressType1 .= $pdf_data->customer->building . "\n";
//pr($customerAddressType1);exit;

$pdf->Cell(18, 5, '', 0, false, 'L');
$pdf->MultiCell($width = 45, $height = '', $text = '所属医療機関名   ：', $border = 0, $align = 'L', $fill=true, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = '', $valign = 'T');
if (!empty($pdf_data->customer->medical_corporation)) {
    $customer_name = $pdf_data->customer->medical_corporation->entity_type.$pdf_data->customer->medical_corporation->$name.'　'.$pdf_data->customer->$name;
} else {
    $customer_name = '';
}
$pdf->MultiCell($width = 110, $height = '', $text = $customer_name, $border = 0, $align = 'L', $fill=true, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = '', $valign = 'T');
$pdf->ln();
$pdf->Cell(18, 5, '', 0, false, 'L');
$pdf->MultiCell($width = 45, $height = '', $text = '医　　師　　名　：', $border = 0, $align = 'L', $fill=true, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = '', $valign = 'T');
$text = $clearenceDoc ? $clearenceDoc['doctor_name'] : '';
$pdf->MultiCell($width = 110, $height = '', $text = $text, $border = 0, $align = 'L', $fill=true, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = '', $valign = 'T');
$pdf->ln();
$pdf->Cell(18, 5, '', 0, false, 'L');
$pdf->MultiCell($width = 45, $height = '', $text = '所　　在　　地　：', $border = 0, $align = 'L', $fill=true, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = '', $valign = 'T');
$pdf->MultiCell($width = 110, $height = '', $text = $customerAddressType1, $border = 0, $align = 'L', $fill=true, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = '', $valign = 'T');
$pdf->ln();



//=========== Custom clearence type 2 ===========
$pdf->AddPage();

// header title
$pdf->SetFont($language, '', 12);
$pdf->Cell($pageWidth, 15, '〔別紙第１号様式〕', 0, false, 'L');
$pdf->ln();

if (!empty($pdf_data->sale_details)) {
    $productType1 = '';
    $productType2 = '';
    foreach ($pdf_data->sale_details as $value) {
        if ($value->product_detail->type == 'Medical Device') {
            // product are medical device
            $productType1 = '医療機器';
        } else {
            // product are medicinal
            $productType2 = '医薬品';
        }
    }
    if ($productType1 && $productType2) {
        $reportProductTitle = '1. ' . $productType1 . ' / 2. ' . $productType2;
    } else {
        $reportProductTitle = $productType1 . $productType2;
    }
    $reportTitle = '※（　' . $reportProductTitle . '　）輸入報告書';
} else {
    $reportTitle = '輸入報告書';
}
$pdf->Cell($pageWidth, 10, $reportTitle, 0, false, 'C');
$pdf->ln();
//pr($pdf_data->sale_details);exit;
$pdf->SetFont($language, '', 11);
$pdf->MultiCell($width = $pageWidth, $height = 10, $text = $date_n, $border = 0, $align = 'R', $fill=true, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = 10, $valign = 'B');
$pdf->ln();

$pdf->SetFont($language, '', 12);
$pdf->Cell(7, 6, '', 0, false, 'L');
$pdf->Cell($pageWidth - 20, 6, '厚生労働大臣殿', 0, false, 'L');
$pdf->ln();

$complex_cell_border = array(
   'T' => array('width' => 0, 'color' => array(255,255,255), 'dash' => 0, 'cap' => 'butt'),
   'R' => array('width' => 0, 'color' => array(255,255,255), 'dash' => 0, 'cap' => 'round'),
   'B' => array('width' => 0, 'color' => array(0,0,0), 'dash' => 0, 'cap' => 'square'),
   'L' => array('width' => 0, 'color' => array(255,255,255), 'dash' => 0, 'cap' => 'butt'),
);

$pdf->SetFont($language, '', 8.5);
$text = '輸入者（受取人）氏名（法人に' . "\n" . 'あっては名称及び代表者の氏名）';
$pdf->MultiCell($width = 48, $height = 8, $text = $text, $border = 0, $align = 'L', $fill=true, $ln = 0, $x = 82, $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = 8, $valign = 'M');
$doctorName = $clearenceDoc ? $clearenceDoc['doctor_name'] : '';
$personInCharge = $pdf_data->customer_name;
$pdf->MultiCell($width = 55, $height = 8, $text = $personInCharge, $border = 0, $align = 'C', $fill = true, $ln = 0, $x = 130, $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = 8, $valign = 'M');

$arrContextOptions=array(
    'ssl' => array(
        'verify_peer' => false,
        'verify_peer_name' => false,
    ),
);

$path = $this->Url->build('/', true) .'img/u_plus_329e.png';
$icon_u_plus_329e = file_get_contents($path, false, stream_context_create($arrContextOptions));
$pdf->Image('@' . $icon_u_plus_329e, $pageWidth + ($page_padding_left - 6), 68, 4, '', 'PNG', '', '', false, '', '', false, false, '', false, false, false);
//$pdf->MultiCell($width = 70, $height = 8, $text = '㊞  ', $border = $complex_cell_border, $align = 'R', $fill = true, $ln = 0, $x = 130, $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = 8, $valign = 'M');
$pdf->ln();

$complex_cell_border = array(
   'T' => array('width' => 0, 'color' => array(0,0,0), 'dash' => 0, 'cap' => 'butt'),
   'R' => array('width' => 0, 'color' => array(255,255,255), 'dash' => 0, 'cap' => 'round'),
   'B' => array('width' => 0, 'color' => array(0,0,0), 'dash' => 0, 'cap' => 'square'),
   'L' => array('width' => 0, 'color' => array(255,255,255), 'dash' => 0, 'cap' => 'butt'),
);
$pdf->SetFont($language, '', 8.5);
$recipientSpaceWidth = 48;
$recipientSpaceWidth2 = 60;
$text = '住所（法人にあっては主たる事務' . "\n" . '所の所在地）';
//$address = '〒' . $pdf_data->customer->postal_code.'　'.$pdf_data->customer->prefecture.$pdf_data->customer->city.$pdf_data->customer->street.'　'.$pdf_data->customer->building;
$pdf->MultiCell($width = $recipientSpaceWidth, $height = 8, $text = $text, $border = 0, $align = 'L', $fill=true, $ln = 0, $x = 82, $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = 8, $valign = 'M');
$pdf->SetFont($language, '', 9);
//pr($pdf_data->customer);exit;
list($address, $jp_addr2, $en_addr2, $phone, $fax) = $this->Comment->addressFormat($local, $pdf_data->customer, TYPE_MP);
if (mb_substr($address, 0, 1) == '〒') {
    if ($pdf_data->customer) {
        $address .= "\n" . $jp_addr2;
    }
} else {
    $address .= "\n" . $en_addr2;
}
$customerAddress = $address;
$pdf->MultiCell($width = $recipientSpaceWidth2, $height = 8, $text = $customerAddress, $border = $complex_cell_border, $align = 'C', $fill = true, $ln = 0, $x = 130, $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = 8, $valign = 'M');
$pdf->ln();

$complex_cell_border = array(
   'T' => array('width' => 0, 'color' => array(0,0,0), 'dash' => 0, 'cap' => 'butt'),
   'R' => array('width' => 0, 'color' => array(255,255,255), 'dash' => 0, 'cap' => 'round'),
   'B' => array('width' => 0, 'color' => array(0,0,0), 'dash' => 0, 'cap' => 'square'),
   'L' => array('width' => 0, 'color' => array(255,255,255), 'dash' => 0, 'cap' => 'butt'),
);
$pdf->SetFont($language, '', 8.5);
$text = '営業所等（貨物の送付先）の名称';
$pdf->MultiCell($width = $recipientSpaceWidth, $height = 4, $text = $text, $border = 0, $align = 'L', $fill=true, $ln = 0, $x = 82, $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = 4, $valign = 'M');
$pdf->SetFont($language, '', 7);
if (!empty($pdf_data->customer->medical_corporation)) {
    $medicalCorporationName = $pdf_data->customer->medical_corporation->entity_type.$pdf_data->customer->medical_corporation->$name;
} else {
    $medicalCorporationName = '';
}
$pdf->MultiCell($width = $recipientSpaceWidth2, $height = 4, $text = $medicalCorporationName, $border = $complex_cell_border, $align = 'C', $fill = true, $ln = 0, $x = 130, $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = 4, $valign = 'M');
$pdf->ln();

$complex_cell_border = array(
   'T' => array('width' => 0, 'color' => array(0,0,0), 'dash' => 0, 'cap' => 'butt'),
   'R' => array('width' => 0, 'color' => array(255,255,255), 'dash' => 0, 'cap' => 'round'),
   'B' => array('width' => 0, 'color' => array(0,0,0), 'dash' => 0, 'cap' => 'square'),
   'L' => array('width' => 0, 'color' => array(255,255,255), 'dash' => 0, 'cap' => 'butt'),
);
$pdf->SetFont($language, '', 8.5);
$text = '同所在地 ';
$pdf->MultiCell($width = $recipientSpaceWidth, $height = 4, $text = $text, $border = 0, $align = 'R', $fill = true, $ln = 0, $x = 82, $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = 4, $valign = 'M');
$pdf->SetFont($language, '', 8.5);
$customerName = $pdf_data->customer->$name;
$pdf->MultiCell($width = $recipientSpaceWidth2, $height = 4, $text = $customerName, $border = $complex_cell_border, $align = 'C', $fill = true, $ln = 0, $x = 130, $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = 4, $valign = 'M');
$pdf->ln();

$pdf->SetFont($language, '', 8.5);
$label = '担当者名';
$pdf->MultiCell($width = $recipientSpaceWidth, $height = 4, $text = $label, $border = 0, $align = 'R', $fill = true, $ln = 0, $x = 82, $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = 4, $valign = 'M');
$pdf->SetFont($language, '', 8.5);
$labelPhoneWidth = 15;
//$doctorName = $clearenceDoc ? $clearenceDoc['doctor_name'] : '';
$personInCharge = $pdf_data->customer_name;
$pdf->MultiCell($width = ($recipientSpaceWidth2 - $labelPhoneWidth) / 2, $height = 4, $text = $personInCharge, $border = 0, $align = 'C', $fill = true, $ln = 0, $x = 130, $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = 4, $valign = 'M');

$customerPhone = $pdf_data->customer_tel ? $pdf_data->customer_tel : '';
$pdf->MultiCell($width = $labelPhoneWidth, $height = 4, $text = '電話', $border = 0, $align = 'C', $fill = true, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = 4, $valign = 'M');
$pdf->MultiCell($width = ($recipientSpaceWidth2 - $labelPhoneWidth) / 2, $height = 4, $text = $customerPhone, $border = 0, $align = 'C', $fill = true, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = 4, $valign = 'M');
$pdf->ln();

$complex_cell_border = array(
   'T' => array('width' => 0, 'color' => array(0,0,0), 'dash' => 0, 'cap' => 'butt'),
   'R' => array('width' => 0, 'color' => array(255, 255, 255), 'dash' => 0, 'cap' => 'round'),
   'B' => array('width' => 0, 'color' => array(0,0,0), 'dash' => 0, 'cap' => 'butt'),
   'L' => array('width' => 0, 'color' => array(255, 255, 255), 'dash' => 0, 'cap' => 'butt'),
);
//pr($pdf_data);exit;
if (isset($pdf_data->customer->info_details) && !empty($pdf_data->customer->info_details)) {
    $customer_email = $pdf_data['customer']['info_details'][0]['email'] ? $pdf_data['customer']['info_details'][0]['email'] : 'Ｅメールはありません';
} else {
    $customer_email = 'Ｅメールはありません';
}
$pdf->SetFont($language, '', 8.5);
$text = 'Ｅメール';
$pdf->MultiCell($width = $recipientSpaceWidth, $height = 4, $text = $text, $border = 0, $align = 'R', $fill = true, $ln = 0, $x = 82, $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = 4, $valign = 'M');
$pdf->SetFont($language, '', 8.5);
$pdf->MultiCell($width = $recipientSpaceWidth2, $height = 4, $text = $customer_email, $border = $complex_cell_border, $align = 'C', $fill = false, $ln = 0, $x = 130, $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = 4, $valign = 'M');
$pdf->ln();


$pdf->MultiCell($width = $pageWidth, $height = 2, $text = ' ', $border = 0, $align = 'C', $fill = false, $ln = '', $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = 2, $valign = 'M');
$pdf->ln();

//----------------- List Products -----------------
$pdf->SetFont($language, '', 10);
$complex_cell_border = array(
   'T' => array('width' => 0, 'color' => array(0,0,0), 'dash' => 0, 'cap' => 'butt'),
   'R' => array('width' => 0, 'color' => array(0,0,0), 'dash' => 0, 'cap' => 'round'),
   'B' => array('width' => 0, 'color' => array(0,0,0), 'dash' => 0, 'cap' => 'butt'),
   'L' => array('width' => 0, 'color' => array(0,0,0), 'dash' => 0, 'cap' => 'butt'),
);

$columnProductWidth = $pageWidth / 2;
$columnProductQuantityWidth = $columnProductWidth * 30 / 100;
$columnRemarkWidth = $columnProductWidth - $columnProductQuantityWidth;
$pdf->MultiCell($width = $columnProductWidth, $height = 6, $text = '品　　　　　名', $border = $complex_cell_border, $align = 'C', $fill = false, $ln = '', $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = 6, $valign = 'M');
$pdf->MultiCell($width = $columnProductQuantityWidth, $height = 6, $text = '数　　量', $border = $complex_cell_border, $align = 'C', $fill = false, $ln = '', $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = 6, $valign = 'M');
$pdf->MultiCell($width = $columnRemarkWidth, $height = 6, $text = '業許可等の有無', $border = $complex_cell_border, $align = 'C', $fill = false, $ln = '', $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = 6, $valign = 'M');
$pdf->ln();
//pr($pdf_data);exit;
// Row product details
$products = '';
$quantities = '';
if (!empty($pdf_data->sale_details)) {
    $i = 0;
    foreach ($pdf_data->sale_details as $sale) {
        //$products = 'BELOTERO BALANCE (1.0ml x 1)' . "\n";
        //$quantities = '10' . "\n";
        $products .= $this->Comment->POProductDetailName($sale) . "\n";
        //$products .= $sale->product_name .' ('.$sale->single_unit_value.$sale->single_unit_name.' x '. $sale->pack_size_value.$sale->pack_size_name.')' . "\n";
        $quantities .= $sale->quantity . '箱' . "\n";
        $i++;
        if ($i == 6) {
            break;
        }
    }
}

$pdf->SetFont($language, '', 9);
$remarks = '□※（　　　　　　）製造販売業' . "\n";
$defaultProductHeight = 30;
$pdf->MultiCell($width = $columnProductWidth, $height = $defaultProductHeight, $text = $products, $border = $complex_cell_border, $align = 'L', $fill = false, $ln = '', $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = '', $valign = 'T');
$pdf->MultiCell($width = $columnProductQuantityWidth, $height = $defaultProductHeight, $text = $quantities, $border = $complex_cell_border, $align = 'R', $fill = false, $ln = '', $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = '', $valign = 'T');
$pdf->MultiCell($width = $columnRemarkWidth, $height = $defaultProductHeight, $text = $remarks, $border = $complex_cell_border, $align = 'L', $fill = false, $ln = '', $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = '', $valign = 'T');
$pdf->ln();

// Circle on text number 6
$style5 = array('width' => 0.1, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(0, 0, 0));
$pdf->SetLineStyle($style5);
$pdf->Ellipse(77,143,16,3);

$purpose_of_import = '①治験（企業）用、②臨床試験（医師）用、③試験研究・社内見本用、④展示用、' . "\n";
$purpose_of_import .= '⑤個人用、⑥医療従事者個人用、⑦再輸入品・返送品用、⑧自家消費用、' . "\n";
$purpose_of_import .= '⑨その他（　　　　　　　　　　　　　　　　　　　　　　）';
$pdf->SetFont($language, '', 10);
$columnLabel = 25;
$pdf->MultiCell($width = $columnLabel, $height = 15, $text = '輸入の目的', $border = $complex_cell_border, $align = 'L', $fill = false, $ln = '', $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = 15, $valign = 'M');
$pdf->SetFont($language, '', 9);
$columnText = $pageWidth - $columnLabel;
$pdf->MultiCell($width = $columnText, $height = 15, $text = $purpose_of_import, $border = $complex_cell_border, $align = 'L', $fill = false, $ln = '', $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = 15, $valign = 'M');
$pdf->ln();

//$pdf->SetFont($font_jp1, '', 10);
//$pdf->MultiCell($width = 6, $height = '', $text = '☑', $border = 0, $align = 'L', $fill = false, $ln = '', $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = '', $valign = 'M');
$matters = '   上記輸入の目的のために使用するもので、他に販売、貸与又は授与するものではありません。' . "\n";
$matters .= '  （個人用又は医療従事者個人用の場合）厚生労働省ホームページの「個人輸入において注意すべき' . "\n";
$matters .= '医薬品等について」を輸入前に確認し、輸入後も随時確認します。' . "\n";
$matters .= '□（試験研究・社内見本用の場合）人又は人の診断の目的には使用しません。';
$pdf->SetFont($language, '', 10);
$pdf->MultiCell($width = $columnLabel, $height = 18, $text = '  誓約事項', $border = $complex_cell_border, $align = 'L', $fill = false, $ln = '', $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = 18, $valign = 'M');
$pdf->SetFont($language, '', 9);
$pdf->MultiCell($width = $columnText, $height = 18, $text = $matters, $border = $complex_cell_border, $align = 'L', $fill = false, $ln = '', $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = 18, $valign = 'M');
$pdf->ln();

// icon checkbox
$path = $this->Url->build('/', true) .'img/checkbox-tik.png';
$checkbox = file_get_contents($path, false, stream_context_create($arrContextOptions));
$pdf->Image('@' . $checkbox, $page_padding_left + $columnLabel + 1, 152.7, 3, '', 'PNG', '', '', false, '', '', false, false, '', false, false, false);
$pdf->Image('@' . $checkbox, $page_padding_left + $columnLabel + 1, 156.8, 3, '', 'PNG', '', '', false, '', '', false, false, '', false, false, false);

$pdf->SetFont($language, '', 11);
$pdf->MultiCell($width = $pageWidth, $height = 6, $text = '製造業者名及び国名', $border = $complex_cell_border, $align = 'C', $fill = false, $ln = '', $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = 6, $valign = 'M');
$pdf->ln();

//$manufacturer = 'MERZ AESTHETICS, Inc.（メルツ エステティクス社） / Germany（ドイツ） →　RAD　＆　BELOTERO
//ANTEIS S.A.　　アンテイス社　　　　ジュネーブ/スイス　→　エセリス系
//MERZ Pharmaceuticals, LLC社（メルツファーマシューティカルズ社） / Germany(ドイツ)　→　Xeomin　＆　Bocouture';
$pdf->SetFont($language, '', 10);
$pdf->MultiCell($width = $pageWidth, $height = 15, $text = $manufacturer, $border = $complex_cell_border, $align = 'C', $fill = false, $ln = '', $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = 15, $valign = 'M');
$pdf->ln();

$pdf->SetFont($language, '', 11);

$pdf->MultiCell($width = $pageWidth / 4, $height = 7, $text = '輸入年月日', $border = $complex_cell_border, $align = 'C', $fill = false, $ln = '', $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = 7, $valign = 'M');
$pdf->MultiCell($width = $pageWidth / 4, $height = 7, $text = 'AWB、B/L等の番号', $border = $complex_cell_border, $align = 'C', $fill = false, $ln = '', $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = 7, $valign = 'M');
$pdf->MultiCell($width = $pageWidth / 2, $height = 7, $text = '到着空港、到着港又は蔵置場所', $border = $complex_cell_border, $align = 'C', $fill = false, $ln = '', $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = 7, $valign = 'M');
$pdf->ln();

$pdf->SetFont($language, '', 10);
$pdf->SetTextColor(0,0,0);
if (!empty($pdf_data->sale_custom_clearance_doc)) {
    $dDate = $this->JpDatetimeUtility->date('J', strtotime($pdf_data->sale_custom_clearance_doc->import_date->i18nFormat('yyyy/MM/dd')));
} else {
    $dDate = '';
}
$dDate .= ' ' . $c_year.'年　'.$c_month.'月　'.$c_day.'日';
$pdf->MultiCell($width = $pageWidth / 4, $height = 13, $text = $dDate, $border = $complex_cell_border, $align = 'C', $fill = false, $ln = '', $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = 13, $valign = 'M');
$pdf->MultiCell($width = $pageWidth / 4, $height = 13, $text = '', $border = $complex_cell_border, $align = 'C', $fill = false, $ln = '', $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = 13, $valign = 'M');
$pdf->MultiCell($width = $pageWidth / 2, $height = 13, $text = $clearenceDoc ? $clearenceDoc['storage_location'] : '', $border = $complex_cell_border, $align = 'C', $fill = false, $ln = '', $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = 13, $valign = 'M');
$pdf->ln();

$pdf->SetTextColor(0,0,0);
$pdf->SetFont($language, '', 11);
$columnLabel = 10;
$remarkHeight = 15;
$pdf->MultiCell($width = $columnLabel, $height = $remarkHeight, $text = '備考', $border = $complex_cell_border, $align = 'C', $fill = false, $ln = '', $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = $remarkHeight, $valign = 'M');
$pdf->SetFont($language, '', 9);
$pdf->MultiCell($width = $pageWidth - $columnLabel, $height = $remarkHeight, $text = '（再輸入品・返送品用の場合）再輸入・返送に至った理由及び今後の措置について記載すること。', $border = $complex_cell_border, $align = 'L', $fill = false, $ln = '', $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = $remarkHeight, $valign = 'T');
$pdf->ln();

$pdf->SetTextColor(0,0,0);
$pdf->SetFont($language, '', 11);
$noteHeight = 30;
$noteWidth = $pageWidth - $columnLabel;
$pdf->MultiCell($width = $columnLabel, $height = $noteHeight, $text = '厚生労働省確認欄', $border = $complex_cell_border, $align = 'C', $fill = false, $ln = '', $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = $noteHeight, $valign = 'M');
$pdf->MultiCell($width = $noteWidth, $height = $noteHeight, $text = '', $border = $complex_cell_border, $align = 'L', $fill = false, $ln = '', $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = $noteHeight, $valign = 'T');
$pdf->SetFont($language, '', 11);
$pdf->MultiCell($width = $noteWidth, $height = 7, $text = '特記事項', $border = 0, $align = 'L', $fill = false, $ln = '', $x = $page_padding_left + $columnLabel, $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = 7, $valign = 'M');
$pdf->ln();
$pdf->Cell(100, 8, '', 0, false, 'L');
$pdf->ln();
$pdf->SetTextColor(0,0,0);
$pdf->MultiCell($width = $noteWidth, $height = 5, $text = $clearenceDoc ? $clearenceDoc['minister'] : '', $border = 0, $align = 'L', $fill = false, $ln = '', $x = 105, $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = '', $valign = 'T');
$pdf->ln();
$pdf->SetTextColor(0,0,0);
$text = '薬事監視専門官' . "\n";
$text .= '毒物劇物監視員';
$pdf->MultiCell($width = 70, $height = '', $text = $text, $border = 0, $align = 'L', $fill = false, $ln = '', $x = 105, $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = '', $valign = 'T');
$pdf->Image('@' . $icon_u_plus_329e, $pageWidth , 250, 4, '', 'PNG', '', '', false, '', '', false, false, '', false, false, false);
$pdf->ln();
$pdf->MultiCell($width = 13, $height = 2, $text = '', $border = 0, $align = 'R', $fill = false, $ln = '', $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = 2, $valign = 'T');
$pdf->ln();

// note text in the document
$pdf->SetFont($language, '', 7);
$pdf->MultiCell($width = 13, $height = 4, $text = '（注）', $border = 0, $align = 'R', $fill = false, $ln = '', $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = 4, $valign = 'T');
$pdf->MultiCell($width = 5, $height = 4, $text = '１.', $border = 0, $align = 'L', $fill = false, $ln = '', $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = 4, $valign = 'T');
$pdf->MultiCell($width = 175, $height = 4, $text = '※（　）欄には，医薬品，医薬部外品，化粧品，医療機器，体外診断用医薬品、再生医療等製品，毒物，劇物の別を', $border = 0, $align = 'L', $fill = false, $ln = '', $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = 4, $valign = 'T');
$pdf->ln();
$pdf->MultiCell($width = 13, $height = 4, $text = '', $border = 0, $align = 'L', $fill = false, $ln = '', $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = 4, $valign = 'T');
$pdf->MultiCell($width = 5, $height = 4, $text = '', $border = 0, $align = 'L', $fill = false, $ln = '', $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = 4, $valign = 'T');
$pdf->MultiCell($width = 175, $height = 4, $text = '記入すること。', $border = 0, $align = 'L', $fill = false, $ln = '', $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = 4, $valign = 'T');
$pdf->ln();
$pdf->MultiCell($width = 13, $height = 4, $text = '', $border = 0, $align = 'L', $fill = false, $ln = '', $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = 4, $valign = 'T');
$pdf->MultiCell($width = 5, $height = 4, $text = '２.', $border = 0, $align = 'L', $fill = false, $ln = '', $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = 4, $valign = 'T');
$pdf->MultiCell($width = 175, $height = 4, $text = 'この様式の大きさは日本工業規格Ａ４とすること。', $border = 0, $align = 'L', $fill = false, $ln = '', $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = 4, $valign = 'T');


//=========== Custom clearence type 3 ===========
$pdf->AddPage();

$pdf->SetFillColor(255,255,255);
//$pdf->MultiCell($width = 80, $height = 60, $text = 'Main column', $border = 1, $align = 'L', $fill=true, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = 60, $valign = 'T');

// header title
$pdf->SetFont($language, '', 12);
$pdf->Cell($pageWidth, 10, '〔別紙参考様式５〕', 0, false, 'L');
$pdf->ln();

$pdf->SetFont($language, '', 14);
$pdf->Cell($pageWidth, 10, '必要理由書', 0, false, 'C');
$pdf->ln();

$pdf->SetFont($language, '', $fontSize);
$pdf->MultiCell($width = $pageWidth, $height = 10, $text = $date_n, $border = 0, $align = 'R', $fill=true, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = 10, $valign = 'M');
$pdf->ln();

$pdf->SetFont($language, '', $fontSize);
$pdf->Cell($pageWidth, 10, '厚生労働大臣　殿', 0, false, 'L');
$pdf->ln();

$pdf->SetFont($language, '', $fontSize);
$cc3PersonInChargeNameLabelWidth = 90;
$pdf->MultiCell($width = $cc3PersonInChargeNameLabelWidth, $height = 10, $text = '輸入者名：' . $personInCharge, $border = 0, $align = 'R', $fill=true, $ln = 0, $x = $pageWidth + $page_padding_left - $cc3PersonInChargeNameLabelWidth  - 7, $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = 10, $valign = 'M');
//$pdf->MultiCell($width = 30, $height = 10, $text = '輸入者名', $border = 0, $align = 'L', $fill=true, $ln = 0, $x = 145, $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = 10, $valign = 'M');
$arrContextOptions=array(
    'ssl' => array(
        'verify_peer' => false,
        'verify_peer_name' => false,
    ),
);
$path = $this->Url->build('/', true) .'img/u_plus_329e.png';
$icon_u_plus_329e = file_get_contents($path, false, stream_context_create($arrContextOptions));
$pdf->Image('@' . $icon_u_plus_329e, $pageWidth + 15, 68, 4, '', 'PNG', '', '', false, '', '', false, false, '', false, false, false);
$pdf->ln();

$pdf->SetFont($language, '', $fontSize);
$pdf->MultiCell($width = $pageWidth, $height = 10, $text = '１． 治療上必要な理由', $border = 0, $align = 'L', $fill=true, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = '', $valign = 'T');
$pdf->ln();

// list product
if (!empty($pdf_data->sale_details)) {
    $i = 0;
    foreach ($pdf_data->sale_details as $sale) {
        $products = $this->Comment->POProductDetailName($sale);
        $quantities = $sale->quantity;

        $pdf->SetTextColor(0,0,0);
        $pdf->SetFont($language, '', 11);
        $pdf->MultiCell($width = 110, $height = 5, $text = $products, $border = 0, $align = 'L', $fill=true, $ln = 0, $x = 25, $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = 5, $valign = 'M');
        $pdf->MultiCell($width = 40, $height = 5, $text = $quantities.' 箱', $border = 0, $align = 'C', $fill=true, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = 5, $valign = 'M');
        $pdf->ln();

        $i++;
        if ($i == 6) {
            break;
        }
    }
    $pdf->ln();
}

$pdf->SetTextColor(0,0,0);
$pdf->SetFont($language, '', $fontSize);
$text = '今回輸入する当該製品はヨーロッパでは既に医療機器として承認されており、既に米国および'. "\n";
$text .= '欧州において市販されており、ＣＥを取得済みである。欧州における臨床試験成績は数多く'. "\n";
$text .= '公表されており、これらの報告から改善効果は満足できるものであると共に安全性が高いこと'. "\n";
$text .= 'が判断できる。'. "\n";
$text .= 'また、改善効果の持続性はコラーゲン製剤よりも長く、患者への侵襲回数を減じることが可能'. "\n";
$text .= 'である。'. "\n";
$text .= '顔面の変形や窪みの修復をその用途とし当院では輸入している。'. "\n";
$text .= '臨床的安全性・有効性、または効果、患者への負担を配慮するため、とても適していると考え'. "\n";
$text .= 'られる。'. "\n";
$text .= '治療上緊急性があり、国内に代替品が流通していない。'. "\n";
$text .= 'これらの理由から輸入したい。';
$pdf->MultiCell($width = $pageWidth, $height = 20, $text = $text, $border = 0, $align = 'L', $fill=true, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = '', $valign = 'T');
$pdf->ln();

$pdf->SetFont($language, '', $fontSize);
$text = '今回輸入する当該製品はヨーロッパでは既に医療機器として承認されており、既に米国および'. "\n";
$text .= '欧州において市販されており、ＣＥを取得済みである。欧州における臨床試験成績は数多く'. "\n";
$text .= '公表されており、これらの報告から改善効果は満足できるものであると共に安全性が高いこと'. "\n";
$text .= 'これらの理由から輸入したい。';
$pdf->MultiCell($width = $pageWidth, $height = '', $text = $text, $border = 0, $align = 'L', $fill=true, $ln = '', $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = '', $valign = 'T');
$pdf->ln();
//$pdf->ln();
$pdf->Cell(170, 6, '', 0, false, 'L');
$pdf->ln();

$text = '<div>・１名の患者に対し<span style="">約１箱（１本）</span>が必要相当量となります。</div>';
$pdf->writeHTMLCell($pageWidth, '', $x = 17, $y = '', $text, 0, 0, 0, true, 'L', true);
$pdf->ln();

if (!empty($pdf_data->sale_details)) {
    $i = 0;
    foreach ($pdf_data->sale_details as $sale) {
        $products = $this->Comment->POProductDetailName($sale); //$sale->product_name .' ('.$sale->single_unit_value.$sale->single_unit_name.' x '. $sale->pack_size_value.$sale->pack_size_name.')';
        $quantities = $sale->quantity;

        $pdf->SetTextColor(0,0,0);
        $pdf->SetFont($language, '', 10);
        $pdf->MultiCell($width = 90, $height = '', $products, $border = 0, $align = 'L', $fill=true, $ln = 0, $x = 25, $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = '', $valign = 'M');
        $pdf->MultiCell($width = 25, $height = '', $text = '待機患者：', $border = 0, $align = 'L', $fill=true, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = '', $valign = 'M');
        $pdf->MultiCell($width = 25, $height = '', $quantities.' 箱', $border = 0, $align = 'R', $fill=true, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = '', $valign = 'M');
        $pdf->ln();

        $i++;
        if ($i == 6) {
            break;
        }
    }
    $pdf->ln();
}

$pdf->SetTextColor(0,0,0);
$pdf->SetFont($language, '', $fontSize);
$pdf->MultiCell($width = $pageWidth, $height = 10, $text = '２．医師の責任', $border = 0, $align = 'L', $fill=true, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = 10, $valign = 'T');
$pdf->ln();

$pdf->SetTextColor(0,0,0);
$pdf->SetFont($language, '', $fontSize);
$text = '今回輸入する当該製品はヨーロッパで市販されているものであるが、国内では薬事法上無許可'. "\n";
$text .= 'であり、本品は医師の責任の元に使用されるもので、一切の責任は医師である私が負うもので'. "\n";
$text .= 'あります。'. "\n";
$text .= '当該商品は顔面の修復治療に使用するために輸入するものであり、販売・譲渡は一切いたしま'. "\n";
$text .= 'せん。'. "\n";
$text .= 'また、注文・支払いの方法については、間違いなく私個人で行っております。';
$pdf->MultiCell($width = $pageWidth, $height = 20, $text = $text, $border = 0, $align = 'L', $fill=true, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = '', $valign = 'T');


//=========== Custom clearence type 4.1 ===========
$pdf->AddPage();

// header title
$pdf->SetFont($language, '', 20);
$pdf->Cell($pageWidth, '', '治療同意書', 0, false, 'C');
$pdf->ln();
$pdf->ln();

$pdf->SetFont($language, '', $fontSize);
$bulletWidth = 7;
$pdf->Cell($bulletWidth, '', '１．', 0, false, 'L');
$pdf->SetTextColor(0,0,0);
$pdf->SetFont($language, '', $fontSize);
$text = '治療に使用するXeominの成分について'. "\n";
$text .= '精製されたA型ボツリヌス毒素（タンパク質）を有効成分とする薬剤です。'. "\n";
$text .= 'A型ボツリヌス毒素は、神経伝達物質であるアセチルコリンの働きを抑制します。'. "\n";
$text .= '本剤を筋肉に注射することにより、筋肉の収縮を一時的に抑制し「しわ」や多汗を改善することができます。'. "\n";
$text .= 'また、本製剤にはヒト血清アルブミンを含有しており、感染症伝播のリスクを完全に排除することは';
$text .= 'できません。';
$pdf->MultiCell($width = $pageWidth - $bulletWidth, $height = '', $text = $text, $border = 0, $align = 'L', $fill=true, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = '', $valign = 'T');

$pdf->ln();
$pdf->Cell($bulletWidth, 6, '', 0, false, 'L');
$pdf->ln();

$pdf->SetFont($language, '', $fontSize);
$pdf->Cell($bulletWidth, '', '２．', 0, false, 'L');
$pdf->SetTextColor(0,0,0);
$pdf->SetFont($language, '', $fontSize);
$text = 'Xeominによる副作用について'. "\n";
$text .= 'Xeominで治療した患者様から次のような副作用が報告されています。'. "\n";
$text .= 'その主たるものは、頭痛、眼瞼下垂（上まぶたが開きにくい状態）、そうよう感（かゆみ）などです。'. "\n";
$text .= 'この薬剤を他の適応症に使用し、因果関係が否定できない死亡例の報告があります。'. "\n";
$text .= '本剤を使用し体調に変化が現れた場合は、医師にご相談ください。';
$pdf->MultiCell($width = $pageWidth - $bulletWidth, $height = '', $text = $text, $border = 0, $align = 'L', $fill=true, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = '', $valign = 'T');

$pdf->ln();
$pdf->Cell($bulletWidth, 6, '', 0, false, 'L');
$pdf->ln();

$pdf->SetFont($language, '', $fontSize);
$pdf->Cell($bulletWidth, '', '３．', 0, false, 'L');
$pdf->SetTextColor(0,0,0);
$pdf->SetFont($language, '', $fontSize);
$text = 'Xeominによる治療を受ける際の注意点'. "\n";
$text .= '[投与前]'. "\n";
$text .= '＊妊娠の可能性のある婦人は、本薬の投与中、および最終月経２回の月経を経るまでは避妊してくだ';
$text .= 'さい。'. "\n";
$text .= '（妊娠中の投与に関する安全性は確立されていない）'. "\n";
$text .= '＊男性は、投与中および最終投与から少なくとも３か月は避妊をしてください。'. "\n";
$text .= '（精子形成期間に投与されることを避けるため）'. "\n";
$text .= '＊３か月間は避妊をすること。（精子生成過程での安全性が証明されていないため）'. "\n";
$text .= '[投与後]'. "\n";
$text .= '＊注射当日は強く揉んだり擦ったりしないでください。'. "\n";
$text .= '＊本剤の注射部位やその周辺が上がった下がったりした感じ、腫れ、まぶたが重くなった感じ、'. "\n";
$text .= '一時的な表情の変化、頭痛などを感じることがありますが、1週間から１カ月で焼失してゆきますの';
$text .= 'でご安心下さい。'. "\n";
$text .= '＊注射部位に内出血をおこすことがあります。'. "\n";
$text .= '＊脱力感、筋力低下、めまい、視力低下が現れることがあるので、自動車の運転等の危険を伴う機械の';
$text .= '操作はご注意ください。';
$pdf->MultiCell($width = $pageWidth - $bulletWidth, $height = '', $text = $text, $border = 0, $align = 'L', $fill=true, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = '', $valign = 'T');

$pdf->ln();
$pdf->Cell($bulletWidth, 12, '', 0, false, 'L');
$pdf->ln();

$pdf->SetTextColor(0,0,0);
$pdf->SetFont($language, '', $fontSize);
$pdf->Cell($bulletWidth, 7, '', 0, false, 'C');
$pdf->Cell(15, 7, '私は', 0, false, 'C');
$pdf->SetFont($language, '', 7);
if (!empty($pdf_data->customer->medical_corporation)) {
    $text = $pdf_data->customer->medical_corporation->entity_type.$pdf_data->customer->medical_corporation->$name.'　'.$pdf_data->customer->$name;
} else {
    $text = '';
}
$pdf->Cell(85, 7, $text, 0, false, 'L');

$pdf->SetFont($language, '', $fontSize);
$pdf->Cell(40, 7, $clearenceDoc ? $clearenceDoc['doctor_name'] : '', 0, false, 'C');
$pdf->Cell(30, 7, '医師により', 0, false, 'L');
$pdf->ln();

$pdf->SetTextColor(0,0,0);
$pdf->SetFont($language, '', $fontSize);
$text = '本剤　Xeominを使用した治療について口頭においての説明および説明文の交付を受け、';
$pdf->MultiCell($width = $pageWidth - $bulletWidth, $height = 10, $text = $text, $border = 0, $align = 'L', $fill=true, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = 10, $valign = 'M');
$pdf->ln();
$text = 'その内容について十分に理解しましたので、本剤　Xeominで治療を受けることに同意します。';
$pdf->MultiCell($width = $pageWidth - $bulletWidth, $height = 10, $text = $text, $border = 0, $align = 'L', $fill=true, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = 10, $valign = 'M');
$pdf->ln();

$pdf->Cell(45, 7, '', 0, false, 'R');
$pdf->Cell(100, 7, '同意日　：' . $date_n, 0, false, 'L');
//$pdf->Cell(35, 7, '平成', 0, false, 'C');
//$pdf->Cell(20, 7, '年', 0, false, 'L');
//$pdf->Cell(20, 7, '月', 0, false, 'L');
//$pdf->Cell(20, 7, '日', 0, false, 'L');
$pdf->ln();
$pdf->Cell(45, 7, '', 0, false, 'R');
$pdf->Cell(100, 7, '氏　名　：', 0, false, 'L');


//=========== Custom clearence type 4.2 ===========
$pdf->AddPage();

// header page
$pdf->SetFont($language, '', $fontSize);

$pdf->SetFillColor(255,255,255);
//$pdf->MultiCell($width = 80, $height = 60, $text = 'Main column', $border = 1, $align = 'L', $fill=true, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = 60, $valign = 'T');

// header title
$pdf->SetFont($language, '', 20);
$pdf->Cell($pageWidth, '', '同意書', 0, false, 'C');
$pdf->ln();
$pdf->ln();
$pdf->ln();

$pdf->SetFont($language, '', $fontSize);
$pdf->Cell('', '', 'この度、Dermaheal SRを使用するにあたり、', 0, false, 'L');
$pdf->ln();
if (!empty($pdf_data->customer->medical_corporation)) {
    $text = $pdf_data->customer->medical_corporation->entity_type.$pdf_data->customer->medical_corporation->$name.'　'.$pdf_data->customer->$name;
} else {
    $text = '';
}
$pdf->Cell('', '', $text, 0, false, 'L');
$pdf->ln();
$pdf->Cell('', '', $clearenceDoc ? $clearenceDoc['doctor_name'] : ''.'         医師により、本品は大腸菌にて産生させたヒト遺伝子組み換えEGF（上皮細胞', 0, false, 'L');
$pdf->ln();
$pdf->Cell('', '', '成長因子）、IGF（インスリン様成長因子）、FGF（繊維芽細胞成長因子）を主成分とする注射剤', 0, false, 'L');
$pdf->ln();
$pdf->Cell('', '', 'であり、万が一、未知の宿主である大腸菌による疾患に罹患する恐れがある旨の説明を受け納得', 0, false, 'L');
$pdf->ln();
$pdf->Cell('', '', 'いたしました。', 0, false, 'L');
$pdf->ln();
$pdf->Cell('', '', 'ここに、Dermaheal SRの治療に同意いたします。', 0, false, 'L');
$pdf->ln();
$pdf->ln();
$pdf->ln();

//$pdf->Cell(70, 7, '同意日　：', 0, false, 'R');
//$pdf->Cell(35, 7, '平成', 0, false, 'C');
//$pdf->Cell(20, 7, '年', 0, false, 'L');
//$pdf->Cell(20, 7, '月', 0, false, 'L');
//$pdf->Cell(20, 7, '日', 0, false, 'L');
//$pdf->ln();
//$pdf->Cell(70, 7, '氏　名　：', 0, false, 'R');

$pdf->Cell(50, 7, '', 0, false, 'R');
$pdf->Cell(100, 7, '同意日　： ' . $date_n, 0, false, 'L');
//$pdf->Cell(35, 7, '平成', 0, false, 'C');
//$pdf->Cell(20, 7, '年', 0, false, 'L');
//$pdf->Cell(20, 7, '月', 0, false, 'L');
//$pdf->Cell(20, 7, '日', 0, false, 'L');
$pdf->ln();
$pdf->Cell(50, 7, '', 0, false, 'R');
$pdf->Cell(100, 7, '氏　名　： ', 0, false, 'L');

$pdf->Output();
