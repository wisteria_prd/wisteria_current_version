<div class="page">
    <div class="page-content">
        <h2 class="text-center">委任状</h2>
        <div class="spacer">&nbsp;</div>
        <h3>厚生労働大臣　殿</h3>
        <div class="spacer">&nbsp;</div>
        <p><strong>【受任者】</strong></p>
        <p style="margin-left: 30px;">所在地：〒103-0025</p>
        <div style="margin-left: 60px;">
            <p>東京都中央区日本橋茅場町1-4-6　木村實業第２ビル４Ｆ</p>
            <p>TEL：03-4588-1847　　　　FAX：03-4588-1848</p>
            <p>株式会社ウィステリア 入月弘美</p>
        </div>
        <div class="spacer">&nbsp;</div>
        <p>私は上記の者を代理人と定め、医療機器および医薬品等の輸入に必要な薬監証明の取得に関する一切の権限を委任します。</p>
        <p class="text-right">昭和45年 2017年　12月　04日</p>
        <p><strong>【委任者】</strong></p>
        <div style="margin-left: 30px;">
            <table class="date-info">
                <tbody>
                    <tr>
                        <td colspan="3">所属医療機関名</td>
                        <td colspan="2" class="colon">:</td>
                    </tr>
                    <tr>
                        <td>医</td>
                        <td>師</td>
                        <td>名</td>
                        <td colspan="2" class="colon">:</td>
                    </tr>
                    <tr>
                        <td>所</td>
                        <td>在</td>
                        <td>地</td>
                        <td class="colon">:</td>
                        <td>〒500-8833<br>岐阜県岐阜市神田町9-2</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
<div class="page">
    <div class="page-content">
        <h2>〔別紙第１号様式〕</h2>
        <div class="spacer"></div>
        <h3 class="text-center">※（　医薬品　）輸入報告書</h3>
        <div class="spacer"></div>
        <p class="text-right">昭和45年 2017年　12月　04日</p>
        <h2>厚生労働大臣殿</h2>

        <table class="recipient">
            <tbody>
                <tr>
                    <td>輸入者（受取人）氏名（法人に<br>あっては名称及び代表者の氏名）</td>
                    <td class="middle-center border-btm">
                        name 1
                        <span style="float: right;">㊞</span>
                    </td>
                </tr>
                <tr>
                    <td>住所（法人にあっては主たる事務<br>所の所在地）</td>
                    <td class="middle-center border-btm">
                        〒500-8833　岐阜県岐阜市<br>神田町9-2
                    </td>
                </tr>
                <tr>
                    <td>営業所等（貨物の送付先）の名称</td>
                    <td class="middle-center border-btm"></td>
                </tr>
                <tr>
                    <td class="text-right">同所在地</td>
                    <td class="middle-center border-btm">なおみ皮フ科クリニック</td>
                </tr>
                <tr>
                    <td class="text-right">担当者名</td>
                    <td class="middle-center border-btm">name 1 電話</td>
                </tr>
                <tr>
                    <td class="text-right">Ｅメール</td>
                    <td class="middle-center border-btm">Ｅメールはありません</td>
                </tr>
            </tbody>
        </table>
        
        <table class="recipient-product">
            <thead>
                <tr>
                    <th colspan="2">品　　　　　名</th>
                    <th>数　　量 </th>
                    <th>業許可等の有無</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td colspan="2">BALANCE LIDOCAINE</td>
                    <td>60箱</td>
                    <td>□※（　　　　　　）製造販売業</td>
                </tr>
                <tr>
                    <td class="middle-center">輸入の目的</td>
                    <td colspan="3">
                        <p class="small">①治験（企業）用、②臨床試験（医師）用、③試験研究・社内見本用、④展示用、</p>
                        <p class="small">⑤個人用、<span class="circle">⑥医療従事者個人用、</span>⑦再輸入品・返送品用、⑧自家消費用、</p>
                        <p class="small">⑨その他（　　　　　　　　　　　　　　　　　　　　　　）</p>
                    </td>
                </tr>
                <tr>
                    <td class="middle-center">誓約事項</td>
                    <td colspan="3" class="small">
                        上記輸入の目的のために使用するもので、他に販売、貸与又は授与するものではありません。
                        （個人用又は医療従事者個人用の場合）厚生労働省ホームページの「個人輸入において注意すべき
                        医薬品等について」を輸入前に確認し、輸入後も随時確認します。
                        □（試験研究・社内見本用の場合）人又は人の診断の目的には使用しません。
                    </td>
                </tr>
                <tr>
                    <td class="middle-center" colspan="4">製造業者名及び国名</td>
                </tr>
                <tr>
                    <td class="middle-center" colspan="4">3gen Llc（ ） /United States of America（ 米国 ）</td>
                </tr>
                <tr>
                    <td class="middle-center">輸入年月日</td>
                    <td class="middle-center">AWB、B/L等の番号</td>
                    <td class="middle-center" colspan="2">到着空港、到着港又は蔵置場所</td>
                </tr>
                <tr>
                    <td class="middle-center">慶応 2017年　12月 04日 </td>
                    <td class="middle-center"></td>
                    <td class="middle-center" colspan="2">新木場 UPS 保税蔵置場所（1BWD7）</td>
                </tr>
                <tr>
                    <td class="middle-center">備考</td>
                    <td colspan="3">（再輸入品・返送品用の場合）再輸入・返送に至った理由及び今後の措置について記載すること。</td>
                </tr>
                <tr>
                    <td class="middle-center">
                        厚生
                        労働
                        省確
                        認欄
                    </td>
                    <td colspan="3">
                        特記事項
                        <div class="spacer">&nbsp;</div>
                        厚生労働省関東信越厚生局
                        薬事監視専門官
                        毒物劇物監視員
                        <span style="float: right;">㊞</span>
                    </td>
                </tr>
            </tbody>
        </table>
        <div class="spacer"></div>
        <table>
            <tbody>
                <tr>
                    <td class="smaller">（注）</td>
                    <td class="smaller">１ ※（　）欄には，医薬品，医薬部外品，化粧品，医療機器，体外診断用医薬品、再生医療等製品，毒物，劇物の別を記入すること。</td>
                </tr>
                <tr>
                    <td class="smaller">&nbsp;</td>
                    <td class="smaller">
                        ２ この様式の大きさは日本工業規格Ａ４とすること。
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
<div class="page">
    <div class="page-content">
        <h3>〔別紙参考様式５〕</h3>
        <div class="spacer"></div>
        <h2 class="text-center">必要理由書</h2>
        <div class="spacer"></div>
        <p class="text-right">昭和45年 2017年　12月　04日</p>
        <p>厚生労働大臣　殿</p>
        <p class="text-right">輸入者名：name 1 <span style="padding-left: 30px;">㊞</span></p>
        <h3>１． 治療上必要な理由</h3>
        <div>
            <span style="margin-left: 30px;">BALANCE LIDOCAINE</span><span style="float: right; margin-right: 60px;">60 箱</span>
        </div>
        <div class="spacer"></div>
        <p>
            今回輸入する当該製品はヨーロッパでは既に医療機器として承認されており、既に米国および
            欧州において市販されており、ＣＥを取得済みである。欧州における臨床試験成績は数多く
            公表されており、これらの報告から改善効果は満足できるものであると共に安全性が高いこと
            が判断できる。
            また、改善効果の持続性はコラーゲン製剤よりも長く、患者への侵襲回数を減じることが可能
            である。
            顔面の変形や窪みの修復をその用途とし当院では輸入している。
            臨床的安全性・有効性、または効果、患者への負担を配慮するため、とても適していると考え
            られる。
            治療上緊急性があり、国内に代替品が流通していない。
            これらの理由から輸入したい。
            今回輸入する当該製品はヨーロッパでは既に医療機器として承認されており、既に米国および
            欧州において市販されており、ＣＥを取得済みである。欧州における臨床試験成績は数多く
            公表されており、これらの報告から改善効果は満足できるものであると共に安全性が高いこと
            これらの理由から輸入したい。
        </p>
        <div class="spacer"></div>
        <p>・１名の患者に対し約１箱（１本）が必要相当量となります。</p>
        <div class="spacer"></div>
        <div>
            <span style="margin-left: 30px;">BALANCE LIDOCAINE</span><span style="float: right; margin-right: 60px;">待機患者： 60 箱</span>
        </div>
        <div class="spacer"></div>
        <h3>２．医師の責任</h3>
        <div class="spacer"></div>
        <p>
            今回輸入する当該製品はヨーロッパで市販されているものであるが、国内では薬事法上無許可
            であり、本品は医師の責任の元に使用されるもので、一切の責任は医師である私が負うもので
            あります。
            当該商品は顔面の修復治療に使用するために輸入するものであり、販売・譲渡は一切いたしま
            せん。
            また、注文・支払いの方法については、間違いなく私個人で行っております。
        </p>
    </div>
</div>
<div class="page">
    <div class="page-content">
        <h1 class="text-center">治療同意書</h1>
        <ol>
            <li>
                治療に使用するXeominの成分について
                精製されたA型ボツリヌス毒素（タンパク質）を有効成分とする薬剤です。
                A型ボツリヌス毒素は、神経伝達物質であるアセチルコリンの働きを抑制します。
                本剤を筋肉に注射することにより、筋肉の収縮を一時的に抑制し「しわ」や多汗を改善する
                ことができます。
                また、本製剤にはヒト血清アルブミンを含有しており、感染症伝播のリスクを完全に排除す
                ることはできません。
            </li>
            <li>
                Xeominによる副作用について
                Xeominで治療した患者様から次のような副作用が報告されています。
                その主たるものは、頭痛、眼瞼下垂（上まぶたが開きにくい状態）、そうよう感（かゆみ）
                などです。
                この薬剤を他の適応症に使用し、因果関係が否定できない死亡例の報告があります。
                本剤を使用し体調に変化が現れた場合は、医師にご相談ください。
            </li>
            <li>
                Xeominによる治療を受ける際の注意点
                [投与前]
                ＊妊娠の可能性のある婦人は、本薬の投与中、および最終月経２回の月経を経るまでは避妊
                してください。
                （妊娠中の投与に関する安全性は確立されていない）
                ＊男性は、投与中および最終投与から少なくとも３か月は避妊をしてください。
                （精子形成期間に投与されることを避けるため）
                ＊３か月間は避妊をすること。（精子生成過程での安全性が証明されていないため）
                [投与後]
                ＊注射当日は強く揉んだり擦ったりしないでください。
                ＊本剤の注射部位やその周辺が上がった下がったりした感じ、腫れ、まぶたが重くなった感
                じ、
                一時的な表情の変化、頭痛などを感じることがありますが、1週間から１カ月で焼失してゆ
                きますのでご安心下さい。
                ＊注射部位に内出血をおこすことがあります。
                ＊脱力感、筋力低下、めまい、視力低下が現れることがあるので、自動車の運転等の危険を
                伴う機械の操作はご注意ください。
            </li>
        </ol>
        <div class="spacer">&nbsp;</div>
        <div>
            <span style="margin-left: 30px;">私は</span><span style="float: right;">医師により</span>
        </div>
        <div class="spacer"></div>
        <p>本剤　Xeominを使用した治療について口頭においての説明および説明文の交付を受け、</p>
        <div class="spacer"></div>
        <p>その内容について十分に理解しましたので、本剤Xeominで治療を受けることに同意します。</p>
        <table class="recipient-date">
            <tbody>
                <tr>
                    <td>同意日</td>
                    <td>:</td>
                    <td>昭和45年 2017年　12月　04日</td>
                </tr>
                <tr>
                    <td>氏　名</td>
                    <td>:</td>
                    <td>&nbsp;</td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
<div class="page">
    <div class="page-content">
        <h1 class="text-center">同意書</h1>
        <div class="spacer">&nbsp;</div>
        <p>この度、Dermaheal SRを使用するにあたり、</p>
        <div class="spacer">&nbsp;</div>
        <p>
            成長因子）、IGF（インスリン様成長因子）、FGF（繊維芽細胞成長因子）を主成分とする注射剤
            であり、万が一、未知の宿主である大腸菌による疾患に罹患する恐れがある旨の説明を受け納得
            いたしました。
            ここに、Dermaheal SRの治療に同意いたします。
        </p>
        <div class="spacer">&nbsp;</div>
        <table class="recipient-date">
            <tbody>
                <tr>
                    <td>同意日</td>
                    <td>:</td>
                    <td>昭和45年 2017年　12月　04日</td>
                </tr>
                <tr>
                    <td>氏　名</td>
                    <td>:</td>
                    <td>&nbsp;</td>
                </tr>
            </tbody>
        </table>
    </div>
</div>