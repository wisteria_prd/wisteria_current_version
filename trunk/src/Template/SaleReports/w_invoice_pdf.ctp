
<?php
ini_set('memory_limit', '-1');

// create new PDF document
// $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
$pdf = new TCPDF('', 'mm', 'A4', true, 'UTF-8');
// Reset the encoding forced from tcpdf
mb_internal_encoding('UTF-8');

//$font_jp = TCPDF_FONTS::addTTFfont(WWW_ROOT.'fonts/ipag.ttf');
$font_jp = TCPDF_FONTS::addTTFfont(WWW_ROOT.'fonts/meiryo-01.ttf');

// language
if ($this->request->session()->read('tb_field') === 'en') {
    $language = 'Helvetica';
} else {
    $language = $font_jp;
}
// test language
$language = $font_jp;

ob_clean(); // cleaning the buffer before Output()

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Nick');
$pdf->SetTitle('Wisteria Invoice');
$pdf->SetSubject('Wisteria Invoice');
$pdf->SetKeywords('');

// Configure header and footer
$pdf->SetPrintHeader(false);
$pdf->SetPrintFooter(false);

// set margins
$pdf->SetMargins(13, 13, 13);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
    require_once(dirname(__FILE__).'/lang/eng.php');
    $pdf->setLanguageArray($l);
}
// set default font subsetting mode
$pdf->setFontSubsetting(true);
$pdf->SetFont('Helvetica', '', 8);

// page padding
$page_padding_top = 13;
$page_padding_left = 13;

$pageMarginX = 13;
$pageMarginY = 13;

// set margins
$pageWidth = $pdf->getPageWidth();
$pageHeight = $pdf->getPageHeight();

$pageContent = $pageWidth - ($pageMarginX + $pageMarginY);
$pdf->SetMargins($pageMarginX, $pageMarginX, $pageMarginY);


// Cover Letter Of Document
$pdf->AddPage();
//pr($saleDetails[0]['sale']);
$pdf->SetFont($language, 'B', 16);
// Medical Corporation Name + Clinic Name
$transectionType = $this->request->query('type') ? $this->request->query('type') : 'send';
$customer = !empty($sale) ? $sale->customer : [];
$customer_name = '';
if (isset($sale) && !empty($sale)) {
    $customer_name = $sale['customer_name'];
}
//pr($sale);exit;
$medicalCorporation = isset($customer['medical_corporation']['name']) ? $customer['medical_corporation']['name'] : '';
$text = $transectionType == 'resend' ? '【再送】' : '';
$text .= $medicalCorporation . '  ' . $customer_name;
$text .= '  御中';
$pdf->MultiCell($width = 150, $height = '', $text = $text, $border = 0, $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = '', $valign = 'M');
$pdf->ln();
$pdf->SetFont('Helvetica', '', 8);
$pdf->Cell($pageMarginX + 30, 4, '', 0, false, 'L');
$pdf->ln();

// Person in charge
$pdf->SetFont($language, '', 11);
$pdf->Cell($pageMarginX + 30, 6, '', 0, false, 'L');
$personIncharge = '様 ' . $sale['person_in_charge_name'];
$pdf->MultiCell($width = $pageMarginX + 150, $height = 15, $text = $personIncharge, $border = 0 ,$align = 'L', $fill=false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = 15, $valign = 'M');
$pdf->ln();

// Customer Fax
$customerFax = !empty($customer) ? $customer['fax'] : '';
$pdf->MultiCell($width = $pageMarginX + 35, $height = '', $text = 'Fax ' . $customerFax, $border = 0, $align = 'R', $fill=false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = '', $valign = 'M');
$pdf->ln();

$pdf->SetFont('Helvetica', '', 8);
$pdf->Cell($pageMarginX + 30, 9, '', 0, false, 'L');
$pdf->ln();

$pdf->SetFont($language, '', 10);
$pdf->Cell(35, '', '送信枚数（本頁含）： ', 0, false, 'L');
$taxInvNumber = isset($sale['sale_tax_invoices'][0]['tax_invoice_number']) ? $sale['sale_tax_invoices'][0]['tax_invoice_number'] : '';
$page = $this->request->query('page') ? $this->request->query('page') : $taxInvNumber;
$pdf->Cell(50, '', $page, 0, false, 'L');
// Right blog
$positionX = $pageWidth - ($pageMarginX + 35 + 50);
//$pdf->MultiCell($width = 90, $height = 6, $text = '', $border = 1, $align = 'L', $fill = false, $ln = 0, $x = $positionX, $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = '', $valign = 'M');
$pdf->ln();
$pdf->MultiCell($width = 90, $height = 6, $text = '株式会社 ウィステリア', $border = 0, $align = 'L', $fill = false, $ln = 0, $x = $positionX, $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = '', $valign = 'M');
$pdf->ln();
$pdf->MultiCell($width = 90, $height = 6, $text = '〒103-0025', $border = 0, $align = 'L', $fill = false, $ln = 0, $x = $positionX, $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = '', $valign = 'M');
$pdf->ln();
$pdf->MultiCell($width = 90, $height = 12, $text = '東京都中央区日本橋茅場町1-4-6' . "\n" . '木村實業第２ビル４Ｆ', $border = 0, $align = 'L', $fill=false, $ln = 0, $x = $positionX, $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = 12, $valign = 'M');
$pdf->ln();
$pdf->MultiCell($width = 90, $height = 6, $text = '電話：03-4588-1847  FAX：03-4588-1848', $border = 0, $align = 'L', $fill = false, $ln = 0, $x = $positionX, $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = '', $valign = 'M');
$pdf->ln();

// Wisteria Logo
$arrContextOptions=array(
    'ssl' => array(
        'verify_peer' => false,
        'verify_peer_name' => false,
    ),
);
$path = $this->Url->build('/', true) .'img/wisteria_logo.png';
$data = file_get_contents($path, false, stream_context_create($arrContextOptions));
$pdf->Image('@' . $data, 170, 60, 25, '', 'PNG', '', 'T', false, '', '', false, false, 0, false, false, false);
$pdf->ln();
$pdf->ln();
$pdf->ln();
$pdf->ln();
$pdf->ln();

// Invoice information
//$nn = $pdf->getPageWidth();
//$pdf->Cell(35, '', $nn, 0, false, 'L');
//$pdf->ln();

$pdf->ln();
$pdf->SetFont($language, '', 16);
$pdf->MultiCell($width = $pageContent, $height = '', $text = '請求書送付のご案内', $border = 0, $align = 'C', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = '', $valign = 'M');
$pdf->ln();
$pdf->SetFont($language, 'B', 10);
$pdf->MultiCell($width = $pageContent, $height = '', $text = '（必ずお読みください）', $border = 0, $align = 'C', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = '', $valign = 'M');
$pdf->ln();

$pdf->Cell($pageContent, 6, '', 0, false, 'L');
$pdf->ln();

$bulletSpace = 10;
$pdf->Cell($bulletSpace, 6, '', 0, false, 'L');
$pdf->MultiCell($width = $pageContent - $bulletSpace, $height = '', $text = '平素より大変お世話になり、厚くお礼申し上げます。', $border = 0, $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = '', $valign = 'M');
$pdf->ln();
$pdf->Cell($bulletSpace, 5, '', 0, false, 'L');
$pdf->ln();

$pdf->Cell($bulletSpace, 6, '', 0, false, 'L');
$pdf->MultiCell($width = $pageContent - $bulletSpace, $height = '', $text = 'この度は、ご注文を頂きまして誠にありがとうございます。', $border = 0, $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = '', $valign = 'M');
$pdf->ln();
$pdf->Cell($bulletSpace, 6, '', 0, false, 'L');
$pdf->MultiCell($width = $pageContent - $bulletSpace, $height = '', $text = 'ご請求書を送らせて頂きますので、ご査収ください。', $border = 0, $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = '', $valign = 'M');
$pdf->ln();

$pdf->Cell($bulletSpace, 5, '', 0, false, 'L');
$pdf->ln();

$pdf->SetFont($language, '', 10);
$pdf->Cell($bulletSpace, 6, '１． ', 0, false, 'C');
$text = '請求書内容をご確認いただき、記載されております金額を指定口座へお振込ください。' . "\n";
$text .= 'お振込手数料は、お客様のご負担にてお願いいたします。';
$pdf->MultiCell($width = $pageContent - $bulletSpace, $height = '', $text = $text, $border = 0, $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = '', $valign = 'M');
$pdf->ln();

$pdf->Cell($bulletSpace, 6, '２． ', 0, false, 'C');
$pdf->SetFont($language, 'UB', 10);
$text = 'ご入金確認後、薬監申請処理を進めさせていただきます。' . "\n";
$text .= '尚、本品は医療機器の為、キャンセルはお受けできません。予めご了承ください。';
$pdf->MultiCell($width = $pageContent - $bulletSpace, $height = '', $text = $text, $border = 0, $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = '', $valign = 'M');
$pdf->ln();

$pdf->SetFont($language, '', 10);
$pdf->Cell($bulletSpace, 6, '３． ', 0, false, 'C');
$text = '医師個人輸入に伴い、「薬監証明書」を取得する為の手続きが必要です。「薬監証明取得」の' . "\n";
if (isset($customer['type'])) {
    if ($customer['type'] == CUSTOMER_TYPE_NEW && $transectionType == 'resend') {
        $text .= '手続きは弊社にて代行いたします。初めてご注文の際は、医師免許証のコピーをＦＡＸください';
        $text .= 'ますよう、お願いいたします。';
    } else {
        $text .= '手続きは弊社にて代行いたします。';
    }
}
$pdf->MultiCell($width = $pageContent - $bulletSpace, $height = '', $text = $text, $border = 0, $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = '', $valign = 'M');
$pdf->ln();

$pdf->SetFont($language, '', 10);
$pdf->Cell($bulletSpace, 6, '４． ', 0, false, 'C');
$text = '商品到着時に、輸入消費税(6.3％)・地方消費税(1.7％)およびハンドリング' . "\n";
$text .= 'フィーを配送業者（TNTｴｸｽﾌﾟﾚｽ or UPS or FedEX）へ別途現金にてお支払いください。';
$pdf->MultiCell($width = $pageContent - $bulletSpace, $height = '', $text = $text, $border = 0, $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = '', $valign = 'M');
$pdf->ln();

$pdf->SetFont($language, '', 10);
$pdf->Cell($bulletSpace, 6, '５． ', 0, false, 'C');
$text = '万一、ご注文と異なる商品が届きました場合には、直ちに全額返金もしくは再送手続きをさせて' . "\n";
$text .= 'いただきます。';
$pdf->MultiCell($width = $pageContent - $bulletSpace, $height = '', $text = $text, $border = 0, $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = '', $valign = 'M');
$pdf->ln();

$pdf->Cell($bulletSpace, 6, '６． ', 0, false, 'C');
$pdf->SetFont($language, 'BU', 10);
$text = '海外からの輸入となる為、日数に余裕をもってご注文ください。';
$pdf->MultiCell($width = $pageContent - $bulletSpace, $height = '', $text = $text, $border = 0, $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = '', $valign = 'M');
$pdf->ln();

$pdf->SetFont($language, '', 10);
$pdf->Cell($bulletSpace, 6, '', 0, false, 'C');
$text = '（年末年始や連休前後は、厚生局・通関業者が混み合い、納期に大幅の遅れが予想されます。' . "\n";
$text .= '在庫をご確認の上、お早めにご注文を頂ければ幸いに存じます）';
$pdf->MultiCell($width = $pageContent - $bulletSpace, $height = '', $text = $text, $border = 0, $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = '', $valign = 'M');
$pdf->ln();
$pdf->ln();

if (isset($customer['type'])) {
    if ($customer['type'] == CUSTOMER_TYPE_NEW && $transectionType == 'resend') {
        $pdf->SetFont($language, 'BU', 10);
        $doctorName = !empty($customer['clinic_doctors']) ? $customer['clinic_doctors'][0]['first_name'] . ' ' . $customer['clinic_doctors'][0]['first_name'] : '';
        $text = '★★★　ご入金を確認後、弊社にて薬監取得手続きをさせて頂きますので、お手数ですが' . "\n";
        $text .= $doctorName . ' 先生の医師免許証のコピーを弊社へＦＡＸしてくださいますようお願い致します。';
        $pdf->MultiCell($width = $pageContent, $height = '', $text = $text, $border = 0, $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = '', $valign = 'M');
        $pdf->ln();
        $pdf->ln();
    }
}

if ($saleDetails) {
    $is_product_special_type = 0;
    foreach ($saleDetails as $value) {
        if ($value['product_name'] == 'Xeomin' || $value['product_name'] == 'Dermaheal SR') {
            $pdf->SetFont($language, '', 10);
            $text = '薬監申請に必要となります同意書を送信いたしますので、患者様●名様のご署名をいただいて、' . "\n";
            $text .= '弊社へファクスにてご返信いただければ幸いです。' . "\n";
            $text .= '医師個人輸入の為、同意書の氏名は医師名では厚生局より許可がおりません。ご注意ください。';
            $pdf->MultiCell($width = $pageContent, $height = '', $text = $text, $border = 1, $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = '', $valign = 'M');
            $pdf->ln();
            $pdf->ln();
        }
    }
}
//pr($saleDetails);exit;

// footer text identifier
$pdf->SetFont($language, '', 10);
$text = 'ご不明な点は弊社、カスタマーサービスまでお問い合わせください。' . "\n";
$text .= '今後ともどうぞよろしくお願い申し上げます。';
$pdf->MultiCell($width = $pageContent, $height = '', $text = $text, $border = 0, $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = '', $valign = 'M');
$pdf->ln();




$pdf->AddPage();

$pdf->SetFillColor(255,255,255);
// Header title of document
$pdf->SetFont($language, 'B', 20);
$pdf->MultiCell($width = 90, $height = 25, $text = '請求書', $border = 0, $align = 'L', $fill=false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = 15, $valign = 'M');
$pdf->ln();

// MediPro Logo
$arrContextOptions=array(
    'ssl' => array(
        'verify_peer' => false,
        'verify_peer_name' => false,
    ),
);
$path = $this->Url->build('/', true) .'img/wisteria_logo.png';
$data = file_get_contents($path, false, stream_context_create($arrContextOptions));
$pdf->Image('@' . $data, 150, $page_padding_top, 20, '', 'PNG', '', 'T', false, '', '', false, false, 0, false, false, false);
$pdf->ln();

$issueDate = '';
$invNumber = '';
if (!empty($sale['sale_tax_invoices'])) {
    $issueDate = $sale['sale_tax_invoices'][0]['issue_date'];
    $issueDate = $issueDate->i18nFormat('dd/MM/yyyy');
    $invNumber = $sale['sale_tax_invoices'][0]['tax_invoice_number'];
}

$pdf->SetFont($language, '', 11);
$date = '発行日         ' . $issueDate . "\n";
$date .= '請求書番号   ' . $invNumber;
$pdf->MultiCell($width = 80, $height = 20, $text = $date, $border = 0, $align = 'L', $fill=false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = 20, $valign = 'T');

$pdf->SetFont($language, 'B', 9);
$address = '株式会社ウィステリア' . "\n";
$address .= '〒103-0025東京都中央区日本橋茅場町1-4-6' . "\n";
$address .= '木村實業第2ビル4F' . "\n";
$address .= '電話：03-4588-1847  FAX：03-4588-1848';
//$pdf->MultiCell($width = 85, $height = '', $text = '', $border = 0, $align = 'C', $fill=true, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = '', $valign = 'M');
$pdf->MultiCell($width = 110, $height = '', $text = $address, $border = 0, $align = 'L', $fill=false, $ln = 0, $x = 126, $y = $page_padding_top + 20, $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = '', $valign = 'M');
$pdf->ln();
$pdf->Cell('', 6, '', 0, false, 'L');
$pdf->ln();

$html = '<hr>';
$pdf->writeHTMLCell(0, 0, '', '', $html, '', 0, 0, true, 'L', true);

$pdf->Cell(15, 6, '', 0, false, 'L');
$pdf->ln();

// Clinic customer
$pdf->Cell(10, 6, '', 0, false, 'L');
$pdf->SetFont($language, '', 9);
$customer = $sale->customer;
$customer_tel = 'Telはありません';
if (isset($customer['info_details']) && !empty($customer['info_details'][0]['tel'])) {
    $customer_tel = $customer['info_details'][0]['tel'];
}

$clinic = $customer['name'] . "\n";
$clinic .= $customer['building'] . ', ' . $customer['street'] . ', ' . $customer['city'];
$clinic .= $customer['prefecture'] . ', ' . $customer['country'] . ', ' . $customer['postal_code'];
$clinic .= "\n";
$clinic .= '電話：'.$customer_tel.'  FAX：' . $customer['fax'];
$pdf->MultiCell($width = 75, $height = '', $text = $clinic, $border = 0, $align = 'L', $fill=false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = '', $valign = 'B');
$dealer = '株式会社ウィステリア' . "\n";
$dealer .= '    〒103-0025東京都中央区日本橋茅場町1-4-6' . "\n";
$dealer .= '    木村實業第2ビル4F' . "\n";
$dealer .= '    電話：03-4588-1847  FAX：03-4588-1848' . "\n";
$dealer .= '＊＊株式会社ウィステリアは、お客様の輸入代行会社です＊＊';
$pdf->MultiCell($width = 105, $height = '', $text = '', $border = 0, $align = 'L', $fill=false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = '', $valign = 'B');
$pdf->ln();
$pdf->ln();
//$pdf->ln();
$pdf->Cell($pdf->GetY(), 5, '', 0, false, 'L');
$pdf->ln();

$pdf->MultiCell($width = 190, $height = '', $text = '(日本円)', $border = 0, $align = 'R', $fill=false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = '', $valign = 'M');
$pdf->ln();

// Products Details
//$pdf->SetFillColor(120,120,120);
$pdf->SetLineStyle(array('width' => 0.1, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(115, 115, 115)));
$pdf->SetFont('Helvetica', 'B', 11);
$fill = '#F4F4F4';
$pdf->Cell(10, 10, 'No.', 1, false, 'C', $fill);
$pdf->SetFont($language, 'B', 10);
$pdf->Cell(105, 10, '詳細', 1, false, 'C', $fill);
$pdf->Cell(30, 10, '単価', 1, false, 'C', $fill);
$pdf->Cell(15, 10, '数', 1, false, 'C', $fill);
$pdf->Cell(30, 10, '金額', 1, false, 'C', $fill);
$pdf->ln();

//$pdf->SetFillColor(255,255,255);
$pdf->SetFont($language, '', 9);
//$pdf->SetFont('Helvetica', '', 9);

// PDF Content
$numRow = 0;
$totalAmount = 0;
$totalPrice = 0;
$countRecords = count($saleDetails);

if ($countRecords <= 20) {
    $products = '';

    $cellHeight = 6;
    // foreach saleDetails
    foreach ($saleDetails as $key => $value) {
        $numRow += 1;
        $childs = $value->children;
        $saleStockDetails = $value->sale_stock_details;
        //$productDetailName = $value->pack_size_value . ' ' . $value->pack_size_name . ' T ' . $value->single_unit_value . ' ' . $value->single_unit_name;
        //$productName = $value->product_name . ' ' . $productDetailName;
        $productName = $this->Comment->POProductDetailName($value);
        $unitPrice = $this->Comment->numberFormat($value->unit_price) . ' ' . $sale->currency->code_jp;

        $pdf->MultiCell($width = 10, $height = $cellHeight, $text = $numRow, $border = 1, $align = 'C', $fill=false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = $cellHeight, $valign = 'M');
        $pdf->MultiCell($width = 105, $height = $cellHeight, $text = $productName, $border = 1, $align = 'L', $fill=false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = $cellHeight, $valign = 'M');
        $pdf->MultiCell($width = 30, $height = $cellHeight, $text = $unitPrice, $border = 1, $align = 'R', $fill=false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = $cellHeight, $valign = 'M');
        $pdf->MultiCell($width = 15, $height = $cellHeight, $text = $value->quantity, $border = 1, $align = 'R', $fill=false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = $cellHeight, $valign = 'M');
        $subTotal = $value->unit_price * $value->quantity;
        $totalPrice += $subTotal;
        $pdf->MultiCell($width = 30, $height = $cellHeight, $text = $this->Comment->numberFormat($subTotal) . ' ' . $sale->currency->code_jp, $border = 1, $align = 'R', $fill=false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = $cellHeight, $valign = 'M');
        $pdf->ln();

        // display sale stock detail
        if ($saleStockDetails) {
            foreach ($saleStockDetails as $key1 => $value1) {
                $numRow += 1;
                //$importDetail = '  ' . $value->product_name . ' ( ' . $productDetailName . ' ) ( ' . $value1->stock->import_detail->lot_number . ', Exp: ' .
                //    date('M-Y', strtotime($value1->stock->import_detail->expiry_date)) . ' ) x ' . $value1->quantity;
                $productDetailName = '  ' . $productName;
                $productDetailName .= ' ( ' . $value1->stock->import_detail->lot_number;
                $productDetailName .= ', Exp: ';
                $productDetailName .= date('M-Y', strtotime($value1->stock->import_detail->expiry_date)) . ' ) ';
                $productDetailName .= ' x ' . $value1->quantity;

                $pdf->Cell(10, 6, $numRow, 1, false, 'C');
                $pdf->Cell(105, 6, $productDetailName , 1, false, 'L');
                $pdf->Cell(30, 6, '', 1, false, 'R');
                $pdf->Cell(15, 6, '', 1, false, 'R');
                $pdf->Cell(30, 6, '', 1, false, 'R');
                $pdf->ln();
            }
        }

        // display child
        if ($childs) {
            foreach ($childs as $key2 => $value2) {
                $numRow += 1;
                $saleStockDetails2 = $value2->sale_stock_details;
                $productDetailName2 = $value2->pack_size_value . ' ' . $value2->pack_size_name . ' X ' . $value2->single_unit_value . ' ' . $value2->single_unit_name;
                $productName2 = '    ' . $value2->product_name . ' ' . $productDetailName2;

                $pdf->Cell(10, 6, $numRow, 1, false, 'C');
                $pdf->Cell(105, 6, $productName2 , 1, false, 'L');
                $pdf->Cell(30, 6, '', 1, false, 'R');
                $pdf->Cell(15, 6, $value->quantity, 1, false, 'R');
                $subTotal = $value2->unit_price * $value2->quantity;
                $totalPrice += $subTotal;
                $pdf->Cell(30, 6, $this->Comment->numberFormat($subTotal) . ' ' . $sale->currency->code_jp, 1, false, 'R');
                $pdf->ln();
            }

            // display sale stock detail for child node
            if ($saleStockDetails) {
                foreach ($saleStockDetails as $key3 => $value3) {
                    $numRow += 1;
                    $importDetail = '      ' . $value2->product_name . ' ( ' . $productDetailName2 . ' ) ( ' . $value3->stock->import_detail->lot_number . ', Exp: ' .
                        date('M-Y', strtotime($value3->stock->import_detail->expiry_date)) . ' ) x ' . $value3->quantity;

                    $pdf->MultiCell($width = 10, $height = 6, $text = $numRow, $border = 1, $align = 'C', $fill=false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = '', $valign = 'M');
                    $pdf->MultiCell($width = 105, $height = 6, $text = $importDetail, $border = 1, $align = 'L', $fill=false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = '', $valign = 'M');
                    $pdf->MultiCell($width = 30, $height = 6, $text = '', $border = 1, $align = 'R', $fill=false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = '', $valign = 'M');
                    $pdf->MultiCell($width = 15, $height = 6, $text = '', $border = 1, $align = 'R', $fill=false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = '', $valign = 'M');
                    $pdf->MultiCell($width = 30, $height = 6, $text = '', $border = 1, $align = 'R', $fill=false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = '', $valign = 'M');
                    $pdf->ln();
                }
            }
        }
    }
}

while ($numRow < 10) {
    $pdf->Cell(10, 6, $numRow + 1, 1, false, 'C');
    $pdf->Cell(105, 6, '', 1, false, 'C');
    $pdf->Cell(30, 6, '', 1, false, 'C');
    $pdf->Cell(15, 6, '', 1, false, 'C');
    $pdf->Cell(30, 6, '', 1, false, 'C');
    $pdf->ln();
    $numRow++;
}

$pdf->SetFont($language, 'B', 10);
$pdf->MultiCell($width = 160, $height = 6, $text = ' a. 輸入課税対象額', $border = 1, $align = 'R', $fill=false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = 6, $valign = 'M');
$pdf->MultiCell($width = 30, $height = 6, $text = $this->Comment->numberFormat($totalPrice) . '円', $border = 1, $align = 'R', $fill=false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = 6, $valign = 'M');
$pdf->ln();
$pdf->MultiCell($width = 160, $height = 6, $text = ' b. 輸入消費税', $border = 1, $align = 'R', $fill=false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = 6, $valign = 'M');
$consumptionTax = ($totalPrice * 6.3) / 100;
$pdf->MultiCell($width = 30, $height = 6, $text = $this->Comment->numberFormat($consumptionTax) . '円', $border = 1, $align = 'R', $fill=false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = 6, $valign = 'M');
$pdf->ln();
$pdf->MultiCell($width = 160, $height = 6, $text = ' c. 地方消費税', $border = 1, $align = 'R', $fill=false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = 6, $valign = 'M');
$localConsumptionTax = ($consumptionTax * 6.3) / 100;
$pdf->MultiCell($width = 30, $height = 6, $text = $this->Comment->numberFormat($localConsumptionTax) . '円', $border = 1, $align = 'R', $fill=false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = 6, $valign = 'M');
$pdf->ln();
$pdf->MultiCell($width = 160, $height = 6, $text = ' d. 小計（a + b + c）', $border = 1, $align = 'R', $fill=false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = 6, $valign = 'M');
$colD = $totalPrice + $consumptionTax + $localConsumptionTax;
$pdf->MultiCell($width = 30, $height = 6, $text = $text = $this->Comment->numberFormat($colD) . '円', $border = 1, $align = 'R', $fill=false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = 6, $valign = 'M');
$pdf->ln();
$pdf->MultiCell($width = 160, $height = 6, $text = ' e. 通関業者国内手数料', $border = 1, $align = 'R', $fill=false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = 6, $valign = 'M');
$brokerDomesticFee = isset($sale['domestic_courier_service_charge']) ? $sale['domestic_courier_service_charge'] : 0;
$pdf->MultiCell($width = 30, $height = 6, $text = $this->Comment->numberFormat($brokerDomesticFee) . '円', $border = 1, $align = 'R', $fill=false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = 6, $valign = 'M');
$pdf->ln();
$pdf->MultiCell($width = 160, $height = 6, $text = ' f. 消費税（e x 8%）', $border = 1, $align = 'R', $fill=false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = 6, $valign = 'M');
$brokerDomesticFeeTax = ($brokerDomesticFee * 8) / 100;
$pdf->MultiCell($width = 30, $height = 6, $text = $text = $this->Comment->numberFormat($brokerDomesticFeeTax) . '円', $border = 1, $align = 'R', $fill=false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = 6, $valign = 'M');
$pdf->ln();
$pdf->MultiCell($width = 160, $height = 6, $text = ' g. 小計（e+f）', $border = 1, $align = 'R', $fill=false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = 6, $valign = 'M');
$colG = $brokerDomesticFee + $brokerDomesticFeeTax;
$pdf->MultiCell($width = 30, $height = 6, $text = $this->Comment->numberFormat($colG) . '円', $border = 1, $align = 'R', $fill=false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = 6, $valign = 'M');
$pdf->ln();
$pdf->MultiCell($width = 160, $height = 6, $text = ' h. ハンドリングフィー', $border = 1, $align = 'R', $fill=false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = 6, $valign = 'M');
$handlingFee = isset($sale['handling_fee']) ? $sale['handling_fee'] : 0;
$pdf->MultiCell($width = 30, $height = 6, $text = $this->Comment->numberFormat($handlingFee) . '円', $border = 1, $align = 'R', $fill=true, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = 6, $valign = 'M');
$pdf->ln();
$pdf->MultiCell($width = 160, $height = 6, $text = ' i. 消費税（h x 8%）', $border = 1, $align = 'R', $fill=false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = 6, $valign = 'M');
$handlingFeeTax = ($handlingFee * 8) / 100;
$pdf->MultiCell($width = 30, $height = 6, $text = $text = $this->Comment->numberFormat($handlingFeeTax) . '円', $border = 1, $align = 'R', $fill=false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = 6, $valign = 'M');
$pdf->ln();
$pdf->MultiCell($width = 160, $height = 6, $text = ' j. 小計（h + i）', $border = 1, $align = 'R', $fill=false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = 6, $valign = 'M');
$colJ = $handlingFee + $handlingFeeTax;
$pdf->MultiCell($width = 30, $height = 6, $text = $text = $this->Comment->numberFormat($colJ) . '円', $border = 1, $align = 'R', $fill=false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = 6, $valign = 'M');
$pdf->ln();
$pdf->MultiCell($width = 160, $height = 6, $text = ' k. 特別値引き', $border = 1, $align = 'R', $fill=true, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = 6, $valign = 'M');
$colK = isset($sale['special_discount']) ? $sale['special_discount'] : 0;
$pdf->MultiCell($width = 30, $height = 6, $text = $text = $this->Comment->numberFormat($colK) . '円', $border = 1, $align = 'R', $fill=true, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = 6, $valign = 'M');
$pdf->ln();
$pdf->MultiCell($width = 160, $height = 15, $text = ' l. ご請求額（d + g + j + k）', $border = 1, $align = 'R', $fill=false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = 15, $valign = 'M');
$netTotal = $colD + $colG + $colJ - $colK;
$pdf->MultiCell($width = 30, $height = 15, $text = $this->Comment->numberFormat($netTotal) . '円', $border = 1, $align = 'R', $fill=false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = 15, $valign = 'M');

$pdf->ln();
$pdf->Cell(100, 1, '', 0, false, 'C');
$pdf->ln();

$pdf->MultiCell($width = 90, $height = 35, $text = '備考：', $border = 0, $align = 'L', $fill = false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = 35, $valign = 'T');

$pdf->MultiCell($width = 90, $height = 6, $text = 'お振込先口座：', $border = 0, $align = 'L', $fill=false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = 6, $valign = 'T');
$pdf->ln();
$pdf->SetFont($language, 'B', 9);
$pdf->MultiCell($width = 100, $height = 8, $text = '   Medical Professionals Pte. Ltd.への送金代行用口座です。', $border = 0, $align = 'L', $fill=true, $ln = 0, $x = 103, $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = 6, $valign = 'T');
$pdf->ln();
$pdf->SetFont($language, 'B', 10);
$text = '  銀行名       三菱東京UFJ銀行' . "\n";
$text .= '  支店名       青山通支店（084）' . "\n";
$text .= '  口座番号     普通　0090906' . "\n";
$text .= '  口座名義     株式会社ウィステリア';
$pdf->MultiCell($width = 90, $height = '', $text = $text, $border = 0, $align = 'L', $fill=true, $ln = 0, $x = 103, $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = '', $valign = 'T');

$pdf->Output();
