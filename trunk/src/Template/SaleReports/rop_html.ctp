<div class="page">
    <div class="page-content">
        <h3>なおみ皮フ科クリニック 御中</h3>
        <p class="salute">様</p>
        <p class="letter-number">送信枚数（本頁含）：</p>
        <table class="table table-address">
            <tbody>
                <tr>
                    <td>
                        <h4>株式会社 ウィステリア</h4>
                    </td>
                    <td class="cell-logo" rowspan="6"><?= $this->Html->image('wisteria_logo.png', ['class' => 'logo']) ?></td>
                </tr>
                <tr>
                    <td>〒103-0025</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>東京都中央区日本橋茅場町1-4-6</td>
                </tr>
                <tr>
                    <td>木村實業第２ビル４Ｆ</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>電話：03-4588-1847</td>
                    <td>FAX：03-4588-1848</td>
                </tr>
            </tbody>
        </table>
        <div class="spacer"></div>
        <h3 class="text-center">支払依頼書送付のご案内</h3>
        <p class="small text-center">（必ずお読みください）</p>
        <div class="spacer"></div>
        <p>平素より大変お世話になり、厚くお礼申し上げます。</p>
        <div class="spacer"></div>
        <p>この度は、ご注文を頂きまして誠にありがとうございます。</p>
        <p>ご請求書を送らせて頂きますので、ご査収ください。</p>
        <div class="spacer"></div>
        <ol class="thx-content">
            <li>
                請求書内容をご確認いただき、記載されております金額を指定口座へお振込ください。
                お振込手数料は、お客様のご負担にてお願いいたします。
            </li>
            <li class="underline">
                ご入金確認後、薬監申請処理を進めさせていただきます。
                尚、本品は医療機器の為、キャンセルはお受けできません。予めご了承ください。
            </li>
            <li>
                医師個人輸入に伴い、「薬監証明書」を取得する為の手続きが必要です。「薬監証明取得」の
                手続きは弊社にて代行いたします。
            </li>
            <li>
                商品到着時に、輸入消費税(6.3％)・地方消費税(1.7％)およびハンドリング
                フィーを配送業者（TNTｴｸｽﾌﾟﾚｽ or UPS or FedEX）へ別途現金にてお支払いください。
            </li>
            <li>万一、ご注文と異なる商品が届きました場合には、直ちに全額返金もしくは再送手続きをさせていただきます。</li>
            <li>
                <span class="underline">海外からの輸入となる為、日数に余裕をもってご注文ください。</span>
                （年末年始や連休前後は、厚生局・通関業者が混み合い、納期に大幅の遅れが予想されます。
                在庫をご確認の上、お早めにご注文を頂ければ幸いに存じます）
            </li>
        </ol>
        <p>&nbsp;</p>
        <p>&nbsp;</p>
        <p>&nbsp;</p>
        <p>ご不明な点は弊社、カスタマーサービスまでお問い合わせください。</p>
        <p>今後ともどうぞよろしくお願い申し上げます。</p>
    </div>
</div>
<div class="page">
    <div class="page-content">
        <h2>支払依頼書</h2>
        <div class="container">
            <div class="row">
                <div class="col col-6">
                    <p>発行日</p>
                    <p>支払依頼書番号</p>
                </div>
                <div class="col col-6">
                    <h3>Medical Professionals Pte. Ltd.</h3>
                    <p>3791 Jalan Bukit Merah, #10-17 E-Centre@Redhill,</p>
                    <p>Singapore 159471</p>
                    <p>Tel: +65-6274-0433 / Fax: +65-6274-0477</p>
                    <p>UEN No. 201403392G</p>
                </div>
            </div>
            <div class="spacer"></div>
            <div class="row">
                <div class="col col-6">
                    <p>なおみ皮フ科クリニック</p>
                    <p>, 神田町9-2, 岐阜市岐阜県, , 500-8833</p>
                    <p>電話： FAX：058-265-0704</p>
                </div>
                <div class="col col-6">
                    <p>株式会社ウィステリア</p>
                    <p>〒103-0025東京都中央区日本橋茅場町1-4-6</p>
                    <p>木村實業第2ビル4F</p>
                    <p>電話：03-4588-1847 FAX：03-4588-1848</p>
                    <p>＊＊株式会社ウィステリアは、お客様の輸入代行会社です＊＊</p>
                </div>
            </div>
        </div>
        <p class="text-right">(日本円)</p>
        <table class="table table-data">
            <thead>
                <tr>
                    <th>No.</th>
                    <th>詳細</th>
                    <th>単価</th>
                    <th>数</th>
                    <th width="20%">金額</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td class="text-center">1</td>
                    <td>Juvederm ULTRA2 0.55ml</td>
                    <td class="text-right">17,000.00</td>
                    <td class="text-right">2</td>
                    <td class="text-right">34,000.00</td>
                </tr>
                <tr>
                    <td class="text-center">2</td>
                    <td>Juvederm ULTRA2 0.55ml ( 34Test56, Exp: Dec-2020 ) x 1</td>
                    <td class="text-right">00.00</td>
                    <td class="text-right">0</td>
                    <td class="text-right">00.00</td>
                </tr>
                <tr>
                    <td class="text-center">3</td>
                    <td>&nbsp;</td>
                    <td class="text-right">&nbsp;</td>
                    <td class="text-right">&nbsp;</td>
                    <td class="text-right">&nbsp;</td>
                </tr>
                <tr>
                    <td class="text-center">4</td>
                    <td>&nbsp;</td>
                    <td class="text-right">&nbsp;</td>
                    <td class="text-right">&nbsp;</td>
                    <td class="text-right">&nbsp;</td>
                </tr>
                <tr>
                    <td class="text-center">5</td>
                    <td>&nbsp;</td>
                    <td class="text-right">&nbsp;</td>
                    <td class="text-right">&nbsp;</td>
                    <td class="text-right">&nbsp;</td>
                </tr>
                <tr>
                    <td class="text-center">6</td>
                    <td>&nbsp;</td>
                    <td class="text-right">&nbsp;</td>
                    <td class="text-right">&nbsp;</td>
                    <td class="text-right">&nbsp;</td>
                </tr>
                <tr>
                    <td class="text-center">7</td>
                    <td>&nbsp;</td>
                    <td class="text-right">&nbsp;</td>
                    <td class="text-right">&nbsp;</td>
                    <td class="text-right">&nbsp;</td>
                </tr>
                <tr>
                    <td class="text-center">8</td>
                    <td>&nbsp;</td>
                    <td class="text-right">&nbsp;</td>
                    <td class="text-right">&nbsp;</td>
                    <td class="text-right">&nbsp;</td>
                </tr>
                <tr>
                    <td class="text-center">9</td>
                    <td>&nbsp;</td>
                    <td class="text-right">&nbsp;</td>
                    <td class="text-right">&nbsp;</td>
                    <td class="text-right">&nbsp;</td>
                </tr>
                <tr>
                    <td class="text-center">10</td>
                    <td>&nbsp;</td>
                    <td class="text-right">&nbsp;</td>
                    <td class="text-right">&nbsp;</td>
                    <td class="text-right">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="4">a. 輸入課税対象額</td>
                    <td class="text-right">34,000.00円</td>
                </tr>
                <tr>
                    <td colspan="4">b. 輸入消費税</td>
                    <td class="text-right">2,142.00円</td>
                </tr>
                <tr>
                    <td colspan="4">c. 地方消費税</td>
                    <td class="text-right">134.95円</td>
                </tr>
                <tr>
                    <td colspan="4">d. 小計（a + b + c）</td>
                    <td class="text-right">36,276.95円</td>
                </tr>
                <tr>
                    <td colspan="4">e. 通関業者国内手数料</td>
                    <td class="text-right">0.00円</td>
                </tr>
                <tr>
                    <td colspan="4">f. 消費税（e x 8%）</td>
                    <td class="text-right">0.00円</td>
                </tr>
                <tr>
                    <td colspan="4">g. 小計（e+f）</td>
                    <td class="text-right">1,000.00円</td>
                </tr>
                <tr>
                    <td colspan="4">h. ハンドリングフィー</td>
                    <td class="text-right">2,142.00円</td>
                </tr>
                <tr>
                    <td colspan="4">i. 消費税（h x 8%）</td>
                    <td class="text-right">80.00円</td>
                </tr>
                <tr>
                    <td colspan="4">j. 小計（h + i）</td>
                    <td class="text-right">1,080.00円</td>
                </tr>
                <tr>
                    <td colspan="4">k. 特別値引き</td>
                    <td class="text-right">0.00円</td>
                </tr>
                <tr>
                    <td class="total" colspan="4">l. ご請求額（d + g + j + k）</td>
                    <td class="total text-right">37,356.95円</td>
                </tr>
            </tbody>
        </table>
    </div>
</div>