
<div class="row"><div class="col-md-12"><ol class="breadcrumb"><li><a href="/wisteria/">ホーム</a></li><li class="active">進捗管理一覧</li></ol></div></div>
<div class="search-section" style="margin-bottom: 20px;">
    <?php
    $_en = $this->request->session()->read('tb_field');
    echo $this->Form->create('WShippings', [
        'role' => 'form',
        'class' => 'form-inline',
        'type' => 'get',
        'name' => 'form_search'
    ]);
    echo $this->Form->hidden('displays', [
        'value' => empty($this->request->query('displays')) ? 10 : $this->request->query('displays')
    ]);
    echo $this->Form->input('keyword', [
        'name' => 'keyword',
        'placeholder' => __('USER_SEARCH_KEYWORD'),
        'class' => 'form-control',
        'label' => false,
        'autocomplete' => 'off',
        'value' => $this->request->query('keyword')
    ]);
    echo '&nbsp;&nbsp;&nbsp;';
    echo $this->Form->button('絞り込み条件', [
        'class' => 'btn btn-primary btn-filter-search',
    ]);
    echo $this->Form->end();
    ?>
</div>
<div class="total-pagination" style="padding-bottom: 20px;">
    <section>1~10件表示48合計</section>
    <?php //echo $this->element('display_info'); ?>
</div>
<div class="row search-section" style="margin-bottom: 20px;">
    <?php echo $this->element('display_number'); ?>
    <?= $this->ActionButtons->btnRegisterNew('WShippings', []) ?>
    <div class="clearfix"></div>
</div>
<div class="data-tb-list">
    <?php //if ($data): ?>
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>#</th>
                    <th>&nbsp;</th>
                    <th class="white-space">
                        <?php echo __('TXT_SALE_STATUS'); //$this->Paginator->sort($this->Comment->getFieldName(), __('TBL_MANUFACTURER_NAME')); ?>
                        <span class="glyphicon glyphicon-triangle-bottom"></span>
                    </th>
                    <th class="white-space">
                        <?php echo __('TXT_TRANSACTION'); ?>
                        <span class="glyphicon glyphicon-triangle-bottom"></span>
                    </th>
                    <th class="white-space">
                        <?php echo __('TXT_SUPPLIER'); ?>
                        <span class="glyphicon glyphicon-triangle-bottom"></span>
                    </th>
                    <th class="white-space">
                        <?php echo __('TXT_ORDER_DATE'); ?>
                        <span class="glyphicon glyphicon-triangle-bottom"></span>
                    </th>
                    <th class="white-space">
                        <?php echo __('TXT_ORDER_NUM'); ?>
                        <span class="glyphicon glyphicon-triangle-bottom"></span>
                    </th>
                    <th class="white-space">
                        <?php echo __('TXT_DELIVER_TO'); ?>
                        <span class="glyphicon glyphicon-triangle-bottom"></span>
                    </th>
                    <th class="white-space">
                        <?php echo __('TXT_PRODUCT'); ?>
                        <span class="glyphicon glyphicon-triangle-bottom"></span>
                    </th>
                    <th class="white-space">
                        <?php echo __('TXT_AMOUNT'); ?>
                        <span class="glyphicon glyphicon-triangle-bottom"></span>
                    </th>
                    <th class="white-space">
                        <?php echo __('TXT_PAYING'); ?>
                        <span class="glyphicon glyphicon-triangle-bottom"></span>
                    </th>
                    <th class="white-space">
                        <?php echo __('TXT_USER'); ?>
                        <span class="glyphicon glyphicon-triangle-bottom"></span>
                    </th>
                    <th>&nbsp;</th>
                    <th>&nbsp;</th>
                    <th>&nbsp;</th>
                    <th>&nbsp;</th>
                    <td><button class="btn btn-primary btn-sm btn_comment" data-id="1" type="submit"><i class="fa fa-comments-o" aria-hidden="true"></i></button></td>
                    <td><a href="/wisteria/manufacturers/edit/1" class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a></td>
                    <td><button class="btn btn-suspend btn-sm btn_suspend" data-name="suspend" type="submit"><i class="fa fa-ban" aria-hidden="true"></i></button></td>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>1</td>
                    <td>&nbsp;</td>
                    <td>完了</td>
                    <td>国内</td>
                    <td>国内サプA</td>
                    <td>02/17/2018</td>
                    <td>WOF0001</td>
                    <td>ウィステリア</td>
                    <td>カニューラタ...</td>
                    <td>¥9,999,999</td>
                    <td>
                        <input type="checkbox" value="" name="" />
                    </td>
                    <td>入月</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td><button class="btn btn-primary btn-sm btn_comment" data-id="1" type="submit"><i class="fa fa-comments-o" aria-hidden="true"></i></button></td>
                    <td><a href="/wisteria/manufacturers/edit/1" class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a></td>
                    <td><button class="btn btn-suspend btn-sm btn_suspend" data-name="suspend" type="submit"><i class="fa fa-ban" aria-hidden="true"></i></button></td>
                </tr>
                <tr>
                    <td>2</td>
                    <td>&nbsp;</td>
                    <td>完了</td>
                    <td>国内</td>
                    <td>国内サプA</td>
                    <td>02/17/2018</td>
                    <td>WOF0001</td>
                    <td>ウィステリア</td>
                    <td>カニューラタ...</td>
                    <td>¥9,999,999</td>
                    <td>
                        <input type="checkbox" value="" name="" />
                    </td>
                    <td>入月</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td><button class="btn btn-primary btn-sm btn_comment" data-id="1" type="submit"><i class="fa fa-comments-o" aria-hidden="true"></i></button></td>
                    <td><a href="/wisteria/manufacturers/edit/1" class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a></td>
                    <td><button class="btn btn-suspend btn-sm btn_suspend" data-name="suspend" type="submit"><i class="fa fa-ban" aria-hidden="true"></i></button></td>
                </tr>
                <tr>
                    <td>3</td>
                    <td>&nbsp;</td>
                    <td>納品済</td>
                    <td>国内</td>
                    <td>国内サプB</td>
                    <td>02/08/2018</td>
                    <td>WOF0002</td>
                    <td>クリニックB</td>
                    <td>遠心機</td>
                    <td>€99,999.00</td>
                    <td>
                        <input type="checkbox" checked="checked" value="" name="" />
                    </td>
                    <td>入月</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td><button class="btn btn-primary btn-sm btn_comment" data-id="1" type="submit"><i class="fa fa-comments-o" aria-hidden="true"></i></button></td>
                    <td><a href="/wisteria/manufacturers/edit/1" class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a></td>
                    <td><button class="btn btn-suspend btn-sm btn_suspend" data-name="suspend" type="submit"><i class="fa fa-ban" aria-hidden="true"></i></button></td>
                </tr>
                <tr>
                    <td>4</td>
                    <td><?= $this->Html->image('uploads/icon/yen.png', ['class' => 'yen-icon']) ?></td>
                    <td>入金確認済</td>
                    <td>国内</td>
                    <td>国内サプC</td>
                    <td>02/08/2018</td>
                    <td>WOF0002</td>
                    <td>クリニックC</td>
                    <td>BELOTERO</td>
                    <td>£99,999.00</td>
                    <td>
                        <input type="checkbox" checked="checked" value="" name="" />
                    </td>
                    <td>入月</td>
                    <td>&nbsp;</td>
                    <td><?= $this->Html->image('uploads/icon/truck.png', ['class' => 'yen-icon']) ?></td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td><button class="btn btn-primary btn-sm btn_comment" data-id="1" type="submit"><i class="fa fa-comments-o" aria-hidden="true"></i></button></td>
                    <td><a href="/wisteria/manufacturers/edit/1" class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a></td>
                    <td><button class="btn btn-suspend btn-sm btn_suspend" data-name="suspend" type="submit"><i class="fa fa-ban" aria-hidden="true"></i></button></td>
                </tr>
                <tr>
                    <td>5</td>
                    <td><?= $this->Html->image('uploads/icon/yen.png', ['class' => 'yen-icon']) ?></td>
                    <td>入金確認済</td>
                    <td>国内</td>
                    <td>ウィステリア</td>
                    <td>02/08/2018</td>
                    <td>WOF0003</td>
                    <td>クリニックD</td>
                    <td>PRC-30025I...</td>
                    <td>₩9,999,999</td>
                    <td>
                        <input type="checkbox" value="" name="" />
                    </td>
                    <td>入月</td>
                    <td>&nbsp;</td>
                    <td><?= $this->Html->image('uploads/icon/box.png', ['class' => 'yen-icon']) ?></td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td><button class="btn btn-primary btn-sm btn_comment" data-id="1" type="submit"><i class="fa fa-comments-o" aria-hidden="true"></i></button></td>
                    <td><a href="/wisteria/manufacturers/edit/1" class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a></td>
                    <td><button class="btn btn-suspend btn-sm btn_suspend" data-name="suspend" type="submit"><i class="fa fa-ban" aria-hidden="true"></i></button></td>
                </tr>
                <tr>
                    <td>6</td>
                    <td><?= $this->Html->image('uploads/icon/yen.png', ['class' => 'yen-icon']) ?></td>
                    <td>入金確認済</td>
                    <td>国内</td>
                    <td>SupplierC</td>
                    <td>02/08/2018</td>
                    <td>WOF0003</td>
                    <td>SupplierC</td>
                    <td>XEOMIN</td>
                    <td>¥9,999,999</td>
                    <td>
                        <input type="checkbox" value="" name="" />
                    </td>
                    <td>藤村</td>
                    <td><?= $this->Html->image('uploads/icon/note.png', ['class' => 'yen-icon']) ?></td>
                    <td><?= $this->Html->image('uploads/icon/plan.png', ['class' => 'yen-icon']) ?></td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td><button class="btn btn-primary btn-sm btn_comment" data-id="1" type="submit"><i class="fa fa-comments-o" aria-hidden="true"></i></button></td>
                    <td><a href="/wisteria/manufacturers/edit/1" class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a></td>
                    <td><button class="btn btn-suspend btn-sm btn_suspend" data-name="suspend" type="submit"><i class="fa fa-ban" aria-hidden="true"></i></button></td>
                </tr>
            </tbody>
        </table>
    <?php //endif; ?>
</div>
<div class="row row-top-space" style="padding-bottom: 20px;">
    <?php echo $this->element('display_number'); ?>
    <?php //echo $this->element('next_prev') ?>
    <div class="col-md-9">
        <p class="page-info">1/5ページ：表示48件中10件</p>
        <nav class="pull-right">
            <ul class="pagination pagination-sm custom-pagination">
                <li class="prev disabled">
                    <a href="" onclick="return false;"><span class="glyphicon glyphicon-chevron-left"></span></a>
                </li>
                <li class="prev disabled">
                    <a href="/wisteria/manufacturers?page=1" id="prevMore" rel="prev">前の5ページ</a>
                </li>
                <li class="active"><a href="">1</a></li>
                <li><a href="#">2</a></li>
                <li><a href="#">3</a></li>
                <li><a href="#">4</a></li>
                <li><a href="#">5</a></li>
                <li class="next disabled"><a href="#" id="nextMore" rel="next">次の5ページ</a></li>
                <li class="next"><a rel="next" href="#"><span class="glyphicon glyphicon-chevron-right"></span></a></li>
                <li><a href="#"><span class="glyphicon glyphicon-chevron-right"></span><span class="glyphicon glyphicon-chevron-right"></span></a></li>
            </ul>
        </nav>
    </div>
    <div class="clearfix"></div>
</div>

<script>
    (function (e) {


        var maxPage = parseInt($('#maxPage').val());
        updatePagination(maxPage);
    })();
</script>
