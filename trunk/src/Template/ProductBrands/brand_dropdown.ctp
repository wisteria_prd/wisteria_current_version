<?php
if ($brands):
    //$name = 'name' . $en;
    $output = '<option value="">' . __('TXT_SELECT_BRAND_NAME') . '</option>';
    foreach($brands as $key => $brand) {
        $name = $this->Comment->nameEnOrJp($brand->name, $brand->name_en, $en);
        $output .= '<option value="' . $brand->id . '">' . $name . '</option>';
    }
    echo $output;
endif;