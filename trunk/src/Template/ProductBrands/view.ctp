
<table class="table table-striped tb-prbr-view" id="tb-detail">
    <tbody>
        <tr>
            <td>
                <?php
                echo __d('product_brand', 'TXT_MANUFACTURER'); ?> :
            </td>
            <td>
                <?php
                echo $this->Comment->getFieldByLocal($data->manufacturer, $locale); ?>
            </td>
        </tr>
        <tr>
            <td>
                <?php
                echo __d('product_brand', 'TXT_PRODUCT_BRAND'); ?> :
            </td>
            <td>
                <?php
                echo $this->Comment->getFieldByLocal($data, $locale); ?>
            </td>
        </tr>
        <tr>
            <td>
                <?php
                echo __d('product_brand', 'TXT_PURCHASE_AVAILABILITY'); ?> :
            </td>
            <td colspan="2">
                <?php
                echo $data->for_purchase ? __d('product_brand', 'TXT_YES') : __d('product_brand', 'TXT_NO'); ?>
            </td>
        </tr>
        <tr>
            <td>
                <?php
                echo __d('product_brand', 'TXT_SALE_AVAILABILITY'); ?> :
            </td>
            <td colspan="2">
                <?php
                echo $data->for_sale ? __d('product_brand', 'TXT_YES') : __d('product_brand', 'TXT_NO');?>
            </td>
        </tr>
        <tr>
            <td>
                <?php
                echo __d('product_brand', 'TXT_URL'); ?> :
            </td>
            <td colspan="2">
                <?php
                echo $data->url; ?>
            </td>
        </tr>
        <tr>
            <td>
                <?php
                echo __d('product_brand', 'TXT_REMARK'); ?> :
            </td>
            <td colspan="2">
                <textarea readonly="readonly" style="background: none;"><?php echo $data->remarks; ?></textarea>
            </td>
        </tr>
        <tr>
            <td>
                <?php
                echo __d('product_brand', 'TXT_REGISTERD_PRODUCT'); ?> :
            </td>
            <td colspan="2">
                <?php
                $txt_product = __('TXT_PRODUCTS');
                if ($data->count_product < 2) {
                    $txt_product = __('TXT_PRODUCT');
                }
                $url = [
                    'controller' => 'products',
                    'action' => ($data->count_product == 0) ? 'create' : 'index',
                    '?' => [
                        'filter_manufacturer' => $data->manufacturer['id'],
                        'filter_brand' => $data->id,
                        'des' => 'product-brands',
                    ]
                ];
                echo $this->Html->link($data->count_product . ' ' . $txt_product, $url, [
                    'class' => 'btn btn-primary btn-sm text-center md-footer-btn btn-sm'
                ]);
                ?>
            </td>
        </tr>
        <tr>
            <td>
                <?php
                echo __d('product_brand', 'TXT_MODIFIED'); ?> :
            </td>
            <td colspan="2">
                <?php
                echo date('Y-m-d', strtotime($data->modified)); ?>
            </tr>
        </tr>
        <tr>
            <td>
                <?php
                echo __d('product_brand', 'TXT_REGISTERED'); ?> :
            </td>
            <td colspan="2">
                <?php
                echo date('Y-m-d', strtotime($data->created)); ?>
            </tr>
        </tr>
    </tbody>
</table>
<section class="certificate-list">
    <fieldset class="fieldset-border">
        <legend>
            <?php
            echo __d('product_brand', 'TXT_DOCUMENT'); ?>
        </legend>
        <div class="fieldset-content-wrap">
            <div class="brand"></div>
            <div class="clearfix"></div>
            <div class="product"></div>
        </div>
    </fieldset>
</section>
<?php
if (isset($data->medias)) {
    $files = [];
    foreach ($data->medias as $file) {
        $files[] = $file->file_name;
    }
    $file = implode(',', $files);
    echo $this->Form->hidden('files', [
        'class' => 'media-brand',
        'value' => $file,
    ]);
}
if (isset($data->products)) {
    $media_list = [];
    foreach ($data->products as $item) {
        $medias = $item->medias;
        if ($medias) {
            foreach ($medias as $file) {
                $media_list[] = $file->file_name;
            }
        }
    }
    $file1 = implode(',', $media_list);
    echo $this->Form->hidden('media-product', [
        'class' => 'media-product',
        'value' => $file1,
    ]);
}
