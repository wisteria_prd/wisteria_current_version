
<div class="search-section" style="margin-bottom: 20px;">
    <?php
    echo $this->Form->create('ProductBrands', [
        'role' => 'form',
        'class' => 'form-inline form-search',
        'type' => 'get',
        'name' => 'form_search'
    ]);
    echo $this->Form->hidden('displays', [
        'value' => empty($this->request->query('displays')) ? 10 : $this->request->query('displays')
    ]);
    echo $this->Form->input('keyword', [
        'name' => 'keyword',
        'placeholder' => __('USER_SEARCH_KEYWORD'),
        'class' => 'form-control',
        'label' => false,
        'value' => $this->request->query('keyword'),
        'autocomplete' => 'off',
        'templates' => [
            'inputContainer' => '<div class="form-group">{{content}}</div>',
        ]
    ]);
    $this->SwitchStatus->render();
    echo $this->Form->submit('submit', [
        'style' => 'display: none',
    ]);
    echo $this->Form->end();
    ?>
</div>
<div class="total-pagination" style="padding-bottom: 20px;">
    <?php echo $this->element('display_info'); ?>
</div>
<div class="row search-section" style="margin-bottom: 20px;">
    <?php echo $this->element('display_number'); ?>
    <?= $this->ActionButtons->btnRegisterNew('ProductBrands', []) ?>
    <div class="clearfix"></div>
</div>
<div class="data-tb-list">
    <?php if ($data): ?>
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>#</th>
                    <th>&nbsp;</th>
                    <th>
                        <?php echo $this->Paginator->sort('manufacturer_id', __('PRODUCTS_TXT_MANUFACTURER')); ?>
                        <i class="fa fa-sort-desc" aria-hidden="true"></i>
                    </th>
                    <th>
                        <?php echo $this->Paginator->sort($this->Comment->getFieldName(), __('PRODUCTS_TXT_PRODUCT_BRAND')); ?>
                        <i class="fa fa-sort-desc" aria-hidden="true"></i>
                    </th>
                    <th>
                        <?php echo $this->Paginator->sort('is_purchase', __('PRODUCT_BRAND_TXT_PURCHASE_AVAILABLE')); ?>
                        <i class="fa fa-sort-desc" aria-hidden="true"></i>
                    </th>
                    <th>
                        <?php echo $this->Paginator->sort('is_sale', __('PRODUCT_BRAND_TXT_SALE_AVAILABLE')); ?>
                        <i class="fa fa-sort-desc" aria-hidden="true"></i>
                    </th>
                    <th>
                        <?php
                        echo $this->Paginator->sort('PersonInCharges.total', __('PRODUCT_BRAND_TXT_PRODUCT'));
                        ?>
                        <span><i class="fa fa-sort-desc" aria-hidden="true"></i></span>
                    </th>
                    <th width="1%">&nbsp;</th>
                    <th width="1%">&nbsp;</th>
                    <th width="1%">&nbsp;</th>
                </tr>
            </thead>
            <tbody>
                <?php $numbering = $this->Paginator->counter('{{start}}'); ?>
                <?php foreach ($data as $item) : ?>
                    <tr data-id="<?php echo h($item->id); ?>">
                        <th scope="row" class="td_evt"><?php echo $numbering; $numbering++; ?></th>
                        <td>
                            <?= $this->ActionButtons->disabledText($item->is_suspend) ?>
                        </td>
                        <td>
                            <?php echo $this->Comment->nameEnOrJp($item->manufacturer['name'], $item->manufacturer['name_en'], $locale); ?>
                        </td>
                        <td>
                            <?php echo $this->Comment->nameEnOrJp($item->name, $item->name_en, $locale); ?>
                        </td>
                        <td><?php echo $item->for_purchase; ?></td>
                        <td><?php echo $item->for_sale; ?></td>
                        <td class="td-btn-wrap">
                            <?php
                            if ($item->count_product == 0) {
                                echo $this->Html->link($item->count_product . ' ' . __('Product'), [
                                    'controller' => 'products',
                                    'action' => 'create',
                                    '?' => [
                                        'filter_manufacturer' => $item->manufacturer['id'],
                                        'filter_brand' => $item->id,
                                        'des' => TYPE_PRODUCT_BRAND,
                                    ]
                                    ], [
                                    'class' => 'btn btn-primary btn-sm',
                                ]);
                            } else {
                                $btn_name = ($item->count_product >= 2) ? __('Products') : __('Product'); //don't use key for translate here.
                                echo $this->Html->link($item->count_product . ' ' . $btn_name, [
                                    'controller' => 'products',
                                    'action' => 'index',
                                    '?' => [
                                        'filter_manufacturer' => $item->manufacturer['id'],
                                        'filter_brand' => $item->id,
                                        'des' => TYPE_PRODUCT_BRAND,
                                    ]
                                    ], [
                                    'class' => 'btn btn-primary btn-sm',
                                ]);
                            }
                            ?>
                        </td>
                        <td class="td-btn-wrap">
                            <?php echo $this->Form->button('<i class="fa fa-comments-o" aria-hidden="true"></i>', [
                                'class' => 'btn btn-primary btn-sm btn_comment',
                                'data-id' => $item->id,
                                'escape' => false,
                            ]); ?>
                        </td>
                         <td data-target="<?php echo ($item->is_suspend) ? 1 : 0; ?>">
                             <?php
                             if ($item->is_suspend == 1) {
                                 echo $this->Form->button(BTN_ICON_UNDO, [
                                     'class' => 'btn btn-recover btn-sm btn_suspend',
                                     'data-name' => 'recover',
                                     'escape' => false
                                 ]);
                             } else {
                                 echo $this->Html->link(BTN_ICON_EDIT, [
                                     'controller' => 'product-brands',
                                     'action' => 'edit', $item->id
                                     ], [
                                     'class' => 'btn btn-primary btn-sm',
                                     'escape' => false
                                 ]);
                             }
                             ?>
                         </td>
                         <td data-target="<?php echo ($item->is_suspend) ? 1 : 0; ?>">
                             <?php
                             if ($item->is_suspend == 1) {
                                 if (!empty($item->product_brands)) {
                                     echo $this->Form->button(BTN_ICON_DELETE, [
                                         'class' => 'btn btn-delete btn-sm',
                                         'id' => 'btn_delete',
                                         'disabled' => 'disabled',
                                         'escape' => false
                                     ]);
                                 } else {
                                     echo $this->Form->button(BTN_ICON_DELETE, [
                                         'class' => 'btn btn-delete btn-sm',
                                         'id' => 'btn_delete',
                                         'data-name' => 'delete',
                                         'escape' => false
                                     ]);
                                 }
                             } else {
                                 echo $this->Form->button(BTN_ICON_STOP, [
                                     'class' => 'btn btn-suspend btn-sm btn_suspend',
                                     'data-name' => 'suspend',
                                     'escape' => false
                                 ]);
                             }
                             ?>
                         </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    <?php endif; ?>
</div>
<div class="row row-top-space" style="padding-bottom: 20px;">
    <?php echo $this->element('display_number'); ?>
    <?= $this->element('next_prev') ?>
    <div class="clearfix"></div>
</div>

<!--Modal Detail-->
<?php echo $this->element('Modal/view_detail'); ?>
<!--Modal Comment-->
<?= $this->element('/Modal/message') ?>
<!--Modal Suspend-->
<?php echo $this->element('Modal/suspend'); ?>
<!--Modal delete-->
<?php echo $this->element('Modal/delete'); ?>

<script>
    (function(e) {
        var optionData = [];
        //auto complete
        $('body').find('#keyword').typeahead({
            minLength: 0,
            autoSelect: true,
            source:  function (query, process) {
                return $.get('<?= $this->Url->build(['action' => 'getBrandNameBySearch']) ?>', {
                        keyword: query
                    }, function (data) {
                        if (data.data !== null && data.data !== 'undefined') {
                            $.each(data.data, function(i, v) {
                                if (v.name !== '') {
                                    optionData.push(v.name);
                                }
                                if (v.name_en !== '') {
                                    optionData.push(v.name_en);
                                }
                            });
                        }
                        return process(optionData);
                    }, 'json');
            }
        });

        $('body').on('click', '.typeahead li', function(e) {
            $('body').find('.form-search').submit();
        });

        $('body').on('change', '.display_data', function (e) {
            window.location.href = '<?php echo $this->Url->build('/product-brands/index'); ?>' + '?displays=' + $(this).val();
        });

        $('body').on('click', '.data-tb-list .table>tbody>tr', function (e) {
            $.LoadingOverlay('show');
            var id = $(this).closest('tr').data('id');

            $.get('<?php echo $this->Url->build('/product-brands/view/'); ?>' + id, function (data) {
                $.LoadingOverlay('hide');
                $('#modal_detail').find('.modal-body').html(data);
                $('#modal_detail').modal('show');
                var file1 = $('#modal_detail').find('.media-brand').val();
                var file2 = $('#modal_detail').find('.media-product').val();
                var media_brand = [];
                var media_product = [];
                var content = $('body').find('#modal_detail .modal-body .fieldset-border');

                if (file1) {
                    media_brand = file1.split(',');
                    display_media(content, media_brand, 1);
                }
                if (file2) {
                    media_product = file2.split(',');
                    display_media(content, media_product, 0);
                }
            }, 'html');
        });

        $('.btn_comment').click(function (e) {
            e.stopPropagation();
            let params = {
                id: $(this).closest('tr').attr('data-id'),
                type: '<?php echo TYPE_PRODUCT_BRAND ?>',
                referer: '<?php echo $this->request->controller; ?>'
            };
            let options = {
                url: BASE + 'Messages/getMessageList',
                type: 'GET',
                data: params,
                beforeSend: function () {
                    $.LoadingOverlay('show');
                }
            };
            ajaxRequest(options, SESSION_TIMEOUT, function (data) {
                $('body').prepend(data);
                $('body').find('.modal-message').modal('show');
            });
        });

        //filter message list search to specific
        $('body').on('click', '.btn-to-individual', function() {
            $(this).removeClass('btn-detault').addClass('btn-primary');
            $('body').find('.btn-to-all').removeClass('btn-primary').addClass('btn-default');
            var toAll = $('.to-individual').val();
            if (parseInt(toAll) === 0) {
                $('.to-all').val(0);
                $('.to-individual').val(1);
            } else {
                $('.to-all').val(1);
                $('.to-individual').val(0);
            }
            messagesSearch(listUrlSearch);
        });

        $('body').on('click', '.btn-search', function() {
            messagesSearch(listUrlSearch);
        });

        $('body').on('click', '.btn-suspend-msg', function() {
            var id = $(this).data('id');
            var data = $('#form-message-status-' + id).serialize();
            var suspendUrl = '<?= $this->Url->build(['controller' => 'Messages', 'action' => 'suspend']) ?>';
            suspendMessage(id, suspendUrl, data);
        });

        //modal confirm suspend
        $('body').on('click', '.btn_suspend', function (e) {
            e.stopPropagation();
            $('.suspend-item-id').val($(this).attr('data-id'));
            $('.is-suspend-item').val($(this).attr('data-target'));
            var confirm = '';
            if (parseInt($(this).attr('data-target')) === 1) {
                $('#btn_suspend_yes').text('<?= __('BTN_SUSPEND') ?>');
                confirm = '<?= __('MSG_CONFIRM_SUSPEND') ?>';
            } else {
                $('#btn_suspend_yes').text('<?= __('BTN_RECOVER') ?>');
                confirm = '<?= __('MSG_CONFIRM_UNDO_SUSPEND') ?>';
            }
            $('.confirm-suspend-text').text(confirm);
            $('#modal_suspend').modal('show');
        });

        //modal change suspend status
        $('body .data-tb-list .table > tbody > tr').on('click', '.btn_suspend', function (e) {
            e.stopPropagation();
            var content = $('#modal_suspend');
            var status = $(this).attr('data-name');
            var txt = '';

            switch (status) {
                case 'suspend':
                    $('#btn_suspend_yes').text('<?= __('BTN_SUSPEND') ?>');
                    txt = '<?= __('MSG_CONFIRM_SUSPEND') ?>';
                    break;
                case 'delete':
                    txt = '<?= __('MSG_CONFIRM_DELETE') ?>';
                    $('#btn_suspend_yes').text('<?= __('BTN_DELETE') ?>');
                    break;
                 case 'recover':
                    $('#btn_suspend_yes').text('<?= __('BTN_RECOVER') ?>');
                    txt = '<?= __('MSG_CONFIRM_UNDO_SUSPEND') ?>';
                    break;
            };

            $(content).find('#btn_suspend_yes').data('id', $(this).closest('tr').data('id'));
            $(content).find('#btn_suspend_yes').data('target', $(this).closest('td').data('target'));
            $(content).find('.modal-body p').html(txt);
            $(content).modal('show');
        });

        $('body').on('click', '#btn_suspend_yes', function (e) {
            var url = '<?php echo $this->Url->build('/product-brands/update-suspend/'); ?>';
            var params = {
                id: $(this).data('id'),
                is_suspend: $(this).data('target')
            };
            $.post(url, params, function (data) {
                if (data.message === 'success') {
                    location.reload();
                }
            }, 'json');
        });

        //modal delete confirm
        $('body').on('click', '#btn_delete', function (e) {
            e.stopPropagation();
            $('#btn_delete_yes').text('<?= __('BTN_DELETE') ?>');
            $('.delete-item').val($(this).attr('data-id'));
            $('#modal_delete').modal('show');
        });

        //modal delete item.
        $('body').on('click', '#btn_delete_yes', function() {
            var data = $('.form-delete').serialize();
            var url = '<?= $this->Url->build('/product-brands/delete') ?>';
            itemDelete(url, data, '<?php echo __('TXT_SESSION_TIMEOUT') ?>');
        });

        $('body').on('click', '.tt-selectable', function (e) {
            $('form[name="form_search"]').submit();
        });

        $('body').on('click', '.table>tbody>tr>td a, .table>tbody>tr>td button', function(e) {
            e.stopPropagation();
        });

        $('#keyword').typeahead({
            hint: true,
            highlight: true,
            minLength: 1
        },
        {
            name: 'optionData',
            source: substringMatcher(optionData)
        });

        pageOutOfRange(<?= $paging ?>, '<?= $this->request->query('page') ?>');
        var maxPage = parseInt($('#maxPage').val());
        updatePagination(maxPage);

        // display media list
        function display_media(content, list, index)
        {
            if (list != null && list != undefined) {
                var element = '<div class="alert alert-warning" role="alert">Documents of Brand</div>';
                if (index == 0) {
                    element = '<div class="alert alert-warning" role="alert">Documents of Product</div>';
                }
                $.each(list, function(i, v) {
                    var path = BASE_URL + 'img/uploads/' + v;
                    var extension = v.split('.').pop().toLowerCase();
                    element += '<div class="col-md-3 col-lg-3 col-sm-3 media-list-wrap">' + dispay_html(v, path, index, extension, i) + '</div>';
                });
                if (index == 1) {
                    $(content).find('.brand').append(element);
                } else {
                    $(content).find('.product').append(element);
                }
            }
        }

        function dispay_html(data, path, index, extension, i)
        {
            var canvas = '';
            var img = '';

            if (extension === 'pdf') {
               canvas = '<canvas id="the-canvas-' + i + '"></canvas>';
            } else {
                img = '<img src="' + path + '" class="open-main-image">';
            }
            var element = '<section class="thumbnail select_img">'
                        + '<a href="' + path + '" target="_blank">' + img + canvas + '</a></section>';

            if (extension === 'pdf') {
               read_pdf_file(path, i);
            }

            return element;
        }

        function read_pdf_file(path, id)
        {
            var full_path = location.protocol + '//' + location.host + path;

            PDFJS.getDocument(full_path).promise.then(function (pdf) {
                pdf.getPage(1).then(function getPageHelloWorld(page) {
                    var scale = 1.5;
                    var viewport = page.getViewport(scale);
                    var canvas = $('body').find('#the-canvas-' + id)[0];
                    var context = canvas.getContext('2d');
                    canvas.height = viewport.height;
                    canvas.width = viewport.width;
                    page.render({canvasContext: context, viewport: viewport})
                });
            });
        }
    })();
</script>
<?php
    echo $this->Html->script([
        'processing-api.min',
        'pdf',
    ], ['block' => 'script']);
