
<?php
$userGroup = $this->request->session()->read('user_group');
$local = $this->request->session()->read('tb_field');
$this->assign('title', 'PO Details');
$fax = '';

//Invoice Condition
$taxableTotal = 0;
?>
<div class="row">
    <div class="col-md-6">
        <h1><?= __('Order Form') ?></h1>
        <p><strong><?= __('Order Form No.') ?></strong>&nbsp;<?= $data->rop_number ?></p>
        <p><strong><?= __('Issue Date:') ?></strong>&nbsp;<?= date('Y-m-d', strtotime($data->issue_date)) ?></p>
    </div>
    <div class="col-md-6 text-right">
        <div class="company-invoice-logo text-center">
            Logo Here
        </div>
        <p>3791 Jalan Bukit Merah, #10-17 E-Centre @ Redhill Singapore 159471</p>
        <P>Tel: +65-6274-0433 / Fax: +65-6274-0477</P>
        <P>UEN No: 201403392G / Reg No: 201403392G</P>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <hr class="hr-invoiced">
    </div>
</div>
<div class="row row-top-space">
    <div class="col-md-8">
        <div class="seller-info">
            <h3>
                <?php
                switch ($data->seller->type) {
                    case TYPE_SUPPLIER:
                        $id = $data->seller->supplier->id;
                        $fax = $data->seller->supplier->fax;
                        echo $this->Comment->getFieldByLocal($data->seller->supplier, $local);
                        break;
                    default :
                        $id = $data->seller->manufacturer->id;
                        $fax = $data->seller->manufacturer->fax;
                        echo $this->Comment->getFieldByLocal($data->seller->manufacturer, $local);
                }
                ?>
            </h3>
            <div class="row">
                <div class="col-md-3"><p><?= __('TXT_ADDRESS') ?></p></div>
                <div class="col-md-9">
                    <?php
                    switch ($data->seller->type) {
                        case TYPE_SUPPLIER:
                            echo $this->Comment->address($local, $data->seller->supplier);
                            break;
                        default :
                            echo $this->Comment->address($local, $data->seller->manufacturer);
                    }
                    ?>
                </div>
            </div>
            <div class="row invoice-form-row">
                <div class="col-md-3"><div class="invoice-form-label"><?= __('TXT_CONTACT') ?></div></div>
                <div class="col-md-4 custom-select">
                    <?= $this->Form->select('seller_id', $person, [
                        'class' => 'form-control select-seller',
                        'value' => $id,
                        'label' => false
                    ]) ?>
                </div>
            </div>
            <div class="row invoice-form-row">
                <div class="col-md-3"><div class="invoice-form-label"><?= __('TXT_EMAIL') ?></div></div>
                <div class="col-md-4 custom-select">
                    <?php echo $this->Form->input('email', [
                        'type' => 'select',
                        'class' => 'form-control select-email',
                        'label' => false,
                        'options' => ['' => __('TXT_SELECT_EMAIL')],
                        'disabled' => true,
                        'templates' => [
                            'inputContainer' => '{{content}}',
                        ],
                    ]); ?>
                </div>
            </div>
            <div class="row invoice-form-row">
                <div class="col-md-3 custom-select">
                    <div class="invoice-form-label"><?= __('TXT_TEL') ?></div>
                </div>
                <div class="col-md-4 custom-select">
                    <?php echo $this->Form->input('tel', [
                        'type' => 'select',
                        'class' => 'form-control select-tel',
                        'label' => false,
                        'options' => ['' => __('TXT_SELECT_TEL')],
                        'disabled' => true,
                        'templates' => [
                            'inputContainer' => '{{content}}',
                        ],
                    ]); ?>
                </div>
                <div class="col-md-1" style="padding-right: 0;">
                    <div class="invoice-form-label">/ <?= __('TXT_FAX') ?></div>
                </div>
                <div class="col-md-4">
                    <?php echo $this->Form->input('fax', [
                        'type' => 'text',
                        'class' => 'form-control txt-fax',
                        'label' => false,
                        'readonly' => 'readonly',
                        'placeholder' => __('TXT_ENTER_FAX'),
                        'value' => $fax,
                        'templates' => [
                            'inputContainer' => '{{content}}',
                        ],
                    ]); ?>
                </div>
            </div>
            <div class="row invoice-form-row">
                <div class="col-md-offset-3 col-md-4">
                    <?php
                    echo $this->Form->button(__('TXT_UPDATE'), [
                        'type' => 'button',
                        'class' => 'btn btn-primary btn-sm btn-add-info',
                        'disabled' => true,
                    ]);
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row row-invoice-incharge">
    <div class="col-md-offset-8 col-md-4">
        <p>Order in Charge: #xxx</p>
        <p>Email: #xxx</p>
        <p>Mobile: #xxx</p>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <table class="table table-striped table-bordered table-po">
            <thead>
                <tr>
                    <th><?= __('No') ?></th>
                    <th width="35%"><?= __('TXT_DESCRIPTION') ?></th>
                    <th><?= __('TXT_UNIT_PRICE') ?></th>
                    <th><?= __('TXT_QUANTITY') ?></th>
                    <th><?= __('TXT_AMOUNT') ?></th>
                </tr>
            </thead>
            <tbody>

                <?php
                if ($saleDetail) {
                    $num = 0;

                    if ($data->sale_details[0]->count <= 20) {
                        for ($i = 0; $i < ((20 - $data->sale_details[0]->count) + count($saleDetail)); $i++) {
                            $num += 1;

                            if (($i + 1) <= count($saleDetail)) {
                                $item = $saleDetail[$i];
                                $childs = $item->children;
                                $amount1 = intval($item->unit_price) * intval($item->quantity);
                                $taxableTotal += $amount1;
                                $is_discount = ($item->is_discount == 1) ? '<label class="label-foc-child no-margin">Discounted</label>&nbsp;' : null;

                                // display parent record
                                echo '<tr data-id="' . $item->id . '">' .
                                    '<td class="text-center">' . $num . '</td>' .
                                    '<td>'
                                    . $this->Comment->POProductDetailName($item) .
                                    '</td>' .
                                    '<td>' . $is_discount . $data->currency->code . ' ' . $this->Comment->numberFormat($item->unit_price) . '</td>' .
                                    '<td>' . $item->quantity . '</td>' .
                                    '<td>' . $data->currency->code . ' ' . $this->Comment->numberFormat($amount1) . '</td></tr>';

                                // display parent record
                                if ($childs) {
                                    foreach ($childs as $key => $value) {
                                        $is_discount1 = ($value->is_discount == 1) ? '<label class="label-foc-child no-margin">Discounted</label>&nbsp;' : null;
                                        $num += 1;
                                        $amount2 = intval($value->unit_price) * intval($value->quantity);
                                        $taxableTotal += $amount2;

                                        echo '<tr data-id="' . $value->id . '">' .
                                        '<td class="text-center">' . $num . '</td>' .
                                        '<td><label class="label-foc-child no-margin">FOC</label>&nbsp;'
                                        . $this->Comment->POProductDetailName($value) .
                                        '</td>' .
                                        '<td>' . $is_discount1 . $data->currency->code . ' ' . $this->Comment->numberFormat($value->unit_price) . '</td>' .
                                        '<td>' . $value->quantity . '</td>' .
                                        '<td>' . $data->currency->code . ' ' . $this->Comment->numberFormat($amount2) . '</td></tr>';
                                    }
                                }
                        } else {
                                echo '<tr><td class="text-center">' . $num . '</td><td>&nbsp;</td>' .
                                     '<td>&nbsp;</td>' .
                                     '<td>&nbsp;</td>' .
                                     '<td>&nbsp;</td></tr>';
                            }
                        }
                    } else {
                        foreach ($saleDetail as $key => $value) {
                            $num += 1;
                            $childs = $value->children;
                            $amount3 = intval($value->unit_price) * intval($value->quantity);
                            $taxableTotal += $amount3;

                            // display parent record
                            echo '<tr data-id="' . $value->id . '">' .
                                '<td class="text-center">' . $num . '</td>' .
                                '<td>'
                                . $this->Comment->POProductDetailName($value) .
                                '</td>' .
                                '<td>' . $data->currency->code . ' ' . $this->Comment->numberFormat($value->unit_price) . '</td>' .
                                '<td>' . $value->quantity . '</td>' .
                                '<td>' . $data->currency->code . ' ' . $this->Comment->numberFormat($amount3) . '</td></tr>';

                            // display parent record
                            if ($childs) {
                                foreach ($childs as $key1 => $value1) {
                                    $num += 1;
                                    $amount4 = intval($value1->unit_price) * intval($value1->quantity);
                                    $taxableTotal += $amount4;

                                    echo '<tr data-id="' . $value1->id . '">' .
                                    '<td class="text-center">' . $num . '</td>' .
                                    '<td><label class="label-foc-child no-margin">FOC</label>&nbsp;'
                                    . $this->Comment->POProductDetailName($value1) .
                                    '</td>' .
                                    '<td>' . $data->currency->code . ' ' . $this->Comment->numberFormat($value1->unit_price) . '</td>' .
                                    '<td>' . $value1->quantity . '</td>' .
                                    '<td>' . $data->currency->code . ' ' . $this->Comment->numberFormat($amount4) . '</td></tr>';
                                }
                            }
                        }
                    }
                } else {
                    for ($i=1; $i<=20; $i++) {
                        echo '<tr>' .
                            '<td>' . $i . '</td>' .
                            '<td>&nbsp;</td>' .
                            '<td>&nbsp;</td>' .
                            '<td>&nbsp;</td>' .
                            '<td>&nbsp;</td>' .
                            '</tr>';
                    }
                }
                ?>

                <!-- Table Footer -->
                <tr class="table-po-footer">
                    <td colspan="2" rowspan="12" class="border-none no-padding-left" style="vertical-align: top;">
                        <section>
                            <p>Medical Professionals Singapore Pte. Ltd. Peter Lim, Managing Director</p>
                            <p class="company-invoice-signature">Digital Signature</p>
                            <hr class="signature-line">
                        </section>
                        <section>
                            <p><?= __('TXT_DATE') ?></p>
                            <div class="input-group with-datepicker">
                                <?= $this->Form->input('date', ['class' => 'form-control datepicker', 'label' => false]) ?>
                                <div class="input-group-btn">
                                    <button type="button" class="btn btn-default trigger-calendar" aria-label="Calendar">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </button>
                                </div>
                            </div>
                            <p></p>
                            <hr class="signature-line">
                        </section>
                    </td>
                    <td colspan="2" class="table-text-right"><b><?= __('TXT_IMPORT_VAT_TAXABLE') ?> :</b></td>
                    <td><?= $data->currency->code . ' ' . $this->Comment->numberFormat($taxableTotal) ?></td>
                    <td>&nbsp;</td>
                </tr>
                <tr class="table-po-footer border-none">
                    <td colspan="2" class="table-text-right"><b><?= __('TXT_IMPORT_VAT') ?> :</b></td>
                    <td>
                        <?php
                        $rowB = round($taxableTotal*0.63, -2);
                        echo $data->currency->code . ' ' . $this->Comment->numberFormat($rowB);
                        ?>
                    </td>
                    <td>&nbsp;</td>
                </tr>
                <tr class="table-po-footer border-none">
                    <td colspan="2" class="table-text-right"><b><?= __('TXT_DOMESTIC_VAT') ?> :</b></td>
                    <td>
                        <?php
                        $rowC = round($rowB*0.63, -2);
                        echo $data->currency->code . ' ' . $this->Comment->numberFormat($rowC);
                        ?>
                    </td>
                    <td>&nbsp;</td>
                </tr>
                <tr class="table-po-footer border-none">
                    <td colspan="2" class="table-text-right"><b><?= __('TXT_SUB_TOTAL') ?> :</b></td>
                    <td>
                        <?php
                        $rowD = $taxableTotal + $rowB + $rowC;
                        echo $data->currency->code . ' ' . $this->Comment->numberFormat($rowD);
                        ?>
                    </td>
                    <td>&nbsp;</td>
                </tr>
                <tr class="table-po-footer border-none">
                    <td colspan="2" class="table-text-right"><b><?= __('TXT_DOMESTIC_COURIER_SERVICE_CHARGE') ?> :</b></td>
                    <td>
                        <span class="label label-success deomestic-charge" style="font-size: inherit;">
                            <i class="fa fa-pencil" aria-hidden="true"></i>
                        </span>&nbsp;
                        <?php
                        if ($data->domestic_courier_service_charge) {
                            echo $data->currency->code . ' ' . $this->Comment->numberFormat($data->domestic_courier_service_charge);
                        } else {
                            echo $data->currency->code . ' 0.00';
                        }
                        ?>
                    </td>
                    <td>&nbsp;</td>
                </tr>
                <tr class="table-po-footer border-none">
                    <td colspan="2" class="table-text-right"><b><?= __('TXT_VAT') ?> :</b></td>
                    <td>
                        <?php
                        $domestic = $data->domestic_courier_service_charge ? $data->domestic_courier_service_charge : 0;
                        $rowF = round($domestic*0.08, 0);
                        echo $data->currency->code . ' ' . $this->Comment->numberFormat($rowF);
                        ?>
                    </td>
                    <td>&nbsp;</td>
                </tr>
                <tr class="table-po-footer border-none">
                    <td colspan="2" class="table-text-right"><b><?= __('TXT_SUB_TOTAL') ?> :</b></td>
                    <td>
                        <?php
                        $rowG = $rowF + $data->domestic_courier_service_charge;
                        echo $data->currency->code . ' ' . $this->Comment->numberFormat($rowG);
                        ?>
                    </td>
                    <td>&nbsp;</td>
                </tr>
                <tr class="table-po-footer border-none">
                    <td colspan="2" class="table-text-right"><b><?= __('TXT_HANDLING_FEE') ?> :</b></td>
                    <td>
                        <span class="label label-success handling-fee" style="font-size: inherit;">
                            <i class="fa fa-pencil" aria-hidden="true"></i>
                        </span>&nbsp;
                        <?php
                        if ($data->handling_fee) {
                            echo $data->currency->code . ' ' . $this->Comment->numberFormat($data->handling_fee);
                        } else {
                            echo $data->currency->code . ' 0.00';
                        }
                        ?>
                    </td>
                    <td>&nbsp;</td>
                </tr>
                <tr class="table-po-footer border-none">
                    <td colspan="2" class="table-text-right"><b><?= __('TXT_VAT') ?> :</b></td>
                    <td>
                        <?php
                        $rowI = round($data->handling_fee * 0.08, 0);
                        echo $data->currency->code . ' ' . $this->Comment->numberFormat($rowI);
                        ?>
                    </td>
                    <td>&nbsp;</td>
                </tr>
                <tr class="table-po-footer border-none">
                    <td colspan="2" class="table-text-right"><b><?= __('TXT_SUB_TOTAL') ?> :</b></td>
                    <td>
                        <?php
                        $rowJ = $data->handling_fee + $rowI;
                        echo $data->currency->code . ' ' . $this->Comment->numberFormat($rowJ);
                        ?>
                    </td>
                    <td>&nbsp;</td>
                </tr>
                <tr class="table-po-footer border-none">
                    <td colspan="2" class="table-text-right"><b><?= __('TXT_SPECIAL_DISCOUNT') ?> :</b></td>
                    <td>
                        <span class="label label-success special-discount" style="font-size: inherit;">
                            <i class="fa fa-pencil" aria-hidden="true"></i>
                        </span>&nbsp;
                        <?php
                        if ($data->special_discount) {
                            echo $data->currency->code . ' ' . $this->Comment->numberFormat($data->special_discount);
                        } else {
                            echo $data->currency->code . ' 0.00';
                        }
                        ?>
                    </td>
                    <td>&nbsp;</td>
                </tr>
                <tr class="table-po-footer border-none">
                    <td colspan="2" class="table-text-right"><b><?= __('TXT_INVOICE_TOTAL') ?> :</b></td>
                    <td>
                        <?php
                        $rowL = $rowD + $rowG + $rowJ - $data->special_discount;
                        echo $data->currency->code . ' ' . $this->Comment->numberFormat($rowL);
                        ?>
                    </td>
                    <td>&nbsp;</td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
<!--Invoice Footer-->
<div style="margin-top: 2em;"></div>
<div class="invoice-footer">
    <?php
    echo $this->Form->button(__('TXT_REQUEST_OF_PAYMENT'), [
        'type' => 'button',
        'class' => 'btn btn-sm btn-primary btn-width btn-invoice-ft',
        'data-type' => 'payment',
    ]);
    ?>
    <?php
    echo $this->Form->button(__('TXT_INVOICE_DELIVERY'), [
        'type' => 'button',
        'class' => 'btn btn-sm btn-primary btn-width btn-invoice-ft',
        'data-type' => 'invoice',
    ]);
    ?>
</div>

<!--Modal Send Page-->
<div class="modal fade" id="send-page" role="dialog" data-backdrop="static">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Send Page</h4>
            </div>
            <div class="modal-body">
                <?php echo $this->Form->create(null, [
                    'name' => 'form_invoice',
                    'role' => 'form',
                ]); ?>
                <div class="form-group no-margin">
                    <?php
                    echo $this->Form->input('page_number', [
                        'type' => 'number',
                        'class' => 'form-control',
                        'label' => false,
                        'placeholder' => __('TXT_SEND_PAGE'),
                        'templates' => [
                            'inputContainer' => '{{content}}'
                        ],
                    ]);
                    ?>
                </div>
                <?php echo $this->Form->end(); ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default btn-sm btn-width close-modal" data-dismiss="modal"><?= __('BTN_CANCEL') ?></button>
                <button type="button" class="btn btn-primary btn-sm btn-width btn-yes"><?= __('TXT_YES') ?></button>
            </div>
        </div>
    </div>
</div>
<!--Modal Domestic-->
<div class="modal fade" id="domestic" role="dialog" data-backdrop="static">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body"></div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default btn-sm btn-width close-modal" data-dismiss="modal"><?= __('BTN_CANCEL') ?></button>
                <button type="button" class="btn btn-primary btn-sm btn-width btn-domestic-yes"><?= __('TXT_YES') ?></button>
            </div>
        </div>
    </div>
</div>

<?php
echo $this->Html->css([
    'bootstrap-datetimepicker.min',
    'jquery-ui',
    'intlTelInput',
]);
?>

<script>
    $(function() {
        var local = '<?= $this->request->session()->read('tb_field') ?>';

        $('.datepicker').datetimepicker({
            format: 'YYYY-MM-DD'
        });

        $('body').on('click', '.btn-invoice-ft', function(e) {
            var type = $(this).attr('data-type');

            $('body').find('#send-page').modal('show');
        });

        $('body').on('click', '.deomestic-charge', function(e) {
            $.LoadingOverlay('show');
            var url = '<?= $this->Url->build(['action' => 'getDomestic']) ?>';
            var params = {
                id: '<?= $data->id ?>',
                currency: '<?= $data->currency->code ?>'
            };

            ajax_request_get(url, params, function(data) {
                var modal = $('body').find('#domestic');

                $(modal).find('.modal-body').html(data);
                $(modal).find('.modal-title').html('<?= __('TXT_DOMESTIC_COURIER_SERVICE_CHARGE') ?>');
                $(modal).modal('show');
            }, 'html', '<?= __('TXT_SESSION_TIMEOUT') ?>');
        });

        $('body').on('click', '.special-discount', function(e) {
            $.LoadingOverlay('show');
            var url = '<?= $this->Url->build(['action' => 'getSpecialDiscount']) ?>';
            var params = {
                id: '<?= $data->id ?>',
                currency: '<?= $data->currency->code ?>'
            };

            ajax_request_get(url, params, function(data) {
                var modal = $('body').find('#domestic');

                $(modal).find('.modal-body').html(data);
                $(modal).find('.modal-title').html('<?= __('TXT_SPECIAL_DISCOUNT') ?>');
                $(modal).find('.btn-primary').removeClass('btn-domestic-yes').addClass('btn-special-discount');
                $(modal).modal('show');
            }, 'html', '<?= __('TXT_SESSION_TIMEOUT') ?>');
        });

        $('body').on('click', '.btn-domestic-yes', function(e) {
            $.LoadingOverlay('show');
            var form = $('body').find('form[name="form_domestic_charge"]');
            var url = '<?= $this->Url->build(['action' => 'updateDeomestic']) ?>';

            ajax_request_post(url, $(form).serialize(), function(data) {
                $(form).find('.error-message').remove();

                if (data.message === '<?= MSG_ERROR ?>') {
                    var message = '<label class="error-message">' + data.data + '</labe>';
                    $(form).find('input[type="number"]').closest('.form-group').append(message);
                } else {
                    location.reload();
                }
            }, '<?= __('TXT_SESSION_TIMEOUT') ?>');
        });

        $('body').on('click', '.btn-special-discount', function(e) {
            $.LoadingOverlay('show');
            var form = $('body').find('form[name="form_special_discount"]');
            var url = '<?= $this->Url->build(['action' => 'updateSpecialDiscount']) ?>';

            ajax_request_post(url, $(form).serialize(), function(data) {
                $(form).find('.error-message').remove();

                if (data.message === '<?= MSG_ERROR ?>') {
                    var message = '<label class="error-message">' + data.data + '</labe>';
                    $(form).find('input[type="number"]').closest('.form-group').append(message);
                } else {
                    location.reload();
                }
            }, '<?= __('TXT_SESSION_TIMEOUT') ?>');
        });

        $('body').on('click', '.btn-handling-yes', function(e) {
            $.LoadingOverlay('show');
            var form = $('body').find('form[name="form_handling_fee"]');
            var url = '<?= $this->Url->build(['action' => 'updateHandlingFee']) ?>';

            ajax_request_post(url, $(form).serialize(), function(data) {
                $(form).find('.error-message').remove();

                if (data.message === '<?= MSG_ERROR ?>') {
                    var message = '<label class="error-message">' + data.data + '</labe>';
                    $(form).find('input[type="number"]').closest('.form-group').append(message);
                } else {
                    location.reload();
                }
            }, '<?= __('TXT_SESSION_TIMEOUT') ?>');
        });

        $('body').on('click', '.handling-fee', function(e) {
            $.LoadingOverlay('show');
            var url = '<?= $this->Url->build(['action' => 'getHandlingFee']) ?>';
            var params = {
                id: '<?= $data->id ?>',
                currency: '<?= $data->currency->code ?>'
            };

            ajax_request_get(url, params, function(data) {
                var modal = $('body').find('#domestic');

                $(modal).find('.modal-body').html(data);
                $(modal).find('.modal-title').html('<?= __('TXT_HANDLING_FEE') ?>');
                $(modal).find('.btn-primary').removeClass('btn-domestic-yes').addClass('btn-handling-yes');
                $(modal).modal('show');
            }, 'html', '<?= __('TXT_SESSION_TIMEOUT') ?>');
        });

        $('body').on('click', '.btn-add-info', function(e) {
            $.LoadingOverlay('show');
            var params = {
                personInChargeId: $('body').find('.seller-info select[name="seller_id"]').val(),
                email: $('body').find('.seller-info #email option:selected').text(),
                tel: $('body').find('.seller-info #tel option:selected').text(),
                fax: $('body').find('.seller-info #fax').val(),
                personInChargeName: $('body').find('.seller-info select[name="seller_id"] option:selected').text(),
                id: '<?= $data->id ?>'
            };
            var url = '<?= $this->Url->build(['action' => 'updateSellInfo']) ?>';

            ajax_request_post(url, params, function(data) {
            }, '<?= __('TXT_SESSION_TIMEOUT') ?>');
        });

        $('.trigger-calendar').click(function() {
            $(this).parents('.with-datepicker').find('.datepicker').focus();
        });

        $('body').on('change', '.select-seller', function(e) {
            if ($(this).val() !== '') {
                $.LoadingOverlay('show');
                var url = '<?= $this->Url->build(['action' => 'get-info']) ?>';
                var params = {
                    external_id: $(this).val(),
                    type: '<?= $data->seller->type ?>'
                };

                ajax_request_get(url, params, function(data) {
                    $('body').find('.btn-add-info').attr('disabled', false);
                    var emails = [];
                    var phones = [];
                    var tels = [];
                    var response = data.data;

                    if (response.length > 0) {
                        $.each(response, function(i, v) {
                            if (v.email !== null) {
                                emails.push({key: v.id, value: v.email});
                            }
                            if (v.phone !== null) {
                                phones.push({key: v.id, value: v.phone});
                            }
                            if (v.tel !== null) {
                                tels.push({key: v.id, value: v.tel});
                            }
                        });
                    }

                    if (emails !== null) {
                        $('body').find('.select-email').removeAttr('disabled');
                        $('body').find('.select-email option:not(:first)').remove();
                        $('body').find('.select-email').append(add_option(emails));
                    }
                    if (phones !== null) {
                        $('body').find('.select-phone').removeAttr('disabled');
                        $('body').find('.select-phone option:not(:first)').remove();
                        $('body').find('.select-phone').append(add_option(phones));
                    }
                    if (tels !== null) {
                        $('body').find('.select-tel').removeAttr('disabled');
                        $('body').find('.select-tel option:not(:first)').remove();
                        $('body').find('.select-tel').append(add_option(tels));
                    }
                }, 'json', '<?= __('TXT_SESSION_TIMEOUT') ?>');
            }
        });

        /**
        * Function add_option
        * use for add option to select dropdown
        * @param object data
        * @returns return string
        */
        function add_option(data)
        {
           var element = '';

           if (data !== null && data !== undefined) {
               $.each(data, function(i, v) {
                   element += '<option value="' + v.key + '">' + v.value + '</option>';
               });
           }

           return element;
        }
    });
</script>
