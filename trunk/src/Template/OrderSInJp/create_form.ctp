
<section class="modal-wrapper">
    <style>
        .create-form2 {
            z-index: 1051;
        }
    </style>
    <div class="modal fade create-form2" data-backdrop="static" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <?php
                    echo $this->Form->button('<span aria-hidden="true">&times;</span>', [
                        'type' => 'button',
                        'class' => 'close modal-close',
                        'data-dismiss' => 'modal',
                        'aria-label' => 'Close',
                        'escape' => false,
                    ]);
                    ?>
                    <h4 class="modal-title text-center"><?= __('ORDER_MANAGEMENT_TXT_DOMESTIC_TRADE_DIRECT_DELIVERY') ?></h4>
                </div>
                <div class="modal-body">
                    <?php
                    echo $this->Form->create($data, [
                        'class' => 'form-add-po form-horizontal',
                        'autocomplete' => 'off',
                        'name' => 'purchase_order',
                        'onsubmit' => 'return false;',
                    ]);
                    echo $this->Form->hidden('type', [
                        'id' => 'type',
                        'value' => TRADE_TYPE_SALE_BEHALF_S_IN_JP,
                    ]);
                    if (!$data->isNew()) {
                        echo $this->Form->hidden('id');
                    }
                    ?>
                    <div class="form-group">
                        <label for="" class="control-label col-md-3">
                            <?= __('TXT_SELLER') ?>
                        </label>
                        <div class="col-md-7 custom-select">
                            <?= $this->Form->select('seller_id', $sellers, [
                                'class' => 'form-control',
                                'label' => false,
                                'required' => false,
                                'empty' => ($kind_of == TYPE_MP ? false : ['' => __('ORDER_MANAGEMENT_TXT_SELECT_SUPPLIER')]),
                                'value' => !$data->isNew() ? $data->seller_id : null,
                            ]);
                            ?>
                        </div>
                    </div>
                    <div class="form-group custom-select">
                        <label for="" class="control-label col-md-3">
                            <?= __('ORDER_MANAGEMENT_TXT_CLINIC') ?>
                        </label>
                        <div class="col-md-7">
                            <?php
                            $options = [];
                            if (isset($data)) {
                                $options = [$data->customer_id => $data->customer_name];
                            }
                            echo $this->Form->select('customer_id', $options, [
                                'class' => 'form-control customer-id',
                                'label' => false,
                                'required' => false,
                                'empty' => ['' => __('ORDER_MANAGEMENT_TXT_SELECT_CLINIC')],
                            ])
                            ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="control-label col-md-3">
                            <?= __('ORDER_MANAGEMENT_TXT_CURRENCY') ?>
                        </label>
                        <div class="col-md-7 custom-select">
                            <?= $this->Form->select('currency_id', $currencies, [
                                'class' => 'form-control',
                                'label' => false,
                                'required' => false,
                                'value' => !$data->isNew() ? $data->currency_id : null,
                                'empty' => ['' => __('ORDER_MANAGEMENT_TXT_SELECT_CURRENCY')],
                            ]);
                            ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="control-label col-md-3">
                            <?= __('ORDER_MANAGEMENT_TXT_ORDER_DATE') ?>
                        </label>
                        <div class="col-md-7">
                            <div class="input-group">
                                <?php
                                echo $this->Form->text('order_date', [
                                    'class' => 'form-control order-date',
                                    'label' => false,
                                    'required' => false,
                                    'id' => 'order-date',
                                    'value' => !$data->isNew() ? date('Y-m-d', strtotime($data->order_date)) : date('Y-m-d'),
                                ])
                                ?>
                                <div class="input-group-btn">
                                    <button type="button" class="btn btn-default" aria-label="Calendar" id="trigger-order-date">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="control-label col-md-3">
                            <?= __('ORDER_MANAGEMENT_TXT_INVOICE_ISSUE_DATE') ?>
                        </label>
                        <div class="col-md-7">
                            <div class="input-group">
                                <?= $this->Form->text('rop_issue_date', [
                                    'class' => 'form-control',
                                    'label' => false,
                                    'required' => false,
                                    'id' => 'datepicker',
                                    'value' => !$data->isNew() ? date('Y-m-d', strtotime($data->rop_issue_date)) : date('Y-m-d'),
                                ])
                                ?>
                                <div class="input-group-btn">
                                    <button type="button" class="btn btn-default" aria-label="Calendar" id="trigger-calendar">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="col-md-3 col-xs-12">
                            <?= __('TXT_PREFER_SHIPMENT') ?>
                        </label>
                        <div class="col-sm-7">
                          <div class="checkbox">
                            <label>
                                <?= $this->Form->checkbox('is_priority', ['hiddenField' => false]) ?>
                            </label>
                          </div>
                        </div>
                      </div>
                    <?php echo $this->Form->end(); ?>
                </div>
                <div class="modal-footer">
                    <div class="col-md-5 col-md-offset-3">
                        <?php
                        echo $this->Form->button(__('TXT_CANCEL'), [
                            'type' => 'button',
                            'class' => 'btn btn-default btn-sm btn-width modal-close',
                            'data-dismiss' => 'modal',
                            'aria-label' => 'Close',
                        ]);
                        echo $this->Form->button(__('TXT_REGISTER'), [
                            'type' => 'button',
                            'class' => 'btn btn-primary btn-sm btn-width btn-sw-stock',
                            'aria-label' => 'Close',
                            'v-on:click' => 'register',
                        ]);
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        var saleWStock = new Vue({
            el: '.create-form2',
            data: {
                url: '<?= $this->Url->build(['action' => 'registerSale']) ?>'
            },
            methods: {
                register: function(e) {
                    $.LoadingOverlay('show')
                    var form = $('form[name="purchase_order"]')
                    this.$http.post(this.url, {data: form.serialize()}, {emulateJSON: true, emulateHTTP: true})
                        .then(function(response) {
                            $(form).find('.error-message').remove()
                            this.data = response.body
                            if (this.data.message === '<?= MSG_ERROR ?>') {
                                showMessageError(form, this.data.data)
                            } else {
                                location.href = '<?= $this->Url->build(['action' => 'index']) ?>'
                            }
                            $.LoadingOverlay('hide')
                        });
                }
            },
            mounted: function() {
                // datetime initialize
                $('#datepicker, #order-date').datetimepicker({format: 'YYYY-MM-DD'});

                // select2 initialize
                $('body').find('select').select2({width: '100%'});
                $('body').find('.customer-id').select2({
                    width: '100%',
                    minLength: 0,
                    ajax: {
                        url: BASE + 'OrderSInJp/getCustomerList',
                        dataType: 'json',
                        delay: 250,
                        data: function (params) {
                            var query = {
                                keyword: params.term,
                                type: 'public'
                            };

                            return query;
                        },
                        processResults: function (data) {
                            var dataSource = [];

                            if (data.data !== null && data.data !== 'undefined') {
                                $.each (data.data, function(i, v) {
                                    dataSource.push({
                                        id: v.id,
                                        text: v.name + ' ' + v.name_en
                                    });
                                });
                            }

                            return { results: dataSource };
                        }
                    }
                });
            }
        })
    </script>
</section>
