
<?php
    $facility_name = [];
    $facility_name_en = [];
    $society_name = [];
    $society_name_en = [];
    if ($data->clinic_doctors) {
        foreach ($data->clinic_doctors as $key => $value) {
            if (!empty($value->customer->name_en)) {
                $facility_name_en[] = $value->customer->name_en;
            }
            if (!empty($value->customer->name)) {
                $facility_name[] = $value->customer->name;
            }
        }
    }
    if ($data->doctor_societies) {
        foreach ($data->doctor_societies as $key => $value) {
            if (!empty($value->society->name_en)) {
                $society_name_en[] = [
                    'name' => $value->society->name_en,
                    'url' => $value->society->url,
                ];
            }
            if (!empty($value->society->name)) {
                $society_name[] = [
                    'name' => $value->society->name,
                    'url' => $value->society->url,
                ];
            }
        }
    }
?>
<div class="row">
    <div class="col-lg-3 col-sm-3">
        <div class="profile-img">
            <?php
            $files = [];
            $profile = '';
            $priority = [];
            if (isset($data->medias)) {
                foreach ($data->medias as $file) {
                    if ($file->status) {
                        $profile = $file->file_name;
                    } else {
                        if ($file->file_priority) {
                            $priority[] = $file->file_name;
                        } else {
                            $files[] = $file->file_name;
                        }
                    }
                }
            }
            $file1 = implode(',', $files);
            $file2 = implode(',', $priority);
            echo $this->Form->hidden('files', [
                'value' => $file1,
                'class' => 'media-file',
            ]);
            echo $this->Form->hidden('priority', [
                'value' => $file2,
                'class' => 'media-priority',
            ]);
            if ($profile) {
                echo $this->Html->link(
                $this->Html->image('/img/uploads/' . $profile, [
                'class' => 'img-responsive']),
                '/img/uploads/' . $profile, [
                    'escape' => false,
                    'target' => '_blank',
                ]);
            } else {
                echo $this->Html->image('/images/no-image.png',[
                    'class' => 'img-responsive no-image',
                ]);
            }
            ?>
        </div>
    </div>
    <div class="col-lg-9 col-sm-9">
        <table class="table table-striped" id="tb-detail">
            <tbody>
                <tr>
                    <td><?php echo __('STR_CLINIC'); ?> :</td>
                    <td>
                        <?php
                        if ($facility_name) {
                            $element1 = '<ul class="email-wrap">';
                            foreach ($facility_name as $item1) {
                                $element1 .= '<li>' . $item1 . '</li>';
                            }
                            echo $element1 . '</ul>';
                        }
                        ?>
                    </td>
                    <td>
                        <?php
                        if ($facility_name_en) {
                            $element2 = '<ul class="email-wrap">';
                            foreach ($facility_name_en as $item2) {
                                $element2 .= '<li>' . $item2 . '</li>';
                            }
                            echo $element2 . '</ul>';
                        }
                        ?>
                    </td>
                </tr>
                <tr>
                    <td><?php echo __('DOCTOR_TXT_DESIGNATION'); ?> :</td>
                    <td colspan="2">
                        <?php
                        if ($data->clinic_doctors) {
                            $element = '<ul class="email-wrap">';
                            foreach ($data->clinic_doctors as $item) {
                                $element .= '<li>' . $item->position . '</li>';
                            }
                            echo $element . '</ul>';
                        }
                        ?>
                    </td>
                </tr>
                <tr>
                    <td><?php echo __('TXT_DOCTOR'); ?> :</td>
                    <td><?php echo $data->last_name . ' ' . $data->first_name; ?></td>
                    <td><?php echo $data->last_name_en . ' ' . $data->first_name_en; ?></td>
                </tr>
                <tr>
                    <td><?php echo __('DOCTOR_TXT_MAIDEN_NAME'); ?> :</td>
                    <td colspan="2"><?php echo $data->maiden_name; ?></td>
                </tr>
                <tr>
                    <td><?php echo __('DOCTOR_TXT_GENDER'); ?> :</td>
                    <td colspan="2"><?php echo ($data->gender == 1) ? 'Male' : 'Female'; ?></td>
                </tr>
                <tr>
                    <td><?php echo __('DOCTOR_TXT_DATE_OF_BIRTH'); ?> :</td>
                    <td colspan="2"><?php echo date('m-d-Y', strtotime($data->dob)); ?></td>
                </tr>
                <tr>
                    <td><?php echo __('DOCTOR_TXT_TEL'); ?> :</td>
                    <td colspan="2">
                        <?php
                        if ($data->info_details) {
                            $phone = '<ul class="email-wrap">';
                            foreach ($data->info_details as $item) {
                                $phone .= '<li>' . $item->phone . '</li>';
                            }
                            echo $phone . '</ul>';
                        }
                        ?>
                    </td>
                </tr>
                <tr>
                    <td><?php echo __('DOCTOR_TXT_EMAIL'); ?> :</td>
                    <td colspan="2">
                        <?php
                        if ($data->info_details) {
                            $element = '<ul class="email-wrap">';
                            foreach ($data->info_details as $item) {
                                $element .= '<li><a href="mailto:' . $item->email . '" target="_blank">' . $item->email . '</a></li>';
                            }
                            echo $element . '</ul>';
                        }
                        ?>
                    </td>
                </tr>
                <tr>
                    <td><?php echo __('DOCTOR_TXT_SOCIETY'); ?> :</td>
                    <td>
                        <?php
                        if ($society_name) {
                            $element1 = '<ul class="email-wrap">';
                            foreach ($society_name as $item1) {
                                $element1 .= '<li><a href="'.$item1['url'].'" target="_blank">' . $item1['name'] . '</a></li>';
                            }
                            echo $element1 . '</ul>';
                        }
                        ?>
                    </td>
                    <td>
                        <?php
                        if ($society_name_en) {
                            $element2 = '<ul class="email-wrap">';
                            foreach ($society_name_en as $item2) {
                                $element2 .= '<li><a href="'.$item2['url'].'" target="_blank">' . $item2['name'] . '</a></li>';
                            }
                            echo $element2 . '</ul>';
                        }
                        ?>
                    </td>
                </tr>
                <tr>
                    <td><?php echo __('DOCTOR_TXT_UNIVERSITY'); ?> :</td>
                    <td colspan="2"><?php echo $data->university; ?></td>
                </tr>
                <tr>
                    <td><?php echo __('DOCTOR_TXT_CAREER'); ?> :</td>
                    <td colspan="2"><?php echo $data->career; ?></td>
                </tr>
                <?php
                if (false) : ?>
                <tr>
                    <td><?php echo __('STR_TEL_EXT'); ?> :</td>
                    <td colspan="2">
                        <?php
                        if ($data->info_details) {
                            $tel = '<ul class="email-wrap">';
                            foreach ($data->info_details as $item) {
                                $tel .= '<li>' . $item->tel . '</li>';
                            }
                            echo $tel . '</ul>';
                        }
                        ?>
                    </td>
                </tr>
                <?php
                endif; ?>

                <tr>
                    <td><?php echo __('DOCTOR_TXT_REMARKS'); ?> :</td>
                    <td colspan="2">
                        <textarea readonly="readonly" style="background: none;"><?php echo $data->remarks; ?></textarea>
                    </td>
                </tr>
                <?php
                if (false) : ?>
                <tr>
                    <td><?php echo __('DOCTOR_TXT_DOCTOR_CERTIFICATE'); ?> :</td>
                    <td colspan="2"><?php echo $data->license_number; ?></td>
                </tr>
                <?php
                endif; ?>
            </tbody>
        </table>
        <section class="certificate-list">
            <fieldset class="fieldset-border">
                <legend><?php echo __('DOCTOR_TXT_DOCTOR_CERTIFICATE'); ?></legend>
                <div class="fieldset-content-wrap">
                    <?php echo $data->license_number; ?>
                    <?php
                    if (false) : ?>
                    <div class="priority_file"></div>
                    <div class="clearfix"></div>
                    <div class="normal_file"></div>
                    <?php
                    endif; ?>
                </div>
            </fieldset>
        </section>
    </div>
</div>
