
<?php
$destination = $this->request->query('des');
?>
<div class="search-section" style="margin-bottom: 20px;">
    <?php
    echo $this->Form->create('Doctors', [
        'role' => 'form',
        'class' => 'form-inline form-search',
        'type' => 'get',
        'name' => 'form_search'
    ]);
    echo $this->Form->hidden('displays', [
        'value' => empty($this->request->query('displays')) ? 10 : $this->request->query('displays')
    ]);
    echo $this->Form->input('keyword', [
        'name' => 'keyword',
        'placeholder' => __('USER_SEARCH_KEYWORD'),
        'class' => 'form-control',
        'label' => false,
        'value' => $this->request->query('keyword'),
        'autocomplete' => 'off',
        'templates' => [
            'inputContainer' => '<div class="form-group">{{content}}</div>',
        ]
    ]);
    echo $this->SwitchStatus->render();
    echo $this->Form->submit('submit', [
        'style' => 'display: none',
    ]);
    echo $this->Form->end();
    ?>
</div>
<div class="total-pagination" style="padding-bottom: 20px;">
    <?php echo $this->element('display_info'); ?>
</div>
<div class="row search-section" style="margin-bottom: 20px;">
    <?php echo $this->element('display_number'); ?>
    <div class="col-sm-6 text-center">
        <h2 class="title-page">
            <?php
            // if ($destination === TYPE_CUSTOMER) {
            //     //echo 'Customers';
            // }
            ?>
        </h2>
    </div>
    <?php //$this->ActionButtons->btnRegisterNew('Doctors', $params) ?>
    <div class="col-sm-3 pull-right" style="text-align: right;">
        <?php
        echo $this->Html->link(__('TXT_REGISTER_NEW'), '/doctors/create', [
            'type' => 'button',
            'class' => 'btn btn-sm btn-primary btn-width register-new-item',
            'data-id' => 'Doctors',
        ]);
        ?>
    </div>
    <div class="clearfix"></div>
</div>
<div class="data-tb-list">
    <?php if ($data): ?>
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>#</th>
                    <th>&nbsp;</th>
                    <th>&nbsp;</th>
                    <th>
                        <?php
                        $d_field = 'last_name' . $this->request->session()->read('tb_field');
                        echo $this->Paginator->sort($d_field, __('TXT_DOCTORE'));
                        ?>
                        <i class="fa fa-sort-desc" aria-hidden="true"></i>
                    </th>
                    <th>
                        <?php echo $this->Paginator->sort('gender', __('STR_GENDER')); ?>
                        <i class="fa fa-sort-desc" aria-hidden="true"></i>
                    </th>
                    <th width="1%">&nbsp;</th>
                    <th width="1%">&nbsp;</th>
                    <th width="1%">&nbsp;</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $numbering = $this->Paginator->counter('{{start}}');
                foreach ($data as $item) : ?>
                    <tr data-id="<?php echo h($item->id); ?>">
                        <th scope="row" class="td_evt"><?php echo $numbering; $numbering++; ?></th>
                        <td>
                            <?= $this->ActionButtons->disabledText($item->is_suspend) ?>
                        </td>
                        <td>
                            <?php
                            if (isset($item->medias)) {
                                if (!empty($item->medias)) {
                                    echo $this->Html->image('/img/uploads/' . $item->medias[0]->file_name, [
                                        'class' => 'img-responsive doctor-profile-view',
                                    ]);
                                }
                            }
                            ?>
                        </td>
                        <td>
                            <?php
                            if ($this->request->session()->read('tb_field') === '_en') {
                                echo h($item->first_name_en) . ' ' . h($item->last_name_en);
                            } else {
                                echo h($item->first_name) . ' ' . h($item->last_name);
                            }
                            ?>
                        </td>
                        <td><?php echo $item->gender ? __('STR_MALE') : __('STR_FEMALE'); ?></td>
                        <td>
                            <?php echo $this->Form->button('<i class="fa fa-comments-o" aria-hidden="true"></i>', [
                                'class' => 'btn btn-primary btn-sm btn_comment',
                                'data-id' => $item->id,
                                'escape' => false,
                            ]); ?>
                        </td>
                        <td data-target="<?php echo ($item->is_suspend) ? 1 : 0; ?>">
                            <?php
                            if ($item->is_suspend == 1) {
                                echo $this->Form->button(BTN_ICON_UNDO, [
                                    'class' => 'btn btn-recover btn-sm btn_suspend',
                                    'data-name' => 'recover',
                                    'escape' => false
                                ]);
                            } else {
                                $url = [
                                    'controller' => 'doctors',
                                    'action' => 'edit', $item->id
                                ];
                                if ($destination === TYPE_CUSTOMER) {
                                    $url = [
                                        'controller' => 'doctors',
                                        'action' => 'edit', $item->id,
                                        '?' => [
                                            'filter_by' => TYPE_CUSTOMER,
                                            'des' => TYPE_CUSTOMER,
                                            'external_id' => $this->request->query('external_id'),
                                        ]
                                    ];
                                }
                                echo $this->Html->link(BTN_ICON_EDIT, $url, [
                                    'class' => 'btn btn-primary btn-sm',
                                    'escape' => false
                                ]);
                            }
                            ?>
                        </td>
                        <td data-target="<?php echo ($item->is_suspend) ? 1 : 0; ?>">
                            <?php
                            if ($item->is_suspend == 1) {
                                if (!empty($item->product_brands)) {
                                    echo $this->Form->button(BTN_ICON_DELETE, [
                                        'class' => 'btn btn-delete btn-sm',
                                        'id' => 'btn_delete',
                                        'disabled' => 'disabled',
                                        'escape' => false
                                    ]);
                                } else {
                                    echo $this->Form->button(BTN_ICON_DELETE, [
                                        'class' => 'btn btn-delete btn-sm',
                                        'id' => 'btn_delete',
                                        'data-name' => 'delete',
                                        'escape' => false
                                    ]);
                                }
                            } else {
                                echo $this->Form->button(BTN_ICON_STOP, [
                                    'class' => 'btn btn-suspend btn-sm btn_suspend',
                                    'data-name' => 'suspend',
                                    'escape' => false
                                ]);
                            }
                            ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    <?php endif; ?>
</div>
<div class="row row-top-space" style="padding-bottom: 20px;">
    <?php echo $this->element('display_number'); ?>
    <?= $this->element('next_prev') ?>
    <div class="clearfix"></div>
</div>

<!--Modal Detail-->
<div class="modal fade" id="modal_detail" data-backdrop="static" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-body"></div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default md-footer-btn btn-sm btn-close-modal" data-dismiss="modal"><?= __('BTN_CANCEL') ?></button>
            </div>
        </div>
    </div>
</div>
<!--Modal Comment-->
<?php echo $this->element('/Modal/message') ?>
<!--Modal Suspend-->
<?php echo $this->element('Modal/suspend'); ?>
<!--Modal delete-->
<?php echo $this->element('Modal/delete'); ?>

<script>
    (function (e) {
        var local = '<?= $this->request->session()->read('tb_field') ?>';

        //auto complete
        $('body').find('#keyword').typeahead({
            minLength: 0,
            autoSelect: true,
            source:  function (query, process) {
                return $.get('<?= $this->Url->build(['action' => 'getDoctorBySearch']) ?>', {
                        keyword: query
                    }, function (data) {
                        var optionData = [];
                        if (data.data !== null && data.data !== 'undefined') {
                            $.each(data.data, function(i, v) {
                                if (local === '_en') {
                                    var name = ''
                                    if (v.first_name_en !== '') {
                                        name += v.first_name_en + ' ';
                                    }
                                    if (v.last_name_en !== '') {
                                        name += v.last_name_en;
                                    }
                                    optionData.push(name);
                                } else {
                                    var name = '';
                                    if (v.last_name !== '') {
                                        name += v.last_name  + ' ';
                                    }
                                    if (v.first_name !== '') {
                                        name += v.first_name;
                                    }
                                    optionData.push(name);
                                }
                            });
                        }
                        return process(optionData);
                    }, 'json');
            }
        });

        $('body').on('click', '.typeahead li', function(e) {
            $('body').find('.form-search').submit();
        });

        $('body').on('click', '.data-tb-list .table>tbody>tr', function (e) {
            $.LoadingOverlay('show');
            var id = $(this).closest('tr').data('id');

            $.get('<?php echo $this->Url->build('/doctors/view/'); ?>' + id, function (data) {
                $.LoadingOverlay('hide');
                $('#modal_detail').find('.modal-body').html(data);
                $('#modal_detail').modal('show');
                var files = $('#modal_detail').find('.media-file').val();
                var priority = $('#modal_detail').find('.media-priority').val();
                var medias = [];
                var file_priority = [];
                var content = $('body').find('#modal_detail .modal-body .fieldset-border');

                if (priority) {
                    file_priority = priority.split(',');
                    display_media(content, file_priority, 1);
                }
                if (files) {
                    medias = files.split(',');
                    display_media(content, medias, 0);
                }
            }, 'html');
        });

        $('.btn_comment').click(function (e) {
            e.stopPropagation();
            let params = {
                id: $(this).closest('tr').attr('data-id'),
                type: '<?php echo TYPE_PERSON_IN_CHARGE ?>',
                referer: '<?php echo $this->request->controller; ?>'
            };
            let options = {
                url: BASE + 'Messages/getMessageList',
                type: 'GET',
                data: params,
                beforeSend: function () {
                    $.LoadingOverlay('show');
                }
            };
            ajaxRequest(options, SESSION_TIMEOUT, function (data) {
                $('body').prepend(data);
                $('body').find('.modal-message').modal('show');
            });
        });

        //filter message list search to specific
        $('body').on('click', '.btn-to-individual', function() {
            $(this).removeClass('btn-detault').addClass('btn-primary');
            $('body').find('.btn-to-all').removeClass('btn-primary').addClass('btn-default');
            var toAll = $('.to-individual').val();
            if (parseInt(toAll) === 0) {
                $('.to-all').val(0);
                $('.to-individual').val(1);
            } else {
                $('.to-all').val(1);
                $('.to-individual').val(0);
            }
            messagesSearch(listUrlSearch);
        });

        $('body').on('click', '.btn-search', function() {
            messagesSearch(listUrlSearch);
        });

        $('body').on('click', '.btn-suspend-msg', function() {
            var id = $(this).data('id');
            var data = $('#form-message-status-' + id).serialize();
            var suspendUrl = '<?= $this->Url->build(['controller' => 'Messages', 'action' => 'suspend']) ?>';
            suspendMessage(id, suspendUrl, data);
        });

        $('body').on('click', '.btn_suspend', function (e) {
            e.stopPropagation();
            var content = $('#modal_suspend');
            var status = $(this).attr('data-name');
            var txt = '';

            switch (status) {
                case 'suspend':
                    $('#btn_suspend_yes').text('<?= __('BTN_SUSPEND') ?>');
                    txt = '<?= __('MSG_CONFIRM_SUSPEND') ?>';
                    break;
                case 'delete':
                    txt = '<?= __('MSG_CONFIRM_DELETE') ?>';
                    $('#btn_suspend_yes').text('<?= __('BTN_DELETE') ?>');
                    break;
                 case 'recover':
                    $('#btn_suspend_yes').text('<?= __('BTN_RECOVER') ?>');
                    txt = '<?= __('MSG_CONFIRM_UNDO_SUSPEND') ?>';
                    break;
            };

            $(content).find('#btn_suspend_yes').data('id', $(this).closest('tr').data('id'));
            $(content).find('#btn_suspend_yes').data('target', $(this).closest('td').data('target'));
            $(content).find('.modal-body p').html(txt);
            $(content).modal('show');
        });

        $('body').on('click', '#btn_suspend_yes', function (e) {
            var params = {
                id: $(this).data('id'),
                is_suspend: $(this).data('target')
            };
            $.post('<?php echo $this->Url->build('/doctors/update-suspend/'); ?>', params, function (data) {
                if (data.message === 'success') {
                    location.reload();
                }
            }, 'json');
        });

        $('body').on('click', '#btn_delete', function (e) {
            e.stopPropagation();
            var content = $('#modal_delete');
            $(content).find('#btn_delete_yes').data('id', $(this).closest('tr').data('id'));
            $(content).modal('show');
        });

        $('body').on('click', '#btn_delete_yes', function (e) {
            $.post('<?php echo $this->Url->build('/doctors/delete/'); ?>', {id: $(this).data('id')}, function (data) {
                if (data.message === 'success') {
                    location.reload();
                }
            }, 'json');
        });

        $('body').on('click', '.tt-selectable', function (e) {
            $('form[name="form_search"]').submit();
        });

        $('body').on('click', '.table>tbody>tr>td a', function(e) {
            e.stopPropagation();
        });

        pageOutOfRange(<?= $paging ?>, '<?= $this->request->query('page') ?>');
        var maxPage = parseInt($('#maxPage').val());
        updatePagination(maxPage);

        // display media list
        function display_media(content, list, is_priority)
        {
            if (list != null && list != undefined) {
                var element = '<div class="alert alert-warning" role="alert"><?php echo __('STR_PRIORITY'); ?></div>';
                if (is_priority == 0) {
                    element = '<div class="alert alert-warning" role="alert"><?php echo __('STR_COMMENTS_NORMAL'); ?></div>';
                }
                $.each(list, function(i, v) {
                    var path = BASE_URL + 'img/uploads/' + v;
                    var extension = v.split('.').pop().toLowerCase();
                    element += '<div class="col-md-3 col-lg-3 col-sm-3 media-list-wrap">' + dispay_html(v, path, is_priority, extension, i) + '</div>';
                });
                if (is_priority == 1) {
                    $(content).find('.priority_file').append(element);
                } else {
                    $(content).find('.normal_file').append(element);
                }
            }
        }

        function dispay_html(data, path, is_priority, extension, i)
        {
            var icon_check = '';
            var canvas = '';
            var img = '';

            if (is_priority == 1) {
                icon_check = '<div class="img-tick"><span><i class="fa fa-check" aria-hidden="true"></i></span></div>';
            }

            if (extension === 'pdf') {
               canvas = '<canvas id="the-canvas-' + i + '"></canvas>';
            } else {
                img = '<img src="' + path + '" class="open-main-image">';
            }
            var element = '<section class="thumbnail select_img">'
                        + '<a href="' + path + '" target="_blank">' + img + canvas + icon_check + '</a></section>';

            if (extension === 'pdf') {
               read_pdf_file(path, i);
            }

            return element;
        }

        function read_pdf_file(path, id)
        {
            var full_path = location.protocol + '//' + location.host + path;

            PDFJS.getDocument(full_path).promise.then(function (pdf) {
                pdf.getPage(1).then(function getPageHelloWorld(page) {
                    var scale = 1.5;
                    var viewport = page.getViewport(scale);
                    var canvas = $('body').find('#the-canvas-' + id)[0];
                    var context = canvas.getContext('2d');
                    canvas.height = viewport.height;
                    canvas.width = viewport.width;
                    page.render({canvasContext: context, viewport: viewport})
                });
            });
        }
    })();
</script>
<?php
echo $this->Html->script([
    'processing-api.min',
    'pdf',
], ['block' => 'script']);
