<?php
ini_set('memory_limit', '-1');
//header("Content-Type: application/pdf");
//header("Pragma: public");
//header("Expires: 0");
//header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
//header("Content-Type: application/force-download");
//header("Content-Type: application/octet-stream");
//header("Content-Type: application/download");
//header('Content-Disposition: attachment; filename="a.pdf"');
//header("Content-Transfer-Encoding: binary ");

// Extend the TCPDF class to create custom Header and Footer
class MYPDF extends TCPDF {

    //Page header
    public function Header() {
//        // Logo
//        $image_file = K_PATH_IMAGES.'logo_example.jpg';
//        $this->Image($image_file, 10, 10, 15, '', 'JPG', '', 'T', false, 300, '', false, false, 0, false, false, false);
//        // Set font
//        $this->SetFont('helvetica', 'B', 20);
//        // Title
//        $this->Cell(0, 15, '<< TCPDF Example 003 >>', 0, false, 'C', 0, '', 0, false, 'M', 'M');
    }

    // Page footer
    public function Footer() {
        // Position at 15 mm from bottom
        $this->SetY(-15);
        // Set font
        $this->SetFont('helvetica', 'I', 8);
        // Page number
        $this->Cell(0, 10, $this->getAliasNumPage().'/'.$this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');
    }
}


// create new PDF document
// $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
$pdf = new MYPDF('L', 'mm', 'A4', true, 'UTF-8');
// Reset the encoding forced from tcpdf
mb_internal_encoding('UTF-8');

$font_jp = TCPDF_FONTS::addTTFfont(WWW_ROOT.'fonts/ipag.ttf');
//$pdf->SetFont($font1, '', 14);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Nick');
$pdf->SetTitle(h($clinic_name));
$pdf->SetSubject(h($clinic_name));
$pdf->SetKeywords('');

// Configure header and footer
$pdf->SetPrintHeader(false);
$pdf->SetPrintFooter(true);

// Load CSS
//TCPDF::getHtmlDomArray();

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
	require_once(dirname(__FILE__).'/lang/eng.php');
	$pdf->setLanguageArray($l);
}

// ---------------------------------------------------------

// set default font subsetting mode
$pdf->setFontSubsetting(true);

// Add a page
// This method has several options, check the source code documentation for more information.
$pdf->AddPage();

// Sett default line styel
$pdf->SetLineStyle(array('width' => 0.1, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(221, 221, 221)));
// product name
$pdf->SetFont($font_jp, '', 14);
$pdf->Cell(40,10, $clinic_name, 0);
$pdf->ln();

$pdf->SetFont($font_jp, '', 8);
$pdf->Cell(30,6, '購入回数', 0);
$pdf->SetFont('Helvetica', '', 8);
$pdf->Cell(30,6, number_format($data['Header']['total_order']), 0);

$pdf->SetFont($font_jp, '', 8);
$pdf->Cell(25,6, '購入間隔', 0);
$pdf->SetFont($font_jp, '', 8);
$pdf->Cell(30,6, round($data['Header']['average_total_order_date'], 2).'日 (約 '. round($data['Header']['average_total_order_date']/30, 2).' ヶ月)', 0);
$pdf->ln();

$pdf->SetFont($font_jp, '', 8);
$pdf->Cell(30,6, '購入額', 0);
$pdf->SetFont($font_jp, '', 8);
$pdf->Cell(30,6, number_format($data['Header']['total_amount']).' 円', 0);

$pdf->SetFont($font_jp, '', 8);
$pdf->Cell(25,6, '１回あたり購入額', 0);
$pdf->SetFont($font_jp, '', 8);
$pdf->Cell(30,6, number_format($data['Header']['total_average_amount']).' 円／回', 0);
$pdf->ln(10);

$ch = 5;
$cw = 0;

//width for column clinic name
$col_clinic_name = 28;
//width for column year(1-12)
$col_year = 8;
//width of each year
$col_each_year = 20;
//width of qty
$col_qty = 7;
//width of amount
$col_amount = 13;
// Table head
$pdf->SetFont($font_jp, '', 6);
$pdf->Cell($col_clinic_name, $ch * 3, '', 1, false, 'C');
$pdf->Cell($col_year, $ch * 3, '', 1, false, 'C');
for ($i = 1; $i <= 12; $i++) {
    $pdf->Cell($col_each_year, $ch * 2, $i.'月', 1, false, 'C');
}
$pdf->ln();
$pdf->Cell($col_clinic_name, $ch * 3, '', 0, false, 'C');
$pdf->Cell($col_year, $ch * 3, '', 0, false, 'C');
for ($i = 1; $i <= 12; $i++) {
    //Column qty
    $pdf->Cell($col_qty,$ch, '個数', 1, false, 'C');
    //Column amount
    $pdf->Cell($col_amount,$ch, '単価', 1, false, 'C');
}
$pdf->ln();

if (!empty($data)) {
    $index = 1;
    $ind = 1;
    foreach ($data['Body'] as $key => $value) {
        // get first order date and last order date for calculation
        $start_date = strtotime(date('Y-m', reset($value['date'])));
        $end_date = strtotime(date('Y-m', end($value['date'])));

        if ($index % 2 == 0) {
            $class_row = 'eve';
            $pdf->SetLineStyle(array('width' => 0.1, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(221, 221, 221)));
            $pdf->SetFillColor(255,255,255);
        } else {
            $class_row = 'odd';
            $pdf->SetLineStyle(array('width' => 0.1, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(221, 221, 221)));
            $pdf->SetFillColor(245,245,245);
        }

        $year = array_keys($value['year'][1]);
        //Clinic name sample cell
        $pdf->MultiCell($width = $col_clinic_name, $height = $ch * 3, $text = h($key), $border = 1, $align = 'L', $fill=true, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = $ch * 3, $valign = 'M');
    //Row 1 or last year
        //last year value
        $pdf->Cell($col_year,$ch, $year[2], 1, false, 'C', 1, 0);
        foreach ($value['year'] as $key1 => $value1) {
            $d = strtotime($year[2].'-'.$key1);
            $number_order1 = count($value1[$year[2]]['qty']);
            $sum_qty1 = array_sum(array_values($value1[$year[2]]['qty']));
            $s_total1 = 0;
            if ($number_order1 > 0 ) {
                //qty
                $pdf->Cell($col_qty,$ch, number_format($sum_qty1), 1, false, 'C', 1, 0);
                for ($in = 0; $in < count($value1[$year[2]]['qty']); $in++) {
                    $sub_total1 = $value1[$year[2]]['qty'][$in] * $value1[$year[2]]['unit_price'][$in];
                    $s_total1 += $sub_total1;
                }
            } else {
                if ($start_date <= $d && $end_date >= $d) {
                    $pdf->Cell($col_qty, $ch, '-', 1, false, 'C', 1, 0);
                } else {
                    $pdf->Cell($col_qty, $ch, '', 1, false, 'C', 1, 0);
                }
            }
            if ($number_order1 > 0) {
                //amount
                $pdf->Cell($col_amount,$ch, number_format($s_total1/$sum_qty1), 1, false, 'R', 1, 0);
            } else {
                if ($start_date <= $d && $end_date >= $d) {
                    $pdf->Cell($col_amount, $ch, '-', 1, false, 'C', 1, 0);
                } else {
                    $pdf->Cell($col_amount, $ch, '', 1, false, 'C', 1, 0);
                }
            }
        }
        $pdf->ln();
    //Row 2 or previous
        $pdf->MultiCell($col_clinic_name, $ch, '', 0, 'L', 0, 0);
        //previous year value
        $pdf->Cell($col_year,$ch, $year[1], 1, false, 'C', 1, 0);
        foreach ($value['year'] as $key2 => $value2) {
            $d = strtotime($year[1].'-'.$key2);
            $number_order2 = count($value2[$year[1]]['qty']);
            $sum_qty2 = array_sum(array_values($value2[$year[1]]['qty']));
            $s_total2 = 0;
            if ($number_order2 > 0 ) {
                //qty
                $pdf->Cell($col_qty,$ch, number_format($sum_qty2), 1, false, 'C', 1, 0);
                for ($in = 0; $in < $number_order2; $in++) {
                    $sub_total2 = $value2[$year[1]]['qty'][$in] * $value2[$year[1]]['unit_price'][$in];
                    $s_total2 += $sub_total2;
                }
            } else {
                if ($start_date <= $d && $end_date >= $d) {
                    $pdf->Cell($col_qty, $ch, '-', 1, false, 'C', 1, 0);
                } else {
                    $pdf->Cell($col_qty, $ch, '', 1, false, 'C', 1, 0);
                }
            }
            if($number_order2 > 0) {
                //amount
                $pdf->Cell($col_amount,$ch, number_format($s_total2/$sum_qty2), 1, false, 'R', 1, 0);
            } else {
                if ($start_date <= $d && $end_date >= $d) {
                    $pdf->Cell($col_amount, $ch, '-', 1, false, 'C', 1, 0);
                } else {
                    $pdf->Cell($col_amount, $ch, '', 1, false, 'C', 1, 0);
                }
            }
        }
        $pdf->ln();
    // Row 3
        $pdf->MultiCell($col_clinic_name, $ch, '', 0, 'C', 0, 0);
        //previous year value
        $pdf->Cell($col_year,$ch, $year[0], 1, false, 'C', 1, 0);
        foreach ($value['year'] as $key3 => $value3) {
            $d = strtotime($year[0].'-'.$key3);
            $number_order3 = count($value3[$year[0]]['qty']);
            $sum_qty3 = array_sum(array_values($value3[$year[0]]['qty']));
            $s_total3 = 0;
            if ($number_order3 > 0 ) {
                //qty
                $pdf->Cell($col_qty, $ch, number_format($sum_qty3), 1, false, 'C', 1, 0);
                for ($in = 0; $in < $number_order3; $in++) {
                    $sub_total3 = $value3[$year[0]]['qty'][$in] * $value3[$year[0]]['unit_price'][$in];
                    $s_total3 += $sub_total3;
                }
            } else {
                if ($start_date <= $d && $end_date >= $d) {
                    $pdf->Cell($col_qty, $ch, '-', 1, false, 'C', 1, 0);
                } else {
                    $pdf->Cell($col_qty, $ch, '', 1, false, 'C', 1, 0);
                }
            }
            if ($number_order3 > 0) {
                //amount
                $pdf->Cell($col_amount, $ch, number_format($s_total3/$sum_qty3), 1, false, 'R', 1, 0);
            } else {
                if ($start_date <= $d && $end_date >= $d) {
                    $pdf->Cell($col_amount, $ch, '-', 1, false, 'C', 1, 0);
                } else {
                    $pdf->Cell($col_amount, $ch, '', 1, false, 'C', 1, 0);
                }
            }
        }
        $pdf->ln();

        // page break
        if ($index == 8) {
            $ind = 0;
        }
        if ($ind % 9 === 0) {
            $pdf->AddPage();
            // product name
            $pdf->SetFont($font_jp, '', 14);
            $pdf->Cell(60,10, h($clinic_name), 0);
            $pdf->ln();
            $pdf->SetFont($font_jp, '', 6);
            $pdf->Cell($col_clinic_name, $ch * 3, '', 1, false, 'C');
            $pdf->Cell($col_year, $ch * 3, '', 1, false, 'C');
            for ($i = 1; $i <= 12; $i++) {
                $pdf->Cell($col_each_year, $ch * 2, $i.'月', 1, false, 'C');
            }
            $pdf->ln();
            $pdf->Cell($col_clinic_name, $ch * 3, '', 0, false, 'C');
            $pdf->Cell($col_year, $ch * 3, '', 0, false, 'C');
            for ($i = 1; $i <= 12; $i++) {
                //Column qty
                $pdf->Cell($col_qty,$ch, '個数', 1, false, 'C');
                //Column amount
                $pdf->Cell($col_amount,$ch, '単価', 1, false, 'C');
            }
            $pdf->ln();
        }
        $index++;
        $ind++;
    }

    $pdf->SetLineStyle(array('width' => 0.1, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(221, 221, 221)));

} // End check empty $data (Convention)


// ---------------------------------------------------------

// Close and output PDF document
// This method has several options, check the source code documentation for more information.
$pdf->Output();
