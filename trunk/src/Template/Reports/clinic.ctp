<?php $this->assign('title', 'クリニック別売上レポート'); ?>
<div class="row row-middle-space">
    <div class="col-md-12">
        <?php
            echo $this->Form->create('clinic', [
                'type' => 'get',
                'id' => 'form-search',
                'autocomplete' => 'off',
                'class' => 'form-horizontal'
            ]);
            $this->Form->templates([
                'inputContainer' => '{{content}}'
            ]);
        ?>
        <div class="form-group">
            <div class="col-md-4">
            <?php
                echo $this->Form->input('name', [
                    'id' => 'keyword-search',
                    'label' => false,
                    'class' => 'form-control',
                    'placeholder' => 'クリニック名を入力してください',
                    'value' => $this->request->query('name'),
                    'escape' => false,
                    'required' => false,
                    'autocomplete' => 'off'
                ]);
            ?>
            </div>
        </div>
        <?php echo $this->Form->end(); ?>
    </div>
</div>
<div class="row">
<?php if (!empty($data)) : ?>
    <div class="col-md-12">
        <h3><strong><?= trim($this->request->query('name')) ?></strong><span class="header-date">（<?= $min_date . '-' . $max_date ?>）</span></h3> <!-- echo date('Y/m/01', strtotime ($data['Header']['min_date'])) .' - '. date('Y/m/t', strtotime ($data['Header']['max_date'])); -->
        <div class="table-responsive">
            <table class="table table-noborder">
                <tr>
                    <td width="100"><strong><span class="glyphicon glyphicon-stop" aria-hidden="true"></span> 期間合計</strong></td>
                    <td width="150"></td>
                    <td width="150"><strong><span class="glyphicon glyphicon-stop" aria-hidden="true"></span> 期間平均</strong></td>
                    <td></td>
                </tr>
                <tr>
                    <td width="100"><strong>&ensp; 購入回数</strong></td>
                    <td width="150">: <?php echo h(number_format($data['Header']['total_order'])); ?> 回</td>
                    <td width="150"><strong>&ensp; 購入間隔</strong></td>
                    <td>:
                        <?php
                            echo round($data['Header']['average_total_order_date'], 2).'日 (約 '. round($data['Header']['average_total_order_date']/30, 2).' ヶ月)';
                        ?>
                    </td>
                </tr>
                <tr>
                    <td width="100"><strong>&ensp; 購入額</strong></td>
                    <td width="150">: <?php echo h(number_format($data['Header']['total_amount'])); ?> 円</td>
                    <td width="150"><strong>&ensp; １回あたり購入額</strong></td>
                    <td>: <?php echo h(number_format($data['Header']['total_average_amount']).' 円／回'); ?></td>
                </tr>
            </table>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-6 col-md-offset-6 text-right">
                <a href="<?= $this->Url->build(['controller' => 'Reports', 'action' => 'clinicPdf', '?' => ['name' => $this->request->query('name')]]) ?>" class="btn btn-info text-right" target="_blank">PDF</a>
            </div>
        </div>
        <a href="javascript:void(0)" class="btn btn-default btn-xs btn-zoom" data-zoom="show">-</a>
        <div class="table-responsive table-fixed-row-column" id="table-fixed-row-column">
            <table class="table table-bordered custom-table">
                <thead>
                    <tr>
                        <th><div>&nbsp;</div></th>
                        <th><div  style="width: 60px;">&nbsp;</div></th>
                        <th class="a-zoom">
                            <p class="p-text">期間集計</p>
                            <p class="p-text"><?= $min_date . '-' . $max_date ?></p>
                            <div class="f-border-row">
                                <div class="border-row-item">合計</div>
                                <div class="border-row-item">平均</div>
                            </div>
                        </th>
                        <th>
                            <p  class="p-text p-day">1月</p>
                            <div class="border-row">
                                <div class="border-row-item">個数</div>
                                <div class="border-row-item">単価</div>
                            </div>
                        </th>
                        <th>
                            <p  class="p-text p-day">2月</p>
                            <div class="border-row">
                                <div class="border-row-item">個数</div>
                                <div class="border-row-item">単価</div>
                            </div>
                        </th>
                        <th>
                            <p  class="p-text p-day">3月</p>
                            <div class="border-row">
                                <div class="border-row-item">個数</div>
                                <div class="border-row-item">単価</div>
                            </div>
                        </th>
                        <th>
                            <p  class="p-text p-day">4月</p>
                            <div class="border-row">
                                <div class="border-row-item">個数</div>
                                <div class="border-row-item">単価</div>
                            </div>
                        </th>
                        <th>
                            <p  class="p-text p-day">5月</p>
                            <div class="border-row">
                                <div class="border-row-item">個数</div>
                                <div class="border-row-item">単価</div>
                            </div>
                        </th>
                        <th>
                            <p  class="p-text p-day">6月</p>
                            <div class="border-row">
                                <div class="border-row-item">個数</div>
                                <div class="border-row-item">単価</div>
                            </div>
                        </th>
                        <th>
                            <p  class="p-text p-day">7月</p>
                            <div class="border-row">
                                <div class="border-row-item">個数</div>
                                <div class="border-row-item">単価</div>
                            </div>
                        </th>
                        <th>
                            <p  class="p-text p-day">8月</p>
                            <div class="border-row">
                                <div class="border-row-item">個数</div>
                                <div class="border-row-item">単価</div>
                            </div>
                        </th>
                        <th>
                            <p  class="p-text p-day">9月</p>
                            <div class="border-row">
                                <div class="border-row-item">個数</div>
                                <div class="border-row-item">単価</div>
                            </div>
                        </th>
                        <th>
                            <p  class="p-text p-day">10月</p>
                            <div class="border-row">
                                <div class="border-row-item">個数</div>
                                <div class="border-row-item">単価</div>
                            </div>
                        </th>
                        <th>
                            <p  class="p-text p-day">11月</p>
                            <div class="border-row">
                                <div class="border-row-item">個数</div>
                                <div class="border-row-item">単価</div>
                            </div>
                        </th>
                        <th>
                            <p  class="p-text p-day">12月</p>
                            <div class="border-row">
                                <div class="border-row-item">個数</div>
                                <div class="border-row-item">単価</div>
                            </div>
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        $i = 0;
                        foreach ($data['Body'] as $key => $value) :
                            if (($i % 2) == 1) {
                                $class = ' class="active"';
                            } else {
                                $class = '';
                            }
                            $i++;

                            // get first order date and last order date for calculation
                            $start_date = strtotime(date('Y-m', reset($value['date'])));
                            $end_date = strtotime(date('Y-m', end($value['date'])));
                    ?>
                    <tr<?= $class ?>>
                        <td rowspan="3" class="text-left"><?= h($key) ?></td>
                        <td><?php $year = array_keys($value['year'][1]); echo $year[2]; ?></td>
                        <td class="a-zoom">
                            <div class="row-data">
                                <div class="rows-items left-item">
                                    <div class="row-item-top">購入回数</div>
                                    <div class="row-item-bottom"><?= number_format($value['total_order']) ?> 回</div>
                                </div>
                                <div class="rows-items right-item">
                                    <div class="row-item-top">購入間隔</div>
                                    <div class="row-item-bottom"><?= round($value['average_order'], 2) ?> ヶ月</div>
                                </div>
                            </div>
                        </td>
                        <?php foreach ($value['year'] as $key1 => $value1) : ?>
                        <td>
                            <div class="row-value">
                                <div class="value-item">
                                    <?php
                                        $d = strtotime($year[2].'-'.$key1);
                                        $number_order1 = count($value1[$year[2]]['qty']);
                                        $sum_qty1 = array_sum(array_values($value1[$year[2]]['qty']));
                                        $s_total1 = 0;
                                        if($number_order1 > 0 ) {
                                            echo number_format($sum_qty1);
                                            for ($in = 0; $in < count($value1[$year[2]]['qty']); $in++) {
                                                $sub_total1 = $value1[$year[2]]['qty'][$in] * $value1[$year[2]]['unit_price'][$in];
                                                $s_total1 += $sub_total1;
                                                //echo '<br>'.$value2[$year[1]]['qty'][$in].'=>'.$value2[$year[1]]['unit_price'][$in];
                                            }
                                        } else {
                                            if ($start_date <= $d && $end_date >= $d) {
                                                echo '-';
                                            } else {
                                                echo '&nbsp;';
                                                //echo $start_date.'<='.$d;
                                            }
                                        }
                                    ?>
                                </div>
                                <div class="value-item">
                                    <?php
                                    // 単価 (Average unit_price of the product purchased by the clinic in the month)
                                        if($number_order1 > 0) {
                                            echo number_format($s_total1/$sum_qty1).' 円';
                                        } else {
                                            if ($start_date <= $d && $end_date >= $d) {
                                                echo '-';
                                            } else {
                                                echo '&nbsp;';
                                                //echo $start_date.'<='.$d;
                                            }
                                        }
                                    ?>
                                </div>
                            </div>
                        </td>
                        <?php endforeach; ?>
                    </tr>
                    <tr<?= $class ?>>
                        <td><?= $year[1] ?></td>
                        <td class="a-zoom">
                            <div class="row-data">
                                <div class="rows-items left-item">
                                    <div class="row-item-top">購入金額</div>
                                    <div class="row-item-bottom"><?= number_format($value['total_unit_price']) ?> 円</div>
                                </div>
                                <div class="rows-items right-item">
                                    <div class="row-item-top">１回あたり購入額</div>
                                    <div class="row-item-bottom"><?= number_format($value['total_unit_price']/$value['total_order']); ?> 円／回</div>
                                </div>
                            </div>
                        </td>

                        <?php foreach ($value['year'] as $key2 => $value2) : ?>
                        <td>
                            <div class="row-value">
                                <div class="value-item">
                                    <?php
                                        $d = strtotime($year[1].'-'.$key2);
                                        $number_order2 = count($value2[$year[1]]['qty']);
                                        $sum_qty2 = array_sum(array_values($value2[$year[1]]['qty']));
                                        $s_total2 = 0;
                                        if($number_order2 > 0 ) {
                                            echo number_format($sum_qty2);
                                            for ($in = 0; $in < count($value2[$year[1]]['qty']); $in++) {
                                                $sub_total2 = $value2[$year[1]]['qty'][$in] * $value2[$year[1]]['unit_price'][$in];
                                                $s_total2 += $sub_total2;
                                                //echo '<br>'.$value2[$year[1]]['qty'][$in].'=>'.$value2[$year[1]]['unit_price'][$in];
                                            }
                                        } else {
                                            if ($start_date <= $d && $end_date >= $d) {
                                                echo '-';
                                            } else {
                                                echo '&nbsp;';
                                                //echo $start_date.'<='.$d;
                                            }
                                        }
                                    ?>
                                </div>
                                <div class="value-item">
                                    <?php
                                        if($number_order2 > 0) {
                                            echo number_format($s_total2/$sum_qty2).' 円';
                                        } else {
                                            if ($start_date <= $d && $end_date >= $d) {
                                                echo '-';
                                            } else {
                                                echo '&nbsp;';
                                                //echo $start_date.'<='.$d;
                                            }
                                        }
                                    ?>
                                </div>
                            </div>
                        </td>
                        <?php endforeach;?>
                    </tr>
                    <tr<?= $class ?>>
                        <td><?php echo $year[0]; ?></td>
                        <td class="row-value a-zoom">&nbsp;</td>
                        <?php foreach ($value['year'] as $key3 => $value3) : ?>
                        <td>
                            <div class="row-value">
                                <div class="value-item">
                                    <?php
                                        $d = strtotime($year[0].'-'.$key3);
                                        $number_order3 = count($value3[$year[0]]['qty']);
                                        $sum_qty3 = array_sum(array_values($value3[$year[0]]['qty']));
                                        $s_total3 = 0;
                                        if($number_order3 > 0 ) {
                                            echo number_format($sum_qty3);
                                            for ($in = 0; $in < count($value3[$year[0]]['qty']); $in++) {
                                                $sub_total3 = $value3[$year[0]]['qty'][$in] * $value3[$year[0]]['unit_price'][$in];
                                                $s_total3 += $sub_total3;
                                            }
                                        } else {
                                            if ($start_date <= $d && $end_date >= $d) {
                                                echo '-';
                                            } else {
                                                echo '&nbsp;';
                                                //echo $start_date.'<='.$d;
                                            }
                                        }
                                    ?>
                                </div>
                                <div class="value-item">
                                    <?php
                                        if($number_order3 > 0) {
                                            echo number_format($s_total3/$sum_qty3).' 円';
                                        } else {
                                            if ($start_date <= $d && $end_date >= $d) {
                                                echo '-';
                                            } else {
                                                echo '&nbsp;';
                                                //echo $start_date.'<='.$d;
                                            }
                                        }
                                    ?>
                                </div>
                            </div>
                        </td>
                        <?php endforeach;?>
                    </tr>
                    <?php if ($i == 5) { $i = 0; } ?>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
    <?php
    else :
        if ($this->request->query('name')) {
    ?>
    <div class="col-md-12 text-center">
        <p><?= __('検索されたクリニックが見つかりません。') ?></p>
    </div>
    <?php
        }
    endif;
    ?>
</div>

<script>
    $(function() {
        var dataSource = <?= $name ?>;
        if (typeof dataSource === 'object') {
            dataSource = Object.values(dataSource);
        }
        $('#keyword-search').autocomplete({
            source: dataSource,
            minLength: 1,
            open: function(event, ui) {
                $(this).autocomplete('widget').css({
                    'width': 360
                });
            },
            select: function( event, ui ) {
                $('#keyword-search').val(ui.item.value);
                $('#form-search').submit();
            }
        });

        $('.custom-table').tableHeadFixer({'left' : 2, 'z-index': 100});
        // show/hidde button
        btn_zoom_auto_position('.btn-zoom', '.custom-table>thead>tr>th', 'show');
        function btn_zoom_auto_position(select, target, s = null){
            var _this = $(select);
            var th = $(target);
            var mar = th.eq(0).width() + th.eq(1).width();
            var left = th.eq(2).width() / 2;
            if (s == 'show') {
                mar+=left;
            }
            _this.css('margin-left', mar);
        }
        $('body').on('click', '.btn-zoom', function(){
            var _this = $(this);
            if (_this.attr('data-zoom') == 'show') {
                _this.attr('data-zoom', 'hide');
                _this.html('+');
                btn_zoom_auto_position('.btn-zoom', '.custom-table>thead>tr>th', 'hide');
                // do hide record action
                $('.a-zoom').hide();
            } else {
                _this.attr('data-zoom', 'show');
                _this.html('-');
                btn_zoom_auto_position('.btn-zoom', '.custom-table>thead>tr>th', 'show');
                $('.a-zoom').show();
            }
        });
    });
</script>
