<?php $this->assign('title', '製品別売上レポート'); ?>
<div class="row">
    <div class="col-md-12 row-middle-space">
        <?php
            echo $this->Form->create('product', [
                'type' => 'get',
                'id' => 'form-search',
                'autocomplete' => 'on',
                'class' => 'form-horizontal'
            ]);
            $this->Form->templates([
                'inputContainer' => '{{content}}'
            ]);
        ?>
        <div class="form-group">
            <div class="col-md-4">
            <?php
                echo $this->Form->input('name', [
                    'id' => 'keyword-search',
                    'label' => false,
                    'class' => 'form-control',
                    'placeholder' => '商品名を入力してください',
                    'value' => $this->request->query('name'),
                    'escape' => false,
                    'required' => false,
                    'autocomplete' => 'off'
                ]);
            ?>
            </div>
        </div>
        <?php echo $this->Form->end(); ?>
    </div>
</div>
<div class="row">
<?php if (!empty($data)) : ?>
    <div class="col-md-12">
        <h3><strong><?php echo trim(h($this->request->query('name'))); ?></strong><span class="header-date">（<?= $min_date . '-' . $max_date  ?>）</span></h3> <!-- echo date('Y/m/01', strtotime ($data['Header']['min_date'])) .' - '. date('Y/m/t', strtotime ($data['Header']['max_date'])); -->
        <div class="table-responsive">
            <table class="table table-noborder">
                <tr>
                    <td width="150"><strong>購入クリニック数</strong></td>
                    <td width="100"><?php echo h(number_format($data['Header']['clinic_name_jp'])); ?></td>
                    <td width="100"><strong>平均単価</strong></td>
                    <td><?php echo number_format($data['Header']['avg_unitprice']).' 円' ?></td>
                </tr>
                <tr>
                    <td width="150"><strong>販売総数</strong></td>
                    <td width="100"><?php echo h(number_format($data['Header']['total_quantity'])); ?></td>
                    <td width="100"><strong>単価範囲</strong></td>
                    <td><?php echo h('最小 '.number_format($data['Header']['min_unitprice']).' 円 - 最大 '.number_format($data['Header']['max_unitprice']).' 円'); ?></td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td width="100"><strong>販売総額</strong></td>
                    <td><?php echo h(number_format($data['Header']['total_amount'])); ?> 円</td>
                </tr>
            </table>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-6 col-md-offset-6 text-right">
                <a href="<?= $this->Url->build(['controller' => 'Reports', 'action' => 'productPdf', '?' => ['name' => $this->request->query('name')]]) ?>" class="btn btn-info text-right" target="_blank">PDF</a>
            </div>
        </div>
        <a href="javascript:void(0)" class="btn btn-default btn-xs btn-zoom" data-zoom="show">-</a>
        <div class="table-responsive table-fixed-row-column" id="table-fixed-row-column">
            <table class="table table-bordered custom-table">
                <thead>
                    <tr>
                        <th><div style="width:155px">&nbsp;</div></th>
                        <th><div  style="width: 60px;">&nbsp;</div></th>
                        <th class="a-zoom">
                            <p class="p-text">期間集計</p>
                            <p class="p-text"><?= $min_date . '-' . $max_date  ?></p>
                            <div class="f-border-row">
                                <div class="border-row-item">合計</div>
                                <div class="border-row-item">平均</div>
                            </div>
                        </th>
                        <th>
                            <p  class="p-text p-day">1月</p>
                            <div class="border-row">
                                <div class="border-row-item">個数</div>
                                <div class="border-row-item">単価</div>
                            </div>
                        </th>
                        <th>
                            <p  class="p-text p-day">2月</p>
                            <div class="border-row">
                                <div class="border-row-item">個数</div>
                                <div class="border-row-item">単価</div>
                            </div>
                        </th>
                        <th>
                            <p  class="p-text p-day">3月</p>
                            <div class="border-row">
                                <div class="border-row-item">個数</div>
                                <div class="border-row-item">単価</div>
                            </div>
                        </th>
                        <th>
                            <p  class="p-text p-day">4月</p>
                            <div class="border-row">
                                <div class="border-row-item">個数</div>
                                <div class="border-row-item">単価</div>
                            </div>
                        </th>
                        <th>
                            <p  class="p-text p-day">5月</p>
                            <div class="border-row">
                                <div class="border-row-item">個数</div>
                                <div class="border-row-item">単価</div>
                            </div>
                        </th>
                        <th>
                            <p  class="p-text p-day">6月</p>
                            <div class="border-row">
                                <div class="border-row-item">個数</div>
                                <div class="border-row-item">単価</div>
                            </div>
                        </th>
                        <th>
                            <p  class="p-text p-day">7月</p>
                            <div class="border-row">
                                <div class="border-row-item">個数</div>
                                <div class="border-row-item">単価</div>
                            </div>
                        </th>
                        <th>
                            <p  class="p-text p-day">8月</p>
                            <div class="border-row">
                                <div class="border-row-item">個数</div>
                                <div class="border-row-item">単価</div>
                            </div>
                        </th>
                        <th>
                            <p  class="p-text p-day">9月</p>
                            <div class="border-row">
                                <div class="border-row-item">個数</div>
                                <div class="border-row-item">単価</div>
                            </div>
                        </th>
                        <th>
                            <p  class="p-text p-day">10月</p>
                            <div class="border-row">
                                <div class="border-row-item">個数</div>
                                <div class="border-row-item">単価</div>
                            </div>
                        </th>
                        <th>
                            <p  class="p-text p-day">11月</p>
                            <div class="border-row">
                                <div class="border-row-item">個数</div>
                                <div class="border-row-item">単価</div>
                            </div>
                        </th>
                        <th>
                            <p  class="p-text p-day">12月</p>
                            <div class="border-row">
                                <div class="border-row-item">個数</div>
                                <div class="border-row-item">単価</div>
                            </div>
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        if (!empty($data['Body'])) :
                        $i = 1;
                        foreach ($data['Body'] as $key => $value) :
                            if ($i % 2 == 0) {
                                $class = ' class="active"';
                            } else {
                                $class = '';
                            }
                            $i++;

                            // get first order date and last order date for calculation
                            $start_date = strtotime(date('Y-m', reset($value['date'])));
                            $end_date = strtotime(date('Y-m', end($value['date'])));

                    ?>
                    <?php //pr($value); ?>
                    <tr<?= $class ?>>
                        <td rowspan="3" class="text-left"><?= h($key) ?></td>
                        <td><?php $year = array_keys($value['years'][1]); echo $year[2]; ?></td>
                        <td class="a-zoom">
                            <div class="row-data">
                                <div class="rows-items left-item">
                                    <div class="row-item-top">
                                        <div class="pull-left">購入数量</div>
                                        <div class="pull-right"><?= number_format(array_sum(array_values($value['year']['last']['qty'])) + array_sum(array_values($value['year']['previous']['qty'])) + array_sum(array_values($value['year']['after_previous']['qty']))) ?></div>
                                    </div>
                                    <div class="row-item-bottom">
                                        <div class="pull-left">金額</div>
                                        <div class="pull-right"><?= number_format(array_sum(array_values($value['year']['last']['amount'])) + array_sum(array_values($value['year']['previous']['amount'])) + array_sum(array_values($value['year']['after_previous']['amount']))) ?> 円</div>
                                    </div>
                                </div>
                                <div class="rows-items right-item">
                                    <div class="row-item-top">
                                        <div class="pull-left">数量</div>
                                        <div class="pull-right"><?= number_format(array_sum(array_values($value['year']['last']['qty']))) ?></div>
                                    </div>
                                    <div class="row-item-bottom">
                                        <div class="pull-left">金額</div>
                                        <div class="pull-right"><?= number_format(array_sum(array_values($value['year']['last']['amount']))) ?> 円</div>
                                    </div>
                                </div>
                            </div>
                        </td>
                        <?php foreach ($value['years'] as $key1 => $value1) : ?>
                        <td>
                            <div class="row-value">
                                <div class="value-item">
                                    <?php
                                        $d = strtotime($year[2].'-'.$key1);
                                        $number_order1 = count($value1[$year[2]]['qty']);
                                        $sum_qty1 = array_sum(array_values($value1[$year[2]]['qty']));
                                        $s_total1 = 0;
                                        if ($number_order1 > 0 ) {
                                            echo number_format($sum_qty1);
                                            for ($in = 0; $in < $number_order1; $in++) {
                                                $sub_total1 = $value1[$year[2]]['qty'][$in] * $value1[$year[2]]['unit_price'][$in];
                                                $s_total1 += $sub_total1;
                                            }
                                        } else {
                                            if ($start_date <= $d && $end_date >= $d) {
                                                echo '-';
                                            } else {
                                                echo '&nbsp;';
                                                //echo $start_date.'<='.$d;
                                            }
                                        }
                                    ?>
                                </div>
                                <div class="value-item">
                                    <?php
                                        if ($number_order1 > 0) {
                                            echo number_format($s_total1/$sum_qty1).' 円';
                                        } else {
                                            if ($start_date <= $d && $end_date >= $d) {
                                                echo '-';
                                            } else {
                                                echo '&nbsp;';
                                            }
                                        }
                                    ?>
                                </div>
                            </div>
                        </td>
                        <?php endforeach; ?>
                    </tr>
                    <tr<?= $class ?>>
                        <td><?= $year[1] ?></td>
                        <td class="a-zoom">
                            <div class="row-data">
                                <div class="rows-items left-item">
                                    <div class="row-item-top">
                                        <div class="pull-left">購入回数</div>
                                        <div class="pull-right"><?= number_format($value['total_order']) ?> 回</div>
                                    </div>
                                    <div class="row-item-bottom">(<?= number_format($value['total_unit_price']/$value['total_order']) ?>円/回)</div>
                                </div>
                                <div class="rows-items right-item">
                                    <div class="row-item-top">
                                        <div class="pull-left">数量</div>
                                        <div class="pull-right"><?= number_format(array_sum(array_values($value['year']['previous']['qty']))) ?></div>
                                    </div>
                                    <div class="row-item-bottom">
                                        <div class="pull-left">金額</div>
                                        <div class="pull-right"><?= number_format(array_sum(array_values($value['year']['previous']['amount']))) ?> 円</div>
                                    </div>
                                </div>
                            </div>
                        </td>
                        <?php foreach ($value['years'] as $key2 => $value2) : ?>
                        <td>
                            <div class="row-value">
                                <div class="value-item">
                                    <?php
                                        $d = strtotime($year[1].'-'.$key2);
                                        $number_order2 = count($value2[$year[1]]['qty']);
                                        $sum_qty2 = array_sum(array_values($value2[$year[1]]['qty']));
                                        $s_total2 = 0;
                                        if ($number_order2 > 0 ) {
                                            echo number_format($sum_qty2);
                                            for ($in = 0; $in < $number_order2; $in++) {
                                                $sub_total2 = $value2[$year[1]]['qty'][$in] * $value2[$year[1]]['unit_price'][$in];
                                                $s_total2 += $sub_total2;
                                            }
                                        } else {
                                            if ($start_date <= $d && $end_date >= $d) {
                                                echo '-';
                                            } else {
                                                echo '&nbsp;';
                                            }
                                        }
                                    ?>
                                </div>
                                <div class="value-item">
                                    <?php
                                        if($number_order2 > 0) {
                                            echo number_format($s_total2/$sum_qty2).' 円';
                                        } else {
                                            if ($start_date <= $d && $end_date >= $d) {
                                                echo '-';
                                            } else {
                                                echo '&nbsp;';
                                            }
                                        }
                                    ?>
                                </div>
                            </div>
                        </td>
                        <?php endforeach;?>
                    </tr>
                    <tr<?= $class ?>>
                        <td><?php echo $year[0]; ?></td>
                        <td class="a-zoom">
                            <div class="row-data">
                                <div class="rows-items left-item">
                                    <div class="row-item-top">最終購入月</div>
                                    <div class="row-item-bottom"><?= date('Y-m-d', end($value['date'])) ?></div>
                                </div>
                                <div class="rows-items right-item">
                                    <div class="row-item-top">
                                        <div class="pull-left">数量</div>
                                        <div class="pull-right"><?= number_format(array_sum(array_values($value['year']['after_previous']['qty']))) ?></div>
                                    </div>
                                    <div class="row-item-bottom">
                                        <div class="pull-left">金額</div>
                                        <div class="pull-right"><?= number_format(array_sum(array_values($value['year']['after_previous']['amount']))) ?> 円</div>
                                    </div>
                                </div>
                            </div>
                        </td>
                        <?php foreach ($value['years'] as $key3 => $value3) : ?>
                        <td>
                            <div class="row-value">
                                <div class="value-item">
                                    <?php
                                        $d = strtotime($year[0].'-'.$key3);
                                        $number_order3 = count($value3[$year[0]]['qty']);
                                        $sum_qty3 = array_sum(array_values($value3[$year[0]]['qty']));
                                        $s_total3 = 0;
                                        if ($number_order3 > 0 ) {
                                            echo number_format($sum_qty3);
                                            for ($in = 0; $in < $number_order3; $in++) {
                                                $sub_total3 = $value3[$year[0]]['qty'][$in] * $value3[$year[0]]['unit_price'][$in];
                                                $s_total3 += $sub_total3;
                                            }
                                        } else {
                                            if ($start_date <= $d && $end_date >= $d) {
                                                echo '-';
                                            } else {
                                                echo '&nbsp;';
                                            }
                                        }
                                    ?>
                                </div>
                                <div class="value-item">
                                    <?php
                                        if ($number_order3 > 0) {
                                            echo number_format($s_total3/$sum_qty3).' 円';
                                        } else {
                                            if ($start_date <= $d && $end_date >= $d) {
                                                echo '-';
                                            } else {
                                                echo '&nbsp;';
                                            }
                                        }
                                    ?>
                                </div>
                            </div>
                        </td>
                        <?php endforeach;?>
                    </tr>
                    <?php if ($i == 5) { $i = 0; } ?>
                    <?php endforeach; endif; ?>
                </tbody>
            </table>
        </div>
    </div>
    <?php
    else :
        if ($this->request->query('name')) {
    ?>
    <div class="col-md-12 text-center">
        <p><?= __('検索された製品が見つかりません。') ?></p>
    </div>
    <?php
        }
    endif;
    ?>
</div>
<script>
    $(function() {
        var dataSource = <?= $name ?>;
        if (typeof dataSource === 'object') {
            dataSource = Object.values(dataSource);
        }
        $('#keyword-search').autocomplete({
            source: dataSource,
            minLength: 1,
            open: function(event, ui) {
                $(this).autocomplete('widget').css({
                    'width': 360
                });
            },
            select: function(event, ui) {
                $('#keyword-search').val(ui.item.value);
                $('#form-search').submit();
            }
        });

        $('.custom-table').tableHeadFixer({'left' : 2, 'z-index': 0});
        // show/hidde button
        btn_zoom_auto_position('.btn-zoom', '.custom-table>thead>tr>th', 'show');
        function btn_zoom_auto_position(select, target, s = null){
            var _this = $(select);
            var th = $(target);
            var mar = th.eq(0).width() + th.eq(1).width();
            var left = th.eq(2).width() / 2;
            if (s == 'show') {
                mar+=left;
            }
            _this.css('margin-left', mar);
        }

        $('body').on('click', '.btn-zoom', function() {
            var _this = $(this);
            if (_this.attr('data-zoom') == 'show') {
                _this.attr('data-zoom', 'hide');
                _this.html('+');
                btn_zoom_auto_position('.btn-zoom', '.custom-table>thead>tr>th', 'hide');
                // do hide record action
                $('.a-zoom').hide();
            } else {
                _this.attr('data-zoom', 'show');
                _this.html('-');
                btn_zoom_auto_position('.btn-zoom', '.custom-table>thead>tr>th', 'show');
                $('.a-zoom').show();
            }
        });
    });
</script>
