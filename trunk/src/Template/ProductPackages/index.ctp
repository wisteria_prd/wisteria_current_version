
<div class="search-section" style="margin-bottom: 20px;">
    <?php
    echo $this->Form->create('ProductPackages', [
        'role' => 'form',
        'class' => 'form-inline form-search',
        'type' => 'get',
        'name' => 'form_search'
    ]);
    echo $this->Form->hidden('displays', ['value' => $display]); ?>
    <div class="form-group">
        <?php
        echo $this->Form->text('keyword', [
            'name' => 'keyword',
            'placeholder' => __('P_PACKAGE_ENTER_KEYWORD'),
            'class' => 'form-control',
            'label' => false,
            'value' => $this->request->query('keyword'),
            'autocomplete' => 'off',
        ]); ?>
    </div>
    <?php
    $this->SwitchStatus->render();
    echo $this->Form->end(); ?>
</div>
<div class="total-pagination" style="padding-bottom: 20px;">
    <?php
    echo $this->element('display_info'); ?>
</div>
<div class="row search-section" style="margin-bottom: 20px;">
    <?php
    echo $this->element('display_number');
    echo  $this->ActionButtons->btnRegisterNew('ProductPackages', []); ?>
    <div class="clearfix"></div>
</div>
<div class="data-tb-list">
    <?php if ($data): ?>
    <table class="table table-striped">
        <thead>
            <tr>
                <th>#</th>
                <th>&nbsp;</th>
                <th class="white-space">
                    <?php
                    echo $this->Paginator->sort('ProductPackages.code', __('P_PACKAGE_OLD_CODE')); ?>
                    <i class="fa fa-sort-desc" aria-hidden="true"></i>
                </th>
                <th class="white-space">
                    <?php
                    echo $this->Paginator->sort('ProductPackages.name', __d('product_package', 'TXT_PRODUCT_PACKAGE')); ?>
                    <i class="fa fa-sort-desc" aria-hidden="true"></i>
                </th>
                <th class="white-space">
                    <?php
                    echo $this->Paginator->sort('ProductPackages.unit_price', __('P_PACKAGE_UNIT_PRICE')); ?>
                    <i class="fa fa-sort-desc" aria-hidden="true"></i>
                </th>
                <th class="white-space">
                    <?php
                    echo __('P_PACKAGE_SALE_AVALABILITY'); ?>
                </th>
                <th class="white-space">
                    <?php
                    echo $this->Paginator->sort('ProductPackages.remark', __('P_PACKAGE_DESCRIPTION')); ?>
                    <i class="fa fa-sort-desc" aria-hidden="true"></i>
                </th>
                <th width="1%">&nbsp;</th>
                <th width="1%">&nbsp;</th>
                <th width="1%">&nbsp;</th>
                <th width="1%">&nbsp;</th>
            </tr>
        </thead>
        <tbody>
            <?php
            $numbering = $this->Paginator->counter('{{start}}');
            foreach ($data as $item):
                $sale_availability = false;
                $collection =  new Cake\Collection\Collection($item->product_package_details);
                $prd_package_details = $collection->filter(function ($value, $key) {
                    $product_detail = $value->product_detail;
                    if ($product_detail->sale_flag && $product_detail->product->sell_flag && $product_detail->product->product_brand->is_sale) {
                        return $value;
                    }
                });
                if (iterator_count($prd_package_details)) {
                    $sale_availability = true;
                }
            ?>
            <tr data-id="<?= h($item->id); ?>">
                <th scope="row">
                    <?php
                    echo $numbering; $numbering++; ?>
                </th>
                <td>
                    <?php
                    echo $this->ActionButtons->disabledText($item->is_suspend) ?>
                </td>
                <td>
                    <?php
                    echo h($item->code); ?>
                </td>
                <td>
                    <?php
                    echo h($item->name); ?>
                </td>
                <td>
                    <?php
                    echo $this->Currency->format($item->currency->code_en, $item->unit_price); ?>
                </td>
                <td>
                    <?php
                    if ($sale_availability) {
                        echo __d('product_package', 'TXT_YES');
                    } else {
                        echo __d('product_package', 'TXT_NO');
                    }
                    ?>
                </td>
                <td>
                    <?php
                    echo h($item->remark); ?>
                </td>
                <td width="1%" data-target="<?php echo ($item->is_suspend) ? 1 : 0; ?>">
                    <?php
                    echo $this->Html->link( __d('product_package', 'TXT_BUNDLE_PRODUCT'), [
                        'controller' => 'ProductPackageDetails',
                        'action' => 'index',
                        '?' => ['pkg_id' => $item->id],
                    ], [
                        'class' => 'btn btn-sm btn-primary',
                    ]); ?>
                </td>
                <td>
                    <?php
                    echo $this->Form->button('<i class="fa fa-comments-o" aria-hidden="true"></i>', [
                        'class' => 'btn btn-primary btn-sm btn_comment',
                        'data-id' => $item->id,
                        'escape' => false,
                    ]); ?>
                </td>
                <td data-target="<?php echo ($item->is_suspend) ? 0 : 1; ?>">
                    <?php
                    if ($item->is_suspend == 1) {
                        echo $this->Form->button(BTN_ICON_UNDO, [
                            'class' => 'btn btn-recover btn-sm btn-restore',
                            'escape' => false
                        ]);
                    } else {
                        echo $this->Html->link(BTN_ICON_EDIT, [
                            'controller' => 'ProductPackages',
                            'action' => 'edit', $item->id
                            ], [
                            'class' => 'btn btn-primary btn-sm',
                            'escape' => false
                        ]);
                    } ?>
                </td>
                <td data-target="<?php echo ($item->is_suspend) ? 0 : 1; ?>">
                    <?php
                    if ($item->is_suspend == 1) {
                        if (!empty($item->product_brands) || !empty($item->count_person)) {
                            echo $this->Form->button(BTN_ICON_DELETE, [
                                'class' => 'btn btn-delete btn-sm',
                                'disabled' => 'disabled',
                                'escape' => false,
                            ]);
                        } else {
                            echo $this->Form->button(BTN_ICON_DELETE, [
                                'class' => 'btn btn-delete btn-sm',
                                'escape' => false,
                            ]);
                        }
                    } else {
                        echo $this->Form->button(BTN_ICON_STOP, [
                            'class' => 'btn btn-suspend btn-sm btn-restore',
                            'escape' => false,
                        ]);
                    } ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <?php endif; ?>
</div>
<div class="row row-top-space">
    <?php
    echo $this->element('display_number'); ?>
    <?php
    echo $this->element('next_prev') ?>
    <div class="clearfix"></div>
</div>

<script>
    $(function(e) {
        $('.btn-restore').click(function (e) {
            e.stopPropagation();
            let params = {
                id: $(this).closest('tr').attr('data-id'),
                status: $(this).closest('td').attr('data-target')
            };
            let options = {
                url: BASE + 'ProductPackages/getStatus',
                type: 'GET',
                data: params,
                beforeSend: function () {
                    $.LoadingOverlay('show');
                }
            };
            ajaxRequest(options, SESSION_TIMEOUT, function (data) {
                $('body').prepend(data);
                let content = $('body').find('.modal-status');
                $(content).modal('show');
                _iframe(content);
            });
        });

        $('.btn-delete').click(function (e) {
            e.stopPropagation();
            let params = {
                id: $(this).closest('tr').attr('data-id')
            };
            let options = {
                url: BASE + 'ProductPackages/getDelete',
                type: 'GET',
                data: params,
                beforeSend: function () {
                    $.LoadingOverlay('show');
                }
            };
            ajaxRequest(options, SESSION_TIMEOUT, function (data) {
                $('body').prepend(data);
                let content = $('body').find('.modal-delete');
                $(content).modal('show');
                _iframe(content);
            });
        });

        $('.table>tbody>tr>td a').click(function (e) {
            e.stopPropagation();
        });

        $('.data-tb-list .table>tbody>tr').click(function (e) {
            let options = {
                type: 'GET',
                url: BASE + 'ProductPackages/view',
                data: { id:  $(this).closest('tr').data('id') },
                beforeSend: function () {
                    $.LoadingOverlay('show');
                }
            };
            ajaxRequest(options, SESSION_TIMEOUT, function (data) {
                $('body').prepend(data);
                $('body').find('.modal-view').modal('show');
            });
        });

        $('.btn_comment').click(function (e) {
            e.stopPropagation();
            let params = {
                id: $(this).closest('tr').attr('data-id'),
                type: PRODUCT_PACKAGE
            };
            let options = {
                url: BASE + 'messages/get-message-list',
                type: 'GET',
                data: params,
                beforeSend: function () {
                    $.LoadingOverlay('show');
                }
            };
            ajaxRequest(options, SESSION_TIMEOUT, function (data) {
                $('body').prepend(data);
                $('body').find('.modal-message').modal('show');
            });
        });

        function _iframe(content)
        {
            /*
            * CHANGE OF STATUS MODAL
            */
            $(content).find('.change-status').click(function (e) {
                let params = {
                    id: $(content).find('[name="id"]').attr('content'),
                    status: $(content).find('[name="status"]').attr('content')
                };
                let options = {
                    url: BASE + 'ProductPackages/changeOfStatus',
                    type: 'POST',
                    data: params,
                    beforeSend: function () {
                        $.LoadingOverlay('show');
                    }
                };
                ajaxRequest(options, SESSION_TIMEOUT, function (data) {
                    if (data.message === MSG_SUCCESS) {
                        location.reload();
                    }
                });
            });

            /**
            * DELETE MODAL
            */
            $(content).find('.delete-mf').click(function (e) {
                let params = {
                    id: $(content).find('[name="id"]').attr('content')
                };
                let options = {
                    url: BASE + 'ProductPackages/delete',
                    type: 'POST',
                    data: params,
                    beforeSend: function () {
                        $.LoadingOverlay('show');
                    }
                };
                ajaxRequest(options, SESSION_TIMEOUT, function (data) {
                    if (data.message === MSG_SUCCESS) {
                        location.reload();
                    }
                });
            });
        }

        pageOutOfRange(<?= $paging ?>, '<?= $this->request->query('page') ?>');
        var maxPage = parseInt($('#maxPage').val());
        updatePagination(maxPage);
    });
</script>
