
<section class="modal-wrapper">
    <div class="modal fade modal-view" data-backdrop="static" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <table class="table table-striped" id="tb-detail">
                    <?php //pr($data); ?>
                        <tbody>
                            <tr>
                                <td>
                                    <?php
                                    echo __d('product_package', 'TXT_OLD_CODE') ?> :
                                </td>
                                <td>
                                    <?php
                                    echo h($data->code); ?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <?php
                                    echo __d('product_package', 'TXT_PRODUCT_PACKAGE') ?> :
                                </td>
                                <td>
                                    <?php
                                    echo h($data->name); ?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <?php
                                    echo __d('product_package', 'TXT_UNIT_PRICE') ?> :
                                </td>
                                <td>
                                    <?php
                                    echo $this->Currency->format($data->currency->code_en, $data->unit_price); ?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <?php
                                    echo __d('product_package', 'TXT_PURCHASE_AVAILABILITY') ?> :
                                </td>
                                <td>
                                    <?php
                                    $purchase_availability = false;
                                    $collection =  new Cake\Collection\Collection($data->product_package_details);
                                    $prd_package_details = $collection->filter(function ($value, $key) {
                                        $product_detail = $value->product_detail;
                                        if ($product_detail->purchase_flag && $product_detail->product->purchase_flag && $product_detail->product->product_brand->is_purchase) {
                                            return $value;
                                        }
                                    });
                                    if (iterator_count($prd_package_details)) {
                                        $purchase_availability = true;
                                    }
                                    if ($purchase_availability) {
                                        echo __d('product_package', 'TXT_YES');
                                    } else {
                                        echo __d('product_package', 'TXT_NO');
                                    }
                                    ?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <?php
                                    echo __d('product_package', 'TXT_SALE_AVAILABILITY') ?> :
                                </td>
                                <td>
                                    <?php
                                    $sale_availability = false;
                                    $collection =  new Cake\Collection\Collection($data->product_package_details);
                                    $prd_package_details = $collection->filter(function ($value, $key) {
                                        $product_detail = $value->product_detail;
                                        if ($product_detail->sale_flag && $product_detail->product->sell_flag && $product_detail->product->product_brand->is_sale) {
                                            return $value;
                                        }
                                    });
                                    if (iterator_count($prd_package_details)) {
                                        $sale_availability = true;
                                    }
                                    if ($sale_availability) {
                                        echo __d('product_package', 'TXT_YES');
                                    } else {
                                        echo __d('product_package', 'TXT_NO');
                                    }
                                    ?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <?php
                                    echo __d('product_package', 'TXT_REMARKS') ?> :
                                </td>
                                <td>
                                    <textarea readonly="readonly" style="background: none;"><?php echo $data->remark; ?></textarea>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <?php
                                    echo __d('product_package', 'TXT_DESCRIPTION') ?> :
                                </td>
                                <td>
                                    <textarea readonly="readonly" style="background: none;"><?php echo $data->description; ?></textarea>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <?php
                                    echo __d('product_package', 'TXT_BUNDLE_PRODUCT') ?> :
                                </td>
                                <td>
                                    <?php
                                    echo $this->Html->link( __d('product_package', 'TXT_BUNDLE_PRODUCT'), [
                                        'controller' => 'ProductPackageDetails',
                                        'action' => 'index',
                                        '?' => ['pkg_id' => $data->id],
                                    ], [
                                        'class' => 'btn btn-sm btn-primary',
                                    ]); ?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <?php
                                    echo __d('product_package', 'TXT_MODIFIED') ?> :
                                </td>
                                <td>
                                    <?php
                                    echo date('Y-m-d', strtotime($data->modified)); ?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <?php
                                    echo __d('product_package', 'TXT_REGISTERED') ?> :
                                </td>
                                <td>
                                    <?php
                                    echo date('Y-m-d', strtotime($data->created)); ?>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="modal-footer" style="text-align: center !important;">
                    <?php
                    echo $this->Form->button(__d('product_package', 'TXT_CANCEL'), [
                        'type' => 'button',
                        'class' => 'btn btn-default btn-sm  btn-close-modal btn-width modal-close',
                        'data-dismiss' => 'modal',
                        'aria-label' => 'Close',
                    ]); ?>
                </div>
            </div>
        </div>
    </div>
</section>
