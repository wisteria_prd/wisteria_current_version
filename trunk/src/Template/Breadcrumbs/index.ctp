<?php
echo $this->Html->link('Create', '/breadcrumbs/create', [
    'class' => 'btn btn-primary',
    'style' => 'margin-bottom: 10px;',
]); ?>

<?php
echo $this->Html->link('Breadcrumbs detail', '/breadcrumbs/update', [
    'class' => 'btn btn-danger',
    'style' => 'margin-bottom: 10px;',
]); ?>
<table class="table table-bordered" id="table-bradcrumb">
    <thead>
        <tr>
            <th>#</th>
            <th>User Group</th>
            <th>Name JP</th>
            <th>Name En</th>
            <th>Controller</th>
            <th>Action</th>
            <th>Url</th>
            <th>Params</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody>
        <?php
        foreach ($data as $key => $value) : ?>
        <tr>
            <td><?php echo $key + 1; ?></td>
            <td><?php echo $value->user_group_id; ?></td>
            <td><?php echo $value->name; ?></td>
            <td><?php echo $value->name_en; ?></td>
            <td><?php echo $value->controller; ?></td>
            <td><?php echo $value->action; ?></td>
            <td><?php echo $value->url; ?></td>
            <td><?php echo $value->param; ?></td>
            <td>
                <?php
                echo $this->Html->link('Edit', '/breadcrumbs/edit/'.$value->id, [
                    'class' => 'btn btn-primary btn-sm',
                ]); ?>
                <?php
                echo $this->Html->link('Delete', '/breadcrumbs/delete/'.$value->id, [
                    'class' => 'btn btn-danger btn-sm',
                ]); ?>
            </td>
        </tr>
        <?php
        endforeach; ?>
    </tbody>
</table>
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap.min.css" type="text/css" />
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap.min.js"></script>
<script>
    $(document).ready(function() {
        $('#table-bradcrumb').DataTable();
    } );
</script>
