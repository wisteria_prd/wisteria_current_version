
<?php
echo $this->Form->create(null, [
    'class' => 'form-horizontal',
    'role' => 'form',
    'id' => 'breadcrumb-form',
    'type' => 'get',
]) ?>
<div class="row" style="padding-top: 2rem;">
    <div class="col-md-6 col-sm-6">
        <div class="panel panel-default">
            <div class="panel-heading">List of controllers</div>
            <div class="panel-body">
                <?php if ($data): ?>
                    <ul class="list-unstyled">
                        <?php foreach ($data as $key => $value): ?>
                        <li data-id="<?= $value->id ?>" data-name="<?= $value->name ?>" data-name-en="<?= $value->name_en ?>" data-controller="<?= $value->controller ?>" data-action="<?= $value->action ?>" data-name-en="<?= $value->name_en ?>" data-param="<?= $value->param ?>">
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" class="menu-item" name="menu-item">
                                    <?php
                                    echo $value->name_en . ' | ' . $value->name . ' -> (' .  $value->controller . ' | ' . $value->action . ') ';
                                    if (empty($value->controller) || empty($value->action)) {
                                        echo '<span class="error-message">(#)</span>';
                                    }
                                    if (!empty($value->param)) {
                                        echo '( ' . $value->param . ' )';
                                    }
                                    ?>
                                </label>
                            </div>
                        </li>
                        <?php endforeach; ?>
                    </ul>
                <?php endif; ?>
            </div>
            <div class="panel-footer">
                <form class="form-inline">
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" class="menu-check-all" name="check-all"> Check All
                        </label>
                    </div>&nbsp;
                    <?php echo $this->Form->button('Choose Menu', [
                        'type' => 'button',
                        'class' => 'btn btn-primary btn-sm choose-menu',
                        'disabled' => true,
                    ]); ?>
                </form>
            </div>
        </div>
    </div>
    <div class="col-md-6 col-sm-6" style="position: sticky; top: 60px;">
        <div class="panel panel-default">
            <div class="panel-heading">In Order Breadcrumbs</div>
            <div class="panel-body">
                <meta name="user-qroup-id" content="<?php echo $user_group_id; ?>"/>
                <div class="form-group custom-select">
                    <label class="col-sm-2 control-label">Controller</label>
                    <div class="col-sm-10">
                        <?php
                        $ctrs = [];
                        if ($data) {
                            foreach ($data as $key => $value) {
                                if (empty($value->controller) || empty($value->action)) {
                                    continue;
                                }
                                $ctrs[] = [
                                    'text' => $value->controller,
                                    'value' => $value->controller,
                                    'data-id' => $value->id,
                                ];
                            }
                        }
                        echo $this->Form->select('controller', $ctrs, [
                            'class' => 'form-control',
                            'empty' => ['' => 'Select controller'],
                            'data-id' => 1,
                        ]); ?>
                    </div>
                </div>
                <div class="form-group custom-select">
                    <label class="col-sm-2 control-label">Action</label>
                    <div class="col-sm-10">
                        <?php echo $this->Form->select('action', [
                            'index' => 'index',
                        ], [
                            'class' => 'form-control',
                            'empty' => ['' => 'Select action'],
                        ]); ?>
                    </div>
                </div>
                <div class="dd">
                    <ol class="dd-list menu-wrap">
                        <?php
                        if ($data) {
                            //echo $this->Breadcrumbs->getList($data);
                        }
                        ?>
                    </ol>
                </div>
            </div>
            <div class="panel-footer">
                <?php echo $this->Form->button('Update', [
                    'type' => 'button',
                    'class' => 'btn btn-primary btn-sm ms-save',
                    'disabled' => $data ? false : true,
                ]); ?>

                <?php
                echo $this->Html->link('Back', '/breadcrumbs', [
                    'class' => 'btn btn-danger',
                ]); ?>
            </div>
        </div>
    </div>
</div>

<?php
echo $this->Html->css([
    'jquery.nestable.min',
]);
echo $this->Html->script([
    'jquery.nestable.min'
], ['blog' => 'script']); ?>

<script>
    $(function(e) {
        $('.dd').nestable();

        $('.menu-check-all').click(function(e) {
            if ($(this).prop('checked') == true) {
                $('.menu-item').prop('checked', true);
                $('.choose-menu').attr('disabled', false);
            } else {
                $('.menu-item').prop('checked', false);
                $('.choose-menu').attr('disabled', true);
            }
        });

        $('.menu-item').change(function(e) {
            var inx = $('.menu-item:checked').length;
            if (inx > 0) {
                $('.choose-menu').attr('disabled', false);
                return true;
            }
            $('.choose-menu').attr('disabled', true);
        });

        $('.choose-menu').click(function(e) {
            $('.ms-save').attr('disabled', false);
            var chkbox = $('.menu-item:checked');
            if (chkbox.length > 0) {
                $.each(chkbox, function(index, value) {
                    var obj = {
                        name: $(this).closest('li').attr('data-name'),
                        name_en: $(this).closest('li').attr('data-name-en'),
                        controller: $(this).closest('li').attr('data-controller'),
                        action: $(this).closest('li').attr('data-action'),
                        param: $(this).closest('li').attr('data-param'),
                        url: $(this).closest('li').attr('data-url'),
                    };
                    var element = renderList(obj);
                    $('.menu-wrap').append(element);
                });
            }
        });

        $('body').on('click', '.mn-remove', function(e) {
            $(this).closest('li').remove();
            if ($('.menu-wrap li').length == 0) {
                $('.ms-save').attr('disabled', true);
            }
        });

        $('.ms-save').click(function(e) {
            var obj = $('.dd').nestable('serialize');
            var params = {
                type: 'POST',
                url: BASE + 'BreadcrumbDetails/update',
                data: {
                    data: obj,
                    controller: $('body').find('select[name="controller"]').val(),
                    action: $('body').find('select[name="action"]').val(),
                    breadcrumb_id: $('body').find('select[name="controller"]').attr('data-id')
                },
                beforeSend: function() {
                    $.LoadingOverlay('show');
                }
            };
            ajaxRequest(params, SESSION_TIMEOUT, function(data) {
                if (data.message === MSG_SUCCESS) {
                    location.reload();
                }
            });
        });

        $('select[name="controller"]').change(function(e) {
            var params = {
                type: 'GET',
                url: BASE + 'Breadcrumbs/getAllActionByController',
                data: { controller: $(this).val() },
                beforeSend: function() {
                    $.LoadingOverlay('show');
                }
            };
            ajaxRequest(params, SESSION_TIMEOUT, function(data) {
                if (data.data === null && data.data === 'undefined') {
                    return false;
                }
                var options = '<option value>Select action</option>';
                $.each(data.data, function(index, value){
                    options += '<option value="' + value.id + '">' + value.action +  checkParam(value.param) + '</option>';
                });
                $('body').find('select[name="action"]').html(options);
            });
        });

        $('select[name="action"]').change(function(e) {
            var params = {
                type: 'GET',
                url: BASE + 'BreadcrumbDetails/getAllByBreadcrumbId',
                data: { breadcrumb_id: $(this).val() },
                beforeSend: function() {
                    $.LoadingOverlay('show');
                }
            };
            ajaxRequest(params, SESSION_TIMEOUT, function(data) {
                if (data.data === null && data.data === 'undefined') {
                    return false;
                }
                var breadcrumbs = data.data;
                var element = '';
                $.each(breadcrumbs, function(index, data) {
                    if (data.parent_id === null) {
                        element += '<li class="dd-item" data-id="' + data.id + '" data-name="' + data.name + '" data-name-en="' + data.name_en + '"  data-controller="' + data.controller + '"  data-action="' + data.action + '" data-param="' + data.param + '" data-url="' + data.url + '">' +
                                '<div class="dd-handle">' + data.name + ' | ' + data.name_en + '</div>' +
                                '<div class="mn-remove mn-icon">' +
                                    '<i class="fa fa-trash-o" aria-hidden="true"></i>' +
                                '</div>' + checkChild(data.id, breadcrumbs) +
                                '</li>';
                    }
                });
                $('.menu-wrap').html(element);
            });
        });

        function checkChild(id, data)
        {
            var children = $.grep(data, function(item, index) {
                return item.parent_id == id;
            });
            return getChild(children);
        }

        function getChild(data)
        {
            var element = '';
            if (data.length > 0) {
                element += '<ol class="dd-list">';
                $.each(data, function(index, value) {
                    element += renderList(value);
                });
                element += '</ol>';
            }
            return element;
        }

        function renderList(data = null)
        {
            var element = '<li class="dd-item" data-name="' + data.name + '" data-name-en="' + data.name_en + '"  data-controller="' + data.controller + '"  data-action="' + data.action + '" data-param="' + data.param + '" data-url="' + data.url + '">' +
                    '<div class="dd-handle">' + data.name + ' | ' + data.name_en + '</div>' +
                    '<div class="mn-remove mn-icon">' +
                        '<i class="fa fa-trash-o" aria-hidden="true"></i>' +
                    '</div></li>';
            return element;
        }

        function checkParam(data = null)
        {
            if ((data !== 'undefined') && (data !== '')) {
                return ' (' + data + ') ';
            }
        }
    });
</script>
