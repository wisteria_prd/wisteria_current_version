
<section class="modal-wrapper">
    <div class="modal fade modal-view" data-backdrop="static" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <table class="table table-striped" id="tb-detail">
                        <tbody>
                            <tr>
                                <td>
                                    <?php
                                    echo __d('person_in_charge', 'TXT_ASSOCIATE_TYPE'); ?> :
                                </td>
                                <td colspan="2">
                                    <?php
                                    echo $data->type; ?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <?php
                                    echo __d('person_in_charge', 'TXT_ASSOCIATE'); ?> :
                                </td>
                                <td colspan="2">
                                    <?php
                                    switch ($data->type) {
                                        case TYPE_MANUFACTURER:
                                            echo $this->Comment->getFieldByLocal($data->manufacturer, $locale);
                                            break;

                                        case TYPE_SUPPLIER:
                                            echo $this->Comment->getFieldByLocal($data->supplier, $locale);
                                            break;

                                        case TYPE_CUSTOMER:
                                            echo $this->Comment->getFieldByLocal($data->customer, $locale);
                                            break;

                                        default:
                                            echo $this->Comment->getFieldByLocal($data->subsidiary, $locale);
                                    }
                                    ?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <?php
                                    echo __d('person_in_charge', 'TXT_PERSON_IN_CHARGE_NAME'); ?> :
                                </td>
                                <td colspan="2">
                                    <?php
                                    echo $data->first_name . ' ' . $data->last_name; ?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <?php
                                    echo __d('person_in_charge', 'TXT_GENDER'); ?> :
                                </td>
                                <td colspan="2">
                                    <?php
                                    echo ($data->gender == 1) ? 'Male' : 'Female'; ?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <?php
                                    echo __d('person_in_charge', 'TXT_DEPARTMENT'); ?> :
                                </td>
                                <td colspan="2">
                                    <?php
                                    echo $data->department; ?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <?php
                                    echo __d('person_in_charge', 'TXT_DESIGNATION'); ?> :
                                </td>
                                <td colspan="2">
                                    <?php
                                    echo $data->position; ?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <?php
                                    echo __d('person_in_charge', 'TXT_TEL'); ?> :
                                </td>
                                <td>
                                    <?php
                                    if ($data->info_details) {
                                        $phone = '<ul class="email-wrap">';
                                        foreach ($data->info_details as $item) {
                                            $phone .= '<li>' . $item->tel . '</li>';
                                        }
                                        echo $phone . '</ul>';
                                    }
                                    ?>
                                </td>
                                <td>
                                    <?php
                                    if ($data->info_details) {
                                        $phone = '<ul class="email-wrap">';
                                        foreach ($data->info_details as $item) {
                                            $phone .= '<li>' . $item->tel_extension . '</li>';
                                        }
                                        echo $phone . '</ul>';
                                    }
                                    ?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <?php
                                    echo __d('person_in_charge', 'TXT_EMAIL'); ?> :
                                </td>
                                <td colspan="2">
                                    <?php
                                    if ($data->info_details) {
                                        $element = '<ul class="email-wrap">';
                                        foreach ($data->info_details as $item) {
                                            $element .= '<li><a href="mailto:' . $item->email . '" target="_blank">' . $item->email . '</a></li>';
                                        }
                                        echo $element . '</ul>';
                                    }
                                    ?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <?php
                                    echo __d('person_in_charge', 'TXT_REMARKS'); ?> :
                                </td>
                                <td colspan="2">
                                    <textarea readonly="readonly"><?php echo $data->remarks; ?></textarea>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <?php
                                    echo __d('person_in_charge', 'TXT_MODIFIED'); ?> :
                                </td>
                                <td colspan="2">
                                    <?php
                                    echo date('Y-m-d', strtotime($data->modified)); ?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <?php
                                    echo __d('person_in_charge', 'TXT_REGISTERED'); ?> :
                                </td>
                                <td colspan="2">
                                    <?php
                                    echo date('Y-m-d', strtotime($data->created)); ?>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="modal-footer" style="text-align: center !important;">
                    <?php
                    echo $this->Form->button(__('PERSON_IN_CHARGES_TXT_CANCEL'), [
                        'type' => 'button',
                        'class' => 'btn btn-default btn-sm  btn-close-modal btn-width modal-close',
                        'data-dismiss' => 'modal',
                        'aria-label' => 'Close',
                    ]);
                    ?>
                </div>
            </div>
        </div>
    </div>
</section>
