<?php
$params = (object) $this->request->query;
// Manufacturer, Customer, Supplier
if (isset($data->first()->{$params->filter_by}) && !empty($data->first()->{$params->filter_by})) {
    $data_parent = $data->first()->{$params->filter_by};
} else {
    $data_parent = new \stdClass();
}
?>
<div class="search-section" style="margin-bottom: 20px;">
    <?php
    echo $this->Form->create('PersonInCharges', [
        'class' => 'form-inline',
        'type' => 'get',
        'name' => 'form_search',
        'autocomplete' => 'off',
    ]);
    echo $this->Form->hidden('displays', [
        'value' => $display,
    ]);
    $keywordVal = '';
    if (isset($params->keyword)) {
        $keywordVal = $params->keyword;
    } elseif ($data_parent) {
        /* if (isset($data_parent->{'name'.$locale})) {
            $keywordVal = $data_parent->{'name'.$locale};
        }*/
    }
    echo $this->Form->input('keyword', [
        'name' => 'keyword',
        'label' => false,
        'placeholder' => __('USER_SEARCH_KEYWORD'),
        'class' => 'form-control',
        'autocomplete' => 'off',
        'value' => $keywordVal,
        'templates' => [
            'inputContainer' => '<div class="form-group">{{content}}</div>',
        ]
    ]);

    $disable = false;
    if (isset($params->filter_by) && !empty($params->filter_by)) {
        $disable = true;
        echo $this->Form->hidden('filter_by', [
            'value' => $params->filter_by,
        ]);
    }
    echo $this->Form->input('filter_by', [
        'type' => 'select',
        'options' => array_map('__', unserialize(ENTITY_TYPE)),
        'empty' => ['' => __('TXT_ASSOCIATION_TYPE')],
        'name' => 'filter_by',
        'class' => 'form-control',
        'label' => false,
        'value' => isset($params->filter_by) ? $params->filter_by : '',
        'disabled' => $disable,
        'templates' => [
            'inputContainer' => '&nbsp;&nbsp;<div class="form-group custom-select">{{content}}</div>',
        ],
    ]);
    $this->SwitchStatus->render();
    echo $this->Form->end();
    ?>
</div>
<div class="total-pagination" style="padding-bottom: 20px;">
    <?php echo $this->element('display_info'); ?>
</div>
<div class="row search-section" style="margin-bottom: 20px;">
    <?php echo $this->element('display_number'); ?>
    <div class="col-sm-6 col-md-6 text-center"></div>
    <div class="col-sm-3 pull-right" style="text-align: right;">
        <?php
        if (isset($params->filter_by)) {
            echo $this->Html->link(__('TXT_REGISTER_NEW'), [
                'controller' => 'PersonInCharges',
                'action' => 'add',
                '?' => ['filter_by' => $params->filter_by],
            ], [
                'class' => 'btn btn-sm btn-primary btn-width',
            ]);
        } else {
            echo $this->Html->link(__('TXT_REGISTER_NEW'), '/PersonInCharges/add', [
                'class' => 'btn btn-sm btn-primary btn-width',
            ]);
        }
        ?>
    </div>
    <div class="clearfix"></div>
</div>
<div class="data-tb-list">
    <?php if ($data): ?>
    <table class="table table-striped">
        <thead>
            <tr>
                <th>#</th>
                <th>&nbsp;</th>
                <th>
                    <?php
                    echo $this->Paginator->sort('type', __d('person_in_charge', 'TXT_ASSOCIATE')); ?>
                    <i class="fa fa-sort-desc" aria-hidden="true"></i>
                </th>
                <th>
                    <?php
                    $field = 'last_name' . $locale;
                    echo $this->Paginator->sort($field, __d('person_in_charge', 'TXT_PERSON_IN_CHARGE_NAME'));
                    ?>
                    <i class="fa fa-sort-desc" aria-hidden="true"></i>
                </th>
                <th>
                    <?php
                    echo $this->Paginator->sort('gender', __('STR_GENDER')); ?>
                    <i class="fa fa-sort-desc" aria-hidden="true"></i>
                </th>
                <th>
                    <?php
                    echo $this->Paginator->sort('department', __('STR_USER_DEPARTMENT')); ?>
                    <i class="fa fa-sort-desc" aria-hidden="true"></i>
                </th>
                <th width="1%">&nbsp;</th>
                <th width="1%">&nbsp;</th>
                <th width="1%">&nbsp;</th>
            </tr>
        </thead>
        <tbody>
            <?php
            $lname = 'last_name' . $locale;
            $fname = 'first_name' . $locale;
            $numbering = $this->Paginator->counter('{{start}}');
            foreach ($data as $item): //pr($item); ?>
            <tr data-id="<?php echo h($item->id); ?>">
                <th scope="row" class="td_evt">
                    <?php
                    echo $numbering; $numbering++; ?>
                </th>
                <td>
                    <?php
                    echo $this->ActionButtons->disabledText($item->is_suspend) ?>
                </td>
                <td>
                    <?php
                    switch ($item->type) {
                        case TYPE_MANUFACTURER:
                            echo $this->Comment->getFieldByLocal($item->manufacturer, $locale);
                            break;

                        case TYPE_SUPPLIER:
                            echo $this->Comment->getFieldByLocal($item->supplier, $locale);
                            break;

                        case TYPE_CUSTOMER:
                            echo $this->Comment->getFieldByLocal($item->customer, $locale);
                            break;

                        default:
                            echo $this->Comment->getFieldByLocal($item->subsidiary, $locale);
                            break;
                    }
                    ?>
                </td>
                <td>
                    <?php
                    if ($locale === LOCAL_EN) {
                        echo h($item->first_name_en) . ' ' . h($item->last_name_en);
                    } else {
                        echo h($item->first_name) . ' ' . h($item->last_name);
                    }
                    ?>
                </td>
                <td>
                    <?php
                    echo ($item->gender == '0') ? __('STR_FEMALE') : __('STR_MALE'); ?>
                </td>
                <td>
                    <?php
                    echo h($item->department); ?>
                </td>
                <td class="td-btn-wrap">
                    <button type="button" class="btn btn-primary btn-sm btn_comment" data-id="<?= $item->id ?>"><i class="fa fa-comments-o" aria-hidden="true"></i></button>
                </td>
                <td data-target="<?php echo ($item->is_suspend) ? 0 : 1; ?>">
                    <?php
                    if ($item->is_suspend == 1) {
                        echo $this->Form->button(BTN_ICON_UNDO, [
                            'class' => 'btn btn-recover btn-sm btn-restore',
                            'escape' => false
                        ]);
                    } else {
                        $edit_url = [
                            'controller' => 'personInCharges',
                            'action' => 'edit', $item->id,
                            '?' => ['filter_by' => $item->type]
                        ];
                        if ($this->request->query('external_id') && $this->request->query('des')) {
                            $edit_url = [
                                'controller' => 'personInCharges',
                                'action' => 'edit', $item->id,
                                '?' => [
                                    'des' => $this->request->query('des'),
                                    'filter_by' => $this->request->query('filter_by'),
                                    'external_id' => $this->request->query('external_id') ? $this->request->query('external_id') : '',
                                ]
                            ];
                        }
                        echo $this->Html->link(BTN_ICON_EDIT, $edit_url, [
                            'class' => 'btn btn-primary btn-sm',
                            'escape' => false
                        ]);
                    }
                    ?>
                </td>
                <td data-target="<?php echo ($item->is_suspend) ? 0 : 1; ?>">
                    <?php
                    if ($item->is_suspend == 1) {
                        echo $this->Form->button(BTN_ICON_DELETE, [
                            'class' => 'btn btn-delete btn-sm btn-rm-record',
                            'escape' => false
                        ]);
                    } else {
                        echo $this->Form->button(BTN_ICON_STOP, [
                            'class' => 'btn btn-suspend btn-sm btn-restore',
                            'escape' => false
                        ]);
                    }
                    ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <?php endif; ?>
</div>
<div class="row row-top-space" style="padding-bottom: 20px;">
    <?php echo $this->element('display_number'); ?>
    <?php echo $this->element('next_prev') ?>
    <div class="clearfix"></div>
</div>

<?php
echo $this->Html->script([
    'person-in-charges/person',
], ['blog' => false]);
