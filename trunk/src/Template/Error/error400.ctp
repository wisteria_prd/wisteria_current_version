<?php
use Cake\Core\Configure;
use Cake\Error\Debugger;
$this->layout = 'error';
?>
<style>
    .jumbotron {
        position: absolute;
        transform: translateX(-50%) translateY(50%);
        top: 50%;
        left: 50%;
    }
</style>
<div class="jumbotron container text-center">
    <h1>Page Not Found <small class="text-danger">Error 404</small></h1>
    <p>
        The page you requested could not be found, either contact your webmaster or try again
    </p>
    <?= $this->Html->link('<i class="fa fa-undo" aria-hidden="true"></i> Go Back', 'javascript:history.back()', [
        'class' => 'btn btn-large btn-info',
        'escape' => false,
    ]) ?>
</div>