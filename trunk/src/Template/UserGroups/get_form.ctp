
<section class="modal-wrapper">
    <div class="modal fade" id="create-form" data-backdrop="static" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <?php
                    echo $this->Form->button('<span aria-hidden="true">&times;</span>', [
                        'type' => 'button',
                        'class' => 'close modal-close',
                        'data-dismiss' => 'modal',
                        'aria-label' => 'Close',
                        'escape' => false,
                    ]);
                    ?>
                    <h4 class="modal-title text-center"><?= __('TXT_CREATE') ?></h4>
                </div>
                <div class="modal-body">
                    <?php
                    echo $this->Form->create(null, [
                        'class' => 'form-add-po form-horizontal',
                        'autocomplete' => 'off',
                        'name' => 'form_user_group',
                        'onsubmit' => 'return false;',
                    ]);
                    echo $this->Form->hidden('target', [
                        'value' => $target,
                    ]);
                    if ($userGroup) {
                        echo $this->Form->hidden('id', [
                            'value' => $userGroup->id,
                        ]);
                    }
                    ?>
                    <div class="form-group">
                        <label for="" class="control-label col-md-3"><?= __('STR_USERNAME_EN') ?></label>
                        <div class="col-md-7">
                            <?= $this->Form->text('name_en', [
                                'class' => 'form-control',
                                'placeholder' => __('STR_USERNAME_EN'),
                                'default' => $userGroup ? $userGroup->name_en : null,
                            ]) ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="control-label col-md-3"><?= __('STR_USERNAME_JP') ?></label>
                        <div class="col-md-7">
                            <?= $this->Form->text('name', [
                                'class' => 'form-control',
                                'placeholder' => __('STR_USERNAME_JP'),
                                'default' => $userGroup ? $userGroup->name : null,
                            ]) ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="control-label col-md-3"><?= __('TXT_AFFILIATION_CLASS') ?></label>
                        <div class="col-md-7">
                            <?= $this->Form->text('affiliation_class', [
                                'class' => 'form-control',
                                'placeholder' => __('TXT_AFFILIATION_CLASS'),
                                'default' => $userGroup ? $userGroup->affiliation_class : null,
                            ]) ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="control-label col-md-3"><?= __('STR_USER_ROLE') ?></label>
                        <div class="col-md-7">
                            <?= $this->Form->text('role', [
                                'class' => 'form-control',
                                'placeholder' => __('STR_USER_ROLE'),
                                'default' => $userGroup ? $userGroup->role : null,
                            ]) ?>
                        </div>
                    </div>
                    <?php echo $this->Form->end(); ?>
                </div>
                <div class="modal-footer">
                    <?php
                    echo $this->Form->button(__('TXT_CANCEL'), [
                        'type' => 'button',
                        'class' => 'btn btn-default btn-sm btn-width modal-close',
                        'data-dismiss' => 'modal',
                        'aria-label' => 'Close',
                    ]);
                    echo $this->Form->button(__('TXT_REGISTER'), [
                        'type' => 'button',
                        'class' => 'btn btn-primary btn-sm btn-width btn-save',
                    ]);
                    ?>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(function(e) {
            $('.btn-save').click(function(e) {
                var form  = $('form[name="form_user_group"]');
                var options = {
                    type: 'POST',
                    url: '<?= $this->Url->build('/user-groups/create-and-update/') ?>',
                    data: $(form).serialize(),
                    beforeSend: function() {
                        $.LoadingOverlay('show');
                        $(form).find('.error-message').remove();
                        $(form).find('.form-group div').removeClass('has-error');
                    }
                };
                ajaxRequest(options, function(data) {
                    if (data.message === '<?= MSG_ERROR ?>') {
                        $.each(data.data, function(i, v) {
                            var message = '<label class="error-message">' + v + '</label>';
                            var content = $(form).find('[name="' + i + '"]');
                            $(content).closest('div')
                                    .append(message)
                                    .addClass('has-error');
                        });
                    } else {
                        location.reload();
                    }
                });
            });

            function ajaxRequest(params, callback)
            {
                $.ajax(params).done(function(data) {
                    if (data === null && data === 'undefined') {
                        return false;
                    }
                    if (typeof callback === 'function') {
                        callback(data);
                    }
                }).fail(function(jqXHR, textStatus, errorThrown) {
                    if (errorThrown === 'Forbidden') {
                        if (confirm('<?= __('TXT_SESSION_TIMEOUT') ?>')) {
                            location.reload();
                        }
                    }
                }).always(function(data) {
                    $.LoadingOverlay('hide');
                });
            }
        });
    </script>
</section>
