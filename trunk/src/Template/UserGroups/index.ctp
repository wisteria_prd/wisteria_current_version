
<div class="total-pagination" style="padding-bottom: 20px;">
    <?php echo $this->element('display_info'); ?>
</div>
<div class="row search-section" style="margin-bottom: 20px;">
    <?php echo $this->element('display_number'); ?>
    <?php if ($local !== LOCAL_EN) : ?>
    <div class="col-sm-6 pull-right" style="text-align: right;">
        <?php
        echo $this->Form->button(__('BTN_REGISTER'), [
            'type' => 'button',
            'class' => 'btn btn-sm btn-primary btn-width btn-register',
            'data-target' => TARGET_NEW,
        ]);
        ?>
    </div>
    <?php endif; ?>
    <div class="clearfix"></div>
</div>
<div class="data-tb-list">
    <?php if ($userGroups): ?>
    <table class="table table-striped table-pos">
        <thead>
            <tr>
                <th>#</th>
                <th class="white-space" style="white-space: nowrap;">
                    <?php echo $this->Paginator->sort('name_en', __('STR_USERNAME_EN')); ?>
                    <i class="fa fa-sort-desc" aria-hidden="true"></i>
                </th>
                <th class="white-space" style="white-space: nowrap;">
                    <?php echo $this->Paginator->sort('name', __('STR_FNAME_JP')); ?>
                    <i class="fa fa-sort-desc" aria-hidden="true"></i>
                </th>
                <th class="white-space" style="white-space: nowrap;">
                    <?php echo $this->Paginator->sort('affiliation_class', __('TXT_AFFILIATION_CLASS')); ?>
                    <i class="fa fa-sort-desc" aria-hidden="true"></i>
                </th>
                <th class="white-space" style="white-space: nowrap;">
                    <?php echo $this->Paginator->sort('role', __('STR_USER_ROLE')); ?>
                    <i class="fa fa-sort-desc" aria-hidden="true"></i>
                </th>
                <th></th>
                <th></th>
                <th></th>
            </tr>
        </thead>
        <tbody>
             <?php
            $numbering = $this->Paginator->counter('{{start}}');
            foreach ($userGroups as $key => $value) :
            ?>
            <tr data-id="<?= $value->id ?>">
                <th><?php echo $numbering; $numbering++; ?></th>
                <td><?= $value->name_en ?></td>
                <td><?= $value->name ?></td>
                <td><?= $this->Comment->textHumanize($value->affiliation_class) ?></td>
                <td><?= $this->Comment->textHumanize($value->role) ?></td>
                <td width="1%">
                    <?= $this->Html->link('<i class="fa fa-cogs" aria-hidden="true"></i>', [
                        'controller' => 'user-permissions',
                        'action' => 'setting',
                        '?' => ['user_group_id' => $value->id],
                    ], [
                        'class' => 'btn btn-recover btn-sm',
                        'escape' => false
                    ]) ?>
                </td>
                <td width="1%">
                    <?= $this->Form->button(BTN_ICON_EDIT, [
                        'class' => 'btn btn-primary btn-sm btn-register',
                        'data-target' => TARGET_EDIT,
                        'escape' => false
                    ]) ?>
                </td>
                <td width="1%">
                    <?= $this->Form->button(BTN_ICON_DELETE, [
                        'class' => 'btn btn-delete btn-sm',
                        'escape' => false
                    ]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <?php endif; ?>
</div>
<div class="row row-top-space" style="padding-bottom: 20px;">
    <?php echo $this->element('display_number'); ?>
    <?= $this->element('next_prev') ?>
    <div class="clearfix"></div>
</div>

<script>
    $(function(e) {
        $('body').on('click', '.btn-register', function(e) {
            var params = {
                target: $(this).attr('data-target'),
                id: null
            };
            if (params.target === '<?= TARGET_EDIT ?>') {
                params.id = $(this).closest('tr').attr('data-id');
            }
            var options = {
                type: 'GET',
                url: '<?= $this->Url->build('/user-groups/get-form') ?>',
                data: params,
                beforeSend: function() {
                    $.LoadingOverlay('show');
                }
            };
            ajaxRequest(options, function(data) {
                $('body').prepend(data);
                $('body').find('#create-form, #access-denied').modal('show');
            });
        });

        $('body').on('click', '.btn-delete', function(e) {
            var options = {
                type: 'GET',
                url: '<?= $this->Url->build('/user-groups/get-delete-form') ?>',
                data: { id: $(this).closest('tr').attr('data-id') },
                beforeSend: function() {
                    $.LoadingOverlay('show');
                }
            };
            ajaxRequest(options, function(data) {
                $('body').prepend(data);
                $('body').find('.modal-delete,#access-denied').modal('show');
            });
        });

        function ajaxRequest(params, callback)
        {
            $.ajax(params).done(function(data) {
                if (data === null && data === 'undefined') {
                    return false;
                }
                if (typeof callback === 'function') {
                    callback(data);
                }
            }).fail(function(jqXHR, textStatus, errorThrown) {
                if (errorThrown === 'Forbidden') {
                    if (confirm('<?= __('TXT_SESSION_TIMEOUT') ?>')) {
                        location.reload();
                    }
                }
            }).always(function(data) {
                $.LoadingOverlay('hide');
            });
        }
    });
</script>