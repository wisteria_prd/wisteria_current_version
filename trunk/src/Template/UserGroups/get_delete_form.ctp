
<section class="modal-wrapper">
    <div class="modal fade modal-delete" data-backdrop="static" role="dialog">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <?php
                    echo $this->Form->button('<span aria-hidden="true">&times;</span>', [
                        'type' => 'button',
                        'class' => 'close modal-close',
                        'data-dismiss' => 'modal',
                        'aria-label' => 'Close',
                        'escape' => false,
                    ]);
                    ?>
                    <h4 class="modal-title text-center"><?= __('BTN_DELETE') ?></h4>
                </div>
                <div class="modal-body">
                    <p class="text-center"><?= __('MSG_CONFIRM_DELETE') ?></p>
                </div>
                <div class="modal-footer">
                    <?php
                    echo $this->Form->button(__('TXT_CANCEL'), [
                        'type' => 'button',
                        'class' => 'btn btn-default btn-sm btn-width modal-close',
                        'data-dismiss' => 'modal',
                        'aria-label' => 'Close',
                    ]);
                    echo $this->Form->button(__('TXT_REGISTER'), [
                        'type' => 'button',
                        'class' => 'btn btn-primary btn-sm btn-width btn-remove',
                        'data-id' => $id,
                    ]);
                    ?>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(function(e) {
            $('.btn-remove').click(function(e) {
                var options = {
                    type: 'POST',
                    url: '<?= $this->Url->build('/user-groups/delete/') ?>',
                    dataType: 'json',
                    data: { id: $(this).attr('data-id') },
                    beforeSend: function() {
                        $.LoadingOverlay('show');
                    }
                };
                ajaxRequest(options, function(data) {
                    if (data.message === '<?= MSG_SUCCESS ?>') {
                        location.reload();
                    }
                });
            });

            function ajaxRequest(params, callback)
            {
                $.ajax(params).done(function(data) {
                    if (data === null && data === 'undefined') {
                        return false;
                    }
                    if (typeof callback === 'function') {
                        callback(data);
                    }
                }).fail(function(jqXHR, textStatus, errorThrown) {
                    if (errorThrown === 'Forbidden') {
                        if (confirm('<?= __('TXT_SESSION_TIMEOUT') ?>')) {
                            location.reload();
                        }
                    }
                }).always(function(data) {
                    $.LoadingOverlay('hide');
                });
            }
        });
    </script>
</section>
