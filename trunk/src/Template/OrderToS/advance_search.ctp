
<div class="row">
    <div class="col-sm-12">
        <?php
        echo $this->Form->create(null, [
            'role' => 'form',
            'class' => 'form-horizontal form-product-list-search',
            'name' => 'search_form',
            'onsubmit' => 'return false;',
        ]);
        $this->Form->templates([
            'inputContainer' => '{{content}}'
        ]);
        ?>
        <div class="form-group">
            <label class="col-sm-4 control-label"><?= __('TXT_BRAND_NAME') ?></label>
            <div class="col-sm-7 custom-select">
                <?php
                $options = [];
                if ($seller) {
                    if ($seller->seller_brands) {
                        foreach ($seller->seller_brands as $value) {
                            $name = !empty($value->product_brand->name) ? $value->product_brand->name : $value->product_brand->name_en;
                            $options[$value->product_brand_id] = $name;
                        }
                    }
                }
                echo $this->Form->input('brand_id', [
                    'type' => 'select',
                    'class' => 'form-control',
                    'label' => false,
                    'required' => false,
                    'options' => $options,
                    'empty' => ['' => __('TXT_SELECT_BRAND_NAME')]
                ]);
                ?>
            </div>
        </div>
        <div class="form-group custom-select">
            <label class="col-sm-4 control-label"><?= __('TXT_PRODUCT_NAME') ?></label>
            <div class="col-sm-7 custom-select">
                <?php
                echo $this->Form->input('product_id',[
                    'type' => 'select',
                    'class' => 'form-control',
                    'label' => false,
                    'required' => false,
                    'options' => ['' => __('TXT_SELECT_PRODUCT_NAME')],
                    'disabled' => true,
                ]);
                ?>
            </div>
        </div>
        <?= $this->Form->end() ?>
    </div>
</div>
<div class="row">
    <div class="col-sm-12 table-product-price">
        <table class="table table-condensed table-striped table-product-detail">
            <thead>
                <tr>
                    <th>&nbsp;</th>
                    <th>#</th>
                    <th><?= __('TXT_PRODUCT_DETAILS') ?></th>
                </tr>
            </thead>
            <tbody></tbody>
        </table>
    </div>
</div>

<script>
    (function(e) {
        var modal = $('body').find('#advance-search');

        $(modal).on('change', '#brand-id', function(e) {
            var brandId = $(this).val();
            var url = '<?= $this->Url->build('/order-to-s/getProductByBrandId/') ?>';

            if (brandId !== '') {
                $.LoadingOverlay('show');
                ajax_request_get(url, {brandId: brandId}, function(data) {
                    var response = data.data;
                    var content = $('body').find('#advance-search');

                    if (response !== null && response !== 'undefined') {
                        var element = '';
                        $.each(response, function(i, v) {
                            element += '<option value="' + i + '">' + v + '</option>';
                        });

                        $(content).find('#product-id').removeAttr('disabled');
                        $(content).find('#product-id option:not(:first)').remove();
                        $(content).find('#product-id').append(element);
                    }
                }, 'json', '<?= __('TXT_SESSION_TIMEOUT') ?>');
            }
        });

        $(modal).on('change', '#product-id', function(e) {
            var productId = $(this).val();
            var url = '<?= $this->Url->build('/order-to-s/getProductDetailByProductId/') ?>';

            if (productId !== '') {
                $.LoadingOverlay('show');
                ajax_request_get(url, {productId: productId}, function(data) {
                    var table = $('body').find('#advance-search .table-product-detail tbody');
                    $(table).empty();

                    if (data !== null && data !== 'undefined') {
                        $(table).html(data);
                    }
                }, 'html', '<?= __('TXT_SESSION_TIMEOUT') ?>');
            }
        });

        $(modal).on('click', '.check-product', function(e) {
            $('.check-product').not(this).prop('checked', false);

            if ($('.check-product:checked')) {
                $('body').find('.prd-detail-add').removeAttr('disabled');
            } else {
                $('body').find('.prd-detail-add').attr('disabled', true);
            }
        });

        $(modal).on('click', '.prd-detail-add', function(e) {
            var tr = $('body').find('.check-product:checked').closest('tr');
            var modal = $('body').find('#modalAddItem');
            var parent = $('body').find('#modalAddItem #product-detail-id');
            var params = {
                id: $(tr).attr('data-id'),
                productName: $('body').find('#product-id option:selected').text(),
                productDetailName: $(tr).find('td:eq(2)').text(),
                singleUnitName: $(tr).attr('data-su-name'),
                singleUnitValue: $(tr).attr('data-su-value'),
                packSizeValue: $(tr).attr('data-ps-value'),
                packSizeName: $(tr).attr('data-ps-name'),
                productBrand: $('body').find('#brand-id option:selected').text(),
                packingId: $(tr).attr('data-pack'),
                description: $(tr).attr('data-dsc')
            };

            $(parent).select2().val(params.id).trigger('change');
            $(this).closest('.modal').modal('hide');

            // Change Value for Sale Stock Detail
            $(modal).find('input[name="brand_name"]').val(params.productBrand);
            $(modal).find('input[name="product_name"]').val(params.productName);
            $(modal).find('input[name="single_unit_value"]').val(params.singleUnitValue);
            $(modal).find('input[name="single_unit_name"]').val(params.singleUnitName);
            $(modal).find('input[name="pack_size_value"]').val(params.packSizeValue);
            $(modal).find('input[name="pack_size_name"]').val(params.packSizeName);
            $(modal).find('input[name="packing_id"]').val(params.packingId);
            $(modal).find('input[name="description"]').val(params.description);
        });
    })();
</script>
