
<?php
$affiliation_class = $this->request->session()->read('user_group.affiliation_class');
echo $this->Form->create($data ? $data : null, [
    'role' => 'form',
    'class' => 'form-horizontal',
    'name' => 'order_form',
    'onsubmit' => 'return false;',
]);
$this->Form->templates([
    'inputContainer' => '<div class="col-sm-9 custom-select">{{content}}</div>',
]);
echo $this->Form->hidden('type', [
    'id' => 'type',
    'value' => $data ? $data->type : null,
]);

if ($data) {
    echo $this->Form->hidden('id', [
        'value' => $data->id
    ]);
    $affiliation_class = $data->affiliation_class;
}
echo $this->Form->hidden('affiliation_class', ['value' => $affiliation_class]);
?>
<div class="form-group">
    <label class="col-sm-3 control-label"><?= __('TXT_SELLER'); ?><i class="required-indicator">＊</i></label></label>
    <?php
    echo $this->Form->input('seller_id', [
        'type' => 'select',
        'class' => 'form-control',
        'label' => false,
        'options' => $sellers,
    ]);
    ?>
</div>
<div class="form-group">
    <label class="col-sm-3 control-label"><?= __('TXT_ISSUE_DATE'); ?><i class="required-indicator">＊</i></label></label>
    <?php
    echo $this->Form->input('issue_date', [
        'type' => 'text',
        'class' => 'form-control',
        'label' => false,
        'placeholder' => __('TXT_SELECT_DATE'),
        'value' => $data ? date('Y-m-d', strtotime($data->issue_date)) : null,
    ]);
    ?>
</div>
<div class="form-group custom-select">
    <label class="col-sm-3 control-label"><?= __('TXT_CURRENCY'); ?><i class="required-indicator">＊</i></label></label>
    <?php
    echo $this->Form->input('currency_id', [
        'type' => 'select',
        'class' => 'form-control',
        'label' => false,
        'options' => $currencies,
    ]);
    ?>
</div>
<?php echo $this->Form->end(); ?>

<script>
    (function(e) {
        $('#seller-id, #currency-id').select2({width: '100%'});
        $('#issue-date').datetimepicker({
            format: 'YYYY-MM-DD',
            minDate: '2007-01-01'
        });

        $('body').on('click', '.btn-order-register', function(e) {
            $.LoadingOverlay('show');
            var form = $('body').find('#modal-register form');
            var url = '<?= $this->Url->build(['action' => 'create-order']) ?>';

            ajax_request_post(url, $(form).serialize(), function(data) {
                $(form).find('.error-message').remove();

                if (data.message === '<?= MSG_ERROR ?>') {
                    displayError(form, data.data);
                } else {
                    location.reload();
                }
            });
        });
    })();
</script>
