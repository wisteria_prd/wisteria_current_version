
<style>
.label-non-pack {
    padding: 3px 5px;
    font-size: 12px;
    border-radius: 2px;
    width: 60px;
    text-align: center;
    color: #fff;
    cursor: pointer;
}
</style>
<?php
$this->assign('title', 'Order Detail');
$userGroup = $this->request->session()->read('user_group');
$local = $this->request->session()->read('tb_field');
$fax = '';

//Total Amount
$totalAmount = 0;
?>
<div class="row">
    <div class="col-md-6">
        <h1><?= __('TXT_ORDER_FORM') ?></h1>
        <p><strong><?= __('TXT_ORDER_FORM_NUMBER') ?></strong>&nbsp;<?= $data->purchase_number ?></p>
        <p>
            <strong><?= __('TXT_ISSUE_DATE') ?>:</strong>&nbsp;
            <?php
            if ($data->issue_date) {
                echo date('Y年m月d日 ', strtotime($data->issue_date));
            }
            ?>
        </p>
    </div>
    <div class="col-md-6 text-right">
        <div class="company-invoice-logo text-center">
            Logo Here
        </div>
        <p>3791 Jalan Bukit Merah, #10-17 E-Centre @ Redhill Singapore 159471</p>
        <P>Tel: +65-6274-0433 / Fax: +65-6274-0477</P>
        <P>UEN No: 201403392G / Reg No: 201403392G</P>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <hr class="hr-invoiced">
    </div>
</div>
<div class="row row-top-space">
    <div class="col-md-8">
        <div class="seller-info" id="seller-id" data-id="<?= $data->seller->id ?>">
            <h3>
                <?php
                switch ($data->seller->type) {
                    case TYPE_SUPPLIER:
                        $id = $data->seller->supplier->id;
                        $fax = $data->seller->supplier->fax;
                        echo $this->Comment->getFieldByLocal($data->seller->supplier, $local);
                        break;
                    default :
                        $id = $data->seller->manufacturer->id;
                        $fax = $data->seller->manufacturer->fax;
                        echo $this->Comment->getFieldByLocal($data->seller->manufacturer, $local);
                }
                ?>
            </h3>
            <div class="row">
                <div class="col-md-3"><p><?= __('TXT_ADDRESS') ?></p></div>
                <div class="col-md-9">
                    <?php
                    switch ($data->seller->type) {
                        case TYPE_SUPPLIER:
                            echo $this->Comment->address($local, $data->seller->supplier);
                            break;
                        default :
                            echo $this->Comment->address($local, $data->seller->manufacturer);
                    }
                    ?>
                </div>
            </div>
            <div class="row invoice-form-row">
                <div class="col-md-3"><div class="invoice-form-label"><?= __('TXT_CONTACT') ?></div></div>
                <div class="col-md-4 custom-select">
                    <?= $this->Form->select('seller_id', $person, [
                        'class' => 'form-control select-seller',
                        'value' => $data->person_in_charge_id ? $data->person_in_charge_id : null,
                        'label' => false
                    ]) ?>
                </div>
            </div>
            <div class="row invoice-form-row">
                <div class="col-md-3"><div class="invoice-form-label"><?= __('TXT_EMAIL') ?></div></div>
                <div class="col-md-4 custom-select">
                    <?php
                    $optEmail = ['' => __('TXT_SELECT_EMAIL')];
                    if (!empty($data->email)) {
                        $optEmail[$data->person_in_charge_id] = $data->email;
                    }
                    echo $this->Form->input('email', [
                        'type' => 'select',
                        'class' => 'form-control select-email',
                        'label' => false,
                        'options' => $optEmail,
                        'value' => $data->person_in_charge_id ? $data->person_in_charge_id : null,
                        'disabled' => $data->email ? false : true,
                        'templates' => [
                            'inputContainer' => '{{content}}',
                        ],
                    ]);
                    ?>
                </div>
            </div>
            <div class="row invoice-form-row">
                <div class="col-md-3 custom-select">
                    <div class="invoice-form-label"><?= __('TXT_TEL') ?></div>
                </div>
                <div class="col-md-4 custom-select">
                    <?php
                    $optTel = ['' => __('TXT_SELECT_TEL')];
                    if (!empty($data->tel)) {
                        $optTel[$data->person_in_charge_id] = $data->tel;
                    }
                    echo $this->Form->input('tel', [
                        'type' => 'select',
                        'class' => 'form-control select-tel',
                        'label' => false,
                        'options' => $optTel,
                        'value' => $data->person_in_charge_id ? $data->person_in_charge_id : null,
                        'disabled' => $data->tel ? false : true,
                        'templates' => [
                            'inputContainer' => '{{content}}',
                        ],
                    ]);
                    ?>
                </div>
                <div class="col-md-1" style="padding-right: 0;">
                    <div class="invoice-form-label">/ <?= __('TXT_FAX') ?></div>
                </div>
                <div class="col-md-4">
                    <?php echo $this->Form->input('fax', [
                        'type' => 'text',
                        'class' => 'form-control txt-fax',
                        'label' => false,
                        'readonly' => 'readonly',
                        'placeholder' => __('TXT_ENTER_FAX'),
                        'value' => $fax,
                        'templates' => [
                            'inputContainer' => '{{content}}',
                        ],
                    ]); ?>
                </div>
            </div>
            <div class="row invoice-form-row">
                <div class="col-md-offset-3 col-md-4">
                    <?php
                    echo $this->Form->button(__('TXT_UPDATE'), [
                        'type' => 'button',
                        'class' => 'btn btn-primary btn-sm btn-add-info',
                        'disabled' => $data->person_in_charge_id ? false : true,
                    ]);
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row row-invoice-incharge">
    <div class="col-md-offset-8 col-md-4">
        <p>Order in Charge: #xxx</p>
        <p>Email: #xxx</p>
        <p>Mobile: #xxx</p>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <table class="table table-striped table-bordered table-po">
            <thead>
                <tr>
                    <th><?= __('No') ?></th>
                    <th width="35%"><?= __('TXT_DESCRIPTION') ?></th>
                    <th><?= __('TXT_UNIT_PRICE') ?></th>
                    <th><?= __('TXT_QUANTITY') ?></th>
                    <th><?= __('TXT_AMOUNT') ?></th>
                    <th width="5%"><?= __('TXT_DELETE') ?></th>
                </tr>
            </thead>
            <tbody>

                <?php
                if ($purchaseDetails) {
                    $num = 0;
                    for ($i=0; $i<(20-$countRecords); $i++) {
                        $num += 1;
                        if (isset($purchaseDetails[$i])) {
                            $item = $purchaseDetails[$i];
                            $childs = $item->children;
                            $amount1 = intval($item->unit_price) * intval($item->quantity);
                            $totalAmount += $amount1;
                            $is_discount = ($item->is_discount == 1) ? '<label class="label-foc-child no-margin">Discounted</label>&nbsp;' : null;

                            // display parent record
                            echo '<tr data-id="' . $item->id . '" data-type="parent" data-target="' . TARGET_EDIT . '">' .
                            '<td class="text-center">' . $num . '</td>' .
                            '<td>' . $this->Comment->POProductDetailName($item) . '</td>' .
                            '<td>' . $is_discount . $data->currency->code . ' ' . $this->Comment->numberFormat($item->unit_price) . '</td>' .
                            '<td>' . $item->quantity . '</td>' .
                            '<td>' . $data->currency->code . ' ' . $this->Comment->numberFormat((intval($item->unit_price) * intval($item->quantity))) . '</td>' .
                            '<td class="text-center"><button type="button" class="btn btn-danger btn-sm btn-rm-row"><span class="glyphicon glyphicon-trash"></span></button></td></tr>';

                            // display parent record
                            if ($childs) {
                                foreach ($childs as $key => $value) {
                                    $is_discount1 = ($value->is_discount == 1) ? '<label class="label-foc-child no-margin">Discounted</label>&nbsp;' : null;
                                    $num += 1;
                                    $amount2 = intval($value->unit_price) * intval($value->quantity);
                                    $totalAmount += $amount2;

                                    echo '<tr data-id="' . $value->id . '" data-type="child" data-target="' . TARGET_EDIT . '">' .
                                    '<td class="text-center">' . $num . '</td>' .
                                    '<td><label class="label-foc-child no-margin">FOC</label>&nbsp;' . $this->Comment->POProductDetailName($value) . '</td>' .
                                    '<td>' . $is_discount1 . $data->currency->code . ' ' . $this->Comment->numberFormat($value->unit_price) . '</td>' .
                                    '<td>' . $value->quantity . '</td>' .
                                    '<td>' . $data->currency->code . ' ' . $this->Comment->numberFormat((intval($value->unit_price) * intval($value->quantity))) . '</td>' .
                                    '<td class="text-center"><button type="button" class="btn btn-danger btn-sm btn-rm-row"><span class="glyphicon glyphicon-trash"></span></button></td></tr>';
                                }
                            }
                        } else {
                            echo '<tr data-target="' . CUSTOMER_TYPE_NEW . '" data-id="" data-type="">' .
                            '<td class="text-center">' . $num . '</td>' .
                            '<td>&nbsp;</td>' .
                            '<td>&nbsp;</td>' .
                            '<td>&nbsp;</td>' .
                            '<td>&nbsp;</td>' .
                            '<td></td>' .
                            '</tr>';
                        }
                    }
                } else {
                    for ($i=1; $i<=20; $i++) {
                        echo '<tr data-target="' . CUSTOMER_TYPE_NEW . 'data-id="" data-type="">' .
                            '<td class="text-center">' . $i . '</td>' .
                            '<td>&nbsp;</td>' .
                            '<td>&nbsp;</td>' .
                            '<td>&nbsp;</td>' .
                            '<td colspan="2">&nbsp;</td>' .
                            '</tr>';
                    }
                }
                ?>

                <!-- Table Footer -->
                <tr class="table-po-footer">
                    <td colspan="2" style="vertical-align: top;"><b>Remark :</b></td>
                    <td colspan="2" class="table-text-right"><b><?= __('TXT_TOTAL') ?> :</b></td>
                    <td colspan="2" style="border: 1px solid #ddd !important;">
                        <?php
                        echo $data->currency->code . ' ' . $this->Comment->numberFormat($totalAmount);
                        ?>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
<div class="row">
    <div class="col-md-8 col-sm-8">
        <div class="row">
            <div class="col-md-8">
                <p>Medical Professionals Singapore Pte. Ltd. Peter Lim, Managing Director</p>
                <p class="company-invoice-signature">Digital Signature</p>
                <hr class="signature-line">
            </div>
            <div class="col-md-4">
                <p>Date:</p>
                <div class="input-group with-datepicker">
                    <div class="input text"><input type="text" name="date" class="form-control datepicker" id="date"></div>            <div class="input-group-btn">
                        <button type="button" class="btn btn-default trigger-calendar" aria-label="Calendar">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </button>
                    </div>
                </div>
                <p></p>
                <hr class="signature-line">
            </div>
        </div>
    </div>
    <div class="col-md-4 col-sm-4">
        <?= $this->element('order_to_s') ?>
    </div>
</div>

<!--Modal Add New PurchaseDetail-->
<div class="modal fade" id="modalAddItem" role="dialog" data-backdrop="static">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"><?= __('TXT_PO_ADD_ITEM') ?></h4>
            </div>
            <div class="modal-body modal-body-fix"></div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default btn-sm btn-width close-modal" data-dismiss="modal"><?= __('BTN_CANCEL') ?></button>
                <button type="button" class="btn btn-primary btn-sm btn-width btn-add-detail"><?= __('TXT_REGISTER') ?></button>
            </div>
        </div>
    </div>
</div>
<!--Modal Advance Search-->
<div class="modal fade" id="advance-search" role="dialog" data-backdrop="static">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"><?= __('TXT_ADVANCED_FIND_PRODUCTS') ?></h4>
            </div>
            <div class="modal-body modal-body-fix"></div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default btn-sm btn-width close-modal" data-dismiss="modal"><?= __('BTN_CANCEL') ?></button>
                <button type="button" class="btn btn-primary btn-sm btn-width prd-detail-add" disabled="disabled"><?= __('TXT_SAVE') ?></button>
            </div>
        </div>
    </div>
</div>
<!--Modal Advance Search-->
<div class="modal fade" id="advance-search" role="dialog" data-backdrop="static">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"><?= __('TXT_ADVANCED_FIND_PRODUCTS') ?></h4>
            </div>
            <div class="modal-body modal-body-fix"></div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default btn-sm btn-width close-modal" data-dismiss="modal"><?= __('BTN_CANCEL') ?></button>
                <button type="button" class="btn btn-primary btn-sm btn-width prd-detail-add" disabled="disabled"><?= __('TXT_SAVE') ?></button>
            </div>
        </div>
    </div>
</div>
<!-- Modal Discount Conditions -->
<div class="modal fade" id="modalDiscountCondition" role="dialog" data-backdrop="static">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"><?= __('STR_DISCOUNT_CONDITIONS') ?></h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <table class="table table-striped table-view-discount">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th><?= __('TXT_FROM') ?></th>
                                    <th><?= __('TXT_TO') ?></th>
                                    <th>&nbsp;</th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default btn-sm btn-width close-modal" data-dismiss="modal"><?= __('BTN_CANCEL') ?></button>
                <button type="button" class="btn btn-primary btn-sm btn-width"><?= __('TXT_REGISTER') ?></button>
            </div>
        </div>
    </div>
</div>
<!--Modal Upload PDF File-->
<div class="modal fade" id="upload-pdf" role="dialog" data-backdrop="static">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"><?= __('TXT_DOCUMENT_UPLOAD') ?></h4>
            </div>
            <div class="modal-body">
                <?php
                echo $this->Form->create(null, [
                    'onsubmit' => 'return false;',
                    'role' => 'form',
                    'class' => 'form-horizontal',
                    'type' => 'file',
                    'name' => 'pdf-form',
                ]);
                $this->Form->templates([
                    'inputContainer' => '<div class="col-sm-8">{{content}}</div>',
                ]);
                echo $this->Form->hidden('seller_id', ['value' => $data->id]);
                ?>
                <div class="form-group">
                    <label class="col-sm-4 control-label"><?= __('TXT_TYPE') ?></label>
                    <?php
                    echo $this->Form->input('document_type', [
                        'type' => 'select',
                        'class' => 'form-control',
                        'label' => false,
                        'required' => false,
                        'options' => $typePdf,
                        'id' => false,
                    ]);
                    ?>
                </div>
                <div class="form-group">
                    <label class="col-sm-4 control-label"><?= __('TXT_PDF_FILE') ?></label>
                    <?php
                    echo $this->Form->input('file', [
                        'type' => 'file',
                        'class' => 'form-control',
                        'label' => false,
                        'required' => false,
                        'options' => $typePdf,
                    ]);
                    ?>
                </div>
                <?php echo $this->Form->end(); ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default btn-sm btn-width close-modal" data-dismiss="modal"><?= __('BTN_CANCEL') ?></button>
                <button type="button" class="btn btn-primary btn-sm btn-width btn-upload-file"><?= __('TXT_DOCUMENT_UPLOAD') ?></button>
            </div>
        </div>
    </div>
</div>
<?= $this->element('Modal/delete') ?>

<?php
echo $this->Html->css([
    'bootstrap-datetimepicker.min',
    'jquery-ui',
]);
?>

<script>
    $(function() {
        var local = '<?= $this->request->session()->read('tb_field') ?>';
        var optionData = [];

        $('.datepicker, #delivery-date').datetimepicker({
            format: 'YYYY-MM-DD',
            minDate: '2007-01-01'
        });

        $('.trigger-calendar').click(function() {
            $(this).parents('.with-datepicker').find('.datepicker').focus();
        });

        $('body').on('click', '.browse-file-edit', function(e) {
            var modal = $('body').find('#upload-pdf');
            var params = {
                document_type: $(this).closest('li').attr('data-document-type'),
                name: $(this).closest('li').attr('data-name'),
                type: $(this).closest('li').attr('data-type')
            };
            var fileName = '<h4 class="file-name text-center"><span class="label label-success">' + params.name + '</span></h4>';

            $(modal).find('select[name="document_type"]').val(params.document_type);
            $(modal).find('form').append(fileName);
            $(modal).modal('show');
        });

        $('#upload-pdf').on('hidden.bs.modal', function (e) {
            $(this).find('select[name="document_type"]').val('');
            $(this).find('.file-name').remove();
        });

        $('body').on('click', '.custom-wrap li .browse-file', function(e) {
            var index = $(this).closest('li').index();

            $('body').find('#upload-pdf').modal('show');
            $('body').find('#upload-pdf .btn-upload-file').attr('data-index', index);
        });

        $('body').on('click', '.rm-pdf-file', function(e) {
            e.stopPropagation();
            var li = $(this).closest('.custom-wrap li').index();

            if (li == 0) {
                $(this).closest('li').find('span:first').remove();
            } else {
                $(this).closest('li').remove();
            }
            optionData = $.grep(optionData, function(value, index) {
                return value.index != li;
            });
        });

        $('body').on('click', '.add-pdf-file', function(e) {
            e.stopPropagation();
            var content = $(this).closest('.custom-wrap');
            var li = '<li data-type="new"><span class="label label-success browse-file"><i class="fa fa-times rm-pdf-file" aria-hidden="true"></i>&nbsp;&nbsp;Browse File</span><span>&nbsp;</span></li>';
            $(content).append(li);
        });

        $('body').on('click', '.btn-upload-file', function(e) {
            var form = $('body').find('form[name="pdf-form"]');
            var index = $(this).attr('data-index');
            var arr = {
                file: $(form).find('#file')[0].files[0],
                external_id: $(form).find('input[name="seller_id"]').val(),
                document_type: $(form).find('select[name="document_type"]').val(),
                index: index
            };
            optionData.push(arr);
            $('body').find('#upload-pdf').modal('hide');
        });

        $('body').on('click', '.table-po tbody tr:not(.table-po-footer)', function(e) {
            $.LoadingOverlay('show');
            var params = {
                type: $(this).attr('data-type'),
                target: $(this).attr('data-target'),
                url: '<?= $this->Url->build('/purchase-details/get-form-create/') ?>',
                purchase_id: '<?= $data->id ?>',
                id: $(this).attr('data-id'),
                seller_id: '<?= $data->seller->id ?>'
            };

            ajax_request_get(params.url, params, function(data) {
                if (data !== null && data !== 'undefined') {
                    var modal = $('body').find('#modalAddItem');
                    $(modal).find('.modal-body').html(data);
                    $(modal).modal('show');
                    $(modal).find('.popup-discount').removeAttr('disabled');
                    $(modal).find('#parent-id, #product-detail-id').select2({width: '100%'});
                }
            }, 'html', '<?= __('TXT_SESSION_TIMEOUT') ?>');
        });

        $('body').on('click', '.btn-rm-row', function(e) {
            e.stopPropagation();
            var id = $(this).closest('tr').attr('data-id');
            var content = $('#modal_delete');

            $(content).find('#btn_delete_yes').attr('data-id', id);
            $(content).modal('show');
        });

        $('body').on('click', '#btn_delete_yes', function(e) {
            var url = '<?php echo $this->Url->build('/purchase-details/delete/'); ?>';

            ajax_request_post(url, {id: $(this).data('id')}, function(data) {
                if (data.message === '<?= MSG_SUCCESS ?>') {
                    location.reload();
                }
            }, '<?= __('TXT_SESSION_TIMEOUT') ?>');
        });

        $('body').on('click', '.btn-register', function() {
            $.LoadingOverlay('show');
            var form = $('body').find('form[name="sale_status"]');
            var affiliationClass = '<?= $userGroup->affiliation_class ?>';
            var url = '<?= $this->Url->build(['action' => 'updateStatus']) ?>';
            var step = parseInt($(this).attr('data-step'));

            $(form).find('input[name="step"]').val(step + 1);
            $(form).find('button[type="submit"]').attr('data-step', step + 1);

            if (step == 2) {
                var formData = new FormData();
                formData.append('step', step);
                formData.append('status', $(form).find('#status').val());
                formData.append('id', $(form).find('input[name="id"]').val());

                // new file
                if (optionData !== null && optionData !== 'undefined') {
                    $.each(optionData, function(i, v) {
                       formData.append('files[]', v.file);
                       formData.append('types[]', v.document_type);
                    });
                }

                // old file
                var oldFile = $('body').find('.custom-wrap li');
                if (oldFile.length > 0) {
                    $.each(oldFile, function(i, v) {
                        if ($(this).attr('data-type') === 'edit') {
                            formData.append('old_files[]', $(this).attr('data-name'));
                            formData.append('old_doc_types[]', $(this).attr('data-document-type'));
                        }
                    });
                }

                ajax_post_type_file('<?= $this->Url->build(['action' => 'statusDownload']) ?>', formData, function(data) {
                    data = JSON.parse(data);
                    renderStep(form, data);
                }, '<?= __('TXT_SESSION_TIMEOUT') ?>');
            } else {
                ajax_request_post(url, $(form).serialize(), function(data) {
                    if (data.message === '<?= MSG_SUCCESS ?>') {
                        renderStep(form, data);
                    }
                }, '<?= __('TXT_SESSION_TIMEOUT') ?>');
            }
        });

        $('body').on('click', '.btn-update-delivery', function(e) {
            $.LoadingOverlay('show');
            var url = '<?= $this->Url->build('/imports/create/') ?>';
            var form = $('body').find('form[name="sale_status"]');
            var user_group = JSON.parse('<?= json_encode($userGroup) ?>');
            var params = {
                tracking: $(form).find('#tracking').val(),
                delivery_date: $(form).find('#delivery-date').val(),
                purchase_id: '<?= $data->id ?>',
                affiliation_class: user_group.affiliation_class,
                import_id: $(form).find('#import-id').val()
            };

            ajax_request_post(url, params, function(data) {
                $(form).find('.error-message').remove();

                if (data.message === '<?= MSG_ERROR ?>') {
                    displayError(form, data.data);
                } else {
                    $('body').find('.btn-register').removeAttr('disabled', true);
                }
            }, '<?= __('TXT_SESSION_TIMEOUT') ?>');
        });

        /**
         * use for render html to po footer
         * @param {html} form
         * @param {object} data
         * @returns {string}
         */
        function renderStep(form, data)
        {
            var buttonSubStatus = '<button class="btn btn-sm btn-primary update-sub-status"><?= __('TXT_UPDATE') ?></button>';

            switch(parseInt(data.data.step)) {
                case 2 :
                   // Change Status to Delivering
                    var importData = JSON.parse('<?= json_encode($data) ?>');
                    var medias = JSON.parse('<?= json_encode($medias) ?>');
                    var date_time = new Date();
                    var currentDate = date_time.getFullYear() + '-' + (date_time.getMonth()+1) + '-' + date_time.getDate();
                    var arr1 = {
                        tracking: '',
                        delivery_date: currentDate
                    };

                    if (importData !== null && importData !== 'undefined') {
                        if (importData.purchase_imports.length > 0) {
                            arr1.tracking = importData.purchase_imports[0].import.tracking,
                            arr1.delivery_date = importData.purchase_imports[0].import.delivery_date;
                            $(form).find('.status-wrap').append('<input type="hidden" name="delivery-id"/>')
                        }
                    }

                    var element1 = '<div class="form-group">' +
                            '<label class="col-sm-6 control-label text-left"><?= __('TXT_CUSTOM_CLEARANCE') ?></label>' +
                            '<div class="col-sm-6 col-md-6"><ul class="custom-wrap">' + add_pdf_file(medias) + '</ul></div>' +
                            '</div>';
                    var element = '<div class="form-group">' +
                            '<label class="col-sm-5 control-label text-left"><?= __('TXT_TRACKING_NUMBER') ?></label>' +
                            '<div class="col-sm-1"></div>' +
                            '<div class="col-sm-6"><input type="text" name="tracking" value="' + arr1.tracking + '" class="form-control" placeholder="<?= __('TXT_TRACKING_NUMBER') ?>" id="tracking"></div></div>'+
                            '<div class="form-group">' +
                            '<label class="col-sm-5 control-label text-left"><?= __('TXT_DELIVERY_DATE') ?></label>' +
                            '<div class="col-sm-1"></div>' +
                            '<div class="col-sm-6"><input type="text" value="' + arr1.delivery_date + '" name="delivery_date" class="form-control" id="delivery-date"></div></div>' +
                            '<div class="form-group"><div class="col-sm-12 text-right"><button type="button" class="btn btn-sm btn-primary btn-update-delivery"><?= __('TXT_UPDATE') ?></button></div></div>' + element1;

                    $(form).find('.status-wrap').prepend(element);
                    $(form).find('.label-draft').text('<?= __('TXT_REQUESTED_SHIPMENT') ?>');
                    $(form).find('.btn-register').text('<?= __('TXT_SUBMIT') ?>');
                    $(form).find('#status').empty().append('<option selected="selected" value="<?= PO_STATUS_REQUESTED_CC ?>"><?= __('TXT_DELIVERING') ?></option>');
                    $(form).find('#delivery-date').datetimepicker({
                        format: 'YYYY-MM-DD',
                        minDate: '2007-01-01'
                    });
                    break;
                case 3 :
                    // Change status Delivering to Delivered
                    var li = '';
                    var data = JSON.parse('<?= json_encode($data) ?>');
                    if (data.purchase_imports !== null) {
                        var element = data.purchase_imports[0].import_id;
                        li = '&nbsp&nbsp<span class="label label-success" style="font-size: small;">' + element;
                    } else {
                        $('body').find('.btn-register').attr('disabled', true);
                    }

                    li = '<?= __('TXT_IMPORT_INFO') ?>' + li;
                    $(form).find('.label-draft').text('<?= __('TXT_DELIVERING') ?>');
                    $(form).find('#status').empty().append('<option selected="selected" value="<?= PO_STATUS_DELIVERED ?>"><?= __('TXT_DELIVERED') ?></option>');
                    $(form).find('.status-wrap').empty();

                    var content = $('body').find('.btn-register').closest('.form-group');
                    $(content).find('div:first').addClass('no-padding-left').append(li);
                    $(content).find('div:last').addClass('no-padding-right');
                    break;
                case 4 :
                    // Change status Delivered to Completed
                    var data = JSON.parse('<?= json_encode($data) ?>');
                    var li = '';
                    if (data.purchase_payments.length > 0) {
                        li = '&nbsp&nbsp<span class="label label-success" style="font-size: small;">' + data.purchase_payments[0].payment_id;
                    } else {
                        $('body').find('.btn-register').attr('disabled', true);
                    }

                    li = '<?= __('TXT_PAYMENT_INFO') ?>' + li;
                    $(form).find('.label-draft').text('<?= __('TXT_DELIVERED') ?>');
                    $(form).find('#status').empty().append('<option selected="selected" value="<?= STATUS_COMPLETED ?>"><?= __('TXT_COMPLETE') ?></option>');
                    $('body').find('.btn-register').closest('.form-group').find('div:first').html(li);
                    break;
                default :
                    $(form).find('.label-draft').text('<?= __('TXT_COMPLETE') ?>');
                    $(form).find('.form-group:eq(1) div:not(:first)').remove();
                    $(form).find('.form-group:eq(1)').append('<span>After 7days change to ‘completed’ auto</span>');
                    $(form).find('button[type="submit"]').remove();
                    $(form).find('.status-wrap').empty();
                    $('body').find('.btn-register').closest('.form-group').remove();
            }
        }

        /**
         * use for add pdf file to po footer
         * @param {object} data
         * @returns {string}
         * */
        function add_pdf_file(data)
        {
            var element = '<li data-type="new">' +
                            '<span class="label label-success browse-file">&nbsp;&nbsp;Browse File</span><span>&nbsp;</span>' +
                            '<span class="label label-primary add-pdf-file">Add</span>' +
                            '</li>';

            if (data !== null && data !== 'undefined') {
                $.each(data, function(i, v) {
                    if (i == 0) {
                        element = '<li data-id="' + v.id + '" data-name="' + v.file_name + '" data-document-type="' + v.document_type + '" data-type="edit">' +
                            '<span class="label label-success browse-file-edit">&nbsp;&nbsp;Browse File</span><span>&nbsp;</span>' +
                            '<span class="label label-primary add-pdf-file">Add</span>' +
                            '</li>';
                    } else {
                        element += '<li data-id="' + v.id + '" data-name="' + v.file_name + '" data-document-type="' + v.document_type + '" data-type="edit">' +
                            '<span class="label label-success browse-file-edit"><i class="fa fa-times rm-pdf-file" aria-hidden="true"></i>&nbsp;&nbsp;Browse File</span><span>&nbsp;</span>' +
                            '</li>';
                    }
                });
            }

            return element;
        }
    });
</script>
