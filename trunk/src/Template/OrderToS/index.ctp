
<div class="search-section" style="margin-bottom: 20px;">
    <?php
    echo $this->Form->create('Purchases', [
        'role' => 'form',
        'class' => 'form-inline form-search',
        'type' => 'get',
        'name' => 'form_search'
    ]);
    echo $this->Form->hidden('displays', [
        'value' => empty($this->request->query('displays')) ? 10 : $this->request->query('displays')
    ]);
    echo $this->Form->input('keyword', [
        'name' => 'keyword',
        'placeholder' => __('USER_SEARCH_KEYWORD'),
        'class' => 'form-control',
        'label' => false,
        'value' => $this->request->query('keyword'),
        'autocomplete' => 'off',
        'templates' => [
            'inputContainer' => '<div class"form-group">{{content}}</div>',
        ]
    ]);
    echo $this->Form->submit('submit', [
        'style' => 'display: none',
    ]);
    echo $this->Form->end();
    ?>
</div>
<div class="total-pagination" style="padding-bottom: 20px;">
    <?php echo $this->element('display_info'); ?>
</div>
<div class="row search-section" style="margin-bottom: 20px;">
    <?php echo $this->element('display_number'); ?>
    <div class="col-sm-6 pull-right" style="text-align: right;">
        <?php
        echo $this->Form->button(__('BTN_REGISTER'), [
            'type' => 'button',
            'class' => 'btn btn-sm btn-primary btn-width btn-register',
            'data-type' => 'new',
        ]);
        ?>
    </div>
    <div class="clearfix"></div>
</div>
<div class="data-tb-list">
    <?php if ($data): ?>
    <table class="table table-striped table-pos">
        <thead>
            <tr>
                <th>#</th>
                <th>&nbsp;</th>
                <th>
                    <?php echo $this->Paginator->sort('status', __('TXT_STATUS')); ?>
                    <i class="fa fa-sort-desc" aria-hidden="true"></i>
                </th>
                <th class="white-space">
                    <?php echo $this->Paginator->sort('purchase_number' . $local, __('TXT_OF_ID')); ?>
                    <i class="fa fa-sort-desc" aria-hidden="true"></i>
                </th>
                <th class="white-space">
                    <?php echo $this->Paginator->sort('Sellers.id', __('TXT_SUPPLIER_NAME')); ?>
                    <i class="fa fa-sort-desc" aria-hidden="true"></i>
                </th>
                <th class="white-space"><?= __('TXT_AMOUNT') ?></th>
                <th colspan="5"></th>
            </tr>
        </thead>
        <tbody>
            <?php
            $numbering = $this->Paginator->counter('{{start}}');
            foreach ($data as $key => $value) :
            ?>
            <tr data-id="<?= $value->id ?>">
                <th><?php echo $numbering; $numbering++; ?></th>
                <td><?= $this->ActionButtons->disabledText($value->is_suspend) ?></td>
                <td><?= __($this->Comment->textHumanize($value->status)) ?></td>
                <td>
                    <?php
                    echo $value->purchase_number . ' (' . date('Y-m-d', strtotime($value->issue_date)) . ')';
                    ?>
                </td>
                <td>
                    <?php
                    if ($value->seller->type === TYPE_MANUFACTURER) {
                        echo $this->Comment->getFieldByLocal($value->seller->manufacturer, $local);
                    } else {
                        echo $this->Comment->getFieldByLocal($value->seller->supplier, $local);
                    }
                    ?>
                </td>
                <td>
                    <?php
                    $total = 0;
                    if ($value->purchase_details) {
                        $total = $value->purchase_details[0]->total_amount;
                    }
                    echo $value->currency->code . ' ' . $this->Comment->numberFormat($total);
                    ?>
                </td>
                <td width="1%">
                    <?php
                    echo $this->Html->link(__('TXT_OF_DOWNLOAD_PDF'), [
                        'controller' => 'order-to-s',
                        'action' => 'detail', $value->id,
                    ], [
                        'class' => 'btn btn-sm btn-primary',
                    ]);
                    if ($value->status === PO_STATUS_DRAFF) {
                        echo $this->Form->button(__('TXT_BUTTON_FAX_ROP'), [
                            'type' => 'button',
                            'class' => 'btn btn-sm btn-primary btn-fax-rop',
                            'style' => 'width: 100%; margin-top: 5px;',
                        ]);
                    }
                    ?>
                </td>
                <td width="1%">
                    <?php
                    echo $this->Html->link(__('TXT_INVOICE'), [
                        'controller' => 'sale-reports',
                        'action' => 'custom-clearence-pdf', $value->id,
                    ], [
                        'class' => 'btn btn-sm btn-primary btn-edit-doc',
                        'target' => '_blank',
                    ]);
                    ?>
                </td>
                <td width="1%">
                    <?php echo $this->Form->button('<i class="fa fa-comments-o" aria-hidden="true"></i>', [
                        'class' => 'btn btn-primary btn-sm btn_comment',
                        'data-id' => $value->id,
                        'escape' => false,
                    ]); ?>
                </td>
                <td width="1%" data-target="<?= $value->is_suspend ? 1 : 0; ?>">
                    <?php
                    if ($value->is_suspend == 1) {
                        echo $this->Form->button(BTN_ICON_UNDO, [
                            'class' => 'btn btn-recover btn-sm btn_suspend',
                            'data-name' => 'recover',
                            'escape' => false
                        ]);
                    } else {
                        echo $this->Form->Button(BTN_ICON_EDIT, [
                            'class' => 'btn btn-primary btn-sm btn-register',
                            'escape' => false,
                            'data-type' => 'edit',
                        ]);
                    }
                    ?>
                </td>
                <td width="1%" data-target="<?= $value->is_suspend ? 1 : 0; ?>">
                    <?php
                    if ($value->is_suspend == 1) {
                        echo $this->Form->button(BTN_ICON_DELETE, [
                            'class' => 'btn btn-delete btn-sm',
                            'id' => 'btn_delete',
                            'data-name' => 'delete',
                            'escape' => false
                        ]);
                    } else {
                        echo $this->Form->button(BTN_ICON_STOP, [
                            'class' => 'btn btn-suspend btn-sm btn_suspend',
                            'data-name' => 'suspend',
                            'escape' => false
                        ]);
                    }
                    ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <?php endif; ?>
</div>
<div class="row row-top-space" style="padding-bottom: 20px;">
    <?php echo $this->element('display_number'); ?>
    <?= $this->element('next_prev') ?>
    <div class="clearfix"></div>
</div>

<!--Modal Comment-->
<?= $this->element('/Modal/message') ?>
<!--Modal Suspend-->
<?= $this->element('Modal/suspend'); ?>
<!--Modal delete-->
<?= $this->element('Modal/delete'); ?>
<!--Modal Register/Edit-->
<div class="modal fade" id="modal-register" data-backdrop="static" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"><?= __('TXT_ADVANCED_FIND_PRODUCTS') ?></h4>
            </div>
            <div class="modal-body"></div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default btn-sm btn-width" data-dismiss="modal"><?= __('TXT_CANCEL') ?></button>
                <button class="btn btn-primary btn-sm btn-width btn-order-register" type="button"><?= __('TXT_REGISTER') ?></button>
            </div>
        </div>
    </div>
</div>
<?php
echo $this->Html->css([
    'bootstrap-datetimepicker.min',
]);
?>
<script>
    $(function(e) {
        //auto complete
        $('body').find('#keyword').typeahead({
            minLength: 0,
            autoSelect: true,
            source:  function (query, process) {
                return $.get('<?= $this->Url->build('/purchases/getPurchaseNumberBySearch/') ?>', {
                        keyword: query
                    }, function (data) {
                        var optionData = [];
                        if (data.data !== null && data.data !== 'undefined') {
                            $.each(data.data, function(i, v) {
                                optionData.push(v.purchase_number);
                            });
                        }
                        return process(optionData);
                    }, 'json');
            }
        });

        $('body').on('click', '.typeahead li', function(e) {
            $('body').find('.form-search').submit();
        });

        $('body').on('click', '.btn-fax-rop', function(e) {
            $.LoadingOverlay('show');
            var id = $(this).closest('tr').attr('data-id');
            ajax_request_get('<?= $this->Url->build('/order-to-snps/getFaxRop/') ?>',
            { id: id },
            function(data) {
                if (data !== null && data !== 'undefined') {
                    $('body').append(data);
                    $('body').find('.fax-rop').modal('show');
                    $.LoadingOverlay('hide');
                }
            }, 'html');
        });

        $('.btn_comment').click(function (e) {
            e.stopPropagation();
            let params = {
                id: $(this).closest('tr').attr('data-id'),
                type: '<?php echo TRADE_TYPE_PURCHASE_MP_STOCK ?>',
                referer: '<?php echo $this->request->controller; ?>'
            };
            let options = {
                url: BASE + 'Messages/getMessageList',
                type: 'GET',
                data: params,
                beforeSend: function () {
                    $.LoadingOverlay('show');
                }
            };
            ajaxRequest(options, SESSION_TIMEOUT, function (data) {
                $('body').prepend(data);
                $('body').find('.modal-message').modal('show');
            });
        });

        //filter message list search to specific
        $('body').on('click', '.btn-to-individual', function() {
            $(this).removeClass('btn-detault').addClass('btn-primary');
            $('body').find('.btn-to-all').removeClass('btn-primary').addClass('btn-default');
            var toAll = $('.to-individual').val();
            if (parseInt(toAll) === 0) {
                $('.to-all').val(0);
                $('.to-individual').val(1);
            } else {
                $('.to-all').val(1);
                $('.to-individual').val(0);
            }
            messagesSearch(listUrlSearch);
        });

        $('body').on('change', '#keyword', function(e) {
            $('body').find('form[name="form_search"]').submit();
        });

        $('body').on('click', '.btn-search', function() {
            messagesSearch(listUrlSearch);
        });

        $('body').on('click', '.btn-suspend-msg', function() {
            var id = $(this).data('id');
            var data = $('#form-message-status-' + id).serialize();
            var suspendUrl = '<?= $this->Url->build(['controller' => 'Messages', 'action' => 'suspend']) ?>';
            suspendMessage(id, suspendUrl, data);
        });

        $('body').on('click', '.btn_suspend', function (e) {
            e.stopPropagation();
            var content = $('#modal_suspend');
            var status = $(this).attr('data-name');
            var txt = '';

            switch (status) {
                case 'suspend':
                    $('#btn_suspend_yes').text('<?= __('BTN_SUSPEND') ?>');
                    txt = '<?= __('MSG_CONFIRM_SUSPEND') ?>';
                    break;
                case 'delete':
                    txt = '<?= __('MSG_CONFIRM_DELETE') ?>';
                    $('#btn_suspend_yes').text('<?= __('BTN_DELETE') ?>');
                    break;
                 case 'recover':
                    $('#btn_suspend_yes').text('<?= __('BTN_RECOVER') ?>');
                    txt = '<?= __('MSG_CONFIRM_UNDO_SUSPEND') ?>';
                    break;
            };

            $(content).find('#btn_suspend_yes').data('id', $(this).closest('tr').data('id'));
            $(content).find('#btn_suspend_yes').data('target', $(this).closest('td').data('target'));
            $(content).find('.modal-body p').html(txt);
            $(content).modal('show');
        });

        $('body').on('click', '#btn_suspend_yes', function (e) {
            var params = {
                id: $(this).data('id'),
                is_suspend: $(this).data('target')
            };
            var url = '<?= $this->Url->build('/purchases/update-suspend/'); ?>';

            ajax_request_post(url, params, function(data) {
                if (data.message === 'success') {
                    location.reload();
                }
            }, '<?= __('TXT_SESSION_TIMEOUT') ?>');
        });

        $('body').on('click', '#btn_delete', function (e) {
            e.stopPropagation();
            var content = $('#modal_delete');

            $(content).find('#btn_delete_yes').attr('data-id', $(this).closest('tr').data('id'));
            $(content).modal('show');
        });

        $('body').on('click', '#btn_delete_yes', function (e) {
            var url = '<?= $this->Url->build('/purchases/delete/'); ?>';

            ajax_request_post(url, {id: $(this).data('id')}, function(data) {
                if (data.message === 'success') {
                    location.reload();
                }
            }, '<?= __('TXT_SESSION_TIMEOUT') ?>');
        });

        $('body').on('click', '.btn-register', function(e) {
            $.LoadingOverlay('show');
            var url = '<?= $this->Url->build(['action' => 'get-form']) ?>';
            var params = {
                type: null,
                id: null
            };

            if ($(this).attr('data-type') === 'edit') {
                params.type = 'edit';
                params.id = $(this).closest('tr').attr('data-id');
            }

            ajax_request_get(url, params, function(data) {
                if (data !== null && data !== 'undefined') {
                    $('body').find('#modal-register .modal-body').html(data);
                    $('body').find('#modal-register').modal('show');
                    $('body').find('#modal-register #type').val('<?= TRADE_TYPE_PURCHASE_MP_STOCK ?>');
                }
            }, 'html', '<?= __('TXT_SESSION_TIMEOUT') ?>');
        });

        var maxPage = parseInt($('#maxPage').val());
        updatePagination(maxPage);
    });
</script>
