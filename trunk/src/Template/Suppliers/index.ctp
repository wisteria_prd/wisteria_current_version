
<div class="search-section" style="margin-bottom: 20px;">
    <?php
    echo $this->Form->create(null, [
        'role' => 'form',
        'class' => 'form-inline form-search',
        'type' => 'get',
        'name' => 'form_search'
    ]);
    echo $this->Form->hidden('displays', [
        'value' => $display,
    ]);
    echo $this->Form->input('keyword', [
        'name' => 'keyword',
        'placeholder' => __('USER_SEARCH_KEYWORD'),
        'class' => 'form-control',
        'label' => false,
        'autocomplete' => 'off',
        'value' => $this->request->query('keyword'),
        'templates' => [
            'inputContainer' => '<div class="form-group">{{content}}</div>',
        ]
    ]);
    $this->SwitchStatus->render();
    echo $this->Form->end();
    ?>
</div>
<div class="total-pagination" style="padding-bottom: 20px;">
    <?php echo $this->element('display_info'); ?>
</div>
<div class="row search-section" style="padding-bottom: 20px;">
    <?php echo $this->element('display_number'); ?>
    <div class="col-sm-3 pull-right" style="text-align: right;">
        <?php
        echo $this->Html->link(__('TXT_REGISTER_NEW'), '/Suppliers/add', [
            'class' => 'btn btn-sm btn-primary btn-width',
        ]);
        ?>
    </div>
</div>
<div class="data-tb-list">
    <?php if ($suppliers): ?>
    <table class="table table-striped">
        <thead>
            <tr>
                <th>#</th>
                <th>&nbsp;</th>
                <th style="white-space: nowrap;">
                    <?php
                    echo $this->Paginator->sort('Suppliers.code', __('TXT_OLD_CODE')); ?>
                    <span><i class="fa fa-sort-desc" aria-hidden="true"></i></span>
                </th>
                <th style="white-space: nowrap;">
                    <?php
                    echo $this->Paginator->sort('Suppliers.name' . $locale, __('TXT_SUPPLIER')); ?>
                    <span><i class="fa fa-sort-desc" aria-hidden="true"></i></span>
                </th>
                <th style="white-space: nowrap;">
                    <?php
                    echo $this->Paginator->sort('Countries.name' . $locale, __('TXT_COUNTRY')); ?>
                    <span><i class="fa fa-sort-desc" aria-hidden="true"></i></span>
                </th>
                <th style="white-space: nowrap;">
                    <?php
                    echo __('TXT_URL') ?>
                </th>
                <th style="white-space: nowrap;">
                    <?php
                    echo __('SUPPLIER_SHOP_URL') ?>
                </th>
                <th style="white-space: nowrap;">
                    <?php
                    echo __('TXT_PERSON_IN_CHARGE'); ?>
                </th>
                <th>&nbsp;</th>
                <th width="1%">&nbsp;</th>
                <th width="1%">&nbsp;</th>
                <th width="1%">&nbsp;</th>
                <th width="1%">&nbsp;</th>
            </tr>
        </thead>
        <tbody>
            <?php
            $numbering = $this->Paginator->counter('{{start}}');
            foreach ($suppliers as $item):
                $manufacturer_id = !empty($item->sellers) ? $item->sellers[0]->manufacturer_id : 0;
            ?>
            <tr data-id="<?php echo $item->id ?>">
                <th scope="row" class="td_evt">
                    <?php
                    echo $numbering; $numbering++; ?>
                </th>
                <td>
                    <?php
                    echo $this->ActionButtons->disabledText($item->is_suspend) ?>
                </td>
                <td style="white-space: nowrap;">
                    <?php
                    echo h($item->code); ?>
                </td>
                <td style="white-space: nowrap;">
                    <?php echo $this->Comment->nameEnOrJp($item->name, $item->name_en, $locale); ?>
                </td>
                <td style="white-space: nowrap;">
                    <?php
                    if ($item->country) {
                        echo ($locale === '_en') ? $item->country->name_en : $item->country->name;
                    }
                    ?>
                </td>
                <td>
                    <?php
                    if ($item->url) {
                        echo $this->Html->link(__('TXT_WEBSITE'), $item->url, [
                            'class' => 'btn btn-primary btn-sm btn-width',
                            'target' => '_blank',
                        ]);
                    } else {
                        echo $this->Form->button(__('TXT_WEBSITE'), [
                            'class' => 'btn btn-sm btn-primary btn-width',
                            'value' => 'Website',
                            'disabled' => 'disabled',
                        ]);
                    }
                    ?>
                </td>
                <td>
                    <?php
                    if ($item->online_shop_url) {
                        echo $this->Html->link(__('BTN_ONLINE_SHOP'), $item->online_shop_url, [
                            'class' => 'btn btn-info btn-sm btn-width',
                            'target' => '_blank',
                        ]);
                    } else {
                        echo $this->Form->button(__('BTN_ONLINE_SHOP'), [
                            'class' => 'btn btn-info btn-sm btn-width btn-disabled',
                            'value' => 'online_shop_url',
                            'disabled' => 'disabled',
                        ]);
                    }
                    ?>
                </td>
                <td>
                    <?php
                    echo $this->Comment->btnPersonInCharge($item->id, $item->person_in_charges, TYPE_SUPPLIER); ?>
                </td>
                <td class="td-btn-wrap">
                    <?php
                    echo $this->Html->link(__('TXT_SELLER_BRAND'), [
                        'controller' => 'SellerBrands',
                        'action' => 'index',
                        '?' => [
                            'supplier_id' => $item->id,
                        ]
                    ], [
                        'class' => 'btn btn-primary btn-sm',
                    ]);
                    ?>
                </td>
                <td class="td-btn-wrap">
                    <?php
                    if ($manufacturer_id == 0) {
                        echo $this->Form->button(__('TXT_PRODUCT'), [
                            'class' => 'btn btn-primary btn-sm btn-width',
                            'disabled' => 'disabled',
                        ]);
                    } else {
                        echo $this->Html->link(__('TXT_PRODUCT'), [
                            'controller' => 'products',
                            'action' => 'index',
                            '?' => [
                                'filter_manufacturer' => $manufacturer_id,
                                'filter_brand' => 0,
                                'des' => TYPE_SUPPLIER,
                            ]
                        ], [
                            'class' => 'btn btn-primary btn-sm btn-width',
                        ]);
                    }
                    ?>
                </td>
                <td>
                    <?php
                    echo $this->Form->button('<i class="fa fa-comments-o" aria-hidden="true"></i>', [
                        'class' => 'btn btn-primary btn-sm btn_comment',
                        'data-id' => $item->id,
                        'escape' => false,
                    ]);
                    ?>
                </td>
                <td class="td-btn-wrap" data-target="<?php echo ($item->is_suspend) ? 0 : 1; ?>">
                    <?php
                    if ($item->is_suspend == 1) {
                        echo $this->Form->button(BTN_ICON_UNDO, [
                            'class' => 'btn btn-recover btn-sm btn-restore',
                            'escape' => false
                        ]);
                    } else {
                        echo $this->Html->link(BTN_ICON_EDIT, [
                            'controller' => 'suppliers',
                            'action' => 'edit', $item->id
                        ], [
                            'class' => 'btn btn-primary btn-sm',
                            'escape' => false
                        ]);
                    }
                    ?>
                </td>
                <td class="td-btn-wrap" data-target="<?php echo ($item->is_suspend) ? 0 : 1; ?>">
                    <?php
                    if ($item->is_suspend == 1) {
                        if (!empty($item->product_brands) || !empty($item->person_in_charges)) {
                            echo $this->Form->button(BTN_ICON_DELETE, [
                                'class' => 'btn btn-delete btn-sm btn-rm-record',
                                'disabled' => 'disabled',
                                'escape' => false
                            ]);
                        } else {
                            echo $this->Form->button(BTN_ICON_DELETE, [
                                'class' => 'btn btn-delete btn-sm btn-rm-record',
                                'id' => 'btn_delete',
                                'data-name' => 'delete',
                                'escape' => false
                            ]);
                        }
                    } else {
                        echo $this->Form->button(BTN_ICON_STOP, [
                            'class' => 'btn btn-suspend btn-sm btn-restore',
                            'escape' => false
                        ]);
                    }
                    ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <?php endif; ?>
</div>
<div class="row row-top-space">
    <?php echo $this->element('display_number'); ?>
    <?php echo $this->element('next_prev') ?>
</div>
<?php
echo $this->Html->script([
    'suppliers/suppliers',
], ['blog' => false]);
