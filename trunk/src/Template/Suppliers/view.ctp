
<section class="modal-wrapper">
    <div class="modal fade modal-view" data-backdrop="static" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <table class="table table-striped" id="tb-detail">
                        <tbody>
                            <tr>
                                <td>
                                    <?php
                                    echo __d('supplier', 'TXT_OLD_CODE'); ?> :
                                </td>
                                <td colspan="2">
                                    <?php
                                    echo $supplier->code; ?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <?php
                                    echo __d('supplier', 'TXT_SUPPLIER'); ?> :
                                </td>
                                <td colspan="2">
                                    <?php
                                    echo $this->Comment->getFieldByLocal($supplier, $locale); ?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <?php
                                    echo __d('supplier', 'TXT_NICKNAME'); ?> :
                                </td>
                                <td colspan="2">
                                    <?php
                                    $name = '';
                                    if (!empty($supplier->nickname) && ($locale === LOCAL_EN)) {
                                        $name = $supplier->nickname_en;
                                    } else {
                                        $name = $supplier->nickname;
                                    }
                                    if (empty($name)) {
                                        $name = $supplier->nickname_en;
                                    }
                                    echo h($name);
                                    ?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <?php
                                    echo __d('supplier', 'TXT_ADDRESS'); ?> :
                                </td>
                                <td>
                                    <?php
                                    echo $this->Address->format($supplier, LOCAL_EN); ?>
                                </td>
                                <td>
                                    <?php
                                    echo $this->Address->format($supplier); ?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <?php
                                    echo __d('supplier', 'TXT_TEL'); ?> :
                                </td>
                                <td>
                                    <?php
                                    if ($supplier->info_details) {
                                        $element = '<ul class="email-wrap">';
                                        foreach ($supplier->info_details as $item) {
                                            $element .= '<li>' . $item->tel . '</li>';
                                        }
                                        echo $element . '</ul>';
                                    }
                                    ?>
                                </td>
                                <td>
                                    <?php
                                    if ($supplier->info_details) {
                                        $element = '<ul class="email-wrap">';
                                        foreach ($supplier->info_details as $item) {
                                            $element .= '<li>' . $item->tel_extension . '</li>';
                                        }
                                        echo $element . '</ul>';
                                    }
                                    ?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <?php
                                    echo __d('supplier', 'TXT_FAX'); ?> :
                                </td>
                                <td colspan="2">
                                    <?php
                                    echo $supplier->fax; ?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <?php
                                    echo __d('supplier', 'TXT_EMAIL'); ?> :
                                </td>
                                <td colspan="2">
                                    <?php
                                    if ($supplier->info_details) {
                                        $element = '<ul class="email-wrap">';
                                        foreach ($supplier->info_details as $item) {
                                            $element .= '<li><a href="mailto:' . $item->email . '" target="_blank">' . $item->email . '</a></li>';
                                        }
                                        echo $element . '</ul>';
                                    }
                                    ?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <?php
                                    echo __d('supplier', 'TXT_URL'); ?> :
                                </td>
                                <td colspan="2">
                                    <?php
                                    echo $supplier->url; ?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <?php
                                    echo __d('supplier', 'TXT_SHOP_URL'); ?> :
                                </td>
                                <td colspan="2">
                                    <?php
                                    echo $supplier->online_shop_url; ?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <?php
                                    echo __d('supplier', 'TXT_PERSON_IN_CHARGE'); ?> :
                                </td>
                                <td colspan="2">
                                    <?php
                                    $TXT_PERSON_IN_CHARGE_NUMBER = '';
                                    if ($supplier->person_in_charges < 2) {
                                        $TXT_PERSON_IN_CHARGE_NUMBER = __d('supplier', 'TXT_PRODUCT_BRAND');
                                    } else {
                                        $TXT_PERSON_IN_CHARGE_NUMBER = __d('supplier', 'TXT_PRODUCT_BRAND');
                                    }
                                    echo $this->Html->link($supplier->person_in_charges.' '.$TXT_PERSON_IN_CHARGE_NUMBER, [
                                        'controller' => 'personInCharges',
                                        'action' => ($supplier->person_in_charges ? 'index' : 'add'),
                                        '?' => [
                                            'external_id' => $supplier['id'],
                                            'filter_by' => TYPE_SUPPLIER,
                                            'des' => TYPE_SUPPLIER,
                                        ]
                                    ], [
                                        'class' => 'btn btn-primary text-center btn-sm',
                                    ]); ?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <?php
                                    echo __d('supplier', 'TXT_REMARKS'); ?> :
                                </td>
                                <td colspan="2">
                                    <?php
                                    echo $this->Form->textarea('remark', [
                                        'readonly' => 'readonly',
                                        'style' => 'background: none;',
                                        'value' => $supplier->remark,
                                    ]);
                                    ?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <?php
                                    echo __d('supplier', 'TXT_MODIFIED'); ?> :
                                </td>
                                <td colspan="2">
                                    <?php
                                    echo date('Y-m-d', strtotime($supplier->modified)); ?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <?php
                                    echo __d('supplier', 'TXT_REGISTERED'); ?> :
                                </td>
                                <td colspan="2">
                                    <?php
                                    echo date('Y-m-d', strtotime($supplier->created)); ?>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="modal-footer" style="text-align: center !important;">
                    <?php
                    echo $this->Form->button(__d('supplier', 'TXT_CANCEL'), [
                        'type' => 'button',
                        'class' => 'btn btn-default btn-sm  btn-close-modal btn-width modal-close',
                        'data-dismiss' => 'modal',
                        'aria-label' => 'Close',
                    ]); ?>
                </div>
            </div>
        </div>
    </div>
</section>
