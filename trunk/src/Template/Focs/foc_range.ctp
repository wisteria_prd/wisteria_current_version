<div class="row">
    <div class="col-sm-12">
        <h2 class="text-center"><?= __('TXT_RANGE') ?></h2>
        <p class="text-center">
            <?php
            $for = ($foc_for == 'PO') ? strtoupper($foc_for) : ucwords($foc_for);
            echo __('TXT_FOC_OF') . ' ' . $seller_name . ' ' . __('TXT_FOR') . ' ' . $for;
            ?>
        </p>
    </div>
</div>
<div class="row row-top-space">
    <div class="col-sm-12">
        <?php
        echo $this->Form->create(null, [
            'role' => 'form',
            'class' => 'form-horizontal form-foc-range',
            'name' => 'common_form'
        ]);

        $this->Form->templates([
            'inputContainer' => '{{content}}'
        ]);
        echo $this->Form->hidden('foc_id', ['class' => 'foc-id', 'value' => $foc_id]);
        echo $this->Form->hidden('seller_name', ['class' => 'tmp-seller-name', 'value' => $seller_name]);
        echo $this->Form->hidden('foc_for', ['class' => 'tmp-foc-for', 'value' => $foc_for]);
        echo $this->Form->hidden('id', ['class' => 'foc-range-id', 'value' => '']);
        ?>
        <div class="form-group">
            <label class="col-sm-2 control-label"><?= __('STR_FROM') ?><?= $this->Comment->formAsterisk() ?></label>
            <div class="col-sm-3 field-small-padding-right">
                <?php echo $this->Form->input('range_from', [
                    'class' => 'form-control',
                    'label' => false,
                    'required' => false,
                    'id' => 'range-from'
                ]); ?>
                <span class="help-block help-tips" id="error-range_from"></span>
            </div>
            <label class="col-sm-2 control-label field-small-padding-right"><?= __('STR_TO') ?><?= $this->Comment->formAsterisk() ?></label>
            <div class="col-sm-3 field-small-padding-right field-small-padding-left">
                <?php echo $this->Form->input('range_to', [
                    'class' => 'form-control',
                    'label' => false,
                    'required' => false,
                    'id' => 'range-to'
                ]); ?>
                <span class="help-block help-tips" id="error-range_to"></span>
            </div>
            <div class="col-sm-2 field-small-padding-left">
                <button type="button" class="btn btn-sm btn-width btn-primary btn-foc-range"><?= __('STR_ADD') ?></button>
            </div>
        </div>
        <?php echo $this->Form->end() ?>
    </div>
</div>
<div class="row row-top-space">
    <div class="col-sm-12">
        <div class="detail-list">
            <table class="table table-condensed table-striped reindex table-foc-range">
                <thead>
                    <tr>
                        <th width="10%">#</th>
                        <th width="25%"><?= __('STR_FROM') ?></th>
                        <th width="25%"><?= __('STR_TO') ?></th>
                        <th width="25%"><?= __('TXT_DISCOUNT_ITEM') ?></th>
                        <th>&nbsp;</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        if ($range) {
                            $i = 1;
                            foreach ($range as $r) {
                                echo '<tr class="foc-range'.$r->id.'">';
                                    echo '<th>' . $i . '</th>';
                                    echo '<td class="r-from">' . $r->range_from . '</td>';
                                    echo '<td class="r-to">' . $r->range_to . '</td>';
                                    echo '<td>';
                                        echo $this->Form->button(count($r->foc_discounts), [
                                            'type' => 'button',
                                            'class' => 'btn btn-primary btn-sm btn-add-discount-range r-' . $r->id,
                                            'data-id' => $r->id
                                        ]);
                                    echo '</td>';
                                    echo '<td>';
                                    echo $this->Form->button(BTN_ICON_EDIT, [
                                            'type' => 'button',
                                            'class' => 'btn btn-primary btn-sm btn-edit-range',
                                            'escape' => false,
                                            'data-id' => $r->id
                                        ]);
                                    echo '&nbsp;&nbsp;';
                                    echo $this->Form->button(BTN_ICON_DELETE, [
                                        'type' => 'button',
                                        'class' => 'btn btn-delete btn-sm btn-delete-foc-range',
                                        'id' => false,
                                        'escape' => false,
                                        'data-id' => $r->id
                                    ]);
                                    echo '</td>';
                                echo '</tr>';
                                $i++;
                            }
                        }
                    ?>
                </tbody>
            </table>
        </div>
    </div>
</div>