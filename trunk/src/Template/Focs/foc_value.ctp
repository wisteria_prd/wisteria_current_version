<?php
if ($data) {
    $output = '<option value="">' . __('TXT_SELECT_VALUE') . '</option>';
    if ($foc_for == TYPE_BRAND) {
        foreach ($data as $key => $value) {
            $name = $this->Comment->nameEnOrJp($value->name, $value->name_en, $en);
            $output .= '<option value="' . $value->id . '">' . $name . '</option>';
        }
    } else if ($foc_for == TYPE_PRODUCT) {
        foreach ($data as $sl_product) {
            if ($sl_product->seller_products) {
                foreach ($sl_product->seller_products as $product) {
                    $p_name = $this->Comment->brandAndProducts($product, $en);
                    if ($product->product->product_details) {
                        foreach ($product->product->product_details as $pd) {
                            $p_name .= $this->Comment->sizeAndUnit($pd, $en);
                        }
                    }

                    $output .= '<option value="' . $product->product->id . '">' . $p_name . '</option>';
                }
            }
        }
    }
    echo $output;
}