
<div class="search-section" style="margin-bottom: 20px;">
    <?php
    echo $this->Form->create('Focs', [
        'role' => 'form',
        'class' => 'form-inline form-search',
        'type' => 'get',
        'name' => 'common_form'
    ]);
    echo $this->Form->hidden('displays', [
       'value' => empty($this->request->query('displays')) ? 10 : $this->request->query('displays')
    ]); ?>
    <div class="form-group">
        <?php
        echo $this->Form->text('keyword', [
            'placeholder' => __d('foc', 'TXT_ENTER_KEYWORD'),
            'class' => 'form-control',
            'autocomplete' => 'off',
            'default' => $this->request->query('keyword'),
        ]); ?>
    </div>
    <div class="form-group custom-select">
        <select class="form-control" name="seller_id" id="select-seller">
            <?php
            $selected = null;
            if ($this->request->query('seller_id')) {
                $selected = $this->request->query('seller_id');
            }
            echo $this->Comment->supplierDropdown($sellers, $locale, $selected);
            ?>
        </select>
    </div>
    <?php
    $this->SwitchStatus->render();
    echo $this->Form->end();
    ?>
</div>
<div class="total-pagination" style="padding-bottom: 20px;">
    <?php
    echo $this->element('display_info'); ?>
</div>
<div class="row search-section" style="margin-bottom: 20px; margin-top: 20px;">
    <?php
    echo $this->element('display_number'); ?>
    <?php
    echo $this->ActionButtons->btnRegisterNew('Focs', []) ?>
    <div class="clearfix"></div>
</div>
<div class="data-tb-list">
    <?php if ($data): ?>
    <table class="table table-striped">
        <thead>
            <tr>
                <th>#</th>
                <th>&nbsp;</th>
                <th class="white-space">
                    <?php
                    echo __d('foc', 'TXT_SUPPLIER') ?>
                </th>
                <th class="white-space">
                    <?php
                    echo __d('foc', 'TXT_MANUFACTURER') ?>
                </th>
                <th class="white-space">
                    <?php
                    echo __d('foc', 'TXT_PRODUCT') ?>
                </th>
                <th class="white-space">
                    <?php
                    echo __d('foc', 'TXT_TYPE') ?>
                </th>
                <th class="white-space">
                    <?php
                    echo __d('foc', 'TXT_FROM') ?>
                </th>
                <th class="white-space">
                    <?php
                    echo __d('foc', 'TXT_TO') ?>
                </th>
                <th class="white-space">
                    <?php
                    echo __d('foc', 'TXT_FOC') ?>
                </th>
                <th width="1%">&nbsp;</th>
                <th colspan="3">&nbsp;</th>
            </tr>
        </thead>
        <tbody>
            <?php
            $numbering = $this->Paginator->counter('{{start}}');
            $name = 'name' . $locale;
            foreach ($data as $item): //pr($item);
            ?>
            <tr data-id="<?= h($item->id); ?>">
                <th scope="row">
                    <?php
                    echo $numbering; $numbering++; ?>
                </th>
                <td>
                    <?php
                    echo $this->ActionButtons->disabledText($item->is_suspend) ?>
                </td>
                <td>
                    <?php
                    if ($item->seller) {
                        if ($item->seller->supplier) {
                            echo $this->Comment->getFieldByLocal($item->seller->supplier, $locale);
                        }
                        if ($item->seller->manufacturer) {
                            echo $this->Comment->getFieldByLocal($item->seller->manufacturer, $locale);
                        }
                    }
                    ?>
                </td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>
                    <?php
                    echo __($this->Comment->textHumanize($item->foc_for)); ?>
                </td>
                <td>
                    <?php
                        $seller_name = '';
                        if ($item->seller) {
                            if ($item->seller->type == TYPE_SUPPLIER) {
                                echo h($item->seller->supplier->$name);
                                $seller_name = h($item->seller->supplier->$name);
                            } else if ($item->seller->type == TYPE_MANUFACTURER) {
                                echo h($item->seller->manufacturer->$name);
                                $seller_name = h($item->seller->manufacturer->$name);
                            }
                        }
                    ?>
                </td>
                <td>
                    <button type="button" class="btn btn-sm btn-info foc-range" data-for="<?= h($item->foc_for) ?>" data-seller="<?= $seller_name ?>" data-foc="<?= $item->id ?>">
                        <?php
                        echo count($item->foc_ranges) ?>
                    </button>
                </td>
                <td>
                    <?php
                    echo __(h(ucfirst($item->range_type))) ?>
                </td>
                <td>
                    <?php
                    echo $this->Form->button('<i class="fa fa-comments-o" aria-hidden="true"></i>', [
                        'class' => 'btn btn-primary btn-sm btn_comment',
                        'data-id' => $item->id,
                        'escape' => false,
                    ]);
                    ?>
                </td>
                <td width="1%" data-target="<?php echo ($item->is_suspend) ? 1 : 0; ?>">
                    <?php
                    if ($item->is_suspend == 1) {
                        echo $this->Form->button(BTN_ICON_UNDO, [
                            'class' => 'btn btn-recover btn-sm btn_suspend',
                            'data-name' => 'recover',
                            'escape' => false
                        ]);
                    } else {
                        echo $this->Html->link(BTN_ICON_EDIT, [
                            'controller' => 'focs',
                            'action' => 'edit', $item->id,
                            '?' => ['pkg_id' => $this->request->query('pkg_id')]
                            ], [
                            'class' => 'btn btn-primary btn-sm',
                            'escape' => false
                        ]);
                    }
                    ?>
                </td>
                <td width="1%" data-target="<?php echo ($item->is_suspend) ? 1 : 0; ?>">
                    <?php
                    if ($item->is_suspend == 1) {
                        echo $this->Form->button(BTN_ICON_DELETE, [
                            'class' => 'btn btn-delete btn-sm',
                            'id' => 'btn_delete',
                            'escape' => false
                        ]);
                    } else {
                        echo $this->Form->button(BTN_ICON_STOP, [
                            'class' => 'btn btn-suspend btn-sm btn_suspend',
                            'data-name' => 'suspend',
                            'escape' => false
                        ]);
                    }
                    ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <?php endif; ?>
</div>
<div class="row row-top-space">
    <?php echo $this->element('display_number'); ?>
    <?= $this->element('next_prev') ?>
    <div class="clearfix"></div>
</div>
<?php echo $this->element('Modal/view_detail'); ?>
<?php echo $this->element('Modal/detail_list'); ?>
<?php echo $this->element('Modal/delete'); ?>
<!--Modal Suspend-->
<?php echo $this->element('Modal/suspend'); ?>

<script>
    (function() {
        $('.btn_comment').click(function (e) {
            e.stopPropagation();
            let params = {
                id: $(this).closest('tr').attr('data-id'),
                type: '<?php echo MESSAGE_TYPE_FOC; ?>',
                referer: CONTROLLER,
            };
            let options = {
                url: BASE + 'messages/get-message-list',
                type: 'GET',
                data: params,
                beforeSend: function () {
                    $.LoadingOverlay('show');
                }
            };
            ajaxRequest(options, SESSION_TIMEOUT, function (data) {
                $('body').prepend(data);
                $('body').find('.modal-message').modal('show');
            });
        });

        function updateRangeList(data) {
            $.ajax({
                url: '<?= $this->Url->build(['controller' => 'Focs', 'action' => 'focRange']) ?>',
                type: 'GET',
                data: data,
                dataType: 'HTML',
                beforeSend: function() {
                    $.LoadingOverlay('show');
                },
                success: function(response) {
                    $.LoadingOverlay('hide');
                    $('#modal_detail .modal-body').empty().html(response);
                    $('#modal_detail').modal('show');
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    $.LoadingOverlay('hide');
                    if (errorThrown === 'Forbidden') {
                        if (confirm('<?= __('Session timeout. Please login again.') ?>')) {
                            location.reload();
                        }
                    }
                },
                complete: function () {
                    reIndexList();
                }
            });
        }

        $('body').on('click', '.foc-range', function () {
            var foc_id = $(this).attr('data-foc');
            var foc_for = $(this).attr('data-for');
            var seller_name = $(this).attr('data-seller');
            var data = {foc_id: foc_id, foc_for: foc_for, seller_name: seller_name};
            updateRangeList(data);
        });

        //modal confirm suspend
        $('body').on('click', '.btn_suspend', function (e) {
            e.stopPropagation();
            $('.suspend-item-id').val($(this).attr('data-id'));
            $('.is-suspend-item').val($(this).attr('data-target'));
            var confirm = '';
            if (parseInt($(this).attr('data-target')) === 1) {
                $('#btn_suspend_yes').text('<?= __('BTN_SUSPEND') ?>');
                confirm = '<?= __('MSG_CONFIRM_SUSPEND') ?>';
            } else {
                $('#btn_suspend_yes').text('<?= __('BTN_RECOVER') ?>');
                confirm = '<?= __('MSG_CONFIRM_UNDO_SUSPEND') ?>';
            }
            $('.confirm-suspend-text').text(confirm);
            $('#modal_suspend').modal('show');
        });

        //modal change suspend status
        $('body .data-tb-list .table > tbody > tr').on('click', '.btn_suspend', function (e) {
            e.stopPropagation();
            var content = $('#modal_suspend');
            var status = $(this).attr('data-name');
            var txt = '';

            switch (status) {
                case 'suspend':
                    $('#btn_suspend_yes').text('<?= __('BTN_SUSPEND') ?>');
                    txt = '<?= __('MSG_CONFIRM_SUSPEND') ?>';
                    break;
                case 'delete':
                    txt = '<?= __('MSG_CONFIRM_DELETE') ?>';
                    $('#btn_suspend_yes').text('<?= __('BTN_DELETE') ?>');
                    break;
                 case 'recover':
                    $('#btn_suspend_yes').text('<?= __('BTN_RECOVER') ?>');
                    txt = '<?= __('MSG_CONFIRM_UNDO_SUSPEND') ?>';
                    break;
            };

            $(content).find('#btn_suspend_yes').data('id', $(this).closest('tr').data('id'));
            $(content).find('#btn_suspend_yes').data('target', $(this).closest('td').data('target'));
            $(content).find('.modal-body p').html(txt);
            $(content).modal('show');
        });

        $('body').on('click', '#btn_suspend_yes', function (e) {
            var params = {
                id: $(this).data('id'),
                is_suspend: $(this).data('target')
            };
            $.post('<?php echo $this->Url->build('/focs/update-suspend/'); ?>', params, function (data) {
                if (data.message === 'success') {
                    location.reload();
                }
            }, 'json');
        });

        //modal delete confirm
        $('body').on('click', '#btn_delete', function (e) {
            e.stopPropagation();
            $('#btn_delete_yes').text('<?= __('BTN_DELETE') ?>');
            $('.delete-item').val($(this).attr('data-id'));
            $('#modal_delete').modal('show');
        });

        //modal delete item.
        $('body').on('click', '#btn_delete_yes', function() {
            var data = $('.form-delete').serialize();
            var url = '<?= $this->Url->build('/focs/delete') ?>';
            itemDelete(url, data, '<?php echo __('TXT_SESSION_TIMEOUT') ?>');
        });

        /**
        * FOC Range Section
         */

        $('body').on('click', '.btn-foc-range', function() {
            var data = $('.form-foc-range').serialize();
            var id = $('.foc-range-id').val();
            var url = '<?= $this->Url->build(['controller' => 'FocRanges', 'action' => 'create']) ?>';
            if (id !== '') {
                url = '<?= $this->Url->build(['controller' => 'FocRanges', 'action' => 'edit']) ?>';
            }
            $.ajax({
                url: url,
                type: 'POST',
                dataType: 'JSON',
                data: data,
                cache: false,
                beforeSend: function() {
                    $.LoadingOverlay('show');
                },
                success: function(response) {
                    $.LoadingOverlay('hide');
                    if (response.status === 1) {
                        var data = {
                            foc_id: $('.foc-id').val(),
                            foc_for: $('.tmp-foc-for').val(),
                            seller_name: $('.tmp-seller-name').val()
                        };
                        updateRangeList(data);
                    } else if (response.status === 0) {
                        $.each(response.message, function(key, elem) {
                            var validate = $('#error-' + key);
                            $.each(elem, function(k, v) {
                                validate.removeClass('help-tips')
                                    .addClass('error-tips')
                                    .text(v)
                                    .show();
                            });
                        });
                    }
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    $.LoadingOverlay('hide');
                    if (errorThrown === 'Forbidden') {
                        if (confirm('<?= __('Session timeout. Please login again.') ?>')) {
                            location.reload();
                        }
                    }
                }
            });
        });

        $('body').on('click', '.btn-edit-range', function() {
            var tr = $(this).parents('tr');
            $('#range-from').val(tr.find('.r-from').text().trim());
            $('#range-to').val(tr.find('.r-to').text().trim());
            var id = $(this).attr('data-id');
            $('.foc-range-id').val(id);
            $('.btn-foc-range').text('<?= __('TXT_UPDATE') ?>');
        });

        $('body').on('click', '.btn-delete-foc-range', function() {
            var id = $(this).attr('data-id');
            $('.delete-item').val(id);
            $('.btn-delete-item').attr('id', 'btn-delete-foc-range-yes');
            $('#modal_delete').modal('show');
        });

        $('body').on('click', '#btn-delete-foc-range-yes', function() {
            var data = $('.form-delete').serialize();
            var id = $('.delete-item').val();
            $.ajax({
                url: '<?= $this->Url->build(['controller' => 'FocRanges', 'action' => 'delete']) ?>',
                type: 'POST',
                dataType: 'JSON',
                data: data,
                cache: false,
                beforeSend: function() {
                    $.LoadingOverlay('show');
                },
                success: function(response) {
                    $.LoadingOverlay('hide');
                    if (response.status === 1) {
                        $('.foc-range' + id).remove();
                        reIndexList();
                    } else if (response.status === 0) {
                        alert(response.message);
                    }
                    $('.close-modal').click();
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    $('.close-modal').click();
                    $.LoadingOverlay('hide');
                    if (errorThrown === 'Forbidden') {
                        if (confirm('<?= __('Session timeout. Please login again.') ?>')) {
                            location.reload();
                        }
                    }
                }
            });
        });

        function reIndexList() {
            $('.reindex tbody tr').each(function (i, v) {
                $(this).find('th').text(i+1);
            });
        }

        $('body').on('click', '#modal_detail .btn-close-modal', function () {
            location.reload();
        });

        /**
        * FOC discount section
         */

        $('body').on('click', '.btn-add-discount-range', function () {
            var data = {
                external_id: $(this).attr('data-id'),
                type: '<?= TYPE_FOC_RANGE ?>'
            };
            get_update_foc(data, false);
        });

        function get_update_foc(data, update) {
            $.ajax({
                url: '<?= $this->Url->build(['controller' => 'FocDiscounts', 'action' => 'discountList']) ?>',
                type: 'GET',
                data: data,
                dataType: 'HTML',
                beforeSend: function() {
                    $.LoadingOverlay('show');
                },
                success: function(response) {
                    $.LoadingOverlay('hide');
                    $('#detail-list .modal-body').empty().html(response);
                    if (update === false) {
                        $('#detail-list').modal('show');
                    }
                    $('#product').select2({
                        width: '100%'
                    });
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    $.LoadingOverlay('hide');
                    if (errorThrown === 'Forbidden') {
                        if (confirm('<?= __('Session timeout. Please login again.') ?>')) {
                            location.reload();
                        }
                    }
                }
            });
        }

        $('body').on('click', '.btn-foc-discount', function() {
            $('.help-block').empty().hide();
            var data = $('.form-foc').serialize();
            var id = $('.foc-discount-id').val();
            var url = '<?= $this->Url->build(['controller' => 'FocDiscounts', 'action' => 'create']) ?>';
            if (id !== '') {
                url = '<?= $this->Url->build(['controller' => 'FocDiscounts', 'action' => 'edit']) ?>';
            }
            $.ajax({
                url: url,
                type: 'POST',
                dataType: 'JSON',
                data: data,
                cache: false,
                beforeSend: function() {
                    $.LoadingOverlay('show');
                },
                success: function(response) {
                    $.LoadingOverlay('hide');
                    if (response.status === 1) {
                        var type = $('.foc-type').val();
                        var external_id = $('.foc-external-id').val();
                        var data = {
                            type: type,
                            external_id: external_id
                        };

                        get_update_foc(data, true);
                    } else if (response.status === 0) {
                        $.each(response.message, function(key, elem) {
                            var validate = $('#error-' + key);
                            $.each(elem, function(k, v) {
                                validate.removeClass('help-tips')
                                    .addClass('error-tips')
                                    .text(v)
                                    .show();
                            });
                        });
                    }
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    $.LoadingOverlay('hide');
                    if (errorThrown === 'Forbidden') {
                        if (confirm('<?= __('Session timeout. Please login again.') ?>')) {
                            location.reload();
                        }
                    }
                }
            });
        });

        $('body').on('click', '.btn-edit', function() {
            var tr = $(this).parents('tr');
            var id = $(this).attr('data-id');
            var detail_id = $(this).attr('data-detail-id');
            $('#value').val(tr.find('.discount-value').text());
            $('#product').val(detail_id).change();
            $('.foc-discount-id').val(id);
            $('.btn-foc-discount').text('<?= __('TXT_UPDATE') ?>');
        });

        $('body').on('click', '.btn-delete-discount', function() {
            var id = $(this).attr('data-id');
            $('.delete-item').val(id);
            $('.btn-delete-item').attr('id', 'btn-delete-foc-yes');
            $('#modal_delete').modal('show');
        });

        $('body').on('click', '#btn-delete-foc-yes', function() {
            var data = $('.form-delete').serialize();
            var id = $('.delete-item').val();
            $.ajax({
                url: '<?= $this->Url->build(['controller' => 'FocDiscounts', 'action' => 'delete']) ?>',
                type: 'POST',
                dataType: 'JSON',
                data: data,
                cache: false,
                beforeSend: function() {
                    $.LoadingOverlay('show');
                },
                success: function(response) {
                    $.LoadingOverlay('hide');
                    if (response.status === 1) {
                        $('.foc-' + id).remove();
                    } else if (response.status === 0) {
                        alert(response.message);
                    }
                    $('.close-modal').click();
                    reIndexList();
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    $('.close-modal').click();
                    $.LoadingOverlay('hide');
                    if (errorThrown === 'Forbidden') {
                        if (confirm('<?= __('Session timeout. Please login again.') ?>')) {
                            location.reload();
                        }
                    }
                }
            });
        });

        $('body').on('click', '.btn-custom-close', function () {
            var external_id = $('.foc-external-id').val();
            var discount_number = $('.table-foc-discount tbody tr').length;
            $('.r-' + external_id).text(discount_number);
        });

        $('body').on('change', '#select-seller', function () {
            $('.form-search').submit();
        });

        pageOutOfRange(<?= $paging ?>, '<?= $this->request->query('page') ?>');
        var maxPage = parseInt($('#maxPage').val());
        updatePagination(maxPage);
    })();
</script>
