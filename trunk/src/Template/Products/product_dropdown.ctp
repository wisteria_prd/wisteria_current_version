<?php
if ($products):
    $name = 'name' . $en;
    $output = '<option value="">' . __('TXT_SELECT_PRODUCT_NAME') . '</option>';
    foreach($products as $key => $product) {
        $output .= '<option value="' . $key . '">' . $product . '</option>';
    }
    echo $output;
endif;