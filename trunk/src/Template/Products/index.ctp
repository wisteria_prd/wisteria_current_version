<?php
$params = [
    'destination' => $this->request->query('des') ? $this->request->query('des') : '',
    'brand' => $this->request->query('filter_brand') ? $this->request->query('filter_brand') : '',
    'manufacturer' => $this->request->query('filter_manufacturer') ? $this->request->query('filter_manufacturer') : '',
];
$parameters = [];
if (!empty($params['destination'])) {
    $parameters = [
        'des' => $params['destination'],
        'filter_brand' => $params['brand'],
        'filter_manufacturer' => $params['manufacturer']
    ];
} ?>
<div class="search-section" style="margin-bottom: 20px;">
    <?php
    echo $this->Form->create('Products', [
        'class' => 'form-inline form-search',
        'type' => 'get',
        'name' => 'form_search',
    ]);
    echo $this->Form->hidden('displays', [
        'value' => $displays
    ]); ?>
    <div class="form-group">
        <?php
        echo $this->Form->text('keyword', [
            'placeholder' => __d('product', 'TXT_ENTER_KEYWORDS'),
            'class' => 'form-control',
            'value' => $this->request->query('keyword'),
            'autocomplete' => 'off',
        ]); ?>
    </div>
    <div class="form-group custom-select">
        <?php
        $manufacturerOption = [
            '' => __('PRODUCT_TXT_SELECT_MANUFACTURER'),
        ];
        if ($manufacturer) {
            $manufacturerOption = [
                $manufacturer->id => $this->Comment->getFieldByLocal($manufacturer, $locale),
            ];
        }
        echo $this->Form->select('filter_manufacturer', [], [
            'class' => 'form-control',
            'id' => 'filter-manufacturer',
            'empty' => $manufacturerOption,
        ]); ?>
    </div>
    <div class="form-group custom-select">
        <?php
        $productBrandOption = [
            '' => __('PRODUCT_TXT_SELECT_PRODUCT_BRAND'),
        ];
        if ($productBrand) {
            $productBrandOption = [
                $productBrand->id => $this->Comment->getFieldByLocal($productBrand, $locale),
            ];
        }
        echo $this->Form->select('filter_brand', [], [
            'class' => 'form-control',
            'id' => 'filter-brand',
            'value' => $this->request->query('filter_brand'),
            'empty' => $productBrandOption,
        ]); ?>
    </div>
    <?php
    $this->SwitchStatus->render();
    echo $this->Form->end(); ?>
</div>
<div class="total-pagination" style="padding-bottom: 20px;">
    <?php
    echo $this->element('display_info'); ?>
</div>
<div class="row search-section" style="margin-bottom: 20px;">
    <?php
    echo $this->element('display_number'); ?>
    <?php if (false) : ?>
    <div class="col-md-6 col-sm-6 text-center">
        <h2 class="title-page">
            <?php
            if ($params['destination'] === TYPE_MANUFACTURER) {
                echo __('TXT_MANUFACTURERS');
            } elseif ($params['destination'] === TYPE_PRODUCT_BRAND) {
                echo __('STR_PRODUCT_BRAND');
            } elseif ($params['destination'] === TYPE_SUPPLIER) {
                echo __('TXT_SUPPLIERS');
            }
            ?>
        </h2>
    </div>
    <?php endif; ?>
    <?= $this->ActionButtons->btnRegisterNew('Products', $parameters) ?>
    <div class="clearfix"></div>
</div>
<div class="data-tb-list">
    <?php if ($data): ?>
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>#</th>
                    <th>&nbsp;</th>
                    <th class="white-space">
                        <?php
                        echo $this->Paginator->sort('Products.code', __d('product', 'TXT_OLD_CODE')); ?>
                        <i class="fa fa-sort-desc" aria-hidden="true"></i>
                    </th>
                    <th class="white-space">
                        <?php
                        echo $this->Paginator->sort('Manufacturers.name', __d('product', 'TXT_MANUFACTURER')); ?>
                        <i class="fa fa-sort-desc" aria-hidden="true"></i>
                    </th>
                    <th class="white-space">
                        <?php
                        echo $this->Paginator->sort('ProductDetails.type', __d('product', 'TXT_PRODUCT_CATEGORY')); ?>
                        <i class="fa fa-sort-desc" aria-hidden="true"></i>
                    </th>
                    <th class="white-space">
                        <?php
                        echo $this->Paginator->sort('ProductBrands.name', __d('product', 'TXT_PRODUCT_BRAND')); ?>
                        <i class="fa fa-sort-desc" aria-hidden="true"></i>
                    </th>
                    <th class="white-space">
                        <?php
                        echo $this->Paginator->sort('Products.name', __d('product', 'TXT_PRODUCT')); ?>
                        <i class="fa fa-sort-desc" aria-hidden="true"></i>
                    </th>
                    <th class="white-space">
                        <?php
                        echo $this->Paginator->sort('Products.purchase_flag', __('PRODUCT_TXT_PURCHASE_AVAILABILITY')); ?>
                        <i class="fa fa-sort-desc" aria-hidden="true"></i>
                    </th>
                    <th class="white-space">
                        <?php echo $this->Paginator->sort('Products.sell_flag', __('PRODUCT_TXT_SALE_AVAILABILITY')); ?>
                        <i class="fa fa-sort-desc" aria-hidden="true"></i>
                    </th>
                    <th>&nbsp;</th>
                    <th width="1%">&nbsp;</th>
                    <th width="1%">&nbsp;</th>
                    <th width="1%">&nbsp;</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $numbering = $this->Paginator->counter('{{start}}');
                foreach ($data as $item): ?>
                    <tr data-id="<?php echo h($item->id); ?>">
                        <th scope="row" class="td_evt">
                            <?php
                            echo $numbering; $numbering++; ?>
                        </th>
                        <td>
                            <?php
                            echo $this->ActionButtons->disabledText($item->is_suspend); ?>
                        </td>
                        <td>
                            <?php
                            echo h($item->code); ?>
                        </td>
                        <td>
                            <?php
                            echo $this->Comment->getFieldByLocal($item->product_brand->manufacturer, $locale) ?>
                        </td>
                        <td>
                            <?php
                            if ($item->product_details) {
                                echo h($item->product_details[0]->type);
                            }
                            ?>
                        </td>
                        <td>
                            <?php
                            $brand = $item->product_brand;
                            $name = 'name' . $locale;
                            echo h($brand[$name]); ?>
                        </td>
                        <td>
                            <?php
                            echo $this->Comment->nameEnOrJp($item->name, $item->name_en, $locale); ?>
                        </td>
                        <td>
                            <?php
                            echo $item->is_purchase_flag; ?>
                        </td>
                        <td>
                            <?php
                            echo $item->is_sell_flag; ?>
                        </td>
                        <td class="td-btn-wrap">
                            <?php
                            echo $this->Html->link(__('PRODUCT_TXT_PRODUCT_SPECIFICATION'), [
                                'controller' => 'product-details',
                                'action' => 'index',
                                '?' => [
                                    'product_id' => $item->id,
                                    'des' => 'products',
                                ]
                            ], [
                                'class' => 'btn btn-primary btn-sm btn_prd_detail'
                            ]); ?>
                        </td>
                        <td class="td-btn-wrap">
                            <?php
                            echo $this->Form->button('<i class="fa fa-comments-o" aria-hidden="true"></i>', [
                                'class' => 'btn btn-primary btn-sm btn_comment',
                                'data-id' => $item->id,
                                'escape' => false,
                            ]); ?>
                        </td>
                        <td data-target="<?php echo ($item->is_suspend) ? 1 : 0; ?>">
                            <?php
                            if ($item->is_suspend == 1) {
                                echo $this->Form->button(BTN_ICON_UNDO, [
                                    'class' => 'btn btn-recover btn-sm btn_suspend',
                                    'data-name' => 'recover',
                                    'escape' => false
                                ]);
                            } else {
                                $edit_url = [
                                'controller' => 'products',
                                'action' => 'edit', $item->id,
                                ];
                                if ($params['brand'] !== null && $params['manufacturer'] !== null) {
                                    $edit_url = [
                                    'controller' => 'products',
                                    'action' => 'edit', $item->id,
                                    '?' => [
                                        'des' => $params['destination'],
                                        'filter_brand' => $params['brand'],
                                        'filter_manufacturer' => $params['manufacturer'],
                                    ]
                                    ];
                                }
                                echo $this->Html->link(BTN_ICON_EDIT, $edit_url, [
                                    'class' => 'btn btn-primary btn-sm',
                                    'escape' => false
                                ]);
                            }
                            ?>
                        </td>
                        <td data-target="<?php echo ($item->is_suspend) ? 1 : 0; ?>">
                            <?php
                            if ($item->is_suspend == 1) {
                                if (!empty($item->count_product)) {
                                    echo $this->Form->button(BTN_ICON_DELETE, [
                                        'class' => 'btn btn-delete btn-sm',
                                        'id' => 'btn_delete',
                                        'disabled' => 'disabled',
                                        'escape' => false
                                    ]);
                                } else {
                                    echo $this->Form->button(BTN_ICON_DELETE, [
                                        'class' => 'btn btn-delete btn-sm',
                                        'id' => 'btn_delete',
                                        'escape' => false
                                    ]);
                                }
                            } else {
                                echo $this->Form->button(BTN_ICON_STOP, [
                                    'class' => 'btn btn-suspend btn-sm btn_suspend',
                                    'data-name' => 'suspend',
                                    'escape' => false
                                ]);
                            }
                            ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    <?php endif; ?>
</div>
<div class="row row-top-space" style="padding-bottom: 20px;">
    <?php echo $this->element('display_number'); ?>
    <?= $this->element('next_prev') ?>
    <div class="clearfix"></div>
</div>

<!--Modal Detail-->
<?php echo $this->element('Modal/view_detail'); ?>
<!--Modal Comment-->
<?php echo $this->element('/Modal/message') ?>
<!--Modal Suspend-->
<?php echo $this->element('Modal/suspend'); ?>
<!--Modal delete-->
<?php echo $this->element('Modal/delete'); ?>

<script>
    $(function(e) {
        $('#filter-manufacturer').select2({
            width: '100%',
            minLength: 0,
            ajax: {
                url: BASE + 'Manufacturers/getListOfManufacturersByAjax',
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    let query = {
                        keyword: params.term,
                    };
                    return query;
                },
                processResults: function (data) {
                    let dataSource = [];
                    if (data.data === null && data.data === 'undefined') {
                        return false;
                    }
                    $.each(data.data, function(i, v) {
                        dataSource.push({
                            id: v.id,
                            text: getNameByLocal(LOCALE, v)
                        });
                    });
                    return { results: dataSource };
                }
            }
        });

        $('#filter-brand').select2({
            width: '100%',
            minLength: 0,
            ajax: {
                url: BASE + 'ProductBrands/getListOfProductBrandsByAjax',
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    var query = {
                        keyword: params.term,
                        manufacturer_id: $('#filter-manufacturer').val()
                    };
                    return query;
                },
                processResults: function (data) {
                    let dataSource = [];
                    if (data.data === null && data.data === 'undefined') {
                        return false;
                    }
                    $.each(data.data, function(i, v) {
                        dataSource.push({
                            id: v.id,
                            text: getNameByLocal(LOCALE, v)
                        });
                    });
                    return { results: dataSource };
                }
            }
        });

        $('body').find('#keyword').typeahead({
            minLength: 0,
            autoSelect: true,
            source:  function (query, process) {
                return $.get('<?= $this->Url->build(['action' => 'getProductNameBySearch']) ?>', {
                        keyword: query
                    }, function (data) {
                        var optionData = [];
                        if (data.data !== null && data.data !== 'undefined') {
                            $.each(data.data, function(i, v) {
                                if (v.name !== '') {
                                    optionData.push(v.name);
                                }
                                if (v.name_en !== '') {
                                    optionData.push(v.name_en);
                                }
                            });
                        }
                        return process(optionData);
                    }, 'json');
            }
        });

        $('body').on('click', '.typeahead li', function(e) {
            $('body').find('.form-search').submit();
        });

        $('body').on('change', '.display_data', function (e) {
            window.location.href = '<?php echo $this->Url->build('/products/index'); ?>' + '?displays=' + $(this).val();
        });

        $('body').on('click', '.data-tb-list .table>tbody>tr', function (e) {
            $.LoadingOverlay('show');
            var id = $(this).closest('tr').data('id');

            $.get('<?php echo $this->Url->build('/products/view/'); ?>' + id, function (data) {
                $.LoadingOverlay('hide');
                $('#modal_detail').find('.modal-body').html(data);
                $('#modal_detail').modal('show');
                var file1 = $('#modal_detail').find('.media-brand').val();
                var file2 = $('#modal_detail').find('.media-product').val();
                var media_brand = [];
                var media_product = [];
                var content = $('body').find('#modal_detail .modal-body .fieldset-border');

                if (file1) {
                    media_brand = file1.split(',');
                    display_media(content, media_brand, 1);
                }
                if (file2) {
                    media_product = file2.split(',');
                    display_media(content, media_product, 0);
                }
            }, 'html');
        });

        $('.btn_comment').click(function (e) {
            e.stopPropagation();
            let params = {
                id: $(this).closest('tr').attr('data-id'),
                type: '<?php echo TYPE_PRODUCT ?>',
                referer: '<?php echo $this->request->controller; ?>'
            };
            let options = {
                url: BASE + 'Messages/getMessageList',
                type: 'GET',
                data: params,
                beforeSend: function () {
                    $.LoadingOverlay('show');
                }
            };
            ajaxRequest(options, SESSION_TIMEOUT, function (data) {
                $('body').prepend(data);
                $('body').find('.modal-message').modal('show');
            });
        });

        //filter message list search to specific
        $('body').on('click', '.btn-to-individual', function() {
            $(this).removeClass('btn-detault').addClass('btn-primary');
            $('body').find('.btn-to-all').removeClass('btn-primary').addClass('btn-default');
            var toAll = $('.to-individual').val();
            if (parseInt(toAll) === 0) {
                $('.to-all').val(0);
                $('.to-individual').val(1);
            } else {
                $('.to-all').val(1);
                $('.to-individual').val(0);
            }
            messagesSearch(listUrlSearch);
        });

        $('body').on('click', '.btn-search', function() {
            messagesSearch(listUrlSearch);
        });

        $('body').on('click', '.btn-suspend-msg', function() {
            var id = $(this).data('id');
            var data = $('#form-message-status-' + id).serialize();
            var suspendUrl = '<?= $this->Url->build(['controller' => 'Messages', 'action' => 'suspend']) ?>';
            suspendMessage(id, suspendUrl, data);
        });

        $('body').on('click', '.btn_suspend', function (e) {
            e.stopPropagation();
            var content = $('#modal_suspend');
            var txt = '';
            var txt_modal_suspend_btn = '';
            if ($(this).data('name') === 'recover') {
                txt = '<?= __('MSG_CONFIRM_UNDO_SUSPEND') ?>';
                txt_modal_suspend_btn = '<?php echo __('BTN_RECOVER'); ?>';
            } else {
                txt = '<?= __('MSG_CONFIRM_SUSPEND') ?>';
                txt_modal_suspend_btn = '<?php echo __('BTN_SUSPEND'); ?>';
            }

            $(content).find('#btn_suspend_yes').data('id', $(this).closest('tr').data('id'));
            $(content).find('#btn_suspend_yes').data('target', $(this).closest('td').data('target'));
            $(content).find('.modal-body p').html(txt);
            $('.modal-content #btn_suspend_yes').text(txt_modal_suspend_btn);
            $(content).modal('show');
        });

        $('body').on('click', '#btn_suspend_yes', function (e) {
            var params = {
                id: $(this).data('id'),
                is_suspend: $(this).data('target')
            };
            $.post('<?php echo $this->Url->build('/products/update-suspend/'); ?>', params, function (data) {
                if (data.message === 'success') {
                    location.reload();
                }
            }, 'json');
        });

        $('body').on('click', '#btn_delete', function (e) {
            e.stopPropagation();
            var content = $('#modal_delete');
            $(content).find('#btn_delete_yes').data('id', $(this).closest('tr').data('id'));
            $(content).modal('show');
        });

        $('body').on('click', '#btn_delete_yes', function (e) {
            $.post('<?php echo $this->Url->build('/products/delete/'); ?>', {id: $(this).data('id')}, function (data) {
                if (data.message === 'success') {
                    location.reload();
                }
            }, 'json');
        });

        $('body').on('change', '#filter-manufacturer, #filter-brand', function(e) {
            $('form[name="form_search"]').submit();
        });

        $('body').on('click', '.tt-selectable', function (e) {
            $('form[name="form_search"]').submit();
        });

        $('body').on('click', '.table>tbody>tr>td a, .table>tbody>tr>td button', function(e) {
            e.stopPropagation();
        });

        pageOutOfRange(<?= $paging ?>, '<?= $this->request->query('page') ?>');
        var maxPage = parseInt($('#maxPage').val());
        updatePagination(maxPage);

        // display media list
        function display_media(content, list, index)
        {
            if (list != null && list != undefined) {
                var element = '<div class="alert alert-warning" role="alert"><?= __('TXT_DEOCUMENTS_OF_BRAND'); ?></div>';
                if (index == 0) {
                    element = '<div class="alert alert-warning" role="alert"><?= __('TXT_DEOCUMENTS_OF_PRODUCT'); ?></div>';
                }
                $.each(list, function(i, v) {
                    var path = BASE_URL + 'img/uploads/' + v;
                    var extension = v.split('.').pop().toLowerCase();
                    element += '<div class="col-md-3 col-lg-3 col-sm-3 media-list-wrap">' + dispay_html(v, path, index, extension, i) + '</div>';
                });
                if (index == 1) {
                    $(content).find('.brand').append(element);
                } else {
                    $(content).find('.product').append(element);
                }
            }
        }

        function dispay_html(data, path, index, extension, i)
        {
            var canvas = '';
            var img = '';

            if (extension === 'pdf') {
               canvas = '<canvas id="the-canvas-' + i + '"></canvas>';
            } else {
                img = '<img src="' + path + '" class="open-main-image">';
            }
            var element = '<section class="thumbnail select_img">'
                        + '<a href="' + path + '" target="_blank">' + img + canvas + '</a></section>';

            if (extension === 'pdf') {
               read_pdf_file(path, i);
            }

            return element;
        }

        function read_pdf_file(path, id)
        {
            var full_path = location.protocol + '//' + location.host + path;

            PDFJS.getDocument(full_path).promise.then(function (pdf) {
                pdf.getPage(1).then(function getPageHelloWorld(page) {
                    var scale = 1.5;
                    var viewport = page.getViewport(scale);
                    var canvas = $('body').find('#the-canvas-' + id)[0];
                    var context = canvas.getContext('2d');
                    canvas.height = viewport.height;
                    canvas.width = viewport.width;
                    page.render({canvasContext: context, viewport: viewport})
                });
            });
        }
    })
</script>
<?php
echo $this->Html->script([
    'processing-api.min',
    'pdf',
], ['block' => 'script']);
