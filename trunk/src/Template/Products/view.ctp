
<table class="table table-striped tb-prbr-view" id="tb-detail">
    <tbody>
        <tr>
            <td>
                <?php
                echo __d('product', 'TXT_OLD_CODE') ?> :
            </td>
            <td colspan="2">
                <?php
                echo h($data->code) ?>
            </td>
        </tr>
        <tr>
            <td>
                <?php
                echo __d('product', 'TXT_MANUFACTURER') ?> :
            </td>
            <td>
                <?php
                echo $data->product_brand->manufacturer->name; ?>
            </td>
            <td>
                <?php
                echo $data->product_brand->manufacturer->name_en; ?>
            </td>
        </tr>
        <tr>
            <td>
                <?php
                echo __d('product', 'TXT_PRODUCT_CATEGORY') ?> :
            </td>
            <td colspan="2">
                <?php
                if ($data->product_details) {
                    echo $data->product_details[0]->type;
                }
                ?>
            </td>
        </tr>
        <tr>
            <td>
                <?php
                echo __d('product', 'TXT_PRODUCT') ?> :
            </td>
            <td colspan="2">
                <?php
                $product = $this->Comment->getFieldByLocal($data->product_brand, $locale);
                $product .= ' ' . $this->Comment->getFieldByLocal($data, $locale);
                echo h($product);
                ?>
            </td>
        </tr>
        <tr>
            <td>
                <?php
                echo __d('product', 'TXT_PURCHASE_AVAILABILITY') ?> :
            </td>
            <td colspan="2">
                <?php
                echo ($data->is_purchase) ? __d('product', 'TXT_YES') : __d('product', 'TXT_NO'); ?>
            </td>
        </tr>
        <tr>
            <td>
                <?php
                echo __d('product', 'TXT_SALE_AVAILABILITY') ?> :
            </td>
            <td colspan="2">
                <?php
                echo ($data->is_sale) ? __d('product', 'TXT_YES') : __d('product', 'TXT_NO'); ?>
            </td>
        </tr>
        <tr>
            <td>
                <?php
                echo __d('product', 'TXT_URL') ?> :
            </td>
            <td colspan="2">
                <?php
                echo $data->url; ?>
            </td>
        </tr>
        <tr>
            <td>
                <?php
                echo __d('product', 'TXT_REMARK') ?> :
            </td>
            <td colspan="2">
                <textarea readonly="readonly" style="background: none;"><?php echo $data->remarks; ?></textarea>
            </td>
        </tr>
        <tr>
            <td>
                <?php
                echo __d('product', 'TXT_MODIFIED') ?> :
            </td>
            <td colspan="2">
                <?php
                echo date('Y-m-d', strtotime($data->modified)) ?>
            </td>
        </tr>
        <tr>
            <td>
                <?php
                echo __d('product', 'TXT_REGISTERED') ?> :
            </td>
            <td colspan="2">
                <?php
                echo date('Y-m-d', strtotime($data->created)) ?>
            </td>
        </tr>
    </tbody>
</table>
<section class="certificate-list">
    <fieldset class="fieldset-border">
        <legend>
            <?php
            echo __d('product', 'TXT_DOCUMENT') ?>
        </legend>
        <div class="fieldset-content-wrap">
            <div class="brand"></div>
            <div class="clearfix"></div>
            <div class="product"></div>
        </div>
    </fieldset>
</section>
<?php
if (isset($data->medias)) {
    $media_list = [];
    foreach ($data->medias as $file) {
        $media_list[] = $file->file_name;
    }
    $file1 = implode(',', $media_list);
    echo $this->Form->hidden('media-product', [
        'class' => 'media-product',
        'value' => $file1,
    ]);
}
if (isset($data->product_brand->medias)) {
    $files = [];
    foreach ($data->product_brand->medias as $file) {
        $files[] = $file->file_name;
    }
    $file = implode(',', $files);
    echo $this->Form->hidden('media-brand', [
        'class' => 'media-brand',
        'value' => $file,
    ]);
}
