<?php
$name = 'name' . $en;
$output = '<option value="">' . __('TXT_SEARCH') . '</option>';

if ($products) {
    foreach ($products as $product) {
        $product_name = $product->$name;
        if ($product->product_details) {
            foreach ($product->product_details as $p) {
                $value = $product_name . ' ';
                $value .= '(';
                $value .= $p->pack_size;
                $value .= $p->tb_pack_sizes->$name;
                $value .= ' x ';
                $value .= $p->single_unit_size;
                $value .= $p->single_unit->$name;
                $value .= ')';

                $output .= '<option value="' . $p->id . '">' . $value . '</option>';
            }
        }
    }
}