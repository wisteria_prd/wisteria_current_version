<div class="search-section" style="margin-bottom: 20px;">
    <?php
    echo $this->Form->create('Subsidiaries', [
        'role' => 'form',
        'class' => 'form-inline form-search',
        'type' => 'get',
        'name' => 'form_search'
    ]);
    echo $this->Form->hidden('displays', ['value' => $displays]);
    echo $this->Form->input('keyword', [
        'name' => 'keyword',
        'placeholder' => __('USER_SEARCH_KEYWORD'),
        'class' => 'form-control',
        'label' => false,
        'value' => $this->request->query('keyword'),
        'autocomplete' => 'off',
        'templates' => [
            'inputContainer' => '<div class="form-group">{{content}}</div>',
        ]
    ]);
    $this->SwitchStatus->render();
    echo $this->Form->end();
    ?>
</div>
<div class="total-pagination" style="padding-bottom: 20px;">
    <?php echo $this->element('display_info'); ?>
</div>
<div class="row search-section" style="margin-bottom: 20px;">
    <?php echo $this->element('display_number'); ?>
    <?= $this->ActionButtons->btnRegisterNew('Subsidiaries', []) ?>
    <div class="clearfix"></div>
</div>
<div class="data-tb-list">
    <?php if ($subsidiaries): ?>
    <table class="table table-striped">
        <thead>
            <tr>
                <th>#</th>
                <th>&nbsp;</th>
                <th class="white-space">
                    <?php
                    echo $this->Paginator->sort('type', __('TXT_SUBSIDIARY_STATUS')); ?>
                    <i class="fa fa-sort-desc" aria-hidden="true"></i>
                </th>
                <th class="white-space">
                    <?php
                    echo $this->Paginator->sort('code', __('TXT_OLD_CODE')); ?>
                    <i class="fa fa-sort-desc" aria-hidden="true"></i>
                </th>
                <th class="white-space">
                    <?php
                    echo $this->Paginator->sort('name_en', __('STR_SUBSIDIARY_TYPE')); ?>
                    <i class="fa fa-sort-desc" aria-hidden="true"></i>
                </th>
                <th class="white-space">
                    <?php
                    echo $this->Paginator->sort('reciever_name', __('TXT_DELIVER_TO')); ?>
                    <i class="fa fa-sort-desc" aria-hidden="true"></i>
                </th>
                <th class="white-space">
                    <?php
                    echo $this->Paginator->sort('payment_category_id',  __('STR_PAYMENT_CAT')); ?>
                    <i class="fa fa-sort-desc" aria-hidden="true"></i>
                </th>
                <th class="white-space">
                    <?php
                    echo $this->Paginator->sort('invoice_delivery_id', __('STR_INVOICE_DELIVERY')); ?>
                    <i class="fa fa-sort-desc" aria-hidden="true"></i>
                </th>
                <th class="white-space">
                    <?php
                    echo __d('subsidiary', 'TXT_ADDRESS') ?>
                </th>
                <th class="white-space">
                    <?php
                    echo __d('subsidiary', 'TXT_URL') ?>
                </th>
                <th class="white-space">
                    <?php
                    echo __('TBL_PERSON_IN_CHARGE') ?>
                </th>
                <th class="white-space">
                    <?php
                    echo __d('subsidiary', 'TXT_W_SALE') ?>
                </th>
                <th colspan="3">&nbsp;</th>
            </tr>
        </thead>
        <tbody>
            <?php
            $numbering = $this->Paginator->counter('{{start}}');
            foreach ($subsidiaries as $item):
            $total = !empty($item->clinic_doctors) ? $item->clinic_doctors[0]->count_doctor : 0;
            $doctor_id = !empty($item->clinic_doctors) ? $item->clinic_doctors[0]->doctor_id : 0;
            ?>
            <tr data-id="<?php echo h($item->id); ?>">
                <th scope="row">
                    <?php
                    echo $numbering; $numbering++; ?>
                </th>
                <td>
                    <?php
                    echo $this->ActionButtons->disabledText($item->is_suspend) ?>
                </td>
                <td>
                    <?php
                    echo __(h($this->Comment->textHumanize($item->type))); ?>
                </td>
                <td>
                    <?php
                    echo h($item->code); ?>
                </td>
                <td>
                    <?php
                    echo $this->Comment->getFieldByLocal($item, $locale); ?>
                </td>
                <td>
                    <?php
                    echo $item->reciever_name; ?>
                </td>
                <td>
                    <?php
                    if ($payment_list && $item->payment_category_id) {
                        echo $payment_list[$item->payment_category_id];
                    }
                    ?>
                </td>
                <td>
                    <?php
                    if ($invoice_list && $item->invoice_delivery_method_id) {
                        echo $invoice_list[$item->invoice_delivery_method_id];
                    }
                    ?>
                </td>
                <td class="white-space">
                    <?php
                    if ($locale === '_en') {
                        echo $item->prefecture_en . ' ' . $item->city_en;
                    } else {
                        echo $item->prefecture . ' ' . $item->city;
                    }
                    ?>
                </td>
                <td>
                    <?php
                    if ($item->url) {
                        echo $this->Html->link(__('TXT_WEBSITE'), $item->url, [
                            'class' => 'btn btn-primary btn-sm btn-width',
                            'target' => '_blank',
                        ]);
                    } else {
                        echo $this->Form->button(__('TXT_WEBSITE'), [
                            'class' => 'btn btn-primary btn-sm btn-width btn-disabled',
                            'value' => 'Website',
                            'disabled' => 'disabled',
                        ]);
                    }
                    ?>
                </td>
                <td>
                    <?php
                    echo $this->Comment->btnPersonInCharge($item->id, count($item->person_in_charges), TYPE_SUBSIDIARY); ?>
                </td>
                <td class="white-space">
                    <?php
                    if ($item->user) {
                        if ($locale === '_en') {
                            echo h($item->user->lastname_en) . ' ' . h($item->user->firstname_en);
                        } else {
                            echo h($item->user->lastname) . ' ' . h($item->user->firstname);
                        }
                    }
                    ?>
                </td>
                <td width="1%">
                    <?php
                    echo $this->Form->button('<i class="fa fa-comments-o" aria-hidden="true"></i>', [
                        'class' => 'btn btn-primary btn-sm btn_comment',
                        'data-id' => $item->id,
                        'escape' => false
                    ]); ?>
                </td>
                <td data-target="<?php echo ($item->is_suspend) ? 1 : 0; ?>">
                    <?php
                    if ($item->is_suspend == 1) {
                        echo $this->Form->button(BTN_ICON_UNDO, [
                            'class' => 'btn btn-recover btn-sm btn_suspend',
                            'data-name' => 'recover',
                            'escape' => false
                        ]);
                    } else {
                        echo $this->Html->link(BTN_ICON_EDIT, [
                            'controller' => 'subsidiaries',
                            'action' => 'edit', $item->id
                            ], [
                            'class' => 'btn btn-primary btn-sm',
                            'escape' => false
                        ]);
                    }
                    ?>
                </td>
                <td data-target="<?php echo ($item->is_suspend) ? 1 : 0; ?>">
                    <?php
                    if ($item->is_suspend == 1) {
                        if (!empty($item->product_brands)) {
                            echo $this->Form->button(BTN_ICON_DELETE, [
                                'class' => 'btn btn-delete btn-sm',
                                'id' => 'btn_delete',
                                'disabled' => 'disabled',
                                'escape' => false
                            ]);
                        } else {
                            echo $this->Form->button(BTN_ICON_DELETE, [
                                'class' => 'btn btn-delete btn-sm',
                                'id' => 'btn_delete',
                                'data-name' => 'delete',
                                'escape' => false
                            ]);
                        }
                    } else {
                        echo $this->Form->button(BTN_ICON_STOP, [
                            'class' => 'btn btn-suspend btn-sm btn_suspend',
                            'data-name' => 'suspend',
                            'escape' => false
                        ]);
                    }
                    ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <?php endif; ?>
</div>
<div class="row row-top-space">
    <?php echo $this->element('display_number'); ?>
    <?= $this->element('next_prev') ?>
    <div class="clearfix"></div>
</div>

<!--Modal Detail-->
<?php echo $this->element('Modal/view_detail'); ?>
<!--Modal Comment-->
<?php echo $this->element('/Modal/message') ?>
<!--Modal delete-->
<?php echo $this->element('Modal/delete'); ?>
<!--Modal Suspend-->
<?php echo $this->element('Modal/suspend'); ?>

<script>
    (function(e) {
        //auto complete
        $('body').find('#keyword').typeahead({
            minLength: 0,
            autoSelect: true,
            source:  function (query, process) {
                return $.get('<?= $this->Url->build(['action' => 'getSubsidiariesBySearch']) ?>', {
                        keyword: query
                    }, function (data) {
                        var optionData = [];
                        if (data.data !== null && data.data !== 'undefined') {
                            $.each(data.data, function(i, v) {
                                var medical_cor = v.medical_corporation;
                                if (v.name !== '') {
                                    optionData.push(v.name);
                                }
                                if (v.name_en !== '') {
                                    optionData.push(v.name_en);
                                }

                                if (medical_cor !== null && medical_cor !== 'undefined') {
                                    $.each(medical_cor, function(i1, v1) {
                                        if (v1.name !== '') {
                                            optionData.push(v1.name);
                                        }
                                        if (v1.name_en !== '') {
                                            optionData.push(v1.name_en);
                                        }
                                    });
                                }
                            });
                        }
                        return process(optionData);
                    }, 'json');
            }
        });

        $('body').on('click', '.typeahead li', function(e) {
            $('body').find('.form-search').submit();
        });

        $('.btn_comment').click(function (e) {
            e.stopPropagation();
            let params = {
                id: $(this).closest('tr').attr('data-id'),
                type: '<?php echo TYPE_SUBSIDIARY ?>',
                referer: '<?php echo $this->request->controller; ?>'
            };
            let options = {
                url: BASE + 'Messages/getMessageList',
                type: 'GET',
                data: params,
                beforeSend: function () {
                    $.LoadingOverlay('show');
                }
            };
            ajaxRequest(options, SESSION_TIMEOUT, function (data) {
                $('body').prepend(data);
                $('body').find('.modal-message').modal('show');
            });
        });

        //filter message list search to specific
        $('body').on('click', '.btn-to-individual', function() {
            var toAll = $('.to-individual').val();
            if (parseInt(toAll) === 0) {
                $('.to-all').val(0);
                $('.to-individual').val(1);
            } else {
                $('.to-all').val(1);
                $('.to-individual').val(0);
            }
            messagesSearch(listUrlSearch);
        });

        $('body').on('click', '.btn-search', function() {
            messagesSearch(listUrlSearch);
        });

        $('body').on('click', '.btn-suspend-msg', function() {
            var id = $(this).data('id');
            var data = $('#form-message-status-' + id).serialize();
            var suspendUrl = '<?= $this->Url->build(['controller' => 'Messages', 'action' => 'suspend']) ?>';
            suspendMessage(id, suspendUrl, data);
        });

        $('body').on('click', '.data-tb-list .table>tbody>tr', function (e) {
            $.LoadingOverlay('show');
            $.get('<?php echo $this->Url->build('/subsidiaries/view/'); ?>' + $(this).closest('tr').data('id'), function (data) {
                $.LoadingOverlay('hide');
                $('#modal_detail').find('.modal-body').html(data);
                $('#modal_detail').modal('show');
            }, 'html');
        });

        $('body').on('click', '.table td a, .table td select', function(e) {
            e.stopPropagation();
        });

        //modal confirm suspend
        $('body').on('click', '.btn_suspend', function (e) {
            e.stopPropagation();
            $('.suspend-item-id').val($(this).attr('data-id'));
            $('.is-suspend-item').val($(this).attr('data-target'));
            var confirm = '';
            if (parseInt($(this).attr('data-target')) === 1) {
                $('#btn_suspend_yes').text('<?= __('BTN_SUSPEND') ?>');
                confirm = '<?= __('MSG_CONFIRM_SUSPEND') ?>';
            } else {
                $('#btn_suspend_yes').text('<?= __('BTN_RECOVER') ?>');
                confirm = '<?= __('MSG_CONFIRM_UNDO_SUSPEND') ?>';
            }
            $('.confirm-suspend-text').text(confirm);
            $('#modal_suspend').modal('show');
        });

        //modal change suspend status
        $('body .data-tb-list .table > tbody > tr').on('click', '.btn_suspend', function (e) {
            e.stopPropagation();
            var content = $('#modal_suspend');
            var status = $(this).attr('data-name');
            var txt = '';

            switch (status) {
                case 'suspend':
                    $('#btn_suspend_yes').text('<?= __('BTN_SUSPEND') ?>');
                    txt = '<?= __('MSG_CONFIRM_SUSPEND') ?>';
                    break;
                case 'delete':
                    txt = '<?= __('MSG_CONFIRM_DELETE') ?>';
                    $('#btn_suspend_yes').text('<?= __('BTN_DELETE') ?>');
                    break;
                 case 'recover':
                    $('#btn_suspend_yes').text('<?= __('BTN_RECOVER') ?>');
                    txt = '<?= __('MSG_CONFIRM_UNDO_SUSPEND') ?>';
                    break;
            };

            $(content).find('#btn_suspend_yes').data('id', $(this).closest('tr').data('id'));
            $(content).find('#btn_suspend_yes').data('target', $(this).closest('td').data('target'));
            $(content).find('.modal-body p').html(txt);
            $(content).modal('show');
        });

        $('body').on('click', '#btn_suspend_yes', function (e) {
            var url = '<?php echo $this->Url->build('/subsidiaries/update-suspend/'); ?>';
            var params = {
                id: $(this).data('id'),
                is_suspend: $(this).data('target')
            };
            $.post(url, params, function (data) {
                if (data.message === 'success') {
                    location.reload();
                }
            }, 'json');
        });

        //modal delete confirm
        $('body').on('click', '#btn_delete', function (e) {
            e.stopPropagation();
            $('#btn_delete_yes').text('<?= __('BTN_DELETE') ?>');
            $('.delete-item').val($(this).attr('data-id'));
            $('#modal_delete').modal('show');
        });

        //modal delete item.
        $('body').on('click', '#btn_delete_yes', function() {
            var data = $('.form-delete').serialize();
            var url = '<?= $this->Url->build('/subsidiaries/delete') ?>';
            itemDelete(url, data, '<?php echo __('TXT_SESSION_TIMEOUT') ?>');
        });

        $('body').on('click', '.tt-selectable', function (e) {
            $('form[name="form_search"]').submit();
        });

        pageOutOfRange(<?= $paging ?>, '<?= $this->request->query('page') ?>');
        var maxPage = parseInt($('#maxPage').val());
        updatePagination(maxPage);
    })();
</script>
