
<table class="table table-striped" id="tb-detail">
    <tbody>
        <tr>
            <td>
                <?php
                echo __d('subsidiary', 'TXT_STATUS'); ?> :
            </td>
            <td colspan="2">
                <?php
                echo __(h($this->Comment->textHumanize($data->type))); ?>
            </td>
        </tr>
        <tr>
            <td>
                <?php
                echo __d('subsidiary', 'TXT_OLD_CODE'); ?> :
            </td>
            <td colspan="2">
                <?php
                echo $data->code; ?>
            </td>
        </tr>
        <tr>
            <td>
                <?php
                echo __('STR_SUBSIDIARY_TYPE'); ?> :
            </td>
            <td>
                <?php
                echo $data->name; ?>
            </td>
            <td>
                <?php
                echo $data->name_en; ?>
            </td>
        </tr>
        <tr>
            <td>
                <?php
                echo __d('subsidiary', 'TXT_ADDRESS'); ?> :
            </td>
            <td>
                <?php
                echo $this->Address->format($data, LOCAL_EN); ?>
            </td>
            <td>
                <?php
                echo $this->Address->format($data); ?>
            </td>
        </tr>
        <tr>
            <td>
                <?php
                echo __d('subsidiary', 'TXT_TEL'); ?> :
            </td>
            <td>
                <?php
                if ($data->info_details) {
                    $element = '<ul class="email-wrap">';
                    foreach ($data->info_details as $item) {
                        $element .= '<li>' . $item->tel . '</li>';
                    }
                    echo $element . '</ul>';
                }
                ?>
            </td>
            <td>
                <?php
                if ($data->info_details) {
                    $element = '<ul class="email-wrap">';
                    foreach ($data->info_details as $item) {
                        $element .= '<li>' . $item->tel_extension . '</li>';
                    }
                    echo $element . '</ul>';
                }
                ?>
            </td>
        </tr>
        <tr>
            <td>
                <?php
                echo __d('subsidiary', 'TXT_FAX'); ?> :
            </td>
            <td colspan="2">
                <?php
                echo $data->fax; ?>
            </td>
        </tr>
        <tr>
            <td>
                <?php
                echo __d('subsidiary', 'TXT_EMAIL'); ?> :
            </td>
            <td colspan="2">
                <?php
                if ($data->info_details) {
                    $element = '<ul class="email-wrap">';
                    foreach ($data->info_details as $item) {
                        $element .= '<li><a href="mailto:' . $item->email . '" target="_blank">' . $item->email . '</a></li>';
                    }
                    echo $element . '</ul>';
                }
                ?>
            </td>
        </tr>
        <tr>
            <td>
                <?php
                echo __d('subsidiary', 'TXT_URL'); ?> :
            </td>
            <td colspan="2">
                <a href="<?= h($data->url) ?>" target="_blank"><?= h($data->url) ?></a>
            </td>
        </tr>
        <tr>
            <td>
                <?php
                echo __d('subsidiary', 'TXT_FORWARDING'); ?> :
            </td>
            <td colspan="2">
                
            </td>
        </tr>
        <tr>
            <td>
                <?php
                echo __d('subsidiary', 'TXT_PAYMENT_TERM'); ?> :
            </td>
            <td colspan="2">
                <?php
                echo $data->payment_name; ?>
            </td>
        </tr>
        <tr>
            <td>
                <?php
                echo __d('subsidiary', 'TXT_INVOICING_BY'); ?> :
            </td>
            <td colspan="2">
                <?php
                echo $data->invoice_name; ?>
            </td>
        </tr>
        <tr>
            <td>
                <?php
                echo __d('subsidiary', 'TXT_PERSON_IN_CHARGE'); ?> :
            </td>
            <td colspan="2">
                <?php
                $TXT_PERSON_IN_CHARGE_NUMBER = '';
                if (count($data->person_in_charges) < 2) {
                    $TXT_PERSON_IN_CHARGE_NUMBER = __('CUSTOMERS_TXT_PERSON_IN_CHARGE_NUMBER_SINGULAR');
                } else {
                    $TXT_PERSON_IN_CHARGE_NUMBER = __('CUSTOMERS_TXT_PERSON_IN_CHARGE_NUMBER_PLURAL');
                }
                echo $this->Html->link(count($data->person_in_charges).' '.$TXT_PERSON_IN_CHARGE_NUMBER, [
                    'controller' => 'personInCharges',
                    'action' => (count($data->person_in_charges) ? 'index' : 'add'),
                    '?' => [
                        'external_id' => $data->id,
                        'filter_by' => TYPE_SUBSIDIARY,
                        'des' => TYPE_SUBSIDIARY,
                    ]
                ], [
                    'class' => 'btn btn-default text-center btn-sm',
                ]); ?>
            </td>
        </tr>
        <tr>
            <td>
                <?php
                echo __d('subsidiary', 'TXT_CLINIC'); ?> :
            </td>
            <td colspan="2">
                <?php if ($data->customer_subsidiaries): ?>
                    <ul>
                    <?php foreach ($data->customer_subsidiaries as $key => $item): ?>
                        <li>
                            <?php
                            echo $this->Comment->getFieldByLocal($item->customer, $locale); ?>
                        </li>
                    <?php endforeach; ?>
                    </ul>
                <?php endif; ?>
            </td>
        </tr>
        <tr>
            <td>
                <?php
                echo __('DATE_TXT_MODIFIED'); ?> :
            </td>
            <td colspan="2">
                <?php
                echo date('Y-m-d', strtotime($data->modified)); ?>
            </td>
        </tr>
        <tr>
            <td>
                <?php
                echo __d('subsidiary', 'TXT_REGISTERD'); ?> :
            </td>
            <td colspan="2">
                <?php
                echo date('Y-m-d', strtotime($data->created)); ?>
            </td>
        </tr>
    </tbody>
</table>
