<table cellspacing="0" cellpadding="5" border="0">
    <tr>
        <td width="60"><strong><?= __('To') ?></strong></td>
        <td width="3">:</td>
        <td width="400"><?= $user_email ?></td>
    </tr>
    <tr>
        <td><strong><?= __('Subject') ?></strong></td>
        <td>:</td>
        <td><?= $service ?> パスワード再登録</td>
    </tr>
</table>
<br />
<br />
<p>ご登録頂いたメールアドレスへのご案内です。</p>
<br />
<p>下記<strong>URL</strong>にアクセスの上、<strong>24</strong>時間の<strong>URL</strong>有効期限内にパスワード再登録を完了してください。</p>
<a href="<?= $this->Url->build(['controller' => 'Users', 'action' => 'activation', $token, '?' => ['type' => 'reset_password']], ['fullBase' => true, 'escape' => false]) ?>" target="_blank"><?= $this->Url->build(['controller' => 'Users', 'action' => 'activation', $token, '?' => ['type' => 'reset_password']], ['fullBase' => true, 'escape' => false]) ?></a>
<br />
<br />
<p>登録に心当たりが無い場合、以下にお問い合わせください。</p>
<br />
<br />
<a href="mailto:XXX@wwisteria.co.jp?Subject=User Activation" target="_top">XXX@wwisteria.co.jp</a>
<p><strong><?= $service ?></strong>　カスタマーサポート</p>