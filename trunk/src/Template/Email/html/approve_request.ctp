<table cellspacing="0" cellpadding="5" border="0">
    <tr>
        <td width="60"><strong><?= __('To') ?></strong></td>
        <td width="3">:</td>
        <td width="400"><?= $user_email ?></td>
    </tr>
    <tr>
        <td><strong><?= __('Subject') ?></strong></td>
        <td>:</td>
        <td><?= $service ?> 仕入れ承認依頼（2017-011-22 Sample Supplier Name）</td>
    </tr>
</table>
<br />
<br />
<p>仕入れ用のPOが登録されましたので、承認をしてください。</p>
<br />
<a href="<?= $this->Url->build(['controller' => 'Pos', 'action' => 'approving'], ['fullBase' => true, 'escape' => false]) ?>" target="_blank"><?= $this->Url->build(['controller' => 'Pos', 'action' => 'approving'], ['fullBase' => true, 'escape' => false]) ?></a>
<br />
<br />
<a href="mailto:XXX@wwisteria.co.jp?Subject=User Activation" target="_top">XXX@wwisteria.co.jp</a>
<p><strong><?= $service ?></strong>　カスタマーサポート</p>