<?php
if ($payments):
    $output = '<option value="">' . __('TXT_SELECT_PAYMENT') . '</option>';
    $name = 'name' . $en;
    foreach ($payments as $key => $payment) {
        $output .= '<option value="' . $key . '">' . $payment . '</option>';
    }
    echo $output;
endif;