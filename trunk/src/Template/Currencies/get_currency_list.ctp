<div class="row">
    <div class="col-sm-12">
        <h2 class="text-center"><?= __('Currencies') ?></h2>
    </div>
</div>
<div class="row row-top-space">
    <div class="col-sm-12">
        <?php
        echo $this->Form->create(null, [
            'role' => 'form',
            'class' => 'form-create-currencies',
            'name' => 'common_form'
        ]);

        $this->Form->templates([
            'inputContainer' => '{{content}}'
        ]);
        
        echo $this->Form->hidden('id', ['class' => 'currency-pop-id', 'value' => '']);
        ?>
        <div class="row">
            <div class="col-sm-5">
                <label for="name"><?= __('Name') ?></label>
            </div>
            <div class="col-sm-5 field-small-padding-left">
                <label for="name"><?= __('Code') ?></label>
            </div>
            <div class="col-sm-2">
                <label for="name">&nbsp;</label>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-5 field-small-padding-right">
                <?php echo $this->Form->input('name', [
                    'class' => 'form-control',
                    'label' => false,
                    'required' => false,
                    'id' => 'currency-name'
                ]); ?>
                <span class="help-block help-tips" id="error-currency-name"></span>
            </div>
            <div class="col-sm-5 field-small-padding-right field-small-padding-left">
                <?php echo $this->Form->input('code', [
                    'class' => 'form-control',
                    'label' => false,
                    'required' => false,
                    'id' => 'currency-code'
                ]); ?>
                <span class="help-block help-tips" id="error-currency-code"></span>
            </div>
            <div class="col-sm-2 field-small-padding-left">
                <button type="button" class="btn btn-sm btn-primary btn-width btn-create-currency"><?= __('Add') ?></button>
            </div>
        </div>
        <?php echo $this->Form->end() ?>
    </div>
</div>
<div class="row row-top-space">
    <div class="col-sm-12">
        <div class="detail-list">
            <table class="table table-condensed table-striped table-currency">
                <thead>
                    <tr>
                        <th width="5%">#</th>
                        <th width="40%"><?= __('Name') ?></th>
                        <th width="40%"><?= __('Code') ?></th>
                        <th width="15%">&nbsp;</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        if ($currencies) {
                            $i = 1;
                            foreach ($currencies as $currency) {
                                echo '<tr class="row-currency'.$currency->id.'">';
                                    echo '<th>' . $i . '</th>';
                                    echo '<td class="r-name">' . $currency->name . '</td>';
                                    echo '<td class="r-code">' . $currency->code . '</td>';
                                    echo '<td>';
                                    echo $this->Form->button(BTN_ICON_EDIT, [
                                            'type' => 'button',
                                            'class' => 'btn btn-primary btn-sm btn-edit-currency',
                                            'escape' => false,
                                            'data-id' => $currency->id
                                        ]);
                                    echo '&nbsp;&nbsp;';
                                    echo $this->Form->button(BTN_ICON_DELETE, [
                                        'type' => 'button',
                                        'class' => 'btn btn-delete btn-sm btn-delete-currency',
                                        'id' => false,
                                        'escape' => false,
                                        'data-id' => $currency->id
                                    ]);
                                    echo '</td>';
                                echo '</tr>';
                                $i++;
                            }
                        }
                    ?>
                </tbody>
            </table>
        </div>
    </div>
</div>