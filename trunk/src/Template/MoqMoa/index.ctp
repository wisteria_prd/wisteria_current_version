<?php $this->assign('title', 'MoqMoa index'); ?>
<div class="row">
    <div class="col-md-12">
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>#</th>
                    <th>&nbsp;</th>
                    <th><?= __('MOQ/MOA Condition') ?></th>
                    <th><?= __('Type') ?></th>
                    <th><?= __('MOA/MOQ Target') ?></th>
                    <th><?= __('Range Type') ?></th>
                    <th><?= __('Range Value') ?></th>
                    <th><?= __('Currency Type') ?></th>
                    <th width="8%">&nbsp;</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>1</td>
                    <td>&nbsp;</td>
                    <td>Brand</td>
                    <td>MOQ</td>
                    <td>&nbsp;</td>
                    <td>Amount</td>
                    <td>1,000</td>
                    <td>USD</td>
                    <td>
                        <?= $this->Html->link(BTN_ICON_EDIT, ['controller' => 'moq_moa', 'action' => 'edit', 1], ['class' => 'btn btn-primary btn-sm', 'role' => 'button', 'escape' => false]) ?>
                        <?= $this->Form->button(BTN_ICON_STOP, ['class' => 'btn btn-suspend btn-sm btn-stop-condition', 'data-toggle' => 'popover', 'escape' => false]) ?>
                    </td>
                </tr>
                <tr>
                    <td>2</td>
                    <td>
                        <label class="stop-label">Stopped</label>
                    </td>
                    <td>Brand</td>
                    <td>MOQ</td>
                    <td>Manufacturer Brand A</td>
                    <td>Quantity</td>
                    <td>1,000</td>
                    <td>USD</td>
                    <td>
                        <?= $this->Form->button(BTN_ICON_UNDO, ['class' => 'btn btn-recover btn-sm btn-toggle-close btn-restore-condition', 'data-toggle' => 'popover', 'escape' => false]) ?>
                        <?= $this->Form->button(BTN_ICON_DELETE, ['class' => 'btn btn-delete btn-sm btn-toggle-close btn-remove-condition', 'data-toggle' => 'popover', 'escape' => false]) ?>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</div>

<script>
    $(function() {
        var msg = '<?= __('Are you sure to stop this condition?') ?>';
        confirm_yesno('btn-stop-condition', msg, 'left', 'Stop item?');

        var msg = '<?= __('Are you sure to restore this condition?') ?>';
        confirm_yesno('btn-restore-condition', msg, 'left', 'Stop item?');

        var msg = '<?= __('Are you sure to physically delete this condition?') ?>';
        confirm_yesno('btn-remove-condition', msg, 'left', 'Stop item?');

        $('body').on('click', '.confirm-no', function() {
            $(this).parents('.popover').prev('button').click();
        });

        $('body').on('click', '.confirm-yes', function() {

        });
    });
</script>
