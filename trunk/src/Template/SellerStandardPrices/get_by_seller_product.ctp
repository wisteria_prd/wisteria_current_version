
<?php
if ($data) :
$num = 0;
foreach ($data as $key => $item) : $num++; ?>
<tr data-id="<?php echo $item->id; ?>" data-price="<?php echo $item->purchase_standard_price; ?>" data-prd-id="<?php echo $item->product_detail_id; ?>">
    <th><?php echo $num; ?></th>
    <td><?php echo $product_details[$item->product_detail_id]; ?></td>
    <td>
        <?php
        echo $item->seller_product->currency->name . ' ' . $this->Comment->floatFormat($item->purchase_standard_price);
        ?>
    </td>
    <td class="text-right">
        <?php
        echo $this->Form->button(BTN_ICON_EDIT,[
            'type' => 'button',
            'class' => 'btn btn-primary btn-sm btn-edit',
            'escape' => false,
        ]);
        echo '&nbsp;&nbsp;';
        echo $this->Form->button(BTN_ICON_DELETE, [
            'type' => 'button',
            'class' => 'btn btn-delete btn-sm btn-remove',
            'escape' => false,
        ]);
        ?>
    </td>
</tr>
<?php endforeach; endif; ?>