<div class="search-section" style="margin-bottom: 20px;">
    <?php
    echo $this->Form->create('SellerStandardPrices', [
        'class' => 'form-inline',
        'type' => 'get',
        'name' => 'form_search',
    ]);
    echo $this->Form->input('product_name', [
        'name' => 'keyword',
        'placeholder' => __d('seller_standard_price', 'TXT_ENTER_KEYWORD'),
        'class' => 'form-control',
        'label' => false,
        'value' => $this->request->query('keyword'),
        'templates' => [
            'inputContainer' => '<div class="form-group">{{content}}</div>',
        ]
    ]);
    $this->SwitchStatus->render();
    echo $this->Form->hidden('type');
    echo $this->Form->end();
    ?>
</div>
<div class="total-pagination" style="padding-bottom: 20px;">
    <?php echo $this->element('display_info'); ?>
</div>
<div class="row search-section" style="margin-bottom: 20px;">
    <?php echo $this->element('display_number'); ?>
    <div class="col-md-6 col-sm-6 text-center"></div>
    <?= $this->ActionButtons->btnRegisterNew('SellerStandardPrices', []) ?>
    <div class="clearfix"></div>
</div>
<div class="data-tb-list">
    <?php if ($data): ?>
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>#</th>
                    <th>&nbsp;</th>
                    <th>
                        <?php
                        echo __d('seller_standard_price', 'TXT_SUPPLIER'); ?>
                    </th>
                    <th>
                        <?php
                        echo __d('seller_standard_price', 'TXT_MANUFACTURER'); ?>
                    </th>
                    <th>
                        <?php
                        echo $this->Paginator->sort('product_name', __d('seller_standard_price', 'TXT_PRODUCT')); ?>
                        <i class="fa fa-sort-desc" aria-hidden="true"></i>
                    </th>
                    <th>
                        <?php
                        echo $this->Paginator->sort('price', __d('seller_standard_price', 'TXT_UNIT_PRICE')); ?>
                        <i class="fa fa-sort-desc" aria-hidden="true"></i>
                    </th>
                    <th width="1%">&nbsp;</th>
                    <th width="1%">&nbsp;</th>
                    <th width="1%">&nbsp;</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $numbering = $this->Paginator->counter('{{start}}');
                foreach ($data as $key => $item):
                    //pr($item);exit;
                ?>
                <tr data-id="<?php echo h($item->id); ?>">
                    <th scope="row" class="td_evt">
                        <?php
                        echo $numbering; $numbering++; ?>
                    </th>
                    <td>
                        <?php
                        echo $this->ActionButtons->disabledText($item->is_suspend) ?>
                    </td>
                    <td>
                        <?php
                        $field = $this->request->session()->read('tb_field');
                        $seller = $item['seller_brand']['seller'];
                        echo $seller[$seller['type']]['name' . $field];
                        ?>
                    </td>
                    <td><?php echo $item->is_suspend; ?></td>
                    <td><?php echo $item->is_suspend; ?></td>
                    <td><?php echo $item->is_suspend; ?></td>
                    <td>
                        <?php
                        echo $this->Form->button('<i class="fa fa-comments-o" aria-hidden="true"></i>', [
                            'class' => 'btn btn-primary btn-sm btn_comment',
                            'data-id' => $item->id,
                            'escape' => false,
                        ]);
                        ?>
                    </td>
                    <td data-target="<?php echo ($item->is_suspend) ? 1 : 0; ?>">
                        <?php
                        if ($item->is_suspend == 1) {
                            echo $this->Form->button(BTN_ICON_UNDO, [
                                'class' => 'btn btn-recover btn-sm btn_suspend',
                                'data-name' => 'recover',
                                'escape' => false
                            ]);
                        } else {
                            echo $this->Html->link(BTN_ICON_EDIT, [
                                'controller' => 'seller-standard-prices',
                                'action' => 'edit-brand', $item->id,
                            ], [
                                'class' => 'btn btn-primary btn-sm',
                                'escape' => false
                            ]);
                        }
                        ?>
                    </td>
                    <td data-target="<?php echo ($item->is_suspend) ? 1 : 0; ?>">
                        <?php
                        if ($item->is_suspend == 1) {
                            if (!empty($item->count_product)) {
                                echo $this->Form->button(BTN_ICON_DELETE, [
                                    'class' => 'btn btn-delete btn-sm',
                                    'id' => 'btn_delete',
                                    'disabled' => 'disabled',
                                    'escape' => false
                                ]);
                            } else {
                                echo $this->Form->button(BTN_ICON_DELETE, [
                                    'class' => 'btn btn-delete btn-sm',
                                    'id' => 'btn_delete',
                                    'escape' => false
                                ]);
                            }
                        } else {
                            echo $this->Form->button(BTN_ICON_STOP, [
                                'class' => 'btn btn-suspend btn-sm btn_suspend',
                                'data-name' => 'suspend',
                                'escape' => false
                            ]);
                        }
                        ?>
                    </td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    <?php endif; ?>
</div>
<div class="row row-top-space" style="padding-bottom: 20px;">
    <?php
    echo $this->element('display_number'); ?>
    <?php
    echo $this->element('next_prev') ?>
    <div class="clearfix"></div>
</div>

<!--Modal Detail-->
<?php
echo $this->element('Modal/view_detail'); ?>
<!--Modal Suspend-->
<?php
echo $this->element('Modal/suspend'); ?>
<!--Modal delete-->
<?php
echo $this->element('Modal/delete'); ?>

<script>
    (function(e) {
        $('.btn_comment').click(function (e) {
            e.stopPropagation();
            let params = {
                id: $(this).closest('tr').attr('data-id'),
                type: '<?php echo MESSAGE_TYPE_SELLER_STANDARD_PRICE; ?>',
                referer: CONTROLLER,
            };
            let options = {
                url: BASE + 'messages/get-message-list',
                type: 'GET',
                data: params,
                beforeSend: function () {
                    $.LoadingOverlay('show');
                }
            };
            ajaxRequest(options, SESSION_TIMEOUT, function (data) {
                $('body').prepend(data);
                $('body').find('.modal-message').modal('show');
            });
        });

        $('#product-name').autocomplete({
            source: function(request, response) {
                $.ajax({
                    url: '<?= $this->Url->build(['controller' => 'products', 'action' => 'loadName']) ?>',
                    type: 'GET',
                    dataType: 'JSON',
                    data: {keyword: request.term},
                    success: function(result) {
                        response($.map(result, function(value) {
                            return {
                                label: value.name,
                                value: value.name
                            };
                        }));
                    }
                });
            },
            minLength: 2,
            open: function(event, ui) {
                $(this).autocomplete('widget').css({
                    'width': 350
                });
            },
            select: function(event, ui) {
                $('#keyword').val(ui.item.value);
                $('#form-search').submit();
            }
        });

        $('body').on('click', '.btn_suspend', function (e) {
            e.stopPropagation();
            var content = $('#modal_suspend');
            var txt = '';
            if ($(this).data('name') === 'recover') {
                $('#btn_suspend_yes').text('<?= __('BTN_RECOVER') ?>');
                txt = '<?= __('MSG_CONFIRM_UNDO_SUSPEND') ?>';
            } else {
                $('#btn_suspend_yes').text('<?= __('BTN_SUSPEND') ?>');
                txt = '<?= __('MSG_CONFIRM_SUSPEND') ?>';
            }

            $(content).find('#btn_suspend_yes').data('id', $(this).closest('tr').data('id'));
            $(content).find('#btn_suspend_yes').data('target', $(this).closest('td').data('target'));
            $(content).find('.modal-body p').html(txt);
            $(content).modal('show');
        });

        $('body').on('click', '#btn_suspend_yes', function (e) {
            var params = {
                id: $(this).data('id'),
                is_suspend: $(this).data('target')
            };
            $.post('<?php echo $this->Url->build('/seller-standard-prices/update-suspend/'); ?>', params, function (data) {
                if (data.message === 'success') {
                    location.reload();
                }
            }, 'json');
        });

        $('body').on('click', '#btn_delete', function (e) {
            e.stopPropagation();
            var content = $('#modal_delete');
            $(content).find('#btn_delete_yes').data('id', $(this).closest('tr').data('id'));
            $(content).modal('show');
        });

        $('body').on('click', '#btn_delete_yes', function (e) {
            $.post('<?php echo $this->Url->build('/seller-standard-prices/delete/'); ?>', {id: $(this).data('id')}, function (data) {
                if (data.message === 'success') {
                    location.reload();
                }
            }, 'json');
        });

        $('body').on('click', '.table>tbody>tr>td a, .table>tbody>tr>td button', function(e) {
            e.stopPropagation();
        });

        pageOutOfRange(<?= $paging ?>, '<?= $this->request->query('page') ?>');
        var maxPage = parseInt($('#maxPage').val());
        updatePagination(maxPage);
    })();
</script>
