<section class="modal-wrapper">
    <div class="modal fade modal-invoice-method" data-backdrop="static" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <?php
                    echo $this->Form->button('<span aria-hidden="true">&times;</span>', [
                        'type' => 'button',
                        'class' => 'close modal-close',
                        'data-dismiss' => 'modal',
                        'aria-label' => 'Close',
                        'escape' => false,
                    ]);
                    ?>
                    <h4 class="modal-title text-center"><?= __('TXT_INVOICE_DELIVERY') ?></h4>
                </div>
                <div class="modal-body fixed-modal-height">
                    <div class="form-wrapper">
                        <label><?= __('TXT_INVOICE_DELIVERY') ?></label>
                        <?= $this->Form->create(null, [
                            'class' => 'form-horizontal',
                            'role' => 'form',
                            'name' => 'invoice_form',
                        ]) ?>
                        <div class="form-group">
                            <div class="col-sm-6 field-small-padding-right">
                                <?= $this->Form->input('name', [
                                    'class' => 'form-control',
                                    'placeholder' => __('TXT_ENTER_INVOICE_NAME_JP'),
                                ]) ?>
                            </div>
                            <div class="col-sm-6 field-small-padding-left">
                                <?= $this->Form->input('name_en', [
                                    'class' => 'form-control',
                                    'placeholder' => __('TXT_ENTER_INVOICE_NAME_EN'),
                                ]) ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-12 text-center">
                                <?= $this->Form->button(__('TXT_ADD'), [
                                    'type' => 'button',
                                    'class' => 'btn btn-double-width btn-primary btn-sm btn-save'
                                ]) ?>
                            </div>
                        </div>
                        <?php echo $this->Form->end(); ?>
                    </div>
                    <table class="table table-striped custom-table-space invoice-table">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th width="38%"><?= __('TXT_NAME') ?></th>
                                <th width="38%"><?= __('TXT_NAME_ENGLISH') ?></th>
                                <th>&nbsp;</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if ($invoices): ?>
                                <?php foreach ($invoices as $key => $value): ?>
                                    <tr data-id="<?= $value->id ?>">
                                        <td><?= h($key+ 1) ?></td>
                                        <td><?= $value->name ?></td>
                                        <td><?= $value->name_en ?></td>
                                        <td>
                                            <?= $this->Form->button(BTN_ICON_EDIT, [
                                                'type' => 'button',
                                                'class' => 'btn btn-primary btn-sm btn-edit-payment',
                                                'escape' => false,
                                            ]) ?>&nbsp;&nbsp;
                                            <?= $this->Form->button(BTN_ICON_DELETE, [
                                                'type' => 'button',
                                                'class' => 'btn btn-delete btn-sm btn-delete-payment',
                                                'id' => false,
                                                'escape' => false,
                                            ]) ?>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </tbody>
                    </table>
                </div>
                <div class="modal-footer">
                    <div class="text-center">
                        <?= $this->Form->button(__('TXT_CLOSE'), [
                            'class' => 'btn btn-default btn-width btn-sm btn-close-modal modal-close btn-close',
                            'data-dismiss' => 'modal',
                            'aria-label' => 'Close',
                        ]) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php
    echo $this->element('Modal/delete');
    echo $this->Html->script([
        'invoice_delivery_method',
    ], ['blog' => false]);
    ?>
</section>