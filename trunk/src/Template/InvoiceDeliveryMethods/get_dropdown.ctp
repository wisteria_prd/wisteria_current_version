<?php
if ($invoices):
    $output = '<option value="">' . __('TXT_SELECT_INVOICE') . '</option>';
    $name = 'name' . $en;
    foreach ($invoices as $key => $invoice) {
        $output .= '<option value="' . $key . '">' . $invoice . '</option>';
    }
    echo $output;
endif;