<div class="row">
    <div class="col-md-10 col-md-offset-1">
        <div class="row">
            <h2><?php echo __('Comments') ?></h2>
        </div>
        <div class="row row-top-space">
            <?php
            echo $this->Form->create(null, [
                'class' => 'form-horizontal',
                'autocomplete' => 'off',
                'type' => 'file',
                'name' => 'form_reply',
                'onsubmit' => 'return false;',
            ]);
            echo $this->Form->hidden('message_id', [
                'value' => $message_id,
            ]);
            echo $this->Form->hidden('parent_id', [
                'value' => $parent_id,
            ]);
            ?>
            <div class="form-group">
                <div class="col-md-6">
                    <?php
                    echo $this->Form->textarea('description', [
                        'class' => 'form-control reply-description',
                    ]);
                    ?>
                </div>
                <div class="col-md-2">
                    <?php
                    echo $this->Form->button(__('Comment'), [
                        'class' => 'btn-sm btn btn-primary btn-add-comment'
                    ]);
                    ?>
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-6 file-wrap">
                    <?php echo $this->Form->file('file') ?>
                    <div class="img-preview" style="display: none;">
                        <img id="imgPreview" src="#"/>
                    </div>
                </div>
            </div>
            <?php echo $this->Form->end() ?>
        </div>
    </div>
</div>