
<div class="jumbotron container text-center">
    <h1>Access Denied!</h1>
    <p>Please contact to administration</p>
    <?= $this->Html->link('<i class="fa fa-undo" aria-hidden="true"></i> Go Back', 'javascript:history.back()', [
        'class' => 'btn btn-large btn-info',
        'escape' => false,
    ]) ?>
</div>