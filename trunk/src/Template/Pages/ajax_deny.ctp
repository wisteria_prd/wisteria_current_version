
<section class="modal-wrapper">
    <style>
        h2 {
            margin: 0;
            font-size: 35px;
            padding-bottom: 10px;
        }
    </style>
    <div class="modal fade" id="access-denied" data-backdrop="static" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <?php
                    echo $this->Form->button('<span aria-hidden="true">&times;</span>', [
                        'type' => 'button',
                        'class' => 'close modal-close',
                        'data-dismiss' => 'modal',
                        'aria-label' => 'Close',
                        'escape' => false,
                    ]);
                    ?>
                    <h4 class="modal-title text-center"><?= __('TXT_CREATE') ?></h4>
                </div>
                <div class="modal-body">
                    <div class="jumbotron text-center">
                        <h2>Access Denied!</h2>
                        <p>Please contact to administration</p>
                    </div>
                </div>
                <div class="modal-footer">
                    <?= $this->Form->button(__('TXT_CANCEL'), [
                        'type' => 'button',
                        'class' => 'btn btn-default btn-sm btn-width modal-close',
                        'data-dismiss' => 'modal',
                        'aria-label' => 'Close',
                    ]) ?>
                </div>
            </div>
        </div>
    </div>
</section>
