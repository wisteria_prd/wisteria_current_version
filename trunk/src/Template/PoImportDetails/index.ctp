
<div class="row">
    <div class="col-sm-12 text-center">
        <h2 style="margin-bottom: 30px;"><?= __('Import Details Information') ?></h2>
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <?php
        echo $this->Form->create(null, [
            'role' => 'form',
            'class' => 'form-horizontal form-po-import-detail',
            'name' => 'common_form'
        ]);

        $this->Form->templates([
            'inputContainer' => '{{content}}'
        ]);
        ?>
        <div class="purchases">
            <div class="form-group row-purchase">
                <label class="control-label col-md-3 col-md-offset-2" for=""><?= __('Product Name:') ?></label>
                <div class="col-md-4">
                    <?php
                        echo $this->Form->input('role', [
                            'label' => false,
                            'class' => 'form-control',
                            'placeholder' => __('Product Name')
                        ]);
                    ?>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-3 col-md-offset-2" for=""><?= __('Lot Number:') ?></label>
            <div class="col-md-4">
                <?php
                    echo $this->Form->input('role', [
                        'label' => false,
                        'class' => 'form-control',
                        'placeholder' => __('Lot Number')
                    ]);
                ?>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-3 col-md-offset-2" for=""><?= __('Expiry Date:') ?></label>
            <div class="col-md-4">
                <div class="input-group with-datepicker">
                    <?= $this->Form->input('date', ['class' => 'form-control datepicker', 'label' => false, 'placeholder' => __('Expiry Date')]) ?>
                    <div class="input-group-btn">
                        <button type="button" class="btn btn-default trigger-calendar" aria-label="Calendar">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-3 col-md-offset-2" for=""><?= __('Quantity') ?></label>
            <div class="col-md-4">
                <?php
                    echo $this->Form->input('role', [
                        'type' => 'text',
                        'label' => false,
                        'class' => 'form-control',
                        'placeholder' => __('Quantity')
                    ]);
                ?>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-3 col-md-offset-2" for=""><?= __('Damage Level') ?></label>
            <div class="col-md-4">
                <?php
                    echo $this->Form->input('role', [
                        'type' => 'text',
                        'label' => false,
                        'class' => 'form-control',
                        'placeholder' => __('Damage Level')
                    ]);
                ?>
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-12 text-center">
                <?php echo $this->Html->link(__('キャンセル'), [
                    'controller' => 'Focs',
                    'action' => 'index'
                ], [
                    'class' => 'btn btn-default form_btn btn-width btn-sm',
                    'id' => 'btn-cancel',
                ]); ?>
                &nbsp; &nbsp;
                <?php
                echo $this->Form->button(__('登録'), [
                    'type' => 'button',
                    'class' => 'btn btn-primary form_btn btn-width btn-sm',
                    'id' => 'btn-register',
                ]);
                ?>
            </div>
        </div>
        <?= $this->Form->end(); ?>
    </div>
</div>

<div class="data-tb-list">
    <?php if ($data): ?>
    <table class="table table-striped">
        <thead>
            <tr>
                <th>#</th>
                <th>
                    <?php echo $this->Paginator->sort('', __('Product Name')); ?>
                    <i class="fa fa-sort-desc" aria-hidden="true"></i>
                </th>
                <th class="white-space">
                    <?php echo $this->Paginator->sort('', __('Lot Number')); ?>
                    <i class="fa fa-sort-desc" aria-hidden="true"></i>
                </th>
                <th class="white-space">
                    <?php echo $this->Paginator->sort('', __('Expiry Date')); ?>
                    <i class="fa fa-sort-desc" aria-hidden="true"></i>
                </th>
                <th class="white-space">
                    <?php echo $this->Paginator->sort('', __('Quantity')); ?>
                    <i class="fa fa-sort-desc" aria-hidden="true"></i>
                </th>
                <th class="white-space">
                    <?php echo $this->Paginator->sort('', __('Damage Level')); ?>
                    <i class="fa fa-sort-desc" aria-hidden="true"></i>
                </th>
                <th colspan="2">&nbsp;</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>1</td>
                <td>Product A (2a x 2b)</td>
                <td>986412</td>
                <td>2017-09-28</td>
                <td>10</td>
                <td>A</td>
                <td width="1%">
                    <button class="btn btn-recover btn-sm btn_suspend" data-name="recover" type="submit">
                        <i class="fa fa-undo" aria-hidden="true"></i>
                    </button>
                </td>
                <td width="1%">
                    <button class="btn btn-delete btn-sm" id="btn_delete" type="submit">
                        <i class="fa fa-trash" aria-hidden="true"></i>
                    </button>
                </td>
            </tr>
            <tr>
                <td>2</td>
                <td>Product A (2a x 2b)</td>
                <td>986412</td>
                <td>2017-09-28</td>
                <td>12</td>
                <td>B</td>
                <td>
                    <a href="#" class="btn btn-primary btn-sm">
                        <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                    </a>
                </td>
                <td>
                    <button class="btn btn-suspend btn-sm btn_suspend" data-name="suspend" type="submit">
                        <i class="fa fa-ban" aria-hidden="true"></i>
                    </button>
                </td>
            </tr>
            <tr>
                <td>3</td>
                <td>Product A (2a x 2b)</td>
                <td>986412</td>
                <td>2017-09-28</td>
                <td>20</td>
                <td>A</td>
                <td>
                    <a href="#" class="btn btn-primary btn-sm">
                        <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                    </a>
                </td>
                <td>
                    <button class="btn btn-suspend btn-sm btn_suspend" data-name="suspend" type="submit">
                        <i class="fa fa-ban" aria-hidden="true"></i>
                    </button>
                </td>
            </tr>
            <tr>
                <td>4</td>
                <td>Product A (2a x 2b)</td>
                <td>986412</td>
                <td>2017-09-28</td>
                <td>10</td>
                <td>A</td>
                <td>
                    <a href="#" class="btn btn-primary btn-sm">
                        <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                    </a>
                </td>
                <td>
                    <button class="btn btn-suspend btn-sm btn_suspend" data-name="suspend" type="submit">
                        <i class="fa fa-ban" aria-hidden="true"></i>
                    </button>
                </td>
            </tr>
        </tbody>
    </table>
    <?php endif; ?>
</div>
<?php
    echo $this->Html->css([
        'bootstrap-datetimepicker.min',
        'jquery-ui',
        'intlTelInput',
    ]);
?>
<script>
    (function(e) {
        $('.datepicker').datetimepicker({
            format: 'YYYY-MM-DD',
            minDate: '2007-01-01'
        });

        $('.trigger-calendar').click(function() {
            $(this).parents('.with-datepicker').find('.datepicker').focus();
        });
    })();
</script>
