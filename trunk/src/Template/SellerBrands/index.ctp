
<div class="search-section" style="margin-bottom: 20px;">
    <?php
    echo $this->Form->create('SellerBrands', [
        'class' => 'form-inline form-search',
        'type' => 'get',
        'name' => 'form_search',
    ]);
    echo $this->Form->hidden('displays', [
        'value' => empty($this->request->query('displays')) ? 10 : $this->request->query('displays')
    ]); ?>
    <div class="form-group custom-select">
        <?php
        echo $this->Form->text('brand_name', [
            'class' => 'form-control',
            'placeholder' => __('USER_SEARCH_KEYWORD'),
            'label' => false,
            'autocomplete' => 'off',
            'value' => $this->request->query('brand_name') ? $this->request->query('brand_name') : null,
        ]); ?>
    </div>
    <div class="form-group custom-select">
        <?php
        $spl = [];
        if ($suppliers) {
            foreach ($suppliers as $s) {
                $spl[$s->id] = $this->Comment->nameEnOrJp($s->name, $s->name_en, $locale);
            }
        }
        echo $this->Form->input('sp_name', [
            'type' => 'select',
            'class' => 'form-control',
            'options' => $spl,
            'empty' => ['' => __d('seller_brand', 'TXT_SELECT_SUPPLIER')],
            'value' => $this->request->query('sp_name') ? $this->request->query('sp_name') : '',
            'label' => false,
        ]);
        ?>
    </div>
    <div class="form-group custom-select">
        <?php
        $manuf = [];
        if ($manufacturers) {
            foreach ($manufacturers as $m) {
                $manuf[$m->id] = $this->Comment->nameEnOrJp($m->name, $m->name_en, $locale);
            }
        }
        echo $this->Form->input('mf_name', [
            'type' => 'select',
            'class' => 'form-control',
            'label' => false,
            'options' => $manuf,
            'empty' => ['' => __d('seller_brand', 'TXT_SELECT_MANUFACTURER')],
            'value' => $this->request->query('mf_name') ? $this->request->query('mf_name') : '',
        ]);
        ?>
    </div>
    <div class="form-group custom-select">
        <?php
        echo $this->Form->select('product_brand_id', [], [
            'class' => 'form-control',
            'label' => false,
            'empty' => ['' => __d('seller_brand', 'TXT_SELECT_PRODUCT_BRAND')],
        ]);
        ?>
    </div>
    <?php
    $this->SwitchStatus->render(); ?>
    <div class="form-group">
        <?php
        echo $this->Form->button(__('TXT_FIND'), [
            'class' => 'btn btn-primary btn-sm btn-width',
        ]); ?>
    </div>
    <?php
    echo $this->Form->end(); ?>
</div>
<div class="total-pagination" style="padding-bottom: 20px;">
    <?php echo $this->element('display_info'); ?>
</div>
<div class="row search-section" style="margin-bottom: 20px;">
    <?php echo $this->element('display_number'); ?>
    <div class="col-md-6 col-sm-6 text-center">
        <h2 class="title-page"><?php //echo __('TXT_SELLER_BRANDS'); ?></h2>
    </div>
    <?= $this->ActionButtons->btnRegisterNew('SellerBrands', []) ?>
    <div class="clearfix"></div>
</div>
<div class="data-tb-list">
    <?php if ($data): ?>
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>#</th>
                    <th>&nbsp;</th>
                    <th class="white-space">
                        <?php
                        echo __d('seller_brand', 'TXT_SUPPLIER'); ?>
                    </th>
                    <th class="white-space">
                        <?php
                        echo __d('seller_brand', 'TXT_OLD_CODE'); ?>
                    </th>
                    <th class="white-space">
                        <?php
                        echo __d('seller_brand', 'TXT_MANUFACTURER'); ?>
                    </th>
                    <th class="white-space">
                        <?php
                        echo $this->Paginator->sort('ProductBrands.' . $this->Comment->getFieldName(), __('SELLER_BRANDS_TXT_PRODUCT_BRAND')); ?>
                        <i class="fa fa-sort-desc" aria-hidden="true"></i>
                    </th>
                    <th class="white-space">
                        <?php
                        echo __d('seller_brand', 'TXT_PURCHASE_AVAILABILITY'); ?>
                    </th>
                    <th class="white-space">
                        <?php
                        echo __d('seller_brand', 'TXT_SALE_AVAILABILITY'); ?>
                    </th>
                    <th width="1%">&nbsp;</th>
                    <th width="1%">&nbsp;</th>
                    <th width="1%">&nbsp;</th>
                    <th width="1%">&nbsp;</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $_en = $this->request->session()->read('tb_field');
                $numbering = $this->Paginator->counter('{{start}}');
                foreach ($data as $item):
                    $total_products = count($item->seller_products);
                    $product = $item->seller_products ? $item->seller_products[0]->product : '';
                ?>
                <tr data-id="<?php echo h($item->id); ?>">
                    <th scope="row" class="td_evt">
                        <?php
                        echo $numbering; $numbering++; ?>
                    </th>
                    <td>
                        <?php
                        echo $this->ActionButtons->disabledText($item->is_suspend) ?>
                    </td>
                    <td>
                        <?php
                        if ($item->seller->type === TYPE_MANUFACTURER) {
                            echo $this->Comment->getFieldByLocal($item->seller->manufacturer, $_en);
                        } else {
                            echo $this->Comment->getFieldByLocal($item->seller->supplier, $_en);
                        }
                        ?>
                    </td>
                    <td>
                        <?php
                        echo $item->seller_products ? h($product->code) : ''; ?>
                    </td>
                    <td>
                        <?php
                        echo $this->Comment->getFieldByLocal($item->seller->manufacturer, $locale); ?>
                    </td>
                    <td>
                        <?php
                        echo $this->Comment->nameEnOrJp($item->product_brand->name, $item->product_brand->name_en, $_en); ?>
                    </td>
                    <td>
                        <?php
                        if ($item->seller_products) {
                            echo $product->purchase_flag ? __d('seller_brand', 'TXT_YES') : __d('seller_brand', 'TXT_NO');
                        }
                        ?>
                    </td>
                    <td>
                        <?php
                        if ($item->seller_products) {
                            echo $product->sell_flg ? __d('seller_brand', 'TXT_YES') : __d('seller_brand', 'TXT_NO');
                        }
                        ?>
                    </td>
                    <td>
                        <?php
                        $TXT_PRODUCT_NUMBER = '';
                        if ($total_products < 2) {
                            $TXT_PRODUCT_NUMBER = __('SELLER_BRANDS_TXT_PRODUCT_NUMBER_SINGULAR');
                        } else {
                            $TXT_PRODUCT_NUMBER = __('SELLER_BRANDS_TXT_PRODUCT_NUMBER_PLURAL');
                        }
                        echo $this->Html->link($total_products.' '.$TXT_PRODUCT_NUMBER, [
                            'controller' => 'seller-products',
                            'action' => 'index',
                            '?' => [
                                'seller_id' => $item->seller_id,
                                'product_brand_id' => $item->product_brand_id,
                                'seller_brand_id' => $item->id,
                            ]
                        ], [
                            'class' => 'btn btn-primary btn-sm btn-width',
                        ]);
                        ?>
                    </td>
                    <td>
                        <?php echo $this->Form->button('<i class="fa fa-comments-o" aria-hidden="true"></i>', [
                            'class' => 'btn btn-primary btn-sm btn_comment',
                            'data-id' => $item->id,
                            'escape' => false,
                        ]); ?>
                    </td>
                    <td data-target="<?php echo ($item->is_suspend) ? 1 : 0; ?>">
                        <?php
                        if ($item->is_suspend == 1) {
                            echo $this->Form->button(BTN_ICON_UNDO, [
                                'class' => 'btn btn-recover btn-sm btn_suspend', //btn_rc_ed
                                'data-name' => 'recover',
                                'escape' => false
                            ]);
                        } else {
                            echo $this->Html->link(BTN_ICON_EDIT, [
                                'controller' => 'sellerBrands',
                                'action' => 'edit', $item->id,
                                ], [
                                'class' => 'btn btn-primary btn-sm',//btn_rc_ed
                                'escape' => false
                            ]);
                        }
                        ?>
                    </td>
                    <td data-target="<?php echo ($item->is_suspend) ? 1 : 0; ?>">
                        <?php
                        if ($item->is_suspend == 1) {
                            echo $this->Form->button(BTN_ICON_DELETE, [
                                'class' => 'btn btn-delete btn-sm',
                                'id' => 'btn_delete',
                                'data-name' => 'delete',
                                'escape' => false
                            ]);
                        } else {
                            echo $this->Form->button(BTN_ICON_STOP, [
                                'class' => 'btn btn-suspend btn-sm btn_suspend',
                                'data-name' => 'suspend',
                                'escape' => false
                            ]);
                        }
                        ?>
                    </td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    <?php endif; ?>
</div>
<div class="row row-top-space" style="padding-bottom: 20px;">
    <?php echo $this->element('display_number'); ?>
    <?= $this->element('next_prev') ?>
    <div class="clearfix"></div>
</div>

<!--Modal Detail-->
<?php echo $this->element('Modal/view_detail'); ?>
<!--Modal Suspend-->
<?php echo $this->element('Modal/suspend'); ?>
<!--Modal delete-->
<?php echo $this->element('Modal/delete'); ?>
<!--Modal Comment-->
<?php echo $this->element('/Modal/message') ?>

<script>
    (function(e) {
        $('body').find('#mf-name, #sp-name').select2();

        //auto complete
        $('body').find('#brand-name').typeahead({
            minLength: 0,
            autoSelect: true,
            source:  function (query, process) {
                return $.get('<?= $this->Url->build('/product-brands/getBrandNameBySearch/') ?>', {
                        keyword: query
                    }, function (data) {
                        var optionData = [];
                        if (data.data !== null && data.data !== 'undefined') {
                            $.each(data.data, function(i, v) {
                                if (v.name !== '') {
                                    optionData.push(v.name);
                                }
                                if (v.name_en !== '') {
                                    optionData.push(v.name_en);
                                }
                            });
                        }
                        return process(optionData);
                    }, 'json');
            }
        });

        $('body').on('click', '.typeahead li', function(e) {
            $('body').find('.form-search').submit();
        });

        //list message
        var listUrl = '<?= $this->Url->build(['controller' => 'messages', 'action' => 'messageList']); ?>';
        var listUrlSearch = '<?= $this->Url->build(['controller' => 'messages', 'action' => 'messageListSearch']); ?>';
        var msgType = '<?= TYPE_SELLER_BRAND ?>';
        messageList(listUrl, msgType);

        //filter message list search to all
        $('body').on('click', '.btn-to-all', function() {
            $(this).removeClass('btn-detault').addClass('btn-primary');
            $('body').find('.btn-to-individual').removeClass('btn-primary').addClass('btn-default');
            var toAll = $('.to-all').val();
            if (parseInt(toAll) === 0) {
                $('.to-individual').val(0);
                $('.to-all').val(1);
            } else {
                $('.to-individual').val(1);
                $('.to-all').val(0);
            }
            messagesSearch(listUrlSearch);
        });

        //filter message list search to specific
        $('body').on('click', '.btn-to-individual', function() {
            $(this).removeClass('btn-detault').addClass('btn-primary');
            $('body').find('.btn-to-all').removeClass('btn-primary').addClass('btn-default');
            var toAll = $('.to-individual').val();
            if (parseInt(toAll) === 0) {
                $('.to-all').val(0);
                $('.to-individual').val(1);
            } else {
                $('.to-all').val(1);
                $('.to-individual').val(0);
            }
            messagesSearch(listUrlSearch);
        });

        $('body').on('click', '.btn-search', function() {
            messagesSearch(listUrlSearch);
        });

        $('body').on('click', '.btn-suspend-msg', function() {
            var id = $(this).data('id');
            var data = $('#form-message-status-' + id).serialize();
            var suspendUrl = '<?= $this->Url->build(['controller' => 'Messages', 'action' => 'suspend']) ?>';
            suspendMessage(id, suspendUrl, data);
        });

        $('body').on('click', '.data-tb-list .table>tbody>tr', function (e) {
            $.LoadingOverlay('show');
            var id = $(this).closest('tr').data('id');

            $.get('<?php echo $this->Url->build('/seller-brands/view/'); ?>' + id, function (data) {
                $.LoadingOverlay('hide');
                $('#modal_detail').find('.modal-body').html(data);
                $('#modal_detail').modal('show');
            }, 'html');
        });

        $('body').on('click', '.btn_suspend', function (e) {
            e.stopPropagation();
            var content = $('#modal_suspend');
            var status = $(this).attr('data-name');
            var txt = '';

            switch (status) {
                case 'suspend':
                    $('#btn_suspend_yes').text('<?= __('BTN_SUSPEND') ?>');
                    txt = '<?= __('MSG_CONFIRM_SUSPEND') ?>';
                    break;
                case 'delete':
                    txt = '<?= __('MSG_CONFIRM_DELETE') ?>';
                    $('#btn_suspend_yes').text('<?= __('BTN_DELETE') ?>');
                    break;
                 case 'recover':
                    $('#btn_suspend_yes').text('<?= __('BTN_RECOVER') ?>');
                    txt = '<?= __('MSG_CONFIRM_UNDO_SUSPEND') ?>';
                    break;
            };

            $(content).find('#btn_suspend_yes').data('id', $(this).closest('tr').data('id'));
            $(content).find('#btn_suspend_yes').data('target', $(this).closest('td').data('target'));
            $(content).find('.modal-body p').html(txt);
            $(content).modal('show');
        });

        $('body').on('click', '#btn_suspend_yes', function (e) {
            var params = {
                id: $(this).data('id'),
                is_suspend: $(this).data('target')
            };
            $.post('<?php echo $this->Url->build('/seller-brands/update-suspend/'); ?>', params, function (data) {
                if (data.message === 'success') {
                    location.reload();
                }
            }, 'json');
        });

        $('body').on('click', '#btn_delete', function (e) {
            e.stopPropagation();
            var content = $('#modal_delete');
            $(content).find('#btn_delete_yes').data('id', $(this).closest('tr').data('id'));
            $(content).modal('show');
        });

        $('body').on('click', '#btn_delete_yes', function (e) {
            $.post('<?php echo $this->Url->build('/seller-brands/delete/'); ?>', {id: $(this).data('id')}, function (data) {
                if (data.message === 'success') {
                    location.reload();
                }
            }, 'json');
        });

        $('body').on('click', '.table>tbody>tr>td a, .table>tbody>tr>td button', function(e) {
            e.stopPropagation();
        });

        pageOutOfRange(<?= $paging ?>, '<?= $this->request->query('page') ?>');
        var maxPage = parseInt($('#maxPage').val());
        updatePagination(maxPage);
    })();
</script>
