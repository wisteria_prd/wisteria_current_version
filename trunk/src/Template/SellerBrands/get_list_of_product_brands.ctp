<?php
if (isset($data)) :
    $index = 0;
    foreach ($data as $key => $value): ?>
    <div class="checkbox">
        <label>
            <?php
            echo $this->Form->checkbox('product_brand_id[' . $index . ']', [
                'hiddenField' => false,
                'class' => 'chk-brands',
                'value' => $value->id,
            ]);
            echo $this->Comment->getFieldByLocal($value, $locale);
            $index++;
            ?>
        </label>
    </div>
<?php endforeach; endif; ?>