
<?php
$_en = $this->request->session()->read('tb_field');
?>
<table class="table table-striped" id="tb-detail">
    <tbody>
        <tr>
            <td>
                <?php
                echo __d('seller_brand', 'TXT_SUPPLIER'); ?> :
            </td>
            <td colspan="2">
                <?php
                if ($data->seller) {
                    if ($data->seller->suplier) {
                        echo $this->Comment->getFieldByLocal($data->seller->suplier, $_en);
                    } else {
                        echo $this->Comment->getFieldByLocal($data->seller->manufacturer, $_en);
                    }
                }
                ?>
            </td>
        </tr>
        <tr>
            <td>
                <?php
                echo __d('seller_brand', 'TXT_OLD_CODE'); ?> :
            </td>
            <td colspan="2">
                <?php
                if ($data->seller_products) {
                    echo h($data->seller_products[0]->product->code);
                }
                ?>
            </td>
        </tr>
        <tr>
            <td>
                <?php
                echo __d('seller_brand', 'TXT_MANUFACTURER'); ?> :
            </td>
            <td colspan="2">
                <?php
                if ($data->seller->manufacturer) {
                    echo $this->Comment->getFieldByLocal($data->seller->manufacturer, $_en);
                }
                ?>
            </td>
        </tr>
        <tr>
            <td>
                <?php
                echo __d('seller_brand', 'TXT_PRODUCT_BRAND'); ?> :
            </td>
            <td colspan="2">
                <?php
                echo $this->Comment->getFieldByLocal($data->product_brand, $_en); ?>
            </td>
        </tr>
        <tr>
            <td>
                <?php
                echo __d('seller_brand', 'TXT_PURCHASE_AVAILABILITY'); ?> :
            </td>
            <td colspan="2">
                <?php
                if ($data->seller_products) {
                    $product = $data->seller_products[0]->product;
                    echo ($product->purchase_flag) ? __d('seller_brand', 'TXT_YES') : __d('seller_brand', 'TXT_NO');
                }
                ?>
            </td>
        </tr>
        <tr>
            <td>
                <?php
                echo __d('seller_brand', 'TXT_SALE_AVAILABILITY'); ?> :
            </td>
            <td colspan="2">
                <?php
                if ($data->seller_products) {
                    $product = $data->seller_products[0]->product;
                    echo ($product->sell_flag) ? __d('seller_brand', 'TXT_YES') : __d('seller_brand', 'TXT_NO');
                }
                ?>
            </td>
        </tr>
        <tr>
            <td>
                <?php
                echo __d('seller_brand', 'TXT_PRODUCT'); ?> :
            </td>
            <td colspan="2">
                <?php
                $name = __d('seller_brand', 'TXT_PRODUCT');
                $collection = new Cake\Collection\Collection($data->seller_products);
                $total = $collection->sumOf('product_id');
                if ($total > 1) {
                    $name = __d('seller_brand', 'TXT_PRODUCTS');
                }
                echo $this->Form->button($total . ' ' . $name, [
                    'type' => 'button',
                    'class' => 'btn btn-sm btn-primary',
                ]);
                ?>
            </td>
        </tr>
        <tr>
            <td>
                <?php
                echo __d('seller_brand', 'TXT_MODIFIED'); ?> :
            </td>
            <td colspan="2">
                <?php
                echo date('Y-m-d', strtotime($data->modified)); ?>
            </td>
        </tr>
        <tr>
            <td>
                <?php
                echo __d('seller_brand', 'TXT_REGISTERED'); ?> :
            </td>
            <td colspan="2">
                <?php
                echo date('Y-m-d', strtotime($data->created)); ?>
            </td>
        </tr>
    </tbody>
</table>
