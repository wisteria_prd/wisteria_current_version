
<div class="search-section" style="margin-bottom: 20px;">
    <?php
    echo $this->Form->create('CustomerGroups', [
        'role' => 'form',
        'class' => 'form-inline',
        'type' => 'get',
        'name' => 'form_search'
    ]);
    echo $this->Form->input('keyword', [
        'name' => 'keyword',
        'placeholder' => __('USER_SEARCH_KEYWORD'),
        'class' => 'form-control',
        'label' => false,
        'templates' => [
            'inputContainer' => '<div class"form-group">{{content}}</div>',
        ]
    ]);
    echo $this->Form->end();
    ?>
</div>
<div class="total-pagination" style="padding-bottom: 20px;">
    <?php echo $this->element('display_info'); ?>
</div>
<div class="row search-section" style="margin-bottom: 20px;">
    <?php echo $this->element('display_number'); ?>
    <div class="col-sm-5"><h2 class="text-center title-page"><?php echo $group->name; ?></h2></div>
    <div class="col-sm-4 no-padding" style="text-align: right;">
        <?php
        echo $this->Form->button(__('TXT_ADD_MEDICAL_CORPORATION'), [
            'type' => 'button',
            'class' => 'btn btn-sm btn-primary btn-add-clinic',
            'data-type' => TYPE_MEDICAL_CORP,
        ]);
        echo '&nbsp;&nbsp;';
        echo $this->Form->button(__('TXT_ADD_CLINIC'), [
            'type' => 'button',
            'class' => 'btn btn-sm btn-primary btn-add-clinic',
            'data-type' => TYPE_CUSTOMER,
        ]);
        if ($group->type === TYPE_CUSTOMER) {
            echo '&nbsp;&nbsp;';
            echo $this->Form->button(__('TXT_ADD_SUBSIDIARIES'), [
                'type' => 'button',
                'class' => 'btn btn-sm btn-primary btn-add-clinic',
                'data-type' => TYPE_SUBSIDIARY,
            ]);
        }
        ?>
    </div>
    <div class="clearfix"></div>
</div>
<div class="data-tb-list">
    <?php if ($data): ?>
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>#</th>
                    <th>&nbsp;</th>
                    <th>
                        <?php echo $this->Paginator->sort(($group->type === TYPE_CUSTOMER) ? 'Customers.code' : 'MedicalCorporations.code', __('TXT_OLD_CODE')); ?>
                        <span><i class="fa fa-sort-desc" aria-hidden="true"></i></span>
                    </th>
                    <th>
                        <?php echo $this->Paginator->sort(($group->type === TYPE_CUSTOMER) ? 'Customers.' . $this->Comment->getFieldName() : 'MedicalCorporations.' . $this->Comment->getFieldName(), __('TXT_NAME')); ?>
                        <span><i class="fa fa-sort-desc" aria-hidden="true"></i></span>
                    </th>
                    <th>
                        <?php echo $this->Paginator->sort('Customers.InfoDetails.email', __('TXT_EMAIL')); ?>
                        <span><i class="fa fa-sort-desc" aria-hidden="true"></i></span>
                    </th>
                    <th>
                        <?php echo $this->Paginator->sort('Users.lastname_en', __('TXT_RECEIVER')); ?>
                        <span><i class="fa fa-sort-desc" aria-hidden="true"></i></span>
                    </th>
                    <th>
                        <?php echo $this->Paginator->sort('PaymentCategories.name', __('TXT_PAYMENT')); ?>
                        <span><i class="fa fa-sort-desc" aria-hidden="true"></i></span>
                    </th>
                    <th>
                        <?php echo $this->Paginator->sort('InvoiceDeliveryMethods.name', __('TXT_INVOICE')); ?>
                        <span><i class="fa fa-sort-desc" aria-hidden="true"></i></span>
                    </th>
                    <th>&nbsp;</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $numbering = $this->Paginator->counter('{{start}}');
                foreach ($data as $item) :
                $associate = isset($item->customer) ? $item->customer : $item->medical_corporation;
                ?>
                <tr data-id="<?php echo h($item->id); ?>">
                    <th scope="row" class="td_evt"><?php echo $numbering; $numbering++; ?></th>
                    <td>
                        <?php if ($item->is_main) : ?>
                        <input type="radio" name="is_main" value="<?php echo $item->is_main?>" checked="check"/>
                        <?php else : ;?>
                        <input type="radio" name="is_main" value="<?php echo $item->is_main?>"/>
                        <?php endif; ?>
                    </td>
                    <td><?php echo h($associate->code); ?></td>
                    <td>
                        <?php
                        if ($this->Comment->getFieldName() === 'name_en') {
                            echo h($associate->name_en);
                        } else {
                            echo h($associate->name);
                        }
                        ?>
                    </td>
                    <td><?php echo ($associate->info_details) ? h($associate->info_details[0]->email) : null; ?></td>
                    <td>
                        <?php
                        if ($associate->user) {
                            if ($this->request->session()->read('tb_field') === '_en') {
                                echo h($associate->user->lastname_en) . ' ' . h($associate->user->firstname_en);
                            } else {
                                echo h($associate->user->lastname) . ' ' . h($associate->user->firstname);
                            }
                        }
                        ?>
                    </td>
                    <td><?php echo h($associate->payment_category->name); ?></td>
                    <td><?php echo h($associate->invoice_delivery_method->name); ?></td>
                    <td class="td-btn-wrap">
                        <?php
                        echo $this->Form->button(BTN_ICON_DELETE, [
                            'type' => 'button',
                            'class' => 'btn btn-delete btn-sm btn-remove',
                            'escape' => false
                        ]);
                        ?>
                    </td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    <?php endif; ?>
</div>
<div class="row row-top-space" style="padding-bottom: 20px;">
    <?php echo $this->element('display_number'); ?>
    <?= $this->element('next_prev') ?>
    <div class="clearfix"></div>
</div>

<!--Modal Create Group-->
<div class="modal fade" id="modal_clinic" data-backdrop="static" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title text-center"><?= __('TXT_ADD_MEDICAL_CORPORATION') ?></h4>
            </div>
            <div class="modal-body fixed-modal-height">
                <?php
                echo $this->Form->create(null, [
                    'role' => 'form',
                    'name' => 'form_search_clinic',
                    'style' => 'margin-bottom: 20px;',
                    'onsubmit' => 'return false;',
                ]);
                ?>
                <div class="row">
                    <div class="col-sm-8 field-no-padding-right">
                        <?php
                        echo $this->Form->input('clinic_search', [
                            'type' => 'text',
                            'label' => false,
                            'placeholder' => __('TXT_SEARCH'),
                            'class' => 'form-control col-sm-10',
                            'templates' => [
                                'inputContainer' => '<div class="form-group">{{content}}</div>'
                            ],
                        ]);
                        ?>
                    </div>
                    <div class="col-sm-4">
                        <?php
                        echo $this->Form->button(__('TXT_SEARCH'), [
                            'class' => 'btn btn-primary btn-width btn-clinic-search',
                        ]);
                        ?>
                    </div>
                </div>
                <?php
                echo $this->Form->end();
                ?>
                <table class="table table-striped custom-table-space">
                    <thead>
                        <tr>
                            <th></th>
                            <th>#</th>
                            <th><?= __('TXT_OLD_CODE') ?></th>
                            <th><?= __('TXT_NAME') ?></th>
                            <th><?= __('TXT_NAME_ENGLISH') ?></th>
                            <th><?= __('TXT_EMAIL') ?></th>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default btn-sm btn-width" data-dismiss="modal"><?= __('TXT_CANCEL') ?></button>
                <button type="button" class="btn btn-primary btn-sm btn-width btn-yes" disabled="disabled"><?= __('TXT_YES') ?></button>
            </div>
            <?php echo $this->Form->end(); ?>
        </div>
    </div>
</div>
<!--Modal Delete-->
<?php echo $this->element('Modal/delete'); ?>

<script>
    (function (e) {
        var clinicData = [];
        var maxPage = parseInt($('#maxPage').val());
        updatePagination(maxPage);

        $('body').on('change', 'select[name="displays"]', function(e) {
            $(this).closest('form').submit();
        });

        // show modal popup
        $('body').on('click', '.btn-add-clinic', function(e) {
            $.LoadingOverlay('show');
            var type = $(this).attr('data-type');
            get_clinic_list(type);
        });

        // add clinic to group by check
        $('body').on('click', '.btn-yes', function(e) {
            var checkbox = $('.modal').find('.table tr input:checkbox:checked');
            var data = [];
            $.each(checkbox, function(i, v) {
                var arr = {
                    'group_id': '<?php echo $group->id; ?>',
                    'external_id': $(this).closest('tr').attr('data-id'),
                    'name': $(this).closest('tr').attr('data-name'),
                    'name_en': $(this).closest('tr').attr('data-name-en'),
                    'external_type': $(this).closest('tr').attr('data-type')
                };
                data.push(arr);
            });
            $.post('<?php echo $this->Url->build('/customer-groups/save-data'); ?>', {data}, function(data) {
                if (data.status == 1) {
                    location.reload();
                }
            }, 'json');
        });

        // click on checkbox
        $('body').on('change', 'input[name="check-box"]', function(e) {
            var checkbox = $('.modal').find('.table tr input:checkbox:checked');
            if (checkbox.length > 0) {
                $('body').find('.btn-yes').removeAttr('disabled');
            } else {
                $('body').find('.btn-yes').attr('disabled', 'disabled');
            }
        });

        // filter clinic by code or name or name_en
        $('body').on('click', '.btn-clinic-search', function(e) {
            $.LoadingOverlay('show');
            var keyword = $('body').find('#clinic-search').val().toUpperCase();
            var filterd = $.grep(clinicData, function(v) {
                var result = '';
                if (v.name.toUpperCase() === keyword) {
                    result = v;
                } else if (v.code === keyword) {
                    result = v;
                } else if (v.name_en.toUpperCase() === keyword) {
                    result = v;
                } else {
                    var result1 = '';
                    result1 = v.name.toUpperCase().indexOf(keyword) >= 0;
                    if (result1) {
                        result = result1;
                    } else {
                        result = v.name_en.toUpperCase().indexOf(keyword) >= 0;
                    }
                }
                return result;
            });
            display_clinic_data(filterd);
            $.LoadingOverlay('hide');
        });

        // remove record when click on remove button
        $('body').on('click', '.btn-remove', function (e) {
            e.stopPropagation();
            var content = $('#modal_delete');
            $(content).find('#btn_delete_yes').attr('data-id', $(this).closest('tr').data('id'));
            $(content).modal('show');
        });

        // confirm message after click on remove button
        $('body').on('click', '#btn_delete_yes', function (e) {
            $.post('<?php echo $this->Url->build('/customer-groups/delete/'); ?>', {id: $(this).data('id')}, function (data) {
                if (data.message === 'success') {
                    location.reload();
                }
            }, 'json');
        });

        // change main clinic in table list
        $('body').on('click', 'input[type="radio"]', function(e) {
            var id = $(this).closest('tr').attr('data-id');
            $.post('<?php echo $this->Url->build('/customer-groups/update-is-main/'); ?>', {id: id}, function(data) {

            });
        });

        // get clinic list by type
        function get_clinic_list(type)
        {
            var params = {
                type: type,
                group_id: '<?php echo $group->id; ?>'
            };

            $.get('<?php echo $this->Url->build('/customer-groups/get-list/'); ?>', params, function(data) {
                var response = data.data;
                if (response.length > 0) {
                    clinicData = [];
                    $.each(response, function(i, v) {
                        clinicData.push(v);
                    });
                }
            }, 'json').done(function(data) {
                display_clinic_data(clinicData, type);
                $.LoadingOverlay('hide');
                var title = '<?= __('TXT_ADD_MEDICAL_CORPORATION'); ?>';
                if (params.type === '<?php echo TYPE_CUSTOMER; ?>') {
                    title = '<?= __('TXT_ADD_CLINIC'); ?>';
                } else if (params.type === '<?php echo TYPE_SUBSIDIARY; ?>') {
                    title = '<?= __('TXT_ADD_SUBSIDIARIES'); ?>';
                }
                $('#modal_clinic').find('.modal-title').html(title);
                $('#modal_clinic').modal('show');
            });
        }

        // display clinic data in table
        function display_clinic_data(data, type)
        {
            var element = '';
            var content = $('body').find('#modal_clinic');
            var table = $(content).find('.table tbody').empty();
            if (data !== null && data !== undefined) {
                $.each(data, function(i, v) {
                    var email = '';
                    if (v.info_details.length > 0) {
                        email = v.info_details[0].email;
                    }
                    element += '<tr data-type="' + type + '" data-id="' + v.id + '" data-name="' + v.name + '" data-name-en="' + v.name_en + '">'
                            + '<td><input type="checkbox" name="check-box"/></td>'
                            + '<td>' + (i + 1) +'</td>'
                            + '<td>' + v.code + '</td>'
                            + '<td>' + v.name + '</td>'
                            + '<td>' + v.name_en + '</td>'
                            + '<td>' + email + '</td>'
                            + '</tr>';
                });
                $(table).append(element);
            }
        }

        // clear data form modal after close
        $('.modal').on('hidden.bs.modal', function (e) {
            $('.modal').find('input[type="text"]').val('');
            $('.modal').find('tbody').html('');
            clinicData = [];
        });
    })();
</script>
