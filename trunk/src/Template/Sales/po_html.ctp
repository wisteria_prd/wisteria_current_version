<div class="page">
    <div class="page-content">
        <h2>Purchase Order</h2>
        <div class="container">
            <div class="row">
                <div class="col col-6">
                    <p>Issue Date 01/01/1970</p>
                    <p>Purchase Order No WOF0026</p>
                </div>
                <div class="col col-6">
                    <div class="text-center">
                        <?= $this->Html->image('wisteria_logo.png', ['class' => 'logo']) ?>
                    </div>
                    <h3>Wisteria Co., Ltd.</h3>
                    <p>4F, 1-4-6 Nihonbashi Kayaba-cho, Chuo-ku, Tokyo JAPAN 103-0025</p>
                    <p>Tel: +81-3-4588-1847 FAX: +81-3-4588-1848</p>
                </div>
            </div>
            <div class="spacer">&nbsp;</div>
            <div class="row">
                <div class="col col-6">
                    <p>To:</p>
                    <div class="addr-wrap">
                        <div class="spacer"></div>
                        <p>株式会社タスク</p>
                        <p>, 惣社町1485-11, 栃木市</p>
                        <p>栃木県, 328-0002</p>
                        <p>Tel: / Fax: 0282-27-1943</p>
                    </div>
                </div>
                <div class="col col-6">
                    <p>For: Doctor Name Dr.</p>
                    <div class="addr-wrap">
                        <p>Shipping Address:</p>
                        <div class="addr">
                            <p>TOMクリニック美容外科 東京新橋院</p>
                            <p>, 新橋3-7-3, 港区</p>
                            <p>東京都, 105-0004</p>
                            <p>Tel: / Fax: </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <p class="text-right">(US Dollar)</p>
        <table class="table table-data">
            <thead>
                <tr>
                    <th>No.</th>
                    <th>Description</th>
                    <th>Unit Price</th>
                    <th>Qty</th>
                    <th width="20%">Amount</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td class="text-center">1</td>
                    <td>Juvederm ULTRA2 0.55ml</td>
                    <td class="text-right">17,000.00</td>
                    <td class="text-right">2</td>
                    <td class="text-right">34,000.00</td>
                </tr>
                <tr>
                    <td class="text-center">2</td>
                    <td>Juvederm ULTRA2 0.55ml ( 34Test56, Exp: Dec-2020 ) x 1</td>
                    <td class="text-right">00.00</td>
                    <td class="text-right">0</td>
                    <td class="text-right">00.00</td>
                </tr>
                <tr>
                    <td class="text-center">3</td>
                    <td>&nbsp;</td>
                    <td class="text-right">&nbsp;</td>
                    <td class="text-right">&nbsp;</td>
                    <td class="text-right">&nbsp;</td>
                </tr>
                <tr>
                    <td class="text-center">4</td>
                    <td>&nbsp;</td>
                    <td class="text-right">&nbsp;</td>
                    <td class="text-right">&nbsp;</td>
                    <td class="text-right">&nbsp;</td>
                </tr>
                <tr>
                    <td class="text-center">5</td>
                    <td>&nbsp;</td>
                    <td class="text-right">&nbsp;</td>
                    <td class="text-right">&nbsp;</td>
                    <td class="text-right">&nbsp;</td>
                </tr>
                <tr>
                    <td class="text-center">6</td>
                    <td>&nbsp;</td>
                    <td class="text-right">&nbsp;</td>
                    <td class="text-right">&nbsp;</td>
                    <td class="text-right">&nbsp;</td>
                </tr>
                <tr>
                    <td class="text-center">7</td>
                    <td>&nbsp;</td>
                    <td class="text-right">&nbsp;</td>
                    <td class="text-right">&nbsp;</td>
                    <td class="text-right">&nbsp;</td>
                </tr>
                <tr>
                    <td class="text-center">8</td>
                    <td>&nbsp;</td>
                    <td class="text-right">&nbsp;</td>
                    <td class="text-right">&nbsp;</td>
                    <td class="text-right">&nbsp;</td>
                </tr>
                <tr>
                    <td class="text-center">9</td>
                    <td>&nbsp;</td>
                    <td class="text-right">&nbsp;</td>
                    <td class="text-right">&nbsp;</td>
                    <td class="text-right">&nbsp;</td>
                </tr>
                <tr>
                    <td class="text-center">10</td>
                    <td>&nbsp;</td>
                    <td class="text-right">&nbsp;</td>
                    <td class="text-right">&nbsp;</td>
                    <td class="text-right">&nbsp;</td>
                </tr>                
                <tr>
                    <td class="total-bold" colspan="4">Sub Total</td>
                    <td class="text-right total-bold">$0.00</td>
                </tr>
                <tr>
                    <td class="total-bold" colspan="4">&nbsp;</td>
                    <td class="text-right total-bold">&nbsp;</td>
                </tr>
                <tr>
                    <td class="total total-bold" colspan="4">Total</td>
                    <td class="total total-bold text-right">$0.00</td>
                </tr>
            </tbody>
        </table>
    </div>
</div>