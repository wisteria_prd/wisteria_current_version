<?php
ini_set('memory_limit', '-1');

$pdf = new TCPDF('', 'mm', 'A4', true, 'UTF-8');
// Reset the encoding forced from tcpdf
mb_internal_encoding('UTF-8');

$font_jp = TCPDF_FONTS::addTTFfont(WWW_ROOT.'fonts/ipag.ttf');

// language
if ($this->request->session()->read('tb_field') === 'en') {
    $language = 'Helvetica';
} else {
    $language = $font_jp;
}
$language = $font_jp;

ob_clean(); // cleaning the buffer before Output()

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Nick');
$pdf->SetTitle('Purchase Order Form');
$pdf->SetSubject('Purchase Order Form');
$pdf->SetKeywords('');

// Configure header and footer
$pdf->SetPrintHeader(false);
$pdf->SetPrintFooter(false);

// set margins
$pdf->SetMargins(13, 13, 13);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
    require_once(dirname(__FILE__).'/lang/eng.php');
    $pdf->setLanguageArray($l);
}
// set default font subsetting mode
$pdf->setFontSubsetting(true);
$pdf->SetFont('Helvetica', '', 8);

// page padding
$page_padding_top = 10;
$page_padding_left = 10;

$pdf->AddPage();

$pdf->SetFillColor(255,255,255);
// Header title of document
$pdf->SetFont($language, 'B', 20);
$pdf->MultiCell($width = 90, $height = 15, $text = 'Custom Invoice', $border = 0, $align = 'C', $fill=true, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = 15, $valign = 'M');
$pdf->ln();

// MediPro Logo
$arrContextOptions=array(
    'ssl' => array(
        'verify_peer' => false,
        'verify_peer_name' => false,
    ),
);
$path = $this->Url->build('/', true) .'img/medipro_logo.jpg';
$data = file_get_contents($path, false, stream_context_create($arrContextOptions));
$pdf->Image('@' . $data, 132, 13, 40, '', 'JPG', '', 'T', false, '', '', false, false, 0, false, false, false);
$pdf->ln();

$issueDate = '';
$invNumber = '';
if (!empty($saleDetails[0]['sale']['sale_tax_invoices'])) {
    $issueDate = $saleDetails[0]['sale']['sale_tax_invoices'][0]['issue_date'];
    $issueDate = $issueDate->i18nFormat('dd/MM/yyyy');
    $invNumber = $saleDetails[0]['sale']['sale_tax_invoices'][0]['tax_invoice_number'];
}
$pdf->SetFont($language, '', 11);
$date = '発行日           ' . $issueDate . "\n";
$date .= '支払依頼書番号   ' . $invNumber;
$pdf->MultiCell($width = 85, $height = 20, $text = $date, $border = 0, $align = 'L', $fill=true, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = 20, $valign = 'T');
$pdf->SetFont('helveticaB', 'B', 11);
$company_name = 'Medical Professionals Pte. Ltd.';
$pdf->MultiCell($width = 110, $height = '', $text = $company_name, $border = 0, $align = 'L', $fill=true, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = '', $valign = 'M');
$pdf->ln();

$pdf->SetFont($language, 'B', 9);
$address = '3791 Jalan Bukit Merah, #10-17 E-Centre@Redhill, Singapore 159471' . "\n";
$address .= 'Tel: +65-6274-0433 / Fax: +65-6274-0477' . "\n";
$address .= 'UEN No. 201403392G';
//$pdf->MultiCell($width = 85, $height = '', $text = '', $border = 0, $align = 'C', $fill=true, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = '', $valign = 'M');
$pdf->MultiCell($width = 110, $height = '', $text = $address, $border = 0, $align = 'L', $fill=true, $ln = 0, $x = 98, $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = '', $valign = 'M');
$pdf->ln();

$pdf->Cell(15, 6, '', 0, false, 'L');
$pdf->ln();

// Clinic customer
$pdf->Cell(10, 6, '', 0, false, 'L');
$pdf->SetFont($language, '', 9);
$customer = $saleDetails[0]['sale']['customer'];
$clinic = $customer['name'] . "\n";
$clinic .= $customer['building'] . ', ' . $customer['street'] . ', ' . $customer['city'];
$clinic .= $customer['prefecture'] . ', ' . $customer['country'] . ', ' . $customer['postal_code'];
$clinic .= "\n";
$clinic .= '電話：'.$customer['info_details'][0]['tel'].'  FAX：' . $customer['fax'];
$pdf->MultiCell($width = 75, $height = '', $text = $clinic, $border = 0, $align = 'L', $fill=true, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = '', $valign = 'B');
$dealer = '株式会社ウィステリア' . "\n";
$dealer .= '    〒103-0025東京都中央区日本橋茅場町1-4-6' . "\n";
$dealer .= '    木村實業第2ビル4F' . "\n";
$dealer .= '    電話：03-4588-1847  FAX：03-4588-1848' . "\n";
$dealer .= '＊＊株式会社ウィステリアは、お客様の輸入代行会社です＊＊';
$pdf->MultiCell($width = 105, $height = '', $text = '', $border = 0, $align = 'L', $fill=true, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = '', $valign = 'B');
$pdf->ln();
$pdf->ln();
//$pdf->ln();
$pdf->Cell($pdf->GetY(), 5, '', 0, false, 'L');
$pdf->ln();

$pdf->MultiCell($width = 190, $height = '', $text = '(日本円)', $border = 0, $align = 'R', $fill=true, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = '', $valign = 'M');
$pdf->ln();

// Products Details
$pdf->SetFillColor(120,120,120);
$pdf->SetLineStyle(array('width' => 0.1, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(115, 115, 115)));
$pdf->SetFont('Helvetica', 'B', 11);
$fill = '#F4F4F4';
$pdf->Cell(10, 10, 'No.', 1, false, 'C', $fill);
$pdf->SetFont($language, 'B', 11);
$pdf->Cell(105, 10, '詳細', 1, false, 'C', $fill);
$pdf->Cell(30, 10, '単価', 1, false, 'C', $fill);
$pdf->Cell(15, 10, '数', 1, false, 'C', $fill);
$pdf->Cell(30, 10, '金額', 1, false, 'C', $fill);
$pdf->ln();

$pdf->SetFillColor(255,255,255);
$pdf->SetFont($language, '', 9);
//$pdf->SetFont('Helvetica', '', 9);

// PDF Content
$numRow = 0;
$totalAmount = 0;
$totalPrice = 0;
$countRecords = count($saleDetails);
if ($countRecords <= 20) {
    // foreach saleDetails
    foreach ($saleDetails as $key => $value) {
        $numRow += 1;
        $childs = $value->children;
        $saleStockDetails = $value->sale_stock_details;
        $productDetailName = $value->pack_size_value . ' ' . $value->pack_size_name . ' X ' . $value->single_unit_value . ' ' . $value->single_unit_name;
        $productName = $value->product_name . ' ' . $productDetailName;

        $pdf->Cell(10, 6, $numRow, 1, false, 'C');
        $pdf->Cell(105, 6, $productName , 1, false, 'L');
        $pdf->Cell(30, 6, $this->Comment->numberFormat($value->unit_price) . ' ' . $sale->currency->code_jp, 1, false, 'R');
        $pdf->Cell(15, 6, $value->quantity, 1, false, 'R');
        $subTotal = $value->unit_price * $value->quantity;
        $totalPrice += $subTotal;
        $pdf->Cell(30, 6, $this->Comment->numberFormat($subTotal) . ' ' . $sale->currency->code_jp, 1, false, 'R');
        $pdf->ln();

        // display sale stock detail
        if ($saleStockDetails) {
            foreach ($saleStockDetails as $key1 => $value1) {
                $numRow += 1;
                $importDetail = '  ' . $value->product_name . ' ( ' . $productDetailName . ' ) ( ' . $value1->stock->import_detail->lot_number . ', Exp: ' .
                    date('M-Y', strtotime($value1->stock->import_detail->expiry_date)) . ' ) x ' . $value1->quantity;

                $pdf->Cell(10, 6, $numRow, 1, false, 'C');
                $pdf->Cell(105, 6, $importDetail , 1, false, 'L');
                $pdf->Cell(30, 6, '', 1, false, 'R');
                $pdf->Cell(15, 6, '', 1, false, 'R');
                $pdf->Cell(30, 6, '', 1, false, 'R');
                $pdf->ln();
            }
        }

        // display child
        if ($childs) {
            foreach ($childs as $key2 => $value2) {
                $numRow += 1;
                $saleStockDetails2 = $value2->sale_stock_details;
                $productDetailName2 = $value2->pack_size_value . ' ' . $value2->pack_size_name . ' X ' . $value2->single_unit_value . ' ' . $value2->single_unit_name;
                $productName2 = '    ' . $value2->product_name . ' ' . $productDetailName2;

                $pdf->Cell(10, 6, $numRow, 1, false, 'C');
                $pdf->Cell(105, 6, $productName2 , 1, false, 'L');
                $pdf->Cell(30, 6, '', 1, false, 'R');
                $pdf->Cell(15, 6, $value->quantity, 1, false, 'R');
                $subTotal = $value2->unit_price * $value2->quantity;
                $totalPrice += $subTotal;
                $pdf->Cell(30, 6, $this->Comment->numberFormat($subTotal) . ' ' . $sale->currency->code_jp, 1, false, 'R');
                $pdf->ln();
            }

            // display sale stock detail for child node
            if ($saleStockDetails) {
                foreach ($saleStockDetails as $key3 => $value3) {
                    $numRow += 1;
                    $importDetail = '      ' . $value2->product_name . ' ( ' . $productDetailName2 . ' ) ( ' . $value3->stock->import_detail->lot_number . ', Exp: ' .
                        date('M-Y', strtotime($value3->stock->import_detail->expiry_date)) . ' ) x ' . $value3->quantity;

                    $pdf->Cell(10, 6, $numRow, 1, false, 'C');
                    $pdf->Cell(105, 6, $importDetail , 1, false, 'L');
                    $pdf->Cell(30, 6, '', 1, false, 'R');
                    $pdf->Cell(15, 6, '', 1, false, 'R');
                    $pdf->Cell(30, 6, '', 1, false, 'R');
                    $pdf->ln();
                }
            }
        }
    }
}

while ($numRow < 10) {
    $pdf->Cell(10, 6, $numRow + 1, 1, false, 'C');
    $pdf->Cell(105, 6, '', 1, false, 'C');
    $pdf->Cell(30, 6, '', 1, false, 'C');
    $pdf->Cell(15, 6, '', 1, false, 'C');
    $pdf->Cell(30, 6, '', 1, false, 'C');
    $pdf->ln();
    $numRow++;
}

$pdf->SetFont($language, 'B', 11);
$pdf->MultiCell($width = 160, $height = '', $text = '　Sub Total', $border = 1, $align = 'L', $fill=true, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = '', $valign = 'M');
$pdf->MultiCell($width = 30, $height = '', $text = $this->Comment->numberFormat($totalPrice) . '円', $border = 1, $align = 'R', $fill=true, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = '', $valign = 'M');
$pdf->ln();
$pdf->MultiCell($width = 160, $height = '', $text = ' －', $border = 1, $align = 'L', $fill=true, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = '', $valign = 'M');
$colK = 0;
$pdf->MultiCell($width = 30, $height = '', $text = $this->Comment->numberFormat($colK) . '円', $border = 1, $align = 'R', $fill=true, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = '', $valign = 'M');
$pdf->ln();
$pdf->MultiCell($width = 160, $height = 15, $text = ' Total', $border = 1, $align = 'L', $fill=true, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = 15, $valign = 'M');

$pdf->MultiCell($width = 30, $height = 15, $text = $this->Comment->numberFormat($totalPrice) . '円', $border = 1, $align = 'R', $fill=true, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = 15, $valign = 'M');

$pdf->ln();
$pdf->Cell(100, 1, '', 0, false, 'C');
$pdf->ln();

$pdf->MultiCell($width = 90, $height = 35, $text = '備考：', $border = 0, $align = 'L', $fill = true, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = 35, $valign = 'T');

$pdf->MultiCell($width = 90, $height = 6, $text = 'お振込先口座：', $border = 0, $align = 'L', $fill=true, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = 6, $valign = 'T');
$pdf->ln();
$pdf->SetFont($language, 'B', 9);
$pdf->MultiCell($width = 100, $height = 8, $text = '   Medical Professionals Pte. Ltd.への送金代行用口座です。', $border = 0, $align = 'L', $fill=true, $ln = 0, $x = 103, $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = 6, $valign = 'T');
$pdf->ln();
$pdf->SetFont($language, 'B', 11);
$text = '  銀行名       三菱東京UFJ銀行' . "\n";
$text .= '  支店名       青山通支店（084）' . "\n";
$text .= '  口座番号     普通　0090906' . "\n";
$text .= '  口座名義     株式会社ウィステリア';
$pdf->MultiCell($width = 90, $height = '', $text = $text, $border = 0, $align = 'L', $fill=true, $ln = 0, $x = 103, $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = '', $valign = 'T');

$pdf->Output();