
<section class="modal-wrapper">
    <div class="modal fade" id="sale-modal" data-backdrop="static" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <?php
                    echo $this->Form->button('<span aria-hidden="true">&times;</span>', [
                        'type' => 'button',
                        'class' => 'close modal-close',
                        'data-dismiss' => 'modal',
                        'aria-label' => 'Close',
                        'escape' => false,
                    ]);
                    ?>
                    <h4 class="modal-title text-center">
                        <?php echo __('TXT_CREATE') ?>
                    </h4>
                </div>
                <div class="modal-body">
                    <?php
                    echo $this->Form->create(null, [
                        'class' => 'form-add-po form-horizontal',
                        'autocomplete' => 'off',
                        'name' => 'purchase_order',
                        'onsubmit' => 'return false;',
                    ]);
                    $this->Form->templates([
                        'inputContainer' => '{{content}}'
                    ]);
                    echo $this->Form->hidden('type', [
                        'id' => 'type',
                    ]);
                    ?>
                    <div class="form-group">
                        <label for="" class="control-label col-md-3">
                            <?php echo __('TXT_SELLER') ?>
                        </label>
                        <div class="col-md-7 custom-select">
                            <?php
                            echo $this->Form->select('seller_id', $sellers, [
                                'default' => isset($data) ? $data->seller_id : null,
                                'empty' => ($kind_of == TYPE_MP ? false : ['' => __('TXT_SELLER')]),
                                'class' => 'form-control',
                            ]);
                            ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="control-label col-md-3">
                            <?php echo __('TXT_CURRENCY') ?>
                        </label>
                        <div class="col-md-7 custom-select">
                            <?php
                            echo $this->Form->select('currency_id', $currencies, [
                                'default' => isset($data) ? $data->currency_id : null,
                                'class' => 'form-control',
                            ]);
                            ?>
                        </div>
                    </div>
                    <div class="form-group custom-select">
                        <label class="control-label col-md-3">
                            <?php echo __('TXT_TO_CUSTOMER') ?>
                        </label>
                        <div class="col-md-7">
                            <?php
                            $options = [];
                            if (isset($data)) {
                                $options = [$data->customer_id => $data->customer_name];
                            }
                            echo $this->Form->select('customer_id', $options, [
                                'class' => 'form-control customer-id',
                            ]);
                            ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">
                            <?= __('TXT_SALE_ORDER_DATE') ?>
                        </label>
                        <div class="col-md-7">
                            <div class="input-group">
                                <?php
                                echo $this->Form->text('order_date', [
                                    'class' => 'form-control order-date',
                                    'value' => isset($data) ? date('Y-m-d', strtotime($data->order_date)) : date('Y-m-d'),
                                ]);
                                ?>
                                <div class="input-group-btn">
                                    <?php
                                    echo $this->Form->button('<span class="glyphicon glyphicon-calendar"></span>', [
                                        'class' => 'btn btn-default',
                                        'aria-label' => 'Calendar',
                                        'id' => 'trigger-order-date',
                                        'escape' => false,
                                    ]);
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">
                            <?php echo __('TXT_ROP_ISSUE_DATE') ?>
                        </label>
                        <div class="col-md-7">
                            <div class="input-group">
                                <?php
                                echo $this->Form->text('rop_issue_date', [
                                    'class' => 'form-control',
                                    'id' => 'datepicker',
                                    'default' => isset($data) ? date('Y-m-d', strtotime($data->rop_issue_date)) : date('Y-m-d'),
                                ]);
                                ?>
                                <div class="input-group-btn">
                                    <?php
                                    echo $this->Form->button('<span class="glyphicon glyphicon-calendar"></span>', [
                                        'class' => 'btn btn-default',
                                        'aria-label' => 'Calendar',
                                        'id' => 'trigger-calendar',
                                        'escape' => false,
                                    ]);
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="control-label col-md-3">
                            <?php echo __('TXT_PREFER_SHIPMENT') ?>
                        </label>
                        <div class="col-sm-7">
                            <div class="checkbox">
                                <label>
                                    <?php echo $this->Form->checkbox('is_priority', ['hiddenField' => false]) ?>
                                </label>
                            </div>
                        </div>
                    </div>
                    <?php echo $this->Form->end(); ?>
                </div>
                <div class="modal-footer">
                    <?php
                    echo $this->Form->button(__('TXT_CANCEL'), [
                        'type' => 'button',
                        'class' => 'btn btn-default btn-sm btn-width modal-close',
                        'data-dismiss' => 'modal',
                        'aria-label' => 'Close',
                    ]);
                    echo $this->Form->button(__('TXT_REGISTER'), [
                        'type' => 'button',
                        'class' => 'btn btn-primary btn-sm btn-width btn-register',
                        'data-type' => 'register',
                    ]);
                    ?>
                </div>
            </div>
        </div>
    </div>
</section>
