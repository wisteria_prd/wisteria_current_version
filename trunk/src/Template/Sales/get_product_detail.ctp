
<?php if ($data) : foreach ($data as $key => $value) :
    $str = '';
    $suName = $this->Comment->getFieldByLocal($value->single_unit, $local);
    $psName = $this->Comment->getFieldByLocal($value->tb_pack_sizes, $local);

    // brand name
    if ($value->product->product_brand) {
        $str = $this->Comment->getFieldByLocal($value->product->product_brand, $local) . ' ';
    }
    // product name
    if ($value->product) {
        $str .= $this->Comment->getFieldByLocal($value->product, $local) . ' ';
    }
    // detail
    if (!empty($value->single_unit_size) && !empty($value->pack_size)) {
        if ($value->pack_size > 1) {
            $psName = $this->Pluralize->pluralize($psName);
        }
        $str .= '(' .
            $value->single_unit_size .
            $suName . ' x ' .
            $value->pack_size .
            $psName . ') ';
    } else if (empty($value->single_unit) && !empty($value->tb_pack_sizes)) {
        if ($value->pack_size > 1) {
            $psName = $this->Pluralize->pluralize($psName);
        }
        $str .= '(' .
            $value->pack_size .
            $psName . ') ';
    } else if (empty($value->tb_pack_sizes) && !empty($value->single_unit)) {
        if ($value->single_unit_size > 1) {
            $suName = $this->Pluralize->pluralize($suName);
        }
        $str .= '(' .
            $value->single_unit_size .
            $suName . ') ';
    }

    if (!empty($value->description)) {
        $str .= '(' . $value->description . ')';
    }
    ?>
        <tr data-id="<?= $value->id ?>" data-su-value="<?= $value->single_unit_size ?>" data-su-name="<?= $suName ?>" data-ps-value="<?= $value->pack_size ?>" data-ps-name="<?= $psName ?>" data-pack="<?= $value->packing_id ?>" data-dsc="<?= $value->description ?>">
            <td>
                <input type="checkbox" class="check-product">
            </td>
            <td><?= $key + 1 ?></td>
            <td><?= $str ?></td>
        </tr>
    <?php endforeach;
endif; ?>
