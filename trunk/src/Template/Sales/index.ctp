
<style>
    .d-status>span {
        display: flex;
    }
</style>
<?php
?>
<div class="search-section" style="margin-bottom: 20px;">
    <?php
    echo $this->Form->create('Sales', [
        'role' => 'form',
        'class' => 'form-inline form-search',
        'type' => 'get',
        'name' => 'form_search'
    ]);
    echo $this->Form->hidden('displays', [
        'value' => empty($this->request->query('displays')) ? 10 : $this->request->query('displays')
    ]);
    echo $this->Form->input('keyword', [
        'name' => 'keyword',
        'placeholder' => __('USER_SEARCH_KEYWORD'),
        'class' => 'form-control',
        'label' => false,
        'value' => $this->request->query('keyword'),
        'autocomplete' => 'off',
        'templates' => [
            'inputContainer' => '<div class"form-group">{{content}}</div>',
        ]
    ]);
    echo $this->Form->submit('submit', [
        'style' => 'display: none',
    ]);
    echo $this->Form->end();
    ?>
</div>
<div class="total-pagination" style="padding-bottom: 20px;">
    <?php echo $this->element('display_info'); ?>
</div>
<div class="row search-section" style="margin-bottom: 20px;">
    <?php echo $this->element('display_number'); ?>
    <?php if ($local !== LOCAL_EN) : ?>
    <div class="col-sm-6 pull-right" style="text-align: right;">
        <?php
        echo $this->Form->button(__('BTN_REGISTER'), [
            'type' => 'button',
            'class' => 'btn btn-sm btn-primary btn-width btn-register-po',
            'data-type' => 'register',
        ]);
        ?>
    </div>
    <?php endif; ?>
    <div class="clearfix"></div>
</div>
<div class="data-tb-list">
    <?php if ($data): ?>
    <table class="table table-striped table-pos">
        <thead>
            <tr>
                <th>#</th>
                <th>&nbsp;</th>
                <th class="white-space" style="white-space: nowrap;">
                    <?php echo $this->Paginator->sort('status', __('TXT_SALE_STATUS')); ?>
                    <i class="fa fa-sort-desc" aria-hidden="true"></i>
                </th>
                <th class="white-space" style="white-space: nowrap;">
                    <?php echo $this->Paginator->sort('order_date', __('TXT_SALE_ORDER_DATE')); ?>
                    <i class="fa fa-sort-desc" aria-hidden="true"></i>
                </th>
               <!-- <th class="white-space"> -->
                    <?php //echo $this->Paginator->sort('rop_number', __('TXT_ROP')); ?>
                    <!-- <i class="fa fa-sort-desc" aria-hidden="true"></i> -->
                <!-- </th> -->
                <th class="white-space" style="white-space: nowrap;">
                    <?php echo $this->Paginator->sort('sale_number', __('TXT_PO')); ?>
                    <i class="fa fa-sort-desc" aria-hidden="true"></i>
                </th>
                <th class="jp-max-character" style="white-space: nowrap;">
                    <?php echo $this->Paginator->sort('Customers.name' . $local, __('TXT_CLINIC_NAME')); ?>
                    <i class="fa fa-sort-desc" aria-hidden="true"></i>
                </th>
                <th class="white-space" style="white-space: nowrap;">
                    <?php echo $this->Paginator->sort('Sellers.id', __('TXT_SALE_SUPPLIER_NAME')); ?>
                    <i class="fa fa-sort-desc" aria-hidden="true"></i>
                </th>
                <th class="white-space" style="white-space: nowrap;"><?= __('TXT_AMOUNT') ?></th>
                <th colspan="5">&nbsp;</th>
            </tr>
        </thead>
        <tbody>
            <?php
            $numbering = $this->Paginator->counter('{{start}}');
            foreach ($data as $key => $value) :
            ?>
            <tr data-id="<?= $value->id ?>" data-customer-id="<?= $value->customer->id ?>">
                <th><?php echo $numbering; $numbering++; ?></th>
                <td style="white-space: nowrap;">
                    <?= $this->ActionButtons->disabledText($value->is_suspend) ?>
                </td>
                <td style="white-space: nowrap;" class="d-status">
                    <?php
                    echo '<span>' . __($this->Comment->textHumanize($value->status)) . '</span>';
                    // If have sub status display under fax_rop status
                    if (!empty($value->sub_status)) {
                        echo '<span> + ' . __($this->Comment->textHumanize($value->sub_status)) . '</span>';
                    }
                    ?>
                </td>
                <td>
                    <?php
                    if (!is_null($value->order_date)) {
                        echo date('m/d/Y', strtotime($value->order_date));
                    }
                    ?>
                </td>
                <!--<td><?php // echo $value->rop_number ?></td>-->
                <td style="white-space: nowrap;"><?= $value->sale_number; ?></td>
                <td style="white-space: nowrap;"><?= $this->Comment->getFieldByLocal($value->customer, $local); ?></td>
                <td style="white-space: nowrap;">
                    <?php
                    if ($value->seller->manufacturer) {
                        echo $this->Comment->nameEnOrJp($value->seller->manufacturer->nickname, $value->seller->manufacturer->nickname_en, $local);
                    } else {
                        echo $this->Comment->nameEnOrJp($value->seller->supplier->nickname, $value->seller->supplier->nickname_en, $local);
                    }
                    ?>
                </td>
                <td style="white-space: nowrap;">
                    <?php
                    $total = 0;
                    if ($value->purchase_details) {
                        $total = $value->purchase_details[0]->total_amount;
                    }
                    echo $this->Comment->currencyEnJpFormat($local, $total, $value->currency);
                    ?>
                </td>
                <td style="width: 1%;">
                    <?= $this->Html->link(__('TXT_ROP_EDIT'), [
                        'controller' => 'sales',
                        'action' => 'rop', $value->id,
                    ], [
                        'class' => 'btn btn-sm btn-primary',
                        'style' => 'margin-bottom: 5px;'
                    ]);
                    ?>
                    <?php
                    if ($value->status === PO_STATUS_DRAFF) {
                        echo $this->Form->button(__('TXT_BUTTON_FAX_ROP'), [
                            'type' => 'button',
                            'class' => 'btn btn-sm btn-primary btn-fax-rop',
                            'style' => 'width: 100%',
                        ]);
                    }
                    ?>
                </td>
                <td style="width: 1%;">
                    <?= $this->Html->link(__('TXT_PO_EDIT'), [
                        'controller' => 'sales',
                        'action' => 'po', $value->id
                    ], [
                        'class' => 'btn btn-sm btn-primary',
                        'style' => 'margin-bottom: 5px;'
                    ]);
                    ?>
                    <?= $this->Html->link(__('TXT_PO_PDF'), [
                        'controller' => 'sale-reports',
                        'action' => 'custom-clearence-pdf', $value->id,
                    ], [
                        'class' => 'btn btn-sm btn-primary',
                        'target' => '_blank',
                    ]);
                    ?>
                </td>
                <td style="width: 1%;">
                    <?= $this->Html->link(__('TXT_CC_EDIT'), [
                        'controller' => 'sales',
                        'action' => 'edit-doc', $value->id,
                    ], [
                        'class' => 'btn btn-primary btn-sm btn-edit-doc',
                        'style' => 'margin-bottom: 5px;'
                    ]);
                    ?>
                    <?= $this->Html->link(__('TXT_CC_PDF'), [
                        'controller' => 'sale-reports',
                        'action' => 'custom-clearence-pdf', $value->id,
                    ], [
                        'class' => 'btn btn-primary btn-sm',
                        'target' => '_blank',
                    ]);
                    ?>
                </td>
                <td style="white-space: nowrap;">
                    <?php echo $this->Form->button('<i class="fa fa-comments-o" aria-hidden="true"></i>', [
                        'class' => 'btn btn-primary btn-sm btn_comment',
                        'data-id' => $value->id,
                        'escape' => false,
                    ]); ?>
                </td>
                <td data-target="<?= $value->is_suspend ? 1 : 0; ?>" style="white-space: nowrap;">
                    <?php
                    if ($value->is_suspend == 1) {
                        echo $this->Form->button(BTN_ICON_UNDO, [
                            'class' => 'btn btn-recover btn-sm btn_suspend',
                            'data-name' => 'recover',
                            'escape' => false,
                            'style' => 'margin-bottom: 5px;'
                        ]);
                    }

                    if ($value->is_suspend == 1) {
                        echo $this->Form->button(BTN_ICON_DELETE, [
                            'class' => 'btn btn-delete btn-sm',
                            'id' => 'btn_delete',
                            'data-name' => 'delete',
                            'escape' => false
                        ]);
                    } else {
                        echo $this->Form->button(BTN_ICON_STOP, [
                            'class' => 'btn btn-suspend btn-sm btn_suspend',
                            'data-name' => 'suspend',
                            'escape' => false
                        ]);
                    }
                    ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <?php endif; ?>
</div>
<div class="row row-top-space" style="padding-bottom: 20px;">
    <?php echo $this->element('display_number'); ?>
    <?= $this->element('next_prev') ?>
    <div class="clearfix"></div>
</div>

<!--Modal Detail-->
<?= $this->element('Modal/view_detail'); ?>
<!--Modal Comment-->
<?= $this->element('/Modal/message') ?>
<!--Modal Suspend-->
<?= $this->element('Modal/suspend'); ?>
<!--Modal delete-->
<?= $this->element('Modal/delete'); ?>
<!--Modal edit pdf document-->
<?= $this->element('Modal/sale_edit_doc'); ?>

<?php
echo $this->Html->css([
    'bootstrap-datetimepicker.min',
]);
echo $this->Html->script([
    'sale',
], ['blog' => false]);
?>
<script>
    $(function(e) {
        //auto complete
        $('body').find('#keyword').typeahead({
            minLength: 0,
            autoSelect: true,
            source:  function (query, process) {
                return $.get('<?= $this->Url->build('/sales/getAllSaleBySearch/') ?>', {
                        keyword: query
                    }, function (data) {
                        var optionData = [];
                        if (data.data !== null && data.data !== 'undefined') {
                            $.each(data.data, function(i, v) {
                                optionData.push(v.rop_number);
                                optionData.push(v.sale_number);
                            });
                        }
                        return process(optionData);
                    }, 'json');
            }
        });

        $('body').on('click', '.typeahead li', function(e) {
            $('body').find('.form-search').submit();
        });

        $('body').on('click', '.btn-fax-rop', function(e) {
            $.LoadingOverlay('show');
            var id = $(this).closest('tr').attr('data-id');
            ajax_request_get('<?= $this->Url->build(['action' => 'getFaxRop']) ?>',
            { id: id },
            function(data) {
                if (data !== null && data !== 'undefined') {
                    $('body').append(data);
                    $('body').find('.fax-rop').modal('show');
                    $.LoadingOverlay('hide');
                }
            }, 'html');
        });

        $('.btn_comment').click(function (e) {
            e.stopPropagation();
            let params = {
                id: $(this).closest('tr').attr('data-id'),
                type: '<?php echo TRADE_TYPE_SALE_BEHALF_MP ?>',
                referer: '<?php echo $this->request->controller; ?>'
            };
            let options = {
                url: BASE + 'Messages/getMessageList',
                type: 'GET',
                data: params,
                beforeSend: function () {
                    $.LoadingOverlay('show');
                }
            };
            ajaxRequest(options, SESSION_TIMEOUT, function (data) {
                $('body').prepend(data);
                $('body').find('.modal-message').modal('show');
            });
        });

        //filter message list search to specific
        $('body').on('click', '.btn-to-individual', function() {
            $(this).removeClass('btn-detault').addClass('btn-primary');
            $('body').find('.btn-to-all').removeClass('btn-primary').addClass('btn-default');
            var toAll = $('.to-individual').val();
            if (parseInt(toAll) === 0) {
                $('.to-all').val(0);
                $('.to-individual').val(1);
            } else {
                $('.to-all').val(1);
                $('.to-individual').val(0);
            }
            messagesSearch(listUrlSearch);
        });

        $('body').on('click', '.btn-search', function() {
            messagesSearch(listUrlSearch);
        });

        $('body').on('click', '.btn-suspend-msg', function() {
            var id = $(this).data('id');
            var data = $('#form-message-status-' + id).serialize();
            var suspendUrl = '<?= $this->Url->build(['controller' => 'Messages', 'action' => 'suspend']) ?>';
            suspendMessage(id, suspendUrl, data);
        });

        $('body').on('click', '#trigger-calendar', function () {
            $('#datepicker').focus();
        });

        $('body').on('click', '#trigger-order-date', function () {
            $('#order-date').focus();
        });

        $('body').on('click', '.table>tbody>tr>td a', function(e) {
            e.stopPropagation();
        });

        $('body').on('click', '.btn-edit', function(e) {
            $.LoadingOverlay('show');
            var id = $(this).closest('tr').attr('data-id');
            var url = '<?= $this->Url->build(['action' => 'edit']); ?>' + '/' + id;
            var form = $('body').find('form[name="purchase_order"]');

            ajax_request_get(url, {kind_of: '<?= TYPE_MP ?>'}, function(data) {
                if (data !== null && data !== undefined) {
                    $('#modal-po-ms-to-s').find('.modal-body').html(data);
                    $('#modal-po-ms-to-s').modal('show');
                    $('#modal-po-ms-to-s').find('.btn-register').attr({
                        'data-type': 'edit',
                        'data-id': id
                    }).text('<?= __('TXT_UPDATE') ?>');
                    $('#seller-id, #currency-id, #type').select2({width: '100%'});
                    $('#datepicker').datetimepicker({format: 'YYYY-MM-DD'});
                }
            }, 'html', '<?= __('TXT_SESSION_TIMEOUT') ?>');
        });

        $('body').on('click', '.btn_suspend', function (e) {
            e.stopPropagation();
            var content = $('#modal_suspend');
            var status = $(this).attr('data-name');
            var txt = '';

            switch (status) {
                case 'suspend':
                    $('#btn_suspend_yes').text('<?= __('BTN_SUSPEND') ?>');
                    txt = '<?= __('MSG_CONFIRM_SUSPEND') ?>';
                    break;
                case 'delete':
                    txt = '<?= __('MSG_CONFIRM_DELETE') ?>';
                    $('#btn_suspend_yes').text('<?= __('BTN_DELETE') ?>');
                    break;
                 case 'recover':
                    $('#btn_suspend_yes').text('<?= __('BTN_RECOVER') ?>');
                    txt = '<?= __('MSG_CONFIRM_UNDO_SUSPEND') ?>';
                    break;
            };

            $(content).find('#btn_suspend_yes').data('id', $(this).closest('tr').data('id'));
            $(content).find('#btn_suspend_yes').data('target', $(this).closest('td').data('target'));
            $(content).find('.modal-body p').html(txt);
            $(content).modal('show');
        });

        $('body').on('click', '#btn_suspend_yes', function (e) {
            var params = {
                id: $(this).data('id'),
                is_suspend: $(this).data('target')
            };
            var url = '<?= $this->Url->build(['action' => 'update-suspend']); ?>';

            ajax_request_post(url, params, function(data) {
                if (data.message === 'success') {
                    location.reload();
                }
            }, '<?= __('TXT_SESSION_TIMEOUT') ?>');
        });

        $('body').on('click', '#btn_delete', function (e) {
            e.stopPropagation();
            var content = $('#modal_delete');

            $(content).find('#btn_delete_yes').attr('data-id', $(this).closest('tr').data('id'));
            $(content).modal('show');
        });

        $('body').on('click', '#btn_delete_yes', function (e) {
            var url = '<?= $this->Url->build(['action' => 'delete']); ?>';

            ajax_request_post(url, {id: $(this).data('id')}, function(data) {
                if (data.message === 'success') {
                    location.reload();
                }
            }, '<?= __('TXT_SESSION_TIMEOUT') ?>');
        });

        $('body').on('change', '.select-person-in-charge', function () {
            if ($(this).val() !== '') {
                var incharge = $(this).find('option:selected').text();
                $('.in-charge-name').val(incharge);
            }
        });

        $('.btn-edit-doc').on('click', function(e) {
            e.preventDefault();
            $.LoadingOverlay('show');
            var that = this;
            var customer_id = $(that).closest('tr').attr('data-customer-id');
            var sale_id = $(that).closest('tr').attr('data-id');

            ajax_request_get('<?= $this->Url->build(['action' => 'edit-doc']) ?>', {
                customer_id: customer_id,
                sale_id: sale_id
            }, function(data) {
                $('#edit-docs').find('.modal-body').append(data);
                $('#edit-docs').modal('show');
            });
        });

        $('#edit-docs').on('hidden.bs.modal', function () {
            var that = this;
            $(that).find('.modal-body').empty();
        });

        var maxPage = parseInt($('#maxPage').val());
        updatePagination(maxPage);
    });
</script>
