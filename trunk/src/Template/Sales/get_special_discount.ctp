
<?php
echo $this->Form->create($data, [
    'name' => 'form_special_discount',
    'role' => 'form',
    'class' => 'form-inline',
]);
echo $this->Form->hidden('id', ['value' => $data->id]);
echo $this->Form->hidden('digits', [
    'id' => 'digits',
    'value' => $currency->decimal_format
]);
?>
<div class="form-group no-margin">
    <div class="input-group">
        <div class="input-group-addon"><?= $currency->code ?></div>
        <?php
        echo $this->Form->input('special_discount', [
            'type' => 'text',
            'class' => 'form-control',
            'label' => false,
            'placeholder' => __('TXT_SPECIAL_DISCOUNT'),
            'value' => $data->special_discount ? $data->special_discount : 0,
            'templates' => [
                'inputContainer' => '{{content}}'
            ],
        ]);
        ?>
    </div>
</div>
<?php echo $this->Form->end(); ?>

<script>
    (function(e) {
        var digits = $('body').find('#digits').val();

        $('#special-discount').inputmask('decimal', {
            'alias': 'numeric',
            'groupSeparator': ',',
            'autoGroup': true,
            'digits': digits,
            'radixPoint': '.',
            'digitsOptional': false,
            'allowMinus': false,
            'prefix': '',
            'placeholder': convertNumToZero(digits)
        });

        $('body').on('click', '.btn-special-discount', function(e) {
            $.LoadingOverlay('show');
            var form = $('body').find('form[name="form_special_discount"]');
            var url = '<?= $this->Url->build(['action' => 'updateSpecialDiscount']) ?>';

            ajax_request_post(url, $(form).serialize(), function(data) {
                $(form).find('.error-message').remove();

                if (data.message === '<?= MSG_ERROR ?>') {
                    var message = '<label class="error-message">' + data.data + '</labe>';
                    $(form).find('input[type="text"]').closest('.form-group').append(message);
                } else {
                    location.reload();
                }
            }, '<?= __('TXT_SESSION_TIMEOUT') ?>');
        });

        function convertNumToZero(value)
        {
            var str = '';
            if (value > 0) {
                str += '0.';
                for(i = 0; i < value; i ++) {
                    str += '0';
                }
            }
            return str;
        }
    })();
</script>