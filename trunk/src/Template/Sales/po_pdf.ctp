<?php
ini_set('memory_limit', '-1');

// create new PDF document
// $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
$pdf = new TCPDF('', 'mm', 'A4', true, 'UTF-8');
// Reset the encoding forced from tcpdf
mb_internal_encoding('UTF-8');

//$font_jp = TCPDF_FONTS::addTTFfont(WWW_ROOT.'fonts/ipag.ttf');
$font_jp = TCPDF_FONTS::addTTFfont(WWW_ROOT.'fonts/meiryo-01.ttf');

// language
if ($en === '_en') {
    $language = 'Helvetica';
} else {
    $language = $font_jp;
}
// test language
$language = $font_jp;

ob_clean(); // cleaning the buffer before Output()

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Nick');
$pdf->SetTitle('Purchase Order Form');
$pdf->SetSubject('Purchase Order Form');
$pdf->SetKeywords('');

// Configure header and footer
$pdf->SetPrintHeader(false);
$pdf->SetPrintFooter(false);

// set margins
$pdf->SetMargins(13, 13, 13);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
    require_once(dirname(__FILE__).'/lang/eng.php');
    $pdf->setLanguageArray($l);
}
// set default font subsetting mode
$pdf->setFontSubsetting(true);
$pdf->SetFont('Helvetica', '', 8);

// page padding
$page_padding_top = 10;
$page_padding_left = 10;

$pdf->AddPage();

$pdf->SetFillColor(255,255,255);
// Header title of document
$pdf->SetFont('Helvetica', 'B', 20);
$pdf->MultiCell($width = 80, $height = 15, 'Purchase Order', $border = 0, $align = 'C', $fill = true, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = 15, $valign = 'M');
$pdf->ln();

// wisteria Logo
$arrContextOptions=array(
    'ssl' => array(
        'verify_peer' => false,
        'verify_peer_name' => false,
    ),
);
$path = $this->Url->build('/', true) .'img/wisteria_logo.png';
$data = file_get_contents($path, false, stream_context_create($arrContextOptions));
$pdf->Image('@' . $data, 132, 13, 15, '', 'PNG', '', 'T', false, 600, '', false, false, 0, false, false, false);
$pdf->ln();

//$name = 'name' . $en;
$name = 'name';

$pdf->SetFont('Helvetica', '', 11);
$date = 'Issue Date           ' . date('d/m/Y', strtotime($sale->issue_date)) . "\n";
$date .= 'Purchase Order No   ' . $sale->sale_number;
$pdf->MultiCell($width = 85, $height = 20, $date, $border = 0, $align = 'L', $fill=true, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = 20, $valign = 'T');
$pdf->SetFont('helveticaB', 'B', 11);
$company_name = 'Wisteria Co., Ltd.';
$pdf->MultiCell($width = 110, $height = '', $company_name, $border = 0, $align = 'L', $fill=true, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = '', $valign = 'M');
$pdf->ln();

$pdf->SetFont('Helvetica', '', 9);
$address = '4F, 1-4-6 Nihonbashi Kayaba-cho, Chuo-ku, Tokyo JAPAN 103-0025' . "\n";
$address .= 'Tel: +81-3-4588-1847  FAX: +81-3-4588-1848' . "\n";
$pdf->MultiCell($width = 110, $height = '', $address, $border = 0, $align = 'L', $fill=true, $ln = 0, $x = 98, $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = '', $valign = 'M');
$pdf->ln();
$pdf->ln();

$pdf->SetFont($language, '', 9);
$pdf->MultiCell($width = 8, $height = '', 'To:', $border = 0, $align = 'L', $fill=true, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = '', $valign = 'B');
$pdf->SetFont($language, 'B', 10);
$pdf->MultiCell($width = 78, $height = '', $sale->doctor_name, $border = 0, $align = 'L', $fill=true, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = '', $valign = 'B');

$pdf->SetFont($language, '', 9);
$pdf->MultiCell($width = 10, $height = '', 'For:', $border = 0, $align = 'L', $fill=true, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = '', $valign = 'B');
$pdf->SetFont($language, 'B', 10);
$doctor_name = 'full_name' . $en;
$doctor = '';

if (!empty($sale->customer->clinic_doctors)) {
    $clinic_doctor = $sale->customer->clinic_doctors[0];
    $doctor = $clinic_doctor->$doctor_name;

    if ($doctor == '') {
        $doctor = $sale->customer->clinic_doctors[0]->doctor->$doctor_name;
    }
}

$pdf->MultiCell($width = 90, $height = '', 'Doctor Name Dr. '. $doctor, $border = 0, $align = 'L', $fill=true, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = '', $valign = 'B');

$pdf->ln();
$pdf->MultiCell($width = 96, $height = '', '', $border = 0, $align = 'L', $fill=true, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = '', $valign = 'B');
$dealer = 'Shipping Address:';
$pdf->MultiCell($width = 50, $height = '', $dealer, $border = 0, $align = 'L', $fill=true, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = '', $valign = 'B');
$pdf->ln();
$pdf->setCellMargins(8, 0, 0, 0);
$supplier_name = '';
$ph = '';
$fax = '';
$supplier_addr = '';
if ($sale->seller->type == TYPE_MANUFACTURER) {
    if ($sale->seller->manufacturer->info_details) {
        if (!empty($sale->seller->manufacturer->info_details[0]->tel)) {
            $ph = $sale->seller->manufacturer->info_details[0]->tel;
        } else {
            $ph = $sale->seller->manufacturer->info_details[0]->phone;
        }
    }
    $supplier_name = $sale->seller->manufacturer->$name;
    $supplier_addr = $sale->seller->manufacturer->building . ', ';
    $supplier_addr .= $sale->seller->manufacturer->street . ', ';
    $supplier_addr .= $sale->seller->manufacturer->city. "\n";
    $supplier_addr .= $sale->seller->manufacturer->prefecture . ', ';
    $supplier_addr .= $sale->seller->manufacturer->country . ' ';
    $supplier_addr .= $sale->seller->manufacturer->postal_code;
    $fax = $sale->seller->manufacturer->fax;
} else {
    if ($sale->seller->supplier->info_details) {
        if (!empty($sale->seller->supplier->info_details[0]->tel)) {
            $ph = $sale->seller->supplier->info_details[0]->tel;
        } else {
            $ph = $sale->seller->supplier->info_details[0]->phone;
        }
    }
    $supplier_name = $sale->seller->supplier->$name;
    $supplier_addr = $sale->seller->supplier->building . ', ';
    $supplier_addr .= $sale->seller->supplier->street . ', ';
    $supplier_addr .= $sale->seller->supplier->city. "\n";
    $supplier_addr .= $sale->seller->supplier->prefecture . ', ';
    $supplier_addr .= $sale->seller->supplier->country . ' ';
    $supplier_addr .= $sale->seller->supplier->postal_code;
    $fax = $sale->seller->supplier->fax;
}
$customer_name = $sale->customer->$name;
$pdf->MultiCell($width = 82, $height = '', $supplier_name, $border = 0, $align = 'L', $fill=true, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = '', $valign = 'B');
$pdf->MultiCell($width = 80, $height = '', $customer_name, $border = 0, $align = 'L', $fill=true, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = '', $valign = 'B');
$pdf->ln();
$pdf->SetFont($language, '', 9);
$pdf->MultiCell($width = 82, $height = '', $supplier_addr, $border = 0, $align = 'L', $fill=true, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = '', $valign = 'B');

$customer_addr = $sale->customer->building . ', ';
$customer_addr .= $sale->customer->street . ', ';
$customer_addr .= $sale->customer->city. "\n";
$customer_addr .= $sale->customer->prefecture . ', ';
$customer_addr .= $sale->customer->country . ' ';
$customer_addr .= $sale->customer->postal_code;

$pdf->MultiCell($width = 80, $height = '', $customer_addr, $border = 0, $align = 'L', $fill=true, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = '', $valign = 'B');
$pdf->ln();

$supplier_contact = 'Tel: ' . $ph;
$supplier_contact .= ' / Fax: ' . $fax;
$pdf->MultiCell($width = 82, $height = '', $supplier_contact, $border = 0, $align = 'L', $fill=true, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = '', $valign = 'B');
$phone = '';
if ($sale->customer->info_details) {
    if (!empty($sale->customer->info_details[0]->tel)) {
        $phone = $sale->customer->info_details[0]->tel;
    } else {
        $phone = $sale->customer->info_details[0]->phone;
    }
}
$customer_contact = 'Tel: ' . $phone;
$customer_contact .= ' / Fax: ' . $sale->customer->fax;
$pdf->MultiCell($width = 80, $height = '', $customer_contact, $border = 0, $align = 'L', $fill=true, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = '', $valign = 'B');
$pdf->ln();
$pdf->ln();

$pdf->setCellMargins(0, 0, 0, 0);

$pdf->MultiCell($width = 190, $height = '', '(US Dollar)', $border = 0, $align = 'R', $fill=true, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = '', $valign = 'M');
$pdf->ln();

//$pdf->SetFillColor(120,120,120);
$pdf->SetLineStyle(array('width' => 0.1, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(115, 115, 115)));
$pdf->SetFont('Helvetica', '', 11);
$fill = '#F4F4F4';
$pdf->Cell(10, 10, 'No.', 1, false, 'C', $fill);
$pdf->Cell(105, 10, 'Description', 1, false, 'C', $fill);
$pdf->Cell(30, 10, 'Unit Price', 1, false, 'C', $fill);
$pdf->Cell(15, 10, 'Qty', 1, false, 'C', $fill);
$pdf->Cell(30, 10, 'Amount', 1, false, 'C', $fill);
$pdf->ln();

$sub_total = 0;
$total = 0;
if ($sale->sale_details) {
    $count = count($sale->sale_details);
    for ($i = 0; $i < $count; $i++) {
        $amount = $sale->sale_details[$i]->quantity * $sale->sale_details[$i]->unit_price;
        $total += $amount;
        $sub_total += $amount;
        $pdf->Cell(10, 6, $i+1, 1, false, 'C');
        $detail = $sale->sale_details[$i]->product_name;
        $detail .= ' ( ';
        $detail .= $sale->sale_details[$i]->single_unit_value;
        $detail .= $sale->sale_details[$i]->single_unit_name;
        $detail .= ' x ';
        $detail .= $sale->sale_details[$i]->pack_size_value;
        $detail .= $sale->sale_details[$i]->pack_size_name;
        $detail .= ' )';
        $pdf->Cell(105, 6, $detail, 1, false, 'L');
        $pdf->Cell(30, 6, '$' . $this->Comment->numberFormat($sale->sale_details[$i]->unit_price), 1, false, 'R');
        $pdf->Cell(15, 6, $sale->sale_details[$i]->quantity, 1, false, 'R');
        $pdf->Cell(30, 6, '$' . $this->Comment->numberFormat($amount), 1, false, 'R');
        $pdf->ln();
        if ($i == 9) {
            break;
        }
    }

    if ($count < 10) {
        $blank = 10 - $count;
        for ($i = 0; $i < $blank; $i++) {
            $pdf->Cell(10, 6, $count+$i+1, 1, false, 'C');
            $pdf->Cell(105, 6, '', 1, false, 'L');
            $pdf->Cell(30, 6, '', 1, false, 'R');
            $pdf->Cell(15, 6, '', 1, false, 'R');
            $pdf->Cell(30, 6, '', 1, false, 'R');
            $pdf->ln();
        }
    }
} else {
    for ($i = 0; $i < 10; $i++) {
        $pdf->Cell(10, 6, $i+1, 1, false, 'C');
        $pdf->Cell(105, 6, '-', 1, false, 'L');
        $pdf->Cell(30, 6, '-', 1, false, 'R');
        $pdf->Cell(15, 6, '-', 1, false, 'R');
        $pdf->Cell(30, 6, '-', 1, false, 'R');
        $pdf->ln();
    }
}
$pdf->SetFont('Helvetica', '', 1);
//$pdf->Cell(190, 0, '', 1, false, 'L');
//$pdf->ln();
$pdf->SetFont('Helvetica', 'B', 11);
$pdf->Cell(160, 6, 'Sub Total', 1, false, 'L');
$pdf->Cell(30, 6, '$' . $this->Comment->numberFormat($sub_total), 1, false, 'R');
$pdf->ln();
$pdf->Cell(160, 6, '-', 1, false, 'L');
$pdf->Cell(30, 6, '', 1, false, 'R');
$pdf->ln();
$pdf->SetFont('Helvetica', '', 1);
//$pdf->Cell(190, 0, '', 1, false, 'L');
//$pdf->ln();
$pdf->SetFont('Helvetica', 'B', 11);
$pdf->Cell(160, 15, 'Total', 1, false, 'L');
$pdf->Cell(30, 15, '$' . $this->Comment->numberFormat($total), 1, false, 'R');
$pdf->ln();

$pdf->Ln(25);
$pdf->Cell(160, 6, 'Remarks:', 0, false, 'L');
$pdf->Ln();
$pdf->SetFont('Helvetica', '', 9);
$pdf->setCellMargins(8, 0, 0, 0);
$pdf->MultiCell($width = 190, $height = '', '', $border = 0, $align = 'L', $fill=false, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = '', $valign = 'M');
$pdf->ln();

$pdf->Output();
