
<?php
echo $this->Form->create($saleCustomClearance, [
    'class' => 'frm-edit-doc-pdf form-horizontal',
    'autocomplete' => 'off',
    'onsubmit' => 'return false;',
]);
$this->Form->templates([
    'inputContainer' => '{{content}}'
]);
if ($saleCustomClearance) {
    echo $this->Form->hidden('id', ['value' => $saleCustomClearance->id]);
}
echo $this->Form->hidden('sale_id', ['value' => $saleId, 'class' => 'sale-id']);

// check doctors list
$doctorList[''] = '経理ご担当者';
if ($clinicDoctors) {
    unset($doctorList);
    $doctorList = [];
    foreach ($clinicDoctors as $key => $value) {
        $doctor = $value->doctor;
        $name = $doctor->last_name . ' ' . $doctor->first_name;
        $doctorList[$doctor->id] = $name;
    }
} else {
    echo $this->Form->hidden('doctor_name', ['value' => '経理ご担当者']);
}
?>
<div class="form-group">
    <label for="" class="control-label col-md-4"><?= __('TXT_CONTACT') ?></label>
    <div class="col-md-7 custom-select">
        <?php
        echo $this->Form->input('doctor_id', [
            'type' => 'select',
            'label' => false,
            'required' => false,
            'class' => 'form-control',
            'options' => $doctorList,
        ]);
        ?>
    </div>
</div>
<div class="form-group">
    <label for="" class="control-label col-md-4"><?= __('TXT_IMPORT_DATE') . $this->Comment->formAsterisk() ?></label>
    <div class="col-md-7">
        <div class="input-group">
            <?=
            $this->Form->input('import_date', [
                'type' => 'text',
                'class' => 'form-control',
                'label' => false,
                'required' => false,
                'id' => 'imp-d-datepicker',
                'value' => isset($saleCustomClearance) ? date('Y-m-d', strtotime($saleCustomClearance->import_date)) : date('Y-m-d'),
            ]);
            ?>
            <div class="input-group-btn">
                <button type="button" class="btn btn-default" aria-label="Calendar" id="imp-d-trigger-calendar">
                    <span class="glyphicon glyphicon-calendar"></span>
                </button>
            </div>
        </div>
        <span class="help-block help-tips" id="error-import_date"></span>
    </div>
</div>
<div class="form-group">
    <label for="" class="control-label col-md-4"><?= __('TXT_STORAGE_LOCATION') . $this->Comment->formAsterisk() ?></label>
    <div class="col-md-7 custom-select">
        <?php
        $storage_locations = [
            'TNT通関サービス株式会社東京ロジスティクスセンター　保税蔵置所（１BWD9）' => 'TNT通関サービス株式会社東京ロジスティクスセンター　保税蔵置所（１BWD9）',
            '新砂 FEHH/W FedEx 保税蔵置所（1BWD4）' => '新砂 FEHH/W FedEx 保税蔵置所（1BWD4）',
            '関西空港 JALKAS H/W' => '関西空港 JALKAS H/W',
            '関西空港 FedEx 保税蔵置場' => '関西空港 FedEx 保税蔵置場',
            '新木場 UPS 保税蔵置場所（1BWD7）' => '新木場 UPS 保税蔵置場所（1BWD7）',
            '株式会社阪急阪神エクスプレス 関西国際空港 保税蔵置場' => '株式会社阪急阪神エクスプレス 関西国際空港 保税蔵置場',
        ];
        echo $this->Form->input('storage_location', [
            'type' => 'select',
            'class' => 'form-control storage-location',
            'label' => false,
            'required' => false,
            'options' => $storage_locations,
            'value' => isset($saleCustomClearance) ? $saleCustomClearance->storage_location : null,
        ]);
        ?>
    </div>
</div>
<div class="form-group">
    <label for="" class="control-label col-md-4"><?= __('TXT_MINISTER') . $this->Comment->formAsterisk() ?></label>
    <div class="col-md-7 custom-select">
        <?php
        echo $this->Form->input('minister', [
            'class' => 'form-control txt-minister',
            'label' => false,
            'readonly' => true,
            'required' => false,
            'value' => isset($saleCustomClearance) ? $saleCustomClearance->minister : '厚生労働省関東信越厚生局',
        ]);
        ?>
    </div>
</div>
<?php echo $this->Form->end(); ?>

<script>
    $(document).ready(function(){
        $('#imp-d-datepicker').datetimepicker({format: 'YYYY-MM-DD'});
        $('body').on('click', '#imp-d-trigger-calendar', function () {
            $('#imp-d-datepicker').focus();
        });

        $('body').on('change', '.storage-location', function(e) {
            var that = this;
            var minister = '';
            switch($(that).val()) {
                case 'TNT通関サービス株式会社東京ロジスティクスセンター　保税蔵置所（１BWD9）':
                    minister = '厚生労働省関東信越厚生局';
                    break;
                case '新砂 FEHH/W FedEx 保税蔵置所（1BWD4）':
                    minister = '厚生労働省関東信越厚生局';
                break;
                case '関西空港 JALKAS H/W':
                    minister = '厚生労働省近畿厚生局';
                    break;
                case '関西空港 FedEx 保税蔵置場':
                    minister = '厚生労働省近畿厚生局';
                    break;
                case '新木場 UPS 保税蔵置場所（1BWD7）':
                    minister = '厚生労働省関東信越厚生局';
                    break;
                case '株式会社阪急阪神エクスプレス 関西国際空港 保税蔵置場':
                    minister = '厚生労働省近畿厚生局';
                    break;
                default:
                    minister = '';
            }
            $('.txt-minister').text(minister);
            $('.txt-minister').val(minister);
        });

        $('body').on('click', '.btn-submit-doc-pdf', function () {
            $('.help-block').empty().hide();
            $.LoadingOverlay('show');
            var target = $('.target-type').val();
            var url = '<?= $this->Url->build('/sale-custom-clearance-docs/createAndUpdate') ?>';
            var form = $('body').find('.frm-edit-doc-pdf');

            ajax_request_post(url, $(form).serialize(), function (response) {
                if (response.message === '<?= MSG_ERROR ?>') {
                    displayError(form, response.data);
                } else {
                    $.LoadingOverlay('hide');
                    $('body').find('#edit-docs').modal('hide');
                }
            }, '<?= __('TXT_SESSION_TIMEOUT') ?>');
        });
    });
</script>