
<style>
    .label-non-pack {
        padding: 3px 5px;
        font-size: 12px;
        border-radius: 2px;
        width: 60px;
        text-align: center;
        color: #fff;
        cursor: pointer;
    }
</style>

<?php
$this->assign('title', 'PO Normal');
$userGroup = $this->request->session()->read('user_group');

//Total Amount
$totalAmount = 0;
?>
<div class="row">
    <div class="col-md-6">
        <h1><?= __('TXT_PO_FORM') ?></h1>
        <p><strong><?= __('TXT_ORDER_FORM_NUMBER') ?></strong>&nbsp;<?= $sale->sale_number ?></p>
        <p class="txt-issue-date">
            <strong><?= __('TXT_ISSUE_DATE') ?>:</strong>&nbsp;
            <span><?php
            if ($sale->issue_date) {
                echo date('Y年m月d日 ', strtotime($sale->issue_date));
            }
            ?></span>
        </p>
    </div>
    <?php
        $supplier_name = '';
        if ($sale->seller->manufacturer) {
            $id = $sale->seller->manufacturer->id;
            list($addr, $jp_addr2, $en_addr2, $tel, $supplier_fax) = $this->Comment->addressFormat($local, $sale->seller->manufacturer);
            $supplier_name = $this->Comment->nameEnOrJp($sale->seller->manufacturer->name, $sale->seller->manufacturer->name_en, $local);
        } else {
            $id = $sale->seller->supplier->id;
            list($addr, $jp_addr2, $en_addr2, $tel, $supplier_fax) = $this->Comment->addressFormat($local, $sale->seller->supplier);
            $supplier_name = $this->Comment->nameEnOrJp($sale->seller->supplier->name, $sale->seller->supplier->name_en, $local);
        }
        echo $this->element('rop_po_address', [
            'addr' => $addr,
            'jp_addr2' => $jp_addr2,
            'en_addr2' =>  $en_addr2,
            'tel' => $tel,
            'top_fax' => $supplier_fax,
            'is_medipro' => $this->request->param('controller'),
            'supplier_name' => $supplier_name,
            'local' => $local
        ]);
    ?>
</div>
<div class="row">
    <div class="col-md-12">
        <hr class="hr-invoiced">
    </div>
</div>
<?php
$params = [
    'c_name' => $sale->customer->name,
    'c_name_en' => $sale->customer->name_en,
    'local' => $local,
    'address' => ($sale->customer_address == '') ? '-' : $sale->customer_address,
    'phone' => ($sale->customer_tel == '') ? '-' : $sale->customer_tel,
    'fax' => ($sale->customer_fax == '') ? '-' : $sale->customer_fax,
    'email' => ($sale->customer_email == '') ? '-' : $sale->customer_email
];
echo $this->element('po_customer_info', $params);

//doctor info
echo $this->element('po_doctor_info', ['doctor_name' => ($sale->doctor_name == '') ? '-' : $sale->doctor_name]);
?>

<div class="row">
    <div class="col-md-12">
        <table class="table table-striped table-bordered table-po">
            <thead>
                <tr>
                    <th><?= __('No') ?></th>
                    <th style="white-space: nowrap;" width="35%"><?= __('TXT_DESCRIPTION') ?></th>
                    <th style="white-space: nowrap;"><?= __('TXT_UNIT_PRICE') ?></th>
                    <th style="white-space: nowrap;"><?= __('TXT_QUANTITY') ?></th>
                    <th style="white-space: nowrap;" colspan="2"><?= __('TXT_AMOUNT') ?></th>
                </tr>
            </thead>
            <tbody>
                <?php
                if ($saleDetail) {
                    $num = 0;
                    for ($i=0; $i<(20-$countRecords); $i++) {
                        $num += 1;
                        if (isset($saleDetail[$i])) {
                            $item = $saleDetail[$i];
                            $amount3 = intval($item->unit_price) * intval($item->quantity);
                            $totalAmount += $amount3;
                            $packClass3 = 'label-success';
                            if ($item->sale_stock_details) {
                                $packClass3 = 'label-danger';
                            }

                            // display parent record
                            echo '<tr data-id="' . $item->id . '" data-type="parent" data-target="edit">' .
                            '<td class="text-center">' . $num . '</td>' .
                            '<td>'
                            . $this->Comment->POProductDetailName($item) .
                            '<label class="' . $packClass3 . ' label-pack no-margin pull-right">' . __('TXT_BUTTON_PACK') . '</label>' .
                            '</td>' .
                            '<td style="white-space: nowrap;">' . $this->Comment->currencyEnJpFormatS($local, $item->unit_price, $sale->currency->code) . '</td>' .
                            '<td>' . $item->quantity . '</td>' .
                            '<td style="white-space: nowrap;" colspan="2">' . $this->Comment->currencyEnJpFormatS($local, $amount3, $sale->currency->code) . '</td></tr>';
                        } else {
                            echo '<tr data-target="' . CUSTOMER_TYPE_NEW . '" data-id="" data-type="">' .
                            '<td class="text-center">' . $num . '</td>' .
                            '<td>&nbsp;</td>' .
                            '<td>&nbsp;</td>' .
                            '<td>&nbsp;</td>' .
                            '<td colspan="2"></td>' .
                            '</tr>';
                        }
                    }
                } else {
                    for ($i=1; $i<=20; $i++) {
                        echo '<tr data-target="new" data-id="" data-type="">' .
                            '<td>' . $i . '</td>' .
                            '<td>&nbsp;</td>' .
                            '<td>&nbsp;</td>' .
                            '<td>&nbsp;</td>' .
                            '<td colspan="2">&nbsp;</td>' .
                            '</tr>';
                    }
                }
                ?>

                <!-- Table Footer -->
                <tr class="table-po-footer">
                    <td colspan="2" style="vertical-align: top;"><b>Remark :</b></td>
                    <td colspan="2" class="table-text-right"><b><?= __('TXT_TOTAL') ?> :</b></td>
                    <td colspan="2" style="border: 1px solid #ddd !important;">
                        <?php
                        echo $this->Comment->currencyEnJpFormatS($local, $totalAmount, $sale->currency->code);
                        ?>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
<div class="row">
    <div class="col-md-8 col-sm-8">
        <div class="row">
            <?php if (!empty($local)) : ?>
            <div class="col-md-8">
                <p>Medical Professionals Singapore Pte. Ltd. Peter Lim, Managing Director</p>
                <p class="company-invoice-signature">Digital Signature</p>
                <hr class="signature-line">
            </div>
            <?php endif; ?>
            <div class="col-md-4">
                <p>Date:</p>
                <div class="input-group with-datepicker">
                    <div class="input text"><input type="text" name="date" class="form-control datepicker" id="date"></div>            <div class="input-group-btn">
                        <button type="button" class="btn btn-default trigger-calendar" aria-label="Calendar">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </button>
                    </div>
                </div>
                <p></p>
                <hr class="signature-line">
            </div>
        </div>
    </div>
    <div class="col-md-4 col-sm-4">
        <?php
        // CHECK USER ACOUNT BASE ON LOCAL
        if ($local === LOCAL_EN) {
            echo $this->element('po_status_normal_en');
        } else {
            echo $this->element('po_status_normal');
        }
        ?>
    </div>
</div>

<!--Modal Add New SaleStockDetails-->
<div class="modal fade modal-pack" role="dialog" data-backdrop="static">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Add Sale Stock Detail</h4>
            </div>
            <div class="modal-body modal-body-fix"></div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default btn-sm btn-width close-modal" data-dismiss="modal"><?= __('BTN_CANCEL') ?></button>
                <button type="button" class="btn btn-primary btn-sm btn-width btn-pack-submit"><?= __('TXT_REGISTER') ?></button>
            </div>
        </div>
    </div>
</div>
<!--Modal Show Update Sub-status Success-->
<div class="modal fade" id="modal-sub-status" role="dialog" data-backdrop="static">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-body"><?= __('TXT_UPDATE_SUB_STATUS') ?></div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary btn-sm btn-width close-modal" data-dismiss="modal"><?= __('TXT_YES') ?></button>
            </div>
        </div>
    </div>
</div>
<!--Modal Upload PDF File-->
<div class="modal fade" id="upload-pdf" role="dialog" data-backdrop="static">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"><?= __('TXT_DOCUMENT_UPLOAD') ?></h4>
            </div>
            <div class="modal-body">
                <?php
                echo $this->Form->create(null, [
                    'onsubmit' => 'return false;',
                    'role' => 'form',
                    'class' => 'form-horizontal',
                    'type' => 'file',
                    'name' => 'pdf-form',
                ]);
                $this->Form->templates([
                    'inputContainer' => '<div class="col-sm-8">{{content}}</div>',
                ]);
                echo $this->Form->hidden('seller_id', ['value' => $sale->id]);
                ?>
                <div class="form-group">
                    <label class="col-sm-4 control-label"><?= __('TXT_TYPE') ?></label>
                    <?php
                    echo $this->Form->input('document_type', [
                        'type' => 'select',
                        'class' => 'form-control',
                        'label' => false,
                        'required' => false,
                        'options' => $typePdf,
                        'id' => false,
                    ]);
                    ?>
                </div>
                <div class="form-group">
                    <label class="col-sm-4 control-label"><?= __('TXT_PDF_FILE') ?></label>
                    <?php
                    echo $this->Form->input('file', [
                        'type' => 'file',
                        'class' => 'form-control',
                        'label' => false,
                        'required' => false,
                        'options' => $typePdf,
                    ]);
                    ?>
                </div>
                <?php echo $this->Form->end(); ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default btn-sm btn-width close-modal" data-dismiss="modal"><?= __('BTN_CANCEL') ?></button>
                <button type="button" class="btn btn-primary btn-sm btn-width btn-upload-file"><?= __('TXT_DOCUMENT_UPLOAD') ?></button>
            </div>
        </div>
    </div>
</div>

<?php
echo $this->Html->css([
    'bootstrap-datetimepicker.min',
    'jquery-ui',
    'intlTelInput',
]);
?>

<script>
    $(function() {
        var local = '<?= $local ?>';
        var optionData = [];

        $('.datepicker, #delivery-date').datetimepicker({
            format: 'YYYY-MM-DD'
        });

        $('.trigger-calendar').click(function() {
            $(this).parents('.with-datepicker').find('.datepicker').focus();
        });

        $('body').on('click', '.btn-update-delivery', function(e) {
            $.LoadingOverlay('show');
            var url = '<?= $this->Url->build('/deliveries/create/') ?>';
            var form = $('body').find('form[name="sale_status"]');
            var user_group = JSON.parse('<?= json_encode($userGroup) ?>');
            var params = {
                tracking: $(form).find('#tracking').val(),
                delivery_date: $(form).find('#delivery-date').val(),
                sale_id: '<?= $sale->id ?>',
                affiliation_class: user_group.affiliation_class,
                id: $(form).find('#delivery-id').val()
            };

            ajax_request_post(url, params, function(data) {
                $(form).find('.error-message').remove();

                if (data.message === '<?= MSG_ERROR ?>') {
                    $.each(data.data, function(i, v) {
                        var msg = '<label class="error-message">' + v + '</label>';
                        $(form).find('input[name="' + i + '"]').closest('.col-sm-6').append(msg);
                    });
                } else {
                    $('body').find('.btn-register').removeAttr('disabled', true);
                }
            }, '<?= __('TXT_SESSION_TIMEOUT') ?>');
        });

        $('body').on('click', '.browse-file-edit', function(e) {
            var modal = $('body').find('#upload-pdf');
            var params = {
                document_type: $(this).closest('li').attr('data-document-type'),
                name: $(this).closest('li').attr('data-name'),
                type: $(this).closest('li').attr('data-type')
            };
            var fileName = '<h4 class="file-name text-center"><span class="label label-success">' + params.name + '</span></h4>';

            $(modal).find('select[name="document_type"]').val(params.document_type);
            $(modal).find('form').append(fileName);
            $(modal).modal('show');
        });

        $('#upload-pdf').on('hidden.bs.modal', function (e) {
            $(this).find('select[name="document_type"]').val('');
            $(this).find('.file-name').remove();
        });

        $('body').on('click', '.custom-wrap li .browse-file', function(e) {
            var index = $(this).closest('li').index();

            $('body').find('#upload-pdf').modal('show');
            $('body').find('#upload-pdf .btn-upload-file').attr('data-index', index);
        });

        $('body').on('click', '.rm-pdf-file', function(e) {
            e.stopPropagation();
            var li = $(this).closest('.custom-wrap li').index();

            if (li == 0) {
                $(this).closest('li').find('span:first').remove();
            } else {
                $(this).closest('li').remove();
            }
            optionData = $.grep(optionData, function(value, index) {
                return value.index != li;
            });
        });

        $('body').on('click', '.add-pdf-file', function(e) {
            e.stopPropagation();
            var content = $(this).closest('.custom-wrap');
            var li = '<li data-type="new"><span class="label label-success browse-file"><i class="fa fa-times rm-pdf-file" aria-hidden="true"></i>&nbsp;&nbsp;Browse File</span><span>&nbsp;</span></li>';
            $(content).append(li);
        });

        $('body').on('click', '.btn-upload-file', function(e) {
            var form = $('body').find('form[name="pdf-form"]');
            var index = $(this).attr('data-index');
            var arr = {
                file: $(form).find('#file')[0].files[0],
                external_id: $(form).find('input[name="seller_id"]').val(),
                document_type: $(form).find('select[name="document_type"]').val(),
                index: index
            };
            optionData.push(arr);
            $('body').find('#upload-pdf').modal('hide');
        });

        $('body').on('click', '.label-pack', function(e) {
            e.stopPropagation();
            $.LoadingOverlay('show');
            var url = '<?= $this->Url->build('/sale-stock-details/get-data/') ?>';
            var params = {
                id: $(this).closest('tr').attr('data-id')
            };

            ajax_request_get(url, params, function(data) {
                var content = $('body').find('.modal-pack');
                $(content).find('.modal-body').html(data);
                $(content).modal('show');
                $(content).find('select[name="stock_id[0]"]').select2({width: '100%'});
                var status = $(content).find('input[name="sale_status"]').val();

                if ((status === '<?= PO_STATUS_CUSTOM_CLEARANCE ?>') || (status === '<?= PO_STATUS_DELIVERED ?>') || (status === '<?= PO_STATUS_REQUESTED_CC ?>')) {
                    $(content).find('.btn-pack-submit').attr('disabled', true);
                }
            }, 'html', '<?= __('TXT_SESSION_TIMEOUT') ?>');
        });

        $('body').on('click', '.btn-stock-rm', function(e) {
            $(this).closest('.form-group').remove();
        });

        $('body').on('click', '.btn-stock-add', function(e) {
            var content = $('body').find('.modal-pack .main-wrap');
            var index = $(content).find('.form-group').length + 1;
            var stock_id = $('body').find('.modal-pack select[name="stock_id[0]"]').html();
            var element = '<div class="form-group">'
                        + '<label for="" class="col-md-4"></label>'
                        + '<div class="col-md-3 custom-select no-padding-r">'
                        + '<select class="form-control" id="stock-id-' + index + '" name="stock_id[' + index + ']">' + stock_id + '</select></div>'
                        + '<div class="col-md-3 no-padding-r"><input type="number" class="form-control" name="quantity[' + index + ']" placeholder="<?= __('TXT_ENTER_QUANTITY') ?>"/></div>'
                        + '<div class="col-md-2"><button class="btn btn-delete btn-stock-rm"><i class="fa fa-trash" aria-hidden="true"></i></button></div>'
                        + '</div>';

            $(content).append(element);
            $(content).find('#stock-id-' + index).select2({width: '100%'});
        });

        $('body').on('click', '.btn-pack-submit', function(e) {
            $.LoadingOverlay('show');
            var form = $('body').find('.modal-pack form');
            var url = '<?= $this->Url->build('/sale-stock-details/create/') ?>';

            ajax_request_post(url, $(form).serialize(), function(data) {
                var response = data.data;
                $('body').find('#warning-msg').empty();
                $(form).find('.error-message').remove();

                if (data.message === '<?= MSG_ERROR ?>') {
                    var stock_id = response.stock_id;
                    var quantity = response.quantity;

                    // error stock_id
                    $.each(stock_id, function(i, v) {
                        var label = '<label class="error-message">' + v + '</label>';
                        $(form).find('select[name="stock_id[' + i + ']"]').closest('div').append(label);
                    });

                    // error quantity
                    $.each(quantity, function(i, v) {
                        var label = '<label class="error-message">' + v + '</label>';
                        $(form).find('input[name="quantity[' + i + ']"]').closest('div').append(label);
                    });
                } else if (data.message === '<?= MSG_WARNING ?>') {
                    $('body').find('.modal-pack #warning-msg').html('<strong>' + '<?= __('TXT_INVALID_LOT_QUANTITY') ?>' + '</strong>');
                } else {
                    location.reload();
                }
            }, '<?= __('TXT_SESSION_TIMEOUT') ?>')
        });

        $('body').on('click', '.btn-register', function() {
            $.LoadingOverlay('show');
            var form = $('body').find('form[name="sale_status"]');
            var affiliationClass = '<?= $userGroup->affiliation_class ?>';
            var url = '<?= $this->Url->build(['action' => 'updateStatus']) ?>';
            var step = parseInt($(this).attr('data-step')) + 1;

            $(form).find('input[name="step"]').val(step);
            $(form).find('button[type="submit"]').attr('data-step', step);

            ajax_request_post(url, $(form).serialize(), function(data) {
                if (data.message === '<?= MSG_SUCCESS ?>') {
                    renderStep(form, data);
                }
            }, '<?= __('TXT_SESSION_TIMEOUT') ?>');
        });

        $('body').on('click', '.update-sub-status', function(e) {
            $.LoadingOverlay('show');
            var params = {
                subStatus: $('body').find('form[name="sale_status"] #sub-status').val(),
                id: '<?= $sale->id ?>'
            };
            var url = '<?= $this->Url->build(['action' => 'updateSubStatus']) ?>';

            ajax_request_post(url, params, function(data) {
                if (data.message === '<?= MSG_SUCCESS ?>') {
                    $('body').find('#modal-sub-status').modal('show');
                }
            }, '<?= __('TXT_SESSION_TIMEOUT') ?>');
        });

        /**
        * Function add_option
        * use for add option to select dropdown
        * @param object data
        * @returns return string
        */
        function add_option(data)
        {
           var element = '';

           if (data !== null && data !== undefined) {
               $.each(data, function(i, v) {
                   element += '<option value="' + v.key + '">' + v.value + '</option>';
               });
           }

           return element;
        }

        /**
         * use for render html to po footer
         * @param {html} form
         * @param {object} data
         * @returns {string}
         */
        function renderStep(form, data)
        {
            switch (parseInt(data.data.step)) {
                // FAX_ROP STATUS
                case 2 :
                    var issueDate = $(form).find('input[name="issue_date"]').val();
                    $('body').find('.txt-issue-date span').empty().html(dateFormat(issueDate));
                    // CHECK AFFILIATION CLASS
                    if (local !== '<?= LOCAL_EN ?>') {
                        $(form).find('.label-draft').text('<?= __('TXT_BUTTON_FAX_ROP') ?>');
                        $(form).find('.btn-register').text('<?= __('TXT_SUBMIT') ?>');
                        $(form).find('#status option:first').remove();
                        $(form).find('#status').prepend('<option selected="selected" value="<?= STATUS_PAID ?>"><?= __('TXT_PAID') ?></option>');
                        $(form).find('.status-wrap').empty().html(element);
                    }
                    break;
                // PAID STATUS
                case 3 :
                    $(form).find('.label-draft').text('<?= __('TXT_PAID') ?>');
                    // CHECK STATUS PAID AND SUB STATUS
                    if ((data.data.status === '<?= STATUS_PAID ?>') && (data.data.subStatus !== '<?= STATUS_SHIPPED ?>')) {
                        $(form).find('.form-group:first label:last').text('<?= __('TXT_PAYMENT_INFO') ?>');
                        $(form).find('#status').remove();
                        var payment = JSON.parse('<?= json_encode($payments) ?>');
                        // CHECK PAYMENT IS NOT EMPTY
                        if (payment !== null && payment !== 'undefined') {
                            var plist = '<ul class="no-padding payment-list">' +
                                '<li><span class="label label-success">' + payment[0] + '</span></li></ul>';
                            $(form).find('.form-group:eq(1) div:last').html(plist);
                            $(form).find('.btn-register').hide();
                        }
                    } else {
                        var next = (data.data.step !== 8) ? '<?=  __('TXT_SELECT_NEXT_STATUS') ?>' : '';
                        $(form).find('.form-group:first label:last').text(next);
                        $(form).find('.btn-register').text('<?= __('TXT_SEND_PO') ?>');
                        $(form).find('#status').empty().append('<option selected="selected" value="<?= PO_STATUS_REQUESTED_SHIPPING ?>"><?= __('TXT_REQUESTED_SHIPMENT') ?></option>');
                    }
                    break;
                // TXT_REQUESTED_SHIPMENT STATUS
                case 4 :
                    $(form).find('.label-draft').text('<?= __('TXT_REQUESTED_SHIPMENT') ?>');
                    $(form).find('.btn-register').text('<?= __('TXT_SUBMIT') ?>');
                    $(form).find('#status').empty().append('<option selected="selected" value="<?= PO_STATUS_CUSTOM_CLEARANCE ?>"><?= __('TXT_CUSTOM_CLEARANCE') ?></option>');
                    break;
                // APPLIED_CUSTOM_CLEARANCE STATUS
                case 5 :
                    var date_time = new Date();
                    var currentDate = date_time.getFullYear() + '-' + (date_time.getMonth()+1) + '-' + date_time.getDate();
                    var saleDelivery = JSON.parse('<?= json_encode($saleDeliveries) ?>');
                    var arr1 = {
                        tracking: '',
                        delivery_date: currentDate
                    };

                    if (saleDelivery) {
                        arr1.tracking = saleDelivery.delivery.tracking;
                        arr1.delivery_date = saleDelivery.delivery.delivery_date;
                        $(form).find('.status-wrap').append('<input type="hidden" name="delivery-id"/>')
                    }

                    var element1 = '<div class="form-group">' +
                            '<label class="col-sm-6 control-label text-left"><?= __('TXT_CUSTOM_CLEARANCE') ?></label>' +
                            '<div class="col-sm-6 col-md-6"><ul class="custom-wrap">' + add_pdf_file(data) + '</ul></div>' +
                            '</div>';
                    var element = '<div class="form-group">' +
                            '<label class="col-sm-5 control-label text-left"><?= __('TXT_TRACKING_NUMBER') ?></label>' +
                            '<div class="col-sm-1"></div>' +
                            '<div class="col-sm-6"><input type="text" name="tracking" value="' + arr1.tracking + '" class="form-control" placeholder="<?= __('TXT_TRACKING_NUMBER') ?>" id="tracking"></div></div>'+
                            '<div class="form-group">' +
                            '<label class="col-sm-5 control-label text-left"><?= __('TXT_DELIVERY_DATE') ?></label>' +
                            '<div class="col-sm-1"></div>' +
                            '<div class="col-sm-6"><input type="text" value="' + arr1.delivery_date + '" name="delivery_date" class="form-control" id="delivery-date"></div></div>' +
                            '<div class="form-group"><div class="col-sm-12 text-right"><button type="button" class="btn btn-sm btn-primary btn-update-delivery"><?= __('TXT_UPDATE') ?></button></div></div>' + element1;

                    $(form).find('.status-wrap').empty().prepend(element);
                    $(form).find('.label-draft').text('<?= __('TXT_CUSTOM_CLEARANCE') ?>');
                    $(form).find('.btn-register').text('<?= __('TXT_SEND_PO') ?>');
                    $(form).find('#status').empty().append('<option selected="selected" value="<?= PO_STATUS_REQUESTED_CC ?>"><?= __('TXT_DELIVERING') ?></option>');
                    $(form).find('#delivery-date').datetimepicker({
                        format: 'YYYY-MM-DD'
                    });
                    $('body').find('.update-sub-status').remove();
                    break;
                // DELIVERING STATUS
                case 6 :
                    $(form).find('.label-draft').text('<?= __('TXT_DELIVERING') ?>');
                    $(form).find('#status').empty().append('<option selected="selected" value="<?= PO_STATUS_DELIVERED ?>"><?= __('TXT_DELIVERED') ?></option>');
                    $(form).find('.status-wrap').empty();
                    break;
                // DELIVED STATUS
                case 7 :
                    $(form).find('.label-draft').text('<?= __('TXT_DELIVERED') ?>');
                    $(form).find('#status').empty().append('<option selected="selected" value="<?= STATUS_COMPLETED ?>"><?= __('TXT_PO_STATUS_COMPLETED') ?></option>');
                    $('body').find('.update-sub-status').remove();
                    $(form).find('.status-wrap').empty();
                    break;
                // COMPLETED STATUS
                case 8 :
                    if (local !== '<?= LOCAL_EN ?>') {
                        $(form).find('.label-draft').text('<?= __('TXT_PO_STATUS_COMPLETED') ?>');
                        $(form).find('.form-group:eq(0) label:last').remove();
                        $(form).find('.form-group:eq(1) div:eq(2)').html('<span>After 7days change to ‘completed’ auto</span>');
                        $(form).find('button[type="submit"]').remove();
                        $(form).find('.status-wrap').empty();
                    }
                    break;
            }
        }

        /**
        * use for add payment to po footer
         * @param {object} data
         * @returns {string}
         * */
        function add_payment(data)
        {
            var element = '';
            if (data !== null && data !== 'undefined') {
                $.each(data, function(i, v) {
                    element += '<li><span class="label label-success">' + v + '</span></li>&nbsp;';
                });
            }
            return element;
        }

        /**
         * use for add pdf file to po footer
         * @param {object} data
         * @returns {string}
         * */
        function add_pdf_file(data)
        {
            var element = '<li data-type="new">' +
                            '<span class="label label-success browse-file">&nbsp;&nbsp;Browse File</span><span>&nbsp;</span>' +
                            '<span class="label label-primary add-pdf-file">Add</span>' +
                            '</li>';

            if (data.data.medias !== null && data.data.medias !== 'undefined') {
                $.each(data.data.medias, function(i, v) {
                    if (i == 0) {
                        element = '<li data-id="' + v.id + '" data-name="' + v.file_name + '" data-document-type="' + v.document_type + '" data-type="edit">' +
                            '<span class="label label-success browse-file-edit">&nbsp;&nbsp;Browse File</span><span>&nbsp;</span>' +
                            '<span class="label label-primary add-pdf-file">Add</span>' +
                            '</li>';
                    } else {
                        element += '<li data-id="' + v.id + '" data-name="' + v.file_name + '" data-document-type="' + v.document_type + '" data-type="edit">' +
                            '<span class="label label-success browse-file-edit"><i class="fa fa-times rm-pdf-file" aria-hidden="true"></i>&nbsp;&nbsp;Browse File</span><span>&nbsp;</span>' +
                            '</li>';
                    }
                });
            }

            return element;
        }
    });
</script>
