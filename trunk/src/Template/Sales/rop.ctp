
<?php
$userGroup = $this->request->session()->read('user_group');
$local = $this->request->session()->read('tb_field');
$this->assign('title', 'ROP');

//Invoice Condition
$taxableTotal = 0;
?>
<div class="row">
    <div class="col-md-7">
        <h1><?= __('TXT_ROP_FORM') ?></h1>
        <?php
            echo $this->Form->create(null, ['class' => 'form-update-rop-sale']);
            $this->Form->templates([
                'inputContainer' => '{{content}}'
            ]);
            echo $this->Form->hidden('id', ['value' => $data->id, 'class' => 'sale-id']);
        ?>
        <table class="sale-table">
            <tr>
                <td width="22%">
                    <strong><?= __('TXT_ORDER_FORM_NUMBER') ?>&nbsp;:</strong>
                </td>
                <td>
                    <?= $data->rop_number ?>
                </td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>
                    <p class="sale-label"><strong><?= __('TXT_ISSUE_DATE') ?>:</strong></p>
                </td>
                <td style="padding-bottom: 0;">
                    <div class="input-group">
                        <?php
                        $issue_date = date('Y年m月d日', strtotime($data->rop_issue_date));
                        if ($local == '_en') {
                            $issue_date = date('Y-m-d', strtotime($data->rop_issue_date));
                        }

                        echo $this->Form->input('rop_issue_date', [
                            'class' => 'form-control rop-issue-date datepicker',
                            'value' => $issue_date,
                            'label' => false,
                            'id' => 'sale-rop-issue-date',
                            'placeholder' => __('TXT_ENTER_DATE')
                        ]);
                        echo $this->Form->hidden('en', [
                            'value' => $local,
                            'class' => 'is-en'
                        ]);
                        echo $this->Form->hidden('type', [
                            'value' => '',
                            'class' => 'type-of-date'
                        ]);
                        ?>
                        <div class="input-group-btn">
                            <button type="button" class="btn btn-default" aria-label="Calendar" id="trigger-calendar">
                                <span class="glyphicon glyphicon-calendar"></span>
                            </button>
                        </div>
                    </div>
                    &nbsp;
                    <span class="hide info-status" id="rop-date-info"></span>
                </td>
                <td width="17%">
                    <p class="sale-label"><strong><?= __('TXT_SALE_ORDER_DATE') ?>:</strong></p>
                </td>
                <td style="padding-bottom: 0;">
                    <div class="input-group">
                        <?php
                        $order_date = date('Y年m月d日', strtotime($data->order_date));
                        if ($local == '_en') {
                            $order_date = date('Y-m-d', strtotime($data->order_date));
                        }

                        echo $this->Form->input('order_date', [
                            'class' => 'form-control order_date datepicker',
                            'value' => $order_date,
                            'label' => false,
                            'id' => 'sale-order-date',
                            'placeholder' => __('TXT_ENTER_DATE')
                        ]);
                        ?>
                        <div class="input-group-btn">
                            <button type="button" class="btn btn-default" aria-label="Calendar" id="trigger-order-date">
                                <span class="glyphicon glyphicon-calendar"></span>
                            </button>
                        </div>
                    </div>
                    &nbsp;
                    <span class="hide info-status" id="order-date-info"></span>
                </td>
            </tr>
        </table>
        <?= $this->Form->end() ?>
    </div>
    <?php
        $supplier_name = '';
        if ($data->seller->manufacturer) {
            $id = $data->seller->manufacturer->id;
            list($addr, $jp_addr2, $en_addr2, $tel, $supplier_fax) = $this->Comment->addressFormat($local, $data->seller->manufacturer, TYPE_MP);
            $supplier_name = $this->Comment->nameEnOrJp($data->seller->manufacturer->name, $data->seller->manufacturer->name_en, $local);
        } else {
            $id = $data->seller->supplier->id;
            list($addr, $jp_addr2, $en_addr2, $tel, $supplier_fax) = $this->Comment->addressFormat($local, $data->seller->supplier, TYPE_MP);
            $supplier_name = $this->Comment->nameEnOrJp($data->seller->supplier->name, $data->seller->supplier->name_en, $local);
        }
        echo $this->element('rop_po_address', [
            'addr' => $addr,
            'jp_addr2' => $jp_addr2,
            'en_addr2' =>  $en_addr2,
            'tel' => $tel,
            'top_fax' => $supplier_fax,
            'is_medipro' => $this->request->param('controller'),
            'supplier_name' => $supplier_name,
            'local' => $local
        ]);
    ?>
</div>
<div class="row">
    <div class="col-md-12">
        <hr class="hr-invoiced">
    </div>
</div>
<?php
echo $this->Form->create(null, ['class' => 'form-update-sale']);
$this->Form->templates([
    'inputContainer' => '{{content}}'
]);
echo $this->Form->hidden('id', ['value' => $data->id, 'class' => 'sale-id']);
echo $this->Form->hidden('seller_id', ['value' => $data->seller->id, 'class' => 'seller-id']);
echo $this->Form->hidden('customer_id', ['value' => $data->customer->id, 'class' => 'customer-id']);
?>
<div class="row row-top-space">
    <div class="col-md-10">
        <div class="seller-info" id="seller-id" data-id="<?= $data->seller->id ?>">
            <div class="row invoice-form-row">
                <div class="col-md-12">
                    <h3 class="c-name" style="display: inline;">
                        <?php
                        echo h($data->customer_name); //display value from table sales, for hidden update value take from table customers.
                        $customer = $this->Comment->nameEnOrJp($data->customer->name, $data->customer->name_en, $local);

                        $btn_enable = '';
                        if ($data->customer_name == $customer) {
                            $btn_enable = ' disabled="disabled"';
                        }
                        ?>
                    </h3>
                    <button<?= $btn_enable ?> class="btn btn-sm btn-primary btn-update-customer-info check-customer-name" style="display: inline; margin-left: 15px;" data-cid="<?= $data->customer->id ?>" data-type="name" type="button">
                        <?= __('マスタとデータ不一致')?><br/><?= __('マスタのデータを表示する') ?>
                    </button>
                </div>
            </div>
            <?= $this->Form->hidden('customer_name', ['class' => 'customer-name', 'value' => $customer]); ?>
            <div class="row invoice-form-row">
                <div class="col-md-2"><p><?= __('TXT_ADDRESS') ?></p></div>
                <div class="col-md-10">
                    <?php
                    $c_address = ''; //address from customers table
                    list($address, $jp_addr2, $en_addr2, $phone, $fax) = $this->Comment->addressFormat($local, $data->customer, TYPE_MP);
                    if (mb_substr($address, 0, 1) == '〒') {
                        if ($data->customer) {
                            $c_address .= $address . $jp_addr2;
                            $address .= "<br />" . $jp_addr2;
                        }
                    } else {
                        $c_address .= $address . $en_addr2;
                        $address .= "<br />" . $en_addr2;
                    }

                    $s_address = $data->customer_address;
                    $s_address = str_replace(["\r\n", '<br />', '<br>'], [null, null, null], $s_address);
                    echo '<span class="c-address" style="float:left;">' . nl2br($data->customer_address) . '</span>';
                    //display value from table sales, for hidden update value take from table customers.
                    echo $this->Form->hidden('customer_address', [
                        'value' => $address,
                        'class' => 'customer-address',
                        'label' => false,
                        'readonly' => true
                    ]);

                    $enable = '';
                    if ($c_address == $s_address) {
                        $enable = ' disabled="disabled"';
                    }
                    ?>
                    <span<?= $enable ?> class="btn btn-sm btn-primary btn-update-customer-info check-customer-address" style="float:left; margin-left: 15px;" data-cid="<?= $data->customer->id ?>" data-type="address" role="button">
                    <?= __('マスタとデータ不一致')?><br/><?= __('マスタのデータを表示する') ?>
                    </span>
                </div>
            </div>
            <div class="row invoice-form-row">
                <div class="col-md-2 custom-select">
                    <div class="invoice-form-label"><?= __('TXT_TEL') ?></div>
                </div>
                <div class="col-md-3 custom-select">
                    <?php
                    $emails = [];
                    $phones = [];
                    if ($data->customer->info_details) {
                        foreach ($data->customer->info_details as $info) {
                            if ($info->email) {
                                $emails[$info->email] = $info->email;
                            }
                            if ($info->phone) {
                                $phones[$info->phone] = $info->phone;
                            } else if ($info->tel) {
                                $phones[$info->tel] = $info->tel;
                            }
                        }
                    }

                    echo $this->Form->select('customer_tel', $phones, [
                        'class' => 'form-control select-tel',
                        'label' => false,
                        'default' => $data->customer_tel,
                        'templates' => [
                            'inputContainer' => '{{content}}',
                        ],
                        'empty' => ['' => __('TXT_SELECT_TEL')]
                    ]);
                    $enable = '';
                    if ($data->customer_fax == $fax) {
                        $enable = ' disabled="disabled"';
                    }
                    ?>
                </div>
                <div class="col-md-1" style="padding-right: 0;">
                    <div class="invoice-form-label">/ <?= __('TXT_FAX') ?></div>
                </div>
                <div class="col-md-3 field-small-padding-right">
                    <?= $this->Form->input('customer_fax', [
                        'type' => 'text',
                        'class' => 'form-control txt-fax',
                        'label' => false,
                        'placeholder' => __('TXT_ENTER_FAX'),
                        'value' => $data->customer_fax,
                        'templates' => [
                            'inputContainer' => '{{content}}',
                        ],
                    ]); ?>
                </div>
                <div class="field-small-padding-left col-md-3">
                    <button<?= $enable ?> class="btn btn-sm btn-primary btn-update-customer-info check-customer-fax" data-cid="<?= $data->customer->id ?>" data-type="fax" type="button">
                    <?= __('マスタとデータ不一致')?><br/><?= __('マスタのデータを表示する') ?>
                    </button>
                </div>
            </div>
            <div class="row invoice-form-row">
                <div class="col-md-2"><div class="invoice-form-label"><?= __('TXT_EMAIL') ?></div></div>
                <div class="col-md-3 custom-select">
                    <?= $this->Form->select('customer_email', $emails, [
                        'class' => 'form-control select-email',
                        'label' => false,
                        'default' => $data->customer_email,
                        'templates' => [
                            'inputContainer' => '{{content}}',
                        ],
                        'empty' => ['' => __('TXT_SELECT_EMAIL')]
                    ]);
                    ?>
                </div>
            </div>
            <div class="row invoice-form-row">
                <div class="col-md-offset-2 col-md-9">
                    <?php
                    echo $this->Form->button(__('TXT_UPDATE'), [
                        'type' => 'button',
                        'class' => 'btn btn-primary btn-sm btn-add-info',
                    ]);
                    ?>
                    &nbsp;
                    <span class="hide info-status" id="customer-info"></span>
                </div>
            </div>
        </div>
    </div>
</div>
<?= $this->Form->end(); ?>
<?php
echo $this->Form->create(null, ['class' => 'form-update-doctor-sale']);
$this->Form->templates([
    'inputContainer' => '{{content}}'
]);
echo $this->Form->hidden('id', ['value' => $data->id, 'class' => 'sale-id']);
?>
<div class="row row-top-small-space">
    <div class="col-md-10">
        <div class="doctor-info">
            <div class="row invoice-form-row">
                <div class="col-md-2">
                    <?= __('TXT_USER_REMARK') ?>
                </div>
            </div>
            <div class="row invoice-form-row">
                <div class="col-md-2">
                    <div class="invoice-form-label"><?= __('TXT_CONTACT') ?></div>
                </div>
                <div class="col-md-3 custom-select">
                    <?php
                    $dct_options = [];
                    $default = '';
                    $doctor_name = '';
                    if ($doctors) {
                        foreach ($doctors as $dct) {
                            if ($dct->is_priority == 1 && $default == '') {
                                $default = $dct->doctor->id;
                                $doctor_name = $this->Comment->nameEnOrJp($dct->doctor->full_name, $dct->doctor->full_name_en, $local);
                            }
                            $dct_options[$dct->doctor->id] = $this->Comment->nameEnOrJp($dct->doctor->full_name, $dct->doctor->full_name_en, $local);
                        }
                    }
                    echo $this->Form->select('doctor_id', $dct_options, [
                        'class' => 'form-control select-doctor',
                        'default' => $default,
                        'label' => false,
                        'empty' => ['' => __('TXT_SELECT_DOCTOR')]
                    ]);
                    echo $this->Form->hidden('doctor_name', ['value' => empty($data->doctor_name) ? $doctor_name : $data->doctor_name, 'class' => 'doctor-name']);
                    ?>
                </div>
                <div class="col-md-3"><div style="margin-top:7px;">先生からのご注文です。</div></div>
            </div>
            <div class="row invoice-form-row">
                <div class="col-md-offset-2 col-md-9">
                    <?php
                    echo $this->Form->button(__('TXT_UPDATE'), [
                        'type' => 'button',
                        'class' => 'btn btn-primary btn-sm btn-add-doctor-info',
                    ]);
                    ?>
                    &nbsp;
                    <span class="hide info-status" id="doctor-info"></span>
                </div>
            </div>
        </div>
    </div>
</div>
<?= $this->Form->end(); ?>
<?php
if ($data->currency->code_en != 'JPY') {
    echo $this->element('rop_exchange', ['data' => $data]);
}
?>
<div class="row">
    <div class="col-md-12">
        <table class="table table-striped table-bordered table-po">
            <thead>
                <tr>
                    <th><?= __('No') ?></th>
                    <th width="35%"><?= __('TXT_DESCRIPTION') ?></th>
                    <th>
                        <span style="position: relative; top: 7px;"><?= __('TXT_UNIT_PRICE') ?>&nbsp;</span>
                    <?php
                        echo $this->Form->create(null, [
                            'autocomplete' => 'off',
                            'class' => 'form-inline form-update-currency',
                            'style' => 'float:right;'
                        ]
                        );
                        echo $this->Form->hidden('id', ['value' => $data->id, 'id' => 'sale-currency-update-id']);
                        echo '<div class="form-group">';
                        echo '<label for="currency_id">'. __('TXT_CURRENCY') .'&nbsp;&nbsp;</label>';
                        echo $this->Form->select('currency_id', $currencies, [
                            'class' => 'form-control select-currency',
                            'default' => $data->currency_id,
                        ]);
                        echo '<span class="hide info-status" id="currency-change"></span>';
                        echo '</div>';
                        echo $this->Form->end();
                    ?>
                    </th>
                    <th><?= __('TXT_QUANTITY') ?></th>
                    <th><?= __('TXT_AMOUNT') ?></th>
                    <th width="5%"><?= __('TXT_DELETE') ?></th>
                </tr>
            </thead>
            <tbody>
                <?php
                // get currency JPY
                $currency = null;
                if ($currencies) {
                    foreach ($currencies as $key => $value) {
                        if (($value === TYPE_CURRENCY_JPY_CODE_EN || $value === TYPE_CURRENCY_JPY_CODE)) {
                            $currency = $value;
                        }
                    }
                }
                if ($saleDetail) {
                    $num = 0;
                    // currecy exchange rate
                    $exchangeRate = 1;
                    if (($data->currency_id != 1) && (!empty($data->currency_exchange_rate))) {
                        $exchangeRate = $data->currency_exchange_rate;
                    }
                    if ($data->sale_details[0]->count <= 20) {
                        for ($i = 0; $i < ((20 - $data->sale_details[0]->count) + count($saleDetail)); $i++) {
                            $num += 1;
                            if (($i + 1) <= count($saleDetail)) {
                                $item = $saleDetail[$i];
                                $unit_price = intval($item->unit_price) * intval($exchangeRate);
                                $amount = $unit_price * intval($item->quantity);
                                $taxableTotal += $amount;
                                $is_discount = ($item->is_discount == 1) ? '<label class="label-foc-child no-margin">Discounted</label>&nbsp;' : null;
                                // display parent record
                                echo '<tr data-id="' . $item->id . '" data-type="' . TARGET_EDIT . '">' .
                                    '<td class="text-center">' . $num . '</td>' .
                                    '<td>' . $this->Comment->POProductDetailName($item) . '</td>' .
                                    '<td>' . $is_discount . $this->Comment->currencyEnJpFormatS($local, $unit_price, $currencies[1]) . '</td>' .
                                    '<td>' . $item->quantity . '</td>' .
                                    '<td>' . $this->Comment->currencyEnJpFormatS($local, $amount, $currencies[1]) . '</td>' .
                                    '<td class="text-center"><button type="button" class="btn btn-danger btn-sm btn-rm-row"><span class="glyphicon glyphicon-trash"></span></button></td></tr>';
                            } else {
                                echo '<tr data-type="' . TARGET_NEW . '"><td class="text-center">' . $num . '</td><td>&nbsp;</td>' .
                                     '<td>&nbsp;</td>' .
                                     '<td>&nbsp;</td>' .
                                     '<td>&nbsp;</td>' .
                                     '<td>&nbsp;</td></tr>';
                            }
                        }
                    } else {
                        foreach ($saleDetail as $key => $value) {
                            $num += 1;
                            $amount3 = intval($value->unit_price) * intval($value->quantity);
                            $taxableTotal += $amount3;
                            // display parent record
                            echo '<tr data-id="' . $value->id . '">' .
                                '<td class="text-center">' . $num . '</td>' .
                                '<td>' . $this->Comment->POProductDetailName($value) . '</td>' .
                                '<td>' . $this->Comment->currencyEnJpFormatS($local, ($value->unit_price * $exchangeRate), $currencies[1]) . '</td>' .
                                '<td>' . $value->quantity . '</td>' .
                                '<td>' . $this->Comment->currencyEnJpFormatS($local, ($amount3 * $exchangeRate), $currencies[1]) . '</td>' .
                                '<td class="text-center"><button type="button" class="btn btn-danger btn-sm btn-rm-row"><span class="glyphicon glyphicon-trash"></span></button></td></tr>';
                        }
                    }
                } else {
                    for ($i = 1; $i <= 20; $i ++) {
                        echo '<tr data-type="' . TARGET_NEW . '">' .
                            '<td>' . $i . '</td>' .
                            '<td>&nbsp;</td>' .
                            '<td>&nbsp;</td>' .
                            '<td>&nbsp;</td>' .
                            '<td>&nbsp;</td>' .
                            '<td>&nbsp;</td>' .
                            '</tr>';
                    }
                }
                ?>

                <!-- Table Footer -->
                <tr class="table-po-footer">
                    <td colspan="2" rowspan="12" class="border-none no-padding-left" style="vertical-align: top;"></td>
                    <td colspan="2" class="light-green"><b>a.&nbsp;<?= __('TXT_IMPORT_VAT_TAXABLE') ?></b></td>
                    <td><?= $this->Comment->exchangeRateFormat($currency, $taxableTotal); ?></td>
                    <td>&nbsp;</td>
                </tr>
                <tr class="table-po-footer border-none">
                    <td colspan="2" class="light-green">b.&nbsp;<?= __('TXT_IMPORT_VAT') ?></td>
                    <td>
                        <?php
                        $rowB = round(($taxableTotal * 0.063), -2);
                        echo $this->Comment->exchangeRateFormat($currency, $rowB);
                        ?>
                    </td>
                    <td>&nbsp;</td>
                </tr>
                <tr class="table-po-footer border-none">
                    <td colspan="2" class="light-green">c.&nbsp;<?= __('TXT_DOMESTIC_VAT') ?></td>
                    <td>
                        <?php
                        $rowC = round($rowB * 0.063, -2);
                        echo $this->Comment->exchangeRateFormat($currency, $rowC);
                        ?>
                    </td>
                    <td>&nbsp;</td>
                </tr>
                <tr class="table-po-footer border-none">
                    <td colspan="2" class="po-sub-total-bg"><b>d.&nbsp;<?= __('TXT_PO_SUB_TOTAL') ?></b></td>
                    <td class="po-sub-total-bg">
                        <?php
                        $rowD = $taxableTotal + $rowB + $rowC;
                        echo $this->Comment->exchangeRateFormat($currency, $rowD);
                        ?>
                    </td>
                    <td>&nbsp;</td>
                </tr>
                <tr class="table-po-footer border-none">
                    <td colspan="2" class="light-green">e.&nbsp;<?= __('TXT_DOMESTIC_COURIER_SERVICE_CHARGE') ?></td>
                    <td>
                        <span class="label label-success deomestic-charge" style="font-size: inherit;">
                            <i class="fa fa-pencil" aria-hidden="true"></i>
                        </span>&nbsp;
                        <?php
                        echo $this->Comment->exchangeRateFormat($currency, $data->domestic_courier_service_charge);
                        ?>
                    </td>
                    <td>&nbsp;</td>
                </tr>
                <tr class="table-po-footer border-none">
                    <td colspan="2" class="light-green">f.&nbsp;<?= __('TXT_VAT') ?></td>
                    <td>
                        <?php
                        $domestic = $data->domestic_courier_service_charge ? $data->domestic_courier_service_charge : 0;
                        $rowF = round($domestic * 0.08, 0);
                        echo $this->Comment->exchangeRateFormat($currency, $rowF);
                        ?>
                    </td>
                    <td>&nbsp;</td>
                </tr>
                <tr class="table-po-footer border-none">
                    <td colspan="2" class="po-sub-total-bg"><b>g.&nbsp;<?= __('TXT_PO_SUB_TOTAL2') ?></b></td>
                    <td class="po-sub-total-bg">
                        <?php
                        $rowG = $rowF + $data->domestic_courier_service_charge;
                        echo $this->Comment->exchangeRateFormat($currency, $rowG);
                        ?>
                    </td>
                    <td>&nbsp;</td>
                </tr>
                <tr class="table-po-footer border-none">
                    <td colspan="2" class="light-green">h.&nbsp;<?= __('TXT_HANDLING_FEE') ?></td>
                    <td>
                        <span class="label label-success handling-fee" style="font-size: inherit;">
                            <i class="fa fa-pencil" aria-hidden="true"></i>
                        </span>&nbsp;
                        <?php
                        echo $this->Comment->exchangeRateFormat($currency, $data->handling_fee);
                        ?>
                    </td>
                    <td>&nbsp;</td>
                </tr>
                <tr class="table-po-footer border-none">
                    <td colspan="2" class="light-green">i.&nbsp;<?= __('TXT_PO_VAT2') ?></td>
                    <td>
                        <?php
                        $rowI = round($data->handling_fee * 0.08, 0);
                        echo $this->Comment->exchangeRateFormat($currency, $rowI);
                        ?>
                    </td>
                    <td>&nbsp;</td>
                </tr>
                <tr class="table-po-footer border-none">
                    <td colspan="2" class="po-sub-total-bg"><b>j.&nbsp;<?= __('TXT_PO_SUB_TOTAL3') ?></b></td>
                    <td class="po-sub-total-bg">
                        <?php
                        $rowJ = $data->handling_fee + $rowI;
                        echo $this->Comment->exchangeRateFormat($currency, $rowJ);
                        ?>
                    </td>
                    <td>&nbsp;</td>
                </tr>
                <tr class="table-po-footer border-none">
                    <td colspan="2" class="light-green"><b>k.&nbsp;<?= __('TXT_SPECIAL_DISCOUNT') ?></b></td>
                    <td>
                        <span class="label label-success special-discount" style="font-size: inherit;">
                            <i class="fa fa-pencil" aria-hidden="true"></i>
                        </span>&nbsp;
                        <?php
                        echo $this->Comment->exchangeRateFormat($currency, $data->special_discount);
                        ?>
                    </td>
                    <td>&nbsp;</td>
                </tr>
                <tr class="table-po-footer border-none">
                    <td colspan="2" class="po-total-bg"><b>l.&nbsp;<?= __('TXT_INVOICE_TOTAL') ?></b></td>
                    <td class="po-total-bg">
                        <?php
                        $rowL = $rowD + $rowG + $rowJ - $data->special_discount;
                        echo $this->Comment->exchangeRateFormat($currency, $rowL);
                        ?>
                    </td>
                    <td>&nbsp;</td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
<!--Invoice Footer-->
<div style="margin-top: 2em;"></div>
<div class="invoice-footer">
    <?php
    echo $this->Html->link(__('TXT_PO_DOWNLOAD_INVOICE'), [
        'controller' => 'sale-reports',
        'action' => 'rop-pdf', $data->id,
    ], [
        'class' => 'btn btn-sm btn-primary btn-width',
        'id' => 'btn-invoice-pdf'
    ]);
    ?>
</div>

<!-- Modal Add PDF Download Page -->
<div class="modal fade" id="modalPDFDownloadPage" role="dialog" data-backdrop="static">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"><?= __('TXT_FAX_TRANSMIT') ?></h4>
            </div>
            <div class="modal-body modal-body-fix">
                <div class="col-sm-12" style="margin-bottom: 15px;">
                    <input type="text" class="form-control" value="2" placeholder="<?= __('TXT_ENTER_FAX_TRANSMIT') ?>">
                </div>
                <div class="col-sm-12">
                    <select name="person_in_charge_id" class="form-control" placeholder="<?= __('TXT_PERSON_IN_CHARGES') ?>">
                        <?php
                        if (isset($data['customer']['person_in_charges']) && !empty($data['customer']['person_in_charges'])) :
                            $field_first_name = 'first_name';
                            $field_last_name = 'last_name';
                            foreach ($data['customer']['person_in_charges'] as $key => $value) :
                                $selected = '';
                                if ($data['person_in_charge_id'] == $value['id']) {
                                    $selected = 'selected';
                                }
                        ?>
                        <option value="<?php echo $value['id']; ?>" <?php echo $selected; ?>><?php echo $value[$field_first_name] . ' ' . $value[$field_last_name]; ?></option>
                        <?php
                            endforeach;
                        endif;
                        ?>
                    </select>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default btn-sm btn-width close-modal" data-dismiss="modal"><?= __('BTN_CANCEL') ?></button>
                <button type="button" class="btn btn-primary btn-sm btn-width" id="btnPdfDownloadSend"><?= __('TXT_ISSUE') ?></button>
                <button type="button" class="btn btn-primary btn-sm btn-width" id="btnPdfDownloadResend"><?= __('TXT_RESEND') ?></button>
            </div>
        </div>
    </div>
</div>

<script>
$(document).ready(function(){
    <?php if ($local == '') { ?>
    $('.datepicker').datetimepicker({
        format: 'YYYY年MM月DD日',
        minDate: '2007-01-01'
    });
    <?php } else { ?>
    $('.datepicker').datetimepicker({
        format: 'YYYY-MM-DD',
        minDate: '2007-01-01'
    });
    <?php } ?>

    $('#trigger-calendar').click(function() {
        $('#sale-rop-issue-date').focus();
    });

    $('#trigger-order-date').click(function() {
        $('#sale-order-date').focus();
    });

    var $pdfUrl;
    var $current_sale_id = <?php echo $data['id']; ?>;
    //alert($current_sale_id);

    $('#btn-invoice-pdf').on('click', function(e){
        e.preventDefault();
        $pdfUrl = $(this).attr('href');
        $('#modalPDFDownloadPage').modal('show');
    });
    $('#btnPdfDownloadSend').on('click', function(){
        var number = $('#modalPDFDownloadPage').find('input[type=text]').val();
        if (number) {
            var person_id = $('#modalPDFDownloadPage').find('select[name=person_in_charge_id]').val();
            if (person_id) {
                savePersonInChargeToSale($current_sale_id, person_id, $pdfUrl + '?page=' + number);
//                if (savePersonInChargeToSale($current_sale_id, person_id)) {
//                    window.open($pdfUrl + '?page=' + number, '_blank');
//                }
            }
        }
    });
    $('#btnPdfDownloadResend').on('click', function(){
        var number = $('#modalPDFDownloadPage').find('input[type=text]').val();
        if (number) {
            var person_id = $('#modalPDFDownloadPage').find('select[name=person_in_charge_id]').val();
            if (person_id) {
                savePersonInChargeToSale($current_sale_id, person_id, $pdfUrl + '?page=' + number + '&type=resend');
//                if (savePersonInChargeToSale($current_sale_id, person_id)) {
//                    window.open($pdfUrl + '?page=' + number + '&type=resend', '_blank');
//                }
            }
        }
    });
    $('#modalPDFDownloadPage').on('hidden.bs.modal', function () {
        $('#modalPDFDownloadPage').find('input[type=text]').empty();
    });

    $('body').on('click', '#pdf-link-new', function(e) {
        e.preventDefault();
        var that = this;
        var url = $(that).attr('href');
        //var $wi = window.open('about:blank', '_blank');

        setTimeout(function(){
            window.open(url, '_blank');
        }, 500);
    });

    function savePersonInChargeToSale(sale_id, person_in_charge_id, redirect_url)
    {
        if (sale_id && person_in_charge_id) {
            var link = '<a id="pdf-link-new" href="'+redirect_url+'" target="_blank" style="display: none">click</a>';
            $.ajax({
                url: '<?= $this->Url->build(['action' => 'savePersonInChargeToSale']) ?>',
                type: 'POST',
                dataType: 'json',
                async: false,
                data: {sale_id: sale_id, person_in_charge_id: person_in_charge_id},
                beforeSend: function() {
                    $.LoadingOverlay('show');
                },
                success: function(respone) {
                    //console.log(respone.status);
                    if (respone.status == 1) {
                        $('#modalPDFDownloadPage').find('.modal-body').append(link);
                        $('#pdf-link-new').trigger('click');
                        //$('#pdf-link-new').click();
                        //window.open(redirect_url, '_blank');
                        //window.location.href = redirect_url;
                    } else {
                        //console.log(0);
                        //return false;
                        console.log(respone);
                    }
                    $.LoadingOverlay('hide');
                },
                complete: function() {
                    $('#modalPDFDownloadPage').find('#pdf-link-new').remove();
                }
            });
        }
    }
});
</script>
<!-- Modal Discount Conditions -->
<div class="modal fade" id="modalDiscountCondition" role="dialog" data-backdrop="static">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"><?= __('STR_DISCOUNT_CONDITIONS') ?></h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <table class="table table-striped table-view-discount">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th><?= __('TXT_FROM') ?></th>
                                    <th><?= __('TXT_TO') ?></th>
                                    <th>&nbsp;</th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default btn-sm btn-width close-modal" data-dismiss="modal"><?= __('BTN_CANCEL') ?></button>
                <button type="button" class="btn btn-primary btn-sm btn-width"><?= __('TXT_REGISTER') ?></button>
            </div>
        </div>
    </div>
</div>
<!--Modal Advance Search-->
<div class="modal fade" id="advance-search" role="dialog" data-backdrop="static">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"><?= __('TXT_ADVANCED_FIND_PRODUCTS') ?></h4>
            </div>
            <div class="modal-body modal-body-fix"></div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default btn-sm btn-width close-modal" data-dismiss="modal"><?= __('BTN_CANCEL') ?></button>
                <button type="button" class="btn btn-primary btn-sm btn-width prd-detail-add" disabled="disabled"><?= __('TXT_SAVE') ?></button>
            </div>
        </div>
    </div>
</div>
<!--Modal Show Update Sub-status Success-->
<div class="modal fade" id="modal-sub-status" role="dialog" data-backdrop="static">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-body"><?= __('TXT_UPDATE_SUB_STATUS') ?></div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary btn-sm btn-width close-modal" data-dismiss="modal"><?= __('TXT_YES') ?></button>
            </div>
        </div>
    </div>
</div>
<!--Modal Domestic-->
<div class="modal fade" id="domestic" role="dialog" data-backdrop="static">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body"></div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default btn-sm btn-width close-modal" data-dismiss="modal"><?= __('BTN_CANCEL') ?></button>
                <button type="button" class="btn btn-primary btn-sm btn-width btn-domestic-yes"><?= __('TXT_YES') ?></button>
            </div>
        </div>
    </div>
</div>
<?= $this->element('Modal/delete') ?>

<?php
echo $this->Html->css([
    '//cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/3.3.4/css/inputmask.min.css',
    'bootstrap-datetimepicker.min',
    'jquery-ui',
    'intlTelInput'
]);
echo $this->Html->script([
    'jquery.inputmask.bundle',
], ['blog' => 'script']);
?>

<script>
    $(function() {
        $('.x-currency-date').datetimepicker({format: 'YYYY/MM/DD'});

        $('.trigger-calendar').click(function() {
            $('.x-currency-date').focus();
        });

        $('body').on('click', '.btn-update-xrate', function(e) {
            $.LoadingOverlay('show');
            $('.help-block').empty().hide();
            $('.info-status').addClass('hide').text('');
            var url = '<?= $this->Url->build(['action' => 'updateCurrencyExchange']) ?>';

            ajax_request_post(url, $('.form-exchange').serialize(), function(data) {
                $.LoadingOverlay('hide');
                if (data.status == 1) {
                    location.reload();
                } else {
                    $.each(data.data, function(key, elem) {
                        var validate = $('#error-' + key);
                        $.each(elem, function(k, v) {
                            validate.removeClass('help-tips')
                                .addClass('error-tips')
                                .text(v)
                                .show();
                        });
                    });
                }
            }, '<?= __('TXT_SESSION_TIMEOUT') ?>');
        });

        $('body').on('change', '.select-currency', function () {
            $.LoadingOverlay('show');
            var url = '<?= $this->Url->build(['controller' => 'Sales', 'action' => 'updateCurrency']) ?>';

            ajax_request_post(url, $('.form-update-currency').serialize(), function(data) {
                location.reload();
            }, '<?= __('TXT_SESSION_TIMEOUT') ?>');
        });

        var local = '<?= $this->request->session()->read('tb_field') ?>';

        $('body').on('click', '.btn-update-customer-info', function () {
            var $this = $(this);
            if ($this.attr('disabled')) {
                return false;
            }
            var update_type = $this.attr('data-type');
            var id = $this.attr('data-cid');
            $.LoadingOverlay('show');
            var data = {en:local, id:id, is_mp: '<?= TYPE_MP ?>', type: update_type};
            var url = '<?= $this->Url->build(['controller' => 'Customers', 'action' => 'getCustomerUpdateName']) ?>';

            ajax_request_get(url, data, function(response) {
                $.LoadingOverlay('hide');
                if (response.status === 1) {
                    if (update_type == 'name') {
                        $('h3.c-name').text(response.data.value);
                        $('.customer-name').empty().val(response.data.value);
                        $this.attr('disabled', true);
                    } else if (update_type == 'address') {
                        $('.c-address').html(response.data.value);
                        $('.customer-address').val(response.data.value);
                        $this.attr('disabled', true);
                    } else if (update_type == 'fax') {
                        $('#customer-fax').val(response.data.value);
                        $this.attr('disabled', true);
                    }
                }
            }, 'json', '<?= __('TXT_SESSION_TIMEOUT') ?>');
        });

        //prevent wrong default select doctor when page refresh
        changeDoctorName();
        function changeDoctorName() {
            var doctor_name = $('.doctor-name').val();
            var selected = $('.select-doctor option:selected').text();
            if (doctor_name != selected) {
                $('.select-doctor option').filter(function () {
                    return $(this).html() == doctor_name;
                }).prop('selected', true);
            }
        }
        $('body').on('change', '.select-doctor', function () {
            $('.doctor-name').val($(this).find('option:selected').text());
        });

        $('#sale-rop-issue-date').on('dp.change', function(e) {
            var type = $('.type-of-date');
            type.val('');
            if ($(this).val() == '') {
                $(this).focus();
                return false;
            }

            var msg = $('#rop-date-info');
            type.val('rop');
            updateDate(msg);
        });

        $('#sale-order-date').on('dp.change', function(e) {
            var type = $('.type-of-date');
            type.val('');
            if ($(this).val() == '') {
                $(this).focus();
                return false;
            }
            var msg = $('#order-date-info');
            type.val('order');
            updateDate(msg);
        });

        function updateDate(msg) { //update rop issue date or order date.
            $.LoadingOverlay('show');
            $('.info-status').removeClass('text-primary text-warning').addClass('hide').text('');
            var data = $('.form-update-rop-sale').serialize();
            var url = '<?= $this->Url->build(['controller' => 'Sales', 'action' => 'updateRopIssueDate']) ?>';

            ajax_request_post(url, data, function(data) {
                $.LoadingOverlay('hide');
                if (data.status == 1) {
                    msg.removeClass('hide').addClass('text-primary').text('<?= __('Date updated.') ?>');
                } else {
                    msg.removeClass('hide').addClass('text-warning').text('<?= __('Date update failed.') ?>');
                }
                setTimeout(function () { msg.addClass('hide'); }, 1500);
            }, '<?= __('TXT_SESSION_TIMEOUT') ?>');
        }

        $('body').on('click', '.btn-add-info', function(e) {
            $.LoadingOverlay('show');
            $('.info-status').removeClass('text-primary text-warning').addClass('hide').text('');
            var data = $('.form-update-sale').serialize();
            var url = '<?= $this->Url->build(['action' => 'updateSaleInfo']) ?>';

            ajax_request_post(url, data, function(data) {
                $.LoadingOverlay('hide');
                if (data.status == 1) {
                    $('#customer-info').removeClass('hide').addClass('text-primary').text('<?= __('Customer info. updated.') ?>');
                } else {
                    $('#customer-info').removeClass('hide').addClass('text-warning').text('<?= __('Customer info. update failed.') ?>');
                }
                setTimeout(function () { $('#customer-info').addClass('hide'); }, 1500);
            }, '<?= __('TXT_SESSION_TIMEOUT') ?>');
        });

        $('body').on('click', '.btn-add-doctor-info', function(e) {
            if ($('.select-doctor').val() == '') {
                $('.select-doctor').focus();
                return false;
            }

            $.LoadingOverlay('show');
            $('.info-status').removeClass('text-primary text-warning').addClass('hide').text('');
            var data = $('.form-update-doctor-sale').serialize();
            var url = '<?= $this->Url->build(['action' => 'updateSaleDoctorInfo']) ?>';

            ajax_request_post(url, data, function(data) {
                $.LoadingOverlay('hide');
                if (data.status == 1) {
                    $('#doctor-info').removeClass('hide').addClass('text-primary').text('<?= __('Doctor name updated.') ?>');
                } else {
                    $('#doctor-info').removeClass('hide').addClass('text-warning').text('<?= __('Doctor name update failed.') ?>');
                }
                setTimeout(function () { $('#doctor-info').addClass('hide'); }, 1500);
            }, '<?= __('TXT_SESSION_TIMEOUT') ?>');
        });

        $('body').on('click', '.deomestic-charge', function(e) {
            $.LoadingOverlay('show');
            var url = '<?= $this->Url->build(['action' => 'getDomestic']) ?>';
            var params = {
                id: '<?= $data->id ?>',
                currency: '<?= $currency ?>'
            };

            ajax_request_get(url, params, function(data) {
                var modal = $('body').find('#domestic');

                $(modal).find('.modal-body').html(data);
                $(modal).find('.modal-title').html('<?= __('TXT_DOMESTIC_COURIER_SERVICE_CHARGE') ?>');
                $(modal).modal('show');
            }, 'html', '<?= __('TXT_SESSION_TIMEOUT') ?>');
        });

        $('body').on('click', '.handling-fee', function(e) {
            $.LoadingOverlay('show');
            var url = '<?= $this->Url->build(['action' => 'getHandlingFee']) ?>';
            var params = {
                id: '<?= $data->id ?>',
                currency: '<?= $currency ?>',
                seller_id: '<?= $data->seller->id ?>'
            };

            ajax_request_get(url, params, function(data) {
                var modal = $('body').find('#domestic');

                $(modal).find('.modal-body').html(data);
                $(modal).find('.modal-title').html('<?= __('TXT_HANDLING_FEE') ?>');
                $(modal).find('.btn-primary').removeClass('btn-domestic-yes').addClass('btn-handling-yes');
                $(modal).modal('show');
            }, 'html', '<?= __('TXT_SESSION_TIMEOUT') ?>');
        });

        $('body').on('click', '.special-discount', function(e) {
            $.LoadingOverlay('show');
            var url = '<?= $this->Url->build(['action' => 'getSpecialDiscount']) ?>';
            var params = {
                id: '<?= $data->id ?>',
                currency: '<?= $currency ?>'
            };

            ajax_request_get(url, params, function(data) {
                var modal = $('body').find('#domestic');

                $(modal).find('.modal-body').html(data);
                $(modal).find('.modal-title').html('<?= __('TXT_SPECIAL_DISCOUNT') ?>');
                $(modal).find('.btn-primary').removeClass('btn-domestic-yes').addClass('btn-special-discount');
                $(modal).modal('show');
            }, 'html', '<?= __('TXT_SESSION_TIMEOUT') ?>');
        });

        $('body').on('click', '.btn-av-search', function(e) {
            $.LoadingOverlay('show');
            var url = '<?= $this->Url->build(['action' => 'advance-search']) ?>';

            ajax_request_get(url, {seller_id: $('#seller-id').attr('data-id')}, function(data) {
                var content = $('body').find('#advance-search');

                $(content).find('.modal-body').html(data);
                $(content).modal('show');

                // Initialize select2
                $(content).find('#manufacturer-id, #brand-id, #product-id').select2({width: '100%'});
            }, 'html', '<?= __('TXT_SESSION_TIMEOUT') ?>');
        });

        $('body').on('change', '#advance-search #manufacturer-id', function(e) {
            $.LoadingOverlay('show');
            var manufacturerId = $(this).val();
            var url = '<?= $this->Url->build(['action' => 'get-brand-by-mf-id']) ?>';

            if (manufacturerId !== '') {
                ajax_request_get(url, {manufacturerId: manufacturerId}, function(data) {
                    var response = data.data;
                    var content = $('body').find('#advance-search');

                    if (response !== null && response !== 'undefined') {
                        var element = '';
                        $.each(response, function(i, v) {
                            element += '<option value="' + i + '">' + v + '</option>';
                        });

                        $(content).find('#brand-id').removeAttr('disabled');
                        $(content).find('#brand-id option:not(:first)').remove();
                        $(content).find('#brand-id').append(element);
                    }
                }, 'json', '<?= __('TXT_SESSION_TIMEOUT') ?>');
            }
        });

        $('body').on('change', '#advance-search #brand-id', function(e) {
            var brandId = $(this).val();
            var url = '<?= $this->Url->build(['action' => 'getProductByBrandId']) ?>';

            if (brandId !== '') {
                $.LoadingOverlay('show');
                ajax_request_get(url, {brandId: brandId}, function(data) {
                    var response = data.data;
                    var content = $('body').find('#advance-search');

                    if (response !== null && response !== 'undefined') {
                        var element = '';
                        $.each(response, function(i, v) {
                            element += '<option value="' + i + '">' + v + '</option>';
                        });

                        $(content).find('#product-id').removeAttr('disabled');
                        $(content).find('#product-id option:not(:first)').remove();
                        $(content).find('#product-id').append(element);
                    }
                }, 'json', '<?= __('TXT_SESSION_TIMEOUT') ?>');
            }
        });

        $('body').on('change', '#advance-search #product-id', function(e) {
            var productId = $(this).val();
            var url = '<?= $this->Url->build(['action' => 'getProductDetailByProductId']) ?>';

            if (productId !== '') {
                $.LoadingOverlay('show');
                ajax_request_get(url, {productId: productId}, function(data) {
                    var table = $('body').find('#advance-search .table-product-detail tbody');
                    $(table).empty();

                    if (data !== null && data !== 'undefined') {
                        $(table).html(data);
                    }
                }, 'html', '<?= __('TXT_SESSION_TIMEOUT') ?>');
            }
        });

        $('body').on('click', '.check-product', function(e) {
            $('.check-product').not(this).prop('checked', false);

            if ($('.check-product:checked')) {
                $('body').find('.prd-detail-add').removeAttr('disabled');
            } else {
                $('body').find('.prd-detail-add').attr('disabled', true);
            }
        });

        $('body').on('click', '.prd-detail-add', function(e) {
            var tr = $('body').find('.check-product:checked').closest('tr');
            var modal = $('body').find('#modalAddItem');
            var parent = $('body').find('#modalAddItem #product-detail-id');
            var params = {
                id: $(tr).attr('data-id'),
                productName: $('body').find('#product-id option:selected').text(),
                productDetailName: $(tr).find('td:eq(2)').text(),
                singleUnitName: $(tr).attr('data-su-name'),
                singleUnitValue: $(tr).attr('data-su-value'),
                packSizeValue: $(tr).attr('data-ps-value'),
                packSizeName: $(tr).attr('data-ps-name'),
                productBrand: $('body').find('#brand-id option:selected').text(),
                packingId: $(tr).attr('data-pack'),
                description: $(tr).attr('data-dsc')
            };

            $(parent).select2().val(params.id).trigger('change');
            $(this).closest('.modal').modal('hide');

            // Change Value for Sale Stock Detail
            $(modal).find('input[name="brand_name"]').val(params.productBrand);
            $(modal).find('input[name="product_name"]').val(params.productName);
            $(modal).find('input[name="single_unit_value"]').val(params.singleUnitValue);
            $(modal).find('input[name="single_unit_name"]').val(params.singleUnitName);
            $(modal).find('input[name="pack_size_value"]').val(params.packSizeValue);
            $(modal).find('input[name="pack_size_name"]').val(params.packSizeName);
            $(modal).find('input[name="packing_id"]').val(params.packingId);
            $(modal).find('input[name="description"]').val(params.description);
        });

        $('body').on('change', '.select-seller', function(e) {
            if ($(this).val() !== '') {
                $.LoadingOverlay('show');
                var url = '<?= $this->Url->build(['action' => 'get-info']) ?>';
                var params = {
                    external_id: $(this).val(),
                    type: '<?= $data->seller->type ?>'
                };

                ajax_request_get(url, params, function(data) {
                    $('body').find('.btn-add-info').attr('disabled', false);
                    var emails = [];
                    var phones = [];
                    var tels = [];
                    var response = data.data;

                    if (response.length > 0) {
                        $.each(response, function(i, v) {
                            if (v.email !== '') {
                                emails.push({key: v.id, value: v.email});
                            }
                            if (v.phone !== '') {
                                phones.push({key: v.id, value: v.phone});
                            }
                            if (v.tel !== '') {
                                tels.push({key: v.id, value: v.tel});
                            }
                        });
                    }

                    if (emails !== '') {
                        $('body').find('.select-email').removeAttr('disabled');
                        $('body').find('.select-email option:not(:first)').remove();
                        $('body').find('.select-email').append(add_option(emails));
                    }
                    if (phones !== '') {
                        $('body').find('.select-phone').removeAttr('disabled');
                        $('body').find('.select-phone option:not(:first)').remove();
                        $('body').find('.select-phone').append(add_option(phones));
                    }
                    if (tels !== '') {
                        $('body').find('.select-tel').removeAttr('disabled');
                        $('body').find('.select-tel option:not(:first)').remove();
                        $('body').find('.select-tel').append(add_option(tels));
                    }
                }, 'json', '<?= __('TXT_SESSION_TIMEOUT') ?>');
            }
        });

        $('body').on('click', '.table-po tbody tr:not(.table-po-footer)', function(e) {
            $.LoadingOverlay('show');
            var inx = $(this).index();
            var params = {
                type: $(this).attr('data-type'),
                target: $(this).attr('data-target'),
                url: '<?= $this->Url->build('/sale-details/getFormCreate') ?>',
                saleId: '<?= $data->id ?>',
                id: $(this).attr('data-id')
            };

            ajax_request_get(params.url, {
                id: params.id,
                saleId: params.saleId,
                seller_id: <?= $data->seller->id ?>,
                type: params.type
            }, function(data) {
                $('body').prepend(data);
                var modal = $('body').find('#sale-detail');
                $(modal).modal('show');
                $(modal).find('#parent-id, #product-detail-id').select2({ width: '100%' });
            }, 'html', '<?= __('TXT_SESSION_TIMEOUT') ?>');
        });

        $('body').on('click', '.s-d-register', function(e) {
            $.LoadingOverlay('show');
            var form = $('body').find('#modalAddItem form');
            var url = '<?= $this->Url->build('/sale-details/create/') ?>';

            ajax_request_post(url, $(form).serialize(), function(data) {
                var response = data.data;
                $(form).find('.error-message').remove();

                if (data.message === '<?= MSG_ERROR ?>') {
                    $.each(response, function(i, v) {
                        var label = '<label class="error-message">' + v + '</label>';
                        $(form).find('[name="' + i + '"]').closest('div').append(label);
                    });
                } else {
                    location.reload();
                }
            }, '<?= __('TXT_SESSION_TIMEOUT') ?>');
        });

        $('body').on('click', '.btn-rm-row', function(e) {
            e.stopPropagation();
            var id = $(this).closest('tr').attr('data-id');
            var content = $('#modal_delete');

            $(content).find('#btn_delete_yes').attr('data-id', id);
            $(content).modal('show');
        });

        $('body').on('click', '#btn_delete_yes', function(e) {
            var url = '<?php echo $this->Url->build('/sale-details/delete/'); ?>';

            ajax_request_post(url, {id: $(this).data('id')}, function(data) {
                if (data.message === '<?= MSG_SUCCESS ?>') {
                    location.reload();
                }
            }, '<?= __('TXT_SESSION_TIMEOUT') ?>');
        });

        $('body').on('click', '.btn-stock-rm', function(e) {
            $(this).closest('.form-group').remove();
        });

        $('body').on('click', '.btn-stock-add', function(e) {
            var content = $('body').find('.modal-pack .main-wrap');
            var index = $(content).find('.form-group').length + 1;
            var stock_id = $('body').find('.modal-pack select[name="stock_id[0]"]').html();
            var element = '<div class="form-group">'
                        + '<label for="" class="col-md-4"></label>'
                        + '<div class="col-md-3 custom-select no-padding-r">'
                        + '<select class="form-control" id="stock-id-' + index + '" name="stock_id[' + index + ']">' + stock_id + '</select></div>'
                        + '<div class="col-md-3 no-padding-r"><input type="number" class="form-control" name="quantity[' + index + ']" placeholder="<?= __('TXT_ENTER_QUANTITY') ?>"/></div>'
                        + '<div class="col-md-2"><button class="btn btn-delete btn-stock-rm"><i class="fa fa-trash" aria-hidden="true"></i></button></div>'
                        + '</div>';

            $(content).append(element);
            $(content).find('#stock-id-' + index).select2({width: '100%'});
        });

        $('body').on('click', '.btn-pack-submit', function(e) {
            $.LoadingOverlay('show');
            var form = $('body').find('.modal-pack form');
            var url = '<?= $this->Url->build('/sale-stock-details/create/') ?>';

            ajax_request_post(url, $(form).serialize(), function(data) {
                var response = data.data;
                $('body').find('#warning-msg').empty();
                $(form).find('.error-message').remove();

                if (data.message === '<?= MSG_ERROR ?>') {
                    var stock_id = response.stock_id;
                    var quantity = response.quantity;

                    // error stock_id
                    $.each(stock_id, function(i, v) {
                        var label = '<label class="error-message">' + v + '</label>';
                        $(form).find('select[name="stock_id[' + i + ']"]').closest('div').append(label);
                    });

                    // error quantity
                    $.each(quantity, function(i, v) {
                        var label = '<label class="error-message">' + v + '</label>';
                        $(form).find('input[name="quantity[' + i + ']"]').closest('div').append(label);
                    });
                } else if (data.message === '<?= MSG_WARNING ?>') {
                    $('body').find('.modal-pack #warning-msg').html('<strong>' + '<?= __('TXT_INVALID_LOT_QUANTITY') ?>' + '</strong>');
                } else {
                    location.reload();
                }
            }, '<?= __('TXT_SESSION_TIMEOUT') ?>')
        });

        $('body').on('click', '.popup-discount', function() {
            $.LoadingOverlay('show');
            var url = '<?= $this->Url->build('/pricing/get-between-quantity/') ?>';
            var params = {
                'type': 'pricing',
                'externalId': $(this).attr('data-id'),
                'quantity': $('body').find('input[name="quantity"]').val()
            };

            ajax_request_get(url, params, function(data) {
                $('body').find('#modalDiscountCondition .table-view-discount tbody').html(data);
                $('body').find('#modalDiscountCondition').modal('show');
                $('body').find('.foc-preview').popover();
            }, 'html', '<?= __('TXT_SESSION_TIMEOUT') ?>');
        });

        $('body').on('click', '.update-sub-status', function(e) {
            $.LoadingOverlay('show');
            var params = {
                subStatus: $('body').find('form[name="sale_status"] #sub-status').val(),
                id: '<?= $data->id ?>'
            };
            var url = '<?= $this->Url->build(['action' => 'updateSubStatus']) ?>';

            ajax_request_post(url, params, function(data) {
                if (data.message === '<?= MSG_SUCCESS ?>') {
                    $('body').find('#modal-sub-status').modal('show');
                }
            }, '<?= __('TXT_SESSION_TIMEOUT') ?>');
        });

        $('body').on('click', '.btn-cancel', function() {
            location.href = '<?= $this->Url->build(['action' => 'index']) ?>';
        });

        /**
        * Function add_option
        * use for add option to select dropdown
        * @param object data
        * @returns return string
        */
        function add_option(data)
        {
           var element = '';

           if (data !== null && data !== undefined) {
               $.each(data, function(i, v) {
                   element += '<option value="' + v.key + '">' + v.value + '</option>';
               });
           }

           return element;
        }

        /**
        * use for add payment to po footer
         * @param {object} data
         * @returns {string}
         * */
        function add_payment(data)
        {
            var element = '';

            if (data !== null && data !== 'undefined') {
                $.each(data, function(i, v) {
                    element += '<li><span class="label label-success">' + v + '</span></li>&nbsp;';
                });
            }

            return element;
        }
    });
</script>
