
<div class="row">
    <div class="col-sm-12">
        <?php
        echo $this->Form->create(null, [
            'role' => 'form',
            'class' => 'form-horizontal form-product-list-search',
            'name' => 'search_form',
            'onsubmit' => 'return false;',
        ]);
        $this->Form->templates([
            'inputContainer' => '{{content}}'
        ]);
        ?>
        <div class="form-group">
            <label class="col-sm-4 control-label"><?= __('TXT_BRAND_NAME') ?></label>
            <div class="col-sm-7 custom-select">
                <?php
                $options = [];
                if ($seller) {
                    if ($seller->seller_brands) {
                        foreach ($seller->seller_brands as $value) {
                            $name = !empty($value->product_brand->name) ? $value->product_brand->name : $value->product_brand->name_en;
                            $options[$value->product_brand_id] = $name;
                        }
                    }
                }
                echo $this->Form->input('brand_id', [
                    'type' => 'select',
                    'class' => 'form-control',
                    'label' => false,
                    'required' => false,
                    'options' => $options,
                    'empty' => ['' => __('TXT_SELECT_BRAND_NAME')]
                ]);
                ?>
            </div>
        </div>
        <div class="form-group custom-select">
            <label class="col-sm-4 control-label"><?= __('TXT_PRODUCT_NAME') ?></label>
            <div class="col-sm-7 custom-select">
                <?php
                echo $this->Form->input('product_id',[
                    'type' => 'select',
                    'class' => 'form-control',
                    'label' => false,
                    'required' => false,
                    'options' => ['' => __('TXT_SELECT_PRODUCT_NAME')],
                    'disabled' => true,
                ]);
                ?>
            </div>
        </div>
        <?= $this->Form->end() ?>
    </div>
</div>
<div class="row">
    <div class="col-sm-12 table-product-price">
        <table class="table table-condensed table-striped table-product-detail">
            <thead>
                <tr>
                    <th>&nbsp;</th>
                    <th>#</th>
                    <th><?= __('TXT_PRODUCT_DETAILS') ?></th>
                </tr>
            </thead>
            <tbody></tbody>
        </table>
    </div>
</div>