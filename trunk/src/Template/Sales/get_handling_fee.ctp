
<?php
echo $this->Form->create($data, [
    'name' => 'form_handling_fee',
    'role' => 'form',
    'class' => 'form-horizontal',
]);
echo $this->Form->hidden('id', ['value' => $data->id]);
echo $this->Form->hidden('digits', [
    'id' => 'digits',
    'value' => $currency->decimal_format
]);
?>
<?php if ($is_contract): ?>
<div class="input-group">
    <div class="input-group-addon"><?= $currency->code ?></div>
    <?php
        echo $this->Form->select('handling_fee', $fees, [
            'class' => 'form-control fee-id',
            'label' => false,
            'empty' => ['' => __('TXT_SELECT_FEE')],
            'templates' => [
                'inputContainer' => '{{content}}'
            ]
        ]);
    ?>
</div>
<?php else: ?>
<div class="input-group">
    <div class="input-group-addon"><?= $currency->code ?></div>
    <?php
    echo $this->Form->input('handling_fee', [
        'type' => 'text',
        'class' => 'form-control',
        'label' => false,
        'placeholder' => __('TXT_HANDLING_FEE'),
        'value' => $data->handling_fee ? $data->handling_fee : 0,
        'templates' => [
            'inputContainer' => '{{content}}'
        ],
    ]);
    ?>
</div>
<?php endif; ?>
<?php echo $this->Form->end(); ?>

<script>
    (function(e) {
        var digits = $('body').find('#digits').val();

        $('#handling-fee').inputmask('decimal', {
            'alias': 'numeric',
            'groupSeparator': ',',
            'autoGroup': true,
            'digits': digits,
            'radixPoint': '.',
            'digitsOptional': false,
            'allowMinus': false,
            'prefix': '',
            'placeholder': convertNumToZero(digits)
        });

        $('body').on('click', '.btn-handling-yes', function(e) {
            $.LoadingOverlay('show');
            var form = $('body').find('form[name="form_handling_fee"]');
            var url = '<?= $this->Url->build(['action' => 'updateHandlingFee']) ?>';

            ajax_request_post(url, $(form).serialize(), function(data) {
                $(form).find('.error-message').remove();

                if (data.message === '<?= MSG_ERROR ?>') {
                    var message = '<label class="error-message">' + data.data + '</labe>';
                    $(form).find('input[type="text"]').closest('form').append(message);
                } else {
                    location.reload();
                }
            }, '<?= __('TXT_SESSION_TIMEOUT') ?>');
        });

        function convertNumToZero(value)
        {
            var str = '';
            if (value > 0) {
                str += '0.';
                for(i = 0; i < value; i ++) {
                    str += '0';
                }
            }
            return str;
        }
    })();
</script>