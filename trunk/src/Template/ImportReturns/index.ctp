
<div class="row row-top-space">
    <div class="col-sm-12">
        <?php
        echo $this->Form->create($return, [
            'role' => 'form',
            'class' => 'form-horizontal form-import-return',
            'name' => 'common_form'
        ]);

        $report_date = '';
        $agree_date = '';
        $export_date = '';

        if ($this->request->query('action') == 'create') {
            echo $this->Form->hidden('import_id', ['class' => 'import-id', 'value' => $this->request->query('import_id')]);
        } else {
            echo $this->Form->hidden('id', ['class' => 'return-id', 'value' => $this->request->query('return_id')]);
            $report_date = date('Y-m-d', strtotime($return['report_date']));
            $agree_date = date('Y-m-d', strtotime($return['agree_date']));
            $export_date = date('Y-m-d', strtotime($return['export_date']));
        }

        $this->Form->templates([
            'inputContainer' => '{{content}}'
        ]);
        ?>
        <div class="form-group">
            <label class="control-label col-md-3 col-md-offset-2" for=""><?= __('TXT_REPORT_DATE') ?><?= $this->Comment->formAsterisk() ?></label>
            <div class="col-md-4">
                <div class="input-group with-datepicker">
                    <?php echo $this->Form->input('report_date', [
                        'class' => 'form-control datepicker',
                        'label' => false,
                        'placeholder' => __('TXT_ENTER_REPORT_DATE'),
                        'type' => 'text',
                        'value' => $report_date
                    ]); ?>
                    <div class="input-group-btn">
                        <button type="button" class="btn btn-default trigger-calendar" aria-label="Calendar">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </button>
                    </div>
                </div>
                <span class="help-block help-tips" id="error-report_date"></span>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-3 col-md-offset-2" for=""><?= __('TXT_AGREE_DATE') ?></label>
            <div class="col-md-4">
                <div class="input-group with-datepicker">
                    <?php echo $this->Form->input('agree_date', [
                        'class' => 'form-control datepicker',
                        'label' => false,
                        'placeholder' => __('TXT_ENTER_AGREE_DATE'),
                        'type' => 'text',
                        'value' => $agree_date
                    ]); ?>
                    <div class="input-group-btn">
                        <button type="button" class="btn btn-default trigger-calendar" aria-label="Calendar">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </button>
                    </div>
                </div>
                <span class="help-block help-tips" id="error-agree_date"></span>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-3 col-md-offset-2" for=""><?= __('TXT_EXPORT_DATE') ?></label>
            <div class="col-md-4">
                <div class="input-group with-datepicker">
                    <?php echo $this->Form->input('export_date', [
                        'class' => 'form-control datepicker',
                        'label' => false,
                        'placeholder' => __('TXT_ENTER_EXPORT_DATE'),
                        'type' => 'text',
                        'value' => $export_date
                    ]); ?>
                    <div class="input-group-btn">
                        <button type="button" class="btn btn-default trigger-calendar" aria-label="Calendar">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </button>
                    </div>
                </div>
                <span class="help-block help-tips" id="error-export_date"></span>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-3 col-md-offset-2" for=""><?= __('TXT_EXPENSE') ?><?= $this->Comment->formAsterisk() ?></label>
            <div class="col-md-4">
                <?php echo $this->Form->input('expense', [
                    'class' => 'form-control',
                    'label' => false,
                    'placeholder' => __('TXT_ENTER_EXPENSE'),
                    'type' => 'text'
                ]); ?>
                <span class="help-block help-tips" id="error-expense"></span>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-3 col-md-offset-2" for=""><?= __('TXT_CURRENCY') ?><?= $this->Comment->formAsterisk() ?></label>
            <div class="col-md-4">
                <?php echo $this->Form->select('currency_id', $currencies, [
                    'class' => 'form-control',
                    'label' => false,
                    'empty' => ['' => __('TXT_SELECT_CURRENCY')]
                ]); ?>
                <span class="help-block help-tips" id="error-currency_id"></span>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-3 col-md-offset-2" for=""><?= __('TXT_STATUS') ?><?= $this->Comment->formAsterisk() ?></label>
            <div class="col-md-4">
                <?php echo $this->Form->select('status', $types, [
                    'class' => 'form-control',
                    'label' => false,
                    'empty' => ['' => __('TXT_SELECT_STATUS')]
                ]); ?>
                <span class="help-block help-tips" id="error-status"></span>
            </div>
        </div>
        <div class="form-group">
            <?= $this->ActionButtons->btnSaveCancel('ImportInternationalProducts', $this->request->query('action')); ?>
        </div>
        <?php echo $this->Form->end(); ?>
    </div>
</div>
<div class="row search-section" style="margin-bottom: 20px;">
    <?php echo $this->element('display_number'); ?>
    <?php
    if ($this->request->query('action') == 'edit') {
        echo $this->ActionButtons->btnRegisterNewNoLink($this->request->query('return_id'), 'register-new-return-item');
    }
    ?>
    <div class="clearfix"></div>
</div>
<div class="data-tb-list">
    <table class="table table-striped">
        <thead>
            <tr>
                <th>#</th>
                <th>
                    <?php echo $this->Paginator->sort('', __('TXT_PRODUCT_NAME')); ?>
                    <i class="fa fa-sort-desc" aria-hidden="true"></i>
                </th>
                <th class="white-space">
                    <?php echo $this->Paginator->sort('', __('TXT_LOT_NUMBER')); ?>
                    <i class="fa fa-sort-desc" aria-hidden="true"></i>
                </th>
                <th class="white-space">
                    <?php echo $this->Paginator->sort('', __('TXT_EXPIRY_DATE')); ?>
                    <i class="fa fa-sort-desc" aria-hidden="true"></i>
                </th>
                <th class="white-space">
                    <?php echo $this->Paginator->sort('', __('TXT_QUANTITY')); ?>
                    <i class="fa fa-sort-desc" aria-hidden="true"></i>
                </th>
                <th class="white-space">
                    <?php echo $this->Paginator->sort('', __('TXT_DAMAGE_LEVEL')); ?>
                    <i class="fa fa-sort-desc" aria-hidden="true"></i>
                </th>
                <th colspan="2">&nbsp;</th>
            </tr>
        </thead>
        <tbody>
            <?php
            if ($imports):
                $numbering = $this->Paginator->counter('{{start}}');
                foreach ($imports as $import):
            ?>
            <tr>
                <th scope="row"><?php echo $numbering; $numbering++; ?></th>
                <td class="pd-detail" data-id="<?= $import->import_detail->product_detail->id ?>">
                    <?php
                    $name = 'name' . $en;
                    $product =  $import->import_detail->product_detail->product->$name;
                    $product .= ' ';
                    $product .= '(';
                    $product .= $import->import_detail->product_detail->pack_size;
                    $product .= $import->import_detail->product_detail->tb_pack_sizes->$name;
                    $product .= ' x ';
                    $product .= $import->import_detail->single_unit_size;
                    $product .= $import->import_detail->product_detail->single_unit->$name;
                    $product .= ')';

                    echo $product;
                    ?>
                </td>
                <td class="lot-num" data-id="<?= $import->import_detail->id ?>">
                    <?= h($import->import_detail->lot_number) ?>
                </td>
                <td class="expiry">
                    <?= date('Y-m-d', strtotime($import->import_detail->expiry_date)) ?>
                </td>
                <td class="qty">
                    <?= $import->quantity; ?>
                </td>
                <td class="level">
                    <?= $import->damage_level; ?>
                </td>
                <td width="1%">
                <?php
                echo $this->Form->button(BTN_ICON_EDIT, [
                    'type' => 'button',
                    'class' => 'btn btn-primary btn-sm btn-edit-details',
                    'escape' => false,
                    'data-id' => $import->id,
                ]);
                ?>
                </td>
                <td width="1%">
                <?php
                echo $this->Form->button(BTN_ICON_DELETE, [
                    'type' => 'button',
                    'class' => 'btn btn-delete btn-sm',
                    'id' => 'btn_delete',
                    'data-id' => $import->id,
                    'escape' => false
                ]);
                ?>
                </td>
            </tr>
            <?php endforeach; endif; ?>
        </tbody>
    </table>
</div>
<div class="row row-top-space">
    <?php echo $this->element('display_number'); ?>
    <?= $this->element('next_prev') ?>
    <div class="clearfix"></div>
</div>

<?php
echo $this->HtmlModal->modalNoHeader('modalRegisterDetail');
?>
<div class="row">
    <div class="col-sm-12 text-center">
        <h3><?= __('New PO') ?></h3>
    </div>
</div>
<div class="row row-top-space">
    <div class="col-sm-12">
        <?php
        echo $this->Form->create(null, [
            'role' => 'form',
            'class' => 'form-horizontal form-return-details',
            'name' => 'common_form'
        ]);

        echo $this->Form->hidden('import_return_id', ['class' => 'import-return-id', 'value' => $this->request->query('return_id')]);
        echo $this->Form->hidden('id', ['class' => 'import-return-detail-id', 'value' => '']);

        $this->Form->templates([
            'inputContainer' => '{{content}}'
        ]);
        ?>
        <div class="form-group">
            <label class="control-label col-md-4" for=""><?= __('TXT_PRODUCT_NAME') ?></label>
            <div class="col-md-8">
                <select class="form-control clear product-detail-id" name="product_detail_id" id="product-detail-id">
                    <?php echo $this->Comment->productDetailsList($en, $product_details); ?>
                </select>
                <span class="help-block help-tips" id="error-product_detail_id"></span>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-4" for=""><?= __('TXT_LOT_EXPIRED') ?></label>
            <div class="col-md-8">
                <?php echo $this->Form->select('import_detail_id', $import_details, [
                    'class' => 'form-control clear import-detail-id',
                    'label' => false,
                    'empty' => ['' => __('TXT_SELECT_LOT_EXPIRED')]
                ]); ?>
                <span class="help-block help-tips" id="error-import_detail_id"></span>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-4" for=""><?= __('TXT_QUANTITY') ?></label>
            <div class="col-md-8">
                <?php echo $this->Form->input('quantity', [
                    'class' => 'form-control clear detail-qty',
                    'label' => false,
                    'placeholder' => __('TXT_ENTER_QUANTITY'),
                    'type' => 'text'
                ]); ?>
                <span class="help-block help-tips" id="error-quantity"></span>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-4" for=""><?= __('TXT_DAMAGE_LEVEL') ?></label>
            <div class="col-md-8">
                <?php echo $this->Form->select('damage_level', $levels, [
                    'class' => 'form-control clear damage-level',
                    'label' => false,
                    'empty' => ['' => __('TXT_SELECT_DAMAGE_LEVEL')]
                ]); ?>
                <span class="help-block help-tips" id="error-damage_level"></span>
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-6 field-small-padding-right text-right">
                <button type="button" class="btn btn-sm btn-default btn-width btn-cancel-details" data-dismiss="modal"><?= __('TXT_CANCEL') ?></button>
            </div>
            <div class="col-sm-6 field-small-padding-left">
                <button type="button" class="btn btn-sm btn-primary btn-width btn-register-details"><?= __('TXT_REGISTER_NEW') ?></button>
            </div>
        </div>
        <?= $this->Form->end() ?>
    </div>
</div>
<?php
echo $this->HtmlModal->modalFooter();
?>

<!--Modal delete-->
<?php echo $this->element('Modal/delete'); ?>

<?php
    echo $this->Html->css([
        'bootstrap-datetimepicker.min',
        'jquery-ui',
        'intlTelInput',
    ]);
?>

<script>
    (function(e) {
        $('.datepicker').datetimepicker({
            format: 'YYYY-MM-DD',
            minDate: '2007-01-01'
        });

        $('.trigger-calendar').click(function() {
            $(this).parents('.with-datepicker').find('.datepicker').focus();
        });

        //modal delete confirm
        $('body').on('click', '#btn_delete', function (e) {
            e.stopPropagation();
            $('#btn_delete_yes').text('<?= __('BTN_DELETE') ?>');
            $('.delete-item').val($(this).attr('data-id'));
            $('#modal_delete').modal('show');
        });

        //modal delete item.
        $('body').on('click', '#btn_delete_yes', function() {
            var data = $('.form-delete').serialize();
            var url = '<?= $this->Url->build('/import-return-details/delete') ?>';
            itemDelete(url, data, '<?php echo __('TXT_SESSION_TIMEOUT') ?>');
        });

        $('#btn-register').on('click', function() {
            $('.help-block').empty().hide();
            var data = $('.form-import-return').serialize();
            var action = 'create';
            <?php
                if ($this->request->query('action') == 'create'):
            ?>
                var url = '<?= $this->Url->build('/import-returns/create'); ?>';
            <?php else: ?>
                action = 'edit';
                var url = '<?= $this->Url->build('/import-returns/edit/'); ?>';
            <?php endif; ?>

            $.ajax({
                url: url,
                type: 'POST',
                dataType: 'JSON',
                data: data,
                cache: false,
                beforeSend: function() {
                    $.LoadingOverlay('show');
                },
                success: function(response) {
                    $.LoadingOverlay('hide');
                    if (response.status === 1) {
                        if (action == 'edit') {
                            location.reload();
                        } else {
                            window.location.href = response.redirect_url;
                        }
                    } else if (response.status === 0) {
                        $.each(response.message, function(key, elem) {
                            var validate = $('#error-' + key);
                            $.each(elem, function(k, v) {
                                validate.removeClass('help-tips')
                                    .addClass('error-tips')
                                    .text(v)
                                    .show();
                            });
                        });
                    }
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    $.LoadingOverlay('hide');
                    if (errorThrown === 'Forbidden') {
                        if (confirm('<?= __('TXT_SESSION_TIMEOUT') ?>')) {
                            location.reload();
                        }
                    }
                }
            });
        });

        $('.register-new-return-item').on('click', function () {
            clearForm();
            $('.btn-cancel-details').removeClass('cancel-details');
            $('.import-return-detail-id').val('');
            $('.btn-register-details').text('<?= __('TXT_REGISTER_NEW') ?>');
            $('#modalRegisterDetail').modal('show');
        });

        function clearForm() {
            $('.help-block').empty().hide();
            $('.clear').val('');
            $('.clear').prop('selectedIndex', 0);
        }

        $('body').on('click', '.cancel-details', function () {
            $('#modalRegisterDetail').modal('hide');
            location.reload();
        });

        $('body').on('click', '.btn-edit-details', function () {
            $('.import-return-detail-id').val($(this).attr('data-id'));
            var $this = $(this).parents('tr');
            $('.damage-level').val($.trim($this.find('.level').text())).change();
            $('.import-detail-id').val($.trim($this.find('.lot-num').attr('data-id'))).change();
            $('.product-detail-id').val($.trim($this.find('.pd-detail').attr('data-id'))).change();
            $('.detail-qty').val($.trim($this.find('.qty').text()));
            $('.btn-register-details').text('<?= __('TXT_UPDATE') ?>');
            $('#modalRegisterDetail').modal('show');
        });

        $('.btn-register-details').on('click', function() {
            $('.help-block').empty().hide();
            var data = $('.form-return-details').serialize();

            var action = 'create';

            var url = '<?= $this->Url->build('/import-return-details/create'); ?>';
            if ($('.import-return-detail-id').val()) {
                action = 'edit';
                url = '<?= $this->Url->build('/import-return-details/edit/'); ?>';
            }

            $.ajax({
                url: url,
                type: 'POST',
                dataType: 'JSON',
                data: data,
                cache: false,
                beforeSend: function() {
                    $.LoadingOverlay('show');
                },
                success: function(response) {
                    $.LoadingOverlay('hide');
                    if (response.status === 1) {
                        if (action === 'edit') {
                            $('#modalRegisterDetail').modal('hide');
                            location.reload();
                        } else {
                            $('.btn-cancel-details').addClass('cancel-details');
                            clearForm();
                        }
                    } else if (response.status === 0) {
                        $.each(response.message, function(key, elem) {
                            var validate = $('#error-' + key);
                            $.each(elem, function(k, v) {
                                validate.removeClass('help-tips')
                                    .addClass('error-tips')
                                    .text(v)
                                    .show();
                            });
                        });
                    }
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    $.LoadingOverlay('hide');
                    if (errorThrown === 'Forbidden') {
                        if (confirm('<?= __('TXT_SESSION_TIMEOUT') ?>')) {
                            location.reload();
                        }
                    }
                }
            });
        });

    })();
</script>
