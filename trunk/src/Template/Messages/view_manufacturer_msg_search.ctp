<?php if (!empty($messages)) : ?>
<table class="table table-condensed">
    <thead>
        <tr>
            <th>#</th>
            <th><?= __('Status') ?></th>
            <th><?= __('Title') ?></th>
            <th><?= __('Charge By') ?></th>
            <th><?= __('Category') ?></th>
            <th><?= __('Date') ?></th>
            <th><?= __('Reply') ?></th>
            <th width="18%">&nbsp;</th>
        </tr>
    </thead>
    <tbody>
        <?php
            $i = 1;
            $badge = 0;
            foreach ($messages as $message) :
                $receiver = __('All');
                $receiver_id = null;
                foreach ($message['receivers'] as $value) {
                    if (!empty($value['user'])) {
                        $receiver = $value['user']->lastname . ' ' . $value['user']->firstname;
                        if ($value['user']->id == $this->request->session()->read('Auth.User.id') && $value['is_read'] == 0) {
                            $badge = $badge + 1;
                        }
                    }
                }
        ?>
        <tr>
            <td><?= $i ?></td>
            <td><?= h($message['status']) ?></td>
            <td><?= h($message['title']) ?></td>
            <td><?= h($receiver) ?></td>
            <td><?= h($message['category']['name_en']) ?></td>
            <td><?= date('Y-m-d', strtotime($message['created'])) ?></td>
            <td>reply to...</td>
            <td>
                <?= $this->Html->link(__('Reply'), ['controller' => 'Messages', 'action' => 'view', '?' => ['external_id' => $id, 'msg_id' => $message['id'], 'type' => TYPE_MANUFACTURER]], ['class' => 'btn btn-sm btn-primary', 'role' => 'button']) ?>&nbsp;
                <?= $this->Html->link(BTN_ICON_EDIT, ['controller' => 'Messages', 'action' => 'edit', $message['id']], ['class' => 'btn btn-primary', 'role' => 'button', 'escape' => false]) ?>&nbsp;
                <?= $this->Html->link(BTN_ICON_STOP, ['controller' => 'Messages', 'action' => 'suspend'], ['class' => 'btn btn-suspend btn-sm', 'role' => 'button', 'escape' => false]) ?>
            </td>
        </tr>
        <?php
            $i++;
            endforeach;
            echo $this->Form->hidden('badge', ['value' => $badge, 'class' => 'personal-message']);
        ?>
    </tbody>
</table>
<?php endif;