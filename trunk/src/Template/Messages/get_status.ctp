
<section class="modal-wrapper">
    <div class="modal fade modal-status" data-backdrop="static" role="dialog" style="z-index: 1200">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <meta name="id" content="<?php echo $id; ?>"/>
                    <meta name="status" content="0"/>
                    <?php
                    echo __('MSG_CONFIRM_DELETE');
                    ?>
                </div>
                <div class="modal-footer">
                    <?php
                    echo $this->Form->button(__('BTN_CANCEL'), [
                        'type' => 'button',
                        'class' => 'btn btn-default btn-sm btn-width modal-close',
                        'data-dismiss' => 'modal',
                        'aria-label' => 'Close',
                    ]);
                    echo $this->Form->button( __('BTN_DELETE'), [
                        'type' => 'button',
                        'class' => 'btn btn-primary btn-sm btn-width change-status',
                    ]);
                    ?>
                </div>
            </div>
        </div>
    </div>
</section>
