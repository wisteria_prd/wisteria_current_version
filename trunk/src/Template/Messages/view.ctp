<style>
    .message-part label {
        font-weight: 200 !important;
        font-size: 16px !important;
    }
    .message-part .text-message-right {
        margin-top: 8px;
    }
    .message-part .text-message-btn-delete {
        margin-top: 15px;
    }
    .table-borderless td,
    .table-borderless th {
        border: 0 !important;
    }
</style>
<div class="row">
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-10">
                <form class="form-horizontal message-part">
                    <div class="form-group">
                        <label class="control-label col-md-1 text-left"><?php echo __d('message', 'SUBJECT'); ?></label>
                        <div class="col-md-8">
                            <div class="text-message-right">
                                <?php if($message->status == 9) : ?>
                                    <span class="btn btn-sm btn-danger"><?php echo __d('message', 'URGENT'); ?></span>
                                <?php endif; ?>

                                <?php echo h($message->title) ?>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-1 text-left"></label>
                        <div class="col-md-5">
                            <div class="text-message-right">
                                <?php echo h($message->description) ?>
                            </div>
                        </div>
                        <div class="col-md-1">
                            <div class="text-message-btn-delete">
                            <?php
                            if ($message->mime_type) {
                                echo $this->Html->link('<i class="fa fa-file"></i>', '/img/uploads/messages/'. $message->file, [
                                    'target' => '__blank',
                                    'class' => 'btn btn-sm btn-primary',
                                    'escape' => false,
                                ]);
                            } ?>
                        </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-1 text-left"><?php echo __d('message', 'CATEGORY'); ?></label>
                        <div class="col-md-8">
                            <div class="text-message-right">
                                <?php echo h($message->message_category->name) ?>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-1 text-left"><?php echo __d('message', 'POSTED'); ?></label>
                        <div class="col-md-8">
                            <div class="text-message-right">

                            <?php
                            $SYSTEM_TYPE = $affiliation_class = $this->request->session()->read('user_group.affiliation_class');
                            // W Account
                            if ($SYSTEM_TYPE === USER_CLASS_WISTERIA) {
                                echo h($message->user->lastname . ' ' . $message->user->firstname). ', '.date('Y-m-d', strtotime($message->created));
                            } else {
                            // MP Account
                                echo h($message->user->firstname . ' ' . $message->user->lastname). ', '.date('Y-m-d', strtotime($message->created));
                            }
                             ?>
                         </div>
                        </div>
                    </div>
                </form>
            </div>
            <?php
            // CHECK AFFILIATION_CLASS OR USER_ID
            $affiliation_class = $this->request->session()
                    ->read('user_group.affiliation_class');
            ?>
        </div>
        <?php if (false): //  TODO: show descript part below ?>
        <div class="row">
            <div class="col-md-12">
                <p><?php echo h($message->description) ?></p>
            </div>
        </div>
        <?php endif; ?>
        <hr />
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-12">
                <?php
                // DISPLAY COMMENTS LIST
                echo $this->element('Messages/comment_reply');
                ?>
                <div style="padding-top: 30px;"></div>
            </div>
            <div class="col-md-12">
                <h2><?php echo __d('message', 'REPLY'); ?></h2>
            </div>
        </div>
        <div class="clearfix"></div>
        <div>
            <?php
            echo $this->Form->create(null, [
                'class' => 'form-horizontal',
                'autocomplete' => 'off',
                'type' => 'file',
                'name' => 'form_reply',
                'onsubmit' => 'return false;',
            ]);
            echo $this->Form->hidden('message_id', [
                'value' => $message->id,
            ]);
            ?>
            <div class="form-group">
                <div class="col-md-6">
                    <?php
                    echo $this->Form->textarea('description', [
                        'class' => 'form-control reply-description',
                        'placeholder' => __d('message', 'WRITE_YOUR_COMMENT')
                    ]);
                    ?>
                </div>
                <div class="col-md-2">
                    <?php
                    echo $this->Form->button( __d('message', 'SUBMIT'), [
                        'class' => 'btn-sm btn btn-primary btn-add-comment'
                    ]);
                    ?>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-10 file-wrap">
                    <?php echo $this->Form->file('file', array(
                        'style' => 'display:none',
                        'id' => 'btn-comment-image',
                        'accept' => 'image/*, .pdf, .docx',
                    )) ?>
                    <button type="button" style="width: 120px;height: 25px;" onclick="document.getElementById('btn-comment-image').click()">
                        <?php echo __('COMMENT_TXT_CHOOSE_UPLOAD'); ?>
                    </button>
                  <span class="text-right comment-no-file-selected"><?php echo __('COMMENT_TXT_NO_FILE_SELECTED');?></span>
                </div>
            </div>
            <br /></br />
            <?php echo $this->Form->end() ?>
        </div>
    </div>
</div>
<script>
    var COMMENT_TXT_SELECT_USER = '<?php echo __('COMMENT_TXT_SELECT_USER'); ?>';
    var selected_receivers = [];
    $(function() {
        $('#btn-comment-image').on('change', function() {
            $('.comment-no-file-selected').text($(this).val());
        });
    });
</script>
<?php
echo $this->Html->css([
    'bootstrap-datetimepicker.min',
]);
echo $this->Html->script([
    'messages/view',
], ['blog' => false]);
