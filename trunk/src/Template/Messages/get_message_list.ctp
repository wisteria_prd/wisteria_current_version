
<section class="modal-wrapper">
    <div class="modal fade modal-message" data-backdrop="static" role="dialog">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <?php
                    echo $this->Form->create(null, [
                        'autocomplete' => 'off',
                        'type' => 'get',
                        'onsubmit' => 'return false;',
                    ]);
                    ?>
                    <meta name="id" content="<?php echo $id; ?>"/>
                    <meta name="type" content="<?php echo $type; ?>"/>
                    <div class="row tab-content">
                        <div class="col-sm-4 col-sm-offset-2 text-right">
                            <?php
                            echo $this->Form->button(__('STR_ALL_COMMENTS'), [
                                'class' => 'btn btn-primary btn-block message-tab',
                                'type' => 'button',
                                'data-tab' => MESSAGE_TYPE,
                            ]);
                            ?>
                        </div>
                        <div class="col-sm-4">
                            <?php
                            echo $this->Form->button(__('STR_COMMENTS_ONLY_U') . ' <span class="badge personal-badge">' . $receiverCount . '</span>', [
                                'class' => 'btn btn-default btn-block receiver-tab',
                                'type' => 'button',
                                'escape' => false,
                                'data-tab' => RECEIVER_TYPE,
                            ]);
                            ?>
                        </div>
                    </div>
                    <div class="row row-border-top"></div>
                    <div class="row row-top-space">
                        <div class="col-sm-5 col-sm-offset-1">
                            <?php
                            echo $this->Form->text('keyword', [
                                'class' => 'form-control',
                                'placeholder' => __('USER_SEARCH_KEYWORD'),
                            ]);
                            ?>
                        </div>
                        <div class="col-sm-3 custom-select">
                            <?php
                            echo $this->Form->select('status', array_map('__', unserialize(MESSAGE_STATUS_TYPE)), [
                                'class' => 'form-control',
                                'empty' => ['-1' => __('TXT_STATUS')]
                            ]);
                            ?>
                        </div>
                        <div class="col-sm-2">
                            <?php
                            echo $this->Form->button(__('TXT_FIND'), [
                                'class' => 'btn btn-primary btn-search btn-width',
                                'type' => 'button',
                            ]);
                            ?>
                        </div>
                    </div>
                    <?php echo $this->Form->end() ?>
                    <div class="messages">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th><?= __('STR_USER_STATUS') ?></th>
                                    <th><?= __('COMMENT_TXT_SUBJECT') ?></th>
                                    <th><?= __('COMMENT_TXT_COMMENT') ?></th>
                                    <th><?= __('STR_COMMENTS_CATEGORY') ?></th>
                                    <th><?= __('DATE_CREATED') ?></th>
                                    <th><?= __('STR_COMMENTS_REPLY') ?></th>
                                    <th width="18%">&nbsp;</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                // DISPLAY DATA TAB BASED ON TYPE
                                echo $this->element('Messages/get_message_tab', [
                                    'data' => $data,
                                ]);
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="modal-footer">
                    <?php
                    echo $this->Form->button(__('TXT_CANCEL'), [
                        'type' => 'button',
                        'class' => 'btn btn-default btn-width modal-close',
                        'data-dismiss' => 'modal',
                        'aria-label' => 'Close',
                    ]);
                    echo $this->Html->link(__('COMMENT_BTN_REGISTER'), [
                        'controller' => 'Messages',
                        'action' => 'register',
                        '?' => [
                            'external_id' => $id,
                            'type' => $type,
                            'referer' => $referer,
                        ]], [
                            'class' => 'btn btn-primary btn-width',
                            'role' => 'button',
                        ]
                    );
                    ?>
                </div>
            </div>
        </div>
    </div>
    <?php
    echo $this->Html->script([
        'messages/message',
    ], ['blog' => false]);
    ?>
</section>
