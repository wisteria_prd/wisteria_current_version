<?php
switch ($tab) {
    case MESSAGE_TYPE:
        echo $this->element('Messages/get_message_tab', [
            'data' => $data,
        ]);
        break;
    default:
        echo $this->element('Messages/get_receiver_tab', [
            'data' => $data,
        ]);
}