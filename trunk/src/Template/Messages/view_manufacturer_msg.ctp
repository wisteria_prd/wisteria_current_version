<?php if (!empty($messages)) : ?>
<?php
    echo $this->Form->create(null, [
        'autocomplete' => 'off',
        'type' => 'get',
        'class' => 'form-search'
    ]);
    $this->Form->templates([
        'inputContainer' => '{{content}}'
    ]);
    echo $this->Form->hidden('to_all', ['class' => 'to-all', 'value' => 0]);
    echo $this->Form->hidden('to_individual', ['class' => 'to-individual', 'value' => 0]);
    echo $this->Form->hidden('id', ['class' => 'manu-id', 'value' => $id]);
?>
<div class="row">
    <div class="col-sm-3 col-sm-offset-3 text-right">
        <button type="button" class="btn btn-info btn-block btn-to-all"><?= __('All') ?></button>
    </div>
    <div class="col-sm-3">
        <button type="button" class="btn btn-default btn-block btn-to-individual"><?= __('Your Charge') ?> <span class="badge personal-badge">0</span></button>
    </div>
</div>
<div class="row row-top-space">
    <div class="col-sm-3">
        <?php
        echo $this->Form->input('keyword', [
            'id' => 'keyword',
            'class' => 'form-control',
            'label' => false
        ]);
        ?>
    </div>
    <div class="col-sm-2">
        <?php
        echo $this->Form->select('status', unserialize(MESSAGES_STATUS), [
            'id' => 'title',
            'class' => 'form-control',
            'label' => false,
            'empty' => ['' => __('Status')]
        ]);
        ?>
    </div>
    <div class="col-sm-1">
        <button type="button" class="btn btn-primary btn-search"><?= __('Search') ?></button>
    </div>
    <?= $this->Form->end() ?>
    <div class="col-sm-offset-3 col-sm-3 text-right">
        <?php
        echo $this->Html->link(__('Register'), [
            'controller' => 'Messages',
            'action' => 'register',
            '?' => ['external_id' => $id, 'type' => TYPE_MANUFACTURER]],
            ['class' => 'btn btn-primary', 'role' => 'button']
        );
        ?>
    </div>
</div>
<div class="row row-top-space">
    <div class="col-sm-12">
        <div class="messages">
            <table class="table table-condensed">
                <thead>
                    <tr>
                        <th>#</th>
                        <th><?= __('Status') ?></th>
                        <th><?= __('Title') ?></th>
                        <th><?= __('Charge By') ?></th>
                        <th><?= __('Category') ?></th>
                        <th><?= __('Date') ?></th>
                        <th><?= __('Reply') ?></th>
                        <th width="18%">&nbsp;</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        $i = 1;
                        $badge = 0;
                        foreach ($messages as $message) :
                            $receiver = __('All');
                            $receiver_id = null;
                            foreach ($message['receivers'] as $value) {
                                if (!empty($value['user'])) {
                                    $receiver = $value['user']->lastname . ' ' . $value['user']->firstname;
                                    if ($value['user']->id == $this->request->session()->read('Auth.User.id') && $value['is_read'] == 0) {
                                        $badge = $badge + 1;
                                    }
                                }
                            }
                    ?>
                    <tr>
                        <td><?= $i ?></td>
                        <td><?= h($message['status']) ?></td>
                        <td><?= h($message['title']) ?></td>
                        <td><?= h($receiver) ?></td>
                        <td><?= h($message['category']['name_en']) ?></td>
                        <td><?= date('Y-m-d', strtotime($message['created'])) ?></td>
                        <td>reply to...</td>
                        <td>
                            <?= $this->Html->link(__('Reply'), ['controller' => 'Messages', 'action' => 'view', '?' => ['external_id' => $id, 'msg_id' => $message['id'], 'type' => TYPE_MANUFACTURER]], ['class' => 'btn btn-sm btn-primary', 'role' => 'button']) ?>&nbsp;
                            <?= $this->Html->link(BTN_ICON_EDIT, ['controller' => 'Messages', 'action' => 'edit', $message['id']], ['class' => 'btn btn-primary', 'role' => 'button', 'escape' => false]) ?>&nbsp;
                            <?= $this->Html->link(BTN_ICON_STOP, ['controller' => 'Messages', 'action' => 'suspend'], ['class' => 'btn btn-suspend btn-sm', 'role' => 'button', 'escape' => false]) ?>
                        </td>
                    </tr>
                    <?php
                        $i++;
                        endforeach;
                        echo $this->Form->hidden('badge', ['value' => $badge, 'class' => 'personal-message']);
                    ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
<div class="row row-top-space">
    <div class="col-sm-12 text-center">
        <button type="button" class="btn btn-default" data-dismiss="modal"><?= __('Close') ?></button>
    </div>
</div>
<?php else: ?>
<?php
    echo $this->Form->create(null, [
        'autocomplete' => 'off',
        'type' => 'get',
        'class' => 'form-search'
    ]);
    $this->Form->templates([
        'inputContainer' => '{{content}}'
    ]);
    echo $this->Form->hidden('to_all', ['class' => 'to-all', 'value' => 0]);
    echo $this->Form->hidden('to_individual', ['class' => 'to-individual', 'value' => 0]);
?>
<div class="row">
    <div class="col-sm-3 col-sm-offset-3 text-right">
        <button type="button" class="btn btn-info btn-block btn-to-all"><?= __('All') ?></button>
    </div>
    <div class="col-sm-3">
        <button type="button" class="btn btn-default btn-block btn-to-individual"><?= __('Your Charge') ?> <span class="badge personal-badge">0</span></button>
    </div>
</div>
<div class="row row-top-space">
    <div class="col-sm-3">
        <?php
        echo $this->Form->input('keyword', [
            'id' => 'keyword',
            'class' => 'form-control',
            'label' => false
        ]);
        ?>
    </div>
    <div class="col-sm-2">
        <?php
        echo $this->Form->select('status', unserialize(MESSAGES_STATUS), [
            'id' => 'title',
            'class' => 'form-control',
            'label' => false,
            'empty' => ['' => __('Status')]
        ]);
        ?>
    </div>
    <div class="col-sm-1">
        <button type="button" class="btn btn-primary btn-search"><?= __('Search') ?></button>
    </div>
    <?= $this->Form->end() ?>
    <div class="col-sm-offset-3 col-sm-3 text-right">
        <?= $this->Html->link(__('Register'), ['controller' => 'Messages', 'action' => 'register', '?' => ['manu_id' => $id]], ['class' => 'btn btn-primary', 'role' => 'button']) ?>
    </div>
</div>
<div class="row row-top-space">
    <div class="col-sm-12">
        <div class="messages"></div>
    </div>
</div>
<div class="row row-top-space">
    <div class="col-sm-12 text-center">
        <button type="button" class="btn btn-default" data-dismiss="modal"><?= __('Close') ?></button>
    </div>
</div>
<?php endif;
