
<section class="modal-wrapper">
    <div class="modal fade modal-society" data-backdrop="static" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <?php
                    echo $this->Form->button('<span aria-hidden="true">&times;</span>', [
                        'type' => 'button',
                        'class' => 'close modal-close',
                        'data-dismiss' => 'modal',
                        'aria-label' => 'Close',
                        'escape' => false,
                    ]); ?>
                    <h4 class="modal-title text-center">
                        <?php
                        echo __('STR_SOCIETY_MODAL_TITLE'); ?>
                    </h4>
                </div>
                <div class="modal-body">
                    <div class="form-wrapper">
                        <?php
                        echo $this->Form->create($data, [
                            'class' => 'form-horizontal',
                            'name' => 'society_form',
                            'role' => 'form',
                            'onsubmit' => 'return false;',
                        ]) ?>
                        <div class="form-group">
                            <div class="col-md-6">
                                <label>
                                    <?php
                                    echo __('STR_SOCIETY') ?>
                                </label>
                            </div>
                        </div>
                        <div class="col-lg-9 col-md-9" style="padding: 0;">
                            <div class="form-group">
                                <div class="col-sm-6" style="padding-right: 0;">
                                    <?php
                                    echo $this->Form->text('name', [
                                        'class' => 'form-control',
                                        'required' => false,
                                        'placeholder' => __('SOCIETY_ENTER_SOCIETY_NAME_JP'),
                                    ]); ?>
                                </div>
                                <div class="col-sm-6">
                                    <?php
                                    echo $this->Form->text('name_en', [
                                        'class' => 'form-control',
                                        'required' => false,
                                        'placeholder' => __('SOCIETY_ENTER_SOCIETY_NAME_EN'),
                                    ]); ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <?php
                                    echo $this->Form->text('url', [
                                        'class' => 'form-control',
                                        'required' => false,
                                        'placeholder' => __('SOCIETY_ENTER_WEBSITE_URL_OF_SOCIETY'),
                                    ]); ?>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3">
                            <div class="form-group">
                                <div class="col-sm-3">
                                    <?php
                                    echo $this->Form->button(__('TXT_REGISTER_SAVE'), [
                                    'class' => 'btn btn-width btn-primary btn-sm btn_tmp_save',
                                    'data-target' => TARGET_NEW,
                                    ]); ?>
                                </div>
                            </div>
                        </div>
                        <?php echo $this->Form->end(); ?>
                    </div>
                    <div class="table-wrapper">
                        <table class="table table-striped society-table">
                            <thead>
                                <tr>
                                    <th>&nbsp;</th>
                                    <th>#</th>
                                    <th><?= __('STR_SOCIETY_JP') ?></th>
                                    <th><?= __('STR_SOCIETY_EN') ?></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                echo $this->element('Societies/get_list'); ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="modal-footer">
                    <?php
                    echo $this->Form->button(__('TXT_CANCEL'), [
                        'type' => 'button',
                        'class' => 'btn btn-default btn-sm btn-width modal-close',
                        'data-dismiss' => 'modal',
                        'aria-label' => 'Close',
                    ]);
                    echo $this->Form->button(__('OK'), [
                        'type' => 'button',
                        'class' => 'btn btn-primary btn-sm btn-width checked-society modal-close',
                        'data-dismiss' => 'modal',
                        'aria-label' => 'Close',
                    ]);
                    ?>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(function(e) {
            $('.btn_tmp_save').click(function(e) {
                var form = $('form[name="society_form"]');
                var params = {
                    type: 'POST',
                    url: BASE + 'Societies/saveOrUpdate',
                    data: $(form).serialize(),
                    beforeSend: function() {
                        $.LoadingOverlay('show');
                        $(form).find('.error-message').remove();
                    }
                };
                ajaxRequest(params, SESSION_TIMEOUT, function(data) {
                    if (data.message === MSG_ERROR) {
                        showMessageError(form, data.data);
                        return false;
                    } else {
                        displayList(data.data);
                        $(form).find('input:text').val('');
                    }
                });
            });

            function displayList(data)
            {
                if ((data === null) && (data === 'undefined')) {
                    return false;
                }
                var element = '';
                $.each (data, function(index, value) {
                    element += '<tr data-id="' + value.id + '">' +
                            '<td><input type="checkbox" name="check_item" value="' + value.id + '" data-name="' + value.name + '"></td>' +
                            '<td>' + (index + 1) + '</td>' +
                            '<td>' + value.name + '</td>' +
                            '<td>' + value.name_en + '</td>' +
                        '</tr>';
                });
                $('body').find('.society-table > tbody').html(element);
            }
        });
    </script>
</section>
