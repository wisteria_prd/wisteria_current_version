
<section class="modal-wrapper">
    <div class="modal fade modal-view" data-backdrop="static" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <table class="table table-striped" id="tb-detail">
                        <tbody>
                            <tr>
                                <td class="white-space">
                                    <?php
                                    echo __d('pricing', 'TXT_SUPPLIER'); ?> :
                                </td>
                                <td colspan="2" class="white-space">
                                    <?php
                                    switch (isset($data->product_detail->product->product_brand->seller_brands[0]->seller->type) ? $data->product_detail->product->product_brand->seller_brands[0]->seller->type : '') {
                                        case TYPE_MANUFACTURER:
                                            echo $this->Comment->getFieldByLocal($data->product_detail->product->product_brand->seller_brands[0]->seller->manufacturer, $locale);
                                            break;

                                        case TYPE_SUPPLIER:
                                            echo $this->Comment->getFieldByLocal($data->product_detail->product->product_brand->seller_brands[0]->seller->supplier, $locale);
                                            break;
                                    } ?>
                                </td>
                            </tr>
                            <tr>
                                <td class="white-space">
                                    <?php
                                    echo __d('pricing', 'TXT_MANUFACTURER'); ?> :
                                </td>
                                <td colspan="2" class="white-space">
                                    <?php
                                    if ($data->product_detail->product->product_brand->manufacturer) {
                                        echo $this->Comment->getFieldByLocal($data->product_detail->product->product_brand->manufacturer, $locale);
                                    } ?>
                                </td>
                            </tr>
                            <tr>
                                <td class="white-space">
                                    <?php
                                    echo __d('pricing', 'TXT_PRODUCT'); ?> :
                                </td>
                                <td colspan="2" class="white-space">
                                    <?php
                                    echo $this->Comment->getFieldByLocal($data->product_detail->product->product_brand, $locale);
                                    echo ' '.$this->Comment->getFieldByLocal($data->product_detail->product, $locale);
                                    echo ' '.$this->Comment->getFieldByLocal($data->product_detail, $locale);
                                    ?>
                                </td>
                            </tr>
                            <tr>
                                <td class="white-space">
                                    <?php
                                    echo __d('pricing', 'TXT_TYPE'); ?> :
                                </td>
                                <td colspan="2" class="white-space">
                                    <?php echo $data->price_type; ?>
                                </td>
                            </tr>
                            <tr>
                                <td class="white-space">
                                    <?php
                                    echo __d('pricing', 'TXT_VALUE'); ?> :
                                </td>
                                <td colspan="2" class="white-space">
                                   <?php
                                   switch (isset($data->pricing_range_management->value_type) ? $data->pricing_range_management->value_type : '') {
                                       case 'amount':
                                           if (isset($data->pricing_range_management->currency->code_en)) {
                                               echo $this->Currency->format($data->pricing_range_management->currency->code_en, $data->pricing_range_management->value);
                                           }
                                           break;

                                       case 'percent':
                                           echo  $data->pricing_range_management->value. '%';
                                           break;
                                   } ?>
                                </td>
                            </tr>
                            <tr>
                                <td class="white-space">
                                    <?php
                                    echo __d('pricing', 'TXT_RANGE'); ?> :
                                </td>
                                <td colspan="2" class="white-space">
                                    <?php
                                    switch (isset($data->pricing_range_management->range_type) ? $data->pricing_range_management->range_type : '') {
                                        case 'amount':
                                            $range_from = '';
                                            if (isset($data->pricing_range_management->currency->code_en)) {
                                                if (isset($data->pricing_range_management->range_from)) {
                                                    $range_from =  $this->Currency->format($data->pricing_range_management->currency->code_en, $data->pricing_range_management->range_from);
                                                }
                                            }
                                            echo $range_from;
                                            echo isset($data->pricing_range_management->range_to) ? ' ~ '.$data->pricing_range_management->range_to : '';
                                            break;

                                        case 'quantity':
                                            echo isset($data->pricing_range_management->range_from) ? $data->pricing_range_management->range_from : '';
                                            echo isset($data->pricing_range_management->range_to) ? ' ~ '.$data->pricing_range_management->range_to : '';
                                            break;
                                    } ?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <?php
                                    echo __d('manufacturer', 'TXT_MODIFIED'); ?> :
                                </td>
                                <td colspan="2">
                                    <?php
                                    echo $data->modified; ?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <?php
                                    echo __d('manufacturer', 'TXT_REGISTERD'); ?> :
                                </td>
                                <td colspan="2">
                                    <?php
                                    echo $data->created; ?>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="modal-footer" style="text-align: center !important;">
                    <?php
                    echo $this->Form->button(__('MANUFACTURERS_TXT_CANCEL'), [
                        'type' => 'button',
                        'class' => 'btn btn-default btn-sm  btn-close-modal btn-width modal-close',
                        'data-dismiss' => 'modal',
                        'aria-label' => 'Close',
                    ]);
                    ?>
                </div>
            </div>
        </div>
    </div>
</section>
