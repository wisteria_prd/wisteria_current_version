<option value=""><?php echo __d('pricing', 'TXT_SELECT_PRODUCT_BRAND'); ?></option>
<?php
foreach ($data->seller_brands as $key => $value) : ?>
<option value="<?php echo $value->product_brand->id; ?>"><?php  echo $this->Comment->getFieldByLocal($value->product_brand, $locale); ?></option>
<?php
endforeach;
