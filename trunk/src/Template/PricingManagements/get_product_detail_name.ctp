<option value=""><?php echo __d('pricing', 'TXT_SELECT_PRODUCT_NAME'); ?></option>
<?php
foreach ($data->products as $key => $value) :
    if (!isset($value->product_details[0]->id)) {
        continue;
    }
?>
<option value="<?php echo $value->product_details[0]->id; ?>"><?php  echo $this->Comment->getFieldByLocal($value, $locale); ?></option>
<?php
endforeach;
