<?php
    $select = [];
    $i = 1;
    $min_range = 0;
    $max_range = 0;
    $product_name = '';
    $name = 'name' . $en;
    if (isset($discounts) && !empty($discounts)) {
        $table_row = '';
        foreach ($discounts as $product) {
            $product_name = $this->Comment->nameEnOrJp($product->product_detail->product->name, $product->product_detail->product->name_en, $en);
            $detail = $product_name . ' ';
            $detail .= $this->Comment->sizeAndUnit($product->product_detail, $en);

            if ($product->foc_discount_value >= $max_range) {
                $max_range = $product->foc_discount_value;
            }
            if ($i == 1) {
                $min_range = $product->foc_discount_value;                
            } else {
                if ($product->foc_discount_value <= $min_range) {
                    $min_range = $product->foc_discount_value;
                }
            }

            $table_row .= '<tr class="foc-'.$product->id.'">';
                $table_row .= '<th>' .$i. '</th>';
                $table_row .= '<td>' .__('Quantity'). '</td>';
                $table_row .= '<td class="discount-value">' .$product->foc_discount_value. '</td>';
                $table_row .= '<td>' .$detail. '</td>';
                $table_row .= '<td>';
                $table_row .= $this->Form->button(BTN_ICON_EDIT, [
                    'type' => 'button',
                    'class' => 'btn btn-primary btn-sm btn-edit',
                    'escape' => false,
                    'data-id' => $product->id,
                    'data-detail-id' => $product->product_detail->id
                ]);
                $table_row .= '&nbsp;&nbsp;';
                $table_row .= $this->Form->button(BTN_ICON_DELETE, [
                    'type' => 'button',
                    'class' => 'btn btn-delete btn-sm btn-delete-discount',
                    'id' => false,
                    'escape' => false,
                    'data-id' => $product->id,
                    'data-detail-id' => $product->product_detail->id
                ]);
                $table_row .= '</td>';
            $table_row . '</tr>';

            $i++;
        }
    }
    
    if ($detail_list) {
        foreach ($detail_list as $product) {
            if ($product->product_details) {                
                foreach ($product->product_details as $p) {
                    $detail = $this->Comment->nameEnOrJp($product->name, $product->name_en, $en);
                    $detail .= $this->Comment->sizeAndUnit($p, $en);
                    $select[$p->id] = $detail;
                }
            }
        }
    }
?>
<div class="row">
    <div class="col-sm-12">
        <h2 class="text-center"><?= __('TXT_DISCOUNT_ITEM') ?></h2>
        <p class="text-center"><?php echo __('TXT_FOC_OF') . ' ' . $product_name; ?></p>
        <p class="text-center"><?php echo __('TXT_RANGE_FROM') . ' ' . $min_range . ' ' . __('STR_TO') . ' ' . $max_range; ?></p>
    </div>
</div>
<div class="row row-top-space">
    <div class="col-sm-12">
        <?php
        echo $this->Form->create(null, [
            'role' => 'form',
            'class' => 'form-horizontal form-foc',
            'name' => 'common_form'
        ]);

        $this->Form->templates([
            'inputContainer' => '{{content}}'
        ]);
        echo $this->Form->hidden('id', ['class' => 'foc-discount-id', 'value' => '']);
        echo $this->Form->hidden('type', ['class' => 'foc-type', 'value' => $type]);
        echo $this->Form->hidden('external_id', ['class' => 'foc-external-id', 'value' => $external_id]);
        echo $this->Form->hidden('foc_discount_type', ['class' => 'foc-discount-type', 'value' => TYPE_QUANTITY]);
        ?>
        <div class="form-group">
            <label class="col-sm-2 control-label"><?= __('TXT_TYPE') ?></label>
            <div class="col-sm-4 field-small-padding-right">
                <?php echo $this->Form->select('discount_type', ['quantity' => __('Quantity')], [
                    'class' => 'form-control',
                    'label' => false,
                    'required' => false,
                    'empty' => false,
                    'disabled' => true,
                    'id' => 'type'
                ]); ?>
                <span class="help-block help-tips" id="error-discount_type"></span>
            </div>
        </div>
        <div class="form-group custom-select">
            <label class="col-sm-2 control-label"><?= __('STR_VALUE') ?></label>
            <div class="col-sm-4 field-small-padding-right">
                <?php echo $this->Form->input('foc_discount_value', [
                    'class' => 'form-control',
                    'label' => false,
                    'required' => false,
                    'id' => 'value'
                ]); ?>
                <span class="help-block help-tips" id="error-foc_discount_value"></span>
            </div>
            <div class="col-sm-4 field-small-padding-left field-small-padding-right">
                <?php echo $this->Form->select('product_detail_id', $select, [
                    'class' => 'form-control',
                    'label' => false,
                    'required' => false,
                    'id' => 'product',
                    'empty' => ['' => __('TXT_SELECT_PRODUCT_NAME')]
                ]); ?>
                <span class="help-block help-tips" id="error-product_detail_id"></span>
            </div>
            <div class="col-sm-2 field-small-padding-left">
                <button type="button" class="btn btn-sm btn-width btn-primary btn-foc-discount"><?= __('STR_ADD') ?></button>
            </div>
        </div>
        <?php echo $this->Form->end() ?>
    </div>
</div>
<div class="row row-top-space">
    <div class="col-sm-12">
        <div class="detail-list">
            <table class="table table-condensed table-striped reindex table-foc-discount">
                <thead>
                    <tr>
                        <th>#</th>
                        <th><?= __('TXT_TYPE') ?></th>
                        <th><?= __('STR_VALUE') ?></th>
                        <th><?= __('TXT_PRODUCT_NAME') ?></th>
                        <th width="15.5%">&nbsp;</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    echo $table_row;
                    ?>
                </tbody>
            </table>
        </div>
    </div>
</div>