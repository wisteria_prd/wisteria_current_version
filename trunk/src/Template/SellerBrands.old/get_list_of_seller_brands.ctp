
<section class="modal-wrapper">
    <div class="modal fade modal-seller-brand" data-backdrop="static" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <?php
                    echo $this->Form->button('<span aria-hidden="true">&times;</span>', [
                        'type' => 'button',
                        'class' => 'close modal-close',
                        'data-dismiss' => 'modal',
                        'aria-label' => 'Close',
                        'escape' => false,
                    ]);
                    ?>
                    <h4 class="modal-title text-center"><?= __('TXT_CREATE') ?></h4>
                </div>
                <div class="modal-body">
                    <meta name="seller_id" content="<?php echo $seller->id; ?>"/>
                    <?php
                    echo $this->Form->create(null, [
                        'role' => 'form',
                        'class' => 'form-horizontal',
                        'name' => 'product_brand_form',
                        'onsubmit' => 'return false;',
                        'autocomplete' => 'off',
                    ]);
                    ?>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">
                            <?php echo __('TXT_SELLER_BRAND'); ?>
                        </label>
                        <div class="col-sm-5 custom-select">
                            <?php
                            $brand1 = [];
                            $value = '';
                            if (isset($seller->seller_brands) && $seller->seller_brands) {
                                $brand1[] = [
                                    'value' => $seller->seller_brands[0]->product_brand_id,
                                    'text' => $seller->seller_brands[0]->product_brand->full_name,
                                ];
                                $value = $seller->seller_brands[0]->product_brand_id;
                            }
                            echo $this->Form->select('product_brand_id[0]', $brand1, [
                                'class' => 'form-control select-brand',
                                'empty' => ['' => __('TXT_SELECT_BRAND_NAME')],
                                'value' => $value,
                            ]);
                            ?>
                        </div>
                        <div class="col-sm-2">
                            <?php
                            echo $this->Form->button('<i class="fa fa-plus" aria-hidden="true"></i>', [
                                'type' => 'button',
                                'class' => 'btn btn-sm btn-primary btn-prd-brand',
                                'escape' => false,
                            ])
                            ?>
                        </div>
                    </div>
                    <div class="form-group-wrap">
                        <?php
                        echo $this->element('SellerBrands/product_brands_list');
                        ?>
                    </div>
                    <?php echo $this->Form->end(); ?>
                </div>
                <div class="modal-footer">
                    <?php
                    echo $this->Form->button(__('TXT_CANCEL'), [
                        'type' => 'button',
                        'class' => 'btn btn-default btn-sm btn-width modal-close',
                        'data-dismiss' => 'modal',
                        'aria-label' => 'Close',
                    ]);
                    echo $this->Form->button(__('TXT_REGISTER'), [
                        'type' => 'button',
                        'class' => 'btn btn-primary btn-sm btn-width btn-prd-brand-register',
                    ]);
                    ?>
                </div>
            </div>
        </div>
    </div>
    <?php
    echo $this->Html->script([
        'seller-brands/seller-brand',
    ], ['blog' => false]);
    ?>
</section>
