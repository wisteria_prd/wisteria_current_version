<?php
if (isset($product_details) && !empty($product_details)) {
    $i = 1;
    foreach ($product_details as $details) { ?>
<tr>
    <td>
        <?php
        echo $this->Form->checkbox('use', [
            'hiddenField' => false,
            'class' => 'check-product',
            'data-id' => $details->id
        ]);
        ?>
    </td>
    <td><?= $i ?></td>
    <td class="product-name">
        <?php
        echo h($product_name);
        ?>
    </td>
    <td><?= $this->Comment->sizeAndUnit($details, $en); ?></td>
    <?php if ($pricing): ?>
    <td>
        <?php
        echo $this->Form->button(__('TXT_DETAIL'), [
            'type' => 'button',
            'class' => 'btn btn-sm btn-primary btn-view-details',
            'data-id' => $details->id
        ]);
        ?>
    </td>
    <?php endif; ?>
</tr>
<?php
    $i++;
    }
}