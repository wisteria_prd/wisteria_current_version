<?php
if ($products):
    $output = '<option value="">' . __('TXT_SELECT_PRODUCT_NAME') . '</option>';
    if ($type == TYPE_PRODUCT_DETAILS) {
        foreach ($products as $product) {
            $detail = $this->Comment->brandAndProducts($product, $en);
            $detail .= $this->comment->sizeAndUnit($product, $en);
            $output .= '<option value="' . $product->id . '">' . $detail . '</option>';
        }
    } else {
        foreach ($products as $product) {
            $name = $this->Comment->nameEnOrJp($product->name, $product->name_en, $en);
            $output .= '<option value="' . $product->id . '">' . $name . '</option>';
        }
    }
    echo $output;
endif;