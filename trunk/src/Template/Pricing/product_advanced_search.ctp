<div class="row">
    <div class="col-sm-12">
        <h3 class="text-center"><?= __('TXT_ADVANCED_FIND_PRODUCTS') ?></h3>
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <?php
            echo $this->Form->create(null, [
                'role' => 'form',
                'class' => 'form-horizontal form-product-list-search',
                'name' => 'common_form'
            ]);
            $this->Form->templates([
                'inputContainer' => '{{content}}'
            ]);
        ?>
        <div class="form-group">
            <label class="col-sm-4 control-label"><?= __('TXT_MANUFACTURER_NAME') ?></label>
            <div class="col-sm-7">
                <?php
                $manuf = [];
                if ($manufacturers) {
                    foreach ($manufacturers as $m) {
                        $manuf[$m->id] = $this->Comment->nameEnOrJp($m->name, $m->name_en, $en);
                    }
                }
                echo $this->Form->select('manufacturer_id', $manuf, [
                    'class' => 'form-control',
                    'label' => false,
                    'required' => false,
                    'id' => 'select-manufacturer',
                    'empty' => ['' => __('TXT_SELECT_MANUFACTURER')]
                ]);
                ?>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-4 control-label"><?= __('TXT_BRAND_NAME') ?></label>
            <div class="col-sm-7">
                <?php echo $this->Form->select('product_brand_id', [], [
                    'class' => 'form-control',
                    'label' => false,
                    'required' => false,
                    'id' => 'select-product-brand',
                    'empty' => ['' => __('TXT_SELECT_BRAND_NAME')],
                    'disabled' => true
                ]); ?>
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-11">
                <h4 class="text-center"><?= __('Found') ?> <span class="amount-found">0</span> <?= __('products') ?></h4>
            </div>
        </div>
        <div class="form-group custom-select">
            <label class="col-sm-4 control-label"><?= __('TXT_PRODUCT_NAME') ?></label>
            <div class="col-sm-7">
                <?php
                    $pd = [];
                    if ($products) {
                        foreach ($products as $p) {
                            $pd[$p->id] = $this->Comment->nameEnOrJp($p->name, $p->name_en, $en);
                        }
                    }
                    echo $this->Form->select('product_id', $pd, [
                        'class' => 'form-control',
                        'label' => false,
                        'required' => false,
                        'id' => 'select-product-name',
                        'default' => isset($product_id) && !empty($product_id) ? $product_id : '',
                        'empty' => ['' => __('TXT_SEARCH')]
                    ]);
                ?>
            </div>
        </div>
        <?= $this->Form->end() ?>
    </div>
</div>
<div class="row">
    <div class="col-sm-12 table-product-price">
        <table class="table table-condensed table-striped table-product-detail">
            <thead>
            <tr>
                <th>&nbsp;</th>
                <th>#</th>
                <th><?= __('TXT_PRODUCT_NAME') ?></th>
                <th><?= __('TXT_PACK') ?></th>
                <?php
                if ($pricing) {
                    echo '<th>&nbsp;</th>';
                }
                ?>
            </tr>
            </thead>
            <tbody></tbody>
        </table>
    </div>
</div>