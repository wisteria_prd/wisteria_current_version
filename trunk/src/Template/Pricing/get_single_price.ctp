<?php if ($data): ?>
    <tr data-id="<?= h($data->id); ?>">
        <th scope="row"></th>
        <td>
            <?= date('Y-m-d', strtotime($data->from_date)) ?>
        </td>
        <td>
            <?= date('Y-m-d', strtotime($data->to_date)) ?>
        </td>
        <td>
            <?= $data->from_qty ?>
        </td>
        <td>
            <?= $data->to_qty ?>
        </td>
        <td>
            <?php
            echo '<span class="unit-price">' . $this->Comment->isJK($data->currency->code, $data->unit_price) . '</span> ';
            echo '<span class="currency-id" data-id="'.$data->currency->id.'">' . $data->currency->code . '</span>';
            ?>
        </td>
        <td>
            <?php
                echo $this->Form->button(__('FOC'), [
                    'type' => 'button',
                    'class' => 'btn btn-info btn-sm select-foc',
                    'data-id' => $data->id,
                    'data-pricingtype' => $data->type,
                    'data-externalid' => $data->external_id
                ]);
            ?>
        </td>
        <td width="1%">
            <?php
            $customer_id = '';
            if (!empty($data->clinic_pricing)) {
                foreach ($data->clinic_pricing as $p) {
                    $customer_id .= $p->customer_id . ',';
                }
            }
            if ($customer_id != '') {
                $customer_id = rtrim($customer_id, ',');
            }
            echo $this->Form->button(BTN_ICON_EDIT, [
                'type' => 'button',
                'class' => 'btn btn-primary btn-sm btn-edit-discount',
                'escape' => false,
                'data-id' => $data->id,
                'data-pricingtype' => $data->type,
                'data-externalid' => $data->external_id,
                'data-customerid' => $customer_id
            ]);
            ?>
        </td>
        <td width="1%">
            <?php
                echo $this->Form->button(BTN_ICON_DELETE, [
                    'type' => 'button',
                    'class' => 'btn btn-delete btn-sm',
                    'id' => 'btn-delete',
                    'data-id' => $data->id,
                    'escape' => false
                ]);
            ?>
        </td>
    </tr>
<?php endif; ?>