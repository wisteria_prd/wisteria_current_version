<?php if ($data): ?>
<table class="table table-striped" id="discount-list">
    <thead>
        <tr>
            <th>#</th>
            <th class="white-space">
                <?php echo __('TXT_FROM_DATE'); ?>
            </th>
            <th class="white-space">
                <?php echo __('TXT_TO_DATE'); ?>
            </th>
            <th class="white-space">
                <?php echo __('TXT_FROM_QUANTITY'); ?>
            </th>
            <th class="white-space">
                <?php echo __('TXT_TO_QUANTITY'); ?>
            </th>
            <th class="white-space">
                <?php echo __('TXT_UNIT_PRICE'); ?>
            </th>
            <th colspan="3">&nbsp;</th>
        </tr>
    </thead>
    <tbody>
        <?php
        $num = 0;
        foreach ($data as $item) : $num++;
        ?>
        <tr data-id="<?= h($item->id); ?>">
            <th scope="row"><?= $num; ?></th>
            <td>
                <?= date('Y-m-d', strtotime($item->from_date)) ?>
            </td>
            <td>
                <?= date('Y-m-d', strtotime($item->to_date)) ?>
            </td>
            <td>
                <?= $item->from_qty ?>
            </td>
            <td>
                <?= $item->to_qty ?>
            </td>
            <td>
                <?php
                echo '<span class="unit-price">' . $this->Comment->isJK($item->currency->code, $item->unit_price) . '</span> ';
                echo '<span class="currency-id" data-id="'.$item->currency->id.'">' . $item->currency->code . '</span>';
                ?>
            </td>
            <td>
                <?php
                    echo $this->Form->button(__('FOC'), [
                        'type' => 'button',
                        'class' => 'btn btn-info btn-sm select-foc',
                        'data-id' => $item->id,
                        'data-pricingtype' => $item->type,
                        'data-externalid' => $item->external_id
                    ]);
                ?>
            </td>
            <td width="1%">
                <?php
                $customer_id = '';
                if (!empty($item->clinic_pricing)) {
                    foreach ($item->clinic_pricing as $p) {
                        $customer_id .= $p->customer_id . ',';
                    }
                }
                if ($customer_id != '') {
                    $customer_id = rtrim($customer_id, ',');
                }
                echo $this->Form->button(BTN_ICON_EDIT, [
                    'type' => 'button',
                    'class' => 'btn btn-primary btn-sm btn-edit-discount',
                    'escape' => false,
                    'data-id' => $item->id,
                    'data-pricingtype' => $item->type,
                    'data-externalid' => $item->external_id,
                    'data-customerid' => $customer_id
                ]);
                ?>
            </td>
            <td width="1%">
                <?php
                    echo $this->Form->button(BTN_ICON_DELETE, [
                        'type' => 'button',
                        'class' => 'btn btn-delete btn-delete-price btn-sm',
                        'id' => 'btn-delete',
                        'data-id' => $item->id,
                        'escape' => false
                    ]);
                ?>
            </td>
        </tr>
        <?php
        endforeach;
        ?>
    </tbody>
</table>
<?php endif; ?>