<?php if ($products) { ?>
<table class="table table-striped tb-prbr-view" id="tb-detail">
    <tbody>
        <tr>
            <td><?= __('TXT_PRODUCT_NAME') ?> :</td>
            <td><?php echo h($products->product->name); ?></td>
            <td><?php echo h($products->product->name_en); ?></td>
        </tr>
        <tr>
            <td><?= __('TXT_BRAND_NAME') ?> :</td>
            <td><?php echo h($products->product->product_brand->name); ?></td>
            <td><?php echo h($products->product->product_brand->name_en); ?></td>
        </tr>
        <tr>
            <td><?= __('TXT_MANUFACTURER_NAME') ?> :</td>
            <td><?php echo h($products->product->product_brand->manufacturer->name); ?></td>
            <td><?php echo h($products->product->product_brand->manufacturer->name_en); ?></td>
        </tr>
        <tr>
            <td><?= __('URL') ?> :</td>
            <td colspan="2"><?php echo $products->product->url; ?></td>
        </tr>
        <tr>
            <td><?= __('TXT_PACK_NAME') ?> :</td>
            <td><?php echo !empty($products->tb_pack_sizes) ? $products->tb_pack_sizes->name : ''; ?></td>
            <td><?php echo !empty($products->tb_pack_sizes) ? $products->tb_pack_sizes->name_en : ''; ?></td>
        </tr>
        <tr>
            <td><?= __('TXT_PACK_SIZE') ?> :</td>
            <td colspan="2"><?php echo $products->pack_size; ?></td>
        </tr>
        <tr>
            <td><?= __('TXT_SINGLE_UNIT_NAME') ?> :</td>
            <td><?php echo !empty($products->single_unit) ? $products->single_unit->name : ''; ?></td>
            <td><?php echo !empty($products->single_unit) ? $products->single_unit->name_en : ''; ?></td>
        </tr>
        <tr>
            <td><?= __('TXT_SINGLE_UNIT_SIZE') ?> :</td>
            <td colspan="2"><?php echo $products->single_unit_size; ?></td>
        </tr>
        <tr>
            <td><?= __('TXT_LOGISTIC_TEMPERATURE_NAME') ?> :</td>
            <td><?php echo $products->logistic_temperature->name; ?></td>
            <td><?php echo $products->logistic_temperature->name_en; ?></td>
        </tr>
        <tr>
            <td><?= __('TXT_TEMPERATURE_RANGE') ?> :</td>
            <td colspan="2">
                <?php echo $products->logistic_temperature->temperature_range; ?>
            </td>
        </tr>
        <tr>
            <td><?= __('TXT_TYPE') ?> :</td>
            <td colspan="2">
                <?php echo $products->type; ?>
            </td>
        </tr>
        <tr>
            <td><?= __('TXT_PATIENT_AGREEMENT') ?> :</td>
            <td colspan="2">
                <?php echo ($products->is_patient_agreement) ? __('TXT_YES') : __('TXT_NO'); ?>
            </td>
        </tr>
        <tr>
            <td><?= __('TXT_IMPORT_CERTIFICATE') ?> :</td>
            <td colspan="2">
                <?php echo ($products->import_certificate) ? __('TXT_YES') : __('TXT_NO'); ?>
            </td>
        </tr>
        <tr>
            <td><?= __('STR_AVAILABLE_SALE') ?> :</td>
            <td colspan="2">
                <?php echo ($products->sale_flag) ? __('TXT_YES') : __('TXT_NO'); ?>
            </td>
        </tr>
        <tr>
            <td><?= __('STR_AVAILABLE_PURCHASE') ?> :</td>
            <td colspan="2">
                <?php echo ($products->purchase_flag) ? __('TXT_YES') : __('TXT_NO'); ?>
            </td>
        </tr>
        <tr>
            <td><?= __('TXT_STANDARD_PRICE') ?> :</td>
            <td colspan="2">
                ???
            </td>
        </tr>
        <tr>
            <td><?= __('TXT_USER_REMARK') ?> :</td>
            <td colspan="2">
                <textarea readonly="readonly" style="background: none;"><?php echo h($products->product->remarks); ?></textarea>
            </td>
        </tr>
    </tbody>
</table>
<?php }