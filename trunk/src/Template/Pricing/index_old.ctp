
<div class="row row-top-space">
    <div class="col-sm-8">
        <?php
        echo $this->Form->create('Pricing', [
            'role' => 'form',
            'class' => 'form-horizontal form-search',
            'type' => 'get',
            'name' => 'common_form'
        ]); ?>
        <div class="form-group custom-select">
            <label class="col-sm-3 control-label" for="product_detail_id">
                <?php echo __('TXT_PRODUCT_OR_PACKAGE_NAME') ?>
            </label>
            <div class="col-sm-4 field-small-padding-right">
                <?php $name = 'name' . $locale; ?>
                <select class="form-control" name="external_id" id="select-product-detail-name">
                    <?php echo $this->Comment->allProductAndDetailsAsSelectOptionAndPackageDetails($products, $packages, $locale); ?>
                </select>
            </div>
            <div class="col-sm-4 field-small-padding-left">
                <?php
                echo $this->Form->button(__('TXT_ADVANCED_FIND_PRODUCTS'), [
                    'type' => 'button',
                    'class' => 'btn btn-primary',
                    'id' => 'advanced-search',
                ]); ?>
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-offset-3 col-sm-4">
                <?php
                echo $this->Form->button(__('TXT_GET_PRICE'), [
                    'type' => 'button',
                    'class' => 'btn btn-primary btn-get-price',
                    'id' => 'get-price',
                    'disabled' => true,
                ]); ?>
            </div>
        </div>
        <?php
        echo $this->Form->end(); ?>
    </div>
</div>
<section id="create_form">
    <div class="row">
        <div class="col-sm-12">
            <?php
            echo $this->Form->create('Pricing', [
                'role' => 'form',
                'class' => 'form-horizontal from-pricing',
                'name' => 'common_form'
            ]);
            $this->Form->templates([
                'inputContainer' => '{{content}}'
            ]); ?>
            <div class="form-group custom-select">
                <label class="col-sm-2 control-label">
                    <?php echo __('TXT_TYPE') . $this->Comment->formAsterisk() ?>
                </label>
                <div class="col-sm-4">
                    <?php
                    echo $this->Form->select('type', $prc, [
                        'class' => 'form-control',
                        'label' => false,
                        'required' => false,
                        'empty' => ['' => __('TXT_SELECT_TYPE')],
                        'id' => 'pricing-type'
                    ]); ?>
                    <span class="help-block help-tips" id="error-type"></span>
                </div>
                <label class="col-sm-2 control-label">
                    <?php echo __('TXT_PRODUCT_NAME') . $this->Comment->formAsterisk() ?>
                </label>
                <div class="col-sm-4">
                    <?php
                    echo $this->Form->select('external_id', [], [
                        'class' => 'form-control',
                        'label' => false,
                        'required' => false,
                        'id' => 'external-id',
                        'empty' => ['' => __('TXT_SELECT_PRODUCT_NAME')],
                        'readonly' => true
                    ]); ?>
                    <span class="help-block help-tips" id="error-external_id"></span>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">
                    <?php echo __('TXT_FROM_DATE') . $this->Comment->formAsterisk() ?>
                </label>
                <div class="col-sm-4">
                    <?php
                    echo $this->Form->input('from_date', [
                        'type' => 'text',
                        'class' => 'form-control',
                        'label' => false,
                        'required' => false,
                        'id' => 'from-date'
                    ]); ?>
                    <span class="help-block help-tips" id="error-from_date"></span>
                </div>
                <label class="col-sm-2 control-label"><?= __('TXT_TO_DATE') ?><?= $this->Comment->formAsterisk() ?></label>
                <div class="col-sm-4">
                    <?php echo $this->Form->input('to_date', [
                        'type' => 'text',
                        'class' => 'form-control',
                        'label' => false,
                        'required' => false,
                        'id' => 'to-date'
                    ]); ?>
                    <span class="help-block help-tips" id="error-to_date"></span>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label"><?= __('TXT_FROM_QUANTITY') ?><?= $this->Comment->formAsterisk() ?></label>
                <div class="col-sm-4">
                    <?php echo $this->Form->input('from_qty', [
                        'class' => 'form-control clear',
                        'label' => false,
                        'required' => false,
                        'id' => 'from-qty'
                    ]); ?>
                    <span class="help-block help-tips" id="error-from_qty"></span>
                </div>
                <label class="col-sm-2 control-label"><?= __('TXT_TO_QUANTITY') ?><?= $this->Comment->formAsterisk() ?></label>
                <div class="col-sm-4">
                    <?php echo $this->Form->input('to_qty', [
                        'class' => 'form-control clear',
                        'label' => false,
                        'required' => false,
                        'id' => 'to-qty'
                    ]); ?>
                    <span class="help-block help-tips" id="error-to_qty"></span>
                </div>
            </div>
            <div class="form-group custom-select">
                <label class="col-sm-2 control-label"><?= __('TXT_UNIT_PRICE') ?><?= $this->Comment->formAsterisk() ?></label>
                <div class="col-sm-4">
                    <?php echo $this->Form->input('unit_price', [
                        'class' => 'form-control clear',
                        'label' => false,
                        'required' => false
                    ]); ?>
                    <span class="help-block help-tips" id="error-unit_price"></span>
                </div>
                <label class="col-sm-2 control-label"><?= __('TXT_CLINIC') ?></label>
                <div class="col-sm-4 multi-clinic">
                    <div class="clinic-item row">
                        <div class="col-sm-11">
                            <?php echo $this->Form->select('clinic_pricing.0.customer_id', $customers, [
                                'class' => 'form-control customer-id customer-id-0',
                                'label' => false,
                                'required' => false,
                                'empty' => ['' => __('TXT_SELECT_FACILITY_NAME')],
                                'id' => false
                            ]); ?>
                            <span class="help-block help-tips error-customers" id="error-customer-0"></span>
                        </div>
                        <div class="col-sm-1" style="padding-left: 0">
                            <button type="button" class="btn btn-sm btn-primary btn-add-clinic form-btn-add">
                                <i class="fa fa-plus" aria-hidden="true"></i>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group custom-select">
                <label class="control-label col-md-2" for=""><?= __('TXT_CURRENCY') ?><?= $this->Comment->formAsterisk() ?></label>
                <div class="col-md-4">
                    <?php
                        echo $this->Form->select('currency_id', $currencies, [
                            'type' => 'text',
                            'label' => false,
                            'class' => 'form-control select-currency',
                            'empty' => ['' => __('TXT_SELECT_CURRENCY')]
                        ]);
                    ?>
                    <span class="help-block help-tips" id="error-currency_id"></span>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-12 text-center">
                    <?php echo $this->Form->button(__('TXT_CLEAR'), [
                        'type' => 'button',
                        'class' => 'btn btn-default form_btn btn-width btn-sm',
                        'id' => 'btn-clear',
                    ]); ?>
                    &nbsp; &nbsp;
                    <?php
                    echo $this->Form->button(__('TXT_SAVE'), [
                        'type' => 'button',
                        'class' => 'btn btn-primary form_btn btn-width btn-sm',
                        'id' => 'btn-register',
                    ]);
                    ?>
                </div>
            </div>
            <?= $this->Form->end(); ?>
        </div>
    </div>
</section>

<div class="data-tb-list">
    <table class="table table-striped" id="discount-list">
        <thead>
            <tr>
                <th>#</th>
                <th class="white-space">
                    <?php echo __('TXT_FROM_DATE'); ?>
                </th>
                <th class="white-space">
                    <?php echo __('TXT_TO_DATE'); ?>
                </th>
                <th class="white-space">
                    <?php echo __('TXT_FROM_QUANTITY'); ?>
                </th>
                <th class="white-space">
                    <?php echo __('TXT_TO_QUANTITY'); ?>
                </th>
                <th class="white-space">
                    <?php echo __('TXT_UNIT_PRICE'); ?>
                </th>
                <th colspan="3">&nbsp;</th>
            </tr>
        </thead>
        <tbody>

        </tbody>
    </table>
</div>

<!--Modal Detail-->
<?php echo $this->element('Modal/view_detail'); ?>

<!--Modal delete-->
<?php echo $this->element('Modal/delete'); ?>

<!--Modal Suspend-->
<?php //echo $this->element('Modal/suspend'); ?>

<?php
echo $this->Form->hidden('pricing_id', [
    'id' => 'pricing-id',
    'value' => ''
]);
echo $this->Form->hidden('seller_id', [
    'id' => 'seller-id',
    'value' => ''
]);
echo $this->Form->hidden('pricing_type', [
    'id' => 'pricing-type',
    'value' => ''
]);
echo $this->Form->hidden('external_id', [ //not the id of pricing for FOC
    'id' => 'external-id',
    'value' => ''
]);
?>
<?= $this->element('Modal/product_list') ?>

<?php
    echo $this->Html->css([
        'bootstrap-datetimepicker.min',
        'jquery-ui',
        'intlTelInput',
    ]);
?>

<script>
    (function() {
        $('.customer-id').select2();

        function updateIndex() {
            $('.customer-id').each(function(i, v) {
                $(this).attr('name', 'clinic_pricing[' + i + '][customer_id]');
            });

            $('.error-customers').each(function (i, v) {
                $(this).attr('id', 'error-customer-' + i);
            });
        }

        $('body').on('click', '.btn-add-clinic', function() {
            $('.customer-id').select2('destroy');
            var doctor = $('.multi-clinic .clinic-item').first().clone();
            doctor.addClass('row-clinic');
            doctor.find('button')
                .removeClass('btn-add-clinic btn-primary')
                .addClass('btn-remove-clinic btn-delete')
                .html('<i class="fa fa-trash" aria-hidden="true"></i>');
            doctor.find('.customer-id > option').removeAttr('selected', true);
            doctor.find('.control-label').text('');
            $('.multi-clinic').append(doctor);
            updateIndex();
            $('.customer-id').select2();
            $('.customer-id').change();
        });

        $('body').on('click', '.btn-remove-clinic', function() {
            $(this).closest('.clinic-item').remove();
        });

        $('body').on('change', '.customer-id', function() { //clone and disabled previous selection
            // enable all options
            $('option[disabled]').prop('disabled', false);

            $('.customer-id').each(function() {
                if (this.value !== '') {
                    $('.customer-id').not(this).find('option[value="' + this.value + '"]').prop('disabled', true);
                }
            });
        });

        $(document).on('show.bs.modal', '.modal', function (event) {
            var zIndex = 1040 + (10 * $('.modal:visible').length);
            $(this).css('z-index', zIndex);
            setTimeout(function() {
                $('.modal-backdrop').not('.modal-stack').css('z-index', zIndex - 1).addClass('modal-stack');
            }, 0);
        });

        $('#from-date').datetimepicker({
            format: 'YYYY-MM-DD',
            minDate: '2007-01-01'
        });

        $('#to-date').datetimepicker({
            format: 'YYYY-MM-DD',
            minDate: '2007-01-01'
        });

        function getDetailList(type, auto, id) {
            $.ajax({
                url: '<?= $this->Url->build(['controller' => 'Pricing', 'action' => 'priceNameList']) ?>',
                type: 'GET',
                data: {type: type},
                dataType: 'HTML',
                beforeSend: function() {
                    $.LoadingOverlay('show');
                },
                success: function(response) {
                    $.LoadingOverlay('hide');
                    if (response) {
                        $('#external-id').empty().html(response).select2();
                    } else {
                        $('#external-id')
                        .empty()
                        .html('<option value=""><?= __('TXT_SELECT_PRODUCT_NAME') ?></option>')
                        .attr('readonly', true);
                    }
                },
                complete: function () {
                    if (auto === true) {
                        $('#external-id').val(id).change();
                    }
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    $.LoadingOverlay('hide');
                    if (errorThrown === 'Forbidden') {
                        if (confirm('<?= __('TXT_SESSION_TIMEOUT') ?>')) {
                            location.reload();
                        }
                    }
                }
            });
        }

        $('body').on('change', '#pricing-type', function() {
            var type = $(this).val();
            if (type !== '') {
                getDetailList(type, false, false);
            } else {
                $('#external-id')
                .empty()
                .html('<option value=""><?= __('TXT_SELECT_PRODUCT_NAME') ?></option>')
                .attr('readonly', true);
            }
        });

        $('body').on('change', '#external-id', function () {
            var external_id = $(this).val();
            var pricing_type = $('#pricing-type').val();
            if (external_id !== '' && pricing_type !== '') {
                get_price(external_id, pricing_type);
            }
        });

        $('body').on('click', '#btn-clear', function () {
            clearForm('yes');
        });

        function clearForm(clearSelect) {
            $('.from-pricing').find('.clear').val('');
            $('#external-id').removeAttr('disabled', true);
            $('#pricing-type').removeAttr('disabled', true);
            if (clearSelect === 'yes') {
                $('.from-pricing').find('input[type="text"]').val('');
                $('#pricing-type').val('').change();
            }
        }

        function reIndexList() {
            $('#discount-list tbody tr').each(function (i, v) {
                $(this).find('th').text(i+1);
            });
        }

        $('body').on('click', '#btn-register', function() {
            $('.help-block').empty().hide();
            var data = $('.from-pricing').serialize();
            var url = '<?= $this->Url->build(['controller' => 'Pricing', 'action' => 'create']) ?>';
            var update = false;
            if ($('.discount-update-id').length) {
                url = '<?= $this->Url->build(['controller' => 'Pricing', 'action' => 'edit']) ?>';
                update = true;
            }
            $.ajax({
                url: url,
                type: 'POST',
                dataType: 'JSON',
                data: data,
                cache: false,
                beforeSend: function() {
                    $.LoadingOverlay('show');
                },
                success: function(response) {
                    $.LoadingOverlay('hide');
                    if (response.status === 1) {
                        $('#btn-register').text('<?= __('TXT_REGISTER_SAVE') ?>');
                        //clear form then load the new inserted data.
                        if (update === false) {
                            clearForm('no');
                            get_single_price(response.inserted_id);
                        } else {
                            clearForm('yes');
                            $('.discount-update-id').remove();
                            get_price(response.external_id, response.type);
                            $('.btn-remove-clinic').click();
                            $('.customer-id-0').val('').change();
                        }
                    } else if (response.status === 0) {
                        if (Object.keys(response.clinic).length > 0) {
                            $.each(response.clinic, function(key, elem) {
                                var validate = $('#error-customer-' + key);
                                validate.removeClass('help-tips')
                                    .addClass('error-tips')
                                    .text(elem)
                                    .show();
                            });
                        } else {
                            if (response.clinic.length > 0) {
                                $.each(response.clinic, function(key, elem) {
                                    var validate = $('#error-customer-' + key);
                                    validate.removeClass('help-tips')
                                        .addClass('error-tips')
                                        .text(elem)
                                        .show();
                                });
                            }
                        }

                        $.each(response.message, function(key, elem) {
                            var validate = $('#error-' + key);
                            $.each(elem, function(k, v) {
                                validate.removeClass('help-tips')
                                    .addClass('error-tips')
                                    .text(v)
                                    .show();
                            });
                        });
                    }
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    $.LoadingOverlay('hide');
                    if (errorThrown === 'Forbidden') {
                        if (confirm('<?= __('TXT_SESSION_TIMEOUT') ?>')) {
                            location.reload();
                        }
                    }
                }
            });
        });

        //get pricing edit on form.
        $('body').on('click', '.btn-edit-discount', function () {
            $('.btn-remove-clinic').click();
            var id = $(this).attr('data-id');
            $('#btn-register').text('<?= __('TXT_SAVE') ?>');
            $('#pricing-type').attr('disabled', true);
            $('#external-id').attr('disabled', true);
            if ($('.discount-update-id').length) {
                $('.discount-update-id').remove();
            }
            $('.from-pricing').append('<input type="hidden" name="id" value="'+id+'" class="discount-update-id">');
            var tr = $(this).closest('tr');
            $.each(tr, function(i, v) {
                $('#from-date').val($.trim($(this).find('td').eq(0).text()));
                $('#to-date').val($.trim($(this).find('td').eq(1).text()));
                $('#from-qty').val($.trim($(this).find('td').eq(2).text()));
                $('#to-qty').val($.trim($(this).find('td').eq(3).text()));
                $('#unit-price').val($.trim($(this).find('td').eq(4).find('.unit-price').text()));
                $('.select-currency').val($.trim($(this).find('td').eq(4).find('.currency-id').attr('data-id')));
            });

            var customer = $(this).attr('data-customerid');
            if (customer != '') {
                var id = customer.split(',');
                for (i = 0; i < id.length; i++) {
                    if (i === 0) {
                        $('.customer-id').val(id[i]).change();
                    } else if ($('.btn-add-clinic').click()) {
                        $('.multi-clinic').find('.customer-id').last().val(id[i]).change();
                    }
                }
            } else {
                $('.customer-id').val('').change();
            }
        });

        $('#select-product-detail-name').select2();

        $('#select-product-detail-name').on('change', function () {
            $('#get-price').attr('disabled', true);
            if ($(this).val() !== '') {
                $('#get-price').removeAttr('disabled', true);
            }
        });

        $('body').on('click', '#get-price', function() {
            $('#btn-clear').click(); //clear form first.
            var external_id = $('#select-product-detail-name').val();
            var type = $('#select-product-detail-name option:selected').attr('data-type');

            $('#pricing-type').val(type).change();
            getDetailList(type, true, external_id);
        });

        $('body').on('click', '.check-product', function () {
            $('.check-product').not(this).prop('checked', false);
        });

        $('body').on('click', '#select-product', function () {
            var detail_id = '';
            $('.check-product').each(function() {
                if ($(this).is(':checked')) {
                    detail_id = $(this).attr('data-id');
                   return false;
                }
            });
            if (detail_id !== '') {
                $('#btn-clear').click(); //clear form first.
                $('#pricing-type').val('<?= TYPE_PRODUCT_DETAILS ?>').change();
                getDetailList('<?= TYPE_PRODUCT_DETAILS ?>', true, detail_id);
            }
            $('.btn-close-modal').click();
        });

        $('body').on('click', '.btn-close-modal', function () {
            $('.btn-get-price').attr('id', 'get-price');
            $('#select-product-detail-name').val('').change();
        });

        function get_price (external_id, type) {
            $.ajax({
                url: '<?= $this->Url->build(['controller' => 'Pricing', 'action' => 'getPrice']) ?>',
                type: 'GET',
                data: {external_id: external_id, type: type},
                dataType: 'HTML',
                beforeSend: function() {
                    $.LoadingOverlay('show');
                },
                success: function(response) {
                    $.LoadingOverlay('hide');
                    $('.data-tb-list').empty().html(response);
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    $.LoadingOverlay('hide');
                    if (errorThrown === 'Forbidden') {
                        if (confirm('<?= __('TXT_SESSION_TIMEOUT') ?>')) {
                            location.reload();
                        }
                    }
                }
            });
        }

        function get_single_price (inserted_id) {
            $.ajax({
                url: '<?= $this->Url->build(['controller' => 'Pricing', 'action' => 'getSinglePrice']) ?>',
                type: 'GET',
                data: {inserted_id: inserted_id},
                dataType: 'HTML',
                beforeSend: function() {
                    $.LoadingOverlay('show');
                },
                success: function(response) {
                    $.LoadingOverlay('hide');
                    $('#discount-list tbody').prepend(response);
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    $.LoadingOverlay('hide');
                    if (errorThrown === 'Forbidden') {
                        if (confirm('<?= __('TXT_SESSION_TIMEOUT') ?>')) {
                            location.reload();
                        }
                    }
                },
                complete: function () {
                    reIndexList();
                }
            });
        }

        $('body').on('click', '.btn-delete-price', function() {
            var id = $(this).attr('data-id');
            $('.delete-item').val(id);
            $('#modal_delete').modal('show');
            $('.btn-delete-item').attr('id', 'btn-delete-pricing-yes');
        });

        $('body').on('click', '#btn-delete-pricing-yes', function() {
            var data = $('.form-delete').serialize();
            var id = $('.delete-item').val();
            $.ajax({
                url: '<?= $this->Url->build(['controller' => 'Pricing', 'action' => 'delete']) ?>',
                type: 'POST',
                data: data,
                dataType: 'JSON',
                cache: false,
                beforeSend: function() {
                    $.LoadingOverlay('show');
                },
                success: function(response) {
                    $.LoadingOverlay('hide');
                    if (response.status === 1) {
                        $('.close-modal').click();
                        $('#discount-list tbody tr[data-id="'+id+'"]').remove();
                        reIndexList();
                    } else {
                        alert('<?= __('TXT_DELETE_TROUBLE') ?>');
                    }
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    $.LoadingOverlay('hide');
                    if (errorThrown === 'Forbidden') {
                        if (confirm('<?= __('TXT_SESSION_TIMEOUT') ?>')) {
                            location.reload();
                        }
                    }
                }
            });
        });

        /**
        * FOC Section
         */
        $('body').on('click', '.btn-edit', function() {
            var tr = $(this).parents('tr');
            var id = $(this).attr('data-id');
            var detail_id = $(this).attr('data-detail-id');
            $('#value').val(tr.find('.discount-value').text());
            $('#product').val(detail_id).change();
            $('.foc-discount-id').val(id);
            $('.btn-foc-discount').text('<?= __('TXT_UPDATE') ?>');
        });

        $('body').on('click', '.select-foc', function() {
            var price_type = $(this).attr('data-pricingtype');
            var external_id = $(this).attr('data-externalid');
            var pricing_id = $(this).attr('data-id');
            var pid = $('#select-product-detail-name option:selected').attr('data-pid'); //product or package id
            $('#pricing-type').val(price_type);
            $('#external-id').val(external_id);
            $('#pricing-id').val(pricing_id);
            var data = {
                price_type: price_type,
                external_id: external_id,
                pricing_id: pricing_id,
                pid: pid
            };

            get_update_foc(data, false);
        });

        function get_update_foc(data, update) {
            $.ajax({
                url: '<?= $this->Url->build(['controller' => 'Pricing', 'action' => 'getFoc']) ?>',
                type: 'GET',
                data: data,
                dataType: 'HTML',
                beforeSend: function() {
                    $.LoadingOverlay('show');
                },
                success: function(response) {
                    $.LoadingOverlay('hide');
                    $('#modal_detail .modal-body').empty().html(response);
                    if (update === false) {
                        $('#modal_detail').modal('show');
                    }
                    $('#product').select2({
                        width: '100%'
                    });
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    $.LoadingOverlay('hide');
                    if (errorThrown === 'Forbidden') {
                        if (confirm('<?= __('TXT_SESSION_TIMEOUT') ?>')) {
                            location.reload();
                        }
                    }
                },
                complete: function () {
                    $('.foc-external-id').val($('#pricing-id').val()); //id of pricing for FOC external_id
                    $('.foc-type').val('<?= TYPE_PRICING ?>');
                }
            });
        }

        $('body').on('click', '.btn-foc-discount', function() {
            var data = $('.form-foc').serialize();
            var id = $('.foc-discount-id').val();
            var url = '<?= $this->Url->build(['controller' => 'FocDiscounts', 'action' => 'create']) ?>';
            if (id !== '') {
                url = '<?= $this->Url->build(['controller' => 'FocDiscounts', 'action' => 'edit']) ?>';
            }
            $.ajax({
                url: url,
                type: 'POST',
                dataType: 'JSON',
                data: data,
                cache: false,
                beforeSend: function() {
                    $.LoadingOverlay('show');
                },
                success: function(response) {
                    $.LoadingOverlay('hide');
                    if (response.status === 1) {
                        var price_type = $('#pricing-type').val();
                        var external_id = $('#external-id').val();
                        var pricing_id = $('#pricing-id').val();
                        var data = {
                            price_type: price_type,
                            external_id: external_id,
                            pricing_id: pricing_id
                        };

                        get_update_foc(data, true);
                    } else if (response.status === 0) {
                        $.each(response.message, function(key, elem) {
                            var validate = $('#error-' + key);
                            $.each(elem, function(k, v) {
                                validate.removeClass('help-tips')
                                    .addClass('error-tips')
                                    .text(v)
                                    .show();
                            });
                        });
                    }
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    $.LoadingOverlay('hide');
                    if (errorThrown === 'Forbidden') {
                        if (confirm('<?= __('TXT_SESSION_TIMEOUT') ?>')) {
                            location.reload();
                        }
                    }
                }
            });
        });

        $('body').on('click', '.btn-delete-discount', function() {
            var id = $(this).attr('data-id');
            $('.delete-item').val(id);
            $('.btn-delete-item').attr('id', 'btn-delete-foc-yes');
            $('#modal_delete').modal('show');
        });

        $('body').on('click', '#btn-delete-foc-yes', function() {
            var data = $('.form-delete').serialize();
            var id = $('.delete-item').val();
            $.ajax({
                url: '<?= $this->Url->build(['controller' => 'FocDiscounts', 'action' => 'delete']) ?>',
                type: 'POST',
                dataType: 'JSON',
                data: data,
                cache: false,
                beforeSend: function() {
                    $.LoadingOverlay('show');
                },
                success: function(response) {
                    $.LoadingOverlay('hide');
                    if (response.status === 1) {
                        $('.foc-' + id).remove();
                    } else if (response.status === 0) {
                        alert(response.message);
                    }
                    $('.close-modal').click();
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    $('.close-modal').click();
                    $.LoadingOverlay('hide');
                    if (errorThrown === 'Forbidden') {
                        if (confirm('<?= __('TXT_SESSION_TIMEOUT') ?>')) {
                            location.reload();
                        }
                    }
                }
            });
        });

        $('#advanced-search').on('click', function () {
            var data = {pricing: true};
            $.ajax({
                url: '<?= $this->Url->build(['controller' => 'Pricing', 'action' => 'productAdvancedSearch']) ?>',
                type: 'GET',
                dataType: 'HTML',
                data: data,
                beforeSend: function() {
                    $.LoadingOverlay('show');
                },
                success: function(response) {
                    $.LoadingOverlay('hide');
                    if (response) {
                        $('.product-list-content').empty().html(response);
                        $('#modal-product-list').modal('show');

                        $('.btn-get-price').attr('id', 'get-more-price');
                    }
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    $.LoadingOverlay('hide');
                    if (errorThrown === 'Forbidden') {
                        if (confirm('<?= __('TXT_SESSION_TIMEOUT') ?>')) {
                            location.reload();
                        }
                    }
                },
                complete: function(data) {
                    $('#select-product-name').select2({
                        width: '100%'
                    });
                }
            });
        });

        $('body').on('change', '#select-manufacturer', function() {
            var manufacturer = $(this).val();
            if (manufacturer !== '') {
                $.ajax({
                    url: '<?= $this->Url->build(['controller' => 'ProductPackageDetails', 'action' => 'productBrandDropdown']) ?>',
                    type: 'GET',
                    data: {man_id: manufacturer},
                    dataType: 'HTML',
                    beforeSend: function() {
                        $.LoadingOverlay('show');
                    },
                    success: function(response) {
                        $.LoadingOverlay('hide');
                        if (response) {
                            $('#select-product-brand').removeAttr('disabled').empty().html(response);
                        } else {
                            $('#select-product-brand').find('option')
                                .remove()
                                .end();
                            $('#select-product-brand').attr('disabled', true).append('<option value=""><?= __('TXT_SELECT_BRAND_NAME') ?></option>');
                        }
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        $.LoadingOverlay('hide');
                        if (errorThrown === 'Forbidden') {
                            if (confirm('<?= __('TXT_SESSION_TIMEOUT') ?>')) {
                                location.reload();
                            }
                        }
                    }
                });
            } else {
                $('#select-product-brand').find('option')
                    .remove()
                    .end();
                $('#select-product-brand').attr('disabled', true).append('<option value=""><?= __('TXT_SELECT_BRAND_NAME') ?></option>');
            }
        });

        $('body').on('change', '#select-product-brand', function() {
            var brand = $(this).val();
            if (brand !== '') {
                selectModalProductList(brand);
            } else {
                selectModalProductList('');
            }
        });

        $('body').on('change', '#select-product-name', function() {
            var product_id = $(this).val();
            var product_name = $(this).find('option:selected').text();

            if (product_id !== '') {
                productList(product_id, product_name);
            } else {
                $('.table-product-detail tbody').empty();
            }
        });

        function productList(product_id, product_name) {
            $.ajax({
                url: '<?= $this->Url->build(['controller' => 'Pricing', 'action' => 'productDetailList']) ?>',
                type: 'GET',
                data: {product_id: product_id, product_name: product_name, pricing: true},
                dataType: 'HTML',
                beforeSend: function() {
                    $.LoadingOverlay('show');
                },
                success: function(response) {
                    $.LoadingOverlay('hide');
                    $('.table-product-price tbody').empty().html(response);
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    $.LoadingOverlay('hide');
                    if (errorThrown === 'Forbidden') {
                        if (confirm('<?= __('TXT_SESSION_TIMEOUT') ?>')) {
                            location.reload();
                        }
                    }
                }
            });
        }

        function selectModalProductList(brand_id) {
            $.ajax({
                url: '<?= $this->Url->build(['controller' => 'ProductPackageDetails', 'action' => 'productNameList']) ?>',
                type: 'GET',
                data: {brand_id: brand_id},
                dataType: 'JSON',
                beforeSend: function() {
                    $.LoadingOverlay('show');
                },
                success: function(response) {
                    $.LoadingOverlay('hide');
                    var element = '<option value=""><?= __('TXT_SEARCH') ?></option>';
                    if (response != null && response != undefined) {
                        $.each(response.data, function (i, v) {
                            element += '<option value="'+ v.id +'">'+ nameEnOrJp(response.en, v) +'</option>';
                        });
                        $('#select-product-name').empty().append(element).select2();
                        countProductDetail(response.data.length);
                        $('.table-product-detail tbody').empty();
                    }
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    $.LoadingOverlay('hide');
                    if (errorThrown === 'Forbidden') {
                        if (confirm('<?= __('TXT_SESSION_TIMEOUT') ?>')) {
                            location.reload();
                        }
                    }
                }
            });
        }

        function countProductDetail(productFound) {
            $('.amount-found').empty().text(productFound);
        }

        $('body').on('click', '.btn-view-details', function () {
            var detail_id = $(this).attr('data-id');
            $.ajax({
                url: '<?= $this->Url->build(['controller' => 'Pricing', 'action' => 'view']) ?>',
                type: 'GET',
                data: {detail_id: detail_id},
                dataType: 'HTML',
                beforeSend: function() {
                    $.LoadingOverlay('show');
                },
                success: function(response) {
                    $.LoadingOverlay('hide');
                    $('#modal_detail .modal-body').empty().html(response);
                    $('#modal_detail').modal('show');
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    $.LoadingOverlay('hide');
                    if (errorThrown === 'Forbidden') {
                        if (confirm('<?= __('TXT_SESSION_TIMEOUT') ?>')) {
                            location.reload();
                        }
                    }
                }
            });
        });

    })();
</script>
