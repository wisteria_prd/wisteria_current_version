
<?php if($data) : foreach ($data as $key => $value) : ?>
<tr data-id="<?= $value->id ?>">
    <td><?= $key+1 ?></td>
    <td><?= $value->from_qty ?></td>
    <td><?= $value->to_qty ?></td>
    <td class="table-text-right">
        <?php
        $local = $this->request->session()->read('tb_field');
        $content = '';
        $data1 = [];

        if ($focDiscounts) {
            $data1 = $focDiscounts->filter(function($value1, $key1) use ($value) {
                return (($value1->type === TYPE_PRICING) && ($value1->external_id === $value->id));
            })->toArray();
        }

        if ($data1) {
            $content .= "<table class='table'><thead><tr>" .
                        "<th>" . __('TXT_PRODUCT_BRAND') . "</th>".
                        "<th>" . __('TXT_PRODUCT_NAME') . "</th>".
                        "<th>" . __('TXT_QUANTITY') . "</th></tr></thead><tbody>";

            foreach ($data1 as $key2 => $value2) {
                $content .= "<tr><td>" . $value2->product_detail->product->product_brand->name . $local . "</td>" .
                            "<td>" . $value2->product_detail->product->name . $local . ' ( ' . $this->Comment->productDetailName($value2->product_detail, $local) . ' ) ' . "</td>" .
                            "<td>" . $value2->foc_discount_type . ' ' . $value2->foc_discount_value . "</td>" .
                            "</tr>";
            }
            $content .= "</tbody></table>";
        }
        ?>
        <button type="button" class="btn btn-warning btn-sm foc-preview" data-html="true" data-placement="left" data-toggle="popover" title="<?= __('FOC Product List') ?>" data-content="<?= $content ?>">View</button>
    </td>
</tr>

<?php endforeach; endif; ?>
