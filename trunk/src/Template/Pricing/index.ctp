
<div class="search-section" style="margin-bottom: 20px;">
    <?php
        echo $this->Form->create('Pricings', [
            'class' => 'form-inline form-search',
            'type' => 'get',
            'name' => 'form_search',
        ]);
        echo $this->Form->input('keyword', [
            'name' => 'keyword',
            'placeholder' => __d('product', 'TXT_ENTER_KEYWORDS'),
            'class' => 'form-control',
            'label' => false,
            'value' => $this->request->query('keyword'),
            'autocomplete' => 'off',
            'templates' => [
                'inputContainer' => '<div class="form-group">{{content}}</div>',
            ]
        ]);
        echo $this->Form->input('filter_manufacturer', [
            'name' => 'filter_manufacturer',
            'type' => 'select',
            'class' => 'form-contro filter-manufacturer',
            'label' => false,
            'value' => $this->request->query('filter_manufacturer'),
            'options' => $manufacturers,
            'empty' => [0 => __('PRODUCT_TXT_SELECT_MANUFACTURER')],
            'templates' => [
                'inputContainer' => '&nbsp;&nbsp;<div class="form-group custom-select">{{content}}</div>',
            ]
        ]);
        echo $this->Form->input('filter_brand', [
            'name' => 'filter_brand',
            'type' => 'select',
            'class' => 'form-control filter-brand',
            'label' => false,
            'value' => $this->request->query('filter_brand'),
            'options' => $brands,
            'empty' => [0 => __('PRODUCT_TXT_SELECT_PRODUCT_BRAND')],
            'templates' => [
                'inputContainer' => '&nbsp;&nbsp;<div class="form-group custom-select">{{content}}</div>',
            ]
        ]);
        echo $this->Form->input('filter_product', [
            'name' => 'filter_product',
            'type' => 'select',
            'class' => 'form-control filter-product',
            'label' => false,
            'value' => $this->request->query('filter_product'),
            'options' => $products,
            // TODO: translate text in jp
            'empty' => [0 => __d('product', 'TXT_SELECT_PRODUCT')],
            'templates' => [
                'inputContainer' => '&nbsp;&nbsp;<div class="form-group custom-select">{{content}}</div>',
            ]
        ]);
        $this->SwitchStatus->render();
        echo $this->Form->submit('submit', [
            'style' => 'display: none',
        ]);
        echo $this->Form->end();
    ?>
</div>
<div class="total-pagination" style="padding-bottom: 20px;">
    <?php echo $this->element('display_info'); ?>
</div>
<div class="row search-section" style="margin-bottom: 20px;">
    <?php
    echo $this->element('display_number');
    echo $this->ActionButtons->btnRegisterNew('Pricing', []);
    ?>
    <div class="clearfix"></div>
</div>
<?php // if ?>
<div class="data-tb-list">
    <?php if ($data): ?>
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>#</th>
                    <th style="white-space: nowrap;">&nbsp;</th>
                    <th><?php echo __d('pricing', 'TXT_SUPPLIER'); ?></th>
                    <th><?php echo __d('pricing', 'TXT_MANUFACTURER'); ?></th>
                    <th><?php echo __d('pricing', 'TXT_PRODUCT'); ?></th>
                    <th><?php echo __d('pricing', 'TXT_TYPE'); ?></th>
                    <th><?php echo __d('pricing', 'TXT_VALUE'); ?></th>
                    <th><?php echo __d('pricing', 'TXT_RANGE'); ?></th>
                    <th width="1%">&nbsp;</th>
                    <th width="1%">&nbsp;</th>
                    <th width="1%">&nbsp;</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $numbering = $this->Paginator->counter('{{start}}');
                foreach ($data as $item):
                    ?>
                    <tr data-id="<?php echo h($item->id); ?>">
                        <th scope="row" class="td_evt">
                            <?php
                            echo $numbering;
                            $numbering++; ?>
                        </th>
                        <td style="white-space: nowrap;">
                            <?php
                            echo $this->ActionButtons->disabledText($item->is_suspend); ?>
                        </td>
                        <td>
                            sup
                        </td>
                        <td>
                            manu
                        </td>
                        <td>
                            product
                        </td>
                        <td><?php echo $item->type; ?></td>
                        <td><?php echo $item->type; ?></td>
                        <td>
                            Range
                        </td>
                        <td>
                            <?php
                            echo $this->Form->button('<i class="fa fa-comments-o" aria-hidden="true"></i>', [
                                'class' => 'btn btn-primary btn-sm btn_comment',
                                'data-id' => $item->id,
                                'escape' => false,
                            ]);
                            ?>
                        </td>
                        <td data-target="<?php echo ($item->is_suspend) ? 0 : 1; ?>">
                            <?php
                            if ($item->is_suspend == 1) {
                                echo $this->Form->button(BTN_ICON_UNDO, [
                                    'class' => 'btn btn-recover btn-sm btn-restore',
                                    'escape' => false
                                ]);
                            } else {
                                echo $this->Html->link(BTN_ICON_EDIT, [
                                    'controller' => 'pricing',
                                    'action' => 'edit', $item->id
                                    ], [
                                    'class' => 'btn btn-primary btn-sm',
                                    'escape' => false
                                ]);
                            }
                            ?>
                        </td>
                        <td data-target="<?php echo ($item->is_suspend) ? 0 : 1; ?>">
                            <?php
                            if ($item->is_suspend == 1) {
                                if (!empty($item->product_brands) || !empty($item->count_person)) {
                                    echo $this->Form->button(BTN_ICON_DELETE, [
                                        'class' => 'btn btn-delete btn-sm',
                                        'disabled' => 'disabled',
                                        'escape' => false
                                    ]);
                                } else {
                                    echo $this->Form->button(BTN_ICON_DELETE, [
                                        'class' => 'btn btn-delete btn-sm',
                                        'escape' => false
                                    ]);
                                }
                            } else {
                                echo $this->Form->button(BTN_ICON_STOP, [
                                    'class' => 'btn btn-suspend btn-sm btn-restore',
                                    'escape' => false
                                ]);
                            }
                            ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    <?php endif; ?>
</div>
<div class="row row-top-space" style="padding-bottom: 20px;">
    <?php
    echo $this->element('display_number');
    echo $this->element('next_prev');
    ?>
    <div class="clearfix"></div>
</div>
<?php // endif; ?>
<script>
    var MESSAGE_TYPE_PRICING = '<?php echo MESSAGE_TYPE_PRICING; ?>';
</script>
<?php
echo $this->Html->script([
    'pricing/index.js',
], ['blog' => false]);
