
<div class="search-section" style="margin-bottom: 20px;">
    <div class="row">
        <?php
        echo $this->Form->create('GreetingCards', [
            'name' => 'form_search',
            'onsubmit' => 'return false;'
        ]); ?>
        <div class="col-sm-2 text-right">
            <label style="margin-top: 7px;">
                <?php echo __('GREETING_SEND_TO_TARGET') ?> :
            </label>
        </div>
        <div class="col-sm-3 custom-select">
        <?php
        echo $this->Form->select('send-to', $entities, [
            'class' => 'form-control',
            'label' => false,
            'id' => 'send-to',
            'empty' => ['' => __('GREETING_SELECT_SEND_TARGET_TYPE')],
        ]); ?>
        </div>
        <div class="col-sm-1 text-right">
            <label style="margin-top: 7px;">
                <?php echo __('GREETING_SEND_TO') ?> :
            </label>
        </div>
        <div class="col-sm-3 custom-select">
        <?php
        echo $this->Form->select('select-name', [], [
            'class' => 'form-control',
            'label' => false,
            'id' => 'select-name',
            'empty' => ['' => __('GREETING_SELECT_SEND_TO')],
        ]); ?>
        </div>
        <?php
        echo $this->Form->end(); ?>
    </div>
</div>
<div class="data-list">
    <table class="table table-striped">
        <thead>
            <tr>
                <th>#</th>
                <th><?php echo __('GREETING_ORGANIZATION_NAME'); ?></th>
                <th><?php echo __('GREETING_SEND_TO_JP'); ?></th>
                <th><?php echo __('GREETING_SEND_TO_EN'); ?></th>
                <th><?php echo __('GREETING_DELIVERY_METHOD'); ?></th>
                <th><?php echo __('GREETING_TYPE'); ?></th>
                <th>&nbsp;</th>
            </tr>
        <tbody></tbody>
        </thead>
    </table>
    <div class="text-center btn-wrap"></div>
</div>

<!--Communication Modal-->
<div class="modal modal-communicate fade" data-backdrop="static" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title text-center"></h4>
            </div>
            <div class="modal-body fixed-modal-height"></div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default btn-sm btn-width" data-dismiss="modal"><?= __('TXT_CANCEL') ?></button>
                <button type="button" class="btn btn-primary btn-sm btn-width btn-yes" disabled="disabled"><?= __('TXT_YES') ?></button>
                <?php
                echo $this->Form->hidden('name_en');
                echo $this->Form->hidden('name');
                ?>
            </div>
        </div>
    </div>
</div>
<!--Message Modal-->
<div class="modal modal-message fade" data-backdrop="static" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <?php echo $this->Form->create(null, [
                'rule' => 'form',
                'name' => 'form_message',
            ]); ?>
            <div class="modal-header"><h4 class="modal-title text-center"></h4></div>
            <div class="modal-body fixed-modal-height">
                <div class="form-group">
                    <?php echo $this->Form->textarea('message', [
                        'class' => 'form-control',
                        'label' => false,
                        'required' => false,
                        'rows' => 10,
                        'placeholder' => __('TXT_MESSAGE'),
                        'templates' => [
                            'inputContainer' => '{{content}}',
                        ],
                    ]); ?>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default btn-sm btn-width" data-dismiss="modal"><?php echo __('BTN_CANCEL'); ?></button>
                <button type="submit" class="btn btn-primary btn-sm btn-width btn-send"><?php echo __('TXT_SEND'); ?></button>
            </div>
            <?php echo $this->form->end(); ?>
        </div>
    </div>
</div>

<?php
echo $this->Html->script([
    'jquery.validate.min',
], ['blog' => 'script']);
?>

<script>
    (function(e) {
        var data = [];
        $('body').find('#send-to, #select-name').select2();

        // list name by select sent to dropdown
        $('body').on('change', '#send-to', function(e) {
            $.LoadingOverlay('show');
            var type = $(this).val();
            $.post('<?php echo $this->Url->build('/greeting-cards/filter-name-by-type/'); ?>', {type: type}, function(data) {
                var element = '<option value=""><?= __('GREETING_SELECT_SEND_TO') ?></option>';
                var content = $('body').find('#select-name');
                $(content).find('option').remove();
                var response = data.data;
                if (response.length > 0) {
                    $.each (response, function(i, v) {
                        var name = (data.field === '_en') ? v.name_en : v.name;
                        if (name != ' ') {
                            element += '<option value="' + name + '" data-type="' + type + '" data-id="' + v.id + '" data-name="' + v.name + '" data-name-en="' + v.name_en + '">' + name + '</option>';
                        }
                    });
                } else {
                    element += '<option value=""><?= __('TXT_DEFAULT_SELECT') ?></option>';
                }
                $(content).append(element);
                $(content).select2({
                    placeholder: '<?php echo __('TXT_DEFAULT_SELECT'); ?>'
                });
            }, 'json').done(function(data) {
                $.LoadingOverlay('hide');
            });
        });

        // list email by type and name
        $('body').on('change', '#select-name', function(e) {
            $.LoadingOverlay('show');
            var params = {
                id: $(this).find('option:selected').attr('data-id'),
                type: $(this).find('option:selected').attr('data-type'),
                value: $(this).val(),
                name: $(this).find('option:selected').attr('data-name'),
                name_en: $(this).find('option:selected').attr('data-name-en')
            };
            $.get('<?php echo $this->Url->build('/greeting-cards/get-info-list/'); ?>', params, function(data) {
                var modal = $('body').find('.modal-communicate');
                $(modal).find('.modal-title').html(params.value);
                $(modal).find('input[name="name"]').val(params.name);
                $(modal).find('input[name="name_en"]').val(params.name_en);
                $(modal).find('.modal-body').html(data);
                $(modal).modal('show');
            }, 'html').done(function(data) {
                $.LoadingOverlay('hide');
            });
        });

        // select email by check
        $('body').on('click', '.btn-yes', function(e) {
            var checkbox = $('.modal').find('.table tr input:checkbox:checked');
            $.each(checkbox, function(i, v) {
                var arr = {
                    type: $(this).closest('tr').attr('data-type'),
                    value: $(this).closest('tr').attr('data-value'),
                    name: $('.modal').find('input[name="name"]').val(),
                    name_en: $('.modal').find('input[name="name_en"]').val(),
                    entity_name: $('body').find('#send-to').val()
                };
                data.push(arr);
            });
            display_email_list(data);
            $('.modal').modal('hide');
        });

        // click on checkbox
        $('body').on('change', 'input[name="check-box"]', function(e) {
            var checkbox = $('.modal').find('.table tr input:checkbox:checked');
            if (checkbox.length > 0) {
                $('body').find('.btn-yes').removeAttr('disabled');
            } else {
                $('body').find('.btn-yes').attr('disabled', 'disabled');
            }
        });

        // click on email button
        $('body').on('click', '.btn-email', function(e) {
            var target = $(this).attr('data-target');
            var title = (target === 'email') ? 'Write your message to send Email' : 'Write your message to send Fax';
            $('.modal-message').find('.modal-title').html(title);
            $('.modal-message').find('button[type="submit"]').attr('data-target', target);
            $('.modal-message').modal('show');
            load_form();
        });

        // remove email from list
        $('body').on('click', '.btn-remove', function(e) {
            var index = $(this).closest('tr').index();
            $(this).closest('tr').remove();
            data.splice(index, 1);
            $('body').find('.data-list .btn-wrap').empty();
            display_email_list(data);
        });

        // add email to data list
        function display_email_list(data)
        {
            if (data.length > 0) {
                $('body').find('.data-list .table>tbody tr').remove();
                var element = '';
                var element1 = '';
                var index = $('body').find('.data-list .table>tbody tr').length;
                $.each(data, function(i, v) {
                    index += 1;
                    element += '<tr data-email="' + v.value + '" data-type="' + v.type + '">'
                            + '<td>' + (index) + '</td>'
                            + '<td>' + v.entity_name + '</td>'
                            + '<td>' + v.name + '</td>'
                            + '<td>' + v.name_en + '</td>'
                            + '<td>' + v.value + '</td>'
                            + '<td>' + v.type + '</td>'
                            + '<td class="text-right"><button type="button" class="btn btn-remove btn-sm btn-primary"><?= __('TXT_DELETE') ?></button></td>'
                            + '</tr>';
                });
                element1 = '<button type="button" class="btn btn-primary btn-sm btn-width btn-email" data-target="email"><?= __('TXT_EMAIL') ?></button>&nbsp;&nbsp;&nbsp;'
                         + '<button type="button" class="btn btn-primary btn-sm btn-width btn-email" data-target="fax">Fax</button>&nbsp;&nbsp;&nbsp;'
                         + '<button type="button" class="btn btn-primary btn-sm">Postal Service</button>';
            }
            $('body').find('.data-list .table>tbody').append(element);
            $('body').find('.btn-wrap').html(element1);
            disable_by_type();
        }

        // disable button if communicate type = 0
        function disable_by_type()
        {
            var type_email = $('body').find('.data-list>table>tbody tr[data-type="email"]');
            var type_fax = $('body').find('.data-list>table>tbody tr[data-type="fax"]');
            // count type email in data list
            if (type_email.length == 0) {
                $('body').find('.btn-wrap button[data-target="email"]').attr('disabled', 'disabled');
            } else {
                $('body').find('.btn-wrap button[data-target="email"]').removeAttr('disabled');
            }
            // count type fax in data list
            if (type_fax.length == 0) {
                $('body').find('.btn-wrap button[data-target="fax"]').attr('disabled', 'disabled');
            } else {
                $('body').find('.btn-wrap button[data-target="fax"]').removeAttr('disabled');
            }
        }

        // load jquery validate form
        function load_form()
        {
            $('body').find('form[name="form_message"]').validate({
                rules: {
                    message: 'required'
                },
                messages: {
                    message: '<?php echo __('TXT_MESSAGE_REQUIRED'); ?>'
                },
                errorClass: 'error-message',
                submitHandler: function(form) {
                    $.LoadingOverlay('show');
                    var message = $('body').find('textarea[name="message"]').val();
                    var target = $(form).find('button[type="submit"]').attr('data-target');
                    var emails = [];
                    var tr = $('body').find('.data-list .table>tbody tr[data-type="'+ target +'"]');
                    if (tr.length > 0) {
                        $.each(tr, function(i, v) {
                            var arr = {
                                type: $(this).attr('data-type'),
                                communicate : (target === 'email') ? $(this).attr('data-email') : $(this).attr('data-email') + '@ml.faximo.jp'
                            };
                            emails.push(arr);
                        });
                    }
                    $.post('<?php echo $this->Url->build('/greeting-cards/send-email/'); ?>', {message: message, emails: emails}, function(data) {
                        if (data.status == 1) {
                            $('.modal-message').modal('hide');
                        }
                    }, 'json').done(function(data) {
                        $.LoadingOverlay('hide');
                    });
                }
            });
        }

        // clear value when modal is hide
        $('.modal-message').on('hidden.bs.modal', function (e) {
            $('.modal-message').find('textarea').val('');
            $('.modal-message').find('button[type="submit"]').removeAttr('data-target');
        });
    })();
</script>
