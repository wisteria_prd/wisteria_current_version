
<?php if (count($data->info_details) > 0) : $num = 0; ?>
<table class="table table-striped">
    <thead>
        <tr>
            <th style="width: 1%;">&nbsp;</th>
            <th style="width: 10px;">#</th>
            <th style="width: 40px;"><?php echo __('TXT_COMMUNICATION'); ?></th>
            <th style="width: 1%;"><?php echo __('STR_TYPE'); ?></th>
        </tr>
    </thead>
    <tbody>
        <?php if (!empty($data->fax)) : ?>
        <tr data-type="fax" data-value="<?php echo $data->fax; ?>">
            <td><input type="checkbox" name="check-box"/></td>
            <td><?php echo $num; ?></td>
            <td><?php echo h($data->fax); ?></td>
            <td>Fax</td>
        </tr>
        <?php endif; ?>
        <?php foreach ($data->info_details as $key => $item) : $num++;?>
        <tr data-type="email" data-value="<?php echo $item->email; ?>">
            <td><input type="checkbox" name="check-box"/></td>
            <td><?php echo $num; ?></td>
            <td><?php echo h($item->email); ?></td>
            <td>Email</td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>
<?php else : ?>
<p><?php echo __('TXT_NOT_FOUND'); ?></p>
<?php endif; ?>
