
<?php
$name = 'name' . $en;
echo $this->Form->create('PaymentManagements', [
    'role' => 'form',
    'class' => 'form-inline form-search',
    'type' => 'get',
    'name' => 'form_search'
]);
$this->Form->templates([
    'inputContainer' => '{{content}}'
]);
echo $this->Form->hidden('type', [
    'class' => 'supplier-types',
    'value' => $this->request->query('type')
]);
echo $this->Form->hidden('displays', [
    'value' => empty($this->request->query('displays')) ? 10 : $this->request->query('displays')
]);
?>
<div class="form-group" style="margin-right: 10px;">
    <?php
    echo $this->Form->input('keyword', [
        'name' => 'keyword',
        'placeholder' => __('USER_SEARCH_KEYWORD'),
        'class' => 'form-control',
        'label' => false,
        'value' => $this->request->query('keyword'),
        'autocomplete' => 'off',
    ]);
    ?>
</div>
<div class="form-group" style="margin-right: 10px;">
    <button type="button" class="btn btn-primary"><?= __('絞り込み条件') ?></button>
</div>
<div class="form-group">
    <div class="uncompleted-payment"><?= __('付け合せ未完了') ?>	<span>10</span><?= __('件') ?></div>
</div>
<?php echo $this->Form->end(); ?>
<div class="total-pagination" style="padding-top: 20px; padding-bottom: 20px;">
    <?php //echo $this->element('display_info'); ?>
</div>
<div class="row" style="margin-bottom: 10px;">
    <div class="col-sm-12 text-right">
        <button type="button" class="btn btn-primary btn-sm btn-width">CSV<br /><?= __('アップロード') ?></button>
    </div>
</div>
<div class="row search-section" style="margin-bottom: 20px;">
    <?php echo $this->element('display_number'); ?>
    <?= $this->ActionButtons->btnRegisterNew('PaymentManagements', []) ?>
    <div class="clearfix"></div>
</div>
<div class="data-tb-list">
    <?php //if ($payments) : ?>
    <table class="table table-striped">
        <thead>
            <tr>
                <th>#</th>
                <th class="white-space">
                    <?php echo $this->Paginator->sort('', __('入金日')); ?>
                    <span><i class="fa fa-sort-desc" aria-hidden="true"></i></span>
                </th>
                <th class="white-space"><?php echo $this->Paginator->sort('', __('受注日')) ?></th>
                <th class="white-space"><?php echo $this->Paginator->sort('', __('クリニック名')); ?></th>
                <th class="white-space"><?php echo $this->Paginator->sort('', __('振込名義人')); ?></th>
                <th class="white-space"><?php echo $this->Paginator->sort('', __('請求書番号')); ?></th>
                <th class="white-space"><?php echo $this->Paginator->sort('', __('請求額')); ?></th>
                <th class="white-space">&nbsp;</th>
                <th class="white-space"><?php echo $this->Paginator->sort('', __('入金額')); ?></th>
                <th class="white-space"><?php echo $this->Paginator->sort('', __('最終処理')); ?></th>
                <th colspan="2">&nbsp;</th>
            </tr>
        </thead>
        <?php
        //$numbering = $this->Paginator->counter('{{start}}');
        ?>
        <tbody>
            <tr data-payid="">
                <th scope="row">1<?php //echo $numbering; $numbering++; ?></th>
                <td>02/03/2018</td>
                <td>02/03/2018</td>
                <td>クリニックA</td>
                <td>イ）アイウエオカ</td>
                <td><a href="#">WIM0001</a><br /><a href="#">WIM0002</a></td>
                <td>¥500,000<br />¥500,000</td>
                <td>
                    <div class="pay-status">
                        &lt;
                    </div>
                </td>
                <td>¥800,000</td>
                <td>入月</td>
                <td>
                    <?php echo $this->Form->button('<i class="fa fa-comments-o" aria-hidden="true"></i>', [
                        'class' => 'btn btn-primary btn-sm btn_comment',
                        'data-id' => 1, //$item->id,
                        'escape' => false,
                    ]); ?>
                </td>
                <td width="1%">
                    <?php
                    echo $this->Form->button(BTN_ICON_DELETE, [
                        'type' => 'button',
                        'class' => 'btn btn-delete btn-sm btn-delete-payment',
                        'data-id' => '', //$payment->id,
                        'escape' => false
                    ]);
                    ?>
                </td>
                <?php
                //echo $this->ActionButtons->btnAction($payment->payment->id, $payment->payment->is_suspend, 'Payments');
                ?>
            </tr>
            <?php //endforeach; ?>
        </tbody>
    </table>
    <?php //endif; ?>
</div>
<div class="row row-top-space" style="padding-bottom: 20px;">
    <?php echo $this->element('display_number'); ?>
    <?php //echo $this->element('next_prev') ?>
    <div class="clearfix"></div>
</div>

<script>
    (function(e) {

    })();
</script>
