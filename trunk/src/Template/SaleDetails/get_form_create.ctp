<section class="modal-wrapper">
    <div class="modal fade" id="sale-detail" role="dialog" data-backdrop="static">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <?php
                    echo $this->Form->button('<span aria-hidden="true">&times;</span>', [
                        'type' => 'button',
                        'class' => 'close modal-close',
                        'data-dismiss' => 'modal',
                        'aria-label' => 'Close',
                        'escape' => false,
                    ]);
                    ?>
                    <h4 class="modal-title"><?= __('TXT_PO_ADD_ITEM') ?></h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <?php
                            echo $this->Form->create($saleDetail ? $saleDetail : null, [
                                    'class' => 'form-po-settings form-horizontal',
                                    'autocomplete' => 'off',
                                    'onsubmit' => 'return false;',
                                ]
                            );
                            echo $this->Form->hidden('id', [
                                'value' => $saleDetail ? $saleDetail->id : null,
                                'name' => 'id',
                            ]);
                            $this->Form->templates([
                                'inputContainer' => '{{content}}'
                            ]);
                            ?>
                            <div class="form-group custom-select">
                                <label for="" class="col-md-3 col-md-offset-1">
                                    <?= __('TXT_PRODUCT_NAME_PO') ?>
                                </label>
                                <div class="col-md-5 custom-select">
                                    <?= $this->Form->select('product_detail_id',
                                        $this->Comment->productDetailsListByProductId($local, $products), [
                                        'class' => 'form-control',
                                        'id' => 'product-detail-id',
                                        'value' => $saleDetail ? $saleDetail->product_detail_id : null,
                                    ]); ?>
                                </div>
                                <div class="col-md-2">
                                    <?= $this->Form->button(__('TXT_ADVANCED_FIND_PRODUCTS'), [
                                        'type' => 'button',
                                        'class' => 'btn btn-primary btn-av-search',
                                    ]); ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="" class="col-md-3 col-md-offset-1">
                                    <?= __('TXT_QUANTITY') ?>
                                </label>
                                <div class="col-md-5">
                                    <?= $this->Form->input('quantity', [
                                        'type' => 'number',
                                        'label' => false,
                                        'class' => 'form-control quantity',
                                        'placeholder' => __('TXT_ENTER_QUANTITY'),
                                        'value' => $saleDetail ? $saleDetail->quantity : null,
                                    ]); ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="" class="col-md-3 col-md-offset-1">
                                    <?= __('TXT_UNIT_PRICE') ?>
                                </label>
                                <div class="col-md-5">
                                    <?= $this->Form->input('unit_price', [
                                        'type' => 'number',
                                        'label' => false,
                                        'readonly' => true,
                                        'class' => 'form-control unit-price',
                                        'placeholder' => __('TXT_ENTER_UNIT_PRICE'),
                                        'value' => $saleDetail ? $saleDetail->unit_price : null,
                                    ]); ?>
                                </div>
                                <div class="col-md-2">
                                    <?= $this->Form->button(__('TXT_VIEW_DISCOUNT'), [
                                        'type' => 'button',
                                        'class' => 'btn btn-primary popup-discount',
                                    ]); ?>
                                </div>
                            </div>
                            <div class="form-group custom-select">
                                <label for="" class="col-md-4"><?= __('TXT_LOT_EXPIRED') ?></label>
                                <div class="col-md-5 custom-select no-padding-r">
                                    <?php
                                    $options = [];
                                    if ($stocks) {
                                        foreach ($stocks as $key => $value) {
                                            $options[] = [
                                                'value' => $value->lot_number,
                                                'text' => $value->lot_number . ', Exp: ' .
                                                date('M-Y', strtotime($value->import_detail->expiry_date)),
                                            ];
                                        }
                                    }
                                    echo $this->Form->select('lot', $options, [
                                        'class' => 'form-control',
                                        'id' => 'lot',
                                        'value' => $saleDetail ? $saleDetail->lot : null,
                                    ]);
                                    ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-5 col-md-offset-4">
                                    <div class="checkbox">
                                        <label>
                                            <?= $this->Form->input('manual', [
                                                'type' => 'checkbox',
                                                'class' => 'chk-register-foc',
                                                'checked' => false,
                                                'data-value' => $saleDetail ? $saleDetail->unit_price : 0,
                                                'hiddenField' => false,
                                                'label' => false]) .__('TXT_MANUAL_REGISTER_UNIT_PRICE')  ?>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <?= $this->Form->end() ?>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <?php
                    echo $this->Form->button(__('BTN_CANCEL'), [
                        'type' => 'button',
                        'class' => 'btn btn-default btn-sm btn-width modal-close',
                        'data-dismiss' => 'modal',
                        'aria-label' => 'Close',
                    ]);
                    echo $this->Form->button(__('TXT_REGISTER'), [
                        'type' => 'button',
                        'class' => 'btn btn-primary btn-sm btn-width btn-save',
                    ]);
                    ?>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(function(e) {
            var content = $('#sale-detail');
            $(content).find('#product-detail-id').select2({ width: '100%' });

            $(content).find('#product-detail-id').change(function(e) {
                var quantity = $(content).find('#quantity').val();
                if (quantity === null || quantity === '') {
                    return false;
                }
                var params = {
                    'id': $('option:selected', this).attr('value'),
                    'quantity': quantity,
                    'customer_id': '<?= $sale->customer_id ?>'
                };
                var options = {
                    type: 'POST',
                    url: '<?= $this->Url->build(['action' => 'getPricing']) ?>',
                    dataType: 'json',
                    data: params,
                    beforeSend: function() {
                        $.LoadingOverlay('show');
                    }
                };
                ajaxRequest(options, function(data) {
                    $(content).find('#unit-price').val(data.data.price);
                    $(content).find('#manual').attr('data-value', data.data.price);
                });
            });

            $(content).find('#quantity').change(function(e) {
                var params = {
                    'id': $('#product-detail-id option:selected').attr('value'),
                    'quantity': $(this).val(),
                    'customer_id': '<?= $sale->customer_id ?>'
                };
                var options = {
                    type: 'POST',
                    url: '<?= $this->Url->build(['action' => 'getPricing']) ?>',
                    dataType: 'json',
                    data: params,
                    beforeSend: function() {
                        $.LoadingOverlay('show');
                    }
                };
                ajaxRequest(options, function(data) {
                    $(content).find('#unit-price').val(data.data.price);
                    $(content).find('#manual').attr('data-value', data.data.price);
                });
            });

            $(content).find('#manual').change(function(e) {
                if (this.checked) {
                    $(content).find('#unit-price').removeAttr('readonly', true);
                } else {
                    $(content).find('#unit-price')
                            .val($(this).attr('data-value'))
                            .attr('readonly',true);
                }
            });

            $(content).find('.btn-save').click(function(e) {
                e.stopPropagation();
                var selected = $('#product-detail-id option:selected');
                var form = $(content).find('form');
                var params = {
                    'id': $(form).find('input[name="id"]').val(),
                    'sale_id': '<?= $sale->id ?>',
                    'product_detail_id': $(form).find('#product-detail-id').val(),
                    'quantity': $(form).find('#quantity').val(),
                    'unit_price': $(form).find('#unit-price').val(),
                    'manual': $(form).find('#manual').prop('checked'),
                    'product_id': $(selected).attr('data-pid'),
                    'product_name': $(selected).attr('data-pn'),
                    'pack_size_value': $(selected).attr('data-psv'),
                    'pack_size_name': $(selected).attr('data-psn'),
                    'single_unit_value': $(selected).attr('data-psuv'),
                    'single_unit_name': $(selected).attr('data-psun'),
                    'packing_id': $(selected).attr('data-pk'),
                    'description': $(selected).attr('data-dsc'),
                    'brand_name': $(selected).attr('data-bn'),
                    'lot': $(form).find('#lot').val()
                };
                var options = {
                    type: 'POST',
                    url: '<?= $this->Url->build(['action' => 'create']) ?>',
                    dataType: 'json',
                    data: params,
                    beforeSend: function() {
                        $.LoadingOverlay('show');
                        $(form).find('.error-message').remove();
                        $(form).find('.form-group').removeClass('has-error');
                    }
                };
                ajaxRequest(options, function(data) {
                    console.log(data);
                    if (data.message === '<?= MSG_ERROR ?>') {
                        $.each(data.data, function (i, v) {
                            var label = '<label class="error-message">' + v + '</label>';
                            var txt = $(form).find('[name="' + i + '"]');
                            $(txt).closest('div').append(label);
                            $(txt).closest('.form-group').addClass('has-error');
                        });
                    } else {
                        location.reload();
                    }
                });
            });

            function ajaxRequest(params, callback)
            {
                $.ajax(params).done(function(data) {
                    if (data === null && data === 'undefined') {
                        return false;
                    }
                    if (typeof callback === 'function') {
                        callback(data);
                    }
                }).fail(function(jqXHR, textStatus, errorThrown) {
                    if (errorThrown === 'Forbidden') {
                        if (confirm('<?= __('TXT_SESSION_TIMEOUT') ?>')) {
                            location.reload();
                        }
                    }
                }).always(function(data) {
                    $.LoadingOverlay('hide');
                });
            }
        });
    </script>
</section>