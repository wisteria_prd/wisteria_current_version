
<section class="modal-wrapper">
    <div class="modal fade modal-temperature" tabindex="-1" role="dialog" data-backdrop="static">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <?php
                    echo $this->Form->button('<span aria-hidden="true">&times;</span>', [
                        'type' => 'button',
                        'class' => 'close modal-close',
                        'data-dismiss' => 'modal',
                        'aria-label' => 'Close',
                        'escape' => false,
                    ]); ?>
                    <h4 class="modal-title text-center">
                        <?php
                        echo __d('logistic_temperature', 'TXT_MODAL_TITLE'); ?>
                    </h4>
                </div>
                <div class="modal-body fixed-modal-height">
                    <div class="row">
                        <div class="col-md-12">
                            <?php
                            echo $this->Form->create('LogisticTemperatures', [
                            'url' => [
                                'controller' => 'logistic-temperatures',
                                'action' => 'create',
                            ],
                            'class' => 'form-horizontal form-temperature',
                            'name' => 'temperaturer',
                            'role' => 'form',
                            ]);
                            echo $this->Form->hidden('id');
                            ?>
                            <label>
                                <?php
                                echo __d('logistic_temperature', 'TXT_LOGISTIC_TEMPERATURE'); ?>
                            </label>
                            <div class="form-group">
                                <div class="col-sm-6 field-small-padding-right">
                                    <?php
                                    echo $this->Form->text('name_en', [
                                        'label' => false,
                                        'class' => 'form-control',
                                        'required' => false,
                                        'placeholder' => __d('logistic_temperature', 'TXT_ENTER_LT_EN'),
                                    ]); ?>
                                </div>
                                <div class="col-sm-6 field-small-padding-left">
                                    <?php
                                    echo $this->Form->text('name', [
                                        'label' => false,
                                        'class' => 'form-control',
                                        'required' => false,
                                        'placeholder' => __d('logistic_temperature', 'TXT_ENTER_LT_JP'),
                                    ]); ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <?php
                                    echo $this->Form->text('temperature_range', [
                                        'label' => false,
                                        'class' => 'form-control',
                                        'required' => false,
                                        'placeholder' => __d('logistic_temperature', 'TXT_ENTER_LT_RNAGE'),
                                    ]); ?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12 text-center">
                                    <?php
                                    echo $this->Form->button(__d('logistic_temperature', 'TXT_REGISTER'), [
                                        'type' => 'button',
                                        'class' => 'btn btn-double-width btn-primary btn-sm btn_tmp_save',
                                        'data-target' => 'new',
                                    ]); ?>
                                </div>
                            </div>
                            <?php echo $this->Form->end(); ?>
                        </div>
                    </div>
                    <table class="table table-striped custom-table-space">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>
                                    <?php
                                    echo __d('logistic_temperature', 'TXT_LOGISTIC_TEMPERATURE'); ?>
                                </th>
                                <th>
                                    <?php
                                    echo __d('logistic_temperature', 'TXT_LT_RANGE'); ?>
                                </th>
                                <th>&nbsp;</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if ($data): ?>
                                <?php foreach ($data as $key => $value): ?>
                                    <tr data-id="<?php echo $value->id; ?>" data-name="<?php echo $value->name; ?>" data-name-en="<?php echo $value->name_en; ?>" data-range="<?php echo $value->temperature_range; ?>">
                                        <td><?php echo (int)($key + 1); ?></td>
                                        <td><?php echo ($locale === '_en') ? $value->name_en : $value->name; ?></td>
                                        <td><?php echo $value->temperature_range; ?></td>
                                        <td>
                                            <?php
                                            echo $this->Form->button(BTN_ICON_EDIT, [
                                                'type' => 'button',
                                                'class' => 'btn btn-primary btn-sm btn-tmp-edit',
                                                'escape' => false,
                                            ]); ?>&nbsp;&nbsp;
                                            <?php
                                            echo $this->Form->button(BTN_ICON_DELETE, [
                                                'type' => 'button',
                                                'class' => 'btn btn-danger btn-sm btn-tmp-delete',
                                                'escape' => false,
                                                'data-toggle' => 'popover',
                                            ]); ?>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </tbody>
                    </table>
                </div>
                <div class="modal-footer">
                    <div class="text-center">
                        <?php
                        echo $this->Form->button(__('TXT_CLOSE'), [
                            'type' => 'button',
                            'class' => 'btn btn-default btn-width btn-sm btn-close-modal modal-close',
                            'data-dismiss' => 'modal',
                        ]); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(function(e) {
            var form = $('form[name="temperaturer"]');

            $('body').on('click', '.btn-tmp-edit', function(e) {
                var tr = $(this).closest('tr');
                var param = {
                    id: $(tr).attr('data-id'),
                    name: $(tr).attr('data-name'),
                    name_en: $(tr).attr('data-name-en'),
                    range: $(tr).attr('data-range')
                };
                $(form).find('input[name="name"]').val(param.name);
                $(form).find('input[name="name_en"]').val(param.name_en);
                $(form).find('input[name="temperature_range"]').val(param.range);
                $(form).find('input[name="id"]').val(param.id);
                $('.modal').find('.btn_tmp_save').text('<?= __('TXT_UPDATE') ?>');
            });

            $('body').on('click', '.btn-tmp-delete', function(e) {
                confirm_delete('btn-tmp-delete');
            });

            $('.btn_tmp_save').click(function(e) {
                var params = {
                    type: 'POST',
                    url: BASE + 'LogisticTemperatures/saveOrUpdate',
                    data: $(form).serialize(),
                    beforeSend: function() {
                        $.LoadingOverlay('show');
                    }
                };
                ajaxRequest(params, SESSION_TIMEOUT, function(data) {
                    getList(data.temperatures);
                });
                // $.get('<?php echo $this->Url->build('/logistic-temperatures/get-list'); ?>', function(data) {
                //     if (data != null && data != undefined) {
                //         var element = '';
                //         var response = data.data;
                //         $.each(response, function(i, v) {
                //             element += '<option value="' + v.id + '">' + v.name_en + '</option>';
                //         });
                //         $('body').find('#logistic-tempearaturer-id option').remove();
                //         $('body').find('#logistic-tempearaturer-id').append(element);
                //     }
                // }).done(function(data) {
                //     $.LoadingOverlay('hide');
                //     $('.modal').modal('hide');
                // });
            });

            $('body').on('click', '.modal-temperature .confirm-yes', function(e) {
                var id = $(this).closest('tr').attr('data-id');
                var params = {
                    type: 'GET',
                    url: BASE + 'LogisticTemperatures/delete',
                    data: { id: id },
                    beforeSend: function() {
                        $.LoadingOverlay('show');
                    }
                };
                ajaxRequest(params, SESSION_TIMEOUT, function(data) {
                    if (data.status == '0') {
                        alert('Warning ! This record use another place!');
                    } else {
                        var tr = $('.modal-temperature').find('.table > tbody tr');
                        $(tr).find('[data-id="' + id + '"]').remove();
                    }
                });
            });

            function confirm_delete(class_name)
            {
                var template = '<p><?php echo __('MSG_CONFIRM_DELETE'); ?></p><div class="text-center">'
                            + '<button class="btn btn-default confirm-no" type="button"><?php echo __('BTN_CANCEL'); ?></button>'
                            + '<button class="btn btn-default confirm-yes" type="button"><?php echo __('BTN_DELETE'); ?></button></div>';
                $('body').find('.' + class_name).popover({
                    placement: 'left',
                    html: 'true',
                    content: template,
                    toggle: 'popover',
                    trigger: 'focus'
                });
            }

            function getList(data)
            {
                if (data === null && data === 'undefined') {
                    return false;
                }
                var element = '';
                $.each (data, function (index, value) {
                    var name = (LOCALE === '_en') ? value.name_en : value.name;
                    element += '<tr data-id="' + value.id + '" data-name="' + value.name + '" data-name-en="' + value.name_en + '" data-range="' + value.temperature_range + '">' +
                        '<td>' + (index + 1) + '</td>' +
                        '<td>' + name + '</td>' +
                        '<td>' + value.temperature_range + '</td>' +
                        '<td>' +
                            '<button type="button" class="btn btn-primary btn-sm btn-tmp-edit"><?= BTN_ICON_EDIT ?></button>&nbsp;&nbsp;' +
                            '<button type="button" data-toggle="popover" class="btn btn-danger btn-sm btn-tmp-delete><?= BTN_ICON_DELETE ?></button>' +
                        '</td>' +
                    '</tr>';
                });
                $('.table > tbody').html(element);
            }
        });
    </script>
</section>
