<?php
$this->assign('title', 'ホーム');

if (isset($data)) : ?>
<div class="row">
    <div class="col-xs-12 form-group">
        <div class="table-responsive">
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th width="18%"></th>
                        <th class="text-center">クリニック名</th><!-- clinic name -->
                        <th width="9%" class="text-center">最終購入日</th><!-- last date purchase -->
                        <th width="11%" class="text-center">売上合計</th>
                    </tr>
                </thead>
                <tbody>
                <?php 
                //$total = 0;
                //$last = count($data->toArray()) -1;
                foreach ($data as $key => $value) :
                    //$total = $total + $value['total_amount'];
                ?>
                <tr>
                    <td>
                        <?php
                        echo $this->Html->link('クリニック別<br />売上レポート', [
                                'controller' => 'reports',
                                'action' => 'clinic',
                                '?' => ['name' => h($value['clinic_name_jp'])]

                            ], ['class' => 'btn btn-primary btn-report-link', 'role' => 'button', 'escape' => false]
                        );
                        echo '&nbsp;';
                        echo $this->Html->link('詳細', [
                                'controller' => 'home',
                                'action' => 'visitTargetDetails',
                                $value['clinic_name_jp']

                            ], ['class' => 'btn btn-lg btn-primary', 'role' => 'button']
                        );
                        ?>
                    </td>
                    <td><?= h($value->clinic_name_jp) ?></td>
                    <td><?= date('Y-m-d', strtotime($value['order_date'])); ?></td>
                    <td>
                        <?= number_format($value['total_amount']) . __(' 円') ?>
                    </td>
                </tr>
                <?php
//                    if ($key === $last) {
//                        echo '<tr>';
//                        echo '<td colspan="5" class="text-right">' . __('合計') . '</td>';
//                        echo '<td>' . number_format($total) . __(' 円') . '</td>';
//                        echo '</tr>';
//                    }
                ?>
                <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
<?php endif;
