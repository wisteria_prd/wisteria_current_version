<?php $this->assign('title', 'ホーム'); ?>
<?php if (isset($data)) : ?>
<div class="row">
    <div class="col-xs-12 form-group">
        <div class="table-responsive">
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th width="7%">&nbsp;</th>
                        <th class="text-center">クリニック名</th>
                        <th width="9%" class="text-center">最終購入日</th>
                        <th class="text-center">購入製品名</th>
                        <th width="7%" class="text-center">個数</th>
                        <th width="12%" class="text-center">単価</th>
                        <th width="12%" class="text-center">合計</th>
                    </tr>
                </thead>
                <tbody>
                <?php
                $all_products = count($data);
                $total = 0;
                $i = 0;
                foreach ($data as $key => $value) :
                    $total = $total + ($value['quantity'] * $value['unit_price']);
                ?>
                    <?php if ($all_products == 1) { ?>
                        <tr>
                            <td>
                                <?php
                                echo $this->Html->link('クリニック別<br />売上レポート', [
                                    'controller' => 'reports',
                                    'action' => 'clinic',
                                    '?' => ['name' => h($value['clinic_name_jp'])]

                                    ], ['class' => 'btn btn-primary btn-report-link', 'role' => 'button', 'escape' => false]
                                );
                                ?>
                            </td>
                            <td><?php echo h($value['clinic_name_jp']); ?></td>
                            <td><?= date('Y-m-d', strtotime($value['order_date'])); ?></td>
                            <td><?= h($value['product_name']); ?></td>
                            <td class="text-right"><?= number_format($value['quantity']); ?></td>
                            <td class="text-right"><?= number_format($value['unit_price']); ?> 円</td>
                            <td class="text-right"><?php echo number_format($value['quantity'] * $value['unit_price']); ?> 円</td>
                        </tr>
                    <?php } else { ?>
                        <?php if ($i == 0) { ?>
                            <tr>
                                <td rowspan="<?= $all_products + 1 ?>">
                                    <?php
                                    echo $this->Html->link('クリニック別<br />売上レポート', [
                                        'controller' => 'reports',
                                        'action' => 'clinic',
                                        '?' => ['name' => h($value['clinic_name_jp'])]

                                        ], ['class' => 'btn btn-primary btn-report-link', 'role' => 'button', 'escape' => false]
                                    );
                                    ?>
                                </td>
                                <td rowspan="<?= $all_products + 1 ?>"><?php echo h($value['clinic_name_jp']); ?></td>
                            </tr>
                            <?php } ?>
                            <tr>
                                <td><?= date('Y-m-d', strtotime($value['order_date'])); ?></td>
                                <td><?= h($value['product_name']); ?></td>
                                <td class="text-right"><?= number_format($value['quantity']); ?></td>
                                <td class="text-right"><?= number_format($value['unit_price']); ?> 円</td>
                                <td class="text-right"><?= number_format($value['quantity'] * $value['unit_price']); ?> 円</td>
                            </tr>
                    <?php } ?>
                    <?php
                        if ($key === $all_products - 1) {
                            echo '<tr>';
                            echo '<td colspan="6" class="text-right">' . __('合計') . '</td>';
                            echo '<td class="text-right">' . number_format($total) . __(' 円') . '</td>';
                            echo '</tr>';
                        }
                    ?>
                <?php $i++; endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
<?php endif;
