<?php
$this->assign('title', 'ホーム');

if (isset($data)) : ?>
<div class="row">
    <div class="col-xs-12 form-group">
        <div class="table-responsive">
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th width="18%"></th>
                        <th width="11%" class="text-center">営業担当者</th>
                        <th width="9%" class="text-center">
                            <?php echo $this->Paginator->sort('Sale1s.order_date', __('購入日')); ?>
                            <i class="fa fa-sort-desc" aria-hidden="true"></i>
                        </th><!-- last date purchase -->
                        <!-- <th width="11%" class="text-center">営業代理店</th> -->
                        <th class="text-center">
                            <?php echo $this->Paginator->sort('Sale1s.clinic_name_jp', __('クリニック名')); ?>
                            <i class="fa fa-sort-desc" aria-hidden="true"></i>
                        </th><!-- clinic name -->
                        <th class="text-center">県</th><!-- prefecture -->
                        <th class="text-center">
                            <?php echo $this->Paginator->sort('Sale1s.product_brand', __('ブランド名')); ?>
                            <i class="fa fa-sort-desc" aria-hidden="true"></i>
                        </th>
                        <th width="11%" class="text-center">売上合計</th>
                    </tr>
                </thead>
                <tbody>
                <?php 
                //$total = 0;
                //$last = count($data->toArray()) -1;
                foreach ($data as $key => $value) :
                    //$total = $total + $value['total_amount'];
                ?>
                <tr>
                    <td>
                        <?php
                        echo $this->Html->link('クリニック別<br />売上レポート', [
                                'controller' => 'reports',
                                'action' => 'clinic',
                                '?' => ['name' => $value['clinic_name_jp']]

                            ], ['class' => 'btn btn-primary btn-report-link', 'role' => 'button', 'escape' => false]
                        );
                        echo '&nbsp;';
                        echo $this->Html->link('詳細', [
                                'controller' => 'home',
                                'action' => 'visitTargetDetails',
                                $value['clinic_name_jp']

                            ], ['class' => 'btn btn-lg btn-primary', 'role' => 'button']
                        );
                        ?>
                    </td>
                    <td>
                        <?php
                            if (!empty($value['user']['firstname'])) {
                                echo h($value['user']['lastname']) . ' ' . h($value['user']['firstname']);
                            } else {
                                echo '&nbsp;';
                            }
                        ?>
                    </td>
                    <td><?= date('Y-m-d', strtotime($value['order_date'])); ?></td>
                    <!--
                    <td>
                        <?php
                            if (!empty($value['agent_name'])) {
                                echo h($value['agent_name']);
                            } else {
                                echo '&nbsp;';
                            }
                        ?>
                    </td>
                    -->
                    <td><?= h($value->clinic_name_jp) ?></td>
                    <td><?= h($value->prefecture) ?></td>
                    <td>
                        <?= h($value->product_brand) ?>
                    </td>
                    <td>
                        <?= number_format($value['total_amount']) . __(' 円') ?>
                    </td>
                </tr>
                <?php
//                    if ($key === $last) {
//                        echo '<tr>';
//                        echo '<td colspan="5" class="text-right">' . __('合計') . '</td>';
//                        echo '<td>' . number_format($total) . __(' 円') . '</td>';
//                        echo '</tr>';
//                    }
                ?>
                <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
<?php endif; ?>

<div class="row row-top-space">
    <?= $this->element('next_prev', ['column' => 12]) ?>
    <div class="clearfix"></div>
</div>
<script>
    $(function() {
        var maxPage = parseInt($('#maxPage').val());
        updatePagination(maxPage);
    });
</script>