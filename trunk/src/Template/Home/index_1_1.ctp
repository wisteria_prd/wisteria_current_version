
<?php
$this->assign('title', 'ホーム');

if (isset($data)) : ?>
<div class="row">
    <div class="col-xs-12 form-group">
        <div class="table-responsive">
            <table class="table table-bordered">
                <tr>
                    <th></th>
                    <th width="250" class="text-center">クリニック名</th>
                    <th width="150" class="text-center">最終購入日</th>
                    <th width="400" class="text-center">購入製品名</th>
                    <th class="text-center">個数</th>
                    <th class="text-center">単価</th>
                    <th class="text-center">合計</th>
                </tr>
                <?php foreach ($data as $key => $value) : ?>
                    <tr>
                        <td rowspan="<?php echo count($value)+1; ?>">
                            <?php echo $this->Html->link('詳細', [
                                    'controller' => 'reports',
                                    'action' => 'clinic',
                                    '?' => ['name' => h($value[0]['clinic_name_jp'])]
                                    
                                ], ['class' => 'btn btn-primary']
                            ); ?>
                        </td>
                        <td rowspan="<?php echo count($value)+1; ?>"><?php echo h($value[0]['clinic_name_jp']); ?></td>
                    </tr>
                    <?php foreach ($value as $key1 => $value1) : ?>
                        <tr>
                            <td><?php echo date('Y-m-d',strtotime($value[0]['order_date'])); ?></td>
                            <td><?php echo h($value1['product_name']); ?></td>
                            <td class="text-right"><?php echo number_format($value1['total_quantity']); ?></td>
                            <td class="text-right"><?php echo number_format($value1['unit_price']); ?> 円</td>
                            <td class="text-right"><?php echo number_format($value1['total_amount']); ?> 円</td>
                        </tr>
                    <?php endforeach; ?>
                <?php endforeach; ?>
            </table>
        </div>
    </div>
</div>
<?php endif; ?>
