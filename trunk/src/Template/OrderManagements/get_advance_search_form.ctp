
<section class="modal-wrapper">
    <style>
        form .select {
            float: left;
            width: 23%;
            padding-right: 15px;
        }
        form .radio .lb {
            position: relative;
        }
        form .radio input[name="setting_date"] {
            transform: translateY(50%);
        }
        .tr-type label {
            padding: 0 20px 10px;
        }
        .border-bt {
            border-bottom: 1px solid #e5e5e5;
            padding-bottom: 10px;
            margin-bottom: 10px;
        }
        .modal-body {
            padding: 15px 0px;
        }
    </style>

    <div class="modal fade advance-search" data-backdrop="static" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <?php
                    echo $this->Form->button('<span aria-hidden="true">&times;</span>', [
                        'type' => 'button',
                        'class' => 'close modal-close',
                        'data-dismiss' => 'modal',
                        'aria-label' => 'Close',
                        'escape' => false,
                    ]);
                    ?>
                    <h4 class="modal-title text-center"><?= __('TXT_CREATE') ?></h4>
                </div>
                <div class="modal-body">
                    <?php
                    echo $this->Form->create(null, [
                        'class' => 'form-horizontal',
                        'autocomplete' => 'off',
                        'name' => 'advance_search',
                        'onsubmit' => 'return false;',
                    ]);
                    ?>

                    <!--Order Type-->
                    <section style="padding: 0px 15px;">
                        <label>取引タイプ</label>
                        <div class="radio tr-type">
                            <label>
                                <input type="radio" name="type" value="<?= TRADE_TYPE_SALE_W_STOCK ?>">国内 - 在庫出荷
                            </label>
                            <label>
                                <input type="radio" name="type" value="<?= TRADE_TYPE_SALE_BEHALF_S_IN_JP ?>">国内 - 直送発注（OF）
                            </label>
                            <label>
                                <input type="radio" name="type" value="<?= TRADE_TYPE_PURCHASE_W_STOCK ?>">国内 - 仕入（OF）
                            </label>
                            <label>
                                <input type="radio" name="type" value="<?= TRADE_TYPE_SALE_BEHALF_MP ?>">海外 - MP発注（PO）
                            </label>
                            <label>
                                <input type="radio" name="type" value="<?= TRADE_TYPE_SALE_BEHALF_S_OUTSIDE_JP ?>">海外 - その他発注（PO）
                            </label>
                            <label>
                                <input type="radio" name="type" value="<?= TRADE_TYPE_PURCHASE_MP_STOCK ?>">海外 - 	仕入（OF）
                            </label>
                            <label>
                                <input type="radio" name="type" value="7">MPによる仕入発注
                            </label>
                        </div>
                    </section>
                    <div class="border-bt"></div>

                    <!--Period-->
                    <section style="padding: 0px 15px;">
                        <label><?= __('TXT_SPECIFIED_DURING') ?></label>
                        <div class="radio">
                            <label id="lb" style="display: flex;">
                                <input type="radio" name="period" value="1">
                                <?php
                                echo $this->Form->input('year', [
                                    'type' => 'select',
                                    'class' => 'form-control',
                                    'options' => $years,
                                    'label' => false,
                                ]);
                                echo $this->Form->input('month', [
                                    'type' => 'select',
                                    'class' => 'form-control',
                                    'options' => $months,
                                    'label' => false,
                                ]);
                                echo $this->Form->input('day', [
                                    'type' => 'select',
                                    'class' => 'form-control',
                                    'options' => $days,
                                    'label' => false,
                                ]);
                                ?>
                            </label>
                        </div>
                        <div class="radio">
                            <label id="lb">
                                <input type="radio" name="period" value="2">
                                <?php
                                echo $this->Form->input('before_month', [
                                    'class' => 'form-control',
                                    'id' => 'before-month',
                                    'type' => 'text',
                                    'label' => false,
                                    'value' => date('Y-m-d', strtotime('-1 month')),
                                    'templates' => [
                                        'inputContainer' => '<div class="col-sm-4 col-md-4" style="padding-left: 0px; display: flex;">{{content}}<label class="control-label" style="display: table;">から</label></div>',
                                    ]
                                ]);
                                echo $this->Form->input('after_month', [
                                    'class' => 'form-control',
                                    'id' => 'after-month',
                                    'type' => 'text',
                                    'value' => date('Y-m-d'),
                                    'label' => false,
                                    'templates' => [
                                        'inputContainer' => '<div class="col-sm-4 col-md-4" style="display: flex; padding-right: 0px;">{{content}}<label class="control-label" style="display: table;">まで</label></div>',
                                    ]
                                ]);
                                ?>
                            </label>
                        </div>
                    </section>
                    <div class="border-bt"></div>

                    <!--Status-->
                    <section style="padding: 0px 15px;">
                        <label>進捗ステータス</label>
                        <div class="radio tr-type">
                            <label>
                                <input type="radio" name="status" value="<?= PO_STATUS_DRAFF ?>">データ入力
                            </label>
                            <label>
                                <input type="radio" name="status" value="<?= STATUS_FAXED_ROP ?>">支払依頼書FAX済
                            </label>
                            <label>
                                <input type="radio" name="status" value="<?= STATUS_PAID ?>">入金確認済
                            </label>
                            <label>
                                <input type="radio" name="status" value="<?= PO_STATUS_REQUESTED_SHIPPING ?>">出荷指示済
                            </label>
                            <label>
                                <input type="radio" name="status" value="<?= STATUS_APPLIED_CC ?>">薬監申請済
                            </label>
                            <label>
                                <input type="radio" name="status" value="<?= STATUS_REQUESTED_CC ?>">通関指示済
                            </label>
                            <label>
                                <input type="radio" name="status" value="<?= PO_STATUS_DELIVERED ?>">納品済
                            </label>
                            <label>
                                <input type="radio" name="status" value="<?= PO_STATUS_COMPLETED ?>">完了
                            </label>
                            <label>
                                <input type="radio" name="status" value="9">キャンセル
                            </label>
                            <label>
                                <input type="radio" name="status" value="10">削除
                            </label>
                        </div>
                    </section>
                    <section style="padding: 0px 15px;">
                        <label>サブステータス</label>
                        <div class="radio tr-type">
                            <label>
                                <input type="radio" name="status" value="<?= STATUS_PACKED ?>">梱包済
                            </label>
                            <label>
                                <input type="radio" name="status" value="<?= STATUS_SHIPPED ?>">出荷済
                            </label>
                        </div>
                    </section>
                    <div class="border-bt"></div>

                    <!--Payment Situation-->
                    <section style="padding: 0px 15px;">
                        <label>支払状況</label>
                        <div class="radio tr-type">
                            <label>
                                <input type="radio" name="payment" value="<?= TYPE_UNPAID ?>">未入金
                            </label>
                            <label>
                                <input type="radio" name="payment" value="<?= STATUS_PAID ?>">入金済
                            </label>
                        </div>
                    </section>
                    <?php echo $this->Form->end(); ?>
                </div>
                <div class="modal-footer">
                    <?php
                    echo $this->Form->button(__('TXT_CANCEL'), [
                        'type' => 'button',
                        'class' => 'btn btn-default btn-sm btn-width modal-close',
                        'data-dismiss' => 'modal',
                        'aria-label' => 'Close',
                    ]);
                    echo $this->Form->button(__('TXT_REGISTER'), [
                        'type' => 'button',
                        'class' => 'btn btn-primary btn-sm btn-width',
                        'v-on:click' => 'advanceSearch',
                    ]);
                    ?>
                </div>
            </div>
        </div>
    </div>

    <script>
        new Vue({
            el: '.advance-search',
            data: {
                url: '<?= $this->Url->build(['action' => 'advanceSearch']) ?>'
            },
            methods: {
                advanceSearch: function() {
                    var form = $('body').find('form[name="advance_search"]')
                    this.$http.post(this.url, { data: form.serialize() }, { emulateJSON: true, emulateHTTP: true })
                        .then(function(response) {
                            console.log(response)
                        })
                }
            },
            mounted: function() {
                $('body').find('#before-month, #after-month').datetimepicker({
                    format: 'YYYY-MM-DD',
                    minDate: '2007-01-01'
                })
            }
        })
    </script>
</section>
