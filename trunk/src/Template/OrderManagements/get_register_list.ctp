
<style>
    .get-register-list .modal-body button {
        width: 70%;
    }
    .get-register-list .button-m-b {
        margin-bottom: 8px;
    }
    .get-register-list h2 {
        margin: 0;
        padding-bottom: 10px;
        font-size: 18px;
    }
</style>
<section class="modal-wrapper">
    <div class="modal fade get-register-list" data-backdrop="static" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <?php
                    echo $this->Form->button('<span aria-hidden="true">&times;</span>', [
                        'type' => 'button',
                        'class' => 'close modal-close',
                        'data-dismiss' => 'modal',
                        'aria-label' => 'Close',
                        'escape' => false,
                    ]); ?>
                    <h4 class="modal-title text-center">
                        <?php echo __('ORDER_LIST_MODAL_TITLE'); ?>
                    </h4>
                </div>
                <div class="modal-body text-center">
                    <div class="row">
                        <div class="col-sm-6 col-md-6">
                            <h2 class="text-center">
                                <?php echo __('ORDER_LIST_DOMESTIC_TRADE') ?>
                            </h2>
                            <div class="button-m-b">
                                <?php
                                echo $this->Form->button(__('ORDER_LIST_SALE_W_STOCK {0}', ['<br>']), [
                                    'type' => 'button',
                                    'class' => 'btn btn-width btn-primary',
                                    'v-on:click' => 'button1',
                                ]); ?>
                            </div>
                            <div class="button-m-b">
                                <?php
                                echo $this->Form->button(__('ORDER_LIST_SALE_BEHALT_S_IN_JP {0}', ['<br>']), [
                                    'class' => 'btn btn-width btn-primary',
                                    'v-on:click' => 'button2',
                                    'type' => 'button',
                                ]); ?>
                            </div>
                            <div>
                                <?php
                                echo $this->Form->button(__('ORDER_LIST_PURCHASE_W_STOCK {0}', ['<br>']), [
                                    'class' => 'btn btn-width btn-primary',
                                    'v-on:click' => 'button3',
                                    'type' => 'button',
                                ]); ?>
                            </div>
                        </div>
                        <div class="col-sm-6 col-md-6">
                            <h2 class="text-center">
                                <?php echo __('ORDER_LIST_IMPORT_TRADE') ?>
                            </h2>
                            <div class="button-m-b">
                                <?php
                                echo $this->Form->button(__('ORDER_LIST_SALE_BEHALT_MP {0}', ['<br>']), [
                                    'class' => 'btn btn-width btn-primary',
                                    'v-on:click' => 'button4',
                                ]); ?>
                            </div>
                            <div class="button-m-b">
                                <?php
                                echo $this->Form->button(__('ORDER_LIST_SALE_BEHALT_S_OUTSITE_JP {0}', ['<br>']), [
                                    'class' => 'btn btn-width btn-primary',
                                    'v-on:click' => 'button5',
                                ]); ?>
                            </div>
                            <div>
                                <?php
                                echo $this->Form->button(__('ORDER_LIST_PURCHASE_MP_STOCK {0}', ['<br>']), [
                                    'class' => 'btn btn-width btn-primary',
                                    'v-on:click' => 'button6',
                                ]); ?>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="modal-footer">
                    <?php
                    echo $this->Form->button(__('TXT_CANCEL'), [
                        'type' => 'button',
                        'class' => 'btn btn-default btn-sm btn-width modal-close',
                        'data-dismiss' => 'modal',
                        'aria-label' => 'Close',
                    ]); ?>
                </div>
            </div>
        </div>
    </div>
    <script>
        var modal = new Vue({
            el: '.modal-wrapper',
            data: {
                url1: '<?= $this->Url->build('/sale-w-stocks/create-form/') ?>',
                url2: '<?= $this->Url->build('/order-s-in-jp/create-form/') ?>',
                url3: '<?= $this->Url->build('/order-to-s-in-jp/create-form/') ?>',
                url4: '<?= $this->Url->build('/sales/create-form/') ?>',
                url5: '<?= $this->Url->build('/order-to-snps/create-form/') ?>',
                url6: '<?= $this->Url->build('/order-to-s/create-form/') ?>'
            },
            methods: {
                button1: function(e) {
                    $.LoadingOverlay('show');
                    this.$http.get(this.url1, {kind_of: 'jp_only'}).then(function(response) {
                        $('body').prepend(response.body)
                        $('body').find('.create-form1').modal('show')
                        $.LoadingOverlay('hide')
                    });
                },
                button2: function(e) {
                    $.LoadingOverlay('show');
                    this.$http.get(this.url2, {kind_of: 'jp_only'}).then(function(response) {
                        $('body').prepend(response.body)
                        $('body').find('.create-form2').modal('show')
                        $.LoadingOverlay('hide')
                    });
                },
                button3: function(e) {
                    $.LoadingOverlay('show');
                    this.$http.get(this.url3, {kind_of: 'jp_only'}).then(function(response) {
                        $('body').prepend(response.body)
                        $('body').find('.create-form3').modal('show')
                        $.LoadingOverlay('hide')
                    });
                },
                button4: function(e) {
                    $.LoadingOverlay('show');
                    this.$http.get(this.url4, {kind_of: 'jp_only'}).then(function(response) {
                        $('body').prepend(response.body)
                        $('body').find('.create-form4').modal('show')
                        $.LoadingOverlay('hide')
                    });
                },
                button5: function(e) {
                    $.LoadingOverlay('show');
                    this.$http.get(this.url5, {kind_of: 'jp_only'}).then(function(response) {
                        $('body').prepend(response.body)
                        $('body').find('.create-form5').modal('show')
                        $.LoadingOverlay('hide')
                    })
                },
                button6: function(e) {
                    $.LoadingOverlay('show');
                    this.$http.get(this.url6, {kind_of: 'jp_only'}).then(function(response) {
                        $('body').prepend(response.body)
                        $('body').find('.create-form6').modal('show')
                        $.LoadingOverlay('hide')
                    })
                }
            }
        })
    </script>
</section>
