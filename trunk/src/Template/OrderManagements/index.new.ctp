
<style>
    .btn-wrap {
        white-space: nowrap;
    }
    .btn-wrap button {
        margin: 0 3px;
    }
    .btn-yen {
        border-radius: 50%;
        padding: unset;
        width: 30px;
        height: 30px;
    }
    .dv-icon {
        font-size: x-large;
        color: red;
        padding: 0 5px;
    }
    .bg-btn-yellow {
        background-color: #ffd700;
        color: #fff;
    }
</style>
<div class="search-section" style="margin-bottom: 20px;">
    <?php
    echo $this->Form->create('OrderManagements', [
        'role' => 'form',
        'class' => 'form-inline form-search',
        'type' => 'get',
        'name' => 'form_search'
    ]);
    $this->Form->templates([
        'inputContainer' => '<div class="form-group">{{content}}</div>',
    ]);
    echo $this->Form->hidden('displays', [
        'value' => empty($this->request->query('displays')) ? 10 : $this->request->query('displays')
    ]);
    echo $this->Form->input('keyword', [
        'name' => 'keyword',
        'placeholder' => __('USER_SEARCH_KEYWORD'),
        'class' => 'form-control',
        'label' => false,
        'value' => $this->request->query('keyword'),
        'autocomplete' => 'off',
    ]);
    $this->SwitchStatus->render();
    echo $this->Form->button(__('TXT_BTN_SEARCH'), [
        'type' => 'button',
        'class' => 'btn btn-primary',
        'style' => 'margin-left: 10px;',
        '@click' => 'searchModal',
    ]);
    echo $this->Form->submit('submit', [
        'style' => 'display: none',
    ]);
    echo $this->Form->end();
    ?>
</div>
<div class="row search-section" style="margin-bottom: 20px;">
    <?php echo $this->element('display_number'); ?>
    <div class="col-sm-6 pull-right" style="text-align: right;">
        <?php
        echo $this->Form->button(__('BTN_REGISTER'), [
            'type' => 'button',
            'class' => 'btn btn-sm btn-primary btn-width btn-register',
            'data-type' => 'new',
            'v-on:click' => 'registerAndUpdate',
        ]);
        ?>
    </div>
    <div class="clearfix"></div>
</div>


<div id="app">
    <order-list>
        <?php
        echo $this->element('OrderManagements/order_list'); ?>
    </order-list>
    <pagination>
        <?php
        echo $this->element('OrderManagements/pagination'); ?>
    </pagination>
    <div class="clearfix"></div>
</div>

<?php
echo $this->element('Modal/vue_suspend');
echo $this->element('/Modal/message');
echo $this->Html->css([
    'bootstrap-datetimepicker.min',
]);
echo $this->Html->script([
    'VueJs/vue.min.js',
    'VueJs/vue-resource.min.js',
    // '//unpkg.com/vuejs-paginate@0.9.0/dist/index.js',
], ['blog' => 'script']);
?>

<script>
    Vue.component('order-list');

    Vue.component('pagination');

    var app = new Vue({
        el: '#app',
        data: function() {
            return {
                orders: [],
                order: {
                    id: '',
                    type: '',
                    customer_id: '',
                    currency_id: '',
                    total_amount: '',
                    w_invoice_amount: '',
                    customer_type: '',
                    status: '',
                    supplier: '',
                    order_date: '',
                    sale_number: '',
                    amount: '',
                    paying: '',
                    user: '',
                    delivery_icon: '',
                    product: '',
                    delivery_to: '',
                    is_kind: '',
                    is_suspend: false
                },
                order_id: '',
                edit: false,
                pagination: {},
                starting_number: '',
                columns: [
                    '#',
                    '',
                    '<?php echo __('ORDER_MANAGEMENT_TXT_STATUS'); ?>',
                    '<?php echo __('ORDER_MANAGEMENT_TXT_ORDER_TYPE'); ?>',
                    '<?php echo __('ORDER_MANAGEMENT_TXT_SUPPLIER'); ?>',
                    '<?php echo __('ORDER_MANAGEMENT_TXT_ORDER_DATE'); ?>',
                    '<?php echo __('ORDER_MANAGEMENT_TXT_PO'); ?>',
                    '<?php echo __('ORDER_MANAGEMENT_TXT_DELIVERY_TO'); ?>',
                    '<?php echo __('ORDER_MANAGEMENT_TXT_PRODUCT'); ?>',
                    '<?php echo __('ORDER_MANAGEMENT_TXT_AMOUNT'); ?>',
                    '<?php echo __('ORDER_MANAGEMENT_TXT_DUE_AMOUNT'); ?>',
                    '<?php echo __('ORDER_MANAGEMENT_TXT_USER'); ?>',
                    '',
                    ''
                ]
            }
        },
        created: function() {
            this.getOrderList();
        },
        methods: {
            getOrderList(page_url) {
                let vm = this;
                vm.makeLoading(true);
                page_url = page_url || 'api/orders';
                fetch(page_url)
                    .then(res => res.json())
                    .then(res => {
                        this.orders = res.data;
                        vm.makePagination(res.pagination);
                        vm.makeLoading(false);
                    })
                    .catch(err => console.log(err));
            },

            makePagination(meta) {
                let pagination = {
                    current_page: meta.page,
                    current_page_link: 'api/orders/?page=' + meta.page,
                    page_count: meta.pageCount,
                    show_record: meta.current,
                    total_record: meta.count,
                    limit: meta.perPage,
                    first: (meta.pageCount != 0) ? 'api/orders/?page=1' : '',
                    last: (meta.pageCount != 0) ? 'api/orders/?page=' + meta.pageCount : '',
                    prev_page: (meta.prevPage === true) ? 'api/orders/?page=' + Number(meta.page - 1) : '',
                    next_page: (meta.nextPage === true) ? 'api/orders/?page=' + Number(meta.page + 1) : ''
                };
                this.pagination = pagination;
                this.starting_number = ((pagination.current_page -1 ) * 10) + 1;
            },

            makeLoading(is_load) {
                if (is_load === true) {
                    $.LoadingOverlay('show');
                } else {
                    $.LoadingOverlay('hide');
                }
            },

            clickLink(page_num) {
                let link = 'api/orders/?page=' + page_num;
                this.getOrderList(link);
            }
        }
    });
</script>
