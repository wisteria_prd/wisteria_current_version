
<style>
    .btn-wrap {
        white-space: nowrap;
    }
    .btn-wrap button {
        margin: 0 3px;
    }
    .btn-yen {
        border-radius: 50%;
        padding: unset;
        width: 30px;
        height: 30px;
    }
    .dv-icon {
        font-size: x-large;
        color: red;
        padding: 0 5px;
    }
    .bg-btn-yellow {
        background-color: #ffd700;
        color: #fff;
    }
</style>
<div class="search-section" style="margin-bottom: 20px;">
    <?php
    echo $this->Form->create('OrderManagements', [
        'role' => 'form',
        'class' => 'form-inline form-search',
        'type' => 'get',
        'name' => 'form_search'
    ]);
    $this->Form->templates([
        'inputContainer' => '<div class="form-group">{{content}}</div>',
    ]);
    echo $this->Form->hidden('displays', [
        'value' => empty($this->request->query('displays')) ? 10 : $this->request->query('displays')
    ]);
    echo $this->Form->input('keyword', [
        'name' => 'keyword',
        'placeholder' => __('USER_SEARCH_KEYWORD'),
        'class' => 'form-control',
        'label' => false,
        'value' => $this->request->query('keyword'),
        'autocomplete' => 'off',
    ]);
    $this->SwitchStatus->render();
    echo $this->Form->button(__('TXT_BTN_SEARCH'), [
        'type' => 'button',
        'class' => 'btn btn-primary',
        'style' => 'margin-left: 10px;',
        '@click' => 'searchModal',
    ]);
    echo $this->Form->submit('submit', [
        'style' => 'display: none',
    ]);
    echo $this->Form->end();
    ?>
</div>
<div class="row search-section" style="margin-bottom: 20px;">
    <?php echo $this->element('display_number'); ?>
    <div class="col-sm-6 pull-right" style="text-align: right;">
        <?php
        echo $this->Form->button(__('BTN_REGISTER'), [
            'type' => 'button',
            'class' => 'btn btn-sm btn-primary btn-width btn-register',
            'data-type' => 'new',
            'v-on:click' => 'registerAndUpdate',
        ]);
        ?>
    </div>
    <div class="clearfix"></div>
</div>


<div id="app">
    <order-list>
        <?php
        echo $this->element('OrderManagements/order_list'); ?>
    </order-list>

    <div class="row row-top-space" style="padding-bottom: 20px;">
        <div class="col-md-3 pull-left">
            <?php
            echo $this->Form->select('displays', [
                '10' => __('DISPLAY_ITEM {0}', [10]),
                '20' => __('DISPLAY_ITEM {0}', [20]),
                '50' => __('DISPLAY_ITEM {0}', [50]),
                '100' => __('DISPLAY_ITEM {0}', [100]),
            ], [
                'class' => 'form-control display-data',
                'id' => false,
                'label' => false,
                'value' => $this->request->query('displays') ? $this->request->query('displays') : 10
            ]); ?>
        </div>
        <div class="col-md-9">
            <nav class="pull-right">
                <paginate
                :page-count="page_count"
                :container-class="'pagination custom-pagination'"
                :prev-text="'<?=  __('BTN_PREV') ?>'"
                :next-text="'<?= __('BTN_NEXT') ?>'"
                :click-handler="setPage">
                </paginate>
            </nav>
        </div>
        <div class="clearfix"></div>
    </div>
</div>

<?php
echo $this->element('Modal/vue_suspend');
echo $this->element('/Modal/message');
echo $this->Html->css([
    'bootstrap-datetimepicker.min',
]);
echo $this->Html->script([
    'VueJs/vue.min.js',
    'VueJs/vue-resource.min.js',
    '//unpkg.com/vuejs-paginate@0.9.0/dist/index.js',
], ['blog' => 'script']);
?>
<script>
    Vue.component('oder-list')
    Vue.component('paginate', VuejsPaginate)

    new Vue({
        el: '#app',
        data: function() {
            return {
                row_number: 1,
                page_count: 1,
                sale_record_count: '<?php echo $saleRecordCounts ?>',
                sales: JSON.parse('<?php echo json_encode($sales) ?>'),
                purchases: JSON.parse('<?php echo json_encode($purchases) ?>'),
                locale: '<?php echo $locale ?>',
                data: [],
                buttons: {
                    report: '<span class="dv-icon"><?php echo BTN_ICON_REPORT ?></span>',
                    plane: '<span class="dv-icon"><?php echo BTN_ICON_PLANE ?></span>',
                    gift: '<span class="dv-icon"><?php echo BTN_ICON_GIFT ?></span>',
                    truck: '<span class="dv-icon"><?php echo BTN_ICON_TRUCK ?></span>',
                    paying: '<button type="button" class="btn-yen btn btn-sm btn-warning">済</button>'
                },
                columns: [
                    '#',
                    '',
                    '<?php echo __('ORDER_MANAGEMENT_TXT_STATUS'); ?>',
                    '<?php echo __('ORDER_MANAGEMENT_TXT_ORDER_TYPE'); ?>',
                    '<?php echo __('ORDER_MANAGEMENT_TXT_SUPPLIER'); ?>',
                    '<?php echo __('ORDER_MANAGEMENT_TXT_ORDER_DATE'); ?>',
                    '<?php echo __('ORDER_MANAGEMENT_TXT_PO'); ?>',
                    '<?php echo __('ORDER_MANAGEMENT_TXT_DELIVERY_TO'); ?>',
                    '<?php echo __('ORDER_MANAGEMENT_TXT_PRODUCT'); ?>',
                    '<?php echo __('ORDER_MANAGEMENT_TXT_AMOUNT'); ?>',
                    '<?php echo __('ORDER_MANAGEMENT_TXT_DUE_AMOUNT'); ?>',
                    '<?php echo __('ORDER_MANAGEMENT_TXT_USER'); ?>',
                    '',
                    ''
                ]
            }
        },
        methods: {
            getList: function() {
                let order_list = []
                let vm = this

                if (this.sales) {
                    this.sales.forEach(function(value, key) {
                        let order = {
                            row_number: this.row_number++,
                            id: value.id,
                            type: value.type,
                            total_amount: 0,
                            delivery_icon: vm.getDeliveryIcon(value.type),
                            supplier: vm.getSupplier(value.seller),
                            customer_id: value.customer_id,
                            currency_id: value.currency_id,
                            customer_type: value.customer.type,
                            is_suspend: value.is_suspend,
                            sale_number: value.sale_number,
                            status: textHumanize('_', value.status),
                            currency_formate: value.currency_formate,
                            order_date: value.order_date ? getFormattedDate(value.order_date) : '',
                            currency: getCurrencyByLocal(this.locale, value.currency),
                            delivery_to: (value.deliveries !== null) ? getNameByLocal(this.locale, value.deliveries.customer) : '',
                            product: (value.sale_details.length > 0) ? value.sale_details[0]['product_name'] : '',
                            user: concatFirstAndLastName(this.locale, value.user),
                            w_invoice_amount: (value.sale_details[0]) ? value.sale_details[0].total_amount : 0,
                            amount: (value.sale_details.length > 0) ? value.sale_details[0]['total_amount'] : 0,
                            paying: this.buttons.paying
                        }
                        if ((value.seller !== null) && (value.seller !== 'undefined')) {
                            order.is_kind = (value.seller.kind === 'mp') ? true : false;
                        }
                        if (value.sale_receive_payments) {
                            $.each(value.sale_receive_payments, function(key1, value1) {
                                order.total_amount += value1.receive_payment.amount
                            });
                        }
                        order_list.push(order);
                    }.bind(this))
                }
                this.data = order_list
            },

            getDeliveryIcon(type) {
                let icon = this.buttons.report
                switch (type) {
                    case '<?php echo TRADE_TYPE_SALE_BEHALF_S_OUTSIDE_JP ?>':
                        icon += this.buttons.plane
                        break;

                    case '<?php echo TRADE_TYPE_SALE_BEHALF_S_IN_JP ?>':
                        icon += this.buttons.truck
                        break;

                    case '<?php echo TRADE_TYPE_SALE_BEHALF_S_IN_JP ?>':
                        icon += this.buttons.gift
                        break;
                }
                return icon
            },

            getSupplier(seller) {
                let name = ''
                if ((seller === null) && (seller === 'undefined')) {
                    return false
                }
                if (seller.manufacturer !== null) {
                    name = getNameByLocal(this.locale, seller.manufacturer)
                }
                if (seller.supplier !== null) {
                    name = getNameByLocal(this.locale, seller.supplier)
                }
                return name
            },

            formatPrice(value) {
                let val = (value/1).toFixed(2).replace('.', ',')
                return val.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")
            },

            modalPopUp: function(data, event) {
                var content = $('body').find('#modal_suspend')
                var status = event.currentTarget.getAttribute('data-name')
                var txt = ''
                switch (status) {
                    case '<?= TARGET_SUSPEND ?>':
                        $('#btn_suspend_yes').text('<?= __('BTN_SUSPEND') ?>');
                        txt = '<?= __('MSG_CONFIRM_SUSPEND') ?>';
                        break;
                    case '<?= TARGET_DELETE ?>':
                        txt = '<?= __('MSG_CONFIRM_DELETE') ?>';
                        $('#btn_suspend_yes').text('<?= __('BTN_DELETE') ?>');
                        break;
                     case '<?= TARGET_RECOVER ?>':
                        $('#btn_suspend_yes').text('<?= __('BTN_RECOVER') ?>');
                        txt = '<?= __('MSG_CONFIRM_UNDO_SUSPEND') ?>';
                        break;
                }
                $(content).find('#btn_suspend_yes').attr({
                    'data-id': data.id,
                    'data-target': status,
                    'data-suspend': data.suspend
                })
                $(content).find('.modal-body p').html(txt)
                $(content).modal('show')
            },

            link: function(row, event) {
                location.href = '<?= $this->Url->build('/receive-payments/index?sale_id=') ?>' + row.id
            },

            setPage: function(page) {
                $.LoadingOverlay('show');
                this.$http.get('<?= $this->Url->build(['action' => 'getDatabyPage']) ?>', {
                    params: {'page': page}
                    })
                    .then(function(response) {
                        $.LoadingOverlay('hide')
                        this.row_number = ((page -1 ) * 10) + 1
                        this.sales = response.body.data
                        this.getList()
                    })
            }
        },
        mounted: function() {
            this.getList();
            let paging = this.sale_record_cound / 10
            if(paging % 1 === 0) {
                this.page_count = parseInt(paging)
            } else {
                this.page_count = parseInt(paging) + 1
            }
        },
        filters: {
            capitalize: function (str) {
                return str.charAt(0).toUpperCase() + str.slice(1)
            }
        },
    })




    $(function(e) {
        $('.btn_comment').click(function (e) {
            e.stopPropagation();
            let params = {
                id: $(this).find('b.data-id').text(),
                type: $(this).find('b.data-type').text(),
                referer: '<?php echo $this->request->controller; ?>'
            };
            let options = {
                url: BASE + 'Messages/getMessageList',
                type: 'GET',
                data: params,
                beforeSend: function () {
                    $.LoadingOverlay('show');
                }
            };
            ajaxRequest(options, SESSION_TIMEOUT, function (data) {
                $('body').prepend(data);
                $('body').find('.modal-message').modal('show');
            });
        });
    });

    // suspend and recover
    var suspend = new Vue({
        el: '#modal_suspend',
        data: {
            id: '',
            suspend: '',
            target: '',
            url: '<?= $this->Url->build('/sales/vue-update-suspend') ?>',
            deleteUrl: '<?= $this->Url->build('/sales/is-delete') ?>'
        },
        methods: {
            suspendAndRecover: function(event) {
                $.LoadingOverlay('show')
                this.id = event.currentTarget.getAttribute('data-id')
                this.target = event.currentTarget.getAttribute('data-target')
                if (this.target === '<?= TARGET_DELETE ?>') {
                    this.$http.post(this.deleteUrl, { id: this.id }, { emulateJSON: true, emulateHTTP: true })
                        .then(function(response) {
                            if (response.body.message === '<?= MSG_SUCCESS ?>') {
                                location.reload()
                            }
                        })
                } else {
                    this.suspend = event.currentTarget.getAttribute('data-suspend')
                    var params = {
                        id: this.id,
                        is_suspend: this.suspend
                    }
                    this.$http.post(this.url, params, { emulateJSON: true, emulateHTTP: true })
                        .then(function(response) {
                            if (response.body.message === '<?= MSG_SUCCESS ?>') {
                                location.reload()
                            }
                        })
                }
            }
        }
    })

    // register and update events
    var register = new Vue({
        el: '.btn-register',
        data: {
            create_url: '<?= $this->Url->build(['action' => 'getRegisterList']) ?>'
        },
        methods: {
            registerAndUpdate: function(e) {
                $.LoadingOverlay('show')
                this.$http.get(this.create_url, {body: 'blob'}).then(function(response) {
                    $('body').append(response.body)
                    $('body').find('.get-register-list').modal('show')
                    $.LoadingOverlay('hide')
                });
            }
        }
    });

    // advance search
    var advanceSearch = new Vue({
        el: '.search-section',
        methods: {
            searchModal: function() {
                $.LoadingOverlay('show')
                this.$http.get('<?= $this->Url->build(['action' => 'getAdvanceSearchForm']) ?>',
                    { body: 'blob' })
                    .then(function(response) {
                        $.LoadingOverlay('hide')
                        $('body').append(response.body)
                        $('body').find('.advance-search').modal('show')
                    })
            }
        }
    })
</script>
