<?php
$name = 'name' . $en;
echo $this->Form->create('Stocks', [
    'role' => 'form',
    'class' => 'form-stock-search',
    'type' => 'get',
    'name' => 'form_search',
]);
$this->Form->templates([
    'inputContainer' => '{{content}}'
]);
echo $this->Form->hidden('product_detail_id', ['class' => 'detail-id', 'value' => $this->request->query('product_detail_id')]);
echo $this->Form->hidden('displays', [
    'value' => empty($this->request->query('displays')) ? 10 : $this->request->query('displays')
]);
?>
<div class="row row-top-space">
    <div class="col-sm-12">
        <div class="row custom-select">
            <div class="col-sm-3 field-small-padding-right">
                <?php
                echo $this->Form->input('keyword', [
                    'name' => 'keyword',
                    'placeholder' => __d('stock', 'TXT_ENTER_KEYWORD'),
                    'class' => 'form-control',
                    'label' => false,
                    'id' => 'keywords',
                    'value' => $this->request->query('keyword'),
                ]); ?>
            </div>
            <div class="col-sm-2 field-small-padding-left field-small-padding-right custom-select">
                <?php
                echo $this->Form->select('affiliation_class', [
                    'wisteria' => __d('stock', 'TXT_WISTERIA'),
                    'medipro' => __d('stock', 'TXT_MEDIPRO'),
                ], [
                    'empty' => ['' => __d('stock', 'TXT_ALL')],
                    'class' => 'form-control affiliation',
                    'label' => false,
                    'default' => $this->request->query('affiliation_class')
                ]); ?>
            </div>
            <div class="col-sm-2 field-small-padding-left">
                <div class="input-group with-datepicker">
                    <?php
                    echo $this->Form->input('date', [
                        'class' => 'form-control datepicker',
                        'label' => false,
                        'placeholder' => __d('stock', 'TXT_ENTER_RECEIVED_DATE'),
                        'type' => 'text',
                        'value' => !empty($this->request->query('date')) ? date('Y-m-d', strtotime($this->request->query('date'))) : ''
                    ]); ?>
                    <div class="input-group-btn">
                        <button type="button" class="btn btn-default trigger-calendar" aria-label="Calendar">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <div class="row row-top-small-space filter-section">
            <div class="col-sm-2 field-small-padding-right custom-select">
                <?php
                $manuf = [];
                if ($manufacturers) {
                    foreach ($manufacturers as $m) {
                        $manuf[$m->id] = $this->Comment->nameEnOrJp($m->name, $m->name_en, $en);
                    }
                }
                echo $this->Form->select('manufacturer', $manuf, [
                    'empty' => ['' => __d('stock', 'TXT_SELECT_MANUFACTURER')],
                    'class' => 'form-control filter select-manufacturer',
                    'label' => false,
                    'default' => $this->request->query('manufacturer')
                ]);
                ?>
            </div>
            <div class="col-sm-2 field-small-padding-left field-small-padding-right custom-select">
                <?php
                echo $this->Form->select('brand', [], [
                    'empty' => ['' => __d('stock', 'TXT_SELECT_PRODUCT_BRAND')],
                    'class' => 'form-control filter select-brand',
                    'label' => false,
                    'disabled' => true,
                    'default' => $this->request->query('brand')
                ]);
                ?>
            </div>
            <div class="col-sm-2 field-small-padding-left field-small-padding-right custom-select">
                <?php
                echo $this->Form->select('product', [], [
                    'empty' => ['' => __d('stock', 'TXT_SELECT_PRODUCT')],
                    'class' => 'form-control filter select-product',
                    'label' => false,
                    'disabled' => true,
                    'default' => $this->request->query('product')
                ]);
                ?>
            </div>
            <!-- <div class="col-sm-2 field-small-padding-left field-small-padding-right custom-select">
                <?php
                // echo $this->Form->select('product_detail', [], [
                //     'empty' => ['' => __('TXT_SELECT_PRODUCT_DETAIL')],
                //     'class' => 'form-control filter select-product-detail',
                //     'label' => false,
                //     'disabled' => true,
                //     'default' => $this->request->query('product_detail')
                // ]);
                ?>
            </div> -->
            <div class="col-sm-2 field-small-padding-left">
                <?php $this->SwitchStatus->render(); ?>
            </div>
            <!-- <div class="col-sm-2 field-small-padding-left">
                <button type="button" class="btn btn-sm btn-primary" id="filter-search">
                    <?php //echo __('TXT_FILTER') ?>
                </button>
            </div> -->
        </div>
    </div>
</div>
<?php echo $this->Form->end(); ?>
<div class="total-pagination row-top-space" style="padding-bottom: 20px;">
    <?php echo $this->element('display_info'); ?>
</div>
<div class="row search-section" style="margin-bottom: 20px;">
    <?php echo $this->element('display_number'); ?>
    <div class="clearfix"></div>
</div>
<div class="data-tb-list">
    <?php if ($stocks) : ?>
    <table class="table table-striped">
        <thead>
            <tr>
                <th>#</th>
                <th>
                    <?php echo $this->Paginator->sort('id', __d('stock', 'TXT_STOCK')); ?>
                    <span><i class="fa fa-sort-desc" aria-hidden="true"></i></span>
                </th>
                <th>
                    <?php echo $this->Paginator->sort('purchase_id', __d('stock', 'TXT_PURCHASE_TYPE')); ?>
                    <span><i class="fa fa-sort-desc" aria-hidden="true"></i></span>
                </th>
                <th>
                    <?php echo $this->Paginator->sort('', __d('stock', 'TXT_RECEIVED_DATE')); ?>
                    <span><i class="fa fa-sort-desc" aria-hidden="true"></i></span>
                </th>
                <th>
                    <?php echo $this->Paginator->sort('', __d('stock', 'TXT_MANUFACTURER')); ?>
                    <span><i class="fa fa-sort-desc" aria-hidden="true"></i></span>
                </th>
                <th>
                    <?php echo $this->Paginator->sort('', __d('stock', 'TXT_PRODUCT')); ?>
                    <span><i class="fa fa-sort-desc" aria-hidden="true"></i></span>
                </th>
                <th>
                    <?php echo $this->Paginator->sort('', __d('stock', 'TXT_LOT#')); ?>
                    <span><i class="fa fa-sort-desc" aria-hidden="true"></i></span>
                </th>
                <th>
                    <?php echo $this->Paginator->sort('', __d('stock', 'TXT_EXPIRY')); ?>
                    <span><i class="fa fa-sort-desc" aria-hidden="true"></i></span>
                </th>
                <th>
                    <?php echo $this->Paginator->sort('', __d('stock', 'TXT_CONDITION')); ?>
                    <span><i class="fa fa-sort-desc" aria-hidden="true"></i></span>
                </th>
                <th>
                    <?php echo $this->Paginator->sort('', __d('stock', 'TXT_QUANTITY')); ?>
                    <span><i class="fa fa-sort-desc" aria-hidden="true"></i></span>
                </th>
                <th width="1%">&nbsp;</th>
            </tr>
        </thead>
        <tbody>
            <?php
            $numbering = $this->Paginator->counter('{{start}}');
            $output = ''; ?>
            <?php foreach ($stocks as $key => $stock): $numbering++; ?>
                <tr data-id="<?php echo $stock->id; ?>">
                    <td><?php echo $numbering; ?></td>
                    <td><?php echo $stock->purchase_id; ?></td>
                    <td><?php echo $stock->purchase_id; ?></td>
                    <td><?php echo date('Y-m-d', strtotime($stock->import_detail->import->receiving_date)); ?></td>
                    <td><?php echo $stock->import_detail->product_detail->product->product_brand->manufacturer->name_en; ?></td>
                    <td><?php echo $stock->import_detail->product_detail->product[$name]; ?></td>
                    <td><?php echo $stock->lot_number; ?></td>
                    <td><?php echo date('Y-m-d', strtotime($stock->expiry_date)); ?></td>
                    <td><?php echo $stock->damage_level; ?></td>
                    <td><?php echo $stock->quantity; ?></td>
                    <td>
                        <?php
                        echo $this->Form->button('<i class="fa fa-comments-o" aria-hidden="true"></i>', [
                            'class' => 'btn btn-primary btn-sm btn_comment',
                            'data-id' => $stock->id,
                            'escape' => false,
                        ]);
                        ?>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <?php endif; ?>
</div>
<div class="row row-top-space">
    <?php echo $this->element('display_number'); ?>
    <?= $this->element('next_prev') ?>
    <div class="clearfix"></div>
</div>
<?php
    echo $this->Html->css([
        'bootstrap-datetimepicker.min',
        'jquery-ui',
        'intlTelInput',
    ]);
?>
<?php
    $stock_detail = $this->request->here();
    $expload = explode('stock-detail?', $stock_detail);
    $stock_url = '';
    if (isset($expload[1]) && !empty($expload[1])) {
        $stock_url = str_replace('?', '', $expload[1]);
    } else {
        $stock_url = isset($expload[1]) ? $expload[1] : '';
    }
?>
<style>
    .select2-container--default .select2-selection--single .select2-selection__arrow {
        display: none !important;
    }
</style>
<script>
    (function(e) {
        $('.btn_comment').click(function (e) {
            e.stopPropagation();
            let params = {
                id: $(this).closest('tr').attr('data-id'),
                type: '<?php echo MESSAGE_TYPE_STOCK_DETAIL; ?>',
                referer: '<?php echo 'stocks/stock-detail?'.$stock_url; ?>',
            };
            let options = {
                url: BASE + 'messages/get-message-list',
                type: 'GET',
                data: params,
                beforeSend: function () {
                    $.LoadingOverlay('show');
                }
            };
            ajaxRequest(options, SESSION_TIMEOUT, function (data) {
                $('body').prepend(data);
                $('body').find('.modal-message').modal('show');
            });
        });

        $('.datepicker').datetimepicker({
           format: 'YYYY-MM-DD'
        });

        $('.trigger-calendar').click(function() {
            $(this).parents('.with-datepicker').find('.datepicker').focus();
        });

        //Keep brand selection on refresh page.
        if (sessionStorage.getItem('brand_dropdown')) {
            $('.select-brand').empty().removeAttr('disabled', true).append(sessionStorage.getItem('brand_dropdown'));
            $('.select-brand option').filter(function() {
                return ($(this).val() == '<?= $this->request->query('brand') ?>');
            }).prop('selected', true);
        }

        //Keep products selection on refresh page.
        if (sessionStorage.getItem('product_dropdown')) {
            $('.select-product').empty().removeAttr('disabled', true).append(sessionStorage.getItem('product_dropdown'));
            $('.select-product option').filter(function() {
                return ($(this).val() == '<?= $this->request->query('product') ?>');
            }).prop('selected', true);
        }

        //Keep product details selection on refresh page.
        if (sessionStorage.getItem('product_detail_dropdown')) {
            $('.select-product-detail').empty().removeAttr('disabled', true).append(sessionStorage.getItem('product_detail_dropdown'));
            $('.select-product-detail option').filter(function() {
                return ($(this).val() == '<?= $this->request->query('product_detail') ?>');
            }).prop('selected', true);
        }

        function clearStorage(select) {
            switch (select) {
                case 0:
                    sessionStorage.removeItem('brand_dropdown');
                    sessionStorage.removeItem('product_dropdown');
                    sessionStorage.removeItem('product_detail_dropdown');
                    break;
                case 1:
                    sessionStorage.removeItem('product_dropdown');
                    sessionStorage.removeItem('product_detail_dropdown');
                    break;
                case 2:
                    sessionStorage.removeItem('product_detail_dropdown');
                    break;
            }
        }

        function disabledUnselect(ch, defaultSelect) {
            $('.filter-section')
                .find('.filter:gt('+ch+')')
                .empty()
                .append('<option value="">'+ defaultSelect +'</option>')
                .attr('disabled', true);
        }

        $('body').on('change', '.select-manufacturer', function() {
            var manufacturer = $(this).val();
            if (manufacturer !== '') {
                $.LoadingOverlay('show');
                var url = '<?= $this->Url->build(['controller' => 'ProductBrands', 'action' => 'brandDropdown']) ?>';
                var params = { man_id: manufacturer };
                ajax_request_get(url, params, function(response) {
                    $.LoadingOverlay('hide');
                    if (response) {
                        sessionStorage.setItem('brand_dropdown', response);
                        $('.select-brand').removeAttr('disabled').empty().html(response);
                    } else {
                        clearStorage(0);
                        disabledUnselect(0, '<?= __('TXT_SELECT_BRAND_NAME') ?>');
                    }
                }, 'html', '<?= __('TXT_SESSION_TIMEOUT') ?>');
            } else {
                disabledUnselect(0, '<?= __('TXT_SELECT_BRAND_NAME') ?>');
                clearStorage(0);
            }
        });

        $('body').on('change', '.select-brand', function() {
            var brand = $(this).val();
            if (brand !== '') {
                $.LoadingOverlay('show');
                var url = '<?= $this->Url->build(['controller' => 'Products', 'action' => 'productDropdown']) ?>';
                var params = { brand_id: brand };
                ajax_request_get(url, params, function(response) {
                    $.LoadingOverlay('hide');
                    if (response) {
                        sessionStorage.setItem('product_dropdown', response);
                        $('.select-product').removeAttr('disabled').empty().html(response);
                    } else {
                        clearStorage(1);
                        disabledUnselect(1, '<?= __('TXT_SELECT_PRODUCT_NAME') ?>');
                    }
                }, 'html', '<?= __('TXT_SESSION_TIMEOUT') ?>');
            } else {
                disabledUnselect(1, '<?= __('TXT_SELECT_PRODUCT_NAME') ?>');
                clearStorage(1);
            }
        });

        $('body').on('change', '.select-product', function() {
            var product = $(this).val();
            var product_name = $(this).find('option:selected').text();
            if (product !== '') {
                $.LoadingOverlay('show');
                var url = '<?= $this->Url->build(['controller' => 'ProductDetails', 'action' => 'productDetailsDropdown']) ?>';
                var params = { product_id: product, product_name: product_name };
                ajax_request_get(url, params, function(response) {
                    $.LoadingOverlay('hide');
                    if (response) {
                        sessionStorage.setItem('product_detail_dropdown', response);
                        $('.select-product-detail').removeAttr('disabled').empty().html(response);
                    } else {
                        clearStorage(2);
                        disabledUnselect(2, '<?= __('TXT_SELECT_PRODUCT_DETAIL') ?>');
                    }
                }, 'html', '<?= __('TXT_SESSION_TIMEOUT') ?>');
            } else {
                disabledUnselect(2, '<?= __('TXT_SELECT_PRODUCT_DETAIL') ?>');
                clearStorage(2);
            }
        });

        /**
        * Search and filter
         * @type type
        */
        var optionData = [];

        autoCompletInitialize();

        function autoCompletInitialize() {
            var url = '<?= $this->Url->build('/stocks/auto-complete-list'); ?>';
            ajax_request_get(url, {}, function(response) {
                $.LoadingOverlay('hide');
                if (response.data) {
                    $.each(response.data, function (i, v) {
                        var productName = '';
                        productName += productAndBrandName(v);
                        if (v.product_details) {
                            $.each(v.product_details, function (k, val) {
                                productName += productDetails(val, response.en);
                                optionData.push({
                                    id: val.id,
                                    text: productName
                                });
                            });
                        }
                    });
                    selectInitialize(optionData);
                }
            }, 'json', '<?= __('TXT_SESSION_TIMEOUT') ?>');
        }

        function selectInitialize(optionData) {
            $('#keywords').select2({
                width: '100%',
                allowClear: true,
                multiple: false,
                maximumSelectionSize: 1,
                placeholder: '<?= __('USER_SEARCH_KEYWORD') ?>',
                data: optionData
            });
        }

        $('body').on('change', '.affiliation', function () {
            $('.form-stock-search').submit();
        });

        $('body').on('change', '#keywords', function () {
            if ($('.detail-id').val($(this).val())) {
                $('form[name="form_search"]').submit();
            }
        });

        $('body').on('click', '#filter-search', function () {
            $('form[name="form_search"]').submit();
        });

        pageOutOfRange(<?= $paging ?>, '<?= $this->request->query('page') ?>');
        var maxPage = parseInt($('#maxPage').val());
        updatePagination(maxPage);
    })();
</script>
