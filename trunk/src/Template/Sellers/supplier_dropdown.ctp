<?php
if ($suppliers):
    //$name = 'name' . $en;
    $output = '<option value="">' . __('TXT_SELECT_SUPPLIER') . '</option>';
    foreach ($suppliers as $supply) {
        $output .= '<option value="' . $supply->supplier->id . '">' . 
            $this->Comment->nameEnOrJp($supply->supplier->name, $supply->supplier->name_en, $en) .
            '</option>';
    }
    echo $output;
endif;