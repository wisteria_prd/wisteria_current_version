<?php if ($data): ?>
    <tr data-id="<?= h($data->id); ?>">
        <th scope="row"><?= $num; ?></th>
        <td>
            <?= date('Y-m-d', strtotime($data->from_date)) ?>
        </td>
        <td>
            <?= date('Y-m-d', strtotime($data->to_date)) ?>
        </td>
        <td>
            <?= $data->from_qty ?>
        </td>
        <td>
            <?= $data->to_qty ?>
        </td>
        <td>
            <?= $data->unit_price . __(' 円') ?>
        </td>
        <td>
            <?php
                echo $this->Form->button(__('FOC'), [
                    'type' => 'button',
                    'class' => 'btn btn-info btn-sm select-foc',
                    'data-id' => $data->id,
                    'data-pricingtype' => TYPE_SELLER_PRICING,
                    'data-spid' => $data->seller_product_id
                ]);
            ?>
        </td>
        <td width="1%">
            <?php
            echo $this->Form->button(BTN_ICON_EDIT, [
                'type' => 'button',
                'class' => 'btn btn-primary btn-sm btn-edit-discount',
                'escape' => false,
                'data-id' => $data->id,
                'data-spid' => $data->seller_product_id
            ]);
            ?>
        </td>
        <td width="1%">
            <?php
                echo $this->Form->button(BTN_ICON_DELETE, [
                    'type' => 'button',
                    'class' => 'btn btn-delete btn-sm',
                    'id' => 'btn-delete',
                    'escape' => false,
                    'data-id' => $data->id,
                ]);
            ?>
        </td>
    </tr>
<?php endif; ?>