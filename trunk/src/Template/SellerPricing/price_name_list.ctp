<?php
if ($products):
    $output = '<option value="">' . __('TXT_SELECT_PRODUCT_DETAIL') . '</option>';
    $name = 'name' . $en;
    if ($type == TYPE_PRODUCT_DETAILS) {
        foreach ($products as $product) {
            $detail = $product->product->$name . ' ';
            $detail .= '(';
            $detail .= $product->pack_size;
            $detail .= $product->tb_pack_sizes->name . $en;
            $detail .= ' x ';
            $detail .= $product->single_unit_size;
            $detail .= $product->single_unit->name . $en;
            $detail .= ')';
            $output .= '<option value="' . $product->id . '">' . $detail . '</option>';
        }
    } else {
        foreach ($products as $product) {
            $output .= '<option value="' . $product->id . '">' . $product->$name . '</option>';
        }
    }
    echo $output;
endif;