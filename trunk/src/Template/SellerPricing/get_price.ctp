<?php if ($data): ?>
<table class="table table-striped" id="discount-list">
    <thead>
        <tr>
            <th>#</th>
            <th class="white-space">
                <?php echo __('TXT_FROM_DATE'); ?>
            </th>
            <th class="white-space">
                <?php echo __('TXT_TO_DATE'); ?>
            </th>
            <th class="white-space">
                <?php echo __('TXT_FROM_QUANTITY'); ?>
            </th>
            <th class="white-space">
                <?php echo __('TXT_TO_QUANTITY'); ?>
            </th>
            <th class="white-space">
                <?php echo __('TXT_UNIT_PRICE'); ?>
            </th>
            <th colspan="5">&nbsp;</th>
        </tr>
    </thead>
    <tbody>
        <?php
        $num = 0;
        foreach ($data as $item) : $num++;
        ?>
        <tr data-id="<?= h($item->id); ?>">
            <th scope="row"><?= $num; ?></th>
            <td>
                <?= date('Y-m-d', strtotime($item->from_date)) ?>
            </td>
            <td>
                <?= date('Y-m-d', strtotime($item->to_date)) ?>
            </td>
            <td>
                <?= $item->from_qty ?>
            </td>
            <td>
                <?= $item->to_qty ?>
            </td>
            <td>
                <?= $item->unit_price . __(' 円') ?>
            </td>
            <td>
                <?php
                    echo $this->Form->button(__('FOC'), [
                        'type' => 'button',
                        'class' => 'btn btn-info btn-sm select-foc',
                        'data-id' => $item->id,
                        'data-pricingtype' => TYPE_SELLER_PRICING,
                        'data-spid' => $item->seller_product_id
                    ]);
                ?>
            </td>
            <td width="1%">
                <?php
                echo $this->Form->button('<i class="fa fa-comments-o" aria-hidden="true"></i>', [
                    'class' => 'btn btn-primary btn-sm btn_comment',
                    'data-id' => $item->id,
                    'escape' => false,
                ]);
                ?>
            </td>
            <td width="1%">
                <?php
                echo $this->Form->button(BTN_ICON_EDIT, [
                    'type' => 'button',
                    'class' => 'btn btn-primary btn-sm btn-edit-discount',
                    'escape' => false,
                    'data-id' => $item->id,
                    'data-spid' => $item->seller_product_id
                ]);
                ?>
            </td>
            <td width="1%">
                <?php
                    echo $this->Form->button(BTN_ICON_DELETE, [
                        'type' => 'button',
                        'class' => 'btn btn-delete btn-sm',
                        'id' => 'btn-delete',
                        'escape' => false,
                        'data-id' => $item->id,
                    ]);
                ?>
            </td>
        </tr>
        <?php
        endforeach;
        ?>
    </tbody>
</table>
<?php endif; ?>
<script>
    $(function() {
        $('.btn_comment').click(function (e) {
            e.stopPropagation();
            let params = {
                id: $(this).closest('tr').attr('data-id'),
                type: '<?php echo MESSAGE_TYPE_SELLER_PRICING; ?>',
                referer: '<?php echo 'seller-pricing'; ?>',
            };
            let options = {
                url: BASE + 'messages/get-message-list',
                type: 'GET',
                data: params,
                beforeSend: function () {
                    $.LoadingOverlay('show');
                }
            };
            ajaxRequest(options, SESSION_TIMEOUT, function (data) {
                $('body').prepend(data);
                $('body').find('.modal-message').modal('show');
            });
        });
    });
</script>
