
<div class="row row-top-space">
    <div class="col-sm-8 col-sm-offset-2">
        <?php
        echo $this->Form->create('SellerPricing', [
            'role' => 'form',
            'class' => 'form-horizontal form-search',
            'type' => 'get',
            'name' => 'common_form'
        ]);
        ?>
        <div class="form-group custom-select">
            <label class="col-sm-3 control-label"><?= __('TXT_SELLER') ?></label>
            <div class="col-sm-6">
                <select class="form-control" name="seller_id" id="select-seller">
                    <?= $this->Comment->sellerDropdown($sellers, $en); ?>
                </select>
            </div>
        </div>
        <div class="form-group custom-select">
            <label class="col-sm-3 control-label" for="product_detail_id"><?= __('TXT_PRODUCT') ?></label>
            <div class="col-sm-6">
                <select class="form-control select-product-details" name="external_id" id="select-product-detail" readonly="readonly">
                    <option value=""><?= __('TXT_SELECT_PRODUCT_NAME') ?></option>
                </select>
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-offset-3 col-sm-4">
                <button type="button" class="btn btn-primary btn-get-price" id="get-price" disabled="disabled"><?= __('TXT_GET_PRICE') ?></button>
            </div>
        </div>
        <?php
        echo $this->Form->end();
        ?>
    </div>
</div>

<section id="create_form">
    <div class="row">
        <div class="col-sm-10 col-sm-offset-1">
            <?php
            echo $this->Form->create('SellerPricing', [
                'role' => 'form',
                'class' => 'form-horizontal form-seller-pricing',
                'name' => 'common_form'
            ]);
            $this->Form->templates([
                'inputContainer' => '{{content}}'
            ]);
            echo $this->Form->hidden('seller_product_id', ['class' => 'seller-product-id', 'value' => '']);
            ?>
            <div class="form-group custom-select">
                <label class="col-sm-2 control-label"><?= __('TXT_SELLER') ?></label>
                <div class="col-sm-4">
                    <select class="form-control sl-pricing" name="seller_id" id="seller-pricing-seller">
                        <?= $this->Comment->sellerDropdown($sellers, $en); ?>
                    </select>
                    <span class="help-block help-tips" id="error-seller_id"></span>
                </div>
                <label class="col-sm-2 control-label"><?= __('TXT_PRODUCT') ?><?= $this->Comment->formAsterisk() ?></label>
                <div class="col-sm-4">
                    <?php echo $this->Form->select('product_detail_id', [], [
                        'class' => 'form-control select-product-details',
                        'label' => false,
                        'required' => false,
                        'id' => 'product-detail-id',
                        'empty' => ['' => __('TXT_DEFAULT_SELECT')],
                        'readonly' => true
                    ]); ?>
                    <span class="help-block help-tips" id="error-product_detail_id"></span>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label"><?= __('TXT_FROM_DATE') ?><?= $this->Comment->formAsterisk() ?></label>
                <div class="col-sm-4">
                    <?php echo $this->Form->input('from_date', [
                        'type' => 'text',
                        'class' => 'form-control',
                        'label' => false,
                        'required' => false,
                        'id' => 'from-date'
                    ]); ?>
                    <span class="help-block help-tips" id="error-from_date"></span>
                </div>
                <label class="col-sm-2 control-label"><?= __('TXT_TO_DATE') ?><?= $this->Comment->formAsterisk() ?></label>
                <div class="col-sm-4">
                    <?php echo $this->Form->input('to_date', [
                        'type' => 'text',
                        'class' => 'form-control',
                        'label' => false,
                        'required' => false,
                        'id' => 'to-date'
                    ]); ?>
                    <span class="help-block help-tips" id="error-to_date"></span>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label"><?= __('TXT_FROM_QUANTITY') ?><?= $this->Comment->formAsterisk() ?></label>
                <div class="col-sm-4">
                    <?php echo $this->Form->input('from_qty', [
                        'class' => 'form-control clear',
                        'label' => false,
                        'required' => false,
                        'id' => 'from-qty'
                    ]); ?>
                    <span class="help-block help-tips" id="error-from_qty"></span>
                </div>
                <label class="col-sm-2 control-label"><?= __('TXT_TO_QUANTITY') ?><?= $this->Comment->formAsterisk() ?></label>
                <div class="col-sm-4">
                    <?php echo $this->Form->input('to_qty', [
                        'class' => 'form-control clear',
                        'label' => false,
                        'required' => false,
                        'id' => 'to-qty'
                    ]); ?>
                    <span class="help-block help-tips" id="error-to_qty"></span>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label"><?= __('TXT_UNIT_PRICE') ?><?= $this->Comment->formAsterisk() ?></label>
                <div class="col-sm-4">
                    <?php echo $this->Form->input('unit_price', [
                        'class' => 'form-control clear',
                        'label' => false,
                        'required' => false
                    ]); ?>
                    <span class="help-block help-tips" id="error-unit_price"></span>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-12 text-center">
                    <?php echo $this->Form->button(__('TXT_CLEAR'), [
                        'type' => 'button',
                        'class' => 'btn btn-default form_btn btn-width btn-sm',
                        'id' => 'btn-clear',
                    ]); ?>
                    &nbsp; &nbsp;
                    <?php
                    echo $this->Form->button(__('TXT_REGISTER_SAVE'), [
                        'type' => 'button',
                        'class' => 'btn btn-primary form_btn btn-width btn-sm',
                        'id' => 'btn-register',
                    ]);
                    ?>
                </div>
            </div>
            <?= $this->Form->end(); ?>
        </div>
    </div>
</section>

<div class="row">
    <div class="col-sm-10 col-sm-offset-1">
        <div class="data-tb-list">
            <table class="table table-striped" id="discount-list">
                <thead>
                    <tr>
                        <th>#</th>
                        <th class="white-space">
                            <?php
                            echo __d('seller_pricing', 'TXT_SUPPLIER'); ?>
                        </th>
                        <th class="white-space">
                            <?php
                            echo __d('seller_pricing', 'TXT_MANUFACTURER'); ?>
                        </th>
                        <th class="white-space">
                            <?php
                            echo __d('seller_pricing', 'TXT_PRODUCT'); ?>
                        </th>
                        <th class="white-space">
                            <?php
                            echo __('TXT_FROM_DATE'); ?>
                        </th>
                        <th class="white-space">
                            <?php
                            echo __('TXT_TO_DATE'); ?>
                        </th>
                        <th class="white-space">
                            <?php
                            echo __('TXT_FROM_QUANTITY'); ?>
                        </th>
                        <th class="white-space">
                            <?php
                            echo __('TXT_TO_QUANTITY'); ?>
                        </th>
                        <th class="white-space">
                            <?php
                            echo __('TXT_UNIT_PRICE'); ?>
                        </th>
                        <th colspan="3">&nbsp;</th>
                    </tr>
                </thead>
                <tbody></tbody>
            </table>
        </div>
    </div>
</div>
<!--Modal Detail-->
<?php
echo $this->element('Modal/view_detail'); ?>
<!--Modal delete-->
<?php
echo $this->element('Modal/delete'); ?>
<!--Modal Suspend-->
<?php
echo $this->element('Modal/suspend'); ?>
<?php
echo $this->Form->hidden('pricing_id', [
    'id' => 'pricing-id',
    'value' => ''
]);
echo $this->Form->hidden('seller_id', [
    'id' => 'seller-id',
    'value' => ''
]);
echo $this->Form->hidden('pricing_type', [
    'id' => 'pricing-type',
    'value' => ''
]);
echo $this->Form->hidden('external_id', [ //not the id of pricing for FOC
    'id' => 'external-id',
    'value' => ''
]);
?>
<?php
echo $this->element('Modal/product_list') ?>
<?php
echo $this->Html->css([
    'bootstrap-datetimepicker.min',
    'jquery-ui',
    'intlTelInput',
]); ?>

<script>
    (function() {
        $(document).on('show.bs.modal', '.modal', function () {
            var zIndex = 1040 + (10 * $('.modal:visible').length);
            $(this).css('z-index', zIndex);
            setTimeout(function() {
                $('.modal-backdrop').not('.modal-stack').css('z-index', zIndex - 1).addClass('modal-stack');
            }, 0);
        });

        $('#from-date').datetimepicker({
           format: 'YYYY-MM-DD'
        });

        $('#to-date').datetimepicker({
           format: 'YYYY-MM-DD'
        });

        $('#select-seller').select2();
        $('#seller-pricing-seller').select2();

        function reIndexList() {
            $('#discount-list tbody tr').each(function (i, v) {
                $(this).find('th').text(i+1);
            });
        }

        function productDetailDropdown(seller_id, auto) {
            $.ajax({
                url: '<?= $this->Url->build(['controller' => 'SellerPricing', 'action' => 'productDetailList']) ?>',
                type: 'GET',
                data: {seller_id: seller_id},
                dataType: 'HTML',
                beforeSend: function() {
                    $.LoadingOverlay('show');
                },
                success: function(response) {
                    $.LoadingOverlay('hide');
                    if (auto === true) {
                        if (response) {
                            $('#product-detail-id').empty().html(response).select2();
                        } else {
                            $('#product-detail-id')
                            .empty()
                            .html('<option value=""><?= __('TXT_SELECT_PRODUCT_NAME') ?></option>')
                            .attr('readonly', true);
                        }
                    } else {
                        if (response) {
                            $('.select-product-details').empty().html(response).select2();
                        } else {
                            $('.select-product-details')
                            .empty()
                            .html('<option value=""><?= __('TXT_SELECT_PRODUCT_NAME') ?></option>')
                            .attr('readonly', true);
                        }
                    }
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    $.LoadingOverlay('hide');
                    if (errorThrown === 'Forbidden') {
                        if (confirm('<?= __('TXT_SESSION_TIMEOUT') ?>')) {
                            location.reload();
                        }
                    }
                },
                complete: function () {
                    if (auto === true) {

                    }
                }
            });
        }

        $('#select-seller').on('change', function () {
            var seller_id = $(this).val();
            if (seller_id !== '') {
                productDetailDropdown(seller_id, false);
            } else {
                $('.select-product-details')
                    .empty()
                    .html('<option value=""><?= __('TXT_SELECT_PRODUCT_NAME') ?></option>')
                    .attr('readonly', true);
            }
        });

        $('#select-product-detail').on('change', function () {
            $('#get-price').attr('disabled', true);
            if ($(this).val() !== '') {
                $('#get-price').removeAttr('disabled', true);
            }
        });

        $('body').on('click', '#get-price', function() {
            $('#btn-clear').click(); //clear form first.
            var sellerOptions = $('#select-seller > option').clone();
            $('#seller-pricing-seller').empty().append(sellerOptions).val($('#select-seller').val());

            var detailOptions = $('#select-product-detail > option').clone();
            $('#product-detail-id').empty().append(detailOptions);

            var pd_detail_id = $('#select-product-detail').val();
            $('#product-detail-id').val(pd_detail_id).change();

        });

        $('#seller-pricing-seller').on('change', function () {
            var seller_id = $(this).val();
            if (seller_id !== '') {
                var seller_product_id = $('#seller-pricing-seller option:selected').attr('data-spid');
                $('.seller-product-id').val(seller_product_id);
                productDetailDropdown(seller_id, true);
            } else {
                $('#product-detail-id')
                    .empty()
                    .html('<option value=""><?= __('TXT_SELECT_PRODUCT_NAME') ?></option>')
                    .attr('readonly', true);
            }
        });

        $('#product-detail-id').on('change', function () {
            var detail_id = $(this).val();
            if (detail_id !== '') {
                var seller_product_id = $('#product-detail-id option:selected').attr('data-spid');
                $('.seller-product-id').val(seller_product_id);
                get_price(detail_id);
            } else {
                $('.seller-product-id').val('');
            }
        });

        $('body').on('click', '#btn-register', function() {
            $('.help-block').empty().hide();
            var data = $('.form-seller-pricing').serialize();
            var url = '<?= $this->Url->build(['controller' => 'SellerPricing', 'action' => 'create']) ?>';
            var update = false;
            if ($('.discount-update-id').length) {
                url = '<?= $this->Url->build(['controller' => 'SellerPricing', 'action' => 'edit']) ?>';
                update = true;
            }
            var detail_id = $('#product-detail-id').val();
            $.ajax({
                url: url,
                type: 'POST',
                dataType: 'JSON',
                data: data,
                cache: false,
                beforeSend: function() {
                    $.LoadingOverlay('show');
                },
                success: function(response) {
                    $.LoadingOverlay('hide');
                    if (response.status === 1) {
                        $('#btn-register').text('<?= __('TXT_REGISTER_SAVE') ?>');
                        //clear form then load the new inserted data.
                        if (update === false) {
                            clearForm('no');
                            get_single_price(response.inserted_id);
                        } else {
                            clearForm('yes');
                            $('.discount-update-id').remove();
                            get_price(detail_id);
                        }
                    } else if (response.status === 0) {
                        $.each(response.message, function(key, elem) {
                            var validate = $('#error-' + key);
                            $.each(elem, function(k, v) {
                                validate.removeClass('help-tips')
                                    .addClass('error-tips')
                                    .text(v)
                                    .show();
                            });
                        });
                    }
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    $.LoadingOverlay('hide');
                    if (errorThrown === 'Forbidden') {
                        if (confirm('<?= __('TXT_SESSION_TIMEOUT') ?>')) {
                            location.reload();
                        }
                    }
                }
            });
        });

        function get_price (detail_id) {
            $.ajax({
                url: '<?= $this->Url->build(['controller' => 'SellerPricing', 'action' => 'getPrice']) ?>',
                type: 'GET',
                data: {detail_id: detail_id},
                dataType: 'HTML',
                beforeSend: function() {
                    $.LoadingOverlay('show');
                },
                success: function(response) {
                    $.LoadingOverlay('hide');
                    $('.data-tb-list').empty().html(response);
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    $.LoadingOverlay('hide');
                    if (errorThrown === 'Forbidden') {
                        if (confirm('<?= __('TXT_SESSION_TIMEOUT') ?>')) {
                            location.reload();
                        }
                    }
                }
            });
        }

        function get_single_price (inserted_id) {
            $.ajax({
                url: '<?= $this->Url->build(['controller' => 'SellerPricing', 'action' => 'getSinglePrice']) ?>',
                type: 'GET',
                data: {inserted_id: inserted_id},
                dataType: 'HTML',
                beforeSend: function() {
                    $.LoadingOverlay('show');
                },
                success: function(response) {
                    $.LoadingOverlay('hide');
                    $('#discount-list tbody').prepend(response);
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    $.LoadingOverlay('hide');
                    if (errorThrown === 'Forbidden') {
                        if (confirm('<?= __('TXT_SESSION_TIMEOUT') ?>')) {
                            location.reload();
                        }
                    }
                },
                complete: function () {
                    reIndexList();
                }
            });
        }

        //get pricing edit on form.
        $('body').on('click', '.btn-edit-discount', function () {
            var id = $(this).attr('data-id');
            $('#btn-register').text('<?= __('TXT_SAVE') ?>');
            $('#seller-pricing-seller').attr('disabled', true);
            $('#product-detail-id').attr('disabled', true);
            if ($('.discount-update-id').length) {
                $('.discount-update-id').remove();
            }
            $('.form-seller-pricing').append('<input type="hidden" name="id" value="'+id+'" class="discount-update-id">');
            var tr = $(this).closest('tr');
            $.each(tr, function(i, v) {
                $('#from-date').val($.trim($(this).find('td').eq(0).text()));
                $('#to-date').val($.trim($(this).find('td').eq(1).text()));
                $('#from-qty').val($.trim($(this).find('td').eq(2).text()));
                $('#to-qty').val($.trim($(this).find('td').eq(3).text()));
                $('#unit-price').val($.trim($(this).find('td').eq(4).text()).replace(' 円', ''));
            });
        });

        $('body').on('click', '#btn-clear', function () {
            clearForm('yes');
        });

        function clearForm(clearSelect) {
            $('.form-seller-pricing').find('.clear').val('');
            $('#seller-pricing-seller').removeAttr('disabled', true);
            $('#product-detail-id').removeAttr('disabled', true);
            if (clearSelect === 'yes') {
                $('.form-seller-pricing').find('input[type="text"]').val('');
                $('#seller-pricing-seller').val('').change();
                $('#discount-list tbody').empty();
            }
        }

        $('body').on('click', '.btn-delete', function() {
            var id = $(this).attr('data-id');
            $('.delete-item').val(id);
            $('#modal_delete').modal('show');
            $('.btn-delete-item').attr('id', 'btn-delete-seller-pricing-yes');
        });

        $('body').on('click', '#btn-delete-seller-pricing-yes', function() {
            var data = $('.form-delete').serialize();
            var id = $('.delete-item').val();
            $.ajax({
                url: '<?= $this->Url->build(['controller' => 'SellerPricing', 'action' => 'delete']) ?>',
                type: 'POST',
                data: data,
                dataType: 'JSON',
                cache: false,
                beforeSend: function() {
                    $.LoadingOverlay('show');
                },
                success: function(response) {
                    $.LoadingOverlay('hide');
                    if (response.status === 1) {
                        $('.close-modal').click();
                        $('#discount-list tbody tr[data-id="'+id+'"]').remove();
                        reIndexList();
                    } else {
                        alert('<?= __('Cannot delete this item, please try again.') ?>');
                    }
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    $.LoadingOverlay('hide');
                    if (errorThrown === 'Forbidden') {
                        if (confirm('<?= __('TXT_SESSION_TIMEOUT') ?>')) {
                            location.reload();
                        }
                    }
                }
            });
        });

        /**
        * FOC Section
         */
        $('body').on('click', '.select-foc', function() {
            var price_type = $(this).attr('data-pricingtype');
            var pricing_id = $(this).attr('data-id');
            //var pdetail_id = $('#select-product-details option:selected').attr('data-pid'); //product or package id
            $('#pricing-type').val(price_type);
            $('#pricing-id').val(pricing_id);
            var data = {
                price_type: price_type,
                pricing_id: pricing_id
            };

            get_update_foc(data, false);
        });

        function get_update_foc(data, update) {
            $.ajax({
                url: '<?= $this->Url->build(['controller' => 'SellerPricing', 'action' => 'getFoc']) ?>',
                type: 'GET',
                data: data,
                dataType: 'HTML',
                beforeSend: function() {
                    $.LoadingOverlay('show');
                },
                success: function(response) {
                    $.LoadingOverlay('hide');
                    $('#modal_detail .modal-body').empty().html(response);
                    if (update === false) {
                        $('#modal_detail').modal('show');
                    }
                    $('#product').select2({
                        width: '100%'
                    });
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    $.LoadingOverlay('hide');
                    if (errorThrown === 'Forbidden') {
                        if (confirm('<?= __('TXT_SESSION_TIMEOUT') ?>')) {
                            location.reload();
                        }
                    }
                },
                complete: function () {
                    $('.foc-external-id').val($('#pricing-id').val()); //id of pricing for FOC external_id
                    $('.foc-type').val('<?= TYPE_SELLER_PRICING ?>');
                }
            });
        }

        //create or update FOC
        $('body').on('click', '.btn-foc-discount', function() {
            var data = $('.form-foc').serialize();
            var id = $('.foc-discount-id').val();
            var url = '<?= $this->Url->build(['controller' => 'FocDiscounts', 'action' => 'create']) ?>';
            if (id !== '') {
                url = '<?= $this->Url->build(['controller' => 'FocDiscounts', 'action' => 'edit']) ?>';
            }
            $.ajax({
                url: url,
                type: 'POST',
                dataType: 'JSON',
                data: data,
                cache: false,
                beforeSend: function() {
                    $.LoadingOverlay('show');
                },
                success: function(response) {
                    $.LoadingOverlay('hide');
                    if (response.status === 1) {
                        var price_type = $('#pricing-type').val();
                        var external_id = $('#external-id').val();
                        var pricing_id = $('#pricing-id').val();
                        var data = {
                            price_type: price_type,
                            external_id: external_id,
                            pricing_id: pricing_id
                        };

                        get_update_foc(data, true);
                    } else if (response.status === 0) {
                        $.each(response.message, function(key, elem) {
                            var validate = $('#error-' + key);
                            $.each(elem, function(k, v) {
                                validate.removeClass('help-tips')
                                    .addClass('error-tips')
                                    .text(v)
                                    .show();
                            });
                        });
                    }
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    $.LoadingOverlay('hide');
                    if (errorThrown === 'Forbidden') {
                        if (confirm('<?= __('TXT_SESSION_TIMEOUT') ?>')) {
                            location.reload();
                        }
                    }
                }
            });
        });

        //Get FOC in form edit.
        $('body').on('click', '.btn-edit', function() {
            var tr = $(this).parents('tr');
            var id = $(this).attr('data-id');
            var detail_id = $(this).attr('data-detail-id');
            $('#value').val(tr.find('.discount-value').text());
            $('#product').val(detail_id).change();
            $('.foc-discount-id').val(id);
            $('.btn-foc-discount').text('<?= __('BTN_UPDATE') ?>');
        });

        $('body').on('click', '.btn-delete-discount', function() {
            var id = $(this).attr('data-id');
            $('.delete-item').val(id);
            $('.btn-delete-item').attr('id', 'btn-delete-foc-yes');
            $('#modal_delete').modal('show');
        });

        $('body').on('click', '#btn-delete-foc-yes', function() {
            var data = $('.form-delete').serialize();
            var id = $('.delete-item').val();
            $.ajax({
                url: '<?= $this->Url->build(['controller' => 'FocDiscounts', 'action' => 'delete']) ?>',
                type: 'POST',
                dataType: 'JSON',
                data: data,
                cache: false,
                beforeSend: function() {
                    $.LoadingOverlay('show');
                },
                success: function(response) {
                    $.LoadingOverlay('hide');
                    if (response.status === 1) {
                        $('.foc-' + id).remove();
                    } else if (response.status === 0) {
                        alert(response.message);
                    }
                    $('.close-modal').click();
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    $('.close-modal').click();
                    $.LoadingOverlay('hide');
                    if (errorThrown === 'Forbidden') {
                        if (confirm('<?= __('TXT_SESSION_TIMEOUT') ?>')) {
                            location.reload();
                        }
                    }
                }
            });
        });
    })();
</script>
