<style>
    legend {
        font-size: 16px;
    }
    .checkbox {
        padding-left: 20px;
    }
    .checkbox label {
        display: inline-block;
        position: relative;
        padding-left: 5px;
    }
    .checkbox label::before {
        content: "";
        display: inline-block;
        position: absolute;
        width: 17px;
        height: 17px;
        left: 0;
        margin-left: -20px;
        border: 1px solid #cccccc;
        border-radius: 3px;
        background-color: #fff;
        -webkit-transition: border 0.15s ease-in-out, color 0.15s ease-in-out;
        -o-transition: border 0.15s ease-in-out, color 0.15s ease-in-out;
        transition: border 0.15s ease-in-out, color 0.15s ease-in-out;
    }
    .checkbox label::after {
        display: inline-block;
        position: absolute;
        width: 16px;
        height: 16px;
        left: 0;
        top: 0;
        margin-left: -20px;
        padding-left: 3px;
        padding-top: 1px;
        font-size: 11px;
        color: #555555;
    }
    .checkbox input[type="checkbox"] {
        opacity: 0;
    }
    .checkbox input[type="checkbox"]:focus + label::before {
        outline: thin dotted;
        outline: 5px auto -webkit-focus-ring-color;
        outline-offset: -2px;
    }
    .checkbox input[type="checkbox"]:checked + label::after {
        font-family: 'FontAwesome';
        content: "\f00c";
    }
    .checkbox input[type="checkbox"]:disabled + label {
        opacity: 0.65;
    }
    .checkbox input[type="checkbox"]:disabled + label::before {
        background-color: #eeeeee;
        cursor: not-allowed;
    }
    .checkbox.checkbox-circle label::before {
        border-radius: 50%;
    }
    .checkbox.checkbox-inline {
        margin-top: 0;
    }
    .checkbox-primary input[type="checkbox"]:checked + label::before {
        background-color: #428bca;
        border-color: #428bca;
    }
    .checkbox-primary input[type="checkbox"]:checked + label::after {
        color: #fff;
    }
    .checkbox-danger input[type="checkbox"]:checked + label::before {
        background-color: #d9534f;
        border-color: #d9534f;
    }
    .checkbox-danger input[type="checkbox"]:checked + label::after {
        color: #fff;
    }
    .checkbox-info input[type="checkbox"]:checked + label::before {
        background-color: #5bc0de;
        border-color: #5bc0de;
    }
    .checkbox-info input[type="checkbox"]:checked + label::after {
        color: #fff;
    }
    .checkbox-warning input[type="checkbox"]:checked + label::before {
        background-color: #f0ad4e;
        border-color: #f0ad4e;
    }
    .checkbox-warning input[type="checkbox"]:checked + label::after {
        color: #fff;
    }
    .checkbox-success input[type="checkbox"]:checked + label::before {
        background-color: #5cb85c;
        border-color: #5cb85c;
    }
    .checkbox-success input[type="checkbox"]:checked + label::after {
        color: #fff;
    }
    .radio {
        padding-left: 20px;
    }
    .radio label {
        display: inline-block;
        position: relative;
        padding-left: 5px;
    }
    .radio label::before {
        content: "";
        display: inline-block;
        position: absolute;
        width: 17px;
        height: 17px;
        left: 0;
        margin-left: -20px;
        border: 1px solid #cccccc;
        border-radius: 50%;
        background-color: #fff;
        -webkit-transition: border 0.15s ease-in-out;
        -o-transition: border 0.15s ease-in-out;
        transition: border 0.15s ease-in-out;
    }
    .radio label::after {
        display: inline-block;
        position: absolute;
        content: " ";
        width: 11px;
        height: 11px;
        left: 3px;
        top: 3px;
        margin-left: -20px;
        border-radius: 50%;
        background-color: #555555;
        -webkit-transform: scale(0, 0);
        -ms-transform: scale(0, 0);
        -o-transform: scale(0, 0);
        transform: scale(0, 0);
        -webkit-transition: -webkit-transform 0.1s cubic-bezier(0.8, -0.33, 0.2, 1.33);
        -moz-transition: -moz-transform 0.1s cubic-bezier(0.8, -0.33, 0.2, 1.33);
        -o-transition: -o-transform 0.1s cubic-bezier(0.8, -0.33, 0.2, 1.33);
        transition: transform 0.1s cubic-bezier(0.8, -0.33, 0.2, 1.33);
    }
    .radio input[type="radio"] {
        opacity: 0;
    }
    .radio input[type="radio"]:focus + label::before {
        outline: thin dotted;
        outline: 5px auto -webkit-focus-ring-color;
        outline-offset: -2px;
    }
    .radio input[type="radio"]:checked + label::after {
        -webkit-transform: scale(1, 1);
        -ms-transform: scale(1, 1);
        -o-transform: scale(1, 1);
        transform: scale(1, 1);
    }
    .radio input[type="radio"]:disabled + label {
        opacity: 0.65;
    }
    .radio input[type="radio"]:disabled + label::before {
        cursor: not-allowed;
    }
    .radio.radio-inline {
        margin-top: 0;
    }
    .radio-primary input[type="radio"] + label::after {
        background-color: #428bca;
    }
    .radio-primary input[type="radio"]:checked + label::before {
        border-color: #428bca;
    }
    .radio-primary input[type="radio"]:checked + label::after {
        background-color: #428bca;
    }
    .radio-danger input[type="radio"] + label::after {
        background-color: #d9534f;
    }
    .radio-danger input[type="radio"]:checked + label::before {
        border-color: #d9534f;
    }
    .radio-danger input[type="radio"]:checked + label::after {
        background-color: #d9534f;
    }
    .radio-info input[type="radio"] + label::after {
        background-color: #5bc0de;
    }
    .radio-info input[type="radio"]:checked + label::before {
        border-color: #5bc0de;
    }
    .radio-info input[type="radio"]:checked + label::after {
        background-color: #5bc0de;
    }
    .radio-warning input[type="radio"] + label::after {
        background-color: #f0ad4e;
    }
    .radio-warning input[type="radio"]:checked + label::before {
        border-color: #f0ad4e;
    }
    .radio-warning input[type="radio"]:checked + label::after {
        background-color: #f0ad4e;
    }
    .radio-success input[type="radio"] + label::after {
        background-color: #5cb85c;
    }
    .radio-success input[type="radio"]:checked + label::before {
        border-color: #5cb85c; 
    }
    .radio-success input[type="radio"]:checked + label::after {
        background-color: #5cb85c;
    }
    .row-flex {
        display: flex;
        flex-wrap: wrap;
    }
</style>
<div class="container">
    <?php
    echo $this->Form->create(null, [
        'role' => 'form',
        'onsubmit' => 'return false;',
        'name' => 'form_permission',
    ]);
    echo $this->Form->hidden('user_group_id', [
        'value' => $this->request->query('user_group_id'),
    ]);
    ?>
    <div class="row row-flex">
        <?php if ($moduleControls): ?>
        <?php foreach ($moduleControls as $key => $value): ?>
            <?php if ($value->module_actions): ?>
                <div class="col-sm-4 col-md-4">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <div class="checkbox checkbox-primary">
                                <?php
                                $countAction = 0;
                                $countPermission = 0;
                                if ($value->module_actions) {
                                    foreach ($value->module_actions as $key1 => $value1) {
                                        $countAction = count($value1);
                                        $countPermission = count($value1->user_permissions);
                                    }
                                }
                                echo $this->Form->checkbox($value->name, [
                                    'data-id' => $value->id,
                                    'class' => 'check-all',
                                    'id' => 'checkbox-parent-' . $value->id,
                                    'hiddenField' => false,
                                    'checked' => ($countAction == $countPermission) ? true : false,
                                    'disabled' => $disabled,
                                ]);
                                ?>
                                <label for="checkbox-parent-<?= $value->id ?>">
                                    <b><?= implode(' ', preg_split('/(?=[A-Z])/', $this->Comment->singularize($value->name))) ?></b>
                                </label>
                            </div>
                        </div>
                        <div class="panel-body">
                            <?php foreach ($value->module_actions as $key1 => $action): ?>
                                <div class="checkbox checkbox-primary">
                                    <?= $this->Form->checkbox('list[]', [
                                        'data-id' => $action->id,
                                        'hiddenField' => false,
                                        'value' => $action->id,
                                        'id' => 'checkbox-' . $action->id,
                                        'checked' => ($action->user_permissions) ? true : false,
                                        'disabled' => $disabled,
                                    ]) ?>
                                    <label for="checkbox-<?= $action->id ?>">
                                        <?= $this->Comment->textHumanize($action->name) ?>
                                    </label>
                                </div>
                            <?php endforeach; ?>
                        </div>
                    </div>
                </div>
            <?php endif; ?>
        <?php endforeach; ?>
        <?php endif; ?>
    </div>
    <?= $this->Html->link('<i class="fa fa-undo" aria-hidden="true"></i> Go Back', 'javascript:history.back()', [
        'class' => 'btn btn-sm btn-large btn-primary',
        'escape' => false,
    ]) ?>
    <?= $this->Form->button(__('TXT_UPDATE'), [
        'type' => 'button',
        'class' => 'btn btn-sm btn-primary update-permission',
        'disabled' => $disabled,
    ]) ?>
    <?= $this->Form->end() ?>
</div>

<script>
    $(function(e) {
        $('body').on('change', '.check-all', function(e) {
            var checkbox =  $(this).closest('.panel').find('.panel-body :checkbox');
            if ($(this).is(':checked')) {
                $(checkbox).prop('checked', true);
            } else {
                $(checkbox).prop('checked', false);
            }
        });

        $('body').on('change', '.panel-body :checkbox', function(e) {
            if (this.checked === true) {
                return;
            }
            var checkbox =  $(this).closest('.panel').find('.panel-body :checkbox:checked');
            if (checkbox.length == 0) {
                $(this).closest('.panel').find('.panel-heading :checkbox').prop('checked', false);
            }
        });

        $('body').on('click', '.update-permission', function(e) {
            var form  = $('form[name="form_permission"]');
            var options = {
                type: 'POST',
                url: '<?= $this->Url->build('/user-permissions/update/') ?>',
                data: $(form).serialize(),
                beforeSend: function() {
                    $.LoadingOverlay('show');
                }
            };
            ajaxRequest(options, function(data) {
                var data = JSON.parse(data);
                if (data.message === '<?= MSG_SUCCESS ?>') {
                    location.reload();
                } else {
                    $('body').prepend(data);
                    $('body').find('#access-denied').modal('show');
                }
            });
        });

        function ajaxRequest(params, callback)
        {
            $.ajax(params).done(function(data) {
                if (data === null && data === 'undefined') {
                    return false;
                }
                if (typeof callback === 'function') {
                    callback(data);
                }
            }).fail(function(jqXHR, textStatus, errorThrown) {
                if (errorThrown === 'Forbidden') {
                    if (confirm('<?= __('TXT_SESSION_TIMEOUT') ?>')) {
                        location.reload();
                    }
                }
            }).always(function(data) {
                $.LoadingOverlay('hide');
            });
        }
    });
</script>