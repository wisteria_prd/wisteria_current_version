<?php $this->assign('title', 'ファイルアップロード'); ?>
<div class="row">
    <div class="col-xs-12 col-sm-6">
        <div id="product-upload">
            <?php echo $this->Form->create('products', [
                        'url' => [
                            'controller' => 'Products',
                            'action' => 'upload/'
                        ],
                        'type' => 'file',
                        'id' => 'product-csv',
                        'class' => 'dropzone text-center',
                        'style' => ['min-height: 400px'],
                    ]
                );
            ?>
            <div class="upload-title">製品マスタ</div>

            <?php if (isset($last_upload_product_csv)) : ?>
                <div class="upload-title text-center">
                    最終アップロード日：<br/>
                    <?php echo date_format($last_upload_product_csv, 'Y-m-d'); ?> <br/>
                    <?php echo date_format($last_upload_product_csv, 'H:i:s'); ?>
                </div>
            <?php endif; ?>
            <div class="dz-default dz-message"><span>この枠内に製品マスタをCSV形式のファイルを枠内にドロップしてください。
「＋」よりファイルを選択してアップロードすることも可能です。</span></div>
            <div class="fallback">
                <?php echo $this->Form->input('file', [
                            'multiple' => false,
                            'div' => false,
                            'label' => false,
                            'accept' => '.csv, .CSV',
                        ]
                    );
                ?>
            </div>

            <div class="bottom-class">
                <div class="upload-file">
                    <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                </div>
                <div class="previewTemplate" style="display: none;">
                    <a class="remove-file" href="javaScript:void(0);" data-dz-remove>
                        <span class="glyphicon glyphicon-remove-sign" aria-hidden="true"></span>
                    </a>
                </div>
            </div>
            <?php echo $this->Form->end(); ?>
            <div class="submit-csv text-center">
                <div id="submit-product-csv" class="btn btn-primary" disabled="disabled">アップロードする</div>
            </div>
        </div>
    </div>

    <div class="col-xs-12 col-sm-6">
        <div id="sale-upload">
            <?php echo $this->Form->create('sale', [
                        'url' => [
                            'controller' => 'sales',
                            'action' => 'upload/'
                        ],
                        'type' => 'file',
                        'id' => 'sale-csv',
                        'class' => 'dropzone text-center',
                        'style' => ['min-height:400px'],
                    ]
                );
            ?>
            <div class="upload-title">過去データ（売上）</div>

            <?php if (isset($last_upload_sale_csv)) : ?>
                <div class="upload-title text-center">
                    最終アップロード日：<br/>
                    <?php echo date_format($last_upload_sale_csv, 'Y-m-d'); ?> <br/>
                    <?php echo date_format($last_upload_sale_csv, 'H:i:s'); ?>
                </div>
            <?php endif; ?>
            <div class="dz-default dz-message"><span>この枠内に過去データをCSV形式のファイルを枠内にドロップしてください。
「＋」よりファイルを選択してアップロードすることも可能です。</span></div>
            <div class="fallback">
                <?php echo $this->Form->input('file', [
                            'multiple' => false,
                            'div' => false,
                            'label' => false,
                            'accept' => '.csv, .CSV',
                        ]
                    );
                ?>
            </div>

            <div class="bottom-class">
                <div class="upload-file">
                    <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                </div>
                <div class="previewTemplate" style="display: none;">
                    <a class="remove-file" href="javaScript:void(0);" data-dz-remove>
                        <span class="glyphicon glyphicon-remove-sign" aria-hidden="true"></span>
                    </a>
                </div>
            </div>
            <?php echo $this->Form->end(); ?>
            <div class="submit-csv text-center">
                <div id="submit-sale-csv" class="btn btn-primary" disabled="disabled">アップロードする</div>
            </div>
        </div>
    </div>
</div>

<script>
    Dropzone.autoDiscover = false;
    $('document').ready(function() {

        var product_upload = new Dropzone('#product-csv', {
            url: BASE_URL + 'product1s/upload/',
            maxFiles: 1,
            paramName: 'file',
            acceptedFiles: '.csv, .CSV',
            maxFilesize: 100,
            uploadMultiple: false,
            autoProcessQueue:false,
            previewsContainer: '#product-csv .previewTemplate',
            init: function() {
                this.on("success", function(file, data) {
                    product_upload.removeAllFiles();
                    $('#submit-product-csv').attr('disabled', true);
                    var obj = jQuery.parseJSON(data);
                    $.LoadingOverlay('hide', {custom: alert(obj.message)});
                });
                this.on("removedfile", function(file) {
                    $('#product-csv').find('.upload-file').show();
                    $('#product-csv').find('.previewTemplate').hide();
                    $('#submit-product-csv').attr('disabled', true);
                });
                this.on("addedfile", function (file) {
                    $('#product-csv').find('.upload-file').hide();
                    $('#product-csv').find('.previewTemplate').show();
                    $('#product-csv').find('.remove-file').on("click", function() {
                        product_upload.removeAllFiles();
                    });
                    $('#submit-product-csv').removeAttr('disabled', true);
                });
                this.on("error", function(file, data) {
                    product_upload.removeAllFiles();
                    $('#submit-product-csv').attr('disabled', true);
                    if(data.message !== undefined) {
                        $.LoadingOverlay('hide', {custom: alert(data.message)});
                    }
                });
            }
        });

        var sale_upload = new Dropzone('#sale-csv', {
            url: BASE_URL + 'sale1s/upload/',
            maxFiles: 1,
            paramName: 'file',
            acceptedFiles: '.csv, .CSV',
            maxFilesize: 100,
            uploadMultiple: false,
            autoProcessQueue:false,
            previewsContainer: '#sale-csv .previewTemplate',
            init: function(){
                this.on("success", function(file, data) {
                    $('#submit-sale-csv').attr('disabled', true);
                    sale_upload.removeAllFiles();
                    $('#submit-sale-csv').attr('disabled', true);
                    var obj = jQuery.parseJSON(data);
                    $.LoadingOverlay('hide', {custom: alert(obj.message)});
                });
                this.on("removedfile", function(file) {
                    $('#sale-csv').find('.upload-file').show();
                    $('#sale-csv').find('.previewTemplate').hide();
                    $('#submit-sale-csv').attr('disabled', true);
                });
                this.on("addedfile", function (file) {
                    $('#sale-csv').find('.upload-file').hide();
                    $('#sale-csv').find('.previewTemplate').show();
                    $('#sale-csv').find('.remove-file').on("click", function() {
                        sale_upload.removeAllFiles();
                    });
                    $('#submit-sale-csv').removeAttr('disabled', true);
                });
                this.on("error", function(file, data) {
                    $('#submit-sale-csv').attr('disabled', true);
                    $.LoadingOverlay('hide');
                    product_upload.removeAllFiles();
                    if(data.message !== undefined) {
                        $.LoadingOverlay('hide', {custom: alert(data.message)});
                    }
                });
            }
        });

        $('.upload-file').on("click", function() {
            $(this).parent().parent().trigger('click');
        });

        $('#submit-product-csv').on("click", function() {
            if ($(this).attr('disabled') === 'disabled') {
                return;
            }
            product_upload.processQueue();
            $.LoadingOverlay('show');
        });

        $('#submit-sale-csv').on("click", function() {
            if ($(this).attr('disabled') === 'disabled') {
                return;
            }
            sale_upload.processQueue();
            $.LoadingOverlay('show');
        });

    });
</script>
