
<?= $this->Form->create(null, [
    'class' => 'form-inline',
    'role' => 'form',
    'name' => 'form_menu',
    'type' => 'get',
]) ?>
<div class="form-group custom-select">
    <?= $this->Form->select('user_group_id', $userGroups, [
        'class' => 'form-control',
        'id' => 'user-group-id',
        'empty' => __('TXT_SELECT_GROUP_TYPE'),
        'default' => !empty($this->request->query('user_group_id')) ? $this->request->query('user_group_id') : null,
    ]) ?>
</div>

<div class="row" style="padding-top: 2rem;">
    <div class="col-md-5 col-sm-6">
        <div class="panel panel-default">
            <div class="panel-heading">List of menus</div>
            <div class="panel-body">
                <?php if ($menus): ?>
                    <ul class="list-unstyled">
                        <?php foreach ($menus as $key => $value): ?>
                        <li data-id="<?= $value->id ?>" data-name="<?= $value->name ?>" data-name-en="<?= $value->name_en ?>" data-url="<?= $value->url ?>">
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" class="menu-item" name="menu-item">
                                    <?php
                                    echo $value->name . ' | ' . $value->name_en . '';
                                    if (empty($value->url) || ($value->url === null)) {
                                        echo '<span class="error-message">(#)</span>';
                                    }
                                    ?>
                                </label>
                            </div>
                        </li>
                        <?php endforeach; ?>
                    </ul>
                <?php endif; ?>
            </div>
            <div class="panel-footer">
                <form class="form-inline">
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" class="menu-check-all" name="check-all"> Check All
                        </label>
                    </div>&nbsp;
                    <?php echo $this->Form->button('Choose Menu', [
                        'type' => 'button',
                        'class' => 'btn btn-primary btn-sm choose-menu',
                        'disabled' => true,
                    ]); ?>
                </form>
            </div>
        </div>
    </div>
    <div class="col-md-5 col-sm-6" style="position: sticky; top: 60px;">
        <div class="panel panel-default">
            <div class="panel-heading">In Order Menu</div>
            <div class="panel-body">
                <meta name="user-qroup-id" content="<?= $this->request->query('user_group_id'); ?>"/>
                <div class="dd">
                    <ol class="dd-list menu-wrap">
                        <?php
                        if ($nestedMenus) {
                            echo $this->Comment->getMenuList($nestedMenus);
                        }
                        ?>
                    </ol>
                </div>
            </div>
            <div class="panel-footer">
                <?php echo $this->Form->button('Save', [
                    'type' => 'button',
                    'class' => 'btn btn-primary btn-sm ms-save',
                    'disabled' => $nestedMenus ? false : true,
                ]); ?>
            </div>
        </div>
    </div>
</div>

<?php
echo $this->Html->css([
    'jquery.nestable.min',
]);
echo $this->Html->script([
    'jquery.nestable.min'
], ['blog' => 'script']);
?>

<script>
    $(function(e) {
        $('.dd').nestable();
        $('#user-group-id').select2({ width: '100%' });

        $('#user-group-id').change(function(e) {
            var value = $(this).val();
            if ((value === null) || (value === '')) {
                return;
            }
            $('form[name="form_menu"]').submit();
        });

        $('body').on('click', '.add-menu', function(e) {
            var params = {
                target: $(this).attr('data-target'),
                user_group_id: $('#user-group-id').val(),
                menu_id: null,
                menu_control_id: null
            };
            if (params.target === '<?= TARGET_EDIT ?>') {
                params.menu_id = $(this).closest('li').attr('data-menu-id');
                params.menu_control_id = $(this).closest('li').attr('data-menu-control');
            }
            var options = {
                type: 'GET',
                url: '<?= $this->Url->build('/menus/get-form/') ?>',
                dataType: 'html',
                data: params,
                beforeSend: function() {
                    $.LoadingOverlay('show');
                }
            };
            ajaxRequest(options, function(data) {
                $('body').prepend(data);
                $('body').find('.menu-modal').modal('show');
            });
        });

        $('.menu-check-all').click(function(e) {
            if ($(this).prop('checked') == true) {
                $('.menu-item').prop('checked', true);
                $('.choose-menu').attr('disabled', false);
            } else {
                $('.menu-item').prop('checked', false);
                $('.choose-menu').attr('disabled', true);
            }
        });

        $('.menu-item').change(function(e) {
            var inx = $('.menu-item:checked').length;
            if (inx > 0) {
                $('.choose-menu').attr('disabled', false);
                return true;
            }
            $('.choose-menu').attr('disabled', true);
        });

        $('.choose-menu').click(function(e) {
            $('.ms-save').attr('disabled', false);
            var menus = $('.menu-item:checked');
            if (menus.length > 0) {
                $.each(menus, function(index, value) {
                    var obj = {
                        id: $(this).closest('li').attr('data-id'),
                        name: $(this).closest('li').attr('data-name'),
                        name_en: $(this).closest('li').attr('data-name-en'),
                    };
                    var element = '<li class="dd-item" data-menu-id="' + obj.id + '">' +
                            '<div class="dd-handle">' + obj.name + ' | ' + obj.name_en + '</div>' +
                            '<div class="mn-remove mn-icon">' +
                                '<i class="fa fa-trash-o" aria-hidden="true"></i>' +
                            '</div>' +
                        '</li></div>';
                    $('.menu-wrap').append(element);
                });
            }
        });

        $('body').on('click', '.mn-remove', function(e) {
            $(this).closest('li').remove();
            if ($('.menu-wrap li').length == 0) {
                $('.ms-save').attr('disabled', true);
            }
        });

        $('.ms-save').click(function(e) {
            var obj = $('.dd').nestable('serialize');
            var params = {
                type: 'POST',
                url: BASE + 'MenuControls/updateMenu',
                data: {
                    data: obj,
                    user_group_id: $('meta[name="user-qroup-id"]').attr('content')
                }
            };
            ajaxRequest(params, SESSION_TIMEOUT, function(data) {
                if (data.message === MSG_SUCCESS) {
                    location.reload();
                }
            });
        });
    });
</script>
