
<section class="modal-wrapper">
    <div class="modal fade menu-modal" data-backdrop="static" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <?php
                    echo $this->Form->button('<span aria-hidden="true">&times;</span>', [
                        'type' => 'button',
                        'class' => 'close modal-close',
                        'data-dismiss' => 'modal',
                        'aria-label' => 'Close',
                        'escape' => false,
                    ]);
                    ?>
                    <h4 class="modal-title text-center"><?= __('TXT_CREATE') ?></h4>
                </div>
                <div class="modal-body">
                    <?php
                    echo $this->Form->create(null, [
                        'role' => 'form',
                        'onsubmit' => 'return false;',
                        'class' => 'form-horizontal',
                        'name' => 'form_create',
                    ]);
                    if ($menu) {
                        echo $this->Form->hidden('id', [
                            'value' => $menu->id,
                        ]);
                        echo $this->Form->hidden('menu_control_id', [
                            'value' => $menuControlId,
                        ]);
                    }
                    ?>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">
                            <?= __('TXT_NAME_ENGLISH') . $this->Comment->formAsterisk() ?>
                        </label>
                        <div class="col-sm-9">
                            <?= $this->Form->text('name_en', [
                                'class' => 'form-control',
                                'placeholder' => __('TXT_ENTER_NAME_ENGLISH'),
                                'default' => $menu ? $menu->name_en : null,
                            ]) ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">
                            <?= __('TXT_NAME') . $this->Comment->formAsterisk() ?>
                        </label>
                        <div class="col-sm-9">
                            <?= $this->Form->text('name', [
                                'class' => 'form-control',
                                'placeholder' => __('TXT_ENTER_NAME'),
                                'default' => $menu ? $menu->name : null,
                            ]) ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">
                            URL<?= $this->Comment->formAsterisk() ?>
                        </label>
                        <div class="col-sm-9">
                            <?= $this->Form->text('url', [
                                'class' => 'form-control',
                                'placeholder' => __('TXT_ENTER_URL'),
                                'default' => $menu ? $menu->url : null,
                            ]) ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">
                            <?= __('TXT_GROUP_TYPE') . $this->Comment->formAsterisk() ?>
                        </label>
                        <div class="col-sm-9 custom-select">
                            <?= $this->Form->select('user_group_id', $userGroups, [
                                'class' => 'form-control',
                                'id' => 'user-group-id',
                                'empty' => __('TXT_SELECT_GROUP_TYPE'),
                                'value' => $menu ? $menu->menu_controls[0]->user_group_id : $userGroupId,
                                'disabled' => $menu ? true : false,
                            ]) ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label"><?= __('TXT_PARENT') ?></label>
                        <div class="col-sm-9 custom-select">
                            <?= $this->Form->select('parent_id', $menus, [
                                'class' => 'form-control',
                                'id' => 'parent-id',
                                'empty' => __('TXT_DEFAULT_SELECT'),
                                'value' => $menu ? $menu->menu_controls[0]->parent_id : null,
                            ]) ?>
                        </div>
                    </div>
                    <?php echo $this->Form->end(); ?>
                </div>
                <div class="modal-footer">
                    <?php
                    echo $this->Form->button(__('TXT_CANCEL'), [
                        'type' => 'button',
                        'class' => 'btn btn-default btn-sm btn-width modal-close',
                        'data-dismiss' => 'modal',
                        'aria-label' => 'Close',
                    ]);
                    echo $this->Form->button(__('TXT_REGISTER'), [
                        'type' => 'button',
                        'class' => 'btn btn-primary btn-sm btn-width btn-register',
                    ]);
                    ?>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(function(e) {
            $('#parent-id, #user-group-id').select2({ width: '100%' });

            $('.btn-register').click(function(e) {
                var form  = $('form[name="form_create"]');
                var options = {
                    type: 'POST',
                    url: '<?= $this->Url->build('/menus/create-and-update/') ?>',
                    dataType: 'json',
                    data: $(form).serialize(),
                    beforeSend: function() {
                        $.LoadingOverlay('show');
                        $(form).find('.error-message').remove();
                        $(form).find('.form-group div').removeClass('has-error');
                    }
                };
                ajaxRequest(options, function(data) {
                    if (data.message === '<?= MSG_ERROR ?>') {
                        $.each(data.data, function (i, v) {
                            var message = '<label class="error-message">' + v + '</label>';
                            var content = $(form).find('[name="' + i + '"]');
                            $(content).closest('div').addClass('has-error').append(message);
                        });
                    } else {
                        location.reload();
                    }
                });
            });

            function ajaxRequest(params, callback)
            {
                $.ajax(params).done(function(data) {
                    if (data === null && data === 'undefined') {
                        return false;
                    }
                    if (typeof callback === 'function') {
                        callback(data);
                    }
                }).fail(function(jqXHR, textStatus, errorThrown) {
                    if (errorThrown === 'Forbidden') {
                        if (confirm('<?= __('TXT_SESSION_TIMEOUT') ?>')) {
                            location.reload();
                        }
                    }
                }).always(function(data) {
                    $.LoadingOverlay('hide');
                });
            }
        });
    </script>
</section>
