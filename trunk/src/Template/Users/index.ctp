
<?php
$getParams = $this->request->query;
$session = $this->request->session();
$this->assign('title', __('TXT_USER_MANAGEMENT'));
?>
<meta content="<?php echo __('USER_SELECT_ROLE'); ?>" id="txt_select_user_role">
<div class="row">
    <div class="col-xs-12 form-group">
        <?php
        echo $this->Form->create('Users', [
            'type' => 'get',
            'class' => 'form-inline',
            'name' => 'form_search',
        ]); ?>
        <div class="form-group">
            <?php
            echo $this->Form->text('keyword', [
                'class' => 'form-control',
                'id' => 'keyword-search',
                'placeholder' => __('USER_SEARCH_KEYWORD'),
                'default' => isset($getParams['keyword']) ? $getParams['keyword'] : '',
            ]); ?>
        </div>
        <div class="form-group custom-select">
            <?php
            $classify = [];
            if ($userGroups) {
                foreach ($userGroups as $key => $value) {
                    $classify[] = [
                        'value' => $value->id,
                        'text' => $value->name,
                    ];
                }
            }
            echo $this->Form->select('role', $classify, [
                'class' => 'form-control role',
                'empty' => ['' => __('USER_SELECT_ROLE')],
                'style' => 'width: 200px;',
                'default' => isset($getParams['role']) ? $getParams['role'] : '',
            ]); ?>
        </div>
        <?php $this->SwitchStatus->render(); ?>
        <?php echo $this->Form->end(); ?>
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <?php echo $this->element('display_info'); ?>
        <?php
        echo $this->Form->button(__('BTN_REGISTER'), [
            'type' => 'button',
            'class' => 'btn btn-primary pull-right btn-sm btn-width create-user',
            'data-target' => TARGET_NEW,
        ]); ?>
    </div>
</div>
<div class="row">
    <div class="col-xs-12 user-list">
        <?php
        echo $this->element('Users/user_list', [
            'users' => $users,
            'session' => $session,
        ]); ?>
    </div>
</div>
<div class="row row-top-space" style="padding-bottom: 20px;">
    <?php echo $this->element('next_prev', ['column' => 12]) ?>
    <div class="clearfix"></div>
</div>
<?php
echo $this->Html->script([
    'users/user',
], ['blog' => false]);
