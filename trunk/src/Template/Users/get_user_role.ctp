<?php
$options = [];
$txt = '';
if ($userGroups) {
    foreach ($userGroups as $key => $value) {
        switch ($value->role) {
            case USER_ROLE_ADMIN:
                if ($affiliation_class === USER_CLASS_WISTERIA) {
                    $txt = __d('user', 'TXT_W_ADMIN');
                } else {
                    $txt = __d('user', 'TXT_MP_ADMIN');
                }
                break;

            case USER_ROLE_CUSTOMER_SUPPORT:
                $txt = __d('user', 'TXT_W_CUSTOMER_SUPPORT');
                break;

            case USER_ROLE_LOGISTIC:
                $txt = __d('user', 'TXT_MP_LOGISTIC');
                break;

            case USER_ROLE_MANAGER_SALE:
                $txt = __d('user', 'TXT_W_MANAGER');
                break;

            case USER_ROLE_SALE:
                $txt = __d('user', 'TXT_W_SALE');
                break;
        }
        $options[] = [
            'value' => $value->id,
            'text' => $txt,
        ];
    }
}
echo $this->Form->select('user_group_id', $options, [
    'empty' => ['' => __('USER_SELECT_ROLE')],
    'class' => 'form-control user-role',
    'id' => 'role',
]);
