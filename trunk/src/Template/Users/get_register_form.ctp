
<section class="modal-wrapper">
    <div class="modal fade" id='register-modal' data-backdrop="static" role="dialog">
        <div class="modal-dialog" role="document">
            <?php echo $this->element('Users/register_form'); ?>
        </div>
    </div>
    <?php
    echo $this->Html->script([
        'users/user_register',
    ], ['inline' => false]);
    ?>
</section>
