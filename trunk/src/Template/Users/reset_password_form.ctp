<section class="modal-wrapper">
    <div class="modal fade" id='reset-password' data-backdrop="static" role="dialog">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="col-sm-12">
                        <?php
                        echo $this->Form->create(null, [
                            'autocomplete' => 'off',
                            'onsubmit' => 'return false;',
                        ]);
                        ?>
                        <div class="form-group">
                            <?php
                            echo $this->Form->password('old_passowrd', [
                                'placeholder' => '現在のパスワードを入力',
                                'class' => 'form-control',
                            ]);
                            ?>
                        </div>
                        <div class="form-group">
                            <?php
                            echo $this->Form->password('password', [
                                'placeholder' => '新しいパスワードを入力',
                                'class' => 'form-control',
                            ]);
                            ?>
                        </div>
                        <div class="form-group">
                            <?php
                            echo $this->Form->password('confirm_passowrd', [
                                'placeholder' => 'もう一度新しいパスワードを入力',
                                'class' => 'form-control',
                            ]);
                            ?>
                        </div>
                        <?php echo $this->Form->end(); ?>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="modal-footer">
                    <div class="btn-inline">
                        <?php
                        echo $this->Form->button(__('BTN_CANCEL'), [
                            'class' => 'btn btn-default btn-sm modal-close',
                            'type' => 'button',
                            'data-dismiss' => 'modal',
                        ]);
                        ?>
                    </div>
                    <div class="btn-inline">
                        <?php
                        echo $this->Form->button(__('BTN_REGISTER_NEXT'), [
                            'class' => 'btn btn-primary btn-sm btn-reset-pw',
                            'type' => 'button',
                        ]);
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php
    echo $this->Html->script([
        'users/user.js',
    ], ['inline' => false]);
    ?>
</section>
