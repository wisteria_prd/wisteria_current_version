
<section class="modal-wrapper">
    <div class="modal fade modal-view" data-backdrop="static" role="dialog">
        <div class="vertical-alignment">
            <div class="modal-dialog vertical-align" role="document">
                <div class="modal-content">
                    <div class="modal-body">
                        <table class="table table-striped" id="tb-detail">
                            <tbody>
                                <tr>
                                    <td>
                                        <?php
                                        echo __d('user', 'TXT_ORGANIZATION'); ?> :
                                    </td>
                                    <td>
                                        <?php
                                        switch ($user->user_group->affiliation_class) {
                                            case USER_CLASS_WISTERIA:
                                                echo __d('user', 'TXT_WISTERIA');
                                                break;

                                            case USER_CLASS_MEDIPRO:
                                                echo __d('user', 'TXT_MEDIPRO');
                                                break;
                                        }
                                        ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <?php
                                        echo __d('user', 'TXT_USER_ROLE'); ?> :
                                    </td>
                                    <td>
                                        <?php
                                        switch ($user->user_group->role) {
                                            case USER_ROLE_ADMIN:
                                                if ($user->user_group->affiliation_class === USER_CLASS_WISTERIA) {
                                                    echo __d('user', 'TXT_W_ADMIN');
                                                } else {
                                                    echo __d('user', 'TXT_MP_ADMIN');
                                                }
                                                break;

                                            case USER_ROLE_CUSTOMER_SUPPORT:
                                                echo __d('user', 'TXT_W_CUSTOMER_SUPPORT');
                                                break;

                                            case USER_ROLE_LOGISTIC:
                                                echo __d('user', 'TXT_MP_LOGISTIC');
                                                break;

                                            case USER_ROLE_MANAGER_SALE:
                                                echo __d('user', 'TXT_W_MANAGER');
                                                break;

                                            case USER_ROLE_SALE:
                                                echo __d('user', 'TXT_W_SALE');
                                                break;
                                        }
                                        ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <?php
                                        echo __d('user', 'TXT_USER_NAME_JP'); ?> :
                                    </td>
                                    <td>
                                        <?php
                                        echo h($user->lastname) . ' ' . h($user->firstname); ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <?php
                                        echo __d('user', 'TXT_USER_NAME_EN'); ?> :
                                    </td>
                                    <td>
                                        <?php
                                        echo h($user->lastname_en) . ' ' . h($user->firstname_en); ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <?php
                                        echo __d('user', 'TXT_ENTER_EMAIL'); ?> :
                                    </td>
                                    <td>
                                        <?php
                                        echo h($user->email); ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <?php
                                        echo __d('user', 'TXT_ENTER_MOBILE'); ?> :
                                    </td>
                                    <td>
                                        <?php
                                        echo h($user->mobile_phone); ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <?php
                                        echo __d('user', 'TXT_DEPARTMENT'); ?> :
                                    </td>
                                    <td>
                                        <?php
                                        echo h($user->department); ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <?php
                                        echo __d('user', 'TXT_DESIGNATION'); ?> :
                                    </td>
                                    <td>
                                        <?php
                                        echo h($user->position); ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <?php
                                        echo __d('user', 'TXT_STATUS'); ?> :
                                    </td>
                                    <td>
                                        <?php
                                        echo h($user->status); ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <?php
                                        echo __d('user', 'TXT_REMARKS'); ?> :
                                    </td>
                                    <td>
                                        <textarea readonly="readonly" style="background: none;"><?php echo $user->remarks; ?></textarea>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <?php
                                        echo __d('user', 'TXT_MODIFIED'); ?> :
                                    </td>
                                    <td>
                                        <?php
                                        echo date('Y-m-d', strtotime($user->modified)); ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <?php
                                        echo __d('user', 'TXT_REGISTERED'); ?> :
                                    </td>
                                    <td>
                                        <?php
                                        echo date('Y-m-d', strtotime($user->created)); ?>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="modal-footer">
                        <div class="text-center">
                            <?php
                            echo $this->Form->button(__d('user', 'TXT_CANCEL'), [
                                'type' => 'button',
                                'class' => 'btn btn-default md-footer-btn btn-width btn-sm btn-close-modal modal-close',
                                'data-dismiss' => 'modal',
                            ]);
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
