<?php $this->assign('title', __('User Activation')); ?>
<meta name="tc_agreement" content="<?php echo $user->tc_agreement; ?>"/>
<meta name="pp_agreement" content="<?php echo $user->pp_agreement; ?>"/>
<meta name="activated" content="<?php echo $user->activated; ?>"/>
<div class="row">
    <div class="text-center">
        <ul class="login-menu">
            <?php
            if ($group->affiliation_class === USER_CLASS_WISTERIA) :
            ?>
            <li>利用規約とプライバシーポリシーに同意しないと本システムは利用出来ません。ログオフして利用を停止するか、再度ログインして同意してください。</li>
            <li>
                <?php
                echo $this->Html->link(__('ログオフ'), [
                    'controller' => 'users',
                    'action' => 'logout',
                ], [
                    'class' => 'btn btn-sm btn-primary',
                ]);
                ?>
            </li>
            <?php else : ?>
            <li> You may not use this system without agreeing with Terms&Conditions and Privacy Policy. Please log off and stop to use, or log in again and agree with Terms&Conditions and Privacy Policy.</li>
            <li>
                <?php
                echo $this->Html->link(__('Log off'), [
                    'controller' => 'users',
                    'action' => 'logout',
                ], [
                    'class' => 'btn btn-sm btn-primary',
                ]);
                ?>
            </li>
            <?php endif; ?>
        </ul>
    </div>
</div>
<!-- Term and Condition -->
<?php
echo $this->HtmlModal->modalNoHeader('modalTerm', ' modal-lg');
?>
<div class="row">
    <div class="col-md-4 text-center btn-index">
        <button class="btn btn-primary btn-blocked btn-no-action text-center">
            <span>利用規約の</span>
            <div>ご同意</div>
        </button>
    </div>
    <div class="col-md-4 text-center btn-index">
        <button class="btn btn-default termNext btn-blocked text-center">
            <span>プライバシーポリシーの</span>
            <div>ご同意</div>
        </button>
    </div>
    <div class="col-md-4 text-center btn-index">
        <button class="btn btn-default btn-no-action btn-blocked text-center">
            <span>アカウント情報の</span>
            <div>ご確認</div>
        </button>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <hr class="btn-btw-break" />
    </div>
    <div class="col-md-6">
        <hr class="btn-btw-break" />
    </div>
</div>
<div class="top-space"></div>
<?php
echo $this->Form->create(null, [
    'novalidate' => true,
    'autocomplete' => 'off',
    'id' => 'form-user-tc-agreement'
]);
?>
<div class="row">
    <div class="col-md-10 col-md-offset-1">
        <div class="wrapper-term">
            <p class="text-center"><u>利用規約</u></p>
            <div class="wrapper-info">
                <?php echo $this->element('term_condition'); ?>
            </div>
            <div class="row">
                <div class="col-md-3 col-md-offset-6">
                    <div class="checkbox">
                        <label>
                            <?php
                            echo $this->Form->checkbox('tc_agreement', [
                                'hiddenField' => false,
                                'id' => 'checkTerm',
                            ]);
                            ?> 同意する
                        </label>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 text-right">
                    <?php
                    echo $this->Form->button(__('キャンセル'), [
                        'class' => 'btn btn-default',
                        'type' => 'button',
                        'data-dismiss' => 'modal',
                        'id' => 'cancelTerm',
                    ]);
                    ?>
                </div>
                <div class="col-md-6 text-left">
                    <?php
                    echo $this->Form->button(__('同意して次へ'), [
                        'class' => 'btn btn-primary termNext',
                        'type' => 'button',
                    ]);
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php echo $this->Form->end(); ?>
<?= $this->HtmlModal->modalFooter(); ?>

<!-- Privacy Policy -->
<?php
echo $this->HtmlModal->modalNoHeader('modalPrivacy', ' modal-lg');
?>
<div class="row">
    <div class="col-md-4 text-center btn-index">
        <button class="btn btn-default btn-step-back btn-blocked text-center">
            <span>利用規約の</span>
            <div>ご同意</div>
        </button>
    </div>
    <div class="col-md-4 text-center btn-index">
        <button class="btn btn-primary btn-no-action btn-blocked text-center">
            <span>プライバシーポリシーの</span>
            <div>ご同意</div>
        </button>
    </div>
    <div class="col-md-4 text-center btn-index">
        <button class="btn btn-default btn-next-step btn-blocked text-center">
            <span>アカウント情報の</span>
            <div>ご確認</div>
        </button>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <hr class="btn-btw-line" />
    </div>
    <div class="col-md-6">
        <hr class="btn-btw-break" />
    </div>
</div>
<div class="top-space"></div>
<?php
echo $this->Form->create(null, [
    'novalidate' => true,
    'autocomplete' => 'off',
    'id' => 'form-user-pp-agreement'
]);
?>
<div class="row">
    <div class="col-md-10 col-md-offset-1">
        <div class="wrapper-term">
            <p class="text-center"><u>プライバシーポリシー</u></p>
            <div class="wrapper-info">
                <?php echo $this->element('privacy'); ?>
            </div>
            <div class="row">
                <div class="col-md-3 col-md-offset-8">
                    <div class="checkbox">
                        <label>
                            <?php
                            echo $this->Form->checkbox('pp_agreement', [
                                'hiddenField' => false,
                                'id' => 'checkPolicy',
                            ]);
                            ?> 同意する
                        </label>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4 text-right">
                    <?php
                    echo $this->Form->button(__('戻る'), [
                        'class' => 'btn btn-primary',
                        'type' => 'button',
                        'data-dismiss' => 'modal',
                        'id' => 'backPolicy',
                    ]);
                    ?>
                </div>
                <div class="col-md-4 text-center">
                    <?php
                    echo $this->Form->button(__('キャンセル'), [
                        'class' => 'btn btn-default',
                        'type' => 'button',
                        'data-dismiss' => 'modal',
                        'id' => 'cancelPolicy',
                    ]);
                    ?>
                </div>
                <div class="col-md-4 text-left">
                    <?php
                    echo $this->Form->button(__('同意して次へ'), [
                        'class' => 'btn btn-primary',
                        'type' => 'button',
                        'id' => 'policyNext',
                    ]);
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php echo $this->Form->end(); ?>
<?php echo $this->HtmlModal->modalFooter(); ?>

<!-- Account Confirmation -->
<?php echo $this->HtmlModal->modalNoHeader('modalConfirmation', ' modal-lg'); ?>
<div class="row">
    <div class="col-md-4 text-center btn-index">
        <button class="btn btn-default btn-blocked text-center">
            <span>利用規約の</span>
            <div>ご同意</div>
        </button>
    </div>
    <div class="col-md-4 text-center btn-index">
        <button class="btn btn-default backAccount btn-blocked text-center">
            <span>プライバシーポリシーの</span>
            <div>ご同意</div>
        </button>
    </div>
    <div class="col-md-4 text-center btn-index">
        <button class="btn btn-primary btn-no-action btn-blocked text-center">
            <span>アカウント情報の</span>
            <div>ご確認</div>
        </button>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <hr class="btn-btw-line" />
    </div>
    <div class="col-md-6">
        <hr class="btn-btw-line" />
    </div>
</div>
<div class="top-space"></div>
<div class="row">
    <div class="col-md-10 col-md-offset-1 form-h-scroll">
        <div class="wrapper-term">
            <p class="text-center"><u>アカウント情報</u></p>
            <div id="accountInfo">
                <?php
                $saveAgreement = '';
                if (isset($user) && !empty($user)) {
                    if (($group->affiliation_class == USER_CLASS_MEDIPRO)
                            || ($group->affiliation_class == USER_CLASS_WISTERIA))
                    {
                        $saveAgreement = $this->Url->build([
                            'controller' => 'users',
                            'action' => 'finalAgreement',
                        ]);
                        echo $this->element('Form/medi_pro_form');
                    }
                }
                ?>
            </div>
            <div class="row">
                <div class="col-md-4 text-right">
                    <?php
                    echo $this->Form->button(__('戻る'), [
                        'class' => 'btn btn-primary backAccount',
                        'type' => 'button',
                        'data-dismiss' => 'modal',
                    ]);
                    ?>
                </div>
                <div class="col-md-4 text-center">
                    <?php
                    echo $this->Form->button(__('キャンセル'), [
                        'class' => 'btn btn-default',
                        'type' => 'button',
                        'data-dismiss' => 'modal',
                        'id' => 'cancelAccount',
                    ]);
                    ?>
                </div>
                <div class="col-md-4 text-left">
                    <?php
                    echo $this->Form->button(__('完了'), [
                        'class' => 'btn btn-primary btn-finished-info',
                        'type' => 'button',
                        'rel' => 'popover',
                        'id' => 'accountNext',
                    ]);
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php echo $this->HtmlModal->modalFooter(); ?>
<?php
echo $this->Html->script([
    'users/activate_user_account',
], ['blog' => false]);