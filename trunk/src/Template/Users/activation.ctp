<?php
$form_text = '';
$title = '';
if ($type == 'activate') {
    $form_text = __('ご登録頂きまして、有り難うございます。パスワードを登録して、ログインください。');
    $title = __('User Activation');
} elseif ($type == 'reset_password') {
    $title = __('User Reset Password');
    $form_text = __('パスワードをリセットしました。新しいパスワードを登録して、ログインください。');
}
?>
<?php $this->assign('title', $title); ?>
<div class="row">
    <div class="text-center">
        <?php echo $this->element('login_menu'); ?>
    </div>
</div>
<div class="row loading hide-loading">
    <div class="col-md-4 col-md-offset-4 text-center">
        <i class="fa fa-spinner fa-pulse fa-1x fa-fw margin-bottom"></i>
        <?php echo __('Processing') ?>
    </div>
</div>
<div class="row row-top-space">
    <div class="col-md-4 col-md-offset-4">
        <p><?php echo $form_text ?></p>
        <?php
        echo $this->Form->create($user, [
            'class' => 'form-horizontal',
            'autocomplete' => 'off',
        ]);
        echo $this->Form->hidden('type', [
            'value' => $type,
        ]);
        echo $this->Form->hidden('token', [
            'value' => $token,
        ]);
        $this->Form->templates([
            'inputContainerError' => '{{content}}<span class="help-block">{{error}}</span>',
        ]);
        ?>
        <div class="form-group <?php echo isset($user->errors()['password_confirm']) ? 'has-error' : ''; ?>">
            <div class="col-md-12">
                <?php
                echo $this->Form->input('password', [
                    'type' => 'password',
                    'label' => false,
                    'class' => 'form-control',
                    'placeholder' => 'パスワードを入力',
                ]);
                ?>
            </div>
        </div>
        <div class="form-group <?php echo isset($user->errors()['password_confirm']) ? 'has-error' : ''; ?>">
            <div class="col-md-12">
                <?php
                echo $this->Form->input('password_confirm', [
                    'type' => 'password',
                    'label' => false,
                    'class' => 'form-control',
                    'placeholder' => 'もう一度パスワードを入力',
                ]);
                ?>
            </div>
        </div>
        <div class="form-group">
            <div class="col-md-12">
                <?php
                echo $this->Form->button('パスワードを登録してログイン', [
                    'class' => 'btn btn-primary btn-block',
                ]);
                ?>
            </div>
        </div>
        <?php echo $this->Form->end() ?>
    </div>
</div>