<?php $this->assign('title', __('セールスレポートシステム')); ?>
<meta name="msg" content="<?php echo __('本当にパスワードをリセットしますか？') ?>"/>
<meta name="btn_cancel" content="<?php echo __('キャンセル') ?>"/>
<meta name="btn_ok" content="<?php echo __('はい') ?>"/>

<div class="row">
    <div class="text-center">
        <?php echo $this->element('login_menu') ?>
    </div>
</div>
<div class="row loading hide-loading">
    <div class="col-md-4 col-md-offset-4 text-center">
        <i class="fa fa-spinner fa-pulse fa-1x fa-fw margin-bottom"></i>
        <?php echo __('Processing') ?>
    </div>
</div>
<div class="row row-top-space">
    <div class="col-md-4 col-md-offset-4">
        <?php
        echo $this->Flash->render();
        echo $this->Form->create(null,[
            'autocomplete' => 'off',
            'class' => 'form-user-login',
        ]);
        ?>
        <div class="form-group">
            <?php
            echo $this->Form->text('email', [
                'class' => 'form-control',
                'placeholder' => __('メールアドレス'),
                'autofocus' => 'autofocus',
            ]);
            ?>
        </div>
        <div class="form-group">
            <?php
            echo $this->Form->password('password', [
                'class' => 'form-control login-password',
                'placeholder' => __('パスワード'),
            ]);
            ?>
            <span class="help-block help-tips">
            </span>
        </div>
        <div class="form-group">
            <?php
            echo $this->Form->button(__('ログイン'), [
                'class' => 'btn btn-primary btn-block submit-login',
            ]);
            ?>
        </div>
        <?php echo $this->Form->end(); ?>
    </div>
</div>
<div class="row row-top-space">
    <div class="col-md-4 col-md-offset-4 text-center">
        <?php
        echo $this->Html->link('<strong>' . __('パスワード忘れはこちら') . '</strong>', '#', [
            'class' => 'show-forget-password',
            'escape' => false,
        ]);
        ?>
    </div>
</div>

<!-- modal reset password -->
<?= $this->HtmlModal->modalHeader('modalResetPassword', __('Reset forget password'), ' modal-sm') ?>
<div class="row load hide-loading">
    <div class="col-md-12 text-center">
        <i class="fa fa-spinner fa-pulse fa-1x fa-fw margin-bottom"></i>
        <?php echo __('Processing') ?>
    </div>
</div>
<div class="row row-top-space" id="reset-password">
    <div class="col-md-12">
        <?php
        echo $this->Form->create('User',[
            'autocomplete' => 'off',
            'class' => 'form-user-reset-password',
            'onsubmit' => 'return false'
        ]);
        ?>
        <div class="form-group">
            <?php
            echo $this->Form->email('email', [
                'class' => 'form-control reset-mail',
                'placeholder' => __('メールアドレス'),
                'autofocus' => 'autofocus',
            ]);
            ?>
        </div>
        <div class="form-group">
            <?php
            echo $this->Form->button(__('パスワードをリセットします'), [
                'type' => 'button',
                'class' => 'btn btn-primary btn-block submit-reset-password',
                'id' => 'submit-reset-password',
            ]);
            ?>
        </div>
        <div class="form-group">
            <span class="help-block help-tips" id="error_email"></span>
        </div>
        <?php echo $this->Form->end(); ?>
    </div>
</div>
<?= $this->HtmlModal->modalFooter(); ?>
<!-- /end modal reset password forget -->

<?php
echo $this->Html->script([
    'users/user_login',
], ['blog' => false]);