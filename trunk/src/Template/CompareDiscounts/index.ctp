
<div class="search-section" style="margin-bottom: 20px;">
    <?php
    echo $this->Form->create('CompareDiscount', [
        'role' => 'form',
        'class' => 'form-inline form-search',
        'type' => 'get',
        'name' => 'form_search'
    ]);
    echo $this->Form->input('product_name', [
        'name' => 'keyword',
        'placeholder' => 'Product Name',
        'class' => 'form-control',
        'label' => false,
        'value' => $this->request->query('keyword'),
        'templates' => [
            'inputContainer' => '<div class"form-group">{{content}}</div>',
        ]
    ]);
    echo $this->Form->submit('submit', [
        'style' => 'display: none',
    ]);
    echo $this->Form->end();
    ?>
</div>
<?php if (isset($data)): ?>
<div class="total-pagination" style="padding-bottom: 20px;">
    <section>
        <?php
        $display_item = '';
        $paginate_count = $this->Paginator->counter('{{count}}');
        if ($paginate_count < 10) {
            $display_item = '1~' . $paginate_count;
        } else {
            $display_item = $this->request->query('displays') ? $this->request->query('displays') : 10 . '~' . $paginate_count;
        }
        ?>
        <?= __('Display Item: ') . $display_item ?> (<?= __('Total: ') . $paginate_count ?>)
    </section>
</div>
<div class="row search-section" style="margin-bottom: 20px;">
    <?php echo $this->Form->input('display_data', [
        'class' => 'form-control display-data',
        'name' => 'display',
        'id' => false,
        'label' => false,
        'value' => $this->request->query('displays') ? $this->request->query('displays') : 10,
        'options' => [
            '10' => 'Display: 10',
            '20' => 'Display: 20',
            '50' => 'Display: 50',
            '100' => 'Display: 100',
        ],
        'templates' => [
            'inputContainer' => '<div class="col-md-3 pull-left">{{content}}</div>',
        ]
    ]); ?>
    <div class="clearfix"></div>
</div>
<div class="data-tb-list">
    <table class="table table-striped">
        <thead>
            <tr>
                <th>#</th>
                <th class="white-space">
                    <?php echo __('Seller'); ?>
                </th>
                <th class="white-space">
                    <?php echo __('Product Name'); ?>
                </th>
                <th class="white-space">
                    <?php echo __('Details'); ?>
                </th>
                <th class="white-space">
                    <?php echo __('Unit Price'); ?>
                </th>
                <th class="white-space">
                    <?php echo __('Currency'); ?>
                </th>
                <th class="white-space">
                    <?php echo __('FOC'); ?>
                </th>
                <th class="white-space">
                    <?php echo __('MOQ'); ?>
                </th>
                <th class="white-space">
                    <?php echo __('MOA'); ?>
                </th>
                <th class="white-space"></th>
            </tr>
        </thead>
        <tbody>
            <?php
            //pr($this->request->params);
            $page = $this->request->params['paging']['SellerProducts']['page'];
            $limit = $this->request->params['paging']['SellerProducts']['perPage'];
            foreach ($data as $key => $value) :
            ?>
            <tr data-product-id="<?php echo h($value['product']['id']); ?>">
                <th scope="row"><?php echo $key + 1; ?></th>
                <td><?php
                // Seller display Name
                if (!empty($value['seller'])) {
                    if (isset($value['seller']['supplier']) && !empty($value['seller']['supplier'])) {
                        $seller = $value['seller']['supplier'];
                    } else {
                        $seller = $value['seller']['manufacturer'];
                    }
                    echo ($this->Comment->getFieldName() === 'name') ? h($seller['name']) : h($seller['name_en']);
                }
                ?></td>
                <td><?php echo ($this->Comment->getFieldName() === 'name') ? h($value['product']['name']) : h($value['product']['name_en']); ?></td>
                <td></td>
                <td><?php echo number_format($value['seller_standard_prices'][0]['purchase_standard_price']); ?></td>
                <td><?php echo $value['currency']['name']; ?></td>
                <td><?php //echo $value['']; ?></td>
                <td><?php echo $value['purchase_conditions'][0]['is_moq']; ?></td>
                <td><?php echo $value['purchase_conditions'][0]['is_moa']; ?></td>
                <td data-target="">

                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>
<div class="row row-top-space">
    <?php echo $this->Form->input('display_data', [
        'class' => 'form-control display-data',
        'name' => 'display',
        'id' => false,
        'label' => false,
        'value' => $this->request->query('displays') ? $this->request->query('displays') : 10,
        'options' => [
            '10' => 'Display: 10',
            '20' => 'Display: 20',
            '50' => 'Display: 50',
            '100' => 'Display: 100',
        ],
        'templates' => [
            'inputContainer' => '<div class="col-md-3">{{content}}</div>',
        ]
    ]); ?>
    <?= $this->element('next_prev') ?>
    <div class="clearfix"></div>
</div>
<?php endif; ?>

<!--Modal Detail-->
<?php echo $this->element('Modal/view_detail'); ?>

<!--Modal Comment-->
<?php echo $this->element('/Modal/message') ?>

<!--Modal delete-->
<?php echo $this->element('Modal/delete'); ?>

<!--Modal Suspend-->
<?php echo $this->element('Modal/suspend'); ?>

<script>
    $(function(e) {
        $('.btn_comment').click(function (e) {
            e.stopPropagation();
            let params = {
                id: $(this).closest('tr').attr('data-id'),
                type: PERSON_IN_CHARGE_TYPE,
                referer: '<?php echo $this->request->controller; ?>'
            };
            let options = {
                url: BASE + 'Messages/getMessageList',
                type: 'GET',
                data: params,
                beforeSend: function () {
                    $.LoadingOverlay('show');
                }
            };
            ajaxRequest(options, SESSION_TIMEOUT, function (data) {
                $('body').prepend(data);
                $('body').find('.modal-message').modal('show');
            });
        });

        $('#product-name').autocomplete({
            source: function(request, response) {
                $.ajax({
                    url: '<?= $this->Url->build(['controller' => 'products', 'action' => 'loadName']) ?>',
                    type: 'GET',
                    dataType: 'JSON',
                    data: {keyword: request.term},
                    success: function(result) {
                        response($.map(result, function(value) {
                            return {
                                label: value.name,
                                value: value.name
                            };
                        }));
                    }
                });
            },
            minLength: 2,
            open: function(event, ui) {
                $(this).autocomplete('widget').css({
                    'width': 350
                });
            },
            select: function(event, ui) {
                $('#keyword').val(ui.item.value);
                $('#form-search').submit();
            }
        });

        //filter message list search to specific
        $('body').on('click', '.btn-to-individual', function() {
            $(this).removeClass('btn-detault').addClass('btn-primary');
            $('body').find('.btn-to-all').removeClass('btn-primary').addClass('btn-default');
            var toAll = $('.to-individual').val();
            if (parseInt(toAll) === 0) {
                $('.to-all').val(0);
                $('.to-individual').val(1);
            } else {
                $('.to-all').val(1);
                $('.to-individual').val(0);
            }
            messagesSearch(listUrlSearch);
        });

        $('body').on('click', '.btn-search', function() {
            messagesSearch(listUrlSearch);
        });

        $('body').on('click', '.btn-suspend-msg', function() {
            var id = $(this).data('id');
            var data = $('#form-message-status-' + id).serialize();
            var suspendUrl = '<?= $this->Url->build(['controller' => 'Messages', 'action' => 'suspend']) ?>';
            suspendMessage(id, suspendUrl, data);
        });

        GetDataList(optionData);
        typehead_initialize('#keyword', optionData);

        $('body').on('click', '.table td a, .table td select', function(e) {
            e.stopPropagation();
        });

        $('body').on('click', '#btn_delete', function (e) {
            e.stopPropagation();
            var content = $('#modal_delete');
            $(content).find('#btn_delete_yes').data('id', $(this).closest('tr').data('id'));
            $(content).modal('show');
        });

        $('body').on('click', '#btn_delete_yes', function (e) {
            $.post('<?php echo $this->Url->build('/medical-corporations/delete/'); ?>', {id: $(this).data('id')}, function (data) {
                if (data.message === 'success') {
                    window.location.href = '<?php echo $this->Url->build('/medical-corporations/index/'); ?>';
                }
            }, 'json');
        });

        $('body').on('click', '.btn_suspend', function (e) {
            e.stopPropagation();
            var content = $('#modal_suspend');
            var txt = '';
            if ($(this).data('name') === 'recover') {
                txt = '<?= __('Are you sure to recover the Medical Corporation?') ?>';
            } else {
                txt = '<?= __('Are you sure to suspend the Medical Corporation?') ?>';
            }

            $(content).find('#btn_suspend_yes').data('id', $(this).closest('tr').data('id'));
            $(content).find('#btn_suspend_yes').data('target', $(this).closest('td').data('target'));
            $(content).find('.modal-body p').html(txt);
            $(content).modal('show');
        });

        $('body').on('click', '#btn_suspend_yes', function (e) {
            var params = {
                id: $(this).data('id'),
                is_suspend: $(this).data('target')
            };
            $.post('<?php echo $this->Url->build('/medical-corporations/update-suspend/'); ?>', params, function (data) {
                if (data.message === 'success') {
                    window.location.href = '<?php echo $this->Url->build('/medical-corporations/index/'); ?>'
                }
            }, 'json');
        });

        $('body').on('click', '.tt-selectable', function (e) {
            $('form[name="form_search"]').submit();
        });

        function GetDataList(optionData) {
            $.ajax({
                url: '<?php echo $this->Url->build('/medical-corporations/get-list'); ?>',
                type: 'get',
                dataType: 'json',
                contentType: 'application/json; charset=utf-8'
            }).done(function (data) {
                var response = data.data;
                if (response != null && response != undefined) {
                    $.each(response, function (i, v) {
                        optionData.push(v.name);
                        optionData.push(v.name_en);
                    });
                }
            });
        }

        var maxPage = parseInt($('#maxPage').val());
        updatePagination(maxPage);
    });
</script>
