
<div class="search-section" style="margin-bottom: 20px;">
    <?php
    echo $this->Form->create('Customers', [
        'role' => 'form',
        'class' => 'form-inline form-search',
        'type' => 'get',
        'name' => 'form_search'
    ]);
    echo $this->Form->hidden('displays', ['value' => $displays]);
    echo $this->Form->input('keyword', [
        'name' => 'keyword',
        'placeholder' => __('USER_SEARCH_KEYWORD'),
        'class' => 'form-control',
        'label' => false,
        'value' => $this->request->query('keyword') ? $this->request->query('keyword') : null,
        'autocomplete' => 'off',
        'templates' => [
            'inputContainer' => '<div class="form-group">{{content}}</div>',
        ]
    ]);
    echo $this->SwitchStatus->render();
    echo $this->Form->end();
    ?>
</div>
<div class="total-pagination" style="padding-bottom: 20px;">
    <?php echo $this->element('display_info'); ?>
</div>
<div class="row search-section" style="margin-bottom: 20px;">
    <?php echo $this->element('display_number'); ?>
    <?= $this->ActionButtons->btnRegisterNew('Customers', []) ?>
    <div class="clearfix"></div>
</div>
<div class="data-tb-list">
    <?php if ($customer): ?>
    <table class="table table-striped">
        <thead>
            <tr>
                <th>#</th>
                <th>&nbsp;</th>
                <th class="white-space">
                    <?php
                     echo $this->Paginator->sort('Customers.type', __('TXT_USER_STATUS')); ?>
                    <i class="fa fa-sort-desc" aria-hidden="true"></i>
                </th>
                <th class="white-space">
                    <?php
                     echo $this->Paginator->sort('Customers.code', __('TXT_OLD_CODE')); ?>
                    <i class="fa fa-sort-desc" aria-hidden="true"></i>
                </th>
                <th class="white-space">
                    <?php
                    $cs_field = 'Customers.' . $this->Comment->getFieldName();
                    echo $this->Paginator->sort($cs_field, __('TXT_CLIENT')); ?>
                    <i class="fa fa-sort-desc" aria-hidden="true"></i>
                </th>
                <th class="white-space">
                    <?php
                    echo $this->Paginator->sort('reciever_name', __('TXT_W_SALE')); ?>
                    <i class="fa fa-sort-desc" aria-hidden="true"></i>
                </th>
                <th class="white-space">
                    <?php
                    echo $this->Paginator->sort('payment_category_id', __('STR_PAYMENT_CAT')); ?>
                    <i class="fa fa-sort-desc" aria-hidden="true"></i>
                </th>
                <th class="white-space">
                    <?php
                    echo $this->Paginator->sort('invoice_delivery_id', __('STR_INVOICE_DELIVERY')); ?>
                    <i class="fa fa-sort-desc" aria-hidden="true"></i>
                </th>
                <th class="white-space">
                    <?php
                    echo __('TXT_ADDRESS') ?>
                </th>
                <th class="white-space">
                    <?php
                    echo __('TXT_URL') ?>
                </th>
                <th class="white-space">
                    <?php
                    echo __('TXT_DOCTORS') ?>
                </th>
                <th class="white-space">
                    <?php
                    echo __('TBL_PERSON_IN_CHARGE') ?>
                </th>
                <th colspan="3">&nbsp;</th>
            </tr>
        </thead>
        <tbody>
            <?php
            $numbering = $this->Paginator->counter('{{start}}');
            foreach ($customer as $item):
            $total = !empty($item->clinic_doctors) ? $item->clinic_doctors[0]->count_doctor : 0;
            $doctor_id = !empty($item->clinic_doctors) ? $item->clinic_doctors[0]->doctor_id : 0;
            ?>
            <tr data-id="<?php echo h($item->id); ?>">
                <th scope="row">
                    <?php
                    echo $numbering; $numbering++; ?>
                </th>
                <td>
                    <?php
                    echo $this->ActionButtons->disabledText($item->is_suspend) ?>
                </td>
                <td>
                    <?php
                    echo h($item->type); ?>
                </td>
                <td>
                    <?php
                    echo h($item->code); ?>
                </td>
                <td>
                    <?php
                    echo ($this->Comment->getFieldName() === 'name') ? h($item->name) : h($item->name_en); ?>
                </td>
                <td>
                    <?php
                    echo $item->reciever_name; ?>
                </td>
                <td>
                    <?php
                    echo $this->Comment->getFieldByLocal($item->payment_category, $locale) ?>
                </td>
                <td>
                    <?php
                    echo $this->Comment->getFieldByLocal($item->invoice_delivery_method, $locale) ?>
                </td>
                <td class="white-space">
                    <?php
                    if ($locale === '_en') {
                        echo $item->prefecture_en . ' ' . $item->city_en;
                    } else {
                        echo $item->prefecture . ' ' . $item->city;
                    }
                    ?>
                </td>
                <td>
                    <?php
                    if ($item->url) {
                        echo $this->Html->link(__('TXT_WEBSITE'), $item->url, [
                            'class' => 'btn btn-primary btn-sm btn-width',
                            'target' => '_blank',
                        ]);
                    } else {
                        echo $this->Form->button(__('TXT_WEBSITE'), [
                            'class' => 'btn btn-primary btn-sm btn-width btn-disabled',
                            'value' => 'Website',
                            'disabled' => 'disabled',
                        ]);
                    }
                    ?>
                </td>
                <td>
                    <?php
                    $params = [$doctor_id, $total, TYPE_CUSTOMER, 'Doctors', TYPE_DOCTOR, 'btn btn-default btn-sm'];
                    echo $this->Comment->btnCommonCreateList($params); ?>
                </td>
                <td>
                    <?php
                    echo $this->Comment->btnPersonInCharge($item->id, count($item->person_in_charges), TYPE_CUSTOMER); ?>
                </td>
                <td>
                    <?php
                    echo $this->Form->button('<i class="fa fa-comments-o" aria-hidden="true"></i>', [
                        'class' => 'btn btn-primary btn-sm btn_comment',
                        'data-id' => $item->id,
                        'escape' => false,
                    ]); ?>
                </td>
                <td data-target="<?php echo ($item->is_suspend) ? 1 : 0; ?>">
                    <?php
                    if ($item->is_suspend == 1) {
                        echo $this->Form->button(BTN_ICON_UNDO, [
                            'class' => 'btn btn-recover btn-sm btn_suspend',
                            'data-name' => 'recover',
                            'escape' => false,
                        ]);
                    } else {
                        echo $this->Html->link(BTN_ICON_EDIT, [
                            'controller' => 'customers',
                            'action' => 'edit', $item->id
                            ], [
                            'class' => 'btn btn-primary btn-sm',
                            'escape' => false,
                        ]);
                    }
                    ?>
                </td>
                <td data-target="<?php echo ($item->is_suspend) ? 1 : 0; ?>">
                    <?php
                    if ($item->is_suspend == 1) {
                        if (!empty($item->product_brands)) {
                            echo $this->Form->button(BTN_ICON_DELETE, [
                                'class' => 'btn btn-delete btn-sm',
                                'id' => 'btn_delete',
                                'disabled' => 'disabled',
                                'escape' => false
                            ]);
                        } else {
                            echo $this->Form->button(BTN_ICON_DELETE, [
                                'class' => 'btn btn-delete btn-sm',
                                'id' => 'btn_delete',
                                'data-name' => 'delete',
                                'escape' => false
                            ]);
                        }
                    } else {
                        echo $this->Form->button(BTN_ICON_STOP, [
                            'class' => 'btn btn-suspend btn-sm btn_suspend',
                            'data-name' => 'suspend',
                            'escape' => false
                        ]);
                    }
                    ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <?php endif; ?>
</div>
<div class="row row-top-space">
    <?php echo $this->element('display_number'); ?>
    <?= $this->element('next_prev') ?>
    <div class="clearfix"></div>
</div>

<?php echo $this->element('Modal/delete'); ?>
<!--Modal Suspend-->
<?php echo $this->element('Modal/suspend'); ?>

<?php
echo $this->Html->script([
    'customer',
], ['blog' => false]);
?>

<script>
    (function(e) {
        //auto complete
        $('body').find('#keyword').typeahead({
            minLength: 0,
            autoSelect: true,
            source:  function (query, process) {
                return $.get('<?= $this->Url->build(['action' => 'getCustomerBySearch']) ?>', {
                        keyword: query
                    }, function (data) {
                        var optionData = [];
                        if (data.data !== null && data.data !== 'undefined') {
                            $.each(data.data, function(i, v) {
                                if (v.name !== '') {
                                    optionData.push(v.name);
                                }
                                if (v.name_en !== '') {
                                    optionData.push(v.name_en);
                                }
                            });
                        }
                        return process(optionData);
                    }, 'json');
            }
        });

        $('body').on('click', '.typeahead li', function(e) {
            $('body').find('.form-search').submit();
        });

        //filter message list search to specific
        $('body').on('click', '.btn-to-individual', function() {
            $(this).removeClass('btn-detault').addClass('btn-primary');
            $('body').find('.btn-to-all').removeClass('btn-primary').addClass('btn-default');
            var toAll = $('.to-individual').val();
            if (parseInt(toAll) === 0) {
                $('.to-all').val(0);
                $('.to-individual').val(1);
            } else {
                $('.to-all').val(1);
                $('.to-individual').val(0);
            }
            messagesSearch(listUrlSearch);
        });

        $('body').on('click', '.btn-search', function() {
            messagesSearch(listUrlSearch);
        });

        $('body').on('click', '.btn-suspend-msg', function() {
            var id = $(this).data('id');
            var data = $('#form-message-status-' + id).serialize();
            var suspendUrl = '<?= $this->Url->build(['controller' => 'Messages', 'action' => 'suspend']) ?>';
            suspendMessage(id, suspendUrl, data);
        });

        $('body .data-tb-list .table > tbody > tr').on('click', '#btn_delete', function (e) {
            e.preventDefault();
            e.stopPropagation();
            var content = $('#modal_delete');
            $(content).find('#btn_delete_yes').data('id', $(this).closest('tr').data('id'));
            $(content).modal('show');
        });

        $('body').on('click', '#btn_delete_yes', function (e) {
            $.post('<?php echo $this->Url->build('/customers/delete/'); ?>', {id: $(this).data('id')}, function (data) {
                if (data.message === 'success') {
                    location.reload();
                }
            }, 'json');
        });

        $('body .data-tb-list .table > tbody > tr').on('click', '.btn_suspend', function (e) {
            e.stopPropagation();
            var content = $('#modal_suspend');
            var status = $(this).attr('data-name');
            var txt = '';

            switch (status) {
                case 'suspend':
                    $('#btn_suspend_yes').text('<?= __('BTN_SUSPEND') ?>');
                    txt = '<?= __('MSG_CONFIRM_SUSPEND') ?>';
                    break;
                case 'delete':
                    txt = '<?= __('MSG_CONFIRM_DELETE') ?>';
                    $('#btn_suspend_yes').text('<?= __('BTN_DELETE') ?>');
                    break;
                 case 'recover':
                    $('#btn_suspend_yes').text('<?= __('BTN_RECOVER') ?>');
                    txt = '<?= __('MSG_CONFIRM_UNDO_SUSPEND') ?>';
                    break;
            };

            $(content).find('#btn_suspend_yes').data('id', $(this).closest('tr').data('id'));
            $(content).find('#btn_suspend_yes').data('target', $(this).closest('td').data('target'));
            $(content).find('.modal-body p').html(txt);
            $(content).modal('show');
        });

        $('body').on('click', '#btn_suspend_yes', function (e) {
            var params = {
                id: $(this).data('id'),
                is_suspend: $(this).data('target')
            };
            $.post('<?php echo $this->Url->build('/customers/update-suspend/'); ?>', params, function (data) {
                if (data.message === 'success') {
                    location.reload();
                }
            }, 'json');
        });

        $('body').on('click', '.tt-selectable', function (e) {
            $('form[name="form_search"]').submit();
        });

        pageOutOfRange(<?= $paging ?>, '<?= $this->request->query('page') ?>');
        var maxPage = parseInt($('#maxPage').val());
        updatePagination(maxPage);
    })();
</script>
