
<section class="modal-wrapper">
    <div class="modal fade" id="modal_detail" data-backdrop="static" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <table class="table table-striped" id="tb-detail">
                        <tbody>
                            <tr>
                                <td>
                                    <?php
                                    echo __d('customer', 'TXT_STATUS') ?>
                                </td>
                                <td colspan="2">
                                    <?php
                                    echo $customer->type; ?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <?php
                                    echo __d('customer', 'TXT_OLD_CODE') ?>
                                </td>
                                <td colspan="2">
                                    <?php
                                    echo $customer->code; ?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <?php
                                    echo __d('customer', 'TXT_CLINIC') ?>
                                </td>
                                <td colspan="2">
                                    <?php
                                    echo $this->Comment->getFieldByLocal($customer, $locale); ?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <?php
                                    echo __d('customer', 'TXT_ADDRESS') ?>
                                </td>
                                <td>
                                    <?php
                                    echo $this->Address->format($customer, LOCAL_EN); ?>
                                </td>
                                <td>
                                    <?php
                                    echo $this->Address->format($customer); ?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <?php
                                    echo __d('customer', 'TXT_TELL') ?>
                                </td>
                                <td>
                                    <?php
                                    if ($customer->info_details) {
                                        $element = '<ul class="email-wrap">';
                                        foreach ($customer->info_details as $item) {
                                            if (!empty($item->tel) || ($item->tel !== null)) {
                                                $element .= '<li>' . $item->tel . '</li>';
                                            }
                                        }
                                        echo $element . '</ul>';
                                    }
                                    ?>
                                </td>
                                <td>
                                    <?php
                                    if ($customer->info_details) {
                                        $element = '<ul class="email-wrap">';
                                        foreach ($customer->info_details as $item) {
                                            if (!empty($item->tel_extension) || ($item->tel_extension !== null)) {
                                                $element .= '<li>' . $item->tel_extension . '</li>';
                                            }
                                        }
                                        echo $element . '</ul>';
                                    }
                                    ?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <?php
                                    echo __d('customer', 'TXT_FAX') ?>
                                </td>
                                <td colspan="2">
                                    <?php
                                    echo $customer->fax; ?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <?php
                                    echo __d('customer', 'TXT_EMAIL') ?>
                                </td>
                                <td colspan="2">
                                    <?php
                                    if ($customer->info_details) {
                                        $element = '<ul class="email-wrap">';
                                        foreach ($customer->info_details as $item) {
                                            $element .= '<li><a href="mailto:' . $item->email . '" target="_blank">' . $item->email . '</a></li>';
                                        }
                                        echo $element . '</ul>';
                                    }
                                    ?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <?php
                                    echo __d('customer', 'TXT_URL') ?>
                                </td>
                                <td colspan="2">
                                    <a href="<?= h($customer->url) ?>" target="_blank"><?= h($customer->url) ?></a>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <?php
                                    echo __d('customer', 'TXT_FORWARDING') ?>
                                </td>
                                <td colspan="2">
                                    
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <?php
                                    echo __d('customer', 'TXT_PAYMENT_TERM') ?>
                                </td>
                                <td colspan="2">
                                    <?php
                                    echo $customer->payment_name; ?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <?php
                                    echo __d('customer', 'TXT_INVOICING_BY') ?>
                                </td>
                                <td colspan="2">
                                    <?php
                                    echo $customer->invoice_name; ?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <?php
                                    echo __d('customer', 'TXT_SUBSIDIARY') ?>
                                </td>
                                <td colspan="2">
                                <?php if ($customer->customer_subsidiaries): ?>
                                    <ul>
                                    <?php foreach ($customer->customer_subsidiaries as $key => $value): ?>
                                        <li>
                                        <?php
                                        echo $this->Comment->getFieldByLocal($value->subsidiary, $locale); ?>
                                        </li>
                                    <?php endforeach; ?>
                                    </ul>
                                <?php endif; ?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <?php
                                    echo __d('customer', 'TXT_DOCTOR') ?>
                                </td>
                                <td colspan="2">
                                    <?php
                                    $doctor_id = !empty($customer->clinic_doctors[0]->doctor_id) ? $customer->clinic_doctors[0]->doctor_id : 0;
                                    $TXT_DOCTOR_NUMBER = '';
                                    if (count($customer->clinic_doctors) < 2) {
                                        $TXT_DOCTOR_NUMBER = __('CUSTOMERS_TXT_DOCTOR_NUMBER_SINGULAR');
                                    } else {
                                        $TXT_DOCTOR_NUMBER = __('CUSTOMERS_TXT_DOCTOR_NUMBER_PLURAL');
                                    }
                                    echo $this->Html->link(count($customer->clinic_doctors).' '.$TXT_DOCTOR_NUMBER, [
                                        'controller' => 'Doctors',
                                        'action' => (count($customer->clinic_doctors) ? 'index' : 'create'),
                                        '?' => [
                                            'external_id' => $doctor_id,
                                            'filter_by' => TYPE_CUSTOMER,
                                            'des' => TYPE_CUSTOMER,
                                        ]
                                    ], [
                                        'class' => 'btn btn-default text-center btn-sm',
                                    ]);
                                    ?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <?php
                                    echo __d('customer', 'TXT_PERSON_IN_CHARGE') ?>
                                </td>
                                <td colspan="2">
                                    <?php
                                    $TXT_PERSON_IN_CHARGE_NUMBER = '';
                                    if (count($customer->clinic_doctors) < 2) {
                                        $TXT_PERSON_IN_CHARGE_NUMBER = __('CUSTOMERS_TXT_PERSON_IN_CHARGE_NUMBER_SINGULAR');
                                    } else {
                                        $TXT_PERSON_IN_CHARGE_NUMBER = __('CUSTOMERS_TXT_PERSON_IN_CHARGE_NUMBER_PLURAL');
                                    }
                                    echo $this->Html->link(count($customer->person_in_charges).' '.$TXT_PERSON_IN_CHARGE_NUMBER, [
                                        'controller' => 'personInCharges',
                                        'action' => (count($customer->person_in_charges) ? 'index' : 'add'),
                                        '?' => [
                                            'external_id' => $customer->id,
                                            'filter_by' => TYPE_CUSTOMER,
                                            'des' => TYPE_CUSTOMER,
                                        ]
                                    ], [
                                        'class' => 'btn btn-default text-center btn-sm',
                                    ]);
                                    ?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <?php
                                    echo __d('customer', 'TXT_MODIFIED') ?>
                                </td>
                                <td colspan="2">
                                    <?php
                                    echo date('Y-m-d', strtotime($customer->modified)); ?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <?php
                                    echo __d('customer', 'TXT_REGISTERED') ?>
                                </td>
                                <td colspan="2">
                                    <?php
                                    echo date('Y-m-d', strtotime($customer->created)); ?>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="modal-footer">
                    <?php
                    echo $this->Form->button(__d('customer', 'TXT_CANCEL'), [
                        'class' => 'btn btn-default btn-width btn-sm btn-close-modal modal-close',
                        'data-dismiss' => 'modal',
                    ]); ?>
                </div>
            </div>
        </div>
    </div>
</section>
