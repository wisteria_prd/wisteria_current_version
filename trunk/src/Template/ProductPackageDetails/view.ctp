<table class="table table-striped tb-prbr-view" id="tb-detail">
    <tbody>
        <tr>
            <td><?= __('TXT_PRODUCT_NAME') ?> :</td>
            <td><?php echo $products->product->name; ?></td>
            <td><?php echo $products->product->name_en; ?></td>
        </tr>
        <tr>
            <td><?= __('TXT_PACK_NAME') ?> :</td>
            <td>
                <?php
                if ($products->tb_pack_sizes) {
                    echo $products->tb_pack_sizes->name;
                } else {
                    echo '&nbsp;';
                }
                ?>
            </td>
            <td>
                <?php
                if ($products->tb_pack_sizes) {
                    echo $products->tb_pack_sizes->name_en;
                } else {
                    echo '&nbsp;';
                }
                ?>
            </td>
        </tr>
        <tr>
            <td><?= __('TXT_PACK_SIZE') ?> :</td>
            <td colspan="2"><?php echo $products->pack_size; ?></td>
        </tr>
        <tr>
            <td><?= __('TXT_SINGLE_UNIT_NAME') ?> :</td>
            <td>
                <?php
                if ($products->single_unit) {
                    echo $products->single_unit->name;
                } else {
                    echo '&nbsp;';
                }
                ?>
            </td>
            <td>
                <?php
                if ($products->single_unit) {
                    echo $products->single_unit->name_en;
                } else {
                    echo '&nbsp;';
                }
                ?>
            </td>
        </tr>
        <tr>
            <td><?= __('TXT_SINGLE_UNIT_SIZE') ?> :</td>
            <td colspan="2"><?php echo $products->single_unit_size; ?></td>
        </tr>
        <tr>
            <td><?= __('TXT_LOGISTIC_TEMPERATURE') ?> :</td>
            <td><?php echo $products->logistic_temperature->name; ?></td>
            <td><?php echo $products->logistic_temperature->name_en; ?></td>
        </tr>
        <tr>
            <td><?= __('TXT_TEMPERATURE_RANGE') ?> :</td>
            <td colspan="2">
                <?php echo $products->logistic_temperature->temperature_range; ?>
            </td>
        </tr>
        <tr>
            <td><?= __('TXT_TYPE') ?> :</td>
            <td colspan="2">
                <?php echo $products->type; ?>
            </td>
        </tr>
        <tr>
            <td><?= __('TXT_PATIENT_AGREEMENT') ?> :</td>
            <td colspan="2">
                <?php echo ($products->is_patient_agreement) ? __('はい') : __('いいえ'); ?>
            </td>
        </tr>
        <tr>
            <td><?= __('TXT_IMPORT_CERTIFICATE') ?> :</td>
            <td colspan="2">
                <?php echo ($products->import_certificate) ? __('はい') : __('いいえ'); ?>
            </td>
        </tr>
        <tr>
            <td><?= __('TXT_AVAILABLE_SALE') ?> :</td>
            <td colspan="2">
                <?php echo ($products->sale_flag) ? __('はい') : __('いいえ'); ?>
            </td>
        </tr>
        <tr>
            <td><?= __('TXT_AVAILABLE_PURCHASE') ?> :</td>
            <td colspan="2">
                <?php echo ($products->purchase_flag) ? __('はい') : __('いいえ'); ?>
            </td>
        </tr>
        <tr>
            <td><?= __('Original Price') ?> :</td>
            <td colspan="2">
                ???
            </td>
        </tr>
        <tr>
            <td><?= __('STR_USER_REMARK') ?> :</td>
            <td colspan="2">
                <textarea readonly="readonly" style="background: none;"><?php echo $products->product->remarks; ?></textarea>
            </td>
        </tr>
    </tbody>
</table>