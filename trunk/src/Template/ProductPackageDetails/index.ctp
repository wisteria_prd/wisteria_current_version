
<?php
$data1 = [];
if (iterator_count($data)) {
    $data1 = $data->first();
}
?>
<div class="row row-top-space">
    <div class="col-sm-6">
        <div class="search-section" style="margin-bottom: 20px;">
            <?php
            echo $this->Form->create('ProductPackageDetails', [
                'role' => 'form',
                'class' => 'form-inline form-search',
                'type' => 'get',
                'name' => 'form_search'
            ]);
            echo $this->Form->hidden('pkg_id', ['value' => $this->request->query('pkg_id')]);
            echo $this->Form->input('keyword', [
                'name' => 'keyword',
                'placeholder' => __d('product_package_detail', 'TXT_ENTER_KEYWORDS'),
                'class' => 'form-control',
                'label' => false,
                'value' => $this->request->query('keyword'),
                'templates' => [
                    'inputContainer' => '<div class="form-group">{{content}}</div>',
                ]
            ]);
            $this->SwitchStatus->render();
            echo $this->Form->end();
            ?>
        </div>
        <div class="total-pagination" style="padding-bottom: 20px;">
            <?php
            echo $this->element('display_info'); ?>
        </div>
    </div>
    <div class="col-sm-6">
        <table class="table table-condensed table-borderless">
            <tr>
                <td width="25%">
                    <?php
                    echo __d('product_package_detail', 'TXT_PACKAGE_UNIT_PRICE'); ?> :
                </td>
                <td>
                    <?php
                    if (iterator_count($data)) {
                        echo $this->Currency->format($data1->product_package->currency->code_en, $data1->product_package->unit_price);
                    }
                    ?>
                </td>
            </tr>
            <tr>
                <td>
                    <?php
                    echo __d('product_package_detail', 'TXT_TOTAL_STANDARD_SALE_PRICE'); ?> :
                </td>
                <td>
                    <?php
                    $totalPrice = 0;
                    if (iterator_count($data)) {
                        foreach ($data as $key => $value) {
                            $price = $value->product_detail->w_standard_price->standard_price;
                            $totalPrice += (int)($price * $value->quantity);
                        }
                        echo $this->Currency->format($data1->product_package->currency->code_en, $totalPrice);
                    }
                    ?>
                </td>
            </tr>
            <tr>
                <td>
                    <?php
                    echo __d('product_package_detail', 'TXT_DIFFERENCE'); ?> :
                </td>
                <td>
                    <?php
                    if (iterator_count($data)) {
                        $difference = (int)($data1->product_package->unit_price - $totalPrice);
                        $round = round($difference / $totalPrice);
                        $percent = ' (' . number_format((float)$round, 2, '.', '') . '%)';
                        if ($difference >= 0) {
                            echo $this->Currency->format($data1->product_package->currency->code_en, $difference) . $percent;
                        } else {
                            echo '<span style="color: red;">' . $this->Currency->format($data1->product_package->currency->code_en, $difference) . $percent . '</span>';
                        }
                    }
                    ?>
                </td>
            </tr>
        </table>
    </div>
</div>
<div class="row search-section" style="margin-bottom: 20px;">
    <?php
    echo $this->ActionButtons->btnRegisterNew('ProductPackageDetails', ['pkg_id' => $this->request->query('pkg_id')]) ?>
    <div class="clearfix"></div>
</div>
<div class="data-tb-list">
    <?php if ($data): ?>
    <table class="table table-striped">
        <thead>
            <tr>
                <th>#</th>
                <th>&nbsp;</th>
                <th class="white-space">
                    <?php
                    echo __d('product_package_detail', 'TXT_OLD_CODE') ?>
                </th>
                <th class="white-space">
                    <?php
                    echo __d('product_package_detail', 'TXT_PRODUCT') ?>
                </th>
                <th class="white-space">
                    <?php
                    echo __d('product_package_detail', 'TXT_PURCHASE_AVAILABILITY') ?>
                </th>
                <th class="white-space">
                    <?php
                    echo __d('product_package_detail', 'TXT_SALE_AVAILABILITY') ?>
                </th>
                <th class="white-space">
                    <?php
                    echo $this->Paginator->sort('quantity', __d('product_package_detail', 'TXT_QUANTITY')); ?>
                    <i class="fa fa-sort-desc" aria-hidden="true"></i>
                </th>
                <th class="white-space">
                    <?php
                    echo __d('product_package_detail', 'TXT_SUB_TOTAL'); ?>
                </th>
                <th colspan="3">&nbsp;</th>
            </tr>
        </thead>
        <tbody class="normal-cursor">
            <?php
            $sub_total = 0;
            $numbering = $this->Paginator->counter('{{start}}');
            foreach ($data as $item):
                $price = $item->product_detail->w_standard_price->standard_price;
                $sub_total = $price * $item->quantity;
            ?>
            <tr data-id="<?= h($item->id); ?>">
                <td scope="row">
                    <?php
                    echo $numbering; $numbering++; ?>
                </td>
                <td>
                    <?php
                    echo $this->ActionButtons->disabledText($item->is_suspend); ?>
                </td>
                <td>
                    <?php
                    echo h($item->product_detail->product->code); ?>
                </td>
                <td>
                    <?php
                    $prd = $this->Comment->getFieldByLocal($item->product_detail->product->product_brand, $locale);
                    $prd .= ' ' . $this->Comment->getFieldByLocal($item->product_detail->product, $locale);
                    echo h($prd); ?>
                </td>
                <td>
                    <?php
                    $purchase_availability = false;
                    if ($item->product_detail->purchase_flag && $item->product_detail->product->purchase_flag && $item->product_detail->product->product_brand->is_purchase) {
                        $purchase_availability = true;
                    }
                    if ($purchase_availability) {
                        echo __d('product_package_detail', 'TXT_YES');
                    } else {
                        echo __d('product_package_detail', 'TXT_NO');
                    }
                    ?>
                </td>
                <td>
                    <?php
                    $sale_availability = false;
                    if ($item->product_detail->sale_flag && $item->product_detail->product->sell_flag && $item->product_detail->product->product_brand->is_sale) {
                        $sale_availability = true;
                    }
                    if ($sale_availability) {
                        echo __d('product_package_detail', 'TXT_YES');
                    } else {
                        echo __d('product_package_detail', 'TXT_NO');
                    }
                    ?>
                </td>
                <td>
                    <?php
                    echo $this->Currency->format($item->product_package->currency->code_en, $item->quantity); ?>
                </td>
                <td>
                    <?php
                    echo $this->Currency->format($item->product_detail->w_standard_price->currency->code_en, $sub_total); ?>
                </td>
                <td width="1%" data-target="<?php echo ($item->is_suspend) ? 1 : 0; ?>">
                    <?php
                    if ($item->is_suspend == 1) {
                        echo $this->Form->button(BTN_ICON_UNDO, [
                            'class' => 'btn btn-recover btn-sm btn_suspend',
                            'data-name' => 'recover',
                            'escape' => false
                        ]);
                    } else {
                        echo $this->Html->link(BTN_ICON_EDIT, [
                            'controller' => 'ProductPackageDetails',
                            'action' => 'edit', $item->id,
                            '?' => ['pkg_id' => $this->request->query('pkg_id')]
                            ], [
                            'class' => 'btn btn-primary btn-sm',
                            'escape' => false
                        ]);
                    }
                    ?>
                </td>
                <td width="1%" data-target="<?php echo ($item->is_suspend) ? 1 : 0; ?>">
                    <?php
                    if ($item->is_suspend == 1) {
                        echo $this->Form->button(BTN_ICON_DELETE, [
                            'class' => 'btn btn-delete btn-sm',
                            'id' => 'btn_delete',
                            'escape' => false
                        ]);
                    } else {
                        echo $this->Form->button(BTN_ICON_STOP, [
                            'class' => 'btn btn-suspend btn-sm btn_suspend',
                            'data-name' => 'suspend',
                            'escape' => false
                        ]);
                    }
                    ?>
                </td>
            </tr>
            <?php
            endforeach; ?>
        </tbody>
    </table>
    <?php endif; ?>
</div>
<div class="row row-top-space" style="padding-bottom: 20px;">
    <?php
    echo $this->element('display_number'); ?>
    <?php
    echo $this->element('next_prev') ?>
    <div class="clearfix"></div>
</div>

<!--Modal Detail-->
<?php echo $this->element('Modal/view_detail'); ?>

<!--Modal delete-->
<?php echo $this->element('Modal/delete'); ?>

<!--Modal Suspend-->
<?php echo $this->element('Modal/suspend'); ?>

<script>
    (function() {
        // price_different();
        // function price_different() {
        //     var currency = $('.pricing-currency').val();
        //     var unit_price = parseInt($('.pricing-unit-price').val());
        //     var amount = parseInt($('#sum-amount').val());
        //     $('.pkg-unit-price').text('<?= __('円') ?>' + unit_price);
        //     $('#product-amount').empty().text(currency + ' ' + amount);
        //     if (unit_price > 0 && amount > 0) {
        //         var different = unit_price - amount;
        //         $('#sub-amount').text(currency + ' ' + different);
        //         var round = '(';
        //         round += Math.round(different / amount).toFixed(2);
        //         round += '%)';
        //         $('#different-by').text(round);
        //     }
        // }

        var optionData = [];
        GetDataList(optionData);
        //typehead_initialize('#keyword', optionData);

        $('body').on('click', '.table td a, .table td select', function(e) {
            e.stopPropagation();
        });

        $('body').on('click', '#btn_delete', function (e) {
            e.stopPropagation();
            var content = $('#modal_delete');
            $(content).find('#btn_delete_yes').data('id', $(this).closest('tr').data('id'));
            $(content).modal('show');
        });

        $('body').on('click', '#btn_delete_yes', function (e) {
            $.post('<?php echo $this->Url->build('/product-package-details/delete/'); ?>', {id: $(this).data('id')}, function (data) {
                if (data.message === 'success') {
                    window.location.href = '<?php echo $this->Url->build(['controller' => 'ProductPackageDetails', 'action' => 'index', '?' => ['pkg_id' => $this->request->query('pkg_id')]]); ?>';
                }
            }, 'json');
        });
        $('body .data-tb-list .table > tbody > tr').on('click', '.btn_suspend', function (e) {
            e.stopPropagation();
            var content = $('#modal_suspend');
            var status = $(this).attr('data-name');
            var txt = '';

            switch (status) {
                case 'suspend':
                    $('#btn_suspend_yes').text('<?= __('BTN_SUSPEND') ?>');
                    txt = '<?= __('MSG_CONFIRM_SUSPEND') ?>';
                    break;
                case 'delete':
                    txt = '<?= __('MSG_CONFIRM_DELETE') ?>';
                    $('#btn_suspend_yes').text('<?= __('BTN_DELETE') ?>');
                    break;
                 case 'recover':
                    $('#btn_suspend_yes').text('<?= __('BTN_RECOVER') ?>');
                    txt = '<?= __('MSG_CONFIRM_UNDO_SUSPEND') ?>';
                    break;
            };

            $(content).find('#btn_suspend_yes').data('id', $(this).closest('tr').data('id'));
            $(content).find('#btn_suspend_yes').data('target', $(this).closest('td').data('target'));
            $(content).find('.modal-body p').html(txt);
            $(content).modal('show');
        });

        $('body').on('click', '.btn_suspend', function (e) {
            e.stopPropagation();
            var content = $('#modal_suspend');
            var txt = '';
            if ($(this).data('name') === 'recover') {
                txt = '<?= __('MSG_CONFIRM_UNDO_SUSPEND') ?>';
            } else {
                txt = '<?= __('MSG_CONFIRM_SUSPEND') ?>';
            }

            $(content).find('#btn_suspend_yes').data('id', $(this).closest('tr').data('id'));
            $(content).find('#btn_suspend_yes').data('target', $(this).closest('td').data('target'));
            $(content).find('.modal-body p').html(txt);
            $(content).modal('show');
        });

        $('body').on('click', '#btn_suspend_yes', function (e) {
            var params = {
                id: $(this).data('id'),
                is_suspend: $(this).data('target')
            };
            $.post('<?php echo $this->Url->build('/product-package-details/update-suspend/'); ?>', params, function (data) {
                if (data.message === 'success') {
                    location.reload();
                }
            }, 'json');
        });

        $('body').on('click', '.tt-selectable', function (e) {
            $('form[name="form_search"]').submit();
        });

        function GetDataList(optionData) {
            $.ajax({
                url: '<?php echo $this->Url->build('/product-package-details/get-list'); ?>',
                type: 'get',
                dataType: 'json',
                contentType: 'application/json; charset=utf-8'
            }).done(function (data) {
                var response = data.data;
                if (response != null && response != undefined) {
                    $.each(response, function (i, v) {
                        optionData.push(v.name<?= $this->request->session()->read('tb_field') ?>);
                    });
                }
            });
        }
    })();
</script>
