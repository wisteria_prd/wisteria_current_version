<div class="row">
    <div class="col-sm-12">
        <h3 class="text-center"><?= __('TXT_FIND_PRODUCT') ?></h3>
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <?php
            echo $this->Form->create(null, [
                'role' => 'form',
                'class' => 'form-horizontal form-product-list-search',
                'name' => 'common_form'
            ]);

            $this->Form->templates([
                'inputContainer' => '{{content}}'
            ]);
        ?>
        <div class="form-group">
            <label class="col-sm-4 control-label"><?= __('TXT_MANUFACTURER_NAME') ?></label>
            <div class="col-sm-7">
                <?php
                $manuf = [];
                if ($menufacturers) {
                    foreach ($menufacturers as $m) {
                        $manuf[$m->id] = $this->Comment->nameEnOrJp($m->name, $m->name_en, $en);
                    }
                }
                echo $this->Form->select('manufacturer_id', $manuf, [
                    'class' => 'form-control',
                    'label' => false,
                    'required' => false,
                    'id' => 'select-manufacturer',
                    'empty' => ['' => __('TXT_SELECT_MANUFACTURER')]
                ]);
                ?>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-4 control-label"><?= __('TXT_BRAND_NAME') ?></label>
            <div class="col-sm-7">
                <?php echo $this->Form->select('product_brand_id', [], [
                    'class' => 'form-control',
                    'label' => false,
                    'required' => false,
                    'id' => 'select-product-brand',
                    'empty' => ['' => __('TXT_SELECT_BRAND_NAME')],
                    'disabled' => true
                ]); ?>
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-11">
                <h4 class="text-center"><?= __('Found') ?> <span class="amount-found">0</span> <?= __('products') ?></h4>
            </div>
        </div>
        <div class="form-group custom-select">
            <label class="col-sm-4 control-label"><?= __('TXT_PRODUCT_NAME') ?></label>
            <div class="col-sm-7">
                <?php
                    $pds = [];
                    if ($products) {
                        foreach ($products as $product) {
                            $n = $product->name;
                            if (empty($n)) {
                                $n = $product->name_en;
                            }
                            $pds[$product->id] = $n;
                        }
                    }
                    echo $this->Form->select('product_id', $pds, [
                        'class' => 'form-control',
                        'label' => false,
                        'required' => false,
                        'id' => 'select-product-name',
                        'default' => isset($product_id) && !empty($product_id) ? $product_id : '',
                        'empty' => ['' => __('TXT_SELECT_PRODUCT_NAME')]
                    ]);
                ?>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-4 control-label"><?= __('TXT_PRODUCT_DETAIL') ?></label>
            <div class="col-sm-7">
                <?php 
                if (isset($product_details)) {
                    $details = $this->Comment->singleProductAndDetails($product_details, $en);
                    echo $this->Form->select('product_detail_id', $details, [
                        'class' => 'form-control',
                        'label' => false,
                        'required' => false,
                        'id' => 'select-product-detail-name',
                        'default' => $product_detail_id,
                        'empty' => ['' => __('TXT_SELECT_PRODUCT_DETAIL')],
                        'data-pid' => $product_id
                    ]);
                } else {
                    echo $this->Form->input('product_detail_id', [
                        'type' => 'select',
                        'options' => [],
                        'class' => 'form-control',
                        'label' => false,
                        'required' => false,
                        'id' => 'select-product-detail-name',
                        'disabled' => true
                    ]);
                }
                ?>
            </div>
            <div class="col-sm-1 text-left" style="padding-left: 0;">
                <button type="button" class="btn btn-sm btn-primary btn-view-detail-price">...</button>
            </div>
        </div>
        <?= $this->Form->end() ?>
    </div>
</div>
<div class="row">
    <div class="col-sm-12 table-product-price">
        <table class="table table-condensed table-striped">
            <thead>
                <tr>
                    <th>&nbsp;</th>
                    <th>#</th>
                    <th><?= __('TXT_CURRENCY') ?></th>
                    <th><?= __('TXT_PRICES') ?></th>
                </tr>
            </thead>
            <tbody>
                <?php
                if (isset($prices) && !empty($prices)) {
                    $i = 1;
                    foreach ($prices as $price) { ?>
                <tr>
                    <td>
                        <?php
                        echo $this->Form->checkbox('use', [
                            'hiddenField' => false,
                            'class' => 'check-product',
                            'data-id' => $price->id
                        ]);
                        ?>
                    </td>
                    <td><?= $i ?></td>
                    <td class="currency">
                        <?php
                        if ($price->currency) {
                            echo $price->currency->code;
                        }
                        ?>
                    </td>
                    <td class="currency-price"><?= $price->standard_price ?></td>
                </tr>
                <?php
                    $i++;
                    }
                }
                ?>
            </tbody>
        </table>
    </div>
</div>