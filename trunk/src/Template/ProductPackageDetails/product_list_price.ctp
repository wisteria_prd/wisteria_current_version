<table class="table table-condensed table-striped">
    <thead>
    <tr>
        <th>&nbsp;</th>
        <th>#</th>
        <th><?= __('TXT_CURRENCY') ?></th>
        <th><?= __('TXT_PRICES') ?></th>
    </tr>
    </thead>
    <tbody>
        <?php
        if ($prices) {
            $i = 1;
            foreach ($prices as $price) { ?>
        <tr>
            <td>
                <?php
                echo $this->Form->checkbox('use', [
                    'hiddenField' => false,
                    'class' => 'check-product',
                    'data-id' => $price->id
                ]);
                ?>
            </td>
            <td><?= $i ?></td>
            <td class="currency">
                <?php
                if ($price->currency) {
                    echo $price->currency->code;
                }
                ?>
            </td>
            <td class="currency-price"><?= $price->standard_price ?></td>
        </tr>
        <?php
            $i++;
            }
        }
        ?>
    </tbody>
</table>