<?php
if ($brands):
    $output = '<option value="">' . __('TXT_SELECT_BRAND_NAME') . '</option>';
    if ($brands) {
        foreach ($brands as $brand) {
            $output .= '<option value="' . $brand->id . '">' . $this->Comment->nameEnOrJp($brand->name, $brand->name_en, $en) . '</option>';
        }
    }
//    $name = 'name' . $en;
//    $output = '<option value="">' . __('TXT_SELECT_BRAND_NAME') . '</option>';
//    foreach ($brands as $brand) {
//        $output .= '<option value="' . $brand->id . '">' . $brand->$name . '</option>';
//    }

    echo $output;
endif;