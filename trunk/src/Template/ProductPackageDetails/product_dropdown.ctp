<?php
if ($products):
    $name = 'name' . $en;
    $output = '<option value="">' . __('TXT_SELECT_PRODUCT_NAME') . '</option>';
    foreach ($products as $product) {
        $output .= '<option value="' . $product->id . '">' . $product->$name . '</option>';
    }
    echo $output;
endif;