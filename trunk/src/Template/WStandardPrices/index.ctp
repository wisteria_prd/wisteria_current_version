<?php
$en = $this->request->session()->read('tb_field'); ?>
<div class="search-section" style="margin-bottom: 20px;">
    <?php
    echo $this->Form->create('WStandardPrices', [
        'role' => 'form',
        'class' => 'form-inline form-search',
        'type' => 'get',
        'name' => 'form_search'
    ]);
    echo $this->Form->hidden('displays', [
        'value' => empty($this->request->query('displays')) ? 10 : $this->request->query('displays')
    ]);
    echo $this->Form->input('keyword', [
        'name' => 'keyword',
        'placeholder' => __d('w_standard_price', 'TXT_ENTER_KEYWORD'),
        'class' => 'form-control',
        'label' => false,
        'value' => $this->request->query('keyword'),
        'autocomplete' => 'off',
        'templates' => [
            'inputContainer' => '<div class="form-group">{{content}}</div>',
        ]
    ]);
    $this->SwitchStatus->render();
    echo $this->Form->submit('submit', [
        'style' => 'display: none',
    ]);
    echo $this->Form->end();
    ?>
</div>
<div class="total-pagination" style="padding-bottom: 20px;">
    <?php echo $this->element('display_info'); ?>
</div>
<div class="row search-section" style="margin-bottom: 20px;">
    <?php echo $this->element('display_number'); ?>
    <?= $this->ActionButtons->btnRegisterNew('WStandardPrices', []) ?>
    <div class="clearfix"></div>
</div>
<div class="data-tb-list">
    <?php if ($data) : ?>
    <table class="table table-striped">
        <thead>
            <tr>
                <th>#</th>
                <th>&nbsp;</th>
                <th>
                    <?php
                    echo __d('w_standard_price', 'TXT_MANUFACTURER') ?>
                </th>
                <th>
                    <?php
                    echo $this->Paginator->sort('Products.name' . $en, __d('w_standard_price', 'TXT_PRODUCT')); ?>
                    <span><i class="fa fa-sort-desc" aria-hidden="true"></i></span>
                </th>
                <th>
                    <?php
                    echo $this->Paginator->sort('standard_price', __d('w_standard_price', 'TXT_UNIT_PRICE')); ?>
                    <span><i class="fa fa-sort-desc" aria-hidden="true"></i></span>
                </th>
                <th width="1%">&nbsp;</th>
                <th width="1%">&nbsp;</th>
                <th width="1%">&nbsp;</th>
            </tr>
        </thead>
        <tbody>
            <?php
            $numbering = $this->Paginator->counter('{{start}}');
            foreach ($data as $key => $value) :
            ?>
            <tr data-id="<?php echo $value->id; ?>">
                <td>
                    <?php
                    echo $numbering; $numbering++; ?>
                </td>
                <td>
                    <?php
                    echo $this->ActionButtons->disabledText($value->is_suspend) ?>
                </td>
                <td>
                    <?php
                    echo $this->Comment->getFieldByLocal($value->product_detail->product->product_brand->manufacturer, $en) ?>
                </td>
                <td>
                    <?php
                    echo $this->Comment->productAndDetails($value->product_detail, $en); ?>
                </td>
                <td>
                    <?php
                    echo $this->Comment->currencyEnJpFormatS($en, $value->standard_price, $value->currency->code);
                    ?>
                </td>
                <td>
                    <?php
                    echo $this->Form->button('<i class="fa fa-comments-o" aria-hidden="true"></i>', [
                        'class' => 'btn btn-primary btn-sm btn_comment',
                        'data-id' => $value->id,
                        'escape' => false,
                    ]);
                    ?>
                </td>
                <td width="1%" data-target="<?php echo ($value->is_suspend) ? 1 : 0; ?>">
                    <?php
                    if ($value->is_suspend == 1) {
                        echo $this->Form->button(BTN_ICON_UNDO, [
                            'class' => 'btn btn-recover btn-sm btn_suspend',
                            'data-name' => 'recover',
                            'escape' => false
                        ]);
                    } else {
                        echo $this->Html->link(BTN_ICON_EDIT, [
                            'controller' => 'w-standard-prices',
                            'action' => 'edit', $value->id,
                            '?' => ['pkg_id' => $this->request->query('pkg_id')]
                            ], [
                            'class' => 'btn btn-primary btn-sm',
                            'escape' => false
                        ]);
                    }
                    ?>
                </td>
                <td width="1%" data-target="<?php echo ($value->is_suspend) ? 1 : 0; ?>">
                    <?php
                    if ($value->is_suspend == 1) {
                        echo $this->Form->button(BTN_ICON_DELETE, [
                            'class' => 'btn btn-delete btn-sm',
                            'id' => 'btn_delete',
                            'escape' => false
                        ]);
                    } else {
                        echo $this->Form->button(BTN_ICON_STOP, [
                            'class' => 'btn btn-suspend btn-sm btn_suspend',
                            'data-name' => 'suspend',
                            'escape' => false
                        ]);
                    }
                    ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <?php endif; ?>
</div>
<div class="row row-top-space" style="padding-bottom: 20px;">
    <?php echo $this->element('display_number'); ?>
    <?= $this->element('next_prev') ?>
    <div class="clearfix"></div>
</div>

<!--Modal Detail-->
<?php echo $this->element('Modal/view_detail'); ?>
<!--Modal Suspend-->
<?php echo $this->element('Modal/suspend'); ?>
<!--Modal delete-->
<?php echo $this->element('Modal/delete'); ?>

<script>
    (function(e) {
        $('.btn_comment').click(function (e) {
            e.stopPropagation();
            let params = {
                id: $(this).closest('tr').attr('data-id'),
                type: '<?php echo MESSAGE_TYPE_W_STANDARD_PRICE; ?>',
                referer: CONTROLLER,
            };
            let options = {
                url: BASE + 'messages/get-message-list',
                type: 'GET',
                data: params,
                beforeSend: function () {
                    $.LoadingOverlay('show');
                }
            };
            ajaxRequest(options, SESSION_TIMEOUT, function (data) {
                $('body').prepend(data);
                $('body').find('.modal-message').modal('show');
            });
        });

        //auto complete
        $('body').find('#keyword').typeahead({
            minLength: 0,
            autoSelect: true,
            source:  function (query, process) {
                return $.get('<?= $this->Url->build('/products/getProductNameBySearch/') ?>', {
                        keyword: query
                    }, function (data) {
                        var optionData = [];
                        if (data.data !== null && data.data !== 'undefined') {
                            $.each(data.data, function(i, v) {
                                if (v.name !== '') {
                                    optionData.push(v.name);
                                }
                                if (v.name_en !== '') {
                                    optionData.push(v.name_en);
                                }
                            });
                        }
                        return process(optionData);
                    }, 'json');
            }
        });

        $('body').on('click', '.typeahead li', function(e) {
            $('body').find('.form-search').submit();
        });

        $('body').on('click', '.data-tb-list .table>tbody>tr', function (e) {
            $.LoadingOverlay('show');
            var url = '<?php echo $this->Url->build('/w-standard-prices/view/'); ?>';
            var id = $(this).closest('tr').data('id');

            $.get(url + id, function (data) {
                $.LoadingOverlay('hide');
                $('#modal_detail').find('.modal-body').html(data);
                $('#modal_detail').modal('show');
            }, 'html');
        });

        $('body').on('click', '.table>tbody>tr>td a', function(e) {
            e.stopPropagation();
        });

        //modal confirm suspend
        $('body').on('click', '.btn_suspend', function (e) {
            e.stopPropagation();
            $('.suspend-item-id').val($(this).attr('data-id'));
            $('.is-suspend-item').val($(this).attr('data-target'));
            var confirm = '';
            if (parseInt($(this).attr('data-target')) === 1) {
                $('#btn_suspend_yes').text('<?= __('BTN_SUSPEND') ?>');
                confirm = '<?= __('MSG_CONFIRM_SUSPEND') ?>';
            } else {
                $('#btn_suspend_yes').text('<?= __('BTN_RECOVER') ?>');
                confirm = '<?= __('MSG_CONFIRM_UNDO_SUSPEND') ?>';
            }
            $('.confirm-suspend-text').text(confirm);
            $('#modal_suspend').modal('show');
        });

        //modal change suspend status
        //modal change suspend status
        $('body .data-tb-list .table > tbody > tr').on('click', '.btn_suspend', function (e) {
            e.stopPropagation();
            var content = $('#modal_suspend');
            var status = $(this).attr('data-name');
            var txt = '';

            switch (status) {
                case 'suspend':
                    $('#btn_suspend_yes').text('<?= __('BTN_SUSPEND') ?>');
                    txt = '<?= __('MSG_CONFIRM_SUSPEND') ?>';
                    break;
                case 'delete':
                    txt = '<?= __('MSG_CONFIRM_DELETE') ?>';
                    $('#btn_suspend_yes').text('<?= __('BTN_DELETE') ?>');
                    break;
                 case 'recover':
                    $('#btn_suspend_yes').text('<?= __('BTN_RECOVER') ?>');
                    txt = '<?= __('MSG_CONFIRM_UNDO_SUSPEND') ?>';
                    break;
            };

            $(content).find('#btn_suspend_yes').data('id', $(this).closest('tr').data('id'));
            $(content).find('#btn_suspend_yes').data('target', $(this).closest('td').data('target'));
            $(content).find('.modal-body p').html(txt);
            $(content).modal('show');
        });
        $('body').on('click', '#btn_suspend_yes', function (e) {
            var params = {
                id: $(this).data('id'),
                is_suspend: $(this).data('target')
            };
            $.post('<?php echo $this->Url->build('/w-standard-prices/update-suspend/'); ?>', params, function (data) {
                if (data.message === 'success') {
                    location.reload();
                }
            }, 'json');
        });

        //modal delete confirm
        $('body').on('click', '#btn_delete', function (e) {
            e.stopPropagation();
            $('#btn_delete_yes').text('<?= __('BTN_DELETE') ?>');
            $('.delete-item').val($(this).attr('data-id'));
            $('#modal_delete').modal('show');
        });

        //modal delete item.
        $('body').on('click', '#btn_delete_yes', function() {
            var data = $('.form-delete').serialize();
            var url = '<?= $this->Url->build('/w-standard-prices/delete') ?>';
            itemDelete(url, data, '<?php echo __('TXT_SESSION_TIMEOUT') ?>');
        });




//        $('body').on('click', '.btn_suspend', function (e) {
//            e.stopPropagation();
//            var content = $('#modal_suspend');
//            var txt = '';
//            if ($(this).data('name') === 'recover') {
//                txt = 'Are you sure to recover the wstandard price?';
//            } else {
//                txt = 'Are you sure to suspend the wstandard price?';
//            }
//
//            $(content).find('#btn_suspend_yes').data('id', $(this).closest('tr').data('id'));
//            $(content).find('#btn_suspend_yes').data('target', $(this).closest('td').data('target'));
//            $(content).find('.modal-body p').html(txt);
//            $(content).modal('show');
//        });
//
//        $('body').on('click', '#btn_suspend_yes', function (e) {
//            var params = {
//                id: $(this).data('id'),
//                is_suspend: $(this).data('target')
//            };
//            $.post('<?php echo $this->Url->build('/w-standard-prices/update-suspend/'); ?>', params, function (data) {
//                if (data.message === 'success') {
//                    location.reload();
//                }
//            }, 'json');
//        });
//
//        $('body').on('click', '#btn_delete', function (e) {
//            e.stopPropagation();
//            var content = $('#modal_delete');
//            $(content).find('#btn_delete_yes').attr('data-id', $(this).closest('tr').data('id'));
//            $(content).modal('show');
//        });
//
//        $('body').on('click', '#btn_delete_yes', function (e) {
//            $.post('<?php echo $this->Url->build('/w-standard-prices/delete/'); ?>', {id: $(this).data('id')}, function (data) {
//                if (data.message === 'success') {
//                    location.reload();
//                }
//            }, 'json');
//        });

        pageOutOfRange(<?= $paging ?>, '<?= $this->request->query('page') ?>');
        var maxPage = parseInt($('#maxPage').val());
        updatePagination(maxPage);
    })();
</script>
