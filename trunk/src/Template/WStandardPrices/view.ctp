
<?php $local = $this->request->session()->read('tb_field');?>
<table class="table table-striped" id="tb-detail">
    <tbody>
        <tr>
            <td><?= __('TXT_PRODUCT_NAME') ?> :</td>
            <td><?php echo $data->product_detail->product->name_en; ?></td>
            <td><?php echo $data->product_detail->product->name; ?></td>
        </tr>
        <tr>
            <td><?= __('TXT_PRODUCT_DETAIL') ?> :</td>
            <td colspan="2">
                <?php
                echo $this->Comment->productAndDetails($data->product_detail, $local);
                ?>
            </td>
        </tr>
        <tr>
            <td><?= __('TXT_PACKING') ?> :</td>
            <td>
                <?php
                if ($data->product_detail->tb_pack_sizes) {
                    echo $data->product_detail->tb_pack_sizes->name_en;
                }
                ?>
            </td>
            <td>
                <?php
                if ($data->product_detail->tb_pack_sizes) {
                    echo $data->product_detail->tb_pack_sizes->name;
                }
                ?>
            </td>
        </tr>
        <tr>
            <td><?= __('TXT_PACKING_UNIT') ?> :</td>
            <td>
                <?php
                if ($data->product_detail->tb_pack_sizes) {
                    echo $data->product_detail->tb_pack_sizes->unit_name_en;
                }
                ?>
            </td>
            <td>
                <?php
                if ($data->product_detail->tb_pack_sizes) {
                    echo $data->product_detail->tb_pack_sizes->unit_name;
                }
                ?>
            </td>
        </tr>
        <tr>
            <td><?= __('TXT_STANDARD_PRICE') ?> :</td>
            <td colspan="2">
                <?php
                echo $this->Comment->currencyEnJpFormatS($local, $data->standard_price,  $data->currency);
                ?>
            </td>
        </tr>
    </tbody>
</table>