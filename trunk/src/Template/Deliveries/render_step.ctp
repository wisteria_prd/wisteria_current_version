<?php
$options = [];
$step = 0;
$status = $saleStatus[$currentStatus];
// Separate controller
// switch status
switch ($currentStatus) {
    case STATUS_PACKED:
        $step = 4;
        $options = [
            PO_STATUS_DELIVERING => $saleStatus[PO_STATUS_DELIVERING],
        ];
        break;
    case PO_STATUS_DELIVERING:
        $step = 5;
        $options = [
            PO_STATUS_DELIVERED => $saleStatus[PO_STATUS_DELIVERED],
        ];
        break;
    case PO_STATUS_DELIVERED:
        $step = 6;
        $options = [
            PO_STATUS_DELIVERING => $saleStatus[PO_STATUS_DELIVERING],
        ];
        break;
}
?>

<?php if ($step == 4) : ?>
<div class="form-group" style="margin-bottom: 10px;">
    <label class="col-sm-4 control-label text-left" style="padding-top: 0px;"><?= __('TXT_CURRENT_STATUS') ?></label>
    <label class="control-label col-md-2"></label>
    <label class="control-label col-md-5 text-left"><?= __('TXT_SELECT_NEXT_STATUS') ?></label>
</div>
<div class="form-group">
    <div class="col-sm-4">
        <label
            class="control-label status label-draft"
            style="width: 100%; text-align: center !important; padding: 7px 0px;">
            <?= $status ?>
        </label>
    </div>
    <div class="col-sm-2" style="position: relative;">
        <span class="glyphicon glyphicon-arrow-right arrow-draft"></span>
    </div>
    <div class="col-sm-6">
        <?php
        echo $this->Form->select('status', $options, [
            'class' => 'form-control',
            'label' => false,
        ]);
        ?>
    </div>
</div>
<div class="form-group">
    <label class="col-sm-5 control-label text-left"><?= __('TXT_TRACKING_NUMBER') ?></label>
    <div class="col-sm-1"></div>
    <?php
    $deliveryId = -1;
    if ($delivery) {
        $deliveryId = $delivery->id;
    }
    echo $this->Form->hidden('delivery_id', [
        'value' => $deliveryId,
        'id' => 'delivery-id',
    ]);
    echo $this->Form->input('tracking', [
        'label' => false,
        'required' => false,
        'class' => 'form-control',
        'placeholder' => __('TXT_TRACKING_NUMBER'),
        'value' => $delivery ? $delivery->tracking : null,
        'templates' => [
            'inputContainer' => '<div class="col-sm-6">{{content}}</div>',
        ],
    ]);
    ?>
</div>
<div class="form-group">
    <label class="col-sm-5 control-label text-left"><?= __('TXT_DELIVERY_DATE') ?></label>
    <div class="col-sm-1"></div>
    <?php
    echo $this->Form->input('delivery_date', [
        'label' => false,
        'required' => false,
        'class' => 'form-control',
        'placeholder' => __('TXT_DELIVERY_DATE'),
        'value' => $delivery ? date('Y-m-d', strtotime($delivery->delivery_date)) : date('Y-m-d'),
        'templates' => [
            'inputContainer' => '<div class="col-sm-6">{{content}}</div>',
        ],
    ]);
    ?>
</div>

<?php elseif ($step == 5) : ?>
<div class="form-group" style="margin-bottom: 10px;">
    <label class="col-sm-4 control-label text-left" style="padding-top: 0px;"><?= __('TXT_CURRENT_STATUS') ?></label>
    <label class="control-label col-md-2"></label>
    <label class="control-label col-md-5 text-left"><?= __('TXT_SELECT_NEXT_STATUS') ?></label>
</div>
<div class="form-group">
    <div class="col-sm-4">
        <label
            class="control-label status label-draft"
            style="width: 100%; text-align: center !important; padding: 7px 0px;">
            <?= $currentStatus ?>
        </label>
    </div>
    <div class="col-sm-2" style="position: relative;">
        <span class="glyphicon glyphicon-arrow-right arrow-draft"></span>
    </div>
    <div class="col-sm-6">
        <?php
        echo $this->Form->select('status', $options, [
            'class' => 'form-control',
            'label' => false,
        ]);
        ?>
    </div>
</div>
<div class="form-group">
    <?php
    echo $this->Form->hidden('delivery_id', [
        'value' => $delivery->id,
    ]);
    $element = '';
    $fileName = '';
    $docType = '';
    $type = 'new';
    $id = 0;
    if ($medias) {
        $type = 'edit';
        foreach ($medias as $key => $value) {
            if ($key != 0) {
                $element .= '<li data-id="' . $value->id . '" data-name="' . $value->file_name . '" data-document-type="' . $value->document_type . '" data-type="' . $type . '"><span class="label label-success browse-file"><i class="fa fa-times rm-pdf-file" aria-hidden="true"></i>&nbsp;&nbsp;Browse File</span><span>&nbsp;</span></li>';
            } else {
                $fileName = $value->file_name;
                $id = $value->id;
                $docType = $value->document_type;
            }
        }
    }
    ?>
    <label class="col-sm-6 control-label text-left"><?= __('TXT_CUSTOM_CLEARANCE') ?></label>
    <div class="col-sm-6 col-md-6">
        <ul class="custom-wrap">
            <li
                data-document-type="<?= $docType ?>"
                data-name="<?= $fileName ?>"
                data-id="<?= $id ?>"
                data-type="<?= $type ?>">
                <span class="label label-success browse-file">
                    &nbsp;&nbsp;Browse File
                </span>
                <span class="label label-primary add-pdf-file">Add</span>
            </li>
            <?= $element ?>
        </ul>
    </div>
</div>

<?php elseif ($step == 6) : ?>
<div class="form-group" style="margin-bottom: 10px;">
    <label class="col-sm-4 control-label text-left" style="padding-top: 0px;"><?= __('TXT_CURRENT_STATUS') ?></label>
    <label class="control-label col-md-2"></label>
    <label class="control-label col-md-5 text-left"><?= __('TXT_SELECT_NEXT_STATUS') ?></label>
</div>
<div class="form-group">
    <div class="col-sm-4">
        <label
            class="control-label status label-draft"
            style="width: 100%; text-align: center !important; padding: 7px 0px;">
            <?= $currentStatus ?>
        </label>
    </div>
    <div class="col-sm-2" style="position: relative;">
        <span class="glyphicon glyphicon-arrow-right arrow-draft"></span>
    </div>
    <div class="col-sm-6">
        <?php
        echo $this->Form->select('status', $options, [
            'class' => 'form-control',
            'label' => false,
        ]);
        ?>
    </div>
</div>

<?php endif; ?>

<div class="form-group">
    <div class="col-sm-6 col-sm-offset-6">
        <?php
        echo $this->Form->button(__('TXT_SUBMIT'), [
            'type' => 'submit',
            'class' => 'btn btn-primary btn-register pull-right',
            'data-step' => $step,
        ]);
        ?>
</div>
<script>
    $(function(e) {
        if ('<?= $step ?>' == 4) {
            $('#delivery-date').datetimepicker({ format: 'YYYY-MM-DD' });
        }
    });
</script>