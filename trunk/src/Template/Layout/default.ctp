<!DOCTYPE html>
<html>
<head>
    <?php echo $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
        <?php echo $this->fetch('title'); ?>
    </title>
    <?php echo $this->Html->meta('icon'); ?>

    <?php
    echo $this->Html->css([
        'bootstrap.min',
        'select2.min',
        'dropzone.min',
        'common',
        'common_admin',
        'jquery-ui.min',
        'jquery-ui.theme.min',
    ]);
    ?>
    <?php
    echo $this->Html->script([
        'jquery-3.1.1.min',
        'jquery-ui.min',
        'bootstrap.min',
        'bootstrap3-typeahead.min',
        'select2.full.min',
        'tableHeadFixer',
        'dropzone.min',
        'moment.min',
        '7f37b65e1a', //font awsome
        'ja',
        'loadingoverlay.min',
        'loadingoverlay_progress.min',
        'bootstrap-datetimepicker.min',
        'common',
    ]);
    ?>
    <?php echo $this->fetch('meta'); ?>
    <?php echo $this->fetch('css'); ?>
    <?php echo $this->element('javascript'); ?>
    <?php echo $this->fetch('script'); ?>
</head>
<body>
    <header>
        <?php
        if ($this->request->session()->read('Auth.User')) {
            echo $this->element('nav');
        }
        ?>
    </header>
    <div class="container-fluid" style="min-height: 86vh;">
        <?php
        echo $this->element('breadcrumb');
        echo $this->fetch('content');
        ?>
    </div>
    <div id="footer-wrap">
        <footer class="mdp-footer">
            <p class="text-center"><?php echo __('Copyright&copy;2017.  Wisteria  Co., Ltd.  All rights reserved.') ?></p>
            <ul>
                <li>
                    <?php echo $this->Html->link(__('利用規約'), [
                        'controller' => 'privacy',
                        'action' => 'term-condition',
                    ]); ?>
                </li>
                <li>
                    <?php echo $this->Html->link(__('プライバシーポリシー'), [
                        'controller' => 'privacy',
                        'action' => 'index',
                    ]); ?>
                </li>
            </ul>
        </footer>
    </div>
</body>
</html>
