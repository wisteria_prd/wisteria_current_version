<!DOCTYPE html>
<html>
<head>
    <?php echo $this->Html->charset() ?>
    <title>
        <?php echo $this->fetch('title'); ?>
    </title>
    <?php echo $this->Html->meta('icon'); ?>

    <?php
        echo $this->Html->css([
            'print'
        ]);
    ?>
    <style>
        @font-face {
            font-family: meiryo;
            src: url('<?= $this->Url->assetUrl('/fonts/meiryo-01.ttf') ?>');
        }
    </style>
    <?= $this->Html->script([]); ?>

    <?php echo $this->fetch('meta'); ?>
    <?php echo $this->fetch('css'); ?>
    <?php echo $this->fetch('script'); ?>
</head>
<body>
    <div class="book">
        <?php echo $this->fetch('content'); ?>
    </div>
</body>
</html>