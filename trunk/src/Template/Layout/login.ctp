<!DOCTYPE html>
<html>
<head>
    <?php echo $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?= $this->fetch('title'); ?></title>
    <?php echo $this->Html->meta('icon'); ?>

    <?php
        echo $this->Html->css([
            'bootstrap.min',
            'common',
            'common_admin'

        ]);
    ?>
    <?php
        echo $this->Html->script([
            'jquery-3.1.1.min',
            'bootstrap.min',
            'bootstrap3-typeahead.min',
            'common',
        ]);
    ?>

    <?php echo $this->fetch('meta'); ?>
    <?php echo $this->fetch('css'); ?>
    <?php echo $this->fetch('script'); ?>
    <script src="https://use.fontawesome.com/7f37b65e1a.js"></script>
</head>
<body>
    <div class="container">
        <?php echo $this->fetch('content'); ?>
    </div>
    <div class="container" style="margin-top: 75px;">
        <div class="row">
            <div class="col-md-12">
                <footer class="row mdp-footer">
                    <p class="text-center">
                        <?php echo __('Copyright&copy;2017.  Wisteria  Co., Ltd.  All rights reserved.') ?>
                    </p>
                </footer>
            </div>
        </div>
    </div>
</body>
</html>