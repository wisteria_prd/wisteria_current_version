
<div class="search-section" style="margin-bottom: 20px;">
    <?php
    echo $this->Form->create('Groups', [
        'role' => 'form',
        'class' => 'form-inline form-search',
        'type' => 'get',
        'name' => 'form_search'
    ]);
    echo $this->Form->hidden('displays', [
        'value' => empty($this->request->query('displays')) ? 10 : $this->request->query('displays')
    ]);
    echo $this->Form->input('keyword', [
        'name' => 'keyword',
        'placeholder' => __('USER_SEARCH_KEYWORD'),
        'class' => 'form-control',
        'label' => false,
        'value' => $this->request->query('keyword'),
        'autocomplete' => 'off',
        'templates' => [
            'inputContainer' => '<div class"form-group">{{content}}</div>',
        ]
    ]);
    echo $this->Form->submit('submit', [
        'style' => 'display: none',
    ]);
    echo $this->Form->end();
    ?>
</div>
<div class="total-pagination" style="padding-bottom: 20px;">
    <?php echo $this->element('display_info'); ?>
</div>
<div class="row search-section" style="margin-bottom: 20px;">
    <?php echo $this->element('display_number'); ?>
    <div class="col-md-6 col-sm-6 text-center">
        <h2 class="title-page"><?php echo __('TXT_GROUPS'); ?></h2>
    </div>
    <div class="col-sm-3 pull-right" style="text-align: right;">
        <?php
        echo $this->Form->button(__('TXT_REGISTER_NEW'), [
            'type' => 'button',
            'class' => 'btn btn-sm btn-primary btn-width btn-add',
            'data-target' => 'register',
        ]);
        ?>
    </div>
    <div class="clearfix"></div>
</div>
<div class="data-tb-list">
    <?php if ($data): ?>
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>#</th>
                    <th>
                        <?php echo $this->Paginator->sort('name', __('STR_GROUP_NAME')); ?>
                        <span><i class="fa fa-sort-desc" aria-hidden="true"></i></span>
                    </th>
                    <th>
                        <?php echo $this->Paginator->sort('type', __('STR_TYPE')); ?>
                        <span><i class="fa fa-sort-desc" aria-hidden="true"></i></span>
                    </th>
                    <th>
                        <?php echo $this->Paginator->sort($this->request->session()->read('user_groups.affiliation_class') === USER_CLASS_MEDIPRO ? 'name_en' : __('名前'), __('STR_MAIN')); ?>
                        <span><i class="fa fa-sort-desc" aria-hidden="true"></i></span>
                    </th>
                    <th><?= __('STR_MEMBER') ?></th>
                    <th width="1%">&nbsp;</th>
                    <th width="1%">&nbsp;</th>
                    <th width="1%">&nbsp;</th>
                </tr>
            </thead>
            <tbody>
                <?php
                    $_en = $this->request->session()->read('tb_field');
                    $numbering = $this->Paginator->counter('{{start}}');
                    foreach ($data as $item) :
                    ?>
                    <tr data-id="<?php echo h($item->id); ?>">
                        <th scope="row" class="td_evt"><?php echo $numbering; $numbering++; ?></th>
                        <td><?php echo $item->name ?></td>
                        <td>
                            <?php
                            $type = ($item->type === TYPE_CUSTOMER) ? __('TXT_CLINIC') : __('TXT_MEDICAL_CORPORATIONS');
                            echo $this->Comment->textHumanize($type);
                            ?>
                        </td>
                        <td>
                            <?php
                            $arr = $this->Comment->filterIsMain($item->customer_groups, 1);
                            if ($arr) {
                                $name = $arr['name'];
                                if ($_en === '_en') {
                                    $name = $arr['name_en'];
                                }
                                echo h($name);
                            }
                            ?>
                        </td>
                        <td class="td-btn-wrap">
                            <?php
                            echo $this->Html->link(count($item->customer_groups), [
                                    'controller' => 'customer-groups',
                                    'action' => 'get-by-group-id', $item->id,
                                    ], [
                                    'class' => 'btn btn-default btn-sm btn-width',
                                ]);
                            ?>
                        </td>
                        <td class="td-btn-wrap">
                            <?php echo $this->Form->button('<i class="fa fa-comments-o" aria-hidden="true"></i>', [
                                'class' => 'btn btn-primary btn-sm btn_comment',
                                'data-id' => $item->id,
                                'type' => 'button',
                                'escape' => false,
                            ]); ?>
                        </td>
                        <td data-target="<?php echo ($item->is_suspend) ? 1 : 0; ?>">
                            <?php
                            if ($item->is_suspend == 1) {
                                echo $this->Form->button(BTN_ICON_UNDO, [
                                    'class' => 'btn btn-recover btn-sm btn_suspend', //btn_rc_ed
                                    'data-name' => 'recover',
                                    'escape' => false
                                ]);
                            } else {
                                echo $this->Form->button(BTN_ICON_EDIT, [
                                    'type' => 'button',
                                    'class' => 'btn btn-primary btn-sm btn-add',
                                    'data-target' => 'edit',
                                    'escape' => false
                                ]);
                            }
                            ?>
                        </td>
                        <td data-target="<?php echo ($item->is_suspend) ? 1 : 0; ?>">
                            <?php
                            if ($item->is_suspend == 1) {
                                if (!empty($item->product_brands) || !empty($item->count_person)) {
                                    echo $this->Form->button(BTN_ICON_DELETE, [
                                        'class' => 'btn btn-delete btn-sm',
                                        'id' => 'btn_delete',
                                        'disabled' => 'disabled',
                                        'escape' => false
                                    ]);
                                } else {
                                    echo $this->Form->button(BTN_ICON_DELETE, [
                                        'class' => 'btn btn-delete btn-sm',
                                        'id' => 'btn_delete',
                                        'data-name' => 'delete',
                                        'escape' => false
                                    ]);
                                }
                            } else {
                                echo $this->Form->button(BTN_ICON_STOP, [
                                    'class' => 'btn btn-suspend btn-sm btn_suspend',
                                    'data-name' => 'suspend',
                                    'escape' => false
                                ]);
                            }
                            ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    <?php endif; ?>
</div>
<div class="row row-top-space" style="padding-bottom: 20px;">
    <?php echo $this->element('display_number'); ?>
    <?= $this->element('next_prev') ?>
    <div class="clearfix"></div>
</div>

<!--Modal Create Group-->
<div class="modal fade" id="modal_group" data-backdrop="static" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <?php echo $this->Form->create('Groups', [
                'role' => 'form',
                'name' => 'form_group',
                'class' => 'form-horizontal',
                'onsubmit' => 'return false;'
            ]); ?>
            <div class="modal-body modal-mt">
                <div class="form-group">
                    <label for="groupName" class="col-sm-3 control-label"><?= __('TXT_GROUP_NAME') ?><?= $this->Comment->formAsterisk() ?></label>
                    <?php echo $this->Form->input('name', [
                        'type' => 'text',
                        'label' => false,
                        'required' => false,
                        'placeholder' => __('TXT_GROUP_NAME'),
                        'class' => 'default-p form-control',
                        'templates' => [
                            'inputContainer' => '<div class="col-sm-9">{{content}}</div>'
                        ],
                    ]); ?>
                </div>
                <div class="form-group custom-select">
                    <label for="groupType" class="col-sm-3 control-label"><?= __('TXT_TYPE') ?><?= $this->Comment->formAsterisk() ?></label>
                    <?php echo $this->Form->input('type', [
                        'type' => 'select',
                        'label' => false,
                        'class' => 'form-control',
                        'options' => $groups,
                        'empty' => ['' => __('TXT_SELECT_GROUP_TYPE')],
                        'templates' => [
                            'inputContainer' => '<div class="col-sm-9">{{content}}</div>'
                        ],
                    ]); ?>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default btn-sm btn-width" data-dismiss="modal"><?= __('TXT_CANCEL') ?></button>
                <?php echo $this->Form->button(__('TXT_REGISTER_SAVE'), [
                    'class' => 'btn btn-primary btn-sm btn-width btn-group-add',
                ]); ?>
            </div>
            <?php echo $this->Form->end(); ?>
        </div>
    </div>
</div>

<!--Modal Comment-->
<?php echo $this->element('/Modal/message') ?>

<!--Modal Suspend-->
<?php echo $this->element('Modal/suspend'); ?>

<!--Modal delete-->
<?php echo $this->element('Modal/delete'); ?>

<script>
    (function (e) {
        //auto complete
        $('body').find('#keyword').typeahead({
            minLength: 0,
            autoSelect: true,
            source:  function (query, process) {
                return $.get('<?= $this->Url->build(['action' => 'getGroupNameBySearch']) ?>', {
                        keyword: query
                    }, function (data) {
                        var optionData = [];
                        if (data.data !== null && data.data !== 'undefined') {
                            $.each(data.data, function(i, v) {
                                optionData.push(v.name);
                            });
                        }
                        return process(optionData);
                    }, 'json');
            }
        });

        $('body').on('click', '.typeahead li', function(e) {
            $('body').find('.form-search').submit();
        });

        $('.btn_comment').click(function (e) {
            e.stopPropagation();
            let params = {
                id: $(this).closest('tr').attr('data-id'),
                type: '<?php echo TYPE_GROUP ?>',
                referer: '<?php echo $this->request->controller; ?>'
            };
            let options = {
                url: BASE + 'Messages/getMessageList',
                type: 'GET',
                data: params,
                beforeSend: function () {
                    $.LoadingOverlay('show');
                }
            };
            ajaxRequest(options, SESSION_TIMEOUT, function (data) {
                $('body').prepend(data);
                $('body').find('.modal-message').modal('show');
            });
        });

        //filter message list search to specific
        $('body').on('click', '.btn-to-individual', function() {
            $(this).removeClass('btn-detault').addClass('btn-primary');
            $('body').find('.btn-to-all').removeClass('btn-primary').addClass('btn-default');
            var toAll = $('.to-individual').val();
            if (parseInt(toAll) === 0) {
                $('.to-all').val(0);
                $('.to-individual').val(1);
            } else {
                $('.to-all').val(1);
                $('.to-individual').val(0);
            }
            messagesSearch(listUrlSearch);
        });

        $('body').on('click', '.btn-search', function() {
            messagesSearch(listUrlSearch);
        });

        $('body').on('click', '.btn-suspend-msg', function() {
            var id = $(this).data('id');
            var data = $('#form-message-status-' + id).serialize();
            var suspendUrl = '<?= $this->Url->build(['controller' => 'Messages', 'action' => 'suspend']) ?>';
            suspendMessage(id, suspendUrl, data);
        });

        // show modal when create new group
        $('body').on('click', '.btn-add', function(e) {
            var target = $(this).attr('data-target');
            if (target === 'edit') {
                var id = $(this).closest('tr').attr('data-id');
                $.LoadingOverlay('show');
                $.get('<?php echo $this->Url->build('/groups/get-by-id/'); ?>', {id: id}, function(data) {
                    $.LoadingOverlay('hide');
                    if (data !== null && data !== undefined) {
                        $('#modal_group').find('#name').val(data.data.name);
                        $('#modal_group').find('#type').val(data.data.type);
                        $('#modal_group').find('button[type="submit"]').attr({
                            'data-id': data.data.id,
                            'data-target': 'edit'
                        }).text('<?= __('TXT_UPDATE') ?>');
                    }
                }, 'json');
            }
            $('#modal_group').modal('show');
        });

        // register new group
        $('body').on('click', '.btn-group-add', function(e) {
            $.LoadingOverlay('show');
            var target = $(this).attr('data-target');
            var form_data = $('form[name="form_group"]').serialize();
            var url = '<?php echo $this->Url->build('/groups/create/'); ?>';
            if (target === 'edit') {
                url = '<?php echo $this->Url->build('/groups/edit/'); ?>' + $(this).attr('data-id');
            }
            $.post(url, form_data, function(data) {
                $.LoadingOverlay('hide');
                var response = data.data;
                var form = $('form[name="form_group"]');
                $(form).find('label[class="error-message"]').remove();
                $(form).removeClass('has-error');

                if (data.status == 0) {
                    $.each(response, function(i, v) {
                        var content = $(form).find('[name="' + i + '"]').closest('.form-group');
                        var message = '<label class="error-message">' + v._empty + '</label>';
                        $(content).find('div').addClass('has-error').append(message);
                    });
                } else {
                    window.location.href = '<?php echo $this->Url->build('/customer-groups/get-by-group-id/'); ?>' + data.data.id;
                }
            }, 'json');
        });

        // show suspend modal
        $('body').on('click', '.btn_suspend', function (e) {
            e.stopPropagation();
            var content = $('#modal_suspend');
            var status = $(this).attr('data-name');
            var txt = '';

            switch (status) {
                case 'suspend':
                    $('#btn_suspend_yes').text('<?= __('BTN_SUSPEND') ?>');
                    txt = '<?= __('MSG_CONFIRM_SUSPEND') ?>';
                    break;
                case 'delete':
                    txt = '<?= __('MSG_CONFIRM_DELETE') ?>';
                    $('#btn_suspend_yes').text('<?= __('BTN_DELETE') ?>');
                    break;
                 case 'recover':
                    $('#btn_suspend_yes').text('<?= __('BTN_RECOVER') ?>');
                    txt = '<?= __('MSG_CONFIRM_UNDO_SUSPEND') ?>';
                    break;
            };

            $(content).find('#btn_suspend_yes').data('id', $(this).closest('tr').data('id'));
            $(content).find('#btn_suspend_yes').data('target', $(this).closest('td').data('target'));
            $(content).find('.modal-body p').html(txt);
            $(content).modal('show');
        });

        // confirm suspend message
        $('body').on('click', '#btn_suspend_yes', function (e) {
            var params = {
                id: $(this).data('id'),
                is_suspend: $(this).data('target')
            };
            $.post('<?php echo $this->Url->build('/groups/update-suspend/'); ?>', params, function (data) {
                if (data.message === 'success') {
                    location.reload();
                }
            }, 'json');
        });

        // show delete modal
        $('body').on('click', '#btn_delete', function (e) {
            e.stopPropagation();
            var content = $('#modal_delete');
            $(content).find('#btn_delete_yes').attr('data-id', $(this).closest('tr').data('id'));
            $(content).modal('show');
        });

        // cofirm delete message
        $('body').on('click', '#btn_delete_yes', function (e) {
            $.post('<?php echo $this->Url->build('/groups/delete/'); ?>', {id: $(this).data('id')}, function (data) {
                if (data.message === 'success') {
                    location.reload();
                }
            }, 'json');
        });

        // reset data in modal after close
        $('.modal').on('hidden.bs.modal', function (e) {
            var form = $('form[name="form_group"]');
            $(form).find('label[class="error-message"]').remove();
            $(form).find('.form-group div').removeClass('has-error');
            $(form).find('input[type="text"]').val('');
            $(form).find('select').val('');
            $(form).find('button[type="submit"]').attr({
                'data-id': 0,
                'data-target': 'register'
            }).text('<?= __('TXT_REGISTER_SAVE') ?>');
        });

        pageOutOfRange(<?= $paging ?>, '<?= $this->request->query('page') ?>');
        var maxPage = parseInt($('#maxPage').val());
        updatePagination(maxPage);
    })();
</script>
