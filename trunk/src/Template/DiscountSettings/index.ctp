<?php $this->assign('title', 'Discount Settings'); ?>
<div class="row">
    <div class="col-md-12">
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>#</th>
                    <th>&nbsp;</th>
                    <th><?= __('Discount Condition') ?></th>
                    <th><?= __('Discount Type') ?></th>
                    <th><?= __('Discount Target') ?></th>
                    <th><?= __('Range Type') ?></th>
                    <th><?= __('From') ?></th>
                    <th><?= __('To') ?></th>
                    <th><?= __('Value') ?></th>
                    <th><?= __('Currency Type') ?></th>
                    <th width="8%">&nbsp;</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>1</td>
                    <td>&nbsp;</td>
                    <td>PO</td>
                    <td>Discount Rate</td>
                    <td>&nbsp;</td>
                    <td>Amount</td>
                    <td>1,000</td>
                    <td>2,000</td>
                    <td>5%</td>
                    <td>&nbsp;</td>
                    <td>
                        <?= $this->Html->link(BTN_ICON_EDIT, ['controller' => 'discount_settings', 'action' => 'edit', 1], ['class' => 'btn btn-sm btn-primary', 'role' => 'button', 'escape' => false]) ?>
                        <?= $this->Form->button(BTN_ICON_STOP, ['class' => 'btn btn-suspend btn-sm btn-stop-condition', 'escape' => false, 'data-toggle' => 'popover']) ?>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>2</td>
                    <td>&nbsp;</td>
                    <td>Brand</td>
                    <td><label class="foc-label" data-toggle="popover" data-trigger="hover">View FOC</label></td>
                    <td>Manufacturer Brand A</td>
                    <td>Quantity</td>
                    <td>21</td>
                    <td>30</td>
                    <td>100</td>
                    <td>USD</td>
                    <td>
                        <?= $this->Html->link(BTN_ICON_EDIT, ['controller' => 'discount_settings', 'action' => 'edit', 1], ['class' => 'btn btn-sm btn-primary', 'role' => 'button', 'escape' => false]) ?>
                        <?= $this->Form->button(BTN_ICON_STOP, ['class' => 'btn btn-suspend btn-sm btn-stop-condition', 'data-toggle' => 'popover', 'escape' => false]) ?>
                    </td>
                </tr>
                <tr>
                    <td>3</td>
                    <td>
                        <label class="stop-label">Stopped</label>
                    </td>
                    <td>Brand</td>
                    <td>Discount Rate</td>
                    <td>Manufacturer Brand B</td>
                    <td>Amount</td>
                    <td>41</td>
                    <td>50</td>
                    <td>20%</td>
                    <td>&nbsp;</td>
                    <td>
                        <?= $this->Form->button(BTN_ICON_UNDO, ['class' => 'btn btn-recover btn-sm btn-toggle-close btn-restore-condition', 'data-toggle' => 'popover']) ?>
                        <?= $this->Form->button(BTN_ICON_DELETE, ['class' => 'btn btn-delete btn-sm btn-toggle-close btn-remove-condition', 'data-toggle' => 'popover']) ?>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</div>

<script>
    $(function() {
        var msg = '<?= __('Are you sure to stop this condition?') ?>';
        confirm_yesno('btn-stop-condition', msg, 'left', 'Stop item?');

        var msg = '<?= __('Are you sure to restore this condition?') ?>';
        confirm_yesno('btn-restore-condition', msg, 'left', 'Stop item?');

        var msg = '<?= __('Are you sure to physically delete this condition?') ?>';
        confirm_yesno('btn-remove-condition', msg, 'left', 'Stop item?');

        $('body').on('click', '.confirm-no', function() {
            $(this).parents('.popover').prev('button').click();
        });

        $('body').on('click', '.confirm-yes', function() {

        });

        var data = '<table class="table">';
        data += '<thead><tr><th>Product Brand</th><th>Product</th><th>Type</th><th>Value</th></tr></thead>';
        data += '<tbody><tr><td>Brand A</td><td>Product A (Packaging)</td><td>Fixed Quantity</td><td>100</td></tr>';
        data += '<tr><td>Brand A</td><td>Product A (Packaging)</td><td>Fixed Quantity</td><td>100</td></tr></tbody>';
        data += '</table>';
        var tmp = '<div class="popover" role="tooltip"><div class="arrow"></div><h3 class="popover-title"></h3>';
        tmp += '<div class="popover-content"></div></div>';
        var options = {
            placement: 'right',
            title: '<?= __('FOC Product List') ?>',
            html: true,
            content: data,
            container: 'body',
            template: tmp
        };
        $('.foc-label').popover(options);
    });
</script>
