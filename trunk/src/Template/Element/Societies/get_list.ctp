<?php if ($societies): ?>
    <?php foreach ($societies as $key => $value): ?>
        <tr data-id="<?= $value->id ?>">
            <td>
                <?php
                $checked = false;
                if (in_array($value->id, $ids)) {
                    $checked = true;
                }
                echo $this->Form->checkbox('check_item', [
                    'hiddenField' => false,
                    'value' => $value->id,
                    'data-name' => $value->name,
                    'checked' => $checked,
                ]);
                ?>
            </td>
            <td><?= h($key +1) ?></td>
            <td><?= h($value->name) ?></td>
            <td><?= h($value->name_en) ?></td>
        </tr>
    <?php endforeach; ?>
<?php endif; ?>