<?php
$col = 9;
if (isset($column)) {
    $col = $column;
}
?>
<div class="col-md-<?= $col ?>">
    <p class="page-info">
        <?php
        // $page = $this->Paginator->counter('{{page}}');
        // $pages = $this->Paginator->counter('{{pages}}');
        // $current = $this->Paginator->counter('{{current}}');
        // $count = $this->Paginator->counter('{{count}}');
        // echo __('PGN_PAGE {0} PGN_OF {1}: {2} PGN_REGCORD_OF {3}', [
        //     $page, $pages, $current, $count
        // ]);

        echo $this->element('display_info');
        ?>
    </p>
    <nav class="pull-right">
        <ul class="pagination pagination-sm custom-pagination">
            <?php
                $custom_url = $this->Paginator->generateUrl([]);
                $page = '';
                if (empty($this->request->query('page'))) {
                    $page = '?page=1';
                }
                echo $this->Paginator->first(__('TXT_PAGE_FIRST'));
                echo $this->Paginator->prev('<span class="glyphicon glyphicon-chevron-left"></span>', ['escape' => false]);
                
                echo '<li class="prev">';
                    echo '<a href="'.$this->request->here().$page.'" id="prevMore" rel="prev">'.__('BTN_PREV_FIVE').'</a>';
                echo '</li>';
                echo $this->Paginator->numbers();
                echo '<li class="next">';
                    echo '<a href="'.$this->request->here().$page.'" id="nextMore" rel="next">'.__('BTN_NEXT_FIVE').'</a>';
                echo '</li>';
                echo $this->Paginator->next('<span class="glyphicon glyphicon-chevron-right"></span>', ['escape' => false]);
                echo $this->Paginator->last(__('TXT_PAGE_LAST'));
            ?>
        </ul>
        <?php echo $this->Form->hidden('maxPage', [
            'id' => 'maxPage',
            'value' => $this->Paginator->counter('{{pages}}'),
        ]); ?>
        <?php echo $this->Form->hidden('current', [
            'id' => 'current',
            'value' => $this->Paginator->counter('{{page}}'),
        ]); ?>
    </nav>
</div>