<div class="container">
    <div class="row">
        <div class="col-sm-10 col-sm-offset-1">
    <div class="form-group">
        <label class="col-sm-2 control-label">
            User Group ID
        </label>
        <div class="col-lg-10 col-md-10 col-sm-10">
            <?= $this->Form->text('user_group_id', [
                'class' => 'form-control',
                'placeholder' => 'Enter User Group ID'
            ]); ?>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label">
            Name JP
        </label>
        <div class="col-lg-10 col-md-10 col-sm-10">
            <?= $this->Form->text('name', [
                'class' => 'form-control',
                'placeholder' => 'Enter Name JP',
            ]); ?>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label">
            Name En
        </label>
        <div class="col-lg-10 col-md-10 col-sm-10">
            <?= $this->Form->text('name_en', [
                'class' => 'form-control',
                'placeholder' => 'Enter Name En',
            ]); ?>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label">
            Controller Name
        </label>
        <div class="col-lg-10 col-md-10 col-sm-10">
            <?= $this->Form->text('controller', [
                'class' => 'form-control',
                'placeholder' => 'Enter Controller Name',
            ]); ?>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label">
            Action Name
        </label>
        <div class="col-lg-10 col-md-10 col-sm-10">
            <?= $this->Form->text('action', [
                'class' => 'form-control',
                'placeholder' => 'Enter Action Name',
            ]); ?>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label">
            URL
        </label>
        <div class="col-lg-10 col-md-10 col-sm-10">
            <?= $this->Form->text('url', [
                'class' => 'form-control',
                'placeholder' => 'Enter URL',
            ]); ?>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label">
            Params
        </label>
        <div class="col-lg-10 col-md-10 col-sm-10">
            <?= $this->Form->text('param', [
                'class' => 'form-control',
                'placeholder' => 'Enter Params',
            ]); ?>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label"></label>
        <div class="col-lg-10 col-md-10 col-sm-10">
            <?php
            echo $this->Form->button('Submit', [
                'class' => 'btn btn-primary pull-right',
            ]); ?>
        </div>
    </div>
<?php
echo $this->Form->end(); ?>
        </div>
    </div>
</div>
