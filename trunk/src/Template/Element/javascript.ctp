
<script>
    // CONSTANT OF LINK IN JAVASCRIPT
    var BASE = '<?php echo $this->request->webroot; ?>';
    var CONTROLLER = '<?php echo $this->request->controller; ?>';
    var ACTION = '<?php echo $this->request->action; ?>';
    var LOCALE = '<?php echo $this->request->session()->read('tb_field'); ?>';
    var LOCALE_EN = '<?php echo LOCAL_EN; ?>';
    $.ajaxSetup({
        beforeSend: function(xhr) {
            xhr.setRequestHeader('X-CSRF-Token', <?php echo json_encode($this->request->param('_csrfToken')); ?>);
        }
    });
    // MESSAGES DISPLAY
    var SESSION_TIMEOUT = '<?php echo  __('Session timeout. Please login again.'); ?>';
    var MSG_REQUIRED = '<?php echo __('TXT_MESSAGE_REQUIRED'); ?>';
    var MSG_ERROR = '<?php echo MSG_ERROR; ?>';
    var MSG_SUCCESS = '<?php echo MSG_SUCCESS; ?>';
    var TARGET_NEW = '<?php echo TARGET_NEW; ?>';
    var TARGET_EDIT = '<?php echo TARGET_EDIT; ?>';

    // MESSAGE SUSPEND TYPE
    var MSG_CONFIRM_SUSPEND = '<?php echo __('MSG_CONFIRM_SUSPEND') ?>';
    var MSG_CONFIRM_DELETE = '<?php echo __('MSG_CONFIRM_DELETE'); ?>';
    var MSG_CONFIRM_RECOVER = '<?php echo __('MSG_CONFIRM_UNDO_SUSPEND'); ?>';

    // STATUS TYPE
    var ACTIVE_STATUS = '<?php echo STATUS_ACTIVE; ?>';
    var INACTIVE_STATUS = '<?php echo STATUS_INACTIVE; ?>';

    // BUTTON NAME
    var BUTTON_SUSPEND = '<?php echo __('BTN_SUSPEND') ?>';
    var BUTTON_DELETE = '<?php echo __('BTN_DELETE') ?>';
    var BUTTON_RECOVERY = '<?php echo __('BTN_RECOVER') ?>';

    // CONTROLLER TYPE
    var MANUFACTURER_TYPE = '<?php echo TYPE_MANUFACTURER ?>';
    var DOCTOR_TYPE = '<?php echo TYPE_DOCTOR ?>';
    var PERSON_IN_CHARGE_TYPE = '<?php echo TYPE_PERSON_IN_CHARGE ?>';
    var CLINIC_PRICE_TYPE = '<?php echo CLINIC_PRICE_TYPE; ?>';
    var PERCENT_TYPE = '<?php echo TYPE_PERCENT; ?>';
    var SUPPLIER = '<?php echo TYPE_SUPPLIER ?>';
    var SUBSIDIARY = '<?php echo TYPE_SUBSIDIARY ?>';
    var CUSTOMER = '<?php echo TYPE_CUSTOMER ?>';
    var PRODUCT_PACKAGE = '<?php echo TYPE_PACKAGE ?>'
    var TYPE_STOCK = '<?php echo TYPE_STOCK; ?>';
    var TYPE_WISTERIA = '<?php echo TYPE_WISTERIA; ?>';

    // MESSAGE TYPE
    var MESSAGE_TYPE = '<?php echo MESSAGE_TYPE; ?>';
    var RECEIVER_TYPE = '<?php echo RECEIVER_TYPE; ?>';

    // FIELD AND PLACEHOLDER
    var TXT_ENTER_EMAIL = '<?php echo __('TXT_ENTER_EMAIL'); ?>';
    var TXT_ENTER_TEL = '<?php echo __('TXT_ENTER_TEL'); ?>';
    var TXT_ENTER_PHONE = '<?php echo __('TXT_ENTER_PHONE'); ?>';
    var TXT_SELECT_PRODUCT_BRAND = '<?php echo __('TXT_SELECT_BRAND_NAME'); ?>';
    var TXT_SELECT_PRODUCT_NAME = '<?php echo __('TXT_SELECT_PRODUCT_NAME'); ?>';
    var TXT_UPDATE = '<?php echo __('TXT_UPDATE'); ?>';
    var TXT_ADD = '<?php echo __('TXT_ADD'); ?>';
    var TXT_SELECT_TYPE = '<?php echo __('TXT_SELECT_TYPE') ?>';
    var STR_TEL_EXT = '<?php echo __('STR_TEL_EXT'); ?>';
    var SELECT_PRODUCT_BRAND = '<?php echo __('SELLER_PRODUCT_SELECT_BRAND'); ?>';

    // TYPE
    var AMOUNT_TYPE = '<?php echo TYPE_AMOUNT; ?>';
    var QUANTITY_TYPE = '<?php echo TYPE_QUANTITY; ?>';

    // RULE VALIDATION
    var VALIDATE_IS_REQUIRED = '_required';
    var VALIDATE_IS_EMPTY = '_empty';
    var VALIDATE_IS_VALID = 'valid';
    var VALIDATE_IS_URL = 'urlWithProtocol';
</script>
