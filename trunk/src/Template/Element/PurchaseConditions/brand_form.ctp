
<section id="register_form">
    <?php
    echo $this->Form->create($data, [
        'role' => 'form',
        'class' => 'form-horizontal',
        'name' => 'brand_form',
    ]);
    if (!$data->isNew()) {
        echo $this->Form->hidden('id');
    }
    echo $this->Form->hidden('type', ['value' => TYPE_SELLER_BRAND]);
    ?>
    <div class="form-group">
        <div class="col-md-6 col-lg-6 col-sm-6">
            <label class="col-sm-4 control-label">
                <?php
                echo __('TXT_SUPPLIER') . $this->Comment->formAsterisk(); ?>
            </label>
            <div class="col-sm-8 custom-select">
                <?php
                $seller = [];
                if (!$data->isNew()) {
                    $seller_name = '';
                    switch ($data->seller_brand->seller->type) {
                        case TYPE_MANUFACTURER:
                            $seller_name = $this->Comment->getFieldByLocal($data->seller_brand->seller->manufacturer, $locale);
                            break;

                        default:
                            $seller_name = $this->Comment->getFieldByLocal($data->seller_brand->seller->supplier, $locale);
                    }
                    $seller = [
                        $data->seller_brand->seller_id => $seller_name,
                    ];
                }
                echo $this->Form->select('seller_id', $seller, [
                    'class' => 'form-control',
                    'label' => false,
                    'id' => 'seller-id',
                    'required' => false,
                    'empty' => ['' => __('PURCHASE_BRAND_SELECT_SUPPLIER')],
                    'value' => !$data->isNew() ? $data->seller_brand->seller_id : '',
                ]);
                ?>
            </div>
        </div>
    </div>
    <div class="form-group">
        <div class="col-md-6 col-lg-6 col-sm-6">
            <label class="col-sm-4 control-label">
                <?php
                echo __('PRODUCTS_TXT_PRODUCT_BRAND') . $this->Comment->formAsterisk(); ?>
            </label>
            <div class="col-sm-8 custom-select">
                <?php
                $product_brand = [];
                if (!$data->isNew()) {
                    $product_brand = [
                        $data->seller_brand->product_brand_id => $this->Comment->getFieldByLocal($data->seller_brand->product_brand, $locale),
                    ];
                }
                echo $this->Form->select('product_brand_id', $product_brand, [
                    'class' => 'form-control',
                    'label' => false,
                    'required' => false,
                    'id' => 'product-brand-id',
                    'empty' => ['' => __('PURCHASE_BRAND_SELECT_PRODUCT_BRAND')],
                    'value' => !$data->isNew() ? $data->seller_brand->product_brand_id : '',
                ]); ?>
            </div>
        </div>
    </div>
    <div class="form-group">
        <div class="col-md-6 col-lg-6 col-sm-6">
            <label class="col-sm-4 control-label">
                <?php
                echo __('PURCHASE_BRAND_CURRENCY') . $this->Comment->formAsterisk(); ?>
            </label>
            <div class="col-sm-8 custom-select">
                <?php
                echo $this->Form->select('currency_id', $currencies, [
                    'class' => 'form-control',
                    'label' => false,
                    'required' => false,
                    'empty' => ['' => __('PURCHASE_BRAND_SELECT_CURRENCY')],
                ]); ?>
            </div>
        </div>
    </div>
    <div class="form-group">
        <div class="col-lg-6 col-md-6 col-sm-6">
            <label class="col-sm-4 control-label">
                <?php
                echo __('PURCHASE_BRAND_APPLY_MOQ') ?>
            </label>
            <div class="col-sm-8">
                <?php
                $is_moq = !$data->isNew() ? $data->is_moq : 0;
                echo $this->Form->checkbox('is_moq', [
                    'checked' => $is_moq ? true : false,
                    'hiddenField' => false,
                ]);
                ?>
            </div>
        </div>
    </div>
    <div class="form-group">
        <div class="col-md-6 col-lg-6 col-sm-6">
            <label class="col-sm-4 control-label">
                <?php
                echo __('TXT_MIN_QUANTITY') ?>
            </label>
            <div class="col-sm-8">
                <?php
                echo $this->Form->text('min_quantity', [
                    'class' => 'form-control',
                    'label' => false,
                    'required' => false,
                    'placeholder' => __('TXT_MIN_QUANTITY'),
                ]); ?>
            </div>
        </div>
    </div>
    <div class="form-group">
        <div class="col-lg-6 col-md-6 col-sm-6">
            <label class="col-sm-4 control-label">
                <?php
                echo __('PURCHASE_BRAND_APPLY_MOA') ?>
            </label>
            <div class="col-sm-8">
                <?php
                $is_moa = !$data->isNew() ? $data->is_moa : 0;
                echo $this->Form->checkbox('is_moa', [
                    'checked' => $is_moa ? true : false,
                    'hiddenField' => false,
                ]); ?>
            </div>
        </div>
    </div>
    <div class="form-group">
        <div class="col-md-6 col-lg-6 col-sm-6">
            <label class="col-sm-4 control-label">
                <?php
                echo __('TXT_MIN_AMOUNT') ?>
            </label>
            <div class="col-sm-8">
                <?php
                echo $this->Form->text('min_amount', [
                    'class' => 'form-control',
                    'label' => false,
                    'required' => false,
                    'placeholder' => __('TXT_MIN_AMOUNT'),
                ]); ?>
            </div>
        </div>
    </div>
    <div class="form-group">
        <div class="col-lg-12 col-sm-12 col-md-12 text-center">
            <?php
            echo $this->Form->button(__('TXT_CANCEL'), [
                'type' => 'button',
                'class' => 'btn btn-default form_btn btn-sm btn-width',
                'id' => 'btn-cancel',
                'onclick' => 'goBack()',
            ]); ?>
            &nbsp;&nbsp;
            <?php
            echo $this->Form->button(($this->request->action === 'editBrand') ? __('TXT_UPDATE') : __('TXT_REGISTER_SAVE'), [
                'type' => 'button',
                'class' => 'btn btn-primary form_btn btn-sm btn-width btn-register',
            ]); ?>
            <div class="clearfix"></div>
        </div>
    </div>
    <?php
    echo $this->Form->end(); ?>
</section>

<?php
echo $this->Html->script([
    'purchase-condition/brand',
], ['blog' => false]);
