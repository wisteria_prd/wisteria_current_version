
<?php
$action = $this->request->action;
$destination = $this->request->query('des') ? $this->request->query('des') : '';
?>
<div class="row">
    <div class="col-sm-10 col-sm-offset-1">
        <section id="create_form">
            <?php
            echo $this->Form->create($personInCharge, [
                'role' => 'form',
                'class' => 'form-horizontal',
                'name' => 'person_in_charge_form',
                'autocomplete' => 'off',
                'onsubmit' => 'return false;',
            ]);
            echo $this->Form->hidden('id');
            ?>
            <meta id="filter" content="<?php echo $this->request->query('filter_by') ?>"/>
            <meta id="type" content="<?php echo $this->request->query('des') ?>"/>
            <div class="form-group custom-select">
                <label class="col-sm-2 control-label">
                    <?php echo __('PERSON_IN_CHARGES_TXT_ASSOCIATE_TYPE') . $this->Comment->formAsterisk(); ?>
                </label>
                <div class="col-md-4 col-lg-4 col-sm-4">
                    <?php
                    echo $this->Form->select('type', array_map('__', unserialize(ENTITY_TYPE)), [
                        'class' => 'form-control',
                        'default' => $this->request->query('filter_by'),
                        'empty' => ['' => __('TXT_ASSOCIATION_TYPE')],
                    ]);
                    ?>
                </div>
                <div class="col-md-4 col-lg-4 col-sm-4">
                    <?php
                    echo $this->Form->select('external_id', isset($ext_data) ? $ext_data : '', [
                        'class' => 'form-control',
                        'empty' => ['' => __('TXT_ASSOCIATION')],
                    ]);
                    ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">
                    <?php echo __('PERSON_IN_CHARGES_TXT_NAME') ?>
                </label>
                <div class="col-md-4 col-lg-4 col-sm-4">
                    <?php
                    echo $this->Form->text('last_name_en', [
                        'class' => 'form-control',
                        'placeholder' => __('PERSON_IN_CHARGES_TXT_ENTER_LAST_NAME_EN'),
                    ]);
                    ?>
                </div>
                <div class="col-md-4 col-lg-4 col-sm-4">
                    <?php
                    echo $this->Form->text('last_name', [
                        'class' => 'form-control',
                        'placeholder' => __('PERSON_IN_CHARGES_TXT_ENTER_LAST_NAME_JP'),
                    ]);
                    ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">
                    <?php // echo __('STR_FIRSTNAME') ?>
                </label>
                <div class="col-md-4 col-lg-4 col-sm-4">
                    <?php
                    echo $this->Form->text('first_name_en', [
                        'class' => 'form-control',
                        'placeholder' => __('PERSON_IN_CHARGES_TXT_ENTER_FIRST_NAME_EN'),
                    ]);
                    ?>
                </div>
                <div class="col-md-4 col-lg-4 col-sm-4">
                    <?php
                    echo $this->Form->text('first_name', [
                        'class' => 'form-control',
                        'placeholder' => __('PERSON_IN_CHARGES_TXT_ENTER_FIRST_NAME_JP'),
                    ]);
                    ?>
                </div>
            </div>
            <div class="form-group custom-select">
                <label class="col-sm-2 control-label">
                    <?php echo __('PERSON_IN_CHARGES_TXT_GENDER') . $this->Comment->formAsterisk() ?>
                </label>
                <div class="col-md-4 col-lg-4 col-sm-4">
                    <?php
                    echo $this->Form->select('gender', unserialize(GENDER), [
                        'class' => 'form-control',
                        'empty' => ['' => __('TXT_SELECT_GENDER')]
                    ]);
                    ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">
                    <?php echo __('PERSON_IN_CHARGES_TXT_DEPARTMENT') ?>
                </label>
                <div class="col-md-4 col-lg-4 col-sm-4">
                    <?php
                    echo $this->Form->text('department_en', [
                        'class' => 'form-control',
                        'placeholder' => __('TXT_ENTER_DEPARTMENT_EN'),
                    ]);
                    ?>
                </div>
                <div class="col-md-4 col-lg-4 col-sm-4">
                    <?php
                    echo $this->Form->text('department', [
                        'class' => 'form-control',
                        'placeholder' => __('TXT_ENTER_DEPARTMENT'),
                    ]);
                    ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">
                    <?php echo __('PERSON_IN_CHARGES_TXT_DESIGNATION') ?>
                </label>
                <div class="col-md-4 col-lg-4 col-sm-4">
                    <?php echo $this->Form->text('position_en', [
                        'class' => 'form-control',
                        'placeholder' => __('TXT_ENTER_DESIGNATION_EN'),
                    ]); ?>
                </div>
                <div class="col-md-4 col-lg-4 col-sm-4">
                    <?php echo $this->Form->text('position', [
                        'class' => 'form-control',
                        'placeholder' => __('TXT_ENTER_DESIGNATION'),
                    ]); ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">
                    <?php echo __('PERSON_IN_CHARGES_TXT_TEL') ?>
                </label>
                <div class="col-md-4 col-lg-4 col-sm-4">
                    <?php
                    $tel = '';
                    $tel_extension = '';
                    if ($personInCharge->info_details) {
                        $tel = $personInCharge->info_details[0]->tel;
                        $tel_extension = $personInCharge->info_details[0]->tel_extension;
                    }
                    echo $this->Form->tel('info_details[0].tel', [
                        'class' => 'form-control multiple-tel',
                        'placeholder' => __('TXT_ENTER_TEL'),
                        'value' => $tel,
                    ]);
                    echo $this->Form->hidden('info_details[0].type', [
                        'value' => TYPE_PERSON_IN_CHARGE,
                    ]);
                    ?>
                </div>
                <div class="col-md-4 col-lg-4 col-sm-4">
                    <?php
                    echo $this->Form->text('info_details[0].tel_extension', [
                        'class' => 'form-control',
                        'placeholder' => __('STR_TEL_EXT'),
                        'value' => $tel_extension,
                    ]);
                    ?>
                </div>
                <div class="col-sm-1">
                    <?php
                    echo $this->Form->button('<i class="fa fa-plus" aria-hidden="true"></i>', [
                        'type' => 'button',
                        'class' => 'btn btn-sm btn-primary btn-tel-add form-btn-add',
                        'escape' => false,
                    ]);
                    ?>
                </div>
            </div>
            <div class="tel-box-wrap">
                <?php echo $this->element('PersonInCharges/tel_list'); ?>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">
                    <?php echo __('PERSON_IN_CHARGES_TXT_EMAIL') ?>
                </label>
                <div class="col-lg-3 col-md-3 col-sm-3 c-lg">
                    <?php
                    $email = '';
                    if ($personInCharge->info_details) {
                        $email = $personInCharge->info_details[0]->email;
                    }
                    echo $this->Form->email('info_details[0].email', [
                        'class' => 'form-control',
                        'placeholder' => __('STR_ENTER_EMAIL'),
                        'value' => $email,
                    ]);
                    echo $this->Form->hidden('info_details[0].type', [
                        'value' => TYPE_PERSON_IN_CHARGE,
                    ]);
                    ?>
                </div>
                <div class="col-sm-1">
                    <?php
                    echo $this->Form->button('<i class="fa fa-plus" aria-hidden="true"></i>', [
                        'type' => 'button',
                        'class' => 'btn btn-sm btn-primary btn-email form-btn-add',
                        'escape' => false,
                    ]);
                    ?>
                </div>
            </div>
            <div class="email-box-wrap">
                 <?php echo $this->element('PersonInCharges/email_list'); ?>
            </div>
            <?php
            if (false) : ?>
            <div class="form-group">
                <label class="col-sm-2 control-label">
                    <?php echo __('STR_TEL_EXT') ?>
                </label>
                <div class="col-md-4 col-lg-4 col-sm-4">
                    <?php
                    echo $this->Form->text('tel_extension', [
                        'class' => 'form-control',
                        'placeholder' => __('STR_TEL_EXT'),
                    ]);
                    ?>
                </div>
            </div>
            <?php
            endif; ?>
            <div class="form-group">
                <label class="col-sm-2 control-label">
                    <?php echo __('STR_USER_REMARK') ?>
                </label>
                <div class="col-lg-8 col-md-8 col-sm-8">
                    <?php
                    echo $this->Form->textarea('remark', [
                        'class' => 'form-control',
                        'placeholder' => __('PERSON_IN_CHARGES_TXT_REMARKS'),
                    ]); ?>
                </div>
            </div>
            <div class="form-group">
                <?php //pr($action) ?>
                <div class="col-lg-12 col-sm-12 col-md-12 text-center">
                    <a href="#" onClick="window.history.back(1)" class="btn btn-default btn-sm"><?php echo __('TXT_CANCEL') ?></a>
                    &nbsp;&nbsp;
                    <?php echo $this->Form->button(($action === 'edit') ? __('TXT_UPDATE') : __('TXT_REGISTER_SAVE'), [
                        'type' => 'button',
                        'class' => 'btn btn-primary form_btn btn-sm',
                        'id' => 'btn-register',
                    ]); ?>
                    <div class="clearfix"></div>
                </div>
                <?php //echo $this->ActionButtons->btnSaveCancel('PersonInCharges', $action); ?>
            </div>
            <?php echo $this->Form->end(); ?>
        </section>
    </div>
</div>

<?php
echo $this->Html->css([
    'intlTelInput',
]);
echo $this->Html->script([
    'intlTelInput.min',
    'utils',
    'person-in-charges/person',
], ['block' => 'script']);
