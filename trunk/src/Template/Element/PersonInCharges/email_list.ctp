<?php if (isset($personInCharge) && $personInCharge->info_details): ?>
    <?php foreach ($personInCharge->info_details as $key => $value): ?>
        <?php if (($value->email !== null) && !empty($value->email) && ($key != 0)): ?>
            <div class="form-group">
                <label class="col-sm-2 control-label"></label>
                <div class="col-lg-3 col-sm-3 col-md-3 c-lg">
                    <?php
                    echo $this->Form->email('info_details[' . $key . '].email', [
                        'class' => 'form-control',
                        'placeholder' => __('TXT_ENTER_EMAIL'),
                        'default' => $value->email
                    ]);
                    echo $this->Form->hidden('info_details[' . $key . '].type', [
                        'value' => TYPE_PERSON_IN_CHARGE,
                    ]);
                    ?>
                </div>
                <div class="col-sm-1">
                    <?php
                    echo $this->Form->button('<i class="fa fa-trash" aria-hidden="true"></i>', [
                        'type' => 'button',
                        'class' => ' btn btn-delete btn-sm form-btn-add btn-remove-field',
                        'escape' => false,
                    ]);
                    ?>
                </div>
            </div>
        <?php endif; ?>
    <?php endforeach; ?>
<?php endif; ?>
