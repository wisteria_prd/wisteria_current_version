
<div class="col-sm-12">
    <section id="create_form">
        <?php
        echo $this->Form->create($data, [
            'role' => 'form',
            'class' => 'form-horizontal',
            'name' => 'pricing_form',
        ]);
        echo $this->Form->hidden('id');
        ?>
        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-md-6">
                    <div class="form-group custom-select">
                        <label class="col-sm-3 control-label">
                            <?php
                            echo __d('pricing', 'TXT_ASSOCIATE') . $this->Comment->formAsterisk(); ?>
                        </label>
                        <div class="col-sm-9">
                            <?php
                            echo $this->Form->select('external_id_type', [
                                PRICE_TYPE_SALE => __d('pricing', 'PRICE_TYPE_SALE'),
                                PRICE_TYPE_PURCHASE =>  __d('pricing', 'PRICE_TYPE_PURCHASE'),
                                PRICE_TYPE_CLINIC => __d('pricing', 'PRICE_TYPE_CLINIC'),
                            ], [
                                'class' => 'form-control',
                                'label' => false,
                                'required' => false,
                                'id' => 'external-id-type',
                                'empty' => ['' => __d('pricing', 'TXT_SELECT_ASSOCIATE')],
                            ]); ?>
                        </div>
                    </div>
                    <div class="form-group custom-select <?php echo $customer ? '' : 'hidden';?>">
                        <label class="col-sm-3 control-label">
                            <?php
                            echo __d('pricing', 'TXT_CUSTOMER') . $this->Comment->formAsterisk(); ?>
                        </label>
                        <div class="col-sm-9">
                            <?php
                            $customerData = [];
                            if (!$data->isNew() && $customer) {
                                $customerData = [
                                    $customer->id => $this->Comment->getFieldByLocal($customer, $locale),
                                ];
                            }
                            echo $this->Form->select('customer_id', $customerData, [
                                'class' => 'form-control',
                                'label' => false,
                                'required' => false,
                                'id' => 'customer-id',
                                'empty' => ['' => __d('pricing', 'TXT_SELECT_CUSTOMER')],
                                'value' => $customer ? $data->external_id : '',
                            ]); ?>
                        </div>
                    </div>
                    <div class="form-group custom-select">
                        <label class="col-sm-3 control-label">
                            <?php
                            echo __d('pricing', 'TXT_SELLER') . $this->Comment->formAsterisk(); ?>
                        </label>
                        <div class="col-sm-9">
                            <?php
                            $seller = [];
                            if (!$data->isNew()) {
                                $seller_name = '';
                                switch ($data->seller->type) {
                                    case TYPE_MANUFACTURER:
                                        $seller_name = $this->Comment->getFieldByLocal($data->seller->manufacturer, $locale);
                                        break;

                                    default:
                                    $seller_name = $this->Comment->getFieldByLocal($data->seller->supplier, $locale);
                                }
                                $seller = [
                                    $data->seller_id => $seller_name,
                                ];
                            }
                            echo $this->Form->select('seller_id', $seller, [
                                'class' => 'form-control',
                                'label' => false,
                                'required' => false,
                                'id' => 'seller-id',
                                'empty' => ['' => __d('pricing', 'TXT_SELECT_SELLER')],
                            ]); ?>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-md-6">
                    <div class="form-group custom-select">
                        <label class="col-sm-3 control-label">
                            <?php
                            echo __d('pricing', 'TXT_PRODUCT_BRAND') . $this->Comment->formAsterisk(); ?>
                        </label>
                        <div class="col-sm-9 custom-select">
                            <?php
                            $product_brand = [];
                            if (!$data->isNew()) {
                                $product_brand = [
                                    $data->product_brand_id => $this->Comment->getFieldByLocal($data->product_brand, $locale),
                                ];
                            }
                            echo $this->Form->select('product_brand_id', $product_brand, [
                                'class' => 'form-control',
                                'label' => false,
                                'required' => false,
                                'id' => 'brand-id',
                                'empty' => ['' => __d('pricing', 'TXT_SELECT_PRODUCT_BRAND')],
                            ]); ?>
                        </div>
                    </div>
                    <div class="form-group custom-select">
                        <label class="col-sm-3 control-label">
                            <?php
                            echo __d('pricing', 'TXT_PRODUCT') . $this->Comment->formAsterisk(); ?>
                        </label>
                        <div class="col-sm-9">
                            <?php
                            $product = [];
                            if (!$data->isNew()) {
                                $product = [
                                    $data->product_detail->product_id => $this->Comment->getFieldByLocal($data->product_detail->product, $locale),
                                ];
                            }
                            echo $this->Form->select('product_id', $product, [
                                'class' => 'form-control',
                                'label' => false,
                                'required' => false,
                                'id' => 'product-id',
                                'empty' => ['' => __d('pricing', 'TXT_SELECT_PRODUCT')],
                                'value' => $product ? $data->product_detail->product_id : '',
                            ]); ?>
                        </div>
                    </div>
                    <div class="form-group custom-select">
                        <label class="col-sm-3 control-label">
                            <?php
                            echo __d('pricing', 'TXT_PRODUCT_DETAIL') . $this->Comment->formAsterisk(); ?>
                        </label>
                        <div class="col-sm-9">
                            <?php
                            $product_detail = [];
                            if (!$data->isNew()) {
                                $prd_detail_name = $this->Comment->productDetailName($data->product_detail, $locale);
                                $product_detail = [
                                    $data->product_detail_id => $prd_detail_name,
                                ];
                            }
                            echo $this->Form->select('product_detail_id', $product_detail, [
                                'class' => 'form-control',
                                'label' => false,
                                'required' => false,
                                'id' => 'product-detail-id',
                                'empty' => ['' => __d('pricing', 'TXT_SELECT_PRODUCT_DETAIL')],
                            ]); ?>
                        </div>
                    </div>
                </div>
            </div>
			<div class="col-sm-4">
				<div class="form-group custom-select">
					<div class="col-sm-12">
                        <label class="control-label">
                            <?php
                            echo __d('pricing', 'TXT_CURRENCY') . $this->Comment->formAsterisk(); ?>
                        </label>
						<?php
						echo $this->Form->select('currency_id', $currencies, [
							'class' => 'form-control',
							'label' => false,
							'required' => false,
							'id' => 'currency-id',
							'empty' => ['' => __d('pricing', 'TXT_SELECT_CURRENCY')],
						]); ?>
					</div>
                </div>
			</div>
			<div class="col-sm-4">
				<div class="form-group">
					<div class="col-sm-12">
                        <label class="control-label">
                            <?php
                            echo __d('pricing', 'TXT_STANDARD_PRICE_TYPE') . $this->Comment->formAsterisk(); ?>
                        </label>
						<?php
						echo $this->Form->number('standard_price', [
							'class' => 'form-control',
							'label' => false,
							'required' => false,
							'placeholder' => __d('pricing', 'TXT_INPUT_STANDARD_PRICE'),
						]); ?>
					</div>
				</div>
			</div>
			<div class="col-sm-4">
				<div class="form-group">
					<div class="col-sm-12">
                        <label class="control-label">
                            <?php
                            echo __d('pricing', 'TXT_MIN_QTY') . $this->Comment->formAsterisk(); ?>
                        </label>
						<?php
						echo $this->Form->number('min_qty_for_standard_price', [
							'class' => 'form-control',
							'label' => false,
							'required' => false,
							'placeholder' => __d('pricing', 'TXT_INPUT_MINIMUM_QUANTITY'),
						]); ?>
					</div>
				</div>
			</div>
        </div>
        <div class="row">
            <div class="col-sm-12 col-md-12">
                <div class="data-tb-list">
                    <table class="table table-bordered pricing-tb-list" id="discount-list">
                        <thead>
                            <tr>
                                <th class="white-space">
                                    <?php
                                    echo __d('pricing', 'TXT_PRICE_TYPE'); ?>
                                </th>
                                <th class="white-space">
                                    <?php
                                    echo __d('pricing', 'TXT_RANGE_TYPE'); ?>
                                </th>
                                <th class="white-space">
                                    <?php
                                    echo __d('pricing', 'TXT_VALUE_TYPE'); ?>
                                </th>
                                <th class="white-space">
                                    <?php
                                    echo  __d('pricing', 'TXT_CURRENCY'); ?>
                                </th>
                                <th class="white-space">
                                    <?php
                                    echo  __d('pricing', 'TXT_RANGE_FROM'); ?>
                                </th>
                                <th class="white-space">
                                    <?php
                                    echo  __d('pricing', 'TXT_RANGE_TO'); ?>
                                </th>
                                <th class="white-space">
                                    <?php
                                    echo __d('pricing', 'TXT_VALUE'); ?>
                                </th>
                                <th style="width: 1%;"></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            if ($data->isNew()) {
                                echo $this->element('PricingManagements/static_table_row');
                            } else {
                                echo $this->element('PricingManagements/list_of_pricing_range', [
                                    'data' => $data,
                                ]);
                            }
                            ?>
                        </tbody>
                    </table>
                </div>
                <div class="form-group">
                    <div class="col-sm-12 text-center">
                        <?php
                        echo $this->Form->button(__('TXT_CLEAR'), [
                            'type' => 'button',
                            'class' => 'btn btn-default form_btn btn-width btn-sm',
                            'id' => 'btn-clear',
                        ]); ?>
                        &nbsp; &nbsp;
                        <?php
                        echo $this->Form->button(__('TXT_SAVE'), [
                            'type' => 'button',
                            'class' => 'btn btn-primary form_btn btn-width btn-sm',
                            'id' => 'btn-register',
                        ]); ?>
                    </div>
                </div>
            </div>
        </div>
        <?php
        echo $this->Form->end(); ?>
    </section>
</div>

<script>
    $(function(e) {
        var selectValueType = $('.pricing-tb-list').find('tbody > tr:eq(0) .value-type option').clone();

        $('.pricing-tb-list').find('select').select2('destroy');

        $('#external-id-type').change(function(e) {
            var type = $(this).val();
            if (type === CUSTOMER) {
                $('#customer-id').closest('.form-group').removeClass('hidden');
            } else {
                $('#customer-id').closest('.form-group').addClass('hidden');
            }
        });

        $('#seller-id').select2({
            width: '100%',
            minLength: 0,
            ajax: {
                url: BASE + 'Sellers/getListofSellersByAjax',
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    let query = {
                        keyword: params.term,
                        kind: $('body').find('#external-id-type').val() === CUSTOMER ? TYPE_WISTERIA : ''
                    };
                    return query;
                },
                processResults: function (data) {
                    let dataSource = [];
                    if (data.data === null && data.data === 'undefined') {
                        return false;
                    }
                    $.each(data.data, function(i, v) {
                        var name = '';
                        switch (v.type) {
                            case MANUFACTURER_TYPE:
                                name = getNameByLocal(LOCALE, v.manufacturer);
                                break;
                            default:
                            name = getNameByLocal(LOCALE, v.supplier);
                        }
                        dataSource.push({
                            id: v.id,
                            text: name
                        });
                    });
                    return { results: dataSource };
                }
            }
        });

        $('#customer-id').select2({
            width: '100%',
            minLength: 0,
            ajax: {
                url: BASE + 'Customers/getCustomerBySearch',
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    let query = {
                        keyword: params.term
                    };
                    return query;
                },
                processResults: function (data) {
                    let dataSource = [];
                    if (data.data === null && data.data === 'undefined') {
                        return false;
                    }
                    $.each(data.data, function(i, v) {
                        dataSource.push({
                            id: v.id,
                            text: getNameByLocal(LOCALE, v)
                        });
                    });
                    return { results: dataSource };
                }
            }
        });

        $('#brand-id').select2({
            width: '100%',
            minLength: 0,
            ajax: {
                url: BASE + 'SellerBrands/getListOfSellerBrandsBySellerId',
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    let query = {
                        keyword: params.term,
                        seller_id: $('body').find('#seller-id').val()
                    };
                    return query;
                },
                processResults: function (data) {
                    let dataSource = [];
                    if (data.data === null && data.data === 'undefined') {
                        return false;
                    }
                    $.each(data.data, function(i, v) {
                        dataSource.push({
                            id: v.id,
                            text: getNameByLocal(LOCALE, v.product_brand)
                        });
                    });
                    return { results: dataSource };
                }
            }
        });

        $('#product-id').select2({
            width: '100%',
            minLength: 0,
            ajax: {
                url: BASE + 'Products/getListOfProductsByProductBrandId',
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    let query = {
                        keyword: params.term,
                        product_brand_id: $('body').find('#brand-id').val()
                    };
                    return query;
                },
                processResults: function (data) {
                    let dataSource = [];
                    if (data.data === null && data.data === 'undefined') {
                        return false;
                    }
                    $.each(data.data, function(i, v) {
                        dataSource.push({
                            id: v.id,
                            text: getNameByLocal(LOCALE, v)
                        });
                    });
                    return { results: dataSource };
                }
            }
        });

        $('#product-detail-id').select2({
            width: '100%',
            minLength: 0,
            ajax: {
                url: BASE + 'ProductDetails/getListOfProductDetailsByProductId',
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    let query = {
                        keyword: params.term,
                        product_id: $('body').find('#product-id').val()
                    };
                    return query;
                },
                processResults: function (data) {
                    let dataSource = [];
                    if (data.data === null && data.data === 'undefined') {
                        return false;
                    }
                    $.each(data.data, function(i, v) {
                        var name = v.single_unit_size;
                        if (v.single_unit !== null) {
                            name += ' X ' + getNameByLocal(LOCALE, v.single_unit);
                        }
                        name += v.pack_size + ' ';
                        if (v.tb_pack_sizes !== null) {
                            name += ' X ' + getNameByLocal(LOCALE, v.tb_pack_sizes);
                        }
                        dataSource.push({
                            id: v.id,
                            text: name
                        });
                    });
                    return { results: dataSource };
                }
            }
        });

        $('.btn-add-row').click(function(e) {
            var table = $('body').find('.pricing-tb-list');
            var buttonDelete = '<button type="button" class="btn btn-delete btn-sm"><i class="fa fa-trash" aria-hidden="true"></i></button>';
            var tr = $(table).find('tbody tr:eq(0)').clone();
            var inx = Number($(table).find('tbody > tr:last').attr('data-index')) + 1;
            var field_name = 'pricing_range_managements[' + inx + ']';

            $(tr).find('td:eq(0) select').attr('name', field_name + '[price_type]');
            $(tr).find('td:eq(1) select').attr('name', field_name + '[range_type]');
            $(tr).find('td:eq(2) select').attr('name', field_name + '[value_type]');
            $(tr).find('td:eq(3) select').attr('name', field_name + '[currency_id]');
            $(tr).find('td:eq(4) input').attr('name', field_name + '[range_from]');
            $(tr).find('td:eq(5) input').attr('name', field_name + '[range_to]');
            $(tr).find('td:eq(6) input').attr('name', field_name + '[value]');
            $(tr).find('td:last').html(buttonDelete);

            $(tr).find('input, select').val('');
            $(tr).find('.error-message').remove();
            $(tr).find('.currency-id').attr('disabled', true);
            $(tr).attr('data-index', inx);
            $(table).find('tbody').append(tr);
        });

        $('#btn-register').click(function(e) {
            var form = $('body').find('form[name="pricing_form"]')
            var params = {
                type: 'POST',
                url: BASE + 'PricingManagements/saveOrUpdate',
                data: $(form).serialize(),
                beforeSend: function(e) {
                    $.LoadingOverlay('show');
                    $(form).find('.error-message').remove();
                }
            };
            ajaxRequest(params, SESSION_TIMEOUT, function(data) {
                if (data.message === MSG_ERROR) {
                    $.each(data.data, function(index, value) {
                        showMessageError(form, data.data);
                        // validation on table row
                        if ('pricing_range_managements' in data.data) {
                            setValidation(data.data.pricing_range_managements);
                        }
                        return false;
                    });
                } else {
                    location.href = BASE + 'PricingManagements';
                }
            });
        });

        $('body').on('change', '.price-type', function(e) {
            var tr = $(this).closest('tr');
            var priceType = $(tr).find('.price-type').val();
            if (priceType === CLINIC_PRICE_TYPE) {
                $(tr).find('.value-type option[value="' + PERCENT_TYPE + '"]').remove();
            } else {
                $(tr).find('.value-type').html(selectValueType).val('');
            }
        });

        $('body').on('change', '.range-type, .value-type', function(e) {
            var tr = $(this).closest('tr');
            var rangeType = $(tr).find('.range-type').val();
            var valueType = $(tr).find('.value-type').val();
            if ((rangeType === AMOUNT_TYPE) && (valueType === AMOUNT_TYPE)) {
                $(tr).find('.currency-id').removeAttr('disabled', false);
            } else {
                $(tr).find('.currency-id').val('').attr('disabled', true);
            }
        });

        $('body').on('click', '.btn-delete', function(e) {
            $(this).closest('tr').remove();
        });

        function setValidation(data)
        {
            var table = $('body').find('.pricing-tb-list > tbody');
            $.each(data, function(index, value) {
                var tr = $(table).find('tr:eq(' + index + ')');
                $.each(value, function(index1, value1) {
                    if (index1 === 'range_to' || index1 === 'range_from') {
                        var message = setValidateBetweenNumber(value1);
                        var content = $(tr).find('[name$="[' + index1 + ']"]');
                        $(content).closest('div').append(message);
                    } else {
                        var txt = value1._empty
                        if ('valid' in value1) {
                            txt = value1.valid;
                        }
                        var message = '<label class="error-message">' + txt + '</label>';
                        var content = $(tr).find('[name$="[' + index1 + ']"]');
                        $(content).closest('div').append(message);
                    }
                });
            });
        }

        function setValidateBetweenNumber(data)
        {
            var element = '';
            if ((data !== null) && (data !== 'undefined')) {
                element += '<ul style="padding-left: 15px;">';
                $.each(data, function(index, value) {
                    element += '<li class="error-message">' + value + '</li>';
                });
                element += '</ul>';
            }
            return element;
        }
    });
</script>
