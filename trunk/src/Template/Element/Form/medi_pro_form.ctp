<?php
    echo $this->Form->create(null, [
        'novalidate' => true,
        'autocomplete' => 'off',
        'id' => 'form-user-final-agreement',
        'class' => 'form-horizontal'
   ]);
    $this->Form->templates([
        'inputContainer' => '{{content}}'
    ]);
    echo $this->Form->hidden('affiliation_class', [
        'value' => $group->affiliation_class,
    ]);
    echo $this->Form->hidden('id', [
        'value' => $this->request->session()->read('Auth.User.id'),
    ]);
    echo $this->Form->hidden('activated', ['value' => 1]);
?>
    <div class="form-group">
        <label for="userDepartment" class="col-md-3 control-label">部署<i class="required-indicator">＊</i></label>
        <div class="col-md-9">
            <?php
                echo $this->Form->input('department', [
                    'label' => false,
                    'class' => 'form-control input-change',
                    'placeholder' => '部署',
                    'id' => 'userDepartment',
                    'value' => $user['department']
                ]);
            ?>
            <p class="help-block help-tips" id="error_userDepartment"></p>
        </div>
    </div>
    <div class="form-group">
        <label for="userPosition" class="col-md-3 control-label">役職</label>
        <div class="col-md-9">
            <?php
                echo $this->Form->input('position', array(
                    'label' => false,
                    'class' => 'form-control input-change',
                    'placeholder' => '役職',
                    'id' => 'userPosition',
                    'value' => $user['position']
                ));
            ?>
            <p class="help-block help-tips" id="error_userPosition"></p>
        </div>
    </div>
    <div class="form-group">
        <label for="userLastName" class="col-md-3 control-label">姓<i class="required-indicator">＊</i></label>
        <div class="col-md-4">
            <?php
                echo $this->Form->input('lastname', array(
                    'label' => false,
                    'class' => 'form-control input-change',
                    'placeholder' => '姓',
                    'id' => 'userLastName',
                    'value' => $user['lastname']
                ));
            ?>
            <p class="help-block help-tips" id="error_userLastName"></p>
        </div>
        <div class="col-md-1" style="padding: 0; text-align: right;">
            <label for="userFirstName" class="control-label">名<i class="required-indicator">＊</i></label>
        </div>
        <div class="col-md-4">
            <?php
                echo $this->Form->input('firstname', array(
                    'label' => false,
                    'class' => 'form-control input-change',
                    'placeholder' => '名',
                    'id' => 'userFirstName',
                    'value' => $user['firstname']
                ));
            ?>
            <p class="help-block help-tips" id="error_userFirstName"></p>
        </div>
    </div>
    <div class="form-group">
        <label for="userNameKana" class="col-md-3 control-label">セイ<i class="required-indicator">＊</i></label>
        <div class="col-md-4">
            <?php
                echo $this->Form->input('lastname_en', array(
                    'label' => false,
                    'class' => 'form-control input-change',
                    'placeholder' => 'セイ',
                    'id' => 'userLastNameK',
                    'value' => $user['lastname_en']
                ));
            ?>
            <p class="help-block help-tips" id="error_userLastNameK"></p>
        </div>
        <div class="col-md-1" style="padding: 0; text-align: right;">
            <label for="userFirstKana" class="control-label">メイ<i class="required-indicator">＊</i></label>
        </div>
        <div class="col-md-4">
            <?php
                echo $this->Form->input('firstname_en', array(
                    'label' => false,
                    'class' => 'form-control input-change',
                    'placeholder' => 'メイ',
                    'id' => 'userFirstNameK',
                    'value' => $user['firstname_en']
                ));
            ?>
            <p class="help-block help-tips" id="error_userFirstNameK"></p>
        </div>
    </div>
    <div class="form-group">
        <label for="userEmail" class="col-md-3 control-label">メールアドレス<i class="required-indicator">＊</i></label>
        <div class="col-md-9">
            <?php
                echo $this->Form->input('email', array(
                    'label' => false,
                    'class' => 'form-control input-change',
                    'placeholder' => 'メールアドレス',
                    'id' => 'userEmail',
                    'value' => $user['email']
                ));
            ?>
            <p class="help-block help-tips" id="error_userEmail"></p>
        </div>
    </div>
    <div class="form-group">
        <label for="userPhone" class="col-md-3 control-label">携帯番号</label>
        <div class="col-md-9">
            <?php
                echo $this->Form->input('mobile_phone', array(
                    'label' => false,
                    'class' => 'form-control input-change',
                    'placeholder' => '090-1234-5678',
                    'id' => 'userPhone',
                    'value' => $user['mobile_phone']
                ));
            ?>
            <p class="help-block help-tips" id="error_userPhone"></p>
        </div>
    </div>
    <div class="form-group">
        <label for="userRole" class="col-md-3 control-label">アカウント権限</label>
        <div class="col-md-9">
            <?php
                $role = __('TXT_ROLE_ADMIN');
                switch ($group->role) {
                    case 'customer_support':
                        $role = __('TXT_CUSTOMER_SUPPORT');
                        break;
                    case 'logistic':
                        $role = __('TXT_ROLE_LOGISTIC');
                        break;
                    case USER_ROLE_SALE:
                        $role = __('TXT_ROLE_SALE');
                        break;
                    case 'staff':
                        $role = __('TXT_ROLE_STAFF');
                        break;
                }
                echo $role;
            ?>
        </div>
    </div>
</form>