<?php
    echo $this->Form->create('', [
        'novalidate' => true,
        'autocomplete' => 'off',
        'id' => 'form-user-final-agreement',
        'class' => 'form-horizontal'
   ]);
    $this->Form->templates([
        'inputContainer' => '{{content}}'
    ]);
?>
    <div class="form-group">
        <label for="customerEntityName" class="col-md-3 control-label">医療法人名<i class="required-indicator">＊</i></label>
        <div class="col-md-9">
            <?php
                echo $this->Form->input('customers.entity_name', array(
                    'label' => false,
                    'class' => 'form-control',
                    'placeholder' => '医療法人名',
                    'id' => 'customerEntityName',
                    'value' => $user['customer']['entity_name']
                ));
            ?>
            <p class="help-block help-tips" id="error_customerEntityName"></p>
        </div>
    </div>
    <div class="form-group">
        <label for="customerClinicName" class="col-md-3 control-label">クリニック名<i class="required-indicator">＊</i></label>
        <div class="col-md-9">
            <?php
                echo $this->Form->input('customers.clinic_name', array(
                    'label' => false,
                    'class' => 'form-control',
                    'placeholder' => 'クリニック名',
                    'id' => 'customerClinicName',
                    'value' => $user['customer']['clinic_name']
                ));
            ?>
            <p class="help-block help-tips" id="error_customerEntityName"></p>
        </div>
    </div>
    <div class="form-group">
        <label for="userZipcode" class="col-md-3 control-label">郵便番号</label>
        <div class="col-md-9">
            <?php
                echo $this->Form->input('customers.zipcode', array(
                    'label' => false,
                    'class' => 'form-control',
                    'placeholder' => '郵便番号',
                    'id' => 'userZipcode',
                    'value' => $user['customer']['zipcode']
                ));
            ?>
            <p class="help-block help-tips" id="error_userZipcode"></p>
        </div>
    </div>
    <div class="form-group">
        <label for="userPrefecture" class="col-md-3 control-label">都道府県</label>
        <div class="col-md-9">
            <?php
                echo $this->Form->input('customers.prefecture', array(
                    'label' => false,
                    'class' => 'form-control',
                    'placeholder' => '都道府県',
                    'id' => 'userPrefecture',
                    'value' => $user['customer']['prefecture']
                ));
            ?>
            <p class="help-block help-tips" id="error_userPrefecture"></p>
        </div>
    </div>
    <div class="form-group">
        <label for="userCity" class="col-md-3 control-label">市区</label>
        <div class="col-md-9">
            <?php
                echo $this->Form->input('customers.city', array(
                    'label' => false,
                    'class' => 'form-control',
                    'placeholder' => '市区',
                    'id' => 'userCity',
                    'value' => $user['customer']['city']
                ));
            ?>
            <p class="help-block help-tips" id="error_userCity"></p>
        </div>
    </div>
    <div class="form-group">
        <label for="userStreet" class="col-md-3 control-label">町村</label>
        <div class="col-md-9">
            <?php
                echo $this->Form->input('customers.street', array(
                    'label' => false,
                    'class' => 'form-control',
                    'placeholder' => '町村',
                    'id' => 'userStreet',
                    'value' => $user['customer']['street']
                ));
            ?>
            <p class="help-block help-tips" id="error_userStreet"></p>
        </div>
    </div>
    <div class="form-group">
        <label for="userBuilding" class="col-md-3 control-label">建物</label>
        <div class="col-md-9">
            <?php
                echo $this->Form->input('customers.building', array(
                    'label' => false,
                    'class' => 'form-control',
                    'placeholder' => '建物',
                    'id' => 'userBuilding',
                    'value' => $user['customer']['building']
                ));
            ?>
            <p class="help-block help-tips" id="error_userBuilding"></p>
        </div>
    </div>
    <div class="form-group">
        <label for="userTel" class="col-md-3 control-label">電話番号</label>
        <div class="col-md-9">
            <?php
                echo $this->Form->input('customers.tel', array(
                    'label' => false,
                    'class' => 'form-control',
                    'placeholder' => '03-1234-5678',
                    'id' => 'userTel',
                    'value' => $user['customer']['tel']
                ));
            ?>
            <p class="help-block help-tips" id="error_userTel"></p>
        </div>
    </div>
    <div class="form-group">
        <label for="userFax" class="col-md-3 control-label">ファックス番号</label>
        <div class="col-md-9">
            <?php
                echo $this->Form->input('customers.fax', array(
                    'label' => false,
                    'class' => 'form-control',
                    'placeholder' => '03-1234-6789',
                    'id' => 'userFax',
                    'value' => $user['customer']['fax']
                ));
            ?>
            <p class="help-block help-tips" id="error_userFax"></p>
        </div>
    </div>
    <div class="form-group">
        <label for="userDepartment" class="col-md-3 control-label">部署</label>
        <div class="col-md-9">
            <?php
                echo $this->Form->input('department', array(
                    'label' => false,
                    'class' => 'form-control',
                    'placeholder' => '部署',
                    'id' => 'userDepartment',
                    'value' => $user['department']
                ));
            ?>
            <p class="help-block help-tips" id="error_userDepartment"></p>
        </div>
    </div>
    <div class="form-group">
        <label for="userPosition" class="col-md-3 control-label">役職</label>
        <div class="col-md-9">
            <?php
                echo $this->Form->input('position', array(
                    'label' => false,
                    'class' => 'form-control',
                    'placeholder' => '役職',
                    'id' => 'userPosition',
                    'value' => $user['position']
                ));
            ?>
            <p class="help-block help-tips" id="error_userPosition"></p>
        </div>
    </div>
    <div class="form-group">
        <label for="userLastName" class="col-md-3 control-label">姓名<i class="required-indicator">＊</i></label>
        <div class="col-md-4">
            <?php
                echo $this->Form->input('lastname', array(
                    'label' => false,
                    'class' => 'form-control',
                    'placeholder' => '姓',
                    'id' => 'userLastName',
                    'value' => $user['lastname']
                ));
            ?>
            <p class="help-block help-tips" id="error_userLastName"></p>
        </div>
        <div class="col-md-1"></div>
        <div class="col-md-4">
            <?php
                echo $this->Form->input('firstname', array(
                    'label' => false,
                    'class' => 'form-control',
                    'placeholder' => '名',
                    'id' => 'userFirstName',
                    'value' => $user['firstname']
                ));
            ?>
            <p class="help-block help-tips" id="error_userFirstName"></p>
        </div>
    </div>
    <div class="form-group">
        <label for="userNameKana" class="col-md-3 control-label">セイメイ<i class="required-indicator">＊</i></label>
        <div class="col-md-4">
            <?php
                echo $this->Form->input('lastname_en', array(
                    'label' => false,
                    'class' => 'form-control',
                    'placeholder' => 'セイ',
                    'id' => 'userLastNameK',
                    'value' => $user['lastname_en']
                ));
            ?>
            <p class="help-block help-tips" id="error_userLastNameK"></p>
        </div>
        <div class="col-md-1"></div>
        <div class="col-md-4">
            <?php
                echo $this->Form->input('firstname_en', array(
                    'label' => false,
                    'class' => 'form-control',
                    'placeholder' => 'メイ',
                    'id' => 'userFirstNameK',
                    'value' => $user['firstname_en']
                ));
            ?>
            <p class="help-block help-tips" id="error_userFirstNameK"></p>
        </div>
    </div>
    <div class="form-group">
        <label for="userEmail" class="col-md-3 control-label">メールアドレス<i class="required-indicator">＊</i></label>
        <div class="col-md-9">
            <?php
                echo $this->Form->input('email', array(
                    'label' => false,
                    'class' => 'form-control',
                    'placeholder' => 'メールアドレス',
                    'id' => 'userEmail',
                    'value' => $user['email']
                ));
            ?>
            <p class="help-block help-tips" id="error_userEmail"></p>
        </div>
    </div>
    <div class="form-group">
        <label for="userPhone" class="col-md-3 control-label">携帯電話番号</label>
        <div class="col-md-9">
            <?php
                echo $this->Form->input('mobile_phone', array(
                    'label' => false,
                    'class' => 'form-control',
                    'placeholder' => '090-1234-5678',
                    'id' => 'userPhone',
                    'value' => $user['mobile_phone']
                ));
            ?>
            <p class="help-block help-tips" id="error_userPhone"></p>
        </div>
    </div>
    <div class="form-group">
        <label for="userRole" class="col-md-3 control-label">アカウント権限</label>
        <div class="col-md-9">
            <?php
                $role = __('TXT_ROLE_ADMIN');
                switch ($user['role']) {
                    case 'customer_support':
                        $role = __('TXT_CUSTOMER_SUPPORT');
                        break;
                    case 'logistic':
                        $role = __('TXT_ROLE_LOGISTIC');
                        break;
                    case 'sales':
                        $role = __('TXT_ROLE_SALE');
                        beak;
                    case 'staff':
                        $role = __('TXT_ROLE_STAFF');
                        beak;
                }
                echo $role;
            ?>
        </div>
    </div>
</form>