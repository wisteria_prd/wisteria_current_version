
<?php
echo $this->Form->create(isset($user) ? $user : '', [
    'class' => 'form-horizontal',
    'autocomplete' => 'off'
]);
?>
<div class="form-group">
    <label for="" class="col-sm-4 control-label">
        <div class="text-left">
            アカウント権限<span><?php echo TEXT_REQUIRED; ?></span>
        </div>
    </label>
    <div class="col-sm-8">
        <?php echo $this->Form->select('role', unserialize(ROLE) , array(
            'label' => false,
            'class' => 'form-control',
            'empty' => '権限を選択',)); ?>
    </div>
</div>

<div class="form-group">
    <label for="" class="col-sm-4 control-label">
        <div class="text-left">姓名 <?php echo TEXT_REQUIRED; ?></div>
    </label>
    <div class="col-sm-8 form-inline">
        <?php
        echo $this->Form->input('lastname', [
            'label' => false,
            'class' => 'form-control',
            'placeholder' => '姓を入力',
            'required' => false
        ]);
        echo $this->Form->input('firstname', [
            'label' => false,
            'class' => 'form-control',
            'placeholder' => '名を入力',
            'required' => false
        ]);
        ?> 
    </div>
</div>
<div class="form-group">
    <label for="" class="col-sm-4 control-label">
        <div class="text-left">姓名カナ <?php echo TEXT_REQUIRED; ?></div>
    </label>
    <div class="col-sm-8 form-inline">
        <?php
        echo $this->Form->input('lastname_kana', [
            'label' => false,
            'class' => 'form-control',
            'placeholder' => 'セイを入力',
            'required' => false
        ]);
        echo $this->Form->input('firstname_kana', [
            'label' => false,
            'class' => 'form-control',
            'placeholder' => 'メイを入力',
            'required' => false
        ]);
        ?> 
    </div>
</div>
<div class="form-group">
    <label for="" class="col-sm-4 control-label">
        <div class="text-left">
            Email <span><?php echo TEXT_REQUIRED; ?></span>
        </div>
    </label>
    <div class="col-sm-8">
        <?php echo $this->Form->input('email', [
            'label' => false,
            'class' => 'form-control',
            'placeholder' => 'Emailを入力',
            'autocomplete' => 'off',
            'required' => true
        ]); ?>
    </div>
</div>
<div class="form-group">
    <label for="" class="col-sm-4 control-label">
        <div class="text-left">
            パスワード<span><?php echo TEXT_REQUIRED; ?></span>
        </div>
    </label>
    <div class="col-sm-8">
        <?php echo $this->Form->input('password', array(
            'label' => false,
            'class' => 'form-control',
            'placeholder' => 'パスワードを入力',
            'autocomplete' => 'off',
            'required' => false)); ?>
    </div>
</div>
<div class="form-group">
    <label for="" class="col-sm-4 control-label">
        <div class="text-left">パスワード確認用 <?php echo TEXT_REQUIRED; ?></div>
    </label>
    <div class="col-sm-8">
        <?php echo $this->Form->input('confirm_password', [
            'label' => false,
            'type' => 'password',
            'class' => 'form-control',
            'placeholder' => 'もう一度パスワードを入力',
            'autocomplete' => 'off',
            'required' => false]); ?>
    </div>
</div>
<div class="text-center">
    <?php
    echo $this->Html->link(__('TXT_CANCEL'),
        ['action' => 'index'],
        [
            'class' => 'btn btn-default btn-sm',
            'data-dismiss' => 'modal',
        ]
    );
    echo $this->Form->button(__('TXT_REGISTER_NEW'), [
        'class' => 'btn btn-primary btn-sm',
        'id' => 'submit'
    ]);
    ?>
</div>
<?php echo $this->Form->end();
