<?php
$action = $this->request->action;
?>
<section id="create_form">
    <div class="row">
        <div class="col-sm-10 col-sm-offset-1">
            <?php
            echo $this->Form->create($foc, [
                'role' => 'form',
                'class' => 'form-horizontal form-foc',
                'name' => 'common_form'
            ]);
            $this->Form->templates([
                'inputContainer' => '{{content}}'
            ]); ?>
            <div class="form-group custom-select">
                <label class="col-sm-2 control-label">
                    <?php echo __d('foc', 'TXT_SUPPLIER') . $this->Comment->formAsterisk() ?>
                </label>
                <div class="col-sm-4">
                    <select name="seller_id" class="form-control" id="select-seller">
                        <?php echo $this->Comment->supplierDropdown($sellers, $en); ?>
                    </select>
                    <span class="help-block help-tips" id="error-seller_id"></span>
                </div>
            </div>
            <div class="form-group custom-select">
                <label class="col-sm-2 control-label">
                    <?php echo __d('foc', 'TXT_TARGET_TYPE') . $this->Comment->formAsterisk() ?>
                </label>
                <div class="col-sm-4">
                    <?php
                    echo $this->Form->select('foc_for', $for_foc, [
                        'class' => 'form-control',
                        'label' => false,
                        'required' => false,
                        'disabled' => ($action == 'edit') ? false : true,
                        'id' => 'foc-for',
                        'empty' => ['' => __d('foc', 'TXT_SELECT_TARGET_TYPE')]
                    ]); ?>
                    <span class="help-block help-tips" id="error-foc_for"></span>
                </div>
                <label class="col-sm-2 control-label">&nbsp;</label>
                <div class="col-sm-4">
                    <div class="f-value">
                    <?php
                    $disabled = true;
                    if ($action == 'edit') {
                        if ($foc->foc_for == TYPE_BRAND || $foc->foc_for == TYPE_PRODUCT) {
                            $disabled = false;
                        }
                    }
                    echo $this->Form->select('for_value', [], [
                        'class' => 'form-control',
                        'label' => false,
                        'required' => false,
                        'id' => 'for-value',
                        'disabled' => $disabled,
                        'empty' => ['' => __d('foc', 'TXT_SELECT_TARGET_TYPE')]
                    ]);
                    ?>
                    </div>
                    <span class="help-block help-tips" id="error-for_value"></span>
                </div>
            </div>
            <div class="form-group custom-select">
                <label class="col-sm-2 control-label">
                    <?php echo __d('foc', 'TXT_CURRENCY') ?>
                </label>
                <div class="col-sm-4">
                    <?php
                    $disabled = true;
                    if ($action == 'edit') {
                        if ($foc->range_type == TYPE_AMOUNT) {
                            $disabled = false;
                        }
                    }
                    echo $this->Form->select('currency_id', $currencies, [
                        'class' => 'form-control',
                        'label' => false,
                        'required' => false,
                        'id' => 'select-currency',
                        'disabled' => $disabled,
                        'empty' => ['' => __d('foc', 'TXT_SELECT_CURRENCY')]
                    ]); ?>
                    <span class="help-block help-tips" id="error-currency_id"></span>
                </div>
            </div>
            <div class="form-group custom-select">
                <label class="col-sm-2 control-label">
                    <?php echo __d('foc', 'TXT_FOC') . $this->Comment->formAsterisk() ?>
                </label>
                <div class="col-sm-4">
                    <?php
                    echo $this->Form->select('range_type', [
                        TYPE_AMOUNT =>  __d('foc', 'TXT_AMOUNT'),
                        TYPE_QUANTITY => __d('foc', 'TXT_QUANTITY'),
                    ], [
                        'class' => 'form-control',
                        'label' => false,
                        'required' => false,
                        'id' => 'range-type',
                    ]); ?>
                    <span class="help-block help-tips" id="error-range_type"></span>
                </div>
            </div>
            <div class="form-group">
                <?php echo $this->ActionButtons->btnSaveCancel('Focs', $action); ?>
            </div>
            <?= $this->Form->end(); ?>
        </div>
    </div>
</section>
<script>
    (function() {
        $('body').on('change', '#select-seller', function() {
            $('#foc-for').val('').change();
            if ($(this).val() !== '') {
                $('#foc-for').removeAttr('disabled', true);
            } else {
                $('#foc-for').attr('disabled', true);
            }
        });

        $('body').on('change', '#foc-for', function() {
            if ($(this).val() == '<?= TYPE_BRAND ?>' || $(this).val() == '<?= TYPE_PRODUCT ?>') {
                var data = {seller_id: $('#select-seller').val(), foc_for: $(this).val()};
                $.ajax({
                    url: '<?= $this->Url->build(['controller' => 'Focs', 'action' => 'focValue']) ?>',
                    type: 'GET',
                    data: data,
                    dataType: 'HTML',
                    beforeSend: function() {
                        $.LoadingOverlay('show');
                    },
                    success: function(response) {
                        $.LoadingOverlay('hide');
                        $('#for-value').removeAttr('disabled', true);
                        $('#for-value').empty().html(response).select2({
                            width: '100%'
                        });
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        $.LoadingOverlay('hide');
                        if (errorThrown === 'Forbidden') {
                            if (confirm('<?= __('TXT_SESSION_TIMEOUT') ?>')) {
                                location.reload();
                            }
                        }
                    },
                    complete: function() {
                        <?php
                        if ($action == 'edit') { ?>
                            $('#for-value').val('<?= $foc->for_value ?>').change();
                        <?php
                        }
                        ?>
                    }
                });
            } else if ($(this).val() == '<?= TYPE_PO ?>') {
                $('#for-value').empty().html('<option value=""><?= __('TXT_SELECT_VALUE') ?></option>').attr('disabled', true);
                $('#range-type').val('<?= TYPE_AMOUNT ?>').change();
            } else {
                $('#for-value').empty().html('<option value=""><?= __('TXT_SELECT_VALUE') ?></option>').attr('disabled', true);
            }
        });

        $('body').on('change', '#range-type', function() {
            if ($(this).val() == '<?= TYPE_AMOUNT ?>') {
                $('#select-currency').removeAttr('disabled', true);
            } else {
                $('#select-currency').attr('disabled', true);
            }
        });

        $('body').on('click', '#btn-register', function() {
            $('.help-block').empty().hide();
            var data = $('.form-foc').serialize();
            $.ajax({
                url: $('.form-foc').attr('action'),
                type: 'POST',
                dataType: 'JSON',
                data: data,
                cache: false,
                beforeSend: function() {
                    $.LoadingOverlay('show');
                },
                success: function(response) {
                    $.LoadingOverlay('hide');
                    if (response.status === 1) {
                        window.history.back();
                    } else if (response.status === 0) {
                        $.each(response.message, function(key, elem) {
                            var validate = $('#error-' + key);
                            $.each(elem, function(k, v) {
                                validate.removeClass('help-tips')
                                    .addClass('error-tips')
                                    .text(v)
                                    .show();
                            });
                        });
                    }
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    $.LoadingOverlay('hide');
                    if (errorThrown === 'Forbidden') {
                        if (confirm('<?= __('TXT_SESSION_TIMEOUT') ?>')) {
                            location.reload();
                        }
                    }
                }
            });
        });

        <?php
        if ($action == 'edit') { ?>
            $('#foc-for').trigger('change');
        <?php
        }
        ?>
    })();
</script>
