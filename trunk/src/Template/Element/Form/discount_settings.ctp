
<?php
echo $this->Form->create(isset($user) ? $user : '', [
        'class' => 'form-discount-settings',
        'autocomplete' => 'off',
        'type' => 'file'
    ]
); ?>
<div class="row">
    <div class="col-md-4">
        <?php
        echo $this->Form->button(__('STR_CSV_UPLOAD'), [
            'type' => 'button',
            'class' => 'btn btn-default',
        ]); ?>
    </div>
</div>
<div class="row row-top-space">
    <div class="col-md-4 custom-select">
        <label for=""><?= __('サプライヤー') ?></label>
        <?php
        echo $this->Form->select('role', unserialize(ROLE) , [
            'label' => false,
            'class' => 'form-control',
            'empty' => false
        ]); ?>
    </div>
    <div class="col-md-4 custom-select">
        <label for=""><?= __('STR_DISCOUNT_CONDITIONS') ?></label>
        <?php
        $options = ['PO', 'Brand', 'Product'];
        echo $this->Form->select('role', $options, [
            'label' => false,
            'class' => 'form-control',
            'empty' => false
        ]); ?>
    </div>
    <div class="col-md-4 custom-select">
        <label for=""><?= __('STR_DISCOUNT_TYPE') ?></label>
        <?php
        $options = ['Price', 'Discount Rate', 'FOC'];
        echo $this->Form->select('role', $options, [
            'label' => false,
            'class' => 'form-control',
            'empty' => false
        ]); ?>
    </div>
</div>
<div class="row row-top-space">
    <div class="col-md-4 custom-select">
        <label for=""><?= __('メーカ') ?></label>
        <?php
        echo $this->Form->select('role', unserialize(ROLE) , [
            'label' => false,
            'class' => 'form-control',
            'empty' => false
        ]); ?>
    </div>
    <div class="col-md-4 custom-select">
        <label for=""><?= __('STR_PRODUCT_BRAND') ?></label>
        <?php
            $options = ['PO', 'Brand', 'Product'];
            echo $this->Form->select('role', $options, [
                'label' => false,
                'class' => 'form-control',
                'empty' => false
            ]);
        ?>
    </div>
    <div class="col-md-4 custom-select">
        <label for=""><?= __('製品') ?></label>
        <?php
            $options = ['PO', 'Brand', 'Product'];
            echo $this->Form->select('role', $options, [
                'label' => false,
                'class' => 'form-control',
                'empty' => false
            ]);
        ?>
    </div>
</div>
<div class="row row-top-space">
    <div class="col-md-4 custom-select">
        <label for=""><?= __('STR_RANGE_TYPE') ?></label>
        <?php
            echo $this->Form->select('role', unserialize(ROLE) , [
                'label' => false,
                'class' => 'form-control',
                'empty' => false
            ]);
        ?>
    </div>
    <div class="col-md-4 custom-select">
        <label for=""><?= __('STR_FROM') ?></label>
        <?php
            echo $this->Form->input('role', [
                'type' => 'text',
                'label' => false,
                'class' => 'form-control'
            ]);
        ?>
    </div>
    <div class="col-md-4">
        <label for=""><?= __('STR_TO') ?></label>
        <?php
            echo $this->Form->input('role', [
                'type' => 'text',
                'label' => false,
                'class' => 'form-control'
            ]);
        ?>
    </div>
</div>
<div class="row row-top-space">
    <div class="col-md-4">
        <label for=""><?= __('STR_VALUE') ?></label>
        <?php
        echo $this->Form->input('role', [
            'type' => 'text',
            'label' => false,
            'class' => 'form-control settings-value'
        ]);
        ?>
    </div>
    <div class="col-md-4 custom-select">
        <label for=""><?= __('STR_CURRENCY_TYPE') ?></label>
        <?php
        echo $this->Form->select('role', unserialize(ROLE) , [
            'label' => false,
            'class' => 'form-control settings-currency-type',
            'empty' => false
        ]);
        ?>
    </div>
    <div class="col-md-4">
        <br />
        <button type="button" class="btn btn-warning settings-additional" style="display: none;"><?= __('Select FOC') ?></button>
    </div>
</div>
    <div class="row row-top-space">
        <div class="col-md-4">
        <?php
        echo $this->Form->button(__('STR_ADD'), [
            'class' => 'btn btn-primary',
            'id' => 'submit'
            ]
        );
        ?>
        </div>
    </div>
<?php echo $this->Form->end(); ?>

<!-- modal -->
<?= $this->HtmlModal->modalNoHeader('modalAdditionalSettings', ' modal-lg') ?>
<div class="row">
    <div class="col-md-10 col-md-offset-1">
        <div class="settings">
            <div class="row row-item">
                <div class="col-md-3">
                    <label for=""><?= __('STR_PRODUCT_BRAND') ?></label>
                    <?php
                        echo $this->Form->select('role', unserialize(ROLE) , [
                            'label' => false,
                            'class' => 'form-control',
                            'empty' => false
                        ]);
                    ?>
                </div>
                <div class="col-md-3">
                    <label for=""><?= __('製品') ?></label>
                    <?php
                        echo $this->Form->select('role', unserialize(ROLE) , [
                            'label' => false,
                            'class' => 'form-control',
                            'empty' => false
                        ]);
                    ?>
                </div>
                <div class="col-md-3">
                    <label for=""><?= __('STR_TYPE') ?></label>
                    <?php
                        echo $this->Form->select('role', unserialize(ROLE) , [
                            'label' => false,
                            'class' => 'form-control',
                            'empty' => false
                        ]);
                    ?>
                </div>
                <div class="col-md-2">
                    <label for=""><?= __('STR_VALUE') ?></label>
                    <?php
                        echo $this->Form->input('role', [
                            'type' => 'text',
                            'label' => false,
                            'class' => 'form-control'
                        ]);
                    ?>
                </div>
                <div class="col-md-1 text-center minus-icon" style="display: none;">
                    <label for="">&nbsp;</label>
                    <button type="button" class="btn btn-default remove-settings">
                        <span class="glyphicon glyphicon-minus-sign"></span>
                    </button>
                </div>
            </div>
        </div>        
    </div>
</div>
<br />
<div class="row row-top-space">
    <div class="col-md-10 col-md-offset-1 text-center">
        <button type="button" class="btn btn-default add-settings">
            <span class="glyphicon glyphicon-plus-sign"></span>
        </button>
    </div>
</div>
<div class="row row-top-space">
    <div class="col-md-10 col-md-offset-1 text-center">
        <button class="btn btn-default btn-gap"><?= __('保存') ?></button>
        <button class="btn btn-default"><?= __('キャンセル') ?></button>
    </div>
</div>
<?= $this->HtmlModal->modalFooter(); ?>
<!-- /end modal -->

<script>
    $(function() {
        $('body').on('click', '.add-settings', function() {
            var clone_item = $('.row-item:last').clone().addClass('row-top-space');
            clone_item.find('.minus-icon').show();
            $('.settings').append(clone_item);
        });

        $('body').on('click', '.remove-settings', function() {
            $(this).parents('.row-item').remove();
        });

        $('body').on('keyup', '.settings-value', function() {
            var currency = $('.settings-value').val();
            var currency_type = $('.settings-currency-type').val();
            if (currency.length && currency_type.length) {
                $('.settings-additional').show();
            } else {
                $('.settings-additional').hide();
            }
        });
        
        $('body').on('change', '.settings-currency-type', function() {
            var currency = $('.settings-value').val();
            var currency_type = $(this).val();
            if (currency.length && currency_type.length) {
                $('.settings-additional').show();
            } else {
                $('.settings-additional').hide();
            }
        });

        $('body').on('click', '.settings-additional', function() {
            $('#modalAdditionalSettings').modal({
                backdrop: 'static',
                keyboard: false
            });
        });
    });
</script>