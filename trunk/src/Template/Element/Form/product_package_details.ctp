<?php
$action = $this->request->action;
?>
<section id="create_form">
    <div class="row">
        <div class="col-sm-8 col-sm-offset-2">
            <?php
            echo $this->Form->create($product_package_details, [
                'role' => 'form',
                'class' => 'form-horizontal form-product-package-details',
                'name' => 'common_form'
            ]);
            echo $this->Form->hidden('product_package_id', ['value' => $this->request->query('pkg_id'), 'class' => 'product-package-id']);
            echo $this->Form->hidden('product_id', ['class' => 'selected-product-id']);
            echo $this->Form->hidden('product_detail_id', ['class' => 'selected-product-details-id']);
            echo $this->Form->hidden('w_standard_price_id', ['class' => 'w-standard-price-id']);
            $this->Form->templates([
                'inputContainer' => '{{content}}'
            ]);
            ?>
            <div class="form-group custom-select">
                <label class="col-sm-3 control-label">
                    <?php
                    echo __d('product_package_detail', 'TXT_PRODUCT') ?>
                </label>
                <div class="col-sm-6">
                    <?php echo $this->Form->input('name', [
                        'type' => 'select',
                        'options' => [],
                        'class' => 'form-control customer-select',
                        'label' => false,
                        'required' => false,
                        'id' => 'product-name'
                    ]); ?>
                    <span class="help-block help-tips" id="error-name"></span>
                </div>
                <div class="col-sm-3 field-small-padding-left">
                    <button class="btn btn-sm btn-primary" id="search-a-product"><?= __('TXT_FIND') ?></button>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label"><?= __('TXT_QUANTITY') ?><?= $this->Comment->formAsterisk() ?></label>
                <div class="col-sm-6">
                    <?php echo $this->Form->input('quantity', [
                        'class' => 'form-control',
                        'placeholder' => __('TXT_QUANTITY'),
                        'label' => false,
                        'required' => false
                    ]); ?>
                    <span class="help-block help-tips" id="error-quantity"></span>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label"><?= __('TXT_UNIT_PRICE') ?></label>
                <div class="col-sm-6">
                    <?php
                    $unit_price = '';
                    if ($action == 'edit') {
                        $unit_price = $product_package_details->w_standard_price->standard_price;
                    }
                    echo $this->Form->input('unit_price', [
                        'class' => 'form-control unit-price',
                        'label' => false,
                        'required' => false,
                        'disabled' => true,
                        'value' => $unit_price
                    ]); ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label"><?= __('TXT_SUB_TOTAL') ?></label>
                <div class="col-sm-6">
                    <?php echo $this->Form->input('sub_total', [
                        'class' => 'form-control sub-total',
                        'label' => false,
                        'required' => false,
                        'disabled' => true
                    ]); ?>
                </div>
            </div>
            <div class="form-group">
                <?= $this->ActionButtons->btnSaveCancel('ProductPackageDetails', $action, 'btn-cancel', 'btn-register', null, null, 'index', ['pkg_id' => $this->request->query('pkg_id')]); ?>
            </div>
            <?= $this->Form->end(); ?>
        </div>
    </div>
</section>

<?= $this->element('Modal/product_list') ?>
<?= $this->element('Modal/view_detail') ?>

<script>
    (function() {
        GetDataList();

        function GetDataList() {
            var url = '<?php echo $this->Url->build('/product-details/product-list'); ?>';
            var pkg_detail_id = '';
            <?php
            if ($action == 'edit') { ?>
                autoCalculateSubtotal();
                pkg_detail_id = $('.selected-product-details-id').val();
            <?php
            }
            ?>
            $.ajax({
                url: url,
                type: 'GET',
                dataType: 'HTML'
            }).done(function (data) {
                $('#product-name').html(data).select2();
                if (pkg_detail_id != '') {
                    $('#product-name').find('option[value='+pkg_detail_id+']').prop('selected', true);
                }
            });
        }

        function searchProductModal(product_id, product_detail_id) {
            $.ajax({
                url: '<?= $this->Url->build(['controller' => 'ProductPackageDetails', 'action' => 'searchProductList']) ?>',
                type: 'GET',
                data: {product_id: product_id, product_detail_id: product_detail_id},
                dataType: 'HTML',
                beforeSend: function() {
                    $.LoadingOverlay('show');
                },
                success: function(response) {
                    $.LoadingOverlay('hide');
                    if (response) {
                        $('.product-list-content').empty().html(response);
                        $('#modal-product-list').modal('show');
                    }
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    $.LoadingOverlay('hide');
                    if (errorThrown === 'Forbidden') {
                        if (confirm('<?= __('TXT_SESSION_TIMEOUT') ?>')) {
                            location.reload();
                        }
                    }
                },
                complete: function(data) {
                    $('#select-product-name').select2({
                        width: '100%'
                    });
                    countProductDetail();
                }
            });
        }

        $('body').on('click', '#search-a-product', function(e) {
            e.preventDefault();
            searchProductModal(0, 0);
        });

        $('body').on('change', '#select-manufacturer', function() {
            var manufacturer = $(this).val();
            if (manufacturer !== '') {
                $.ajax({
                    url: '<?= $this->Url->build(['controller' => 'ProductBrands', 'action' => 'brandDropdown']) ?>',
                    type: 'GET',
                    data: {man_id: manufacturer},
                    dataType: 'HTML',
                    beforeSend: function() {
                        $.LoadingOverlay('show');
                    },
                    success: function(response) {
                        $.LoadingOverlay('hide');
                        if (response) {
                            $('#select-product-brand').removeAttr('disabled').empty().html(response);
                        } else {
                            $('#select-product-brand').find('option')
                                .remove()
                                .end();
                            $('#select-product-brand').attr('disabled', true).append('<option value=""><?= __('TXT_SELECT_BRAND_NAME') ?></option>');
                        }
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        $.LoadingOverlay('hide');
                        if (errorThrown === 'Forbidden') {
                            if (confirm('<?= __('TXT_SESSION_TIMEOUT') ?>')) {
                                location.reload();
                            }
                        }
                    }
                });
            } else {
                $('#select-product-brand').find('option')
                    .remove()
                    .end();
                $('#select-product-brand').attr('disabled', true).append('<option value=""><?= __('TXT_SELECT_BRAND_NAME') ?></option>');
            }
        });

        $('body').on('change', '#select-product-brand', function() {
            var brand = $(this).val();
            if (brand !== '') {
                selectModalProductList(brand);
            } else {
                selectModalProductList('');
            }
        });

        function selectModalProductList(brand_id) {
            $.ajax({
                url: '<?= $this->Url->build(['controller' => 'ProductPackageDetails', 'action' => 'productNameList']) ?>',
                type: 'GET',
                data: {brand_id: brand_id},
                dataType: 'JSON',
                beforeSend: function() {
                    $.LoadingOverlay('show');
                },
                success: function(response) {
                    $.LoadingOverlay('hide');
                    var element = '<option value=""><?= __('TXT_SELECT_PRODUCT_NAME') ?></option>';
                    if (response) {
                        $.each(response.data, function (i, v) {
                            var name = v.name;
                            if (!name) {
                                name = v.name_en;
                            }
                            element += '<option value="'+ v.id +'">'+ name +'</option>';
                        });
                        $('#select-product-name').empty().append(element).select2();
                    }
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    $.LoadingOverlay('hide');
                    if (errorThrown === 'Forbidden') {
                        if (confirm('<?= __('TXT_SESSION_TIMEOUT') ?>')) {
                            location.reload();
                        }
                    }
                },
                complete: function() {
                    countProductDetail();
                    if ($('.selected-product-id').val()) {
                        $('#select-product-name').val($('.selected-product-id').val()).change();
                    }
                }
            });
        }

        function countProductDetail() {
            var found = $('#select-product-name option').length -1;
            $('.amount-found').empty().text(found);
        }

        function productList(product_id) {
            $.ajax({
                url: '<?= $this->Url->build(['controller' => 'ProductPackageDetails', 'action' => 'productDetailList']) ?>',
                type: 'GET',
                data: {product_id: product_id},
                dataType: 'HTML',
                beforeSend: function() {
                    $.LoadingOverlay('show');
                },
                success: function(response) {
                    $.LoadingOverlay('hide');
                    $('#select-product-detail-name').removeAttr('disabled', true);
                    $('#select-product-detail-name').empty().html(response);
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    $.LoadingOverlay('hide');
                    if (errorThrown === 'Forbidden') {
                        if (confirm('<?= __('TXT_SESSION_TIMEOUT') ?>')) {
                            location.reload();
                        }
                    }
                },
                complete: function() {
                    countProductDetail();
                    if ($('.selected-product-details-id').val()) {
                        $('#select-product-detail-name').val($('.selected-product-details-id').val()).change();
                    }
                }
            });
        }
        $('body').on('change', '#select-product-name', function() {
            var product_id = $(this).val();
            productList(product_id);
        });

        $('body').on('change', '#select-product-detail-name', function() {
            var product_detail_id = $(this).val();
            if (product_detail_id) {
                $.ajax({
                    url: '<?= $this->Url->build(['controller' => 'ProductPackageDetails', 'action' => 'productListPrice']) ?>',
                    type: 'GET',
                    data: {product_detail_id: product_detail_id},
                    dataType: 'HTML',
                    beforeSend: function() {
                        $.LoadingOverlay('show');
                    },
                    success: function(response) {
                        $.LoadingOverlay('hide');
                        $('.table-product-price').empty().html(response);
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        $.LoadingOverlay('hide');
                        if (errorThrown === 'Forbidden') {
                            if (confirm('<?= __('TXT_SESSION_TIMEOUT') ?>')) {
                                location.reload();
                            }
                        }
                    }
                });
            }
        });

        $('body').on('change', '#product-name', function() {
            if ($(this).val()) {
                var product_id = $(this).find('option:selected').attr('data-pid');
                var product_detail_id = $(this).val();
                searchProductModal(product_id, product_detail_id);
            }
        });

        $('body').on('click', '.check-product', function() {
            $('.check-product').not(this).prop('checked', false);
        });

        $('body').on('click', '#select-product', function() {
            var value;
            var stand_price_id;
            $('.check-product').each(function() {
                if ($(this).is(':checked')) {
                    stand_price_id = $(this).attr('data-id');
                    value = $(this).parents('tr').find('.currency-price').text();
                }
            });

            $('#unit-price').val(value);
            var options = $('#select-product-detail-name option').clone();
            var selected = $('#select-product-detail-name').val();
            options.each(function() {
                if ($(this).val() == selected) {
                    $(this).attr('selected', true);
                }
            });
            $('#product-name').empty().append(options);
            $('.w-standard-price-id').val(stand_price_id);
            $('.selected-product-id').val($('#select-product-detail-name').attr('data-pid'));
            $('.selected-product-details-id').val($('#select-product-detail-name').val());
            $('.btn-close-modal').click();
        });

        function autoCalculateSubtotal() {
            var unit_price = parseInt($('#unit-price').val());
            if (unit_price) {
                var qty = parseFloat($('#quantity').val());
                var total = parseFloat(qty * unit_price).toFixed(2);
                $('#sub-total').val(total);
            } else {
                $('#sub-total').val('');
            }
        }

        $('body').on('input keypress', '#quantity', function() {
            autoCalculateSubtotal();
        });

        //view product details
        $('body').on('click', '.btn-view-detail-price', function() {
            var product_detail_id = $('#select-product-detail-name').val();
            if (!product_detail_id) {
                alert('<?= __('TXT_PRODUCT_DETAIL_REQUIRED') ?>');
                return false;
            }
            $.ajax({
                url: '<?= $this->Url->build(['controller' => 'ProductPackageDetails', 'action' => 'view']) ?>',
                type: 'GET',
                data: {product_detail_id: product_detail_id},
                dataType: 'HTML',
                beforeSend: function() {
                    $.LoadingOverlay('show');
                },
                success: function(response) {
                    $.LoadingOverlay('hide');
                    $('#modal_detail .modal-body').empty().html(response);
                    $('#modal_detail').modal('show');
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    $.LoadingOverlay('hide');
                    if (errorThrown === 'Forbidden') {
                        if (confirm('<?= __('TXT_SESSION_TIMEOUT') ?>')) {
                            location.reload();
                        }
                    }
                }
            });
        });

        $('#btn-register').on('click', function() {
            $('.help-block').empty().hide();
            var data = $('.form-product-package-details').serialize();
            $.ajax({
                url: $('.form-product-package-details').attr('action'),
                type: 'POST',
                dataType: 'JSON',
                data: data,
                cache: false,
                beforeSend: function() {
                    $.LoadingOverlay('show');
                },
                success: function(response) {
                    $.LoadingOverlay('hide');
                    if (response.status === 1) {
                        window.history.back();
                    } else if (response.status === 0) {
                        $.each(response.message, function(key, elem) {
                            var validate = $('#error-' + key);
                            $.each(elem, function(k, v) {
                                validate.removeClass('help-tips')
                                    .addClass('error-tips')
                                    .text(v)
                                    .show();
                            });
                        });
                    }
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    $.LoadingOverlay('hide');
                    if (errorThrown === 'Forbidden') {
                        if (confirm('<?= __('TXT_SESSION_TIMEOUT') ?>')) {
                            location.reload();
                        }
                    }
                }
            });
        });
    })();
</script>
