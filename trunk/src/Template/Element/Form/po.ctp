<?php
    echo $this->Form->create(isset($user) ? $user : '', [
            'class' => 'form-po-settings',
            'autocomplete' => 'off'
        ]
    );
?>
<span style="visibility: hidden;" id="goToSetting"></span>
    <div class="row row-top-space">
        <div class="col-md-2">
            <label for=""><?= __('Supplier') ?></label>
            <?php
                echo $this->Form->select('role', unserialize(ROLE) , [
                    'label' => false,
                    'class' => 'form-control',
                    'empty' => false
                ]);
            ?>
        </div>
        <div class="col-md-2">
            <label for=""><?= __('Issue Date') ?></label>
            <div class="input-group">
                <?= $this->Form->input('date', ['class' => 'form-control', 'label' => false, 'id' => 'datepicker']) ?>
                <div class="input-group-btn">
                    <button type="button" class="btn btn-default" aria-label="Calendar" id="trigger-calendar">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </button>
                </div>
            </div>
        </div>
        <div class="col-md-2">
            <label for=""><?= __('Currency Type') ?></label>
            <?php
                $options = ['PO', 'Brand', 'Product'];
                echo $this->Form->select('role', $options, [
                    'label' => false,
                    'class' => 'form-control',
                    'empty' => false
                ]);
            ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-offset-4 col-md-4">
            <div class="checkbox">
                <label>
                    <?= $this->Form->input('manual', ['type' => 'checkbox', 'label' => false]) ?> Manually change
                </label>
            </div>
        </div>
    </div>
    <div class="row row-top-space">
        <div class="col-md-2">
            <label for=""><?= __('Manufacturer') ?></label>
            <?php
                echo $this->Form->select('role', unserialize(ROLE) , [
                    'label' => false,
                    'class' => 'form-control',
                    'empty' => false
                ]);
            ?>
        </div>
        <div class="col-md-2">
            <label for=""><?= __('Product Brand') ?></label>
            <?php
                $options = ['PO', 'Brand', 'Product'];
                echo $this->Form->select('role', $options, [
                    'label' => false,
                    'class' => 'form-control',
                    'empty' => false
                ]);
            ?>
        </div>
        <div class="col-md-2">
            <label for=""><?= __('Product') ?></label>
            <?php
                $options = ['PO', 'Brand', 'Product', 'Product Test'];
                echo $this->Form->select('role', $options, [
                    'label' => false,
                    'class' => 'form-control dr-product',
                    'empty' => false
                ]);
            ?>
        </div>
        <div class="col-md-2">
            <label for=""><?= __('Specification') ?></label>
            <p class="txt-specification">something here.</p>
        </div>
        <div class="col-md-2">
            <label for=""><?= __('Dosage') ?></label>
            <p class="txt-specification">something here.</p>
        </div>
    </div>
    <div class="row row-top-space">
        <div class="col-md-4">
            <label for=""><?= __('Quantity') ?></label>
            <?php
                echo $this->Form->input('role', [
                    'type' => 'text',
                    'label' => false,
                    'class' => 'form-control'
                ]);
            ?>
        </div>
    </div>
<div class="row">
    <div class="col-md-4 col-md-offset-8 text-right">
        <a href="#goToList" class="btn btn-info" role="button">
            <span class="glyphicon glyphicon-arrow-down"></span> <?= __('Go to List') ?>
        </a>
    </div>
</div>
    <div class="row row-top-space">
        <div class="col-md-4">
            <label for=""><?= __('Unit Price') ?></label>
            <?php
                echo $this->Form->input('role', [
                    'type' => 'text',
                    'label' => false,
                    'class' => 'form-control'
                ]);
            ?>
        </div>
        <div class="col-md-4">
            <button type="button" class="btn btn-default btn-view-discount popup-discount"><?= __('View Discount') ?></button>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <div class="checkbox">
                <label>
                    <?= $this->Form->input('manual', ['type' => 'checkbox', 'label' => false]) ?> Manually change
                </label>
            </div>
        </div>
    </div>
    <div class="row row-top-space">
        <div class="col-md-4">
            <div class="checkbox">
                <label>
                    <?= $this->Form->input('manual', ['type' => 'checkbox', 'label' => false, 'class' => 'chk-manual-foc']) ?> Manually add as FOC
                </label>
            </div>
        </div>
        <div class="col-md-4">
            <div class="original-foc hidden-item">
                <label for=""><?= __('FOC Original Order') ?></label>
                <?php
                    echo $this->Form->select('role', unserialize(ROLE) , [
                        'label' => false,
                        'class' => 'form-control settings-currency-type',
                        'empty' => false
                    ]);
                ?>
            </div>
        </div>        
    </div>
    <div class="row row-top-space">
        <div class="col-md-4">
        <?php
            echo $this->Form->button('Add', [
                'class' => 'btn btn-primary',
                'id' => 'submit'
                ]
            );
        ?>
        </div>
    </div>
<?php echo $this->Form->end(); ?>
<div class="row row-top-space">
    <div class="col-md-12">
        <table class="table table-noborder">
            <thead>
                <tr>
                    <th colspan="8">
                        <p style="float: right;"><?= __('TOTAL:') ?> 145,2584.5</p>
                    </th>
                    <th width="13%"></th>
                </tr>
            </thead>
        </table>
    </div>
</div>
<div class="row" id="goToList">
    <div class="col-md-12">
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>#</th>
                    <th>&nbsp;</th>
                    <th><?= __('Manufacturer') ?></th>
                    <th><?= __('Product Brand') ?></th>
                    <th><?= __('Product') ?></th>
                    <th><?= __('Packing') ?></th>
                    <th><?= __('Quantity') ?></th>
                    <th><?= __('Unit_price') ?></th>
                    <th width="13%">&nbsp;</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td class="popup-discount">1</td>
                    <td class="popup-discount"><label data-status="discounted" class="settings label-discounted"><?= __('Discounted') ?></label></td>
                    <td class="popup-discount">Maker A</td>
                    <td class="popup-discount">Brand A</td>
                    <td class="popup-discount">Product A</td>
                    <td class="popup-discount">4 vials  x  5 ml</td>
                    <td class="popup-discount">100</td>
                    <td class="popup-discount">1000</td>
                    <td>
                        <?= $this->Html->link(__('Edit'), ['controller' => 'po', 'action' => 'edit', 1], ['class' => 'btn btn-default', 'role' => 'button']) ?>
                        <?= $this->Form->button(__('Delete'), ['class' => 'btn btn-warning btn-delete-po', 'data-toggle' => 'popover']) ?>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class="popup-discount">2</td>
                    <td class="popup-discount"><label data-status="foc-parent" class="settings label-foc-parent"><?= __('FOC') ?></label></td>
                    <td class="popup-discount">Maker B</td>
                    <td class="popup-discount">Brand B</td>
                    <td class="popup-discount">Product C</td>
                    <td class="popup-discount">1 syringe  x  0.5 ml</td>
                    <td class="popup-discount">25</td>
                    <td class="popup-discount">154</td>
                    <td>
                        <?= $this->Html->link(__('Edit'), ['controller' => 'po', 'action' => 'edit', 1], ['class' => 'btn btn-default', 'role' => 'button']) ?>
                        <?= $this->Form->button(__('Delete'), ['class' => 'btn btn-warning btn-delete-po', 'data-toggle' => 'popover']) ?>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class="popup-discount">3</td>
                    <td><label data-status="foc-child" class="settings label-foc-child"><?= __('FOC') ?></label></td>
                    <td class="popup-discount">Maker C</td>
                    <td class="popup-discount">Brand C</td>
                    <td class="popup-discount">Product D</td>
                    <td class="popup-discount">6 ampoules   x  5 ml</td>
                    <td class="popup-discount">20</td>
                    <td class="popup-discount">&nbsp;</td>
                    <td>
                        <?= $this->Html->link(__('Edit'), ['controller' => 'po', 'action' => 'edit', 1], ['class' => 'btn btn-default', 'role' => 'button']) ?>
                        <?= $this->Form->button(__('Delete'), ['class' => 'btn btn-warning btn-delete-po', 'data-toggle' => 'popover']) ?>
                        </div>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</div>

<div class="row row-top-space">
    <div class="col-md-4 col-md-offset-4 text-center">
        <button type="button" class="btn btn-default btn-complete-conditions"><?= __('Complete') ?></button>
    </div>
</div>
<div class="row row-top-space">
    <div class="col-md-12 text-right">
        <a href="#goToSetting" class="btn btn-info" role="button">
            <span class="glyphicon glyphicon-arrow-up"></span> <?= __('Go to Setting') ?>
        </a>
    </div>
</div>

<!-- modal 
//<?= $this->HtmlModal->modalNoHeader('modalAdditionalSettings', ' modal-lg') ?>
<div class="row">
    <div class="col-md-10 col-md-offset-1">
        <div class="settings">
            <div class="row row-item">
                <div class="col-md-3">
                    <label for="">//<?= __('Product Brand') ?></label>
                    //<?php
//                        echo $this->Form->select('role', unserialize(ROLE) , [
//                            'label' => false,
//                            'class' => 'form-control',
//                            'empty' => false
//                        ]);
//                    ?>
                </div>
                <div class="col-md-3">
                    <label for="">//<?= __('Product') ?></label>
                    //<?php
//                        echo $this->Form->select('role', unserialize(ROLE) , [
//                            'label' => false,
//                            'class' => 'form-control',
//                            'empty' => false
//                        ]);
//                    ?>
                </div>
                <div class="col-md-3">
                    <label for="">//<?= __('Type') ?></label>
                    //<?php
//                        echo $this->Form->select('role', unserialize(ROLE) , [
//                            'label' => false,
//                            'class' => 'form-control',
//                            'empty' => false
//                        ]);
//                    ?>
                </div>
                <div class="col-md-2">
                    <label for="">//<?= __('Value') ?></label>
                    //<?php
//                        echo $this->Form->input('role', [
//                            'type' => 'text',
//                            'label' => false,
//                            'class' => 'form-control'
//                        ]);
//                    ?>
                </div>
                <div class="col-md-1 text-center minus-icon" style="display: none;">
                    <label for="">&nbsp;</label>
                    <button type="button" class="btn btn-default remove-settings">
                        <span class="glyphicon glyphicon-minus-sign"></span>
                    </button>
                </div>
            </div>
        </div>        
    </div>
</div>
<br />
<div class="row row-top-space">
    <div class="col-md-10 col-md-offset-1 text-center">
        <button type="button" class="btn btn-default add-settings">
            <span class="glyphicon glyphicon-plus-sign"></span>
        </button>
    </div>
</div>
<div class="row row-top-space">
    <div class="col-md-10 col-md-offset-1 text-center">
        <button class="btn btn-default btn-gap">//<?= __('Save') ?></button>
        <button class="btn btn-default">//<?= __('Cancel') ?></button>
    </div>
</div>
//<?= $this->HtmlModal->modalFooter(); ?>
<!-- /end modal -->

<!-- modal change status -->
<?= $this->HtmlModal->modalNoHeader('modalChangeStatus', ' modal-sm') ?>
<div class="row">
    <div class="col-md-12">
        <label data-status="canceled" class="label-status label-canceled"><?= __('Canceled') ?></label>
        <label data-status="completed" class="label-status label-completed"><?= __('Completed') ?></label>
        <label data-status="shipped" class="label-status label-shipped"><?= __('Shipped') ?></label>
        <label data-status="pending" class="label-status label-pending"><?= __('Pending') ?></label>
    </div>
</div>
<?php
    echo $this->Form->create(isset($user) ? $user : '', [
            'class' => 'form-change-status',
            'autocomplete' => 'off'
        ]
    );
    echo $this->Form->hidden('status', ['class' => 'change-status']);
    echo $this->Form->end();
?>
<?= $this->HtmlModal->modalFooter(); ?>
<!-- /end modal change status -->

<!-- modal add po -->
<?= $this->HtmlModal->modalHeader('modalAddPo', __('New PO')) ?>
<?php
    echo $this->Form->create(isset($user) ? $user : '', [
            'class' => 'form-add-po form-horizontal',
            'autocomplete' => 'off'
        ]
    );
?>
<div class="form-group">
    <label for="" class="control-label col-md-3 col-md-offset-1"><?= __('Supplier Name') ?></label>
    <div class="col-md-7">
        <?= $this->Form->input('name', ['class' => 'form-control', 'label' => false]) ?>
    </div>
</div>
<div class="form-group">
    <label for="" class="control-label col-md-3 col-md-offset-1"><?= __('Issue Date') ?></label>
    <div class="col-md-7">
        <div class="input-group">
            <?= $this->Form->input('date', ['class' => 'form-control', 'label' => false]) ?>
            <div class="input-group-btn">
                <button type="button" class="btn btn-default" aria-label="Calendar">
                    <span class="glyphicon glyphicon-calendar"></span>
                </button>
            </div>
        </div>
    </div>
</div>
<div class="form-group">
    <label for="" class="control-label col-md-3 col-md-offset-1"><?= __('Currency') ?></label>
    <div class="col-md-7">
        <?php
            $options = ['USD', 'SGD', 'JPY'];
        ?>
        <?= $this->Form->select('currency', $options, ['class' => 'form-control', 'label' => false, 'empty' => false]) ?>
    </div>
</div>
<div class="form-group">
    <div class="col-md-3 col-md-offset-4">
        <button type="button" class="btn btn-primary submit-po"><?= __('Create') ?></button>
    </div>
</div>
<?php
    echo $this->Form->end();
?>
<?= $this->HtmlModal->modalFooter(); ?>
<!-- /end modal add po -->

<!-- modal invoice -->
<?= $this->HtmlModal->modalHeader('modalInvoice', __('Invoice of [SUPPLIER NAME]')) ?>
<?php
    echo $this->Form->create(isset($user) ? $user : '', [
            'class' => 'form-add-invoice form-horizontal',
            'type' => 'file',
            'autocomplete' => 'off'
        ]
    );
?>
<div class="form-group">
    <label for="" class="control-label col-md-3 col-md-offset-1"><?= __('Invoice Number') ?></label>
    <div class="col-md-7">
        <?= $this->Form->input('invoice_number', ['class' => 'form-control', 'label' => false]) ?>
    </div>
</div>
<div class="form-group">
    <label for="" class="control-label col-md-3 col-md-offset-1"><?= __('PDF File') ?></label>
    <div class="col-md-7">
        <?= $this->Form->input('file', ['type' => 'file', 'class' => 'invoice-upload', 'label' => false]) ?>
    </div>
</div>
<div class="form-group">
    <div class="col-md-3 col-md-offset-4">
        <button type="button" class="btn btn-primary submit-invoice"><?= __('Save') ?></button>
    </div>
</div>
<?php
    echo $this->Form->end();
?>
<?= $this->HtmlModal->modalFooter(); ?>
<!-- /end modal invoice -->

<!-- modal discount conditions -->
<?= $this->HtmlModal->modalHeader('modalDiscountCondition', __('Discount Conditions')) ?>
<div class="row">
    <div class="col-md-12">
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>#</th>
                    <th><?= __('Discount Type') ?></th>
                    <th><?= __('Range Type') ?></th>
                    <th><?= __('From') ?></th>
                    <th><?= __('To') ?></th>
                    <th>&nbsp;</th>            
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>1</td>
                    <td>FOC</td>
                    <td>Quantity</td>
                    <td>21</td>
                    <td>20</td>
                    <td><button type="button" class="btn btn-danger">View FOC</button></td>
                </tr>
                <tr class="row-highlight">
                    <td>2</td>
                    <td>FOC</td>
                    <td>Quantity</td>
                    <td>21</td>
                    <td>20</td>
                    <td><button type="button" class="btn btn-danger">View FOC</button></td>
                </tr>
                <tr>
                    <td>3</td>
                    <td>FOC</td>
                    <td>Quantity</td>
                    <td>21</td>
                    <td>20</td>
                    <td><button type="button" class="btn btn-danger">View FOC</button></td>
                </tr>
                <tr>
                    <td>4</td>
                    <td>FOC</td>
                    <td>Quantity</td>
                    <td>21</td>
                    <td>20</td>
                    <td><button type="button" class="btn btn-danger">View FOC</button></td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
<div class="row">
    <div class="col-md-12 text-center">
        <button type="button" class="btn btn-default" data-dismiss="modal"><?= __('Close') ?></button>
    </div>
</div>
<?= $this->HtmlModal->modalFooter(); ?>
<!-- /end modal discount conditions -->

<!-- modal product specifications -->
<?= $this->HtmlModal->modalHeader('modalProductSpecification', __('Select Specification and Dosage'), ' modal-sm') ?>
<div class="row">
    <div class="col-md-12">
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>&nbsp;</th>
                    <th><?= __('Product Name') ?></th>
                    <th><?= __('Packing') ?></th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td><input type="checkbox" class="chk-select-spec" /></td>
                    <td>Product A</td>
                    <td>4 vials  x  5 ml</td>
                </tr>
                <tr class="row-highlight">
                    <td><input type="checkbox" class="chk-select-spec" /></td>
                    <td>Product C</td>
                    <td>5 vials  x  5 ml</td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
<div class="row">
    <div class="col-md-12 text-center">
        <button type="button" class="btn btn-default select-spec" data-dismiss="modal"><?= __('Select') ?></button>
    </div>
</div>
<?= $this->HtmlModal->modalFooter(); ?>
<!-- /end modal product specifications -->

<script>
    $(function() {
        $('#datepicker').datepicker({
            dateFormat: 'yy-mm-dd'
        }).datepicker('setDate', '0');
        $('#trigger-calendar').click(function() {
            $('#datepicker').focus();
        });

        $('.chk-manual-foc').on('click', function() {
            if ($(this).is(':checked')) {
                $('.original-foc').removeClass('hidden-item');
                $('.btn-view-discount').addClass('hidden-item');
            } else {
                $('.btn-view-discount').removeClass('hidden-item');
                $('.original-foc').addClass('hidden-item');
            }
        });

        $('.dr-product').on('change', function() {
            if (parseInt($(this).val()) === 3) {
                $('#modalProductSpecification').modal({
                    backdrop: 'static',
                    keyboard: false
                });
            }
        });

//        $('body').on('click', '.add-settings', function() {
//            var clone_item = $('.row-item:last').clone().addClass('row-top-space');
//            clone_item.find('.minus-icon').show();
//            $('.settings').append(clone_item);
//        });
//
//        $('body').on('click', '.remove-settings', function() {
//            $(this).parents('.row-item').remove();
//        });
//
//        $('body').on('keyup', '.settings-value', function() {
//            var currency = $('.settings-value').val();
//            var currency_type = $('.settings-currency-type').val();
//            if (currency.length && currency_type.length) {
//                $('.settings-additional').show();
//            } else {
//                $('.settings-additional').hide();
//            }
//        });
//
//        $('body').on('change', '.settings-currency-type', function() {
//            var currency = $('.settings-value').val();
//            var currency_type = $(this).val();
//            if (currency.length && currency_type.length) {
//                $('.settings-additional').show();
//            } else {
//                $('.settings-additional').hide();
//            }
//        });
//
//        $('body').on('click', '.settings-additional', function() {
//            $('#modalAdditionalSettings').modal({
//                backdrop: 'static',
//                keyboard: false
//            });
//        });

        var incomplete = 0;
        $('.btn-complete-conditions').on('click', function() {
            incomplete = 1;
        });

        window.onbeforeunload = function() {
            if (incomplete === 0) {
                return '<?= __('Are you sure to leave this page without saving?') ?>';
            }
            return null;
        };

        //view discount conditions
        $('body').on('click', '.popup-discount', function() {
            $('#modalDiscountCondition').modal({
                backdrop: 'static',
                keyboard: false
            });
        });

        //open add new po
        $('body').on('click', '.btn-add-po', function() {
            $('#modalAddPo').modal({
                backdrop: 'static',
                keyboard: false
            });
        });

        //open invoice modal
        $('body').on('click', '.btn-invoice', function() {
            $('#modalInvoice').modal({
                backdrop: 'static',
                keyboard: false
            });
        });

        $('body').on('click', '.status', function() {
            var status = $(this).data('status');
            $('.label-status').each(function() {
                if ($(this).data('status') === status) {
                    $(this).attr('disabled', true);
                }
            });
            $('#modalChangeStatus').modal({
                backdrop: 'static',
                keyboard: false
            });
        });
        var msg = '<?= __('Are you sure to stop this condition?') ?>';
        confirm_yesno('btn-stop-condition', msg, 'left', 'Stop item?');

        var msg = '<?= __('Are you sure to restore this condition?') ?>';
        confirm_yesno('btn-restore-condition', msg, 'left', 'Stop item?');

        var msg = '<?= __('Are you sure to physically delete this condition?') ?>';
        confirm_yesno('btn-remove-condition', msg, 'left', 'Stop item?');

        $('body').on('click', '.confirm-no', function() {
            $(this).parents('.popover').prev('button').click();
        });

        $('body').on('click', '.confirm-yes', function() {

        });

        var data = '<table class="table">';
        data += '<thead><tr><th>Product Brand</th><th>Product</th><th>Type</th><th>Value</th></tr></thead>';
        data += '<tbody><tr><td>Brand A</td><td>Product A (Packaging)</td><td>Fixed Quantity</td><td>100</td></tr>';
        data += '<tr><td>Brand A</td><td>Product A (Packaging)</td><td>Fixed Quantity</td><td>100</td></tr></tbody>';
        data += '</table>';
        var tmp = '<div class="popover" role="tooltip"><div class="arrow"></div><h3 class="popover-title"></h3>';
        tmp += '<div class="popover-content"></div></div>';
        var options = {
            placement: 'right',
            title: '<?= __('FOC Product List') ?>',
            html: true,
            content: data,
            container: 'body',
            template: tmp
        };
        $('.foc-label').popover(options);
    });
</script>