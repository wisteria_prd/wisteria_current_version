<?php $action = $this->request->action; ?>
<section id="register_form">
    <?php
    echo $this->Form->create($data, [
        'role' => 'form',
        'class' => 'form-horizontal form-product-brand',
        'name' => 'product_brand_form',
    ]);
    if (!$data->isNew()) {
        echo $this->Form->hidden('id');
    }
    ?>
    <div class="form-group custom-select">
        <label class="col-sm-2 control-label">
            <?php
            echo __('PRODUCT_TXT_MANUFACTURER') . $this->Comment->formAsterisk(); ?>
        </label>
        <div class="col-md-4 col-lg-4 col-sm-4">
            <?php
            $manufacturer = [];
            if (!$data->isNew()) {
                $manufacturer = [
                    $data->manufacturer_id => $this->Comment->getFieldByLocal($data->manufacturer, $locale),
                ];
            }
            echo $this->Form->select('manufacturer_id', $manufacturer, [
                'class' => 'form-control',
                'id' => 'manufacturer-id',
                'empty' => ['' => __('PRODUCT_BRAND_TXT_SELECT_MANUFACTURER')],
                'required' => false,
            ]); ?>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label">
            <?php
            echo __('PRODUCT_TXT_PRODUCT_BRAND') . $this->Comment->formAsterisk(); ?>
        </label>
        <div class="col-lg-4 col-md-4 col-sm-4">
            <?php
            echo $this->Form->text('name_en', [
                'class' => 'form-control',
                'placeholder' => __('PRODUCT_BRAND_TXT_ENTER_PRODUCT_BRAND_NAME_EN'),
                'label' => false,
                'required' => false,
            ]); ?>
        </div>
        <div class="col-lg-4 col-sm-4 col-md-4">
            <?php
            echo $this->Form->text('name', [
                'class' => 'form-control',
                'placeholder' => __('PRODUCT_BRAND_TXT_ENTER_PRODUCT_BRAND_NAME_JP'),
                'label' => false,
                'required' => false,
            ]); ?>
        </div>
    </div>
    <div class="form-group">
        <div class="col-lg-6 col-md-6 col-sm-6">
            <label class="col-sm-4 control-label">
                <?php
                echo __('PRODUCT_BRAND_TXT_PURCHASE_AVAILABLE'); ?>
            </label>
            <div class="col-sm-8">
                <?php
                echo $this->Form->checkbox('is_purchase'); ?>
            </div>
        </div>
    </div>
    <div class="form-group">
        <div class="col-lg-6 col-md-6 col-sm-6">
            <label class="col-sm-4 control-label">
                <?php
                echo __('PRODUCT_BRAND_TXT_SALE_AVAILABLE'); ?>
            </label>
            <div class="col-sm-8">
                <?php
                echo $this->Form->checkbox('is_sale'); ?>
            </div>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label">
            <?php
            echo __('PRODUCT_BRAND_TXT_URL'); ?>
        </label>
        <div class="col-lg-4 col-md-4 col-sm-4">
            <?php
            echo $this->Form->text('url', [
                'class' => 'form-control',
                'placeholder' => __('PRODUCT_BRAND_TXT_ENTER_URL'),
                'label' => false,
                'required' => false,
            ]); ?>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label">
            <?php
            echo __('PRODUCT_BRAND_TXT_REMARKS'); ?>
        </label>
        <div class="col-lg-8 col-md-8 col-sm-8">
            <?php
            echo $this->Form->textarea('remarks', [
                'class' => 'form-control',
                'placeholder' => __('PRODUCT_BRAND_TXT_ENTER_REMARKS'),
                'required' => false,
                'label' => false,
            ]); ?>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label">
            <?php
            echo __('PRODUCT_BRAND_TXT_DOCUMENTS'); ?>
        </label>
        <div class="col-lg-8 col-sm-8 col-md-8">
            <div class="dropzone dropzone-wrap">
                <?php
                $files = [];
                if(isset($medias)) {
                    foreach ($medias as $key => $file) {
                        $files[] = $file->file_name;
                    }
                }
                $files1 = implode(',', $files);
                echo $this->Form->hidden('files', [
                    'class' => 'form-control media-file',
                    'value' => $files1
                ]);
                ?>
                <div class="bottom-class text-center">
                    <div class="upload-file">
                        <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="form-group">
        <?php
        echo $this->ActionButtons->btnSaveCancel('ProductBrands', $action); ?>
    </div>
    <?php echo $this->Form->end(); ?>
</section>

<script>
    (function(e){
        Dropzone.autoDiscover = false;
        var file_array = [];
        var priority_array = [];
        var filename = $('.media-file').val();
        if (filename !== '') {
            file_array = filename.split(',');
        }

        dropzone();

        function dropzone()
        {
            // Dropzone certificate upload
            $('.dropzone-wrap').dropzone({
                url: BASE + 'Medias/upload',
                paramName: 'file',
                acceptedFiles: '.png, .gif, .jpg, .jpeg, .PNG, .GIF, .JPG, .JPEG, .pdf, .csv',
                dictDefaultMessage: '<?php echo __('MSG_DROPZONE'); ?>',
                dictCancelUpload: '',
                dictCancelUploadConfirmation: '',
                maxFilesize: 100,
                previewTemplate: preview_template(),
                init: function () {
                    thisDropzone = this;
                    for (var index = 0; index < file_array.length; index++) {
                        var mockFile = {
                            name: file_array[index],
                            type: file_array[index].split('.').pop().toLowerCase(),
                            size: 75,
                            path: BASE + 'img/uploads/' + file_array[index]
                        };
                        thisDropzone.options.addedfile.call(thisDropzone, mockFile);
                        thisDropzone.options.thumbnail.call(thisDropzone, mockFile, mockFile.path);
                        var content = mockFile.previewElement;

                        if (mockFile.type === 'pdf') {
                            var canvas_id = 'canvas-' + index;
                            var full_path = location.protocol + '//' + location.host + mockFile.path;
                            $(content).find('.dz-image').append('<canvas id="' + canvas_id + '">');
                            read_pdf_file(content, full_path, canvas_id, index);
                            $(content).find('.dz-image img').css({'width': '0','height': '0'});
                        }
                    }
                    $('.dz-progress').hide();

                    thisDropzone.on('success', function (file, result, events) {
                        var extension = file.name.split('.').pop().toLowerCase();
                        var canvas = file.previewElement.querySelector('canvas');
                        var fileName = file.previewElement.querySelector('.dz-image');
                        $(fileName).attr('data-name', JSON.parse(result));
                        file.previewElement.querySelector('img').alt = JSON.parse(result);

                        if (extension === 'pdf') {
                            var content = file.previewElement;
                            $(content).find('.dz-image img').css({
                                'width': '0',
                                'height': '0'
                            });
                            $(content).find('.dz-image').append('<canvas></canvas>');
                            convert_pdf_to_canvas(file, content);
                        }

                        file_array.push(JSON.parse(result));
                        $('.media-file').val(file_array);
                        if ($('.media-file').val()) {
                            update_file();
                        }
                    });

                    thisDropzone.on('removedfile', function(file) {
                        update_file();
                        if ($('.media-file').val()) {
                            $('.dz-default').hide();
                        } else {
                            $('.dz-default').show();
                        }
                    });
                }
            });
        }

        // dropzone preview template
        function preview_template()
        {
            var element = '<div class="dz-preview ui-sortable-handle dz-image-preview">'
                        + '<div class="dz-image"><img data-dz-thumbnail/></div><a class="dz-remove" href="javascript:undefined;" data-dz-remove=""><span><i class="fa fa-trash-o" aria-hidden="true"></i></span></a></div></div>';
            return element;
        }

        // convert pdf file to canvas
        function convert_pdf_to_canvas(file, content)
        {
            PDFJS.disableWorker = true;
            fileReader = new FileReader();
            fileReader.onload = function(ev) {
                PDFJS.getDocument(fileReader.result).then(function getPdfHelloWorld(file) {
                    file.getPage(1).then(function getPageHelloWorld(page) {
                        var scale = 1.5;
                        var viewport = page.getViewport(scale);
                        var canvas = $(content).find('.dz-image').find('canvas')[0];
                        var context = canvas.getContext('2d');
                        canvas.height = viewport.height;
                        canvas.width = viewport.width;
                        var task = page.render({
                            canvasContext: context,
                            viewport: viewport
                        });
                        });
                    });
                };
                fileReader.readAsArrayBuffer(file);
        }

        // read pdf file from server
        function read_pdf_file(content, path, id, index)
        {
            PDFJS.getDocument(path).promise.then(function (pdf) {
                pdf.getPage(1).then(function getPageHelloWorld(page) {
                    var scale = 1.5;
                    var viewport = page.getViewport(scale);
                    var canvas = document.getElementById(id);
                    var context = canvas.getContext('2d');
                    canvas.height = viewport.height;
                    canvas.width = viewport.width;
                    var renderContext = {
                        canvasContext: context,
                        viewport: viewport
                    };
                    page.render(renderContext);
                });
            });
        }

        // update file after sort
        function update_file()
        {
            var new_file = [];
            var img_list = $('.dropzone-wrap').find('.dz-image');

            $.each(img_list, function(i, v) {
                var file_name = $(this).find('img').attr('alt');
                new_file.push(file_name);
            });

            if (new_file) {
                $('.media-file').val(new_file);
            }
        }
    })();

    $(function(e) {
        $('#manufacturer-id').select2({
            width: '100%',
            minLength: 0,
            ajax: {
                url: BASE + 'Manufacturers/getListOfManufacturersByAjax',
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    let query = {
                        keyword: params.term,
                        type: $('body').find('select[name="type"]').val()
                    };
                    return query;
                },
                processResults: function (data) {
                    let dataSource = [];
                    if (data.data === null && data.data === 'undefined') {
                        return false;
                    }
                    $.each(data.data, function(i, v) {
                        dataSource.push({
                            id: v.id,
                            text: getNameByLocal(LOCALE, v)
                        });
                    });
                    return { results: dataSource };
                }
            }
        });

        $('#btn-register').on('click', function() {
            var form = $('form[name="product_brand_form"]');
            var params = {
                type: 'POST',
                url: BASE + 'ProductBrands/saveOrUpdate',
                data: $(form).serialize(),
                beforeSend: function() {
                    $(form).find('.error-message').remove();
                    $.LoadingOverlay('show');
                }
            };
            ajaxRequest(params, SESSION_TIMEOUT, function(data) {
                if (data.message === MSG_ERROR) {
                    showMessageError(form, data.data);
                    return false;
                }
                location.href = BASE + 'ProductBrands';
            });
        });

        $('.upload-file').on('click', function() {
            $(this).parent().parent().trigger('click');
        });

        // Sort
        $('.dropzone-wrap').sortable({
            items: '.dz-preview',
            cursor: 'move',
            opacity: 0.5,
            containment: '.dropzone-wrap',
            distance: 20,
            tolerance: 'pointer',
            update: function (e, ui) {
                update_file();
            }
        });
    });
</script>
<?php
    echo $this->Html->css([
        'jquery-ui',
    ]);
    echo $this->Html->script([
        'jquery-sortable-min',
        'processing-api.min',
        'pdf',
        'dropzone.min',
        'jquery-ui',
    ], ['block' => 'script']);
