
<?php $action = $this->request->action; ?>
<section id="register_form">
    <?php echo $this->Form->create($data, [
        'role' => 'form',
        'class' => 'form-horizontal form-product',
        'name' => 'manufacturer_form',
        'type' => 'file',
    ]); ?>
    <div class="form-group">
        <label class="col-sm-2 control-label"><?= __('TXT_OLD_CODE') ?><?= $this->Comment->formAsterisk() ?></label>
        <div class="col-lg-4 col-md-4 col-sm-4">
            <?php echo $this->Form->input('code', [
                'type' => 'text',
                'class' => 'form-control',
                'placeholder' => __('TXT_ENTER_OLD_CODE'),
                'label' => false,
                'required' => false
            ]); ?>
            <span class="help-block help-tips" id="error-code"></span>
        </div>
    </div>
    <div class="form-group custom-select">
        <label class="col-sm-2 control-label"><?= __('PRODUCT_TXT_MANUFACTURER'); ?></label>
        <div class="col-md-4 col-lg-4 col-sm-4">
            <?php
            $manuf = [];
            if ($manufacturer_list) {
                foreach ($manufacturer_list as $m) {
                    $manuf[$m->id] = $this->Comment->nameEnOrJp($m->name, $m->name_en, $en);
                }
            }
            echo $this->Form->input('manufacturer_id', [
                'type' => 'select',
                'class' => 'form-control',
                'empty' => ['' => __('PRODUCT_TXT_SELECT_MANUFACTURER')],
                'label' => false,
                'value' => isset($product_brand) ? $product_brand->manufacturer_id : -1,
                'options' => $manuf
            ]);
            ?>
            <span class="help-block help-tips" id="error-manufacturer_id"></span>
        </div>
    </div>
    <div class="form-group custom-select">
        <label class="col-sm-2 control-label">
            <?php
            echo __('PRODUCT_TXT_PRODUCT_BRAND'); ?>
        </label>
        <div class="col-md-4 col-lg-4 col-sm-4">
            <?php
            echo $this->Form->input('product_brand_id', [
                'type' => 'select',
                'class' => 'form-control',
                'label' => false,
                'empty' => ['' => __('PRODUCT_TXT_SELECT_PRODUCT_BRAND')]
            ]); ?>
            <span class="help-block help-tips" id="error-product_brand_id"></span>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label">
            <?php
            echo __('PRODUCT_TXT_PRODUCT') . $this->Comment->formAsterisk(); ?>
        </label>
        <div class="col-lg-4 col-md-4 col-sm-4">
            <?php
            echo $this->Form->input('name_en', [
                'type' => 'text',
                'class' => 'form-control',
                'placeholder' => __('PRODUCT_TXT_PLACEHOLDER_PRODUCT_NAME_EN'),
                'label' => false,
                'required' => false
            ]); ?>
            <span class="help-block help-tips" id="error-name_en"></span>
        </div>
        <div class="col-lg-4 col-sm-4 col-md-4">
            <?php
            echo $this->Form->input('name', [
                'type' => 'text',
                'class' => 'form-control',
                'placeholder' => __('PRODUCT_TXT_PLACEHOLDER_PRODUCT_NAME_JP'),
                'label' => false,
                'required' => false
            ]); ?>
            <span class="help-block help-tips" id="error-name"></span>
        </div>
    </div>
    <div class="form-group custom-select">
        <label class="col-sm-2 control-label">
            <?php
            echo __('PRODUCT_TXT_PRODUCT_CATEGORY'); ?>
        </label>
        <div class="col-lg-4 col-md-4 col-sm-4">
            <?php
            echo $this->Form->select('type', [
                TYPE_MEDICINAL_PRODUCT => __d('product', 'TXT_MEDICINAL_PRODUCT'),
                TYPE_MEDICIAL_DEVICE => __d('product', 'TXT_MEDICAL_DEVICE'),
                NORMAL_TYPE => __d('product', 'TXT_NORMAL'),
            ], [
                'class' => 'form-control',
                'empty' => ['' => __('PRODUCT_TXT_SELECT_PRODUCT_CATEGORY')],
                'label' => false
            ]); ?>
            <span class="help-block help-tips" id="error-type"></span>
        </div>
    </div>
    <div class="form-group">
        <div class="col-lg-6 col-md-6 col-sm-6">
            <label class="col-sm-4 control-label">
                <?php
                echo __('PRODUCT_TXT_PURCHASE_AVAILABILITY'); ?>
            </label>
            <div class="col-sm-8">
                <?php
                echo $this->Form->checkbox('purchase_flag'); ?>
            </div>
            <span class="help-block help-tips" id="error-purchase_flag"></span>
        </div>
    </div>
    <div class="form-group">
        <div class="col-lg-6 col-md-6 col-sm-6">
            <label class="col-sm-4 control-label">
                <?php
                echo __('PRODUCT_TXT_SALE_AVAILABILITY'); ?>
            </label>
            <div class="col-sm-8">
                <?php
                echo $this->Form->checkbox('sell_flag'); ?>
            </div>
            <span class="help-block help-tips" id="error-sell_flag"></span>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label">
            <?php
            echo __('PRODUCT_TXT_URL'); ?>
        </label>
        <div class="col-lg-4 col-md-4 col-sm-4">
            <?php
            echo $this->Form->input('url', [
                'type' => 'text',
                'class' => 'form-control',
                'placeholder' => __('TXT_ENTER_URL'),
                'label' => false,
                'required' => false
            ]); ?>
            <span class="help-block help-tips" id="error-url"></span>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label">
            <?php
            echo __('STR_USER_REMARK'); ?>
        </label>
        <div class="col-lg-8 col-md-8 col-sm-8">
            <?php
            echo $this->Form->input('remarks', [
                'type' => 'textarea',
                'class' => 'form-control',
                'placeholder' => __('STR_ENTER_REMARK'),
                'required' => false,
                'label' => false
            ]); ?>
            <span class="help-block help-tips" id="error-remarks"></span>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label">
            <?php
            echo __('TXT_DOCUMENTS'); ?>
        </label>
        <div class="col-lg-8 col-sm-8 col-md-8">
            <div class="dropzone dropzone-wrap">
                <?php
                $files = [];
                if(isset($medias)) {
                    foreach ($medias as $key => $file) {
                        $files[] = $file->file_name;
                    }
                }
                $files1 = implode(',', $files);
                echo $this->Form->hidden('files', [
                    'class' => 'form-control media-file',
                    'value' => $files1
                ]);
                ?>
                <div class="bottom-class text-center">
                    <div class="upload-file">
                        <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="form-group">
        <?php
        echo $this->ActionButtons->btnSaveCancel('Products', $action); ?>
    </div>
    <?php echo $this->Form->end(); ?>
</section>

<script>
    (function(e) {
        $('#btn-register').on('click', function() {
            $('.help-block').empty().hide();
            var data = $('.form-product').serialize();
            $.ajax({
                url: $('.form-product').attr('action'),
                type: 'POST',
                dataType: 'JSON',
                data: data,
                cache: false,
                beforeSend: function() {
                    $.LoadingOverlay('show');
                },
                success: function(response) {
                    $.LoadingOverlay('hide');
                    if (response.status === 1) {
                        window.location.href = '<?php echo $this->Url->build('/products/index/'); ?>';
                    } else if (response.status === 0) {
                        $.each(response.message, function(key, elem) {
                            var validate = $('#error-' + key);
                            $.each(elem, function(k, v) {
                                validate.removeClass('help-tips')
                                    .addClass('error-tips')
                                    .text(v)
                                    .show();
                            });
                        });
                    }
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    $.LoadingOverlay('hide');
                    if (errorThrown === 'Forbidden') {
                        if (confirm('<?= __('TXT_SESSION_TIMEOUT') ?>')) {
                            location.reload();
                        }
                    }
                }
            });
        });

        Dropzone.autoDiscover = false;
        var file_array = [];
        var filename = $('.media-file').val();
        if (filename !== '') {
            file_array = filename.split(',');
        }

        dropzone();
        getData();
        get_product_brand_list();

        $('body').on('change', '#manufacturer-id', function(e) {
            var content = $('body').find('#manufacturer-id');
            var id = $(this).val();
            $(content).find('option[value="-1"]').remove();
            $.LoadingOverlay('show');

            $.get('<?php echo $this->Url->build('/products/get-product-brand/'); ?>', {id: id}, function(data) {
                $.LoadingOverlay('hide');
                var product_brand = $('body').find('#product-brand-id');
                $(product_brand).find('option').remove();
                var element = '<option value=""><?= __('TXT_SELECT_BRAND_NAME') ?></option>';

                if (data !== null && data !== undefined) {
                    var response = data.data;
                    $.each(response, function(i, v) {
                        element += '<option value="' + v.id + '">' + v.name_en + '</option>';
                    });
                }
                $(product_brand).append(element);
            }, 'json');
        });

        function get_product_brand_list()
        {
            var action = '<?php echo $this->request->action; ?>';
            if (action === 'edit') {
                var element = '';
                var id = $('body').find('#manufacturer-id').val();
                var product_brand_id = '<?php echo $data->product_brand_id; ?>';

                $.get('<?php echo $this->Url->build('/products/get-product-brand'); ?>', {id: id}, function(data) {
                    if (data !== null && data !== undefined) {
                        var response = data.data;
                        $.each(response, function(i, v) {
                            element += '<option value="' + v.id + '">' + v.name_en + '</option>';
                        });
                        $('body').find('#product-brand-id').append(element).val(product_brand_id);
                    }
                }, 'json');
            }
        }

        $('.upload-file').on('click', function() {
            $(this).parent().parent().trigger('click');
        });

        function dropzone()
        {
            // Dropzone certificate upload
            $('.dropzone-wrap').dropzone({
                url: '<?php echo $this->Url->build('/medias/upload'); ?>',
                paramName: 'file',
                acceptedFiles: '.png, .gif, .jpg, .jpeg, .PNG, .GIF, .JPG, .JPEG, .pdf, .csv',
                dictDefaultMessage: '<?= __('MSG_DROPZONE'); ?>',
                dictCancelUpload: '',
                dictCancelUploadConfirmation: '',
                maxFilesize: 100,
                previewTemplate: preview_template(),
                init: function () {
                    thisDropzone = this;
                    for (var index = 0; index < file_array.length; index++) {
                        var mockFile = {
                            name: file_array[index],
                            type: file_array[index].split('.').pop().toLowerCase(),
                            size: 75,
                            path: BASE_URL + 'img/uploads/' + file_array[index]
                        };
                        thisDropzone.options.addedfile.call(thisDropzone, mockFile);
                        thisDropzone.options.thumbnail.call(thisDropzone, mockFile, mockFile.path);
                        var content = mockFile.previewElement;

                        if (mockFile.type === 'pdf') {
                            var canvas_id = 'canvas-' + index;
                            var full_path = location.protocol + '//' + location.host + mockFile.path;
                            $(content).find('.dz-image').append('<canvas id="' + canvas_id + '">');
                            read_pdf_file(content, full_path, canvas_id, index);
                            $(content).find('.dz-image img').css({'width': '0','height': '0'});
                        }
                    }
                    $('.dz-progress').hide();

                    thisDropzone.on('success', function (file, result, events) {
                        var extension = file.name.split('.').pop().toLowerCase();
                        var canvas = file.previewElement.querySelector('canvas');
                        var fileName = file.previewElement.querySelector('.dz-image');
                        $(fileName).attr('data-name', JSON.parse(result));
                        file.previewElement.querySelector('img').alt = JSON.parse(result);

                        if (extension === 'pdf') {
                            var content = file.previewElement;
                            $(content).find('.dz-image img').css({
                                'width': '0',
                                'height': '0'
                            });
                            $(content).find('.dz-image').append('<canvas></canvas>');
                            convert_pdf_to_canvas(file, content);
                        }

                        file_array.push(JSON.parse(result));
                        $('.media-file').val(file_array);
                        if ($('.media-file').val()) {
                            update_file();
                        }
                    });

                    thisDropzone.on('removedfile', function(file) {
                        update_file();
                        if ($('.media-file').val()) {
                            $('.dz-default').hide();
                        } else {
                            $('.dz-default').show();
                        }
                    });
                }
            });
        }

        // update file after sort
        function update_file() {
            var new_file = [];
            var img_list = $('.dropzone-wrap').find('.dz-image');

            $.each(img_list, function(i, v) {
                var file_name = $(this).find('img').attr('alt');
                new_file.push(file_name);
            });

            if (new_file) {
                $('.media-file').val(new_file);
            }
        }

        // Sort
        $('.dropzone-wrap').sortable({
            items: '.dz-preview',
            cursor: 'move',
            opacity: 0.5,
            containment: '.dropzone-wrap',
            distance: 20,
            tolerance: 'pointer',
            update: function (e, ui) {
                update_file();
            }
        });

        // read pdf file from server
        function read_pdf_file(content, path, id, index)
        {
            PDFJS.getDocument(path).promise.then(function (pdf) {
                pdf.getPage(1).then(function getPageHelloWorld(page) {
                    var scale = 1.5;
                    var viewport = page.getViewport(scale);
                    var canvas = document.getElementById(id);
                    var context = canvas.getContext('2d');
                    canvas.height = viewport.height;
                    canvas.width = viewport.width;
                    var renderContext = {
                        canvasContext: context,
                        viewport: viewport
                    };
                    page.render(renderContext);
                });
            });
        }

        // convert pdf file to canvas
        function convert_pdf_to_canvas(file, content)
        {
            PDFJS.disableWorker = true;
            fileReader = new FileReader();
            fileReader.onload = function(ev) {
                PDFJS.getDocument(fileReader.result).then(function getPdfHelloWorld(file) {
                    file.getPage(1).then(function getPageHelloWorld(page) {
                        var scale = 1.5;
                        var viewport = page.getViewport(scale);
                        var canvas = $(content).find('.dz-image').find('canvas')[0];
                        var context = canvas.getContext('2d');
                        canvas.height = viewport.height;
                        canvas.width = viewport.width;
                        var task = page.render({
                            canvasContext: context,
                            viewport: viewport
                        });
                     });
                 });
             };
             fileReader.readAsArrayBuffer(file);
        }

        // dropzone preview template
        function preview_template()
        {
            var element = '<div class="dz-preview ui-sortable-handle dz-image-preview">'
                        + '<div class="dz-image"><img data-dz-thumbnail/></div><a class="dz-remove" href="javascript:undefined;" data-dz-remove=""><span><i class="fa fa-trash-o" aria-hidden="true"></i></span></a></div></div>';
            return element;
        }

        function getQueryString(name)
        {
            var regexS = '[\\?&]' + name + '=([^&#]*)',
            regex = new RegExp( regexS ),
            results = regex.exec( window.location.search );
            if( results == null ){
                return '';
            } else{
                return decodeURIComponent(results[1].replace(/\+/g, ' '));
            }
        }

        function getData()
        {
            var params = {
                des: getQueryString('des'),
                id: getQueryString('external_id')
            };

            if (params.des !== '' && params.id !== '') {
                $.get('<?php echo $this->Url->build('/products/filter-by-brand-id/'); ?>', params, function(data) {
                    if (data != null && data != undefined) {
                        var response = data.data[0];
                        var manufacturer = response.manufacturer;
                        $('#manufacturer-id, #product-brand-id').find('option').remove();
                        $('#product-brand-id').append('<option value="' + response.id + '">' + response.name_en + '</option>').attr('readonly', 'readonly');
                        $('#manufacturer-id').append('<option value="' + manufacturer.id + '">' + manufacturer.name_en + '</option>').attr('disabled', 'disabled');
                    }
                }, 'json');
            }
        }
    })();
</script>
<?php
    echo $this->Html->css([
        'jquery-ui',
    ]);
    echo $this->Html->script([
        'jquery-sortable-min',
        'processing-api.min',
        'pdf',
        'dropzone.min',
        'jquery-ui',
    ], ['block' => 'script']);
