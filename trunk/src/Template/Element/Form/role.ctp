<?php
echo $this->Form->create($data, [
    'class' => 'form-horizontal',
    'novalidate' => true,
    'autocomplete' => 'off',
    'id' => 'formBasicRegister',
]);
?>
    <div class="form-group">
        <label for="userBasicEmail" class="col-md-4 control-label">name<i class="required-indicator">＊</i></label>
        <div class="col-md-8">
            <?php
                echo $this->Form->input('name', array(
                    'label' => false,
                    'class' => 'form-control',
                ));
            ?>
            <p class="help-block help-tips" id="error_userBasicEmail"></p>
        </div>
    </div>
    <div class="form-group">
        <label for="userBasicEmail" class="col-md-4 control-label">comment</label>
        <div class="col-md-8">
            <?php
                echo $this->Form->input('comment', array(
                    'label' => false,
                    'class' => 'form-control',
                ));
            ?>
            <p class="help-block help-tips" id="error_userBasicEmail"></p>
        </div>
    </div>
<?php
if ($this->request->action == 'edit') {
    $submit_txt = __('Update');
} else {
    $submit_txt = __('Create');
}
?>
<?= $this->Form->button($submit_txt, ['class' => 'btn btn-primary pull-right']) ?>
<?= $this->Form->end() ?>