<?php
    echo $this->Form->create(isset($user) ? $user : '', [
            'class' => 'form-moq-moa',
            'autocomplete' => 'off',
            'type' => 'file'
        ]
    );
?>
    <div class="row">
        <div class="col-md-4">
            <?= $this->Form->button('CSV Upload', ['type' => 'button', 'class' => 'btn btn-default']) ?>
        </div>
    </div>
    <div class="row row-top-space">
        <div class="col-md-4">
            <label for=""><?= __('Supplier') ?></label>
            <?php
                echo $this->Form->select('role', unserialize(ROLE) , [
                    'label' => false,
                    'class' => 'form-control',
                    'select-supplier',
                    'empty' => ['' => 'Please select']
                ]);
            ?>
        </div>
        <div class="col-md-4">
            <label for=""><?= __('MOQ/MOA Condition') ?></label>
            <?php
                $options = ['PO', 'Brand', 'Product'];
                echo $this->Form->select('role', $options, [
                    'label' => false,
                    'class' => 'form-control',
                    'empty' => false
                ]);
            ?>
        </div>
        <div class="col-md-4">
            <label for=""><?= __('Type') ?></label>
            <?php
                $options = ['MOA', 'MOQ'];
                echo $this->Form->select('role', $options, [
                    'label' => false,
                    'class' => 'form-control',
                    'empty' => false
                ]);
            ?>
        </div>
    </div>
    <div class="row row-top-space">
        <div class="col-md-4">
            <label for=""><?= __('Product Brand') ?></label>
            <?php
                echo $this->Form->select('role', unserialize(ROLE) , [
                    'label' => false,
                    'class' => 'form-control',
                    'id' => 'select-product-brand',
                    'disabled' => true,
                    'empty' => false
                ]);
            ?>
        </div>
        <div class="col-md-4">
            <label for=""><?= __('Product') ?></label>
            <?php
                $options = ['PO', 'Brand', 'Product'];
                echo $this->Form->select('role', $options, [
                    'label' => false,
                    'class' => 'form-control',
                    'id' => 'select-product',
                    'disabled' => true,
                    'empty' => false
                ]);
            ?>
        </div>
    </div>
    <div class="row row-top-space">
        <div class="col-md-4">
            <label for=""><?= __('Range Value') ?></label>
            <?php
                echo $this->Form->select('role', unserialize(ROLE) , [
                    'label' => false,
                    'class' => 'form-control',
                    'empty' => false
                ]);
            ?>
        </div>
        <div class="col-md-4">
            <label for=""><?= __('Currency Type') ?></label>
            <?php
                $options = ['PO', 'Brand', 'Product'];
                echo $this->Form->select('role', $options, [
                    'label' => false,
                    'class' => 'form-control',
                    'empty' => false
                ]);
            ?>
        </div>
    </div>
    <div class="row row-top-space">
        <div class="col-md-4">
        <?php
            echo $this->Form->button('Add', [
                'class' => 'btn btn-primary',
                'id' => 'submit'
                ]
            );
        ?>
        </div>
    </div>
<?php echo $this->Form->end(); ?>

<script>
    $(function() {
        $('#select-supplier').on('change', function() {
            $.ajax({
                url: '',
                type: 'GET',
                dataType: 'HTML',
                beforeSend: function() {
                    $('#select-product-brand').empty();
                    $('#select-product-brand').append('<option value="">Please wait...</option>');
                }
            }).done(function() {

            });
        });

        $('#select-product-brand').on('change', function() {
            $.ajax({
                url: '',
                type: 'GET',
                dataType: 'HTML',
                beforeSend: function() {
                    $('#select-brand').empty();
                    $('#select-brand').append('<option value="">Please wait...</option>');
                }
            }).done(function() {

            });
        });
    });
</script>