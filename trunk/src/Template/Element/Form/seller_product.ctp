
<section id="register_form">
    <?php
    echo $this->Form->create($data, [
        'role' => 'form',
        'id' => 'seller-product',
        'class' => 'form-horizontal seller-product',
    ]);
    echo $this->Form->hidden('seller_brand_id'); ?>
    <div class="form-group custom-select">
        <div class="col-md-6 col-lg-6 col-sm-6">
            <label class="col-sm-4 control-label">
                <?php
                echo __d('seller_product', 'TXT_SUPPLIER') . $this->Comment->formAsterisk(); ?>
            </label>
            <div class="col-sm-8 custom-select">
                <?php
                echo $this->Form->select('seller_id', $sellers, [
                    'id' => 'seller-id',
                    'class' => 'form-control',
                    'label' => false,
                    'required' => false,
                    'empty' => ['' => __d('seller_product', 'TXT_SELECT_SUPPLIER')],
                    'value' => $this->request->query('seller_id'),
                ]); ?>
            </div>
        </div>
    </div>
    <div class="form-group custom-select">
        <div class="col-md-6 col-lg-6 col-sm-6">
            <label class="col-sm-4 control-label">
                <?php
                echo __d('seller_product', 'TXT_MANUFACTURER') . $this->Comment->formAsterisk(); ?>
            </label>
            <div class="col-sm-8 custom-select">
                <?php
                echo $this->Form->select('seller_id', $sellers, [
                    'id' => 'seller-id',
                    'class' => 'form-control',
                    'label' => false,
                    'required' => false,
                    'empty' => ['' => __d('seller_product', 'TXT_SELECT_MANUFACTURER')],
                    'value' => $this->request->query('seller_id'),
                ]); ?>
            </div>
        </div>
    </div>
    <div class="form-group">
        <div class="col-md-6 col-lg-6 col-sm-6">
            <label class="col-sm-4 control-label">
                <?php
                echo __d('seller_product', 'TXT_PRODUCT_BRAND') . $this->Comment->formAsterisk(); ?>
            </label>
            <div class="col-sm-8 custom-select">
                <?php
                $options = [];
                if ($sellerBrands &&  $sellerBrands->product_brand) {
                    $prd_brand = $sellerBrands->product_brand;
                    $options[] = [
                        'value' => $prd_brand->id,
                        'text' => $this->Comment->getFieldByLocal($prd_brand, $locale),
                    ];
                }
                echo $this->Form->select('brand_id', $options, [
                    'id' => 'brand-id',
                    'class' => 'form-control',
                    'label' => false,
                    'required' => false,
                    'empty' => ['' => __d('seller_product', 'TXT_SELECT_PRODUCT_BRAND')],
                    'value' => $this->request->query('product_brand_id'),
                ]); ?>
            </div>
        </div>
    </div>
    <div class="form-group">
        <div class="col-md-6 col-lg-6 col-sm-6">
            <label class="col-sm-4 control-label">
                <?php
                echo __d('seller_product', 'TXT_PRODUCT'); ?>
            </label>
            <div class="col-sm-7 pr-detail-wrap">
                <div id="jstree">
                    <?php if ($sellerBrands && $sellerBrands->seller_products): ?>
                        <ul>
                            <?php foreach($sellerBrands->seller_products as $key => $value):
                                $item = $value->product_detail;
                                ?>
                                <li data-id="<?php echo $item->id ?>">
                                    <?php
                                    $prd_detail_name = '';
                                    if ($item->single_unit) {
                                        $prd_detail_name = $item->single_unit->single_unit_size . ' ' . $this->Comment->getFieldByLocal($item->single_unit, $locale);
                                    }
                                    if ($item->tb_pack_sizes) {
                                        $prd_detail_name .= ' X ' . $item->tb_pack_sizes->pack_size . ' ' .  $this->Comment->getFieldByLocal($item->tb_pack_sizes, $locale);
                                    }
                                    echo $prd_detail_name; ?>
                                </li>
                            <?php endforeach ?>
                        </ul>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
    <div class="form-group">
        <div class="col-lg-12 col-sm-12 col-md-12 text-center">
            <?php echo $this->Html->link(__('BTN_CANCEL'), [
                'controller' => 'seller-brands',
                'action' => 'index',
            ], [
                'class' => 'btn btn-default form_btn btn-sm btn-width',
                'id' => 'btn-cancel',
            ]); ?>
            &nbsp;&nbsp;
            <?php echo $this->Form->button(($this->request->action === 'edit') ? __('BTN_UPDATE') : __('BTN_REGISTER_SAVE'), [
                'type' => 'button',
                'class' => 'btn-register btn btn-primary form_btn btn-sm btn-width btn-submit',
            ]); ?>
            <div class="clearfix"></div>
        </div>
    </div>
    <?php echo $this->Form->end(); ?>
</section>
<?php
echo $this->Html->css([
    '/js/jstree/themes/default/style.min',
]);
echo $this->Html->script([
    'jstree/jstree.min',
    'seller_product',
], ['blog' => 'script']);
