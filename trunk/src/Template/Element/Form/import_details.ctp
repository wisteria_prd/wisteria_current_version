<?php
$action = $this->request->action;
?>
<section id="create_form">
    <div class="row">
        <div class="col-sm-12 text-center">
            <h2 style="margin-bottom: 30px;">
                <?php echo  __('TXT_DETAIL_IMPORT_INFO') ?>
            </h2>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <?php
            echo $this->Form->create($import, [
                'role' => 'form',
                'class' => 'form-horizontal form-import-details',
                'name' => 'common_form'
            ]);
            $p_number = '';
            $pch_number = $p_number = $this->request->query('pch_number');
            if ($action == 'create') {
                echo $this->Form->hidden('import_id', ['value' => $this->request->query('import_id'), 'id' => 'import-id']);
                $p_number = $this->request->query('pch_number');
            } else {
                $p_number = $import->purchase_number;
            }
            echo $this->Form->hidden('p_number', ['class' => 'p-number', 'value' => $pch_number]); //purchase number automatically get from import. It will not save just for url redirect.
            $this->Form->templates([
                'inputContainer' => '{{content}}'
            ]);
            ?>
<!--            <div class="form-group">
                <label class="control-label col-md-3 col-md-offset-2" for=""><?= __('TXT_PURCHASE_NUMBER') ?><?= $this->Comment->formAsterisk() ?></label>
                <div class="col-md-4">
                    <?php
//                        $number = [];
//                        if ($purchase_number) {
//                            foreach ($purchase_number as $num) {
//                                $number[$num->purchase->id] = $num->purchase->purchase_number;
//                            }
//                        }
//                        $default = '';
//                        if ($action == 'edit') {
//                            $default = $import->stocks[0]->purchase_id;
//                        }
//
//                        echo $this->Form->select('purchase_id', $number, [
//                            'label' => false,
//                            'class' => 'form-control',
//                            'empty' => __('TXT_SELECT_PURCHASE_NUMBER'),
//                            'default' => $default
//                        ]);
                    ?>
                    <span class="help-block help-tips" id="error-purchase_id"></span>
                </div>
            </div>-->
            <div class="form-group">
                <label class="control-label col-md-3 col-md-offset-2" for="">
                    <?php echo  __d('import_detail', 'TXT_OF#') ?><?= $this->Comment->formAsterisk() ?>
                </label>
                <div class="col-md-4">
                    <?php
                        echo $this->Form->input('purchase_number', [
                            'type' => 'text',
                            'label' => false,
                            'class' => 'form-control',
                            'value' => $p_number,
                            'placeholder' => __('TXT_ENTER_PURCHASE_NUMBER')
                        ]);
                    ?>
                    <span class="help-block help-tips" id="error-purchase_number"></span>
                </div>
            </div>
            <div class="form-group custom-select">
                <label class="control-label col-md-3 col-md-offset-2" for="">
                    <?php echo __d('import_detail', 'TXT_PRODUCT') . $this->Comment->formAsterisk(); ?>
                </label>
                <div class="col-md-4">
                    <select class="form-control" name="product_detail_id" id="product-detail-id">
                        <?php echo $this->Comment->productDetailsList($en, $products, $import->product_detail_id, $action); ?>
                    </select>
                    <span class="help-block help-tips errors" id="error-product_detail_id"></span>
                </div>
                <!-- <div class="col-md-2 field-no-padding-left">
                    <button type="button" class="btn btn-sm btn-primary btn-width" id="advanced-search">
                        <?php //echo __d('import_detail', 'TXT_ADVANCED_FIND_PRODUCTS') ?>
                    </button>
                </div> -->
            </div>
            <div class="form-group">
                <label class="control-label col-md-3 col-md-offset-2" for="">
                    <?php echo __d('import_detail', 'TXT_LOT#'); ?><?= $this->Comment->formAsterisk() ?>
                </label>
                <div class="col-md-4">
                    <?php
                        echo $this->Form->input('lot_number', [
                            'type' => 'text',
                            'label' => false,
                            'class' => 'form-control',
                            'placeholder' => __d('import_detail', 'TXT_ENTER_LOT#'),
                        ]);
                    ?>
                    <span class="help-block help-tips" id="error-lot_number"></span>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-3 col-md-offset-2" for="">
                    <?php echo __d('import_detail', 'TXT_EXPIRY') ?><?= $this->Comment->formAsterisk() ?>
                </label>
                <div class="col-md-4">
                    <div class="input-group with-datepicker">
                        <?php echo $this->Form->input('expiry_date', [
                            'class' => 'form-control datepicker',
                            'label' => false,
                            'placeholder' => __d('import_detail', 'TXT_ENTER_EXPIRY'),
                            'type' => 'text',
                            'value' => $action == 'edit' ? date('Y-m-d', strtotime($import->expiry_date)) : ''
                        ]); ?>
                        <div class="input-group-btn">
                            <button type="button" class="btn btn-default trigger-calendar" aria-label="Calendar">
                                <span class="glyphicon glyphicon-calendar"></span>
                            </button>
                        </div>
                    </div>
                    <span class="help-block help-tips" id="error-expiry_date"></span>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-3 col-md-offset-2" for="">
                    <?php echo __d('import_detail', 'TXT_QUANTITY') ?><?= $this->Comment->formAsterisk() ?>
                </label>
                <div class="col-md-4">
                    <?php
                    echo $this->Form->input('quantity', [
                        'type' => 'text',
                        'label' => false,
                        'class' => 'form-control',
                        'placeholder' => __d('import_detail', 'TXT_ENTER_QUANTITY'),
                    ]);
                    ?>
                    <span class="help-block help-tips" id="error-quantity"></span>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-3 col-md-offset-2" for="">
                    <?php echo __d('import_detail', 'TXT_FOC') ?>
                </label>
                <div class="col-md-4">
                    <div class="checkbox">
                        <label>
                            <?php echo $this->Form->input('is_foc', [
                                'type' => 'checkbox',
                                'label' => false,
                            ]); ?>
                        </label>
                    </div>
                    <span class="help-block help-tips" id="error-is_foc"></span>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-3 col-md-offset-2" for="">
                    <?php echo __d('import_detail', 'TXT_DAMAGE_A') ?>
                </label>
                <div class="col-md-4">
                    <?php
                        echo $this->Form->input('damage_a_quanity', [
                            'type' => 'text',
                            'label' => false,
                            'class' => 'form-control',
                            'placeholder' => __d('import_detail', 'TXT_ENTER_DAMAGE_A'),
                        ]);
                    ?>
                    <span class="help-block help-tips" id="error-damage_a_quanity"></span>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-3 col-md-offset-2" for="">
                    <?php echo __d('import_detail', 'TXT_DAMAGE_B') ?>
                </label>
                <div class="col-md-4">
                    <?php
                    echo $this->Form->input('damage_b_quanity', [
                        'type' => 'text',
                        'label' => false,
                        'class' => 'form-control',
                        'placeholder' => __d('import_detail', 'TXT_ENTER_DAMAGE_B'),
                    ]);
                    ?>
                    <span class="help-block help-tips" id="error-damage_b_quanity"></span>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-3 col-md-offset-2" for="">
                    <?php echo __d('import_detail', 'TXT_DAMAGE_C') ?>
                </label>
                <div class="col-md-4">
                    <?php
                    echo $this->Form->input('damage_c_quanity', [
                        'type' => 'text',
                        'label' => false,
                        'class' => 'form-control',
                        'placeholder' => __d('import_detail', 'TXT_ENTER_DAMAGE_C'),
                    ]);
                    ?>
                    <span class="help-block help-tips" id="error-damage_c_quanity"></span>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-3 col-md-offset-2" for="">
                    <?php echo __d('import_detail', 'TXT_WISE') ?>
                </label>
                <div class="col-md-4">
                    <?php
                    echo $this->Form->input('damage_f_quanity', [
                        'type' => 'text',
                        'label' => false,
                        'class' => 'form-control',
                        'placeholder' => __d('import_detail', 'TXT_ENTER_WISE_QUANTITY'),
                    ]);
                    ?>
                    <span class="help-block help-tips" id="error-damage_f_quanity"></span>
                </div>
            </div>
<!--            <div class="form-group">
                <label class="control-label col-md-3 col-md-offset-2" for=""><?= __('TXT_HAS_ISSUE') ?></label>
                <div class="col-md-4">
                    <div class="checkbox">
                        <label>
                            <?php
//                            echo $this->Form->input('is_issue', [
//                                'type' => 'checkbox',
//                                'class' => 'is-issue',
//                                'label' => false,
//                            ]);
                            ?>
                        </label>
                    </div>
                    <span class="help-block help-tips" id="error-is_issue"></span>
                </div>
            </div>-->
            <?php
                $style = ' style="display: none;"';
                $less = $style;
                if ($action == 'edit') {
                    if ($import->is_issue == 1) {
                        $style = '';
                        if (!empty($import->less_quantity)) {
                            $less = '';
                        }
                    }
                }
            ?>
<!--            <div class="issue-check"<?= $style ?>>
                <div class="form-group check-issue-type"<?= $style ?>>
                    <label class="control-label col-md-3 col-md-offset-2" for=""><?= __('TXT_ISSUE_TYPE') ?></label>
                    <div class="col-md-4">
                        <?php
//                            echo $this->Form->select('issue_type', $issue_types, [
//                                'label' => false,
//                                'class' => 'form-control issue-type',
//                                'empty' => ['' => __('TXT_SELECT_ISSUE_TYPE')]
//                            ]);
                        ?>
                        <span class="help-block help-tips errors" id="error-issue_type"></span>
                    </div>
                </div>
                <div class="form-group check-issue-less-qty"<?= $less ?>>
                    <label class="control-label col-md-3 col-md-offset-2" for=""><?= __('TXT_LESS_QTY') ?></label>
                    <div class="col-md-4">
                        <?php
//                            echo $this->Form->input('less_quantity', [
//                                'type' => 'text',
//                                'label' => false,
//                                'class' => 'form-control less-qty',
//                                'placeholder' => __('TXT_ENTER_LESS_QTY')
//                            ]);
                        ?>
                        <span class="help-block help-tips" id="error-less_quantity"></span>
                    </div>
                </div>
            </div>-->
            <div class="form-group">
                <?= $this->ActionButtons->btnSaveCancel('ImportDetails', $action); ?>
            </div>
            <?= $this->Form->end(); ?>
        </div>
    </div>
</section>

<?= $this->element('Modal/product_list') ?>

<?php
    echo $this->Html->css([
        'bootstrap-datetimepicker.min',
        'jquery-ui',
        'intlTelInput',
    ]);
?>

<script>
    (function() {
        $('body').on('click', '.is-issue', function () {
            if ($(this).is(':checked')) {
                $('.issue-check').show();
                $('.check-issue-type').show();
            } else {
                $('.issue-check').hide();
                $('.issue-type').val('').change();
                $('.less-qty').val('');
                $('.check-issue-less-qty').hide();
            }
        });

        $('body').on('change', '.issue-type', function () {
            if ($(this).val() == '<?= ISSUE_TYPE_LESS_QTY ?>') {
                $('.check-issue-less-qty').show();
            } else {
                $('.less-qty').val('');
                $('.check-issue-less-qty').hide();
            }
        });

        $('#product-detail-id').select2();

        $('.datepicker').datetimepicker({
            format: 'YYYY-MM-DD',
            minDate: '2007-01-01'
        });

        $('.trigger-calendar').click(function() {
            $(this).parents('.with-datepicker').find('.datepicker').focus();
        });

        $('#advanced-search').on('click', function () {
            var data = {};
            $.ajax({
                url: '<?= $this->Url->build(['controller' => 'Pricing', 'action' => 'productAdvancedSearch']) ?>',
                type: 'GET',
                dataType: 'HTML',
                data: data,
                beforeSend: function() {
                    $.LoadingOverlay('show');
                },
                success: function(response) {
                    $.LoadingOverlay('hide');
                    if (response) {
                        $('.product-list-content').empty().html(response);
                        $('#modal-product-list').modal('show');

                        $('.btn-get-price').attr('id', 'get-more-price');
                    }
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    $.LoadingOverlay('hide');
                    if (errorThrown === 'Forbidden') {
                        if (confirm('<?= __('TXT_SESSION_TIMEOUT') ?>')) {
                            location.reload();
                        }
                    }
                },
                complete: function(data) {
                    $('#select-product-name').select2({
                        width: '100%'
                    });
                }
            });
        });

        $('body').on('click', '.check-product', function () {
            $('.check-product').not(this).prop('checked', false);
        });

        $('body').on('click', '#select-product', function () {
            var detail_id = '';
            $('.check-product').each(function() {
                if ($(this).is(':checked')) {
                    detail_id = $(this).attr('data-id');
                   return false;
                }
            });
            $('#product-detail-id').val(detail_id).change();
            $('.btn-close-modal').click();
        });

        function countProductDetail(productFound) {
            $('.amount-found').empty().text(productFound);
        }

        $('body').on('change', '#select-manufacturer', function() {
            var manufacturer = $(this).val();
            if (manufacturer !== '') {
                $.LoadingOverlay('show');
                var url = '<?= $this->Url->build(['controller' => 'ProductBrands', 'action' => 'brandDropdown']) ?>';
                var params = { man_id: manufacturer };
                ajax_request_get(url, params, function(response) {
                    $.LoadingOverlay('hide');
                    if (response) {
                        $('#select-product-brand').removeAttr('disabled').empty().html(response);
                    } else {
                        $('#select-product-brand').find('option')
                            .remove()
                            .end();
                        $('#select-product-brand').attr('disabled', true).append('<option value=""><?= __('TXT_SELECT_BRAND_NAME') ?></option>');
                    }
                }, 'html', '<?= __('TXT_SESSION_TIMEOUT') ?>');
            } else {
                $('#select-product-brand').find('option')
                    .remove()
                    .end();
                $('#select-product-brand').attr('disabled', true).append('<option value=""><?= __('TXT_SELECT_BRAND_NAME') ?></option>');
            }
        });

        $('body').on('change', '#select-product-brand', function() {
            var brand = $(this).val();
            if (brand !== '') {
                selectModalProductList(brand);
            } else {
                selectModalProductList('');
            }
        });

        function selectModalProductList(brand_id) {
            $.ajax({
                url: '<?= $this->Url->build(['controller' => 'ProductPackageDetails', 'action' => 'productNameList']) ?>',
                type: 'GET',
                data: {brand_id: brand_id},
                dataType: 'JSON',
                beforeSend: function() {
                    $.LoadingOverlay('show');
                },
                success: function(response) {
                    $.LoadingOverlay('hide');
                    var element = '<option value=""><?= __('TXT_SEARCH') ?></option>';
                    if (response != null && response != undefined) {
                        $.each(response.data, function (i, v) {
                            element += '<option value="'+ v.id +'">'+ v.name<?= $this->request->session()->read('tb_field') ?> +'</option>';
                        });
                        $('#select-product-name').empty().append(element).select2();
                        countProductDetail(response.data.length);
                        $('.table-product-detail tbody').empty();
                    }
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    $.LoadingOverlay('hide');
                    if (errorThrown === 'Forbidden') {
                        if (confirm('<?= __('TXT_SESSION_TIMEOUT') ?>')) {
                            location.reload();
                        }
                    }
                }
            });
        }

        $('body').on('change', '#select-product-name', function() {
            var product_id = $(this).val();
            var product_name = $(this).find('option:selected').text();

            if (product_id !== '') {
                productList(product_id, product_name);
            } else {
                $('.table-product-detail tbody').empty();
            }
        });

        function productList(product_id, product_name) {
            $.ajax({
                url: '<?= $this->Url->build(['controller' => 'Pricing', 'action' => 'productDetailList']) ?>',
                type: 'GET',
                data: {product_id: product_id, product_name: product_name},
                dataType: 'HTML',
                beforeSend: function() {
                    $.LoadingOverlay('show');
                },
                success: function(response) {
                    $.LoadingOverlay('hide');
                    $('.table-product-price tbody').empty().html(response);
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    $.LoadingOverlay('hide');
                    if (errorThrown === 'Forbidden') {
                        if (confirm('<?= __('TXT_SESSION_TIMEOUT') ?>')) {
                            location.reload();
                        }
                    }
                }
            });
        }

        $('#btn-register').on('click', function() {
            $('.help-block').empty().hide();
            var data = $('.form-import-details').serialize();
            $.ajax({
                url: $('.form-import-details').attr('action'),
                type: 'POST',
                dataType: 'JSON',
                data: data,
                cache: false,
                beforeSend: function() {
                    $.LoadingOverlay('show');
                },
                success: function(response) {
                    $.LoadingOverlay('hide');

                    if (response.status === 1) {
                        window.location.href = response.redirect;
                    } else if (response.status === 0) {
                        $.each(response.message, function(key, elem) {
                            var validate = $('#error-' + key);
                            $.each(elem, function(k, v) {
                                validate.removeClass('help-tips')
                                    .addClass('error-tips')
                                    .text(v)
                                    .show();
                            });
                        });
                    }
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    $.LoadingOverlay('hide');
                    if (errorThrown === 'Forbidden') {
                        if (confirm('<?= __('TXT_SESSION_TIMEOUT') ?>')) {
                            location.reload();
                        }
                    }
                }
            });
        });

    })();
</script>
