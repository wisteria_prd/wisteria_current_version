<?php $action = $this->request->action; ?>
<section id="register_form">
    <?php
    echo $this->Form->create(isset($data) ? $data : null, [
        'role' => 'form',
        'class' => 'form-horizontal purchase-conditions',
    ]);
    ?>
    <div class="form-group">
        <label class="col-sm-2 control-label">
            <?php echo __d('seller_standard_price', 'TXT_SUPPLIER') . $this->Comment->formAsterisk() ?>
        </label>
        <?php
        echo $this->Form->hidden('seller_type', [
            'class' => 'seller-type',
            'value' => isset($data['seller_product']['seller_brand']['seller']) ? $data['seller_product']['seller_brand']['seller']['type'] : '',
        ]);
        ?>
        <div class="col-sm-4 custom-select">
            <select name="seller_id" class="form-control" id="seller-id">
                <?php echo $this->Comment->supplierDropdown($sellers, $this->request->session()->read('tb_field')); ?>
            </select>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label">
            <?php echo __d('seller_standard_price', 'TXT_PRODUCT_BRAND') . $this->Comment->formAsterisk() ?>
        </label>
        <?php
        echo $this->Form->input('brand_id', [
            'type' => 'select',
            'class' => 'form-control',
            'label' => false,
            'required' => false,
            'options' => isset($product_brands) ? $product_brands : [],
            'value' => isset($data['seller_product']['seller_brand']['product_brand']) ? $data['seller_product']['seller_brand']['product_brand']['id'] : '',
            'templates' => [
                'inputContainer' => '<div class="col-sm-4 custom-select">{{content}}</div>',
            ],
            'empty' => ['' => __d('seller_standard_price', 'TXT_SELECT_PRODUCT_BRAND')]
        ]);
        ?>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label">
            <?php echo __d('seller_standard_price', 'TXT_PRODUCT') . $this->Comment->formAsterisk() ?>
        </label>
        <?php
        echo $this->Form->input('product_detail_id', [
            'type' => 'select',
            'class' => 'form-control',
            'id' => 'product-detail-id',
            'label' => false,
            'required' => false,
            'options' => isset($products) ? $products : [],
            'value' => isset($data['seller_product']['seller_brand']['product_brand']) ? $data['seller_product']['seller_brand']['product_brand']['id'] : '',
            'templates' => [
                'inputContainer' => '<div class="col-sm-4 custom-select">{{content}}</div>',
            ],
            'empty' => ['' => __d('seller_standard_price', 'TXT_SELECT_PRODUCT')]
        ]);
        ?>
    </div>
    <div class="form-group standard-price-inner main">
        <label class="col-sm-2 control-label">
            <?php echo  __d('seller_standard_price', 'TXT_STANDARD_UNIT_PRICE') . $this->Comment->formAsterisk() ?>
        </label>
        <?php
        echo $this->Form->input('price.0.price', [
            'class' => 'form-control',
            'label' => false,
            'required' => false,
            'placeholder' => __d('seller_standard_price', 'TXT_ENTER_STANDARD_UNIT_PRICE'),
            'value' => isset($data['standard_price']) ? $data['standard_price'] : '',
            'templates' => [
                'inputContainer' => '<div class="col-sm-4 custom-select">{{content}}</div>',
            ],
        ]);
        echo $this->Form->input('price.0.currency_id', [
            'type' => 'select',
            'class' => 'form-control currency-id',
            'label' => false,
            'required' => false,
            'options' => $currencies,
            'value' => isset($data['currency_id']) ? $data['currency_id'] : '',
            'templates' => [
                'inputContainer' => '<div class="col-sm-3 no-padding-left custom-select">{{content}}</div>',
            ],
            'empty' => ['' => __d('seller_standard_price', 'TXT_SELECT_CURRENCY')]
        ]);
        ?>
        <div class="col-sm-2 no-padding-left">
            <button type="button" class="btn btn-sm btn-primary btn-add-price">
                <i class="fa fa-plus" aria-hidden="true"></i>
            </button>
        </div>
    </div>
    <div class="standard-price-wrapper"></div>
    <div class="form-group">
        <?= $this->ActionButtons->btnSaveCancel('SellerStandardPrices', $action, 'btn-cancel', 'btn-register', null, 'btn-submit'); ?>
    </div>
    <?php echo $this->Form->end(); ?>
</section>
<!--Modal Message-->
<div class="modal fade" id="modal_message" data-backdrop="static" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <p class="text-center"><?= __('データが空です。') ?></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary btn-sm btn-width" data-dismiss="modal"><?= __('はい') ?></button>
            </div>
        </div>
    </div>
</div>

<script>
$(function() {
    // seller dropdown
    $('#seller-id').select2();
    $('#brand-id').select2();
    $('#product-detail-id').select2();
    // get supplier and product brand by manufacturer
    $('body').on('change', '#seller-id', function(e) {
        var that = this;
        var seller_type = $(that).find('option:selected').attr('data-type');
        $('input.seller-type').val(seller_type);
        $(that).find('option[value=""]').remove();
        var id = $(that).val();
        //console.log(id);
        if (id !== '') {
            $.LoadingOverlay('show');
            $.get('<?php echo $this->Url->build('/product-brands/get-brand-by-seller-id'); ?>',
            {id: id, seller_type: seller_type},
            function(data) {
                var options = '<option value=""><?= __('TXT_SELECT_BRAND_NAME') ?></option>';
                $.each(data.data, function(i, element) {
                    var n_name = element.name;
                    if (n_name == '') {
                        n_name = element.name_en;
                    }
                    options += '<option value="' + element.id + '">'+n_name+'</option>';
                });
                $('select#brand-id').html(options);
            }, 'json').done(function(data) {
                $.LoadingOverlay('hide');
            });
        }
    });

    // get product by product brand
    $('body').on('change', '#brand-id', function(e) {
        var that = this;
        $(that).find('option[value=""]').remove();
        var id = $(that).val();
        //console.log(id);
        if (id !== '') {
            $.LoadingOverlay('show');
            $.get('<?php echo $this->Url->build('/products/get-product-by-brand-id'); ?>',
            {id: id},
            function(data) {
                var options = '<option value=""><?= __('TXT_SELECT_PRODUCT_NAME') ?></option>';
                $.each(data.data, function(i,element) {
                    var name = element.name;
                    if (name == '') {
                        name = element.name_en;
                    }
                    options += '<option value="' + element.id + '">'+name+'</option>';
                });
                $('select#product-detail-id').html(options);
            }, 'json').done(function(data) {
                $.LoadingOverlay('hide');
            });
        }
    });

    // add standard price
    var standard_price_index = $('.standard-price-inner').length;
    $('.btn-add-price').on('click', function() {
        var html = '';
        html += '<div class="standard-price-inner">';
        html +=     '<div class="form-group">';
        html +=     '<div class="col-sm-2">&nbsp;</div>';
        html +=     '<div class="col-sm-4">';
        html +=         '<input type="text" name="price[' + standard_price_index + '][price]" class="form-control standard-price" value="" placeholder="<?= __('TXT_ENTER_STANDARD_PRICE') ?>">';
        html +=     '</div>';
        html +=     '<div class="col-md-3 custom-select no-padding-left">';
        html +=         '<select name="price[' + standard_price_index + '][currency_id]" class="form-control currency-id"></select>';
        html +=     '</div>';
        html +=     '<div class="col-sm-1 no-padding-left">';
        html +=         '<button type="button" class="btn btn-sm btn-danger remove-price">';
        html +=             '<i class="fa fa-trash" aria-hidden="true"></i>';
        html +=         '</button>';
        html +=     '</div>';
        html +=     '</div>';
        html += '</div>';
        $('.standard-price-wrapper').append(html);
        var currency_options = $('.standard-price-inner.main').find('select.currency-id > option').clone();
        $('select[name="price[' + standard_price_index + '][currency_id]"]').append(currency_options);
        standard_price_index++;
    });

    // submit data
    $('body').on('click', '.btn-submit', function(e) {
        e.preventDefault();
        $.LoadingOverlay('show');
        var form = $('form.purchase-conditions');
        $.post(form.attr('action'), form.serialize(), function(response) {
            $('p.error-message').remove();
            // success
            if (response.status === 1) {
                location.href = '<?php echo $this->Url->build('/purchase-conditions/product'); ?>';
            } else {
                $.each(response.data, function(i, element) {
                    if (i == 'price') {
                        $.each(element, function (k, elem) {
                            var name = i;
                            name += '[' + k + ']';
                            $.each(elem, function (k1, e) {
                                var n1 = name;
                                var n2 = name;
                                if (k1 == 'price') {
                                    n1 += '[price]';
                                    $('[name="' + n1 + '"]').after('<p class="error-message">'+e+'</p>');
                                } else if (k1 == 'currency') {
                                    n2 += '[currency_id]';
                                    $('[name="' + n2 + '"]').after('<p class="error-message">'+e+'</p>');
                                }
                            });
                        });
                    } else {
                        $('[name="' + i + '"]').siblings('.select2').after('<p class="error-message">'+element+'</p>');
                    }
                });
            }
        }, 'json').done(function() {
            $.LoadingOverlay('hide');
        });
    });
});
</script>
