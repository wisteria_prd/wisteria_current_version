<?php
$action = $this->request->action;
?>
<section id="create_form">
    <?php if (false) : ?>
    <div class="row">
        <div class="col-sm-12 text-center">
            <h2 style="margin-bottom: 30px;"><?= __('TXT_PAYMENT_INFO') ?></h2>
        </div>
    </div>
    <?php endif; ?>
    <div class="row">
        <div class="col-sm-10 col-sm-offset-1">
            <?php
            echo $this->Form->create($payment, [
                'role' => 'form',
                'class' => 'form-horizontal form-payments',
                'name' => 'common_form'
            ]);
            $this->Form->templates([
                'inputContainer' => '{{content}}'
            ]);
            $charge_date = '';
            $pay_date = '';
            if ($action == 'edit') {
                if (!empty($payment->payment_date)) {
                    $pay_date = date('Y-m-d', strtotime($payment->payment_date));
                }
                if (!empty($payment->bank_charge_date)) {
                    $charge_date = date('Y-m-d', strtotime($payment->bank_charge_date));
                }
            }
            ?>
            <div class="purchases">
                <div class="form-group row-purchase">
                    <label class="control-label col-md-3" for="">
                        <?= __('PAYMENT_WPO') . $this->Comment->formAsterisk() ?>
                    </label>
                    <div class="col-md-6 purchase-id-section">
                        <?php
                        if ($action == 'create') {
                            echo $this->Form->hidden('sale_payments.0.sale_id', ['class' => 'selected-sale-ids', 'value' => '']);
                            echo '<span class="help-block help-tips" id="error-sale_id"></span>';
                        } else {
                            if (!empty($payment->sale_payments)) {
                                $ids = '<table class="table-ids purchase-ids"><tbody>';
                                $s = 0;
                                $c = 0;
                                foreach ($payment->sale_payments as $s_id) {
                                    if ($s == 0) {
                                        $ids .= '<tr>';
                                    }
                                    $ids .= '<td><div class="pid purchase-id">';
                                        $ids .= '<div class="ids">'.$s_id->sale->sale_number.'</div>';
                                        $ids .= '<span data-sellerid="'.$s_id->sale->seller_id.'" data-curid="'.$s_id->sale->currency->id.'" data-curcode="'.$s_id->sale->currency->code.'" data-number="'.$s_id->sale->sale_number.'" data-amt="'.$s_id->sale->amount.'" data-id="'.$s_id->sale_id.'" class="glyphicon glyphicon-remove icon-remove-id"></span>';
                                        $ids .= '<input type="hidden" value="'.$s_id->sale_id.'" name="sale_payments['.$c.'][sale_id]" class="selected-sale-ids">';
                                    $ids .= '</div></td>';
                                    if ($s == 4) {
                                        $ids .= '</tr>';
                                    }
                                    $s++;
                                    $c++;
                                }
                                $ids .= '</tbody></table>';
                                echo $ids;
                            } else {
                                echo $this->Form->hidden('sale_payments.0.sale_id', ['class' => 'selected-sale-ids', 'value' => '']);
                                echo '<span class="help-block help-tips" id="error-sale_id"></span>';
                            }
                        }
                        ?>
                    </div>
                    <div class="col-md-1 field-no-padding-left">
                        <button type="button" class="btn btn-sm btn-primary btn-add-sale-id form-btn-add">
                            <i class="fa fa-plus" aria-hidden="true"></i>
                        </button>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-3" for="">
                    <?= __('PAYMENT_PRODUCT_AMOUNT') . $this->Comment->formAsterisk() ?>
                </label>
                <div class="col-md-6">
                    <?php
                    echo $this->Form->input('product_amount', [
                        'label' => false,
                        'class' => 'form-control',
                        'placeholder' => __('PAYMENT_PRODUCT_AMOUNT')
                    ]); ?>
                    <span class="help-block help-tips" id="error-product_amount"></span>
                </div>
            </div>
            <div class="additionals">
                <div class="form-group additional-item">
                    <label class="control-label col-md-3" for="">
                        <?= __('PAYMENT_ADDITIONAL_PAYMENT') ?>
                    </label>
                    <div class="col-md-4 field-small-padding-right">
                        <?php
                        echo $this->Form->input('additional_payments.0.description', [
                            'type' => 'text',
                            'label' => false,
                            'id' => false,
                            'class' => 'form-control descriptions',
                            'placeholder' => __('PAYMENT_ENTER_DESCRIPTION'),
                        ]); ?>
                        <span class="help-block help-tips desc-errors" id="error-description-0"></span>
                    </div>
                    <div class="col-md-2 field-small-padding-left">
                        <?php
                        echo $this->Form->input('additional_payments.0.amount', [
                            'label' => false,
                            'id' => false,
                            'class' => 'form-control amounts',
                            'placeholder' => __('PAYMENT_ENTER_AMOUNT'),
                        ]); ?>
                        <span class="help-block help-tips amt-errors" id="error-amount-0"></span>
                    </div>
                    <div class="col-md-1 field-no-padding-left">
                        <button type="button" class="btn btn-sm btn-primary btn-add-additional form-btn-add">
                            <i class="fa fa-plus" aria-hidden="true"></i>
                        </button>
                    </div>
                </div>
                <?php
                if ($action == 'edit' && !empty($payment->additional_payments)) {
                    $idx = 0;
                    foreach ($payment->additional_payments as $additional) {
                        if ($idx == 0) {
                            $idx++;
                            continue;
                        } else {
                            $p = '<div class="form-group additional-item">';
                                $p .= '<label class="control-label col-md-3" for=""></label>';
                                $p .= '<div class="col-md-4 field-small-padding-right">';
                                    $p .= $this->Form->input('additional_payments.'.$idx.'.description', [
                                        'type' => 'text',
                                        'label' => false,
                                        'id' => false,
                                        'class' => 'form-control descriptions',
                                        'placeholder' => __('TXT_ENTER_DESCRIPTION')
                                    ]);
                                    $p .= '<span class="help-block help-tips desc-errors" id="error-description-'.$idx.'"></span>';
                                $p .= '</div>';
                                $p .= '<div class="col-md-2 field-small-padding-left">';
                                    $p .= $this->Form->input('additional_payments.'.$idx.'.amount', [
                                        'label' => false,
                                        'id' => false,
                                        'class' => 'form-control amounts',
                                        'placeholder' => __('TXT_ENTER_AMOUNT')
                                    ]);
                                    $p .= '<span class="help-block help-tips amt-errors" id="error-amount-'.$idx.'"></span>';
                                $p .= '</div>';
                                $p .= '<div class="col-md-1 field-no-padding-left">';
                                    $p .= '<button type="button" class="btn btn-sm form-btn-add btn-remove-additional btn-delete">';
                                        $p .= '<i class="fa fa-trash" aria-hidden="true"></i>';
                                    $p .= '</button>';
                                $p .= '</div>';
                            $p .= '</div>';
                            echo $p;
                            $idx++;
                        }
                    }
                }
                ?>
            </div>
            <div class="form-group custom-select">
                <label class="control-label col-md-3" for="">
                    <?= __('PAYMENT_CURRENCY') . $this->Comment->formAsterisk() ?>
                </label>
                <div class="col-md-6">
                    <?php
                    echo $this->Form->select('currency_id', $currencies, [
                        'label' => false,
                        'class' => 'form-control',
                        'empty' => ['' => __('PAYMENT_SELECT_CURRENCY')]
                    ]); ?>
                    <span class="help-block help-tips" id="error-currency_id"></span>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-3" for="">
                    <?= __('PAYMENT_TRANSFER_AMOUNT') . $this->Comment->formAsterisk() ?>
                </label>
                <div class="col-md-6">
                    <?php
                    echo $this->Form->input('paid_amount', [
                        'label' => false,
                        'class' => 'form-control',
                        'placeholder' => __('PAYMENT_ENTER_TRANSFER_AMOUNT')
                    ]); ?>
                    <span class="help-block help-tips" id="error-paid_amount"></span>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-3" for="">
                    <?php echo __('PAYMENT_BANK_CHARGE') . $this->Comment->formAsterisk() ?>
                </label>
                <div class="col-md-6">
                    <?php
                    echo $this->Form->input('bank_charge', [
                        'label' => false,
                        'class' => 'form-control',
                        'placeholder' => __('PAYMENT_ENTER_BANK_CHARGE')
                    ]); ?>
                    <span class="help-block help-tips" id="error-bank_charge"></span>
                </div>
            </div>
            <!-- <div class="form-group">
                <label class="control-label col-md-3" for="">
                    <?php // echo __('TXT_BANK_CHARGE_DATE') . $this->Comment->formAsterisk() ?>
                </label>
                <div class="col-md-6">
                    <div class="input-group with-datepicker">
                        <?php
                        // echo $this->Form->input('bank_charge_date', [
                        //     'class' => 'form-control datepicker',
                        //     'label' => false,
                        //     'placeholder' => __('TXT_ENTER_BANK_CHARGE_DATE'),
                        //     'type' => 'text',
                        //     'value' => $charge_date
                        // ]); ?>
                        <div class="input-group-btn">
                            <button type="button" class="btn btn-default trigger-calendar" aria-label="Calendar">
                                <span class="glyphicon glyphicon-calendar"></span>
                            </button>
                        </div>
                    </div>
                    <span class="help-block help-tips" id="error-bank_charge_date"></span>
                </div>
            </div> -->
            <!-- <div class="form-group">
                <label class="control-label col-md-3" for="">
                    <?php //echo __('TXT_IMPORT_EXPENSE') . $this->Comment->formAsterisk()?>
                </label>
                <div class="col-md-6">
                    <?php
                    // echo $this->Form->input('import_expense', [
                    //     'label' => false,
                    //     'class' => 'form-control',
                    //     'placeholder' => __('TXT_ENTER_IMPORT_EXPENSE')
                    // ]); ?>
                    <span class="help-block help-tips" id="error-import_expense"></span>
                </div>
            </div> -->
            <div class="form-group">
                <label class="control-label col-md-3" for="">
                    <?= __('PAYMENT_PAYMENT_DATE') . $this->Comment->formAsterisk() ?>
                </label>
                <div class="col-md-6">
                    <div class="input-group with-datepicker">
                        <?php
                        echo $this->Form->input('payment_date', [
                            'class' => 'form-control datepicker',
                            'label' => false,
                            'placeholder' => __('PAYMENT_ENTER_PAYMENT_DATE'),
                            'type' => 'text',
                            'value' => $pay_date
                        ]); ?>
                        <div class="input-group-btn">
                            <button type="button" class="btn btn-default trigger-calendar" aria-label="Calendar">
                                <span class="glyphicon glyphicon-calendar"></span>
                            </button>
                        </div>
                    </div>
                    <span class="help-block help-tips" id="error-payment_date"></span>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-3" for="">
                    <?= __('PAYMENT_CURRENCY_TRANSFER_DATE') ?>
                </label>
                <div class="col-md-6">
                    <?php
                    echo $this->Form->input('currency_rate_oc_ap', [
                        'label' => false,
                        'class' => 'form-control',
                        'placeholder' => __('PAYMENT_ENTER_CURRENCY_TRANSFER_DATE')
                    ]);
                    ?>
                    <span class="help-block help-tips" id="error-currency_rate_oc_ap"></span>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-3" for="">
                    <?= __('PAYMENT_CURRENCY_ADDITIONAL_PAYMENT') ?>
                </label>
                <div class="col-md-6">
                    <?php
                    echo $this->Form->input('currency_rate_extra_ap', [
                        'label' => false,
                        'class' => 'form-control',
                        'placeholder' => __('PAYMENT_ENTER_CURRENCY_ADDITIONAL_PAYMENT')
                    ]); ?>
                    <span class="help-block help-tips" id="error-currency_rate_extra_ap"></span>
                </div>
            </div>
            <!-- <div class="form-group">
                <label class="control-label col-md-3" for="">
                    <?= __('PAYMENT_CURRENCY_RATE') ?>
                </label>
                <div class="col-md-6">
                    <?php
                    // echo $this->Form->input('currency_rate_jpy_ap', [
                    //     'label' => false,
                    //     'class' => 'form-control',
                    //     'placeholder' => __('TXT_ENTER_CURRENCY_RATE_JPY_AP')
                    // ]); ?>
                    <span class="help-block help-tips" id="error-currency_rate_jpy_ap"></span>
                </div>
            </div> -->
            <div class="form-group">
                <label class="control-label col-md-3" for=""><?= __('PAYMENT_PDF_FILE') ?></label>
                <div class="col-md-6">
                    <div class="row" style="margin-left: 0; margin-right: 0;">
                        <div class="col-sm-10 uploaded-file" style="padding-left: 0">
                            <?php
                            if ($action == 'edit' && !empty($payment->medias)) {
                                $y = 0;
                                $md = '';
                                foreach ($payment->medias as $media) {
                                    $cls = '';
                                    if ($y >= 1) {
                                        $cls = ' new-row-pdf';
                                    }
                                    $md .= '<div class="pdf-files file-uploaded'.$cls.'" id="pdf-'.$y.'">';
                                        $md .= '<input type="hidden" name="medias['.$y.'][file_name]" value="'.$media->file_name.'" class="file-name">';
                                        $md .= '<input type="hidden" name="medias['.$y.'][document_type]" value="'.$media->document_type.'" class="document-type">';
                                        $md .= '<div class="media-file-type">'.$media->document_type.'</div>';
                                        $md .= '<span class="glyphicon glyphicon-remove icon-remove-file"></span> ';
                                    $md .= '</div>';
                                    $y++;
                                }

                                echo $md;
                            }
                            ?>
                        </div>
                        <div class="col-md-2 text-right" style="padding-right: 0">
                            <button type="button" class="btn btn-sm btn-primary btn-add-pdf form-btn-add">
                                <i class="fa fa-plus" aria-hidden="true"></i>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <?= $this->ActionButtons->btnSaveCancel('Payments', $action); ?>
            </div>
            <?= $this->Form->end(); ?>
        </div>
    </div>
</section>
<!-- modal pdf upload -->
<?= $this->HtmlModal->modalHeader('modalPDFUpload', __('TXT_DOCUMENT_UPLOAD')) ?>
<div class="row">
    <div class="col-md-12">
        <?php
            echo $this->Form->create('Medias', [
                'autocomplete' => 'off',
                'class' => 'form-pdf-upload form-horizontal',
                'type' => 'file'
            ]);
            $this->Form->templates([
                'inputContainer' => '{{content}}'
            ]);
        ?>
        <div class="form-group">
            <label for="" class="control-label col-md-3"><?= __('TXT_TYPE') ?><?= $this->Comment->formAsterisk() ?></label>
            <div class="col-md-7">
                <?php
                    echo $this->Form->select('document_type', $files, [
                        'label' => false,
                        'class' => 'form-control',
                        'empty' => false
                    ]);
                    echo $this->Form->hidden('tmp', ['class' => 'file-index', 'value' => '']);
                    echo $this->Form->hidden('old_file', ['class' => 'old-pdf-file', 'value' => '']);
                ?>
                <span class="help-block help-tips" id="error-document_type"></span>
            </div>
        </div>
        <div class="form-group">
            <label for="" class="control-label col-md-3"><?= __('TXT_PDF_FILE') ?><?= $this->Comment->formAsterisk() ?></label>
            <div class="col-md-7">
                <?php
                    echo $this->Form->input('file', [
                        'type' => 'file',
                        'label' => false
                    ]);
                ?>
                <span class="help-block help-tips" id="error-file"></span>
            </div>
        </div>
        <?= $this->Form->end() ?>
    </div>
</div>
<div class="row">
    <div class="col-md-6 text-right">
        <button type="button" class="btn btn-default btn-sm btn-width" data-dismiss="modal"><?= __('TXT_CANCEL') ?></button>
    </div>
    <div class="col-md-6 text-left">
        <button type="button" class="btn btn-primary btn-sm btn-width pdf-upload-file"><?= __('TXT_UPLOAD') ?></button>
    </div>
</div>
<?= $this->HtmlModal->modalFooter(); ?>
<!-- /end modal pdf upload -->
<!-- modal search PO -->
<?= $this->HtmlModal->modalHeader('modalSearchPo', __('PAYMENT_WPO_TITLE')) ?>
<div class="row">
    <div class="col-md-12">
        <?php
        echo $this->Form->create(null, [
            'autocomplete' => 'off',
            'class' => 'form-search-po form-horizontal',
            'type' => 'get',
            'name' => 'form_search',
        ]);
        $this->Form->templates([
            'inputContainer' => '{{content}}',
        ]); ?>
        <div class="form-group">
            <div class="col-md-4">
                <?php
                echo $this->Form->input('keyword', [
                    'type' => 'text',
                    'placeholder' => __('PAYMENT_ENTER_KEYWORD'),
                    'class' => 'form-control',
                    'label' => false,
                    'id' => 'keyword',
                ]); ?>
            </div>
        </div>
        <div class="form-group custom-select">
            <div class="col-md-4 field-small-padding-right">
                <select class="form-control select-supplier" name="supplier">
                    <?= $this->Comment->sellerDropdownById($suppliers, $en); ?>
                </select>
            </div>
            <div class="col-md-4 field-small-padding-right field-small-padding-left">
                <?php
                echo $this->Form->select('currency', $currencies, [
                    'empty' => ['' => __('PAYMENT_SELECT_CURRENCY')],
                    'class' => 'form-control select-currency',
                    'label' => false,
                ]); ?>
            </div>
            <div class="col-md-4 field-small-padding-left">
                <?php
                echo $this->Form->select('date', $this->Comment->dropdownMonth($months, 'sale_date'), [
                    'empty' => ['' => __('PAYMENT_SELECT_MONTH')],
                    'class' => 'form-control select-date',
                    'label' => false,
                ]); ?>
            </div>
        </div>
        <?php echo $this->Form->end(); ?>
    </div>
</div>
<div class="filter-sales">
    <div class="row row-top-small-space">
        <div class="col-sm-12">
            <section>
                <?php echo __('DISPLAY_ITEM_INFO {0} DISPLAY_TOTAL {1}', [0, 0]); ?>
            </section>
        </div>
    </div>
    <div class="row row-top-small-space">
        <div class="col-sm-12">
            <p><?= __('PAYMENT_SELECT_ITEM') ?>: <span class="selected-items">0</span></p>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <p>
                <?= __('PAYMENT_TOTAL_AMOUNT_SELECT_ITEM') ?>:
                <span class="selected-currency"></span>
                <span class="selected-amounts">0</span>
            </p>
        </div>
    </div>
    <div class="row row-top-small-space">
        <div class="col-sm-12" style="max-height: 400px; overflow-y: scroll;">
            <table class="table table-bordered table-condensed">
                <thead>
                    <tr>
                        <th><input type="checkbox" value="all" class="check-alls" /> <?= __('PAYMENT_ALL') ?></th>
                        <th>#</th>
                        <th class="white-space"><?= __('PAYMENT_WPO') ?></th>
                        <th class="white-space"><?= __('PAYMENT_CLINIC') ?></th>
                        <th class="white-space"><?= __('PAYMENT_PRODUCT_AMOUNT') ?></th>
                        <th class="white-space"><?= __('PAYMENT_CURRENCY') ?></th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
</div>
<?php
$button = [
    'cancel_class' => ' btn-default btn-width md-footer-btn btn-close-modal',
    'cancel_id' => null,
    'cancel_text' => __('TXT_CANCEL'),
    'ok_id' => 'create-payment',
    'ok_class' => ' btn-primary btn-width po-ok',
    'ok_text' => __('OK')
];
echo $this->HtmlModal->modalWithFooter($button);
?>
<!-- /end modal search PO -->
<?= $this->Html->css([
    'bootstrap-datetimepicker.min',
    'jquery-ui',
    'intlTelInput',
]); ?>
<?= $this->Html->script([
    'jquery.form.min'
]); ?>
<script>
    (function() {
        var saleId = {};

        function checkExistId() {
            if ($('.table-ids').length) {
                $('.table-ids td').each(function () {
                    var sId = $(this).find('span').attr('data-id');
                    var obj = jQuery.extend({}, obj);
                    obj.sale_id = parseInt(sId);
                    obj.currency_id = $(this).find('span').attr('data-curid');
                    obj.currency_code = $(this).find('span').attr('data-curcode');
                    obj.seller_id = $(this).find('span').attr('data-sellerid');
                    obj.sale_number = $(this).find('span').attr('data-number');
                    obj.amount = $(this).find('span').attr('data-amt');

                    saleId[sId] = obj;
                });
            }
        }

        <?php if ($action == 'edit'): ?>
            checkExistId();
        <?php endif; ?>

        $('body').on('click', '.btn-add-sale-id', function () {
            updateCheckInfo(saleId);
            if (Object.keys(saleId).length === 0) {
                $('.filter-sales').find('section').text('<?= __('DISPLAY_ITEM_INFO {0} DISPLAY_TOTAL {1}', [0, 0]); ?>');
                $('.check-value').prop('checked', false);
                $('.check-alls').prop('checked', false);
            }
            $('#modalSearchPo').modal('show');
        });

        $('body').on('click', '.check-value', function () {
            if ($(this).prop('checked') !== true) { //check all or remove check all box.
                $('.check-alls').prop('checked', false);
            }

            saleIdChecked($(this));
        });

        $('body').on('click', '.check-alls', function () {
            $('input:checkbox').not(this).prop('checked', this.checked); //toggle check all

            $('.check-value').each(function (index, value) {
                saleIdChecked($(this));
            });
        });

        function saleIdChecked($this) {
            var valid = true;
            if (!validateSelectedCurrency(saleId, $this.attr('data-currency'))) {
                valid = false;
            }

            var changeSeller = false;
            if (!validateSelectedSupplier(saleId, $this.attr('data-sellerid'))) {
                if (confirm('<?= __('You have selected from different seller. The old data will be clear, are you sure?') ?>')) {
                    changeSeller = true;
                } else {
                    valid = false;
                }
            }

            if (changeSeller) {
                //uncheck all on seller change.
                removeOldCheck(saleId);
                saleId = {};
                valid = true;
            }

            var sId = parseInt($this.val());
            if (valid) {
                var obj = jQuery.extend({}, obj);
                obj.sale_id = sId;
                obj.currency_id = $this.attr('data-currency');
                obj.currency_code = $this.attr('data-currencycode');
                obj.seller_id = $this.attr('data-sellerid');
                obj.sale_number = $this.attr('data-salenumber');
                obj.amount = $this.attr('data-amount');

                saleId[sId] = obj;
                updateCheckInfo(saleId);
                //console.log(saleId);
            } else {
                $('#each-' + $this.val()).prop('checked', false);
            }

            //remove obj on uncheck
            if (!$this.is(':checked')) {
                delete saleId[sId];
                updateCheckInfo(saleId);
                //console.log(saleId);
            }
        }

        function removeOldCheck(saleId) {
            $.each(saleId, function (idx, val) {
                $('#each-' + idx).prop('checked', false);
            });
        }

        function updateCheckInfo(saleId) {
            $('.selected-items').text(Object.keys(saleId).length);
            var amount = 0;
            var currency = '';
            if (Object.keys(saleId).length > 0) {
                $.each(saleId, function (item, value) {
                    amount += parseInt(value.amount);
                    currency = value.currency_code;
                });
                $('.selected-amounts').text(amount);
                $('.selected-currency').text(currency);
            } else {
                $('.selected-amounts').text(amount);
                $('.selected-currency').text(currency);
            }
        }

        function updateCurrencySelect(selectedCurrency) {
            $('select[name="currency_id"] option').removeAttr('selected', true);

            $('select[name="currency_id"] option').filter(function() {
                return $(this).val() == selectedCurrency;
            }).prop('selected', true);
        }

        function validateSelectedCurrency(saleId, selected) {
            var valid = true;
            if (Object.keys(saleId).length) {
                $.each(saleId, function (item, value) {
                    if (value.currency_id != selected) {
                        alert('<?= __('The selected value has different currency type') ?>');
                        valid = false;
                        return false;
                    }
                });
            }
            return valid;
        }

        function validateSelectedSupplier(saleId, selected) { //both supplier and manufacturer
            var valid = true;
            if (Object.keys(saleId).length) {
                $.each(saleId, function (item, value) {
                    if (value.seller_id != selected) {
                        valid = false;
                        return false;
                    }
                });
            }
            return valid;
        }

        $('body').on('click', '.po-ok', function () {
            $('#product-amount').val($('.selected-amounts').text()).attr('readonly', true);

            updateSelectedSaleId();
            $('.close').click();
        });

        $('body').on('click', '.icon-remove-id', function () {
            if (Object.keys(saleId).length > 0) {
                var key = $(this).attr('data-id');
                var amount = parseInt($(this).attr('data-amt'));
                delete saleId[key];
                var productAmount = parseInt($('#product-amount').val());
                $('#product-amount').val(productAmount - amount);
                if (Object.keys(saleId).length > 0) {
                    updateSelectedSaleId();
                } else {
                    saleId = {};
                    $('#product-amount').removeAttr('readonly', true);
                    $('.purchase-id-section').empty().html('<input type="hidden" value="" name="sale_payments[0][sale_id]" class="selected-sale-ids"><span class="help-block help-tips" id="error-sale_id"></span>');
                }
            } else {
                saleId = {};
                $('#product-amount').removeAttr('readonly', true);
                $('.purchase-id-section').empty().html('<input type="hidden" value="" name="sale_payments[0][sale_id]" class="selected-sale-ids"><span class="help-block help-tips" id="error-sale_id"></span>');
            }
        });

        function updateSelectedSaleId() {
            //update sale id selections
            var selectedCurrency;
            var $tbl = '';
            if (Object.keys(saleId).length > 0) {
                $tbl += '<table class="table-ids purchase-ids"><tbody>';
                    var idx = 0;
                    var i = 0;
                    $.each(saleId, function( index, value ) {
                        if (idx === 0) {
                            selectedCurrency = value.currency_id;
                            $tbl += '<tr>';
                        }

                        $tbl += '<td>';
                            $tbl += '<div class="pid purchase-id">';
                                $tbl += '<div class="ids">'+ value.sale_number +'</div>';
                                $tbl += '<span data-amt="'+ value.amount +'" data-id="'+ index +'" class="glyphicon glyphicon-remove icon-remove-id"></span>';
                                $tbl += '<input type="hidden" value="'+ index +'" name="sale_payments['+ i +'][sale_id]" class="selected-sale-ids" />';
                            $tbl += '</div>';
                        $tbl += '</td>';

                        if (idx === 4) {
                            $tbl += '</tr>';
                            idx = 0;
                        } else {
                            idx++;
                        }
                        i++;
                    });
                $tbl += '</tbody></table>';
            }
            $('.purchase-id-section').empty().html($tbl);
            updateCurrencySelect(selectedCurrency);
        }

        /**
         * Searching for sale number
         * @type Array
         */
        var optionData = [];
        sale_list();
        // typehead_initialize('#keyword', optionData);
        function sale_list() {
            $.ajax({
                url: '<?php echo $this->Url->build('/payments/get-sale-list'); ?>',
                type: 'get',
                dataType: 'json',
                contentType: 'application/json; charset=utf-8'
            }).done(function(data) {
                if (data.data) {
                    $.each(data.data, function(i, v) {
                        optionData.push(v.customer.name);
                        optionData.push(v.customer.name_en);
                        optionData.push(v.sale_number);
                    });
                }
            });
        }

        function saleSearch() {
            $.LoadingOverlay('show');
            var url = '<?= $this->Url->build(['action' => 'sale-list']) ?>';

            ajax_request_get(url, $('.form-search-po').serialize(), function(data) {
                if (data) {
                    $('.filter-sales').empty().html(data);
                    if (Object.keys(saleId).length > 0) {
                        $.each(saleId, function (index, value) {
                            if ($('.check-value').length) {
                                $('.check-value').each(function (i, v) {
                                    var selectedId = parseInt($(this).val());
                                    if (parseInt(index) === selectedId) {
                                        $('#each-' + selectedId).prop('checked', true);
                                    }
                                });
                            }
                        });
                        updateCheckInfo(saleId);
                    }
                }
            }, 'html', '<?= __('TXT_SESSION_TIMEOUT') ?>');
        }

        jQuery(document).on('keydown', 'input#keyword', function(ev) {
            if (ev.which === 13) {
                saleSearch();
            }
        });

        $('body').on('click', '.tt-selectable', function(e) {
            saleSearch();
        });

        $('body').on('change', '.select-supplier', function () {
            saleSearch();
        });

        $('body').on('change', '.select-currency', function () {
            saleSearch();
        });

        $('body').on('change', '.select-date', function () {
            saleSearch();
        });

        /**
        * Date picker
         */
        $('.datepicker').datetimepicker({
            format: 'YYYY-MM-DD',
            minDate: '2007-01-01'
        });

        $('.trigger-calendar').click(function() {
            $(this).parents('.with-datepicker').find('.datepicker').focus();
        });

        /**
        * Additional Payments
         */
        $('body').on('click', '.btn-add-additional', function() {
            var item = $(this).parents('.additional-item').clone();
            item.find('label').text('');
            item.find('button')
                .removeClass('btn-primary btn-add-additional')
                .addClass('btn-remove-additional btn-delete')
                .html('<i class="fa fa-trash" aria-hidden="true"></i>');

            $('.additionals').append(item);
            updateIndex();
        });

        $('body').on('click', '.btn-remove-additional', function () {
            $(this).parents('.additional-item').remove();
            updateIndex();
        });

        function updateIndex() {
            $('.descriptions').each(function(i, v) {
                $(this).attr('name', 'additional_payments[' + i + '][description]');
            });
            $('.amounts').each(function(i, v) {
                $(this).attr('name', 'additional_payments[' + i + '][amount]');
            });
            $('.desc-errors').each(function(i, v) {
                $(this).attr('id', 'error-description-' + i);
            });
            $('.amt-errors').each(function(i, v) {
                $(this).attr('id', 'error-amount-' + i);
            });
        }

        /**
         * Register new sale payment
         */
        $('#btn-register').on('click', function() {
            $('.help-block').empty().hide();
            var data = $('.form-payments').serialize();
            $.ajax({
                url: $('.form-payments').attr('action'),
                type: 'POST',
                dataType: 'JSON',
                data: data,
                cache: false,
                beforeSend: function() {
                    $.LoadingOverlay('show');
                },
                success: function(response) {
                    $.LoadingOverlay('hide');
                    if (response.status === 1) {
                        window.location.href = '<?php echo $this->Url->build('/payments/index/'); ?>';
                    } else if (response.status === 0) {
                        $.each(response.message, function(key, elem) {
                            var validate = $('#error-' + key);
                            $.each(elem, function(k, v) {
                                if (key == 'additional') {
                                    if (v.length || Object.keys(v).length) {
                                        $.each(v, function(k1, v1) {
                                            $('#error-amount-' + k1).removeClass('help-tips')
                                                .addClass('error-tips')
                                                .text(v1)
                                                .show();
                                        });
                                    }
                                }

                                if (key == 'sale_payments') {
                                    $.each(v, function(k1, v1) {
                                        $.each(v1, function(k2, v2) {
                                            $('#error-sale_id').removeClass('help-tips')
                                                .addClass('error-tips')
                                                .text(v2)
                                                .show();
                                        });
                                    });
                                }

                                validate.removeClass('help-tips')
                                    .addClass('error-tips')
                                    .text(v)
                                    .show();
                            });
                        });
                    }
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    $.LoadingOverlay('hide');
                    if (errorThrown === 'Forbidden') {
                        if (confirm('<?= __('TXT_SESSION_TIMEOUT') ?>')) {
                            location.reload();
                        }
                    }
                }
            });
        });

        /**
        * PDF File Management
         */
        $('body').on('click', '.btn-add-pdf', function () {
            var newFile = '';
            var className = ' file-uploaded';
            if ($('.uploaded-file').children('.file-uploaded').length >= 1) {
                className = ' file-uploaded new-row-pdf';
            }

            newFile += '<div class="pdf-files'+className+'">';
                newFile += '<div class="media-file-type"><?= __('Add File') ?></div>';
                newFile += '<span class="glyphicon glyphicon-remove icon-remove-file"></span> ';
                newFile += '<input type="hidden" name="medias[0][file_name]" value="" class="file-name" />';
                newFile += '<input type="hidden" name="medias[0][document_type]" value="" class="document-type">';
            newFile += '</div>';
            $('.uploaded-file').append(newFile);
            updateIndexFile();
        });

        $('body').on('click', '.pdf-files', function () {
            $('.file-index').val($(this).attr('id'));
            $('#file').replaceWith($('#file').val('').clone(true));
            $('.old-pdf-file').val($(this).find('.file-name').val());
            var isEdit = $(this).find('.document-type').val();
            if (isEdit != '') {
                $('select[name="document_type"]').val(isEdit).change();
            }
            $('#modalPDFUpload').modal('show');
        });

        $('body').on('click', '.pdf-upload-file', function () {
            uploadImage();
        });

        function updateIndexFile() {
            if ($('.file-name').length) {
                $('.file-name').each(function (i, v) {
                    $(this).attr('name', 'medias[' + i + '][file_name]');
                });
            }
            if ($('.document-type').length) {
                $('.document-type').each(function (i, v) {
                    $(this).attr('name', 'medias[' + i + '][document_type]');
                });
            }
            if ($('.pdf-files').length) {
                $('.pdf-files').each(function (i, v) {
                    $(this).attr('id', 'pdf-' + i);
                });
            }
        }

        $('body').on('click', '.icon-remove-file', function (e) {
            e.stopPropagation();
            var fileName = $(this).parents('.pdf-files').attr('data-name');
            $('.' + fileName).remove();
            $(this).parents('.pdf-files').remove();
            updateIndexFile();
        });

        function uploadImage() {
            $('.help-block').empty().hide();
            $('.form-pdf-upload').ajaxForm({
                url: '<?php echo $this->Url->build('/payments/upload/'); ?>',
                type: 'POST',
                dataType: 'JSON',
                cache: false,
                contentType: false,
                processData: false,
                beforeSend: function () {
                    $.LoadingOverlay('show');
                },
                success: function (response) {
                    $.LoadingOverlay('hide');
                    if (response.status === 0) {
                        $.each(response.message, function(key, elem) {
                            $('#error-' + key)
                                .addClass('error-tips')
                                .text(elem)
                                .show();
                        });
                    } else {
                        var indexId = $('.file-index').val();
                        var doc_type = $('select[name="document_type"]').val();
                        var selectedType = $('select[name="document_type"]').find('option:selected').text();
                        $('#' + indexId).find('.document-type').val(doc_type);
                        $('#' + indexId).find('.media-file-type').text(selectedType);
                        $('#' + indexId).find('.file-name').val(response.file.new_name);
                        $('.close').click();
                    }
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    $.LoadingOverlay('hide');
                    if (errorThrown === 'Forbidden') {
                        if (confirm('<?= __('TXT_SESSION_TIMEOUT') ?>')) {
                            location.reload();
                        }
                    }
                }
            }).submit();
        }

    })();
</script>
