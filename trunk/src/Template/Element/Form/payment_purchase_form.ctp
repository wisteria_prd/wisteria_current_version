<?php $action = $this->request->action; ?>
<section id="create_form">
    <?php if (false) : ?>
    <div class="row">
        <div class="col-sm-12 text-center">
            <h2 style="margin-bottom: 30px;"><?= __('TXT_PAYMENT_INFO') ?></h2>
        </div>
    </div>
    <?php endif; ?>
    <div class="row">
        <div class="col-sm-12">
            <?php
            echo $this->Form->create($payment, [
                'role' => 'form',
                'class' => 'form-horizontal form-payments',
                'name' => 'common_form'
            ]);

            $this->Form->templates([
                'inputContainer' => '{{content}}'
            ]);

            $charge_date = '';
            $pay_date = '';
            if ($action == 'editPurchase') {
                if (!empty($payment->payment_date)) {
                    $pay_date = date('Y-m-d', strtotime($payment->payment_date));
                }

                if (!empty($payment->bank_charge_date)) {
                    $charge_date = date('Y-m-d', strtotime($payment->bank_charge_date));
                }
            }
            ?>
            <div class="purchases">
                <div class="form-group row-purchase">
                    <label class="control-label col-md-3" for=""><?= __('PAYMENT_PURCHASE_TXT_OF_NUMBER') ?><?= $this->Comment->formAsterisk() ?></label>
                    <div class="col-md-6 purchase-id-section">
                        <?php
                        if ($action == 'create') {
                            echo $this->Form->hidden('purchase_payments.0.puchase_id', ['class' => 'selected-purchase-ids', 'value' => '']);
                            echo '<span class="help-block help-tips" id="error-purchase_id"></span>';
                        } else {
                            if (!empty($payment->purchase_payments)) {
                                $ids = '<table class="table-ids purchase-ids"><tbody>';
                                $s = 0;
                                $c = 0;
                                foreach ($payment->purchase_payments as $s_id) {
                                    if ($s == 0) {
                                        $ids .= '<tr>';
                                    }
                                    $ids .= '<td><div class="pid purchase-id">';
                                        $ids .= '<span data-id="'.$s_id->purchase_id.'" class="glyphicon glyphicon-remove-circle icon-remove-id"></span>';
                                        $ids .= '<div class="ids">'.$s_id->purchase->purchase_number.'</div>';
                                        $ids .= '<input type="hidden" value="'.$s_id->purchase_id.'" name="purchase_payments['.$c.'][purchase_id]" class="selected-purchase-ids">';
                                    $ids .= '</div></td>';
                                    if ($s == 4) {
                                        $ids .= '</tr>';
                                    }
                                    $s++;
                                    $c++;
                                }
                                $ids .= '</tbody></table>';
                                echo $ids;
                            } else {
                                echo $this->Form->hidden('purchase_payments.0.purchase_id', ['class' => 'selected-purchase-ids', 'value' => '']);
                                echo '<span class="help-block help-tips" id="error-purchase_id"></span>';
                            }
                        }
                        ?>
                    </div>
                    <div class="col-md-1 field-no-padding-left">
                        <button type="button" class="btn btn-sm btn-primary btn-add-sale-id form-btn-add">
                            <i class="fa fa-plus" aria-hidden="true"></i>
                        </button>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-3" for=""><?= __('PAYMENT_PURCHASE_TXT_PRODUCT_AMOUNT') ?><?= $this->Comment->formAsterisk() ?></label>
                <div class="col-md-6">
                    <?php
                        echo $this->Form->input('product_amount', [
                            'label' => false,
                            'class' => 'form-control',
                            'placeholder' => __('PAYMENT_PURCHASE_TXT_ENTER_PRODUCT_AMOUNT')
                        ]);
                    ?>
                    <span class="help-block help-tips" id="error-product_amount"></span>
                </div>
            </div>
            <div class="additionals">
                <div class="form-group additional-item">
                    <label class="control-label col-md-3" for=""><?= __('PAYMENT_PURCHASE_TXT_ADDITIONAL_PAYMENT') ?></label>
                    <div class="col-md-4 field-small-padding-right">
                        <?php
                            echo $this->Form->input('additional_payments.0.description', [
                                'type' => 'text',
                                'label' => false,
                                'id' => false,
                                'class' => 'form-control descriptions',
                                'placeholder' => __('PAYMENT_PURCHASE_TXT_ENTER_DESCRIPTION')
                            ]);
                        ?>
                        <span class="help-block help-tips desc-errors" id="error-description-0"></span>
                    </div>
                    <div class="col-md-2 field-small-padding-left">
                        <?php
                            echo $this->Form->input('additional_payments.0.amount', [
                                'label' => false,
                                'id' => false,
                                'class' => 'form-control amounts',
                                'placeholder' => __('PAYMENT_PURCHASE_TXT_ENTER_AMOUNT')
                            ]);
                        ?>
                        <span class="help-block help-tips amt-errors" id="error-amount-0"></span>
                    </div>
                    <div class="col-md-1 field-no-padding-left">
                        <button type="button" class="btn btn-sm btn-primary btn-add-additional form-btn-add">
                            <i class="fa fa-plus" aria-hidden="true"></i>
                        </button>
                    </div>
                </div>
                <?php
                if ($action == 'editPurchase' && !empty($payment->additional_payments)) {
                    $idx = 0;
                    foreach ($payment->additional_payments as $additional) {
                        if ($idx == 0) {
                            $idx++;
                            continue;
                        } else {
                            $p = '<div class="form-group additional-item">';
                                $p .= '<label class="control-label col-md-3" for=""></label>';
                                $p .= '<div class="col-md-4 field-small-padding-right">';
                                    $p .= $this->Form->input('additional_payments.'.$idx.'.description', [
                                        'type' => 'text',
                                        'label' => false,
                                        'id' => false,
                                        'class' => 'form-control descriptions',
                                        'placeholder' => __('TXT_ENTER_DESCRIPTION')
                                    ]);
                                    $p .= '<span class="help-block help-tips desc-errors" id="error-description-'.$idx.'"></span>';
                                $p .= '</div>';
                                $p .= '<div class="col-md-2 field-small-padding-left">';
                                    $p .= $this->Form->input('additional_payments.'.$idx.'.amount', [
                                        'label' => false,
                                        'id' => false,
                                        'class' => 'form-control amounts',
                                        'placeholder' => __('TXT_ENTER_AMOUNT')
                                    ]);
                                    $p .= '<span class="help-block help-tips amt-errors" id="error-amount-'.$idx.'"></span>';
                                $p .= '</div>';
                                $p .= '<div class="col-md-1 field-no-padding-left">';
                                    $p .= '<button type="button" class="btn btn-sm form-btn-add btn-remove-additional btn-delete">';
                                        $p .= '<i class="fa fa-trash" aria-hidden="true"></i>';
                                    $p .= '</button>';
                                $p .= '</div>';
                            $p .= '</div>';
                            echo $p;
                            $idx++;
                        }
                    }
                }
                ?>
            </div>
            <div class="form-group custom-select">
                <label class="control-label col-md-3" for=""><?= __('PAYMENT_PURCHASE_TXT_CURRENCY') ?><?= $this->Comment->formAsterisk() ?></label>
                <div class="col-md-6">
                    <?php
                        echo $this->Form->select('currency_id', $currencies, [
                            'label' => false,
                            'class' => 'form-control',
                            'empty' => ['' => __('PAYMENT_PURCHASE_TXT_SELECT_CURRENCY')]
                        ]);
                    ?>
                    <span class="help-block help-tips" id="error-currency_id"></span>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-3" for=""><?= __('PAYMENT_PURCHASE_TXT_TRANSFERED_AMOUNT') ?><?= $this->Comment->formAsterisk() ?></label>
                <div class="col-md-6">
                    <?php
                        echo $this->Form->input('paid_amount', [
                            'label' => false,
                            'class' => 'form-control',
                            'placeholder' => __('PAYMENT_PURCHASE_TXT_ENTER_TRANSFERED_AMOUNT')
                        ]);
                    ?>
                    <span class="help-block help-tips" id="error-paid_amount"></span>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-3" for=""><?= __('PAYMENT_PURCHASE_TXT_BANK_CHARGE') ?><?= $this->Comment->formAsterisk() ?></label>
                <div class="col-md-6">
                    <?php
                        echo $this->Form->input('bank_charge', [
                            'label' => false,
                            'class' => 'form-control',
                            'placeholder' => __('PAYMENT_PURCHASE_TXT_ENTER_BANK_CHARGE')
                        ]);
                    ?>
                    <span class="help-block help-tips" id="error-bank_charge"></span>
                </div>
            </div>
            <?php
            if (false) : ?>
            <div class="form-group">
                <label class="control-label col-md-3" for=""><?= __('TXT_BANK_CHARGE_DATE') ?><?= $this->Comment->formAsterisk() ?></label>
                <div class="col-md-6">
                    <div class="input-group with-datepicker">
                        <?php echo $this->Form->input('bank_charge_date', [
                            'class' => 'form-control datepicker',
                            'label' => false,
                            'placeholder' => __('TXT_ENTER_BANK_CHARGE_DATE'),
                            'type' => 'text',
                            'value' => $charge_date
                        ]); ?>
                        <div class="input-group-btn">
                            <button type="button" class="btn btn-default trigger-calendar" aria-label="Calendar">
                                <span class="glyphicon glyphicon-calendar"></span>
                            </button>
                        </div>
                    </div>
                    <span class="help-block help-tips" id="error-bank_charge_date"></span>
                </div>
            </div>
            <?php
            endif; ?>
            <div class="form-group">
                <label class="control-label col-md-3" for=""><?= __('PAYMENT_PURCHASE_TXT_PAYMENT_DATE') ?><?= $this->Comment->formAsterisk() ?></label>
                <div class="col-md-6">
                    <div class="input-group with-datepicker">
                        <?php echo $this->Form->input('payment_date', [
                            'class' => 'form-control datepicker',
                            'label' => false,
                            'placeholder' => __('PAYMENT_PURCHASE_TXT_ENTER_PAYMENT_DATE'),
                            'type' => 'text',
                            'value' => $pay_date
                        ]); ?>
                        <div class="input-group-btn">
                            <button type="button" class="btn btn-default trigger-calendar" aria-label="Calendar">
                                <span class="glyphicon glyphicon-calendar"></span>
                            </button>
                        </div>
                    </div>
                    <span class="help-block help-tips" id="error-payment_date"></span>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-3" for=""><?= __('PAYMENT_PURCHASE_TXT_IMPORT_EXPENSE') ?><?= $this->Comment->formAsterisk() ?></label>
                <div class="col-md-6">
                    <?php
                        echo $this->Form->input('import_expense', [
                            'label' => false,
                            'class' => 'form-control',
                            'placeholder' => __('PAYMENT_PURCHASE_TXT_ENTER_IMPORT_EXPENSE_AMOUNT')
                        ]);
                    ?>
                    <span class="help-block help-tips" id="error-import_expense"></span>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-3" for=""><?= __('PAYMENT_PURCHASE_TXT_CURRENCY_RATE') ?></label>
                <div class="col-md-6">
                    <?php
                        echo $this->Form->input('currency_rate_oc_ap', [
                            'label' => false,
                            'class' => 'form-control',
                            'placeholder' => __('TXT_ENTER_CURRENCY_RATE_OC_AP')
                        ]);
                    ?>
                    <span class="help-block help-tips" id="error-currency_rate_oc_ap"></span>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-3" for=""><?= __('TXT_CURRENCY_RATE_EXTRA_AP') ?></label>
                <div class="col-md-6">
                    <?php
                        echo $this->Form->input('currency_rate_extra_ap', [
                            'label' => false,
                            'class' => 'form-control',
                            'placeholder' => __('TXT_ENTER_CURRENCY_RATE_EXTRA_AP')
                        ]);
                    ?>
                    <span class="help-block help-tips" id="error-currency_rate_extra_ap"></span>
                </div>
            </div>
            <?php
            if (false) : ?>
            <div class="form-group">
                <label class="control-label col-md-3" for=""><?= __('TXT_CURRENCY_RATE_JPY_AP') ?></label>
                <div class="col-md-6">
                    <?php
                        echo $this->Form->input('currency_rate_jpy_ap', [
                            'label' => false,
                            'class' => 'form-control',
                            'placeholder' => __('TXT_ENTER_CURRENCY_RATE_JPY_AP')
                        ]);
                    ?>
                    <span class="help-block help-tips" id="error-currency_rate_jpy_ap"></span>
                </div>
            </div>
            <?php
            endif; ?>
<!--            <div class="form-group">
                <label class="control-label col-md-3" for=""><?= __('TXT_PAYMENT_CODE') ?><?= $this->Comment->formAsterisk() ?></label>
                <div class="col-md-6">
                    <?php
//                        echo $this->Form->input('payment_code', [
//                            'type' => 'text',
//                            'label' => false,
//                            'class' => 'form-control',
//                            'placeholder' => __('TXT_ENTER_PAYMENT_CODE')
//                        ]);
                    ?>
                    <span class="help-block help-tips" id="error-payment_code"></span>
                </div>
            </div>-->
            <div class="form-group">
                <label class="control-label col-md-3" for=""><?= __('PAYMENT_PURCHASE_TXT_PDF_FILE') ?></label>
                <div class="col-md-6">
                    <div class="row" style="margin-left: 0; margin-right: 0;">
                        <div class="col-sm-10 uploaded-file" style="padding-left: 0">
                            <?php
                            if ($action == 'edit' && !empty($payment->medias)) {
                                $y = 0;
                                $md = '';
                                foreach ($payment->medias as $media) {
                                    $cls = '';
                                    if ($y >= 1) {
                                        $cls = ' new-row-pdf';
                                    }
                                    $md .= '<div class="pdf-files file-uploaded'.$cls.'" id="pdf-'.$y.'">';
                                        $md .= '<input type="hidden" name="medias['.$y.'][file_name]" value="'.$media->file_name.'" class="file-name">';
                                        $md .= '<input type="hidden" name="medias['.$y.'][document_type]" value="'.$media->document_type.'" class="document-type">';
                                        $md .= '<div class="media-file-type">'.$media->document_type.'</div>';
                                        $md .= '<span class="glyphicon glyphicon-remove icon-remove-file"></span> ';
                                    $md .= '</div>';
                                    $y++;
                                }

                                echo $md;
                            }
                            ?>
                        </div>
                        <div class="col-md-2 text-right" style="padding-right: 0">
                            <button type="button" class="btn btn-sm btn-primary btn-add-pdf form-btn-add">
                                <i class="fa fa-plus" aria-hidden="true"></i>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <?= $this->ActionButtons->btnSaveCancel('ImportInternationalProducts', $action); ?>
            </div>
            <?= $this->Form->end(); ?>
        </div>
    </div>
</section>
<!-- modal pdf upload -->
<?= $this->HtmlModal->modalHeader('modalPDFUpload', __('TXT_DOCUMENT_UPLOAD')) ?>
<div class="row">
    <div class="col-md-12">
        <?php
            echo $this->Form->create('Medias', [
                'autocomplete' => 'off',
                'class' => 'form-pdf-upload form-horizontal',
                'type' => 'file'
            ]);
            $this->Form->templates([
                'inputContainer' => '{{content}}'
            ]);
        ?>
        <div class="form-group">
            <label for="" class="control-label col-md-3"><?= __('TXT_TYPE') ?><?= $this->Comment->formAsterisk() ?></label>
            <div class="col-md-7">
                <?php
                    echo $this->Form->select('document_type', $files, [
                        'label' => false,
                        'class' => 'form-control',
                        'empty' => false
                    ]);
                    echo $this->Form->hidden('tmp', ['class' => 'file-index', 'value' => '']);
                    echo $this->Form->hidden('old_file', ['class' => 'old-pdf-file', 'value' => '']);
                ?>
                <span class="help-block help-tips" id="error-document_type"></span>
            </div>
        </div>
        <div class="form-group">
            <label for="" class="control-label col-md-3"><?= __('TXT_PDF_FILE') ?><?= $this->Comment->formAsterisk() ?></label>
            <div class="col-md-7">
                <?php
                    echo $this->Form->input('file', [
                        'type' => 'file',
                        'label' => false
                    ]);
                ?>
                <span class="help-block help-tips" id="error-file"></span>
            </div>
        </div>
        <?= $this->Form->end() ?>
    </div>
</div>
<div class="row">
    <div class="col-md-6 text-right">
        <button type="button" class="btn btn-default btn-sm btn-width" data-dismiss="modal"><?= __('TXT_CANCEL') ?></button>
    </div>
    <div class="col-md-6 text-left">
        <button type="button" class="btn btn-primary btn-sm btn-width pdf-upload-file"><?= __('TXT_UPLOAD') ?></button>
    </div>
</div>
<?= $this->HtmlModal->modalFooter(); ?>
<!-- /end modal pdf upload -->
<!-- modal search PO -->
<?= $this->HtmlModal->modalHeader('modalSearchPo', __('PO')) ?>
<div class="row">
    <div class="col-md-12">
        <?php
            echo $this->Form->create(null, [
                'autocomplete' => 'off',
                'class' => 'form-search-po form-horizontal',
                'type' => 'get'
            ]);
            $this->Form->templates([
                'inputContainer' => '{{content}}'
            ]);
        ?>
        <div class="form-group">
            <div class="col-md-4 field-small-padding-right">
                <select class="form-control select-supplier" name="supplier">
                    <?= $this->Comment->sellerDropdownById($suppliers, $local); ?>
                </select>
            </div>
            <div class="col-md-4 field-small-padding-right field-small-padding-left">
                <?php
                    echo $this->Form->select('currency', $currencies, [
                        'empty' => ['' => __('PAYMENT_PURCHASE_TXT_SELECT_CURRENCY')],
                        'class' => 'form-control select-currency',
                        'label' => false
                    ]);
                ?>
            </div>
            <div class="col-md-4 field-small-padding-left">
                <?php
                    echo $this->Form->select('date', $this->Comment->dropdownMonth($months, 'purchase_date'), [
                        'empty' => ['' => __('PAYMENT_PURCHASE_TXT_SELECT_MONTH')],
                        'class' => 'form-control select-date',
                        'label' => false
                    ]);
                ?>
            </div>
        </div>
        <?= $this->Form->end() ?>
    </div>
</div>
<div class="filter-sales">

</div>
<div class="row">
    <div class="col-md-12 text-center">
        <button type="button" class="btn btn-primary btn-sm btn-width po-ok"><?= __('PAYMENT_PURCHASE_TXT_SELECT') ?></button>
    </div>
</div>
<?= $this->HtmlModal->modalFooter(); ?>
<!-- /end modal search PO -->
<?php
    echo $this->Html->css([
        '//cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.min.css',
        '//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css',
        'intlTelInput.css',
    ]);
?>
<?php
    echo $this->Html->script([
        'jquery.form.min'
    ]);
?>
<script>
    (function() {
        var PurchaseIds = {};
        checkExistIds();

        function purchaseSearch() {
            $.LoadingOverlay('show');
            var url = '<?= $this->Url->build(['action' => 'purchaseList']) ?>';

            ajax_request_get(url, $('.form-search-po').serialize(), function(data) {
                if (data) {
                    $('.filter-sales').empty().html(data);
                }
            }, 'html', '<?= __('TXT_SESSION_TIMEOUT') ?>');
        }

        $('body').on('click', '.tt-selectable', function(e) {
            purchaseSearch();
        });

        $('body').on('change', '.select-supplier', function () {
            purchaseSearch();
        });

        $('body').on('change', '.select-currency', function () {
            purchaseSearch();
        });

        $('body').on('change', '.select-date', function () {
            purchaseSearch();
        });

        $('.datepicker').datetimepicker({
            format: 'YYYY-MM-DD',
            minDate: '2007-01-01'
        });

        $('.trigger-calendar').click(function() {
            $(this).parents('.with-datepicker').find('.datepicker').focus();
        });

        $('body').on('click', '.btn-add-additional', function() {
            var item = $(this).parents('.additional-item').clone();
            item.find('label').text('');
            item.find('button')
                .removeClass('btn-primary btn-add-additional')
                .addClass('btn-remove-additional btn-delete')
                .html('<i class="fa fa-trash" aria-hidden="true"></i>');

            $('.additionals').append(item);
            updateIndex();
        });

        $('body').on('click', '.btn-remove-additional', function () {
            $(this).parents('.additional-item').remove();
            updateIndex();
        });

        function updateIndex() {
            $('.descriptions').each(function(i, v) {
                $(this).attr('name', 'additional_payments[' + i + '][description]');
            });
            $('.amounts').each(function(i, v) {
                $(this).attr('name', 'additional_payments[' + i + '][amount]');
            });
            $('.desc-errors').each(function(i, v) {
                $(this).attr('id', 'error-description-' + i);
            });
            $('.amt-errors').each(function(i, v) {
                $(this).attr('id', 'error-amount-' + i);
            });
        }

        function updateCurrencySelect(selectedCurrency) {
            $('select[name="currency_id"] option').removeAttr('selected', true);

            $('select[name="currency_id"] option').filter(function() {
                return $(this).text() == selectedCurrency;
            }).prop('selected', true);
        }

        function countCheck() {
            var chk_count = 0;
            var chk_amount = 0;
            var chk_currency = [];
            $('.check-value').each(function () {
                if ($(this).is(':checked')) {
                    chk_count++;
                    chk_amount += parseInt($.trim($(this).parents('tr').find('.value-amount').text()));
                    chk_currency.push($.trim($(this).parents('tr').find('.value-currency').text()));
                }
            });

            $('.selected-items').text(chk_count);
            $('.selected-amounts').text(chk_amount);

            if (chk_currency.length > 0) {
                var unique = chk_currency.filter(function(item, i, chk_currency) {
                    return i == chk_currency.indexOf(item);
                });
                if (unique.length > 1) {
                    alert('<?= __('The selected value has different currency type') ?>');
                }
            $('.selected-currency').text(chk_currency[0]);
                updateCurrencySelect(chk_currency[0]);
            } else {
                $('.selected-currency').text('');
            }
        }

        function updateProductAmount () {
            if ($('.selected-purchase-ids').length) {
                $.LoadingOverlay('show');
                var url = '<?= $this->Url->build(['action' => 'getUpdatePurchaseAmount']) ?>';
                var ids = '';
                $('.selected-purchase-ids').each(function () {
                    ids += $(this).val() + ',';
                });
                var dt = { purchase_ids: ids };
                ajax_request_get(url, dt, function(data) {
                    if (data.status === 1) {
                        $('#product-amount').val(data.amount);
                        $('#paid-amount').val(data.amount);
                    }
                }, 'json', '<?= __('TXT_SESSION_TIMEOUT') ?>');
            } else {
                $('#product-amount').val('');
                $('#paid-amount').val('');
                $('select[name="currency_id"]').val('').change();
            }
        }

        $('body').on('click', '.check-value', function () {
            if ($(this).prop('checked') !== true) {
                $('.check-alls').prop('checked', false);
            }
            countCheck();
        });

        $('body').on('click', '.check-alls', function () {
            $('input:checkbox').not(this).prop('checked', this.checked);
            countCheck();
        });

        //check to see if the sale or purchase ids are already exist
        function checkExistIds() {
            if ($('.table-ids').length) {
                $('.table-ids td').each(function () {
                    var codes = $.trim($(this).find('.ids').text());
                    var id = $(this).find('.selected-purchase-ids').val();
                    PurchaseIds[id] = codes;
                });
            }
            //console.log(PurchaseIds);
        }

        $('body').on('click', '.btn-add-sale-id', function () {
            checkExistIds();
            $('#modalSearchPo').modal('show');
        });

        //update sale id selections
        function updatePurchaseId() {
            var $tbl = ''; console.log(PurchaseIds);
            if (Object.keys(PurchaseIds).length > 0) {
                $tbl += '<table class="table-ids purchase-ids"><tbody>';
                    var idx = 0;
                    var i = 0;
                    $.each(PurchaseIds, function( index, value ) { //index here is the ids of sale or purchase
                        if (idx === 0) {
                            $tbl += '<tr>';
                        }

                        $tbl += '<td>';
                            $tbl += '<div class="pid purchase-id">';
                                $tbl += '<span data-id="'+ index +'" class="glyphicon glyphicon-remove-circle icon-remove-id"></span>';
                                $tbl += '<div class="ids">'+ value +'</div>';
                                $tbl += '<input type="hidden" value="'+ index +'" name="purchase_payments['+ i +'][purchase_id]" class="selected-purchase-ids" />';
                            $tbl += '</div>';
                        $tbl += '</td>';

                        if (idx === 4) {
                            $tbl += '</tr>';
                            idx = 0;
                        } else {
                            idx++;
                        }
                        i++;
                    });
                $tbl += '</tbody></table>';
            }
            $('.purchase-id-section').empty().html($tbl);
        }

        $('body').on('click', '.po-ok', function () {
            checkExistIds();

            $('.check-value').each(function () {
                if ($(this).is(':checked')) {
                    PurchaseIds[$(this).val()] = $(this).attr('data-purchasenumber');
                }
            });

            $('#product-amount').val($('.selected-amounts').text());
            updatePurchaseId();

            $('.close').click();
            updateProductAmount();
        });

        $('body').on('click', '.icon-remove-id', function () {
            if (Object.keys(PurchaseIds).length > 0) {
                var key = $(this).attr('data-id');
                delete PurchaseIds[key];
                if (Object.keys(PurchaseIds).length > 0) {
                    updatePurchaseId();
                } else {
                    $('.purchase-id-section').empty().html('<input type="hidden" value="" name="purchase_payments[0][sale_id]" class="selected-purchase-ids">');
                }
            } else {
                $('.purchase-id-section').empty().html('<input type="hidden" value="" name="purchase_payments[0][sale_id]" class="selected-purchase-ids">');
            }
            updateProductAmount();
        });

        $('#btn-register').on('click', function() {
            $('.help-block').empty().hide();
            var data = $('.form-payments').serialize();
            $.ajax({
                url: $('.form-payments').attr('action'),
                type: 'POST',
                dataType: 'JSON',
                data: data,
                cache: false,
                beforeSend: function() {
                    $.LoadingOverlay('show');
                },
                success: function(response) {
                    $.LoadingOverlay('hide');
                    if (response.status === 1) {
                        window.location.href = '<?php echo $this->Url->build('/payments/index-purchase/'); ?>';
                    } else if (response.status === 0) {
                        $.each(response.message, function(key, elem) {
                            var validate = $('#error-' + key);
                            $.each(elem, function(k, v) {
                                if (key == 'additional_payments') {
                                    $.each(v, function(k1, v1) {
                                        $.each(v1, function(k2, v2) {
                                            $('#error-purchase_id-' + k).removeClass('help-tips')
                                                .addClass('error-tips')
                                                .text(v2)
                                                .show();
                                        });
                                    });
                                }

                                if (key == 'purchase_payments') {
                                    $.each(v, function(k1, v1) {
                                        $.each(v1, function(k2, v2) {
                                            $('#error-sale_id').removeClass('help-tips')
                                                .addClass('error-tips')
                                                .text(v2)
                                                .show();
                                        });
                                    });
                                }

                                validate.removeClass('help-tips')
                                    .addClass('error-tips')
                                    .text(v)
                                    .show();
                            });
                        });
                    }
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    $.LoadingOverlay('hide');
                    if (errorThrown === 'Forbidden') {
                        if (confirm('<?= __('TXT_SESSION_TIMEOUT') ?>')) {
                            location.reload();
                        }
                    }
                }
            });
        });

        /**
        * PDF File Management
         */
        $('body').on('click', '.btn-add-pdf', function () {
            var newFile = '';
            var className = ' file-uploaded';
            if ($('.uploaded-file').children('.file-uploaded').length >= 1) {
                className = ' file-uploaded new-row-pdf';
            }

            newFile += '<div class="pdf-files'+className+'">';
                newFile += '<div class="media-file-type"><?= __('Add File') ?></div>';
                newFile += '<span class="glyphicon glyphicon-remove icon-remove-file"></span> ';
                newFile += '<input type="hidden" name="medias[0][file_name]" value="" class="file-name" />';
                newFile += '<input type="hidden" name="medias[0][document_type]" value="" class="document-type">';
            newFile += '</div>';
            $('.uploaded-file').append(newFile);
            updateIndexFile();
        });

        $('body').on('click', '.pdf-files', function () {
            $('.file-index').val($(this).attr('id'));
            $('#file').replaceWith($('#file').val('').clone(true));
            $('.old-pdf-file').val($(this).find('.file-name').val());
            var isEdit = $(this).find('.document-type').val();
            if (isEdit != '') {
                $('select[name="document_type"]').val(isEdit).change();
            }
            $('#modalPDFUpload').modal('show');
        });

        $('body').on('click', '.pdf-upload-file', function () {
            uploadImage();
        });

        function updateIndexFile() {
            if ($('.file-name').length) {
                $('.file-name').each(function (i, v) {
                    $(this).attr('name', 'medias[' + i + '][file_name]');
                });
            }
            if ($('.document-type').length) {
                $('.document-type').each(function (i, v) {
                    $(this).attr('name', 'medias[' + i + '][document_type]');
                });
            }
            if ($('.pdf-files').length) {
                $('.pdf-files').each(function (i, v) {
                    $(this).attr('id', 'pdf-' + i);
                });
            }
        }

        $('body').on('click', '.icon-remove-file', function (e) {
            e.stopPropagation();
            var fileName = $(this).parents('.pdf-files').attr('data-name');
            $('.' + fileName).remove();
            $(this).parents('.pdf-files').remove();
            updateIndexFile();
        });

        function uploadImage() {
            $('.help-block').empty().hide();
            $('.form-pdf-upload').ajaxForm({
                url: '<?php echo $this->Url->build('/payments/upload/'); ?>',
                type: 'POST',
                dataType: 'JSON',
                cache: false,
                contentType: false,
                processData: false,
                beforeSend: function () {
                    $.LoadingOverlay('show');
                },
                success: function (response) {
                    $.LoadingOverlay('hide');
                    if (response.status === 0) {
                        $.each(response.message, function(key, elem) {
                            $('#error-' + key)
                                .addClass('error-tips')
                                .text(elem)
                                .show();
                        });
                    } else {
                        var indexId = $('.file-index').val();
                        var doc_type = $('select[name="document_type"]').val();
                        var selectedType = $('select[name="document_type"]').find('option:selected').text();
                        $('#' + indexId).find('.document-type').val(doc_type);
                        $('#' + indexId).find('.media-file-type').text(selectedType);
                        $('#' + indexId).find('.file-name').val(response.file.new_name);
                        $('.close').click();
                    }
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    $.LoadingOverlay('hide');
                    if (errorThrown === 'Forbidden') {
                        if (confirm('<?= __('TXT_SESSION_TIMEOUT') ?>')) {
                            location.reload();
                        }
                    }
                }
            }).submit();
        }

//        $('body').on('click', '.btn-add-pdf', function () {
//            $('#modalPDFUpload').modal('show');
//        });
//
//        $('body').on('click', '.pdf-upload-file', function () {
//            uploadImage();
//        });
//
//        function updateIndexFile() {
//            if ($('.file-name').length) {
//                $('.file-name').each(function (i, v) {
//                    $(this).attr('name', 'medias[' + i + '][file_name]');
//                });
//            }
//        }
//
//        $('body').on('click', '.icon-remove-file', function () {
//            var fileName = $(this).parents('.pdf-files').attr('data-name');
//            $('.' + fileName).remove();
//            $(this).parents('.pdf-files').remove();
//            updateIndexFile();
//        });
//
//        function uploadImage() {
//            $('.help-block').empty().hide();
//            $('.form-pdf-upload').ajaxForm({
//                url: '<?php echo $this->Url->build('/payments/upload/'); ?>',
//                type: 'POST',
//                dataType: 'JSON',
//                cache: false,
//                contentType: false,
//                processData: false,
//                beforeSend: function () {
//                    $.LoadingOverlay('show');
//                },
//                success: function (response) {
//                    $.LoadingOverlay('hide');
//                    if (response.status === 0) {
//                        $.each(response.message, function(key, elem) {
//                            $('#error-' + key)
//                                .addClass('error-tips')
//                                .text(elem)
//                                .show();
//                        });
//                    } else {
//                        var newFile = '';
//                        var className = ' file-uploaded';
//                        if ($('.uploaded-file').children('.file-uploaded').length >= 1) {
//                            className = ' file-uploaded new-row-pdf';
//                        }
//
//                        newFile += '<div class="pdf-files'+className+'">';
//                            newFile += '<input type="hidden" name="medias[0][file_name]" value="'+response.file.new_name+'" class="file-name" />';
//                            newFile += '<span class="glyphicon glyphicon-remove-circle icon-remove-file"></span> ' + response.file.original_name;
//                        newFile += '</div>';
//                        $('.uploaded-file').append(newFile);
//                        updateIndexFile();
//                    }
//                },
//                error: function(jqXHR, textStatus, errorThrown) {
//                    $.LoadingOverlay('hide');
//                    if (errorThrown === 'Forbidden') {
//                        if (confirm('<?= __('TXT_SESSION_TIMEOUT') ?>')) {
//                            location.reload();
//                        }
//                    }
//                }
//            }).submit();
//        }

    })();
</script>
