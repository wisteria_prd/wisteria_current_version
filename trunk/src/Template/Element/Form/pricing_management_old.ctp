<div class="col-sm-12">
    <?php
    echo $this->Form->create($entity, [
        'role' => 'form',
        'class' => 'form-horizontal from-pricing',
        'name' => 'common_form'
    ]);
    $this->Form->templates([
        'inputContainer' => '{{content}}'
    ]); ?>
    <div class="form-group custom-select">
        <label class="col-sm-2 control-label">
            <?php echo __d('pricing', 'TXT_SELLER_TYPE') . $this->Comment->formAsterisk() ?>
        </label>
        <div class="col-sm-4">
            <?php
            echo $this->Form->input('external_id_type', [
                'class' => 'form-control',
                'label' => false,
                'required' => false,
                'options' => [
                    'manufacturer' => 'manufacturer',
                    'supplier' => 'supplier',
                    'customer' => 'customer',
                    'wisteria' => 'wisteria',
                ],
                'empty' => [
                    '' => __d('pricing', 'TXT_SELECT_SELLER_TYPE')
                ],
                'id' => 'external-id-type'
            ]); ?>
        </div>
    </div>
    <div class="form-group custom-select customer-wrapper" style="<?php $entity->external_id_type === 'customer' ? 'display:none': ''; ?>">
        <label class="col-sm-2 control-label">
            <?php echo __d('pricing', 'TXT_CUSTOMER') . $this->Comment->formAsterisk() ?>
        </label>
        <div class="col-sm-4">
            <?php
            echo $this->Form->input('_customer_id', [
                'class' => 'form-control',
                'label' => false,
                'required' => false,
                'options' => [],
                'empty' => ['' => __d('pricing', 'TXT_SELECT_CUSTOMER')],
                'id' => '_customer-id'
            ]); ?>
        </div>
    </div>
    <div class="form-group custom-select">
        <label class="col-sm-2 control-label">
            <?php echo __d('pricing', 'TXT_SELLER') . $this->Comment->formAsterisk() ?>
        </label>
        <div class="col-sm-4">
            <?php
            echo $this->Form->input('_external_id', [
                'class' => 'form-control',
                'label' => false,
                'required' => false,
                'options' => [],
                'empty' => ['' => __d('pricing', 'TXT_SELECT_SELLER')],
                'id' => '_external_id'
            ]); ?>
            <?php
            echo $this->Form->hidden('external_id', ['id' => 'external-id']); ?>
        </div>
    </div>
    <div class="form-group custom-select">
        <label class="col-sm-2 control-label">
            <?php echo __d('pricing', 'TXT_PRODUCT_BRAND') . $this->Comment->formAsterisk() ?>
        </label>
        <div class="col-sm-4">
            <?php
            echo $this->Form->input('_product_brand_id', [
                'class' => 'form-control',
                'label' => false,
                'required' => false,
                'options' => [],
                'empty' => ['' => __d('pricing', 'TXT_SELECT_PRODUCT_BRAND')],
                'id' => '_product-brand-id'
            ]); ?>
            <?php
            echo $this->Form->hidden('product_brand_id', ['id' => 'product-brand-id']); ?>
        </div>
    </div>
    <div class="form-group custom-select">
        <label class="col-sm-2 control-label">
            <?php echo __d('pricing', 'TXT_PRODUCT') . $this->Comment->formAsterisk() ?>
        </label>
        <div class="col-sm-4">
            <?php
            echo $this->Form->input('_product_detail_id', [
                'class' => 'form-control',
                'label' => false,
                'required' => false,
                'options' => [],
                'empty' => ['' => __d('pricing', 'TXT_SELECT_PRODUCT_NAME')],
                'id' => '_product-detail-id'
            ]); ?>
            <?php
            echo $this->Form->hidden('product_detail_id', ['id' => 'product-detail-id']); ?>
        </div>
    </div>
    <div class="form-group custom-select">
        <label class="col-sm-2 control-label">
            <?php echo __d('pricing', 'TXT_PRICE_TYPE') . $this->Comment->formAsterisk() ?>
        </label>
        <div class="col-sm-4">
            <?php
            echo $this->Form->input('price_type', [
                'class' => 'form-control',
                'label' => false,
                'required' => false,
                'options' => [
                    'standard_price' => 'standard_price',
                    'discount_price' => 'discount_price',
                    'clinic_price' => 'clinic_price',
                    'foc' => 'foc',
                ],
                'empty' => ['' => __d('pricing', 'TXT_SELECT_PRICE_TYPE')],
                'id' => 'price-type'
            ]); ?>
        </div>
    </div>
    <div class="form-group custom-select">
        <label class="col-sm-2 control-label">
            <?php echo __d('pricing', '_TXT_VALUE') . $this->Comment->formAsterisk() ?>
        </label>
        <div class="col-sm-4">
            <?php
            echo $this->Form->input('pricing_range_management.value', [
                'class' => 'form-control',
                'label' => false,
                'required' => false,
                'placeholder' => ['' => __d('pricing', '_TXT_SELECT_VALUE')],
            ]); ?>
        </div>
    </div>
    <div class="form-group custom-select">
        <label class="col-sm-2 control-label">
            <?php echo __d('pricing', '_TXT_VALUE_TYPE') . $this->Comment->formAsterisk() ?>
        </label>
        <div class="col-sm-4">
            <?php
            echo $this->Form->input('pricing_range_management.value_type', [
                'class' => 'form-control',
                'label' => false,
                'required' => false,
                'options' => [
                    'percent' => 'percent',
                    'amount' => 'amount',
                ],
                'empty' => ['' => __d('pricing', '_TXT_SELECT_VALUE_TYPE')],
            ]); ?>
        </div>
    </div>
    <div class="form-group custom-select">
        <label class="col-sm-2 control-label">
            <?php echo __d('pricing', 'TXT_RANGE_TYPE') . $this->Comment->formAsterisk() ?>
        </label>
        <div class="col-sm-4">
            <?php
            echo $this->Form->input('pricing_range_management.range_type', [
                'class' => 'form-control',
                'label' => false,
                'required' => false,
                'options' => [
                    'amount' => 'amount',
                    'quantity' => 'quantity',
                ],
                'empty' => ['' => __d('pricing', 'TXT_SELECT_RANG_TYPE')],
                'id' => 'range-type'
            ]); ?>
        </div>
    </div>
    <div class="form-group custom-select">
        <label class="col-sm-2 control-label">
            <?php echo __d('pricing', 'TXT_RANGE_CURRENCY') . $this->Comment->formAsterisk() ?>
        </label>
        <div class="col-sm-4">
            <?php
            echo $this->Form->input('pricing_range_management.currency_id', [
                'class' => 'form-control',
                'label' => false,
                'required' => false,
                'options' => $currencies,
                'value' => isset($entity->pricing_range_management->currency_id) ? $entity->pricing_range_management->currency_id : null,
                'empty' => ['' => __d('pricing', 'TXT_SELECT_RANG_CURRENCY')],
                'id' => 'pricing-type'
            ]); ?>
        </div>
    </div>
    <div class="form-group custom-select">
        <label class="col-sm-2 control-label">
            <?php echo __d('pricing', 'TXT_RANGE_FROM') . $this->Comment->formAsterisk() ?>
        </label>
        <div class="col-sm-4">
            <?php
            echo $this->Form->input('pricing_range_management.range_from', [
                'class' => 'form-control',
                'label' => false,
                'required' => false,
                'placeholder' => ['' => __d('pricing', 'TXT_ENTER_RANG_FROM')],
            ]); ?>
        </div>
    </div>
    <div class="form-group custom-select">
        <label class="col-sm-2 control-label">
            <?php echo __d('pricing', 'TXT_RANGE_TO') . $this->Comment->formAsterisk() ?>
        </label>
        <div class="col-sm-4">
            <?php
            echo $this->Form->input('pricing_range_management.range_to', [
                'class' => 'form-control',
                'label' => false,
                'required' => false,
                'placeholder' => ['' => __d('pricing', 'TXT_ENTER_RANG_TO')],
            ]); ?>
        </div>
    </div>

    <div class="form-group">
        <div class="col-sm-12 text-center">
            <?php echo $this->Form->button(__d('pricing', 'TXT_CANCEL'), [
                'type' => 'button',
                'class' => 'btn btn-default form_btn btn-width btn-sm',
                'id' => 'btn-clear',
            ]); ?>
            &nbsp; &nbsp;
            <?php
            echo $this->Form->button($this->request->action === 'edit' ? __d('pricing', 'TXT_UPDATE') : __d('pricing', 'TXT_REGISTER'), [
                'type' => 'submit',
                'class' => 'btn btn-primary form_btn btn-width btn-sm',
                'id' => 'btn-register',
            ]);
            ?>
        </div>
    </div>
    <?= $this->Form->end(); ?>
</div>
<script>
    $(document).ready(function() {
        // chose type
    	if ($('#external-id-type').val()) {
            let external_id_type = $('#external-id-type').val();
            let external_id = $('#external-id').val();
            $.post(BASE + 'pricing-managements/get-seller-name', {filter_type: external_id_type}, function(data) {
                $('#_external_id').html(data);
                $('#_external_id').select2('destroy');
                $('#_external_id').val(external_id);
                $('#_external_id').select2();
                if (external_id_type === 'wisteria') {
                    $('#external-id').val('0');
                }
            }, 'html');
        }

        // seller selected
        if ($('#external-id').val()) {
            let product_brand_id = $('#product-brand-id').val();
            let external_id = $('#external-id').val();
            $.post(BASE + 'pricing-managements/get-product-brand-name', {seller_id: external_id}, function(data) {
                $('#_product-brand-id').html(data);
                $('#_product-brand-id').select2('destroy');
                $('#_product-brand-id').val(product_brand_id);
                $('#_product-brand-id').select2();
                //$('#external-id').val($(that).val());
            }, 'html');
        }

        // product brand
        if ($('#product-brand-id').val()) {
            let product_detail_id = $('#product-detail-id').val();
            let product_brand_id = $('#product-brand-id').val();
            $.post(BASE + 'pricing-managements/get-product-detail-name', {product_brand_id: product_brand_id}, function(data) {
                $('#_product-detail-id').html(data);
                $('#_product-detail-id').select2('destroy');
                $('#_product-detail-id').val(product_detail_id);
                $('#_product-detail-id').select2();
            }, 'html');
        }
    });
    $(function() {

        function clearOption()
        {
            $('#external-id').val('');
            $('#product-brand-id').val('');
            $('#product-detail-id').val('');
            // empty product brand
            $('#_product-brand-id').html('');
            $('#_product-brand-id').select2('destroy');
            $('#_product-brand-id').select2();
            // empty product deail
            $('#_product-detail-id').html('');
            $('#_product-detail-id').select2('destroy');
            $('#_product-detail-id').select2();
        }

        // seller type
        $('#external-id-type').on('change', function() {
            clearOption();
            var that = this;
            $('#external-id').val('');
            $.post(BASE + 'pricing-managements/get-seller-name', {filter_type: $(that).val()}, function(data) {
                $('#_external_id').html(data);
                $('#_external_id').select2('destroy');
                $('#_external_id').select2();
                if ($(that).val() === 'wisteria') {
                    $('#external-id').val('0');
                }
            }, 'html');
        });

        // seller and set external_id
        $('#_external_id').on('change', function() {
            var that = this;
            $.post(BASE + 'pricing-managements/get-product-brand-name', {seller_id: $(that).val()}, function(data) {
                $('#_product-brand-id').html(data);
                $('#_product-brand-id').select2('destroy');
                $('#_product-brand-id').select2();
                $('#external-id').val($(that).val());
            }, 'html');
        });

        // product brand and set product-brand-id
        $('#_product-brand-id').on('change', function() {
            var that = this;
            $.post(BASE + 'pricing-managements/get-product-detail-name', {product_brand_id: $(that).val()}, function(data) {
                $('#_product-detail-id').html(data);
                $('#_product-detail-id').select2('destroy');
                $('#_product-detail-id').select2();
                $('#product-brand-id').val($(that).val());
            }, 'html');
        });

        // product detail and set value
        $('#_product-detail-id').on('change', function() {
            $('#product-detail-id').val($(this).val());
        });
    });
</script>
