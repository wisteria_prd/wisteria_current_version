
<?php
$controller = $this->request->controller;
$action = $this->request->action;
?>
<section id="register_form">
    <?php
    echo $this->Form->create(isset($data) ? $data : '', [
        'role' => 'form',
        'class' => 'form-horizontal',
        'name' => 'manufacturer_form',
        'type' => 'file',
    ]);
    echo $this->Form->hidden('product_id', [
        'value' => $this->request->query('product_id') ? $this->request->query('product_id') : $data->product_id,
    ]);
    ?>
    <div class="form-group">
        <div class="col-md-6 col-lg-6 col-sm-6">
            <label class="col-sm-4 control-label"><?= __('PRODUCT_DETAILS_TXT_PACK_SIZE') ?></label>
            <?php echo $this->Form->input('pack_size', [
                'type' => 'text',
                'class' => 'form-control',
                'required' => false,
                'label' => false,
                'placeholder' => __('PRODUCT_DETAILS_TXT_ENTER_PACK_SIZE'),
                'templates' => [
                    'inputContainer' => '<div class="col-sm-8">{{content}}</div>',
                    'inputContainerError' => '<div class="col-sm-8 has-error">{{content}}</div><label id="error-message" class="col-sm-8 col-sm-offset-4">{{error}}</label>'
                ]
            ]); ?>
        </div>
        <div class="col-lg-6 col-sm-6 col-md-6">
            <?php
            echo $this->Form->input('pack_size', [
                'type' => 'select',
                'class' => 'form-control',
                'label' => false,
                'required' => false,
                'options' => $packings,
                'empty' => ['' => __('PRODUCT_DETAILS_TXT_SELECT_PACK_TYPE')],
                'templates' => [
                    'inputContainer' => '<div class="col-sm-8 custom-select">{{content}}</div>',
                    'inputContainerError' => '<div class="col-sm-8 has-error custom-select">{{content}}</div><label id="error-message" class="col-sm-8">{{error}}</label>'
                ]
            ]);
            echo $this->Form->input('<i class="fa fa-plus" aria-hidden="true"></i>', [
                'type' => 'button',
                'class' => 'btn btn-sm btn-primary add_pack_size_id form-btn-add',
                'escape' => false,
                'label' => false,
                'templates' => [
                    'inputContainer' => '{{content}}',
                ]
            ]);
            ?>
        </div>
    </div>
    <div class="form-group">
        <div class="col-md-6 col-lg-6 col-sm-6">
            <label class="col-sm-4 control-label"><?= __('PRODUCT_DETAILS_TXT_SINGLE_UNIT_SIZE') ?></label>
            <?php echo $this->Form->input('single_unit_size', [
                'type' => 'text',
                'class' => 'form-control',
                'label' => false,
                'required' => false,
                'placeholder' => __('PRODUCT_DETAILS_TXT_ENTER_SINGLE_UNIT_SIZE'),
                'templates' => [
                    'inputContainer' => '<div class="col-sm-8">{{content}}</div>',
                    'inputContainerError' => '<div class="col-sm-8 has-error">{{content}}</div><label id="error-message" class="col-sm-8 col-sm-offset-4">{{error}}</label>'
                ]
            ]); ?>
        </div>
        <div class="col-lg-6 col-sm-6 col-md-6">
            <?php
            echo $this->Form->input('single_unit_id', [
                'type' => 'select',
                'class' => 'form-control',
                'label' => false,
                'required' => false,
                'empty' => ['' => __('PRODUCT_DETAILS_TXT_SELECT_SINGLE_UNIT')],
                'options' => $single_units,
                'templates' => [
                    'inputContainer' => '<div class="col-sm-8 custom-select">{{content}}</div>',
                    'inputContainerError' => '<div class="col-sm-8 has-error custom-select">{{content}}</div><label id="error-message" class="col-sm-8">{{error}}</label>'
                ]
            ]);
            echo $this->Form->input('<i class="fa fa-plus" aria-hidden="true"></i>', [
                'type' => 'button',
                'class' => 'btn btn-sm btn-primary add_single_unit form-btn-add',
                'escape' => false,
                'label' => false,
                'templates' => [
                    'inputContainer' => '{{content}}',
                ]
            ]);
            ?>
        </div>
    </div>
    <div class="form-group">
        <div class="col-md-6 col-lg-6 col-sm-6">
            <label class="col-sm-4 control-label"><?= __('PRODUCT_DETAILS_TXT_LOGISTIC_TEMPERATURE') ?></label>
            <?php
            echo $this->Form->input('logistic_temperature_id', [
                'type' => 'select',
                'class' => 'form-control',
                'label' => false,
                'required' => false,
                'empty' => ['' => __('PRODUCT_DETAILS_TXT_SELECT_LOGISTIC_TEMPERATURE')],
                'options' => $temperaturers,
                'templates' => [
                    'inputContainer' => '<div class="col-sm-7 custom-select">{{content}}</div>',
                    'inputContainerError' => '<div class="col-sm-7 has-error custom-select">{{content}}</div><label id="error-message" class="col-sm-8 col-sm-offset-4">{{error}}</label>'
                ]
            ]);
            echo $this->Form->input('<i class="fa fa-plus" aria-hidden="true"></i>', [
                'type' => 'button',
                'class' => 'btn btn-sm btn-primary add_temperaturer form-btn-add',
                'escape' => false,
                'label' => false,
                'templates' => [
                    'inputContainer' => '{{content}}',
                ]
            ]);
            ?>
        </div>
    </div>
    <div class="form-group">
        <div class="col-md-6 col-lg-6 col-sm-6">
            <label class="col-sm-4 control-label"><?= __('PRODUCT_DETAILS_TXT_PACKING') ?></label>
            <?php
            echo $this->Form->input('pack_size_id', [
                'type' => 'select',
                'class' => 'form-control',
                'label' => false,
                'required' => false,
                'empty' => ['' => __('PRODUCT_DETAILS_TXT_SELECT_PACKING')],
                'options' => $pack_sizes,
                'templates' => [
                    'inputContainer' => '<div class="col-sm-7 custom-select">{{content}}</div>',
                    'inputContainerError' => '<div class="col-sm-7 has-error custom-select">{{content}}</div><label id="error-message" class="col-sm-8 col-sm-offset-4">{{error}}</label>'
                ]
            ]);
            echo $this->Form->input('<i class="fa fa-plus" aria-hidden="true"></i>', [
                'type' => 'button',
                'class' => 'btn btn-sm btn-primary btn_add_packing_id form-btn-add',
                'escape' => false,
                'label' => false,
                'templates' => [
                    'inputContainer' => '{{content}}',
                ]
            ]);
            ?>
        </div>
    </div>
    <?php if (false) : ?>
    <div class="form-group">
        <div class="col-md-6 col-lg-6 col-sm-6">
            <label class="col-sm-4 control-label"><?= __('PRODUCT_DETAILS_TXT_PRODUCT_CATEGORY') ?></label>
            <?php echo $this->Form->input('type', [
                'type' => 'text',
                'class' => 'form-control',
                'label' => false,
                'required' => false,
                'placeholder' => __('PRODUCT_DETAILS_TXT_SELECT_PRODUCT_CATEGORY'),
                'templates' => [
                    'inputContainer' => '<div class="col-sm-8">{{content}}</div>',
                    'inputContainerError' => '<div class="col-sm-8 has-error">{{content}}</div><label id="error-message" class="col-sm-8 col-sm-offset-4">{{error}}</label>'
                ]
            ]); ?>
        </div>
    </div>
    <?php endif; ?>
    <div class="form-group">
        <div class="col-lg-12 col-md-12 col-sm-12">
            <label class="col-sm-2 control-label">
                <?php
                echo __d('product_detail', 'TXT_DESCRIPTION') ?>
            </label>
            <?php echo $this->Form->input('description', [
                'type' => 'textarea',
                'class' => 'form-control',
                'placeholder' => __d('product_detail', 'TXT_ENTER_DESCRIPTION'),
                'required' => false,
                'label' => false,
                'templates' => [
                    'inputContainer' => '<div class="col-lg-8 col-sm-8 col-md-8 textarea-wrap">{{content}}</div>',
                    'inputContainerError' => '<div class="col-sm-8 textarea-wrap has-error">{{content}}<label id="error-message">{{error}}</label></div>'
                ]
            ]); ?>
        </div>
    </div>
    <div class="form-group">
        <div class="col-lg-6 col-md-6 col-sm-6">
            <label class="col-sm-4 control-label"><?= __('PRODUCT_DETAILS_TXT_PATIENT_AGREEMENT') ?></label>
            <div class="col-sm-8">
                <?php echo $this->Form->checkbox('is_patient_agreement'); ?>
            </div>
        </div>
    </div>
    <div class="form-group">
        <div class="col-lg-6 col-md-6 col-sm-6">
            <label class="col-sm-4 control-label"><?= __('PRODUCT_DETAILS_TXT_IMPORT_CERTIFICATE') ?></label>
            <div class="col-sm-8">
                <?php echo $this->Form->checkbox('import_certificate'); ?>
            </div>
        </div>
    </div>
    <div class="form-group">
        <div class="col-lg-6 col-md-6 col-sm-6">
            <label class="col-sm-4 control-label"><?= __('PRODUCT_DETAILS_TXT_PURCHASE_AVAILABILITY') ?></label>
            <div class="col-sm-8">
                <?php echo $this->Form->checkbox('purchase_flag'); ?>
            </div>
        </div>
    </div>
    <div class="form-group">
        <div class="col-lg-6 col-md-6 col-sm-6">
            <label class="col-sm-4 control-label"><?= __('PRODUCT_DETAILS_TXT_SALE_AVAILABILITY') ?></label>
            <div class="col-sm-8">
                <?php echo $this->Form->checkbox('sale_flag'); ?>
            </div>
        </div>
    </div>
    <div class="form-group">
        <div class="col-lg-12 col-md-12 col-sm-12">
            <label class="col-sm-2 control-label">
                <?php
                echo __d('product_detail', 'TXT_REMARKS') ?>
            </label>
            <?php echo $this->Form->input('remark', [
                'type' => 'textarea',
                'class' => 'form-control',
                'placeholder' => __d('product_detail', 'TXT_ENTER_REMARK'),
                'required' => false,
                'label' => false,
                'templates' => [
                    'inputContainer' => '<div class="col-lg-8 col-sm-8 col-md-8 textarea-wrap">{{content}}</div>',
                    'inputContainerError' => '<div class="col-sm-8 textarea-wrap has-error">{{content}}<label id="error-message">{{error}}</label></div>'
                ]
            ]); ?>
        </div>
    </div>
    <div class="form-group">
        <div class="col-lg-12 col-sm-12 col-md-12 text-center">
            <div class="form_btn_wrap">
                <?php echo $this->Html->link(__('PRODUCT_DETAILS_TXT_CANCEL'), [
                    'controller' => 'product-details',
                    'action' => 'index',
                    '?' => ['product_id' => $this->request->query('product_id')]
                ], [
                    'class' => 'btn btn-default form_btn btn-sm btn-width',
                    'id' => 'btn_cancel',
                ]); ?>
                <?php echo $this->Form->submit(($action === 'edit') ? __('TXT_UPDATE') : __('PRODUCT_DETAILS_TXT_REGISTER'), [
                    'class' => 'btn btn-primary form_btn btn-sm btn-width',
                    'id' => 'btn_register',
                ]); ?>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
    <?php echo $this->Form->end(); ?>
</section>

<!--Modal Pack Size-->
<?php echo $this->element('Modal/ProductDetail/pack_size'); ?>
<!--Modal Single Unit-->
<?php echo $this->element('Modal/ProductDetail/single_unit'); ?>
<!--Modal Packing-->
<?php echo $this->element('Modal/ProductDetail/packing'); ?>
<?php echo $this->element('Modal/ProductDetail/temperature'); ?>

<?php
echo $this->Html->script([
    'jquery.validate.min',
], [['block' => 'script']]);
?>

<script>
    (function(e) {
        // open paking modal
        $('body').on('click', '.btn_add_packing_id', function(e) {
            $.LoadingOverlay('show');
            packing_list();
            $('.modal-packings-list').modal('show');
            packing_form();
        });

        // packing edit
        $('body').on('click', '.btn-packing-edit', function(e) {
            var tr = $(this).closest('tr');
            var form = $('.modal-packings-list').find('.form-packings-list');
            var param = {
                id: $(tr).attr('data-id'),
                name: $(tr).attr('data-name'),
                name_en: $(tr).attr('data-name-en'),
                unit_name: $(tr).attr('data-unit-name'),
                unit_name_en: $(tr).attr('data-unit-name-en')
            };

            $(form).find('input[name="name"]').val(param.name);
            $(form).find('input[name="name_en"]').val(param.name_en);
            $(form).find('input[name="unit_name"]').val(param.unit_name);
            $(form).find('input[name="unit_name_en"]').val(param.unit_name_en);
            $('.modal').find('.btn_packing_save').attr('data-id', param.id);
            $('.modal').find('.btn_packing_save').attr('data-target', 'edit');
            $('.modal').find('.btn_packing_save').text('<?= __('TXT_UPDATE') ?>');
        });

        // paking delete
        $('body').on('click', '.modal-packings-list .confirm-yes', function(e) {
            var id = $(this).closest('tr').attr('data-id');
            delete_record('<?php echo $this->Url->build('/packings/delete/'); ?>' + id, 'packing');
        });

        // paking disable
        $('body').on('click', '.modal-packings-list .btn-suspend-confirm-yes', function(e) {
            var params = {
                id: $(this).closest('tr').attr('data-id'),
                is_suspend: 0
            }
            disable_record('<?php echo $this->Url->build('/packings/update-suspend/'); ?>', params, 'packing');
        });

        // paking recover
        $('body').on('click', '.modal-packings-list .btn-recover-confirm-yes', function(e) {
            var params = {
                id: $(this).closest('tr').attr('data-id'),
                is_suspend: 1,
            }
            recover_record('<?php echo $this->Url->build('/packings/update-suspend/'); ?>', params, 'packing');
        });


        // pack size open modal
        $('body').on('click', '.add_pack_size_id', function(e) {
            $.LoadingOverlay('show');
            pack_size_list();
            $('.modal-pack-size-list').modal('show');
            pack_size_form();
        });

        // pack size delete
        $('body').on('click', '.modal-pack-size-list .confirm-yes', function(e) {
            var id = $(this).closest('tr').attr('data-id');
            delete_record('<?php echo $this->Url->build('/pack-sizes/delete/'); ?>' + id, 'pack-size');
        });

        // pack size disable
        $('body').on('click', '.modal-pack-size-list .btn-suspend-confirm-yes', function(e) {
            var params = {
                id: $(this).closest('tr').attr('data-id'),
                is_suspend: 0
            }
            disable_record('<?php echo $this->Url->build('/pack-sizes/update-suspend/'); ?>', params, 'pack-size');
        });

        // pack size recover
        $('body').on('click', '.modal-pack-size-list .btn-recover-confirm-yes', function(e) {
            var params = {
                id: $(this).closest('tr').attr('data-id'),
                is_suspend: 1,
            }
            recover_record('<?php echo $this->Url->build('/pack-sizes/update-suspend/'); ?>', params, 'pack-size');
        });

        // package size click edit
        $('body').on('click', '.btn-pack-size-edit-id', function(e) {
            var tr = $(this).closest('tr');
            var form = $('.modal-pack-size-list').find('.form-pack-size-list');
            var param = {
                id: $(tr).attr('data-id'),
                name: $(tr).attr('data-name'),
                name_en: $(tr).attr('data-name-en'),
                unit_name: $(tr).attr('data-unit-name'),
                unit_name_en: $(tr).attr('data-unit-name-en')
            };

            $(form).find('input[name="name"]').val(param.name);
            $(form).find('input[name="name_en"]').val(param.name_en);
            $(form).find('input[name="unit_name"]').val(param.unit_name);
            $(form).find('input[name="unit_name_en"]').val(param.unit_name_en);
            $('.modal').find('.btn_pack_size_save_id').attr('data-id', param.id);
            $('.modal').find('.btn_pack_size_save_id').attr('data-target', 'edit');
            $('.modal').find('.btn_pack_size_save_id').text('<?= __('TXT_UPDATE') ?>');
        });

        // pakage size form save or edit
        function pack_size_form()
        {
            $('form[name="pack-size"]').validate({
                rules: {
                    name: 'required',
                    name_en: 'required',
                    unit_name: 'required',
                    unit_name_en: 'required',
                },
                messages: {
                    name: '<?php echo __('TXT_MESSAGE_REQUIRED'); ?>',
                    name_en: '<?php echo __('TXT_MESSAGE_REQUIRED'); ?>',
                    unit_name: '<?php echo __('TXT_MESSAGE_REQUIRED'); ?>',
                    unit_name_en: '<?php echo __('TXT_MESSAGE_REQUIRED'); ?>',
                },
                errorClass: 'error-message',
                submitHandler: function(form) {
                    $.LoadingOverlay('show');
                    var target = $(form).find('button').attr('data-target');
                    var url = '<?php echo $this->Url->build('/pack-sizes/create'); ?>';
                    if (target === 'edit') {
                        url = '<?php echo $this->Url->build('/pack-sizes/edit/'); ?>' + $(form).find('button').attr('data-id');
                    }
                    $.post(url, $(form).serialize(), function(data) {
                        $('.form-pack-size-list').find('input[type=text]').val('');
                        pack_size_list();
                        $(form).find('button').text('<?= __d('product_detail', 'TXT_MODAL_PACKAGE_REGISTER') ?>').attr('data-target', 'new').removeAttr('data-id');
                    });
                    return false;
                }
            });
        }

        // pack size list
        function pack_size_list()
        {
            $.get('<?php echo $this->Url->build('/pack-sizes/get-list'); ?>', function(data) {
                if (data != null && data != undefined) {
                    var content = $('body').find('.modal-pack-size-list');
                    $(content).find('.table tbody tr').remove();
                    var element = '';
                    var response = data.data;
                    $.each(response, function(i, v) {
                        var name1 = (LOCALE === '_en') ? v.name_en : v.name;
                        var name2 = (LOCALE === '_en') ? v.unit_name_en : v.unit_name_en;
                        element += '<tr data-id="' + v.id + '" data-name="' + v.name + '" data-name-en="' + v.name_en + '" data-unit-name="' + v.unit_name + '" data-unit-name-en="' + v.unit_name_en + '">'
                                + '<td>' + (i+1) + '</td>'
                                + '<td>'+ disabledText(v.is_suspend) +'</td>'
                                + '<td>' + name1 + '</td>'
                                + '<td>' + name2 + '</td>'
                                + '<td class="text-right">'+ renderEditOrRecoverButton(v.is_suspend, 'btn-pack-size-edit-id') +'</td>'
                                + '<td class="text-right">'+ renderDeleteOrSuspend(v.is_suspend) +'</td>'
                                + '</tr>';
                    });
                    $(content).find('.table tbody').append(element);
                    $.LoadingOverlay('hide');
                }
            }).done(function(data) {
                popover_initialize('btn-tmp-delete');
                popover_disadble('btn-suspend');
                popover_recover('btn-recover');
            });
        }

        // single list
        function single_unit_list()
        {
            $.get('<?php echo $this->Url->build('/single-units/get-list'); ?>', function(data) {
                if (data != null && data != undefined) {
                    var content = $('body').find('.modal-single-unit');
                    $(content).find('.table tbody tr').remove();
                    var element = '';
                    var response = data.data;
                    $.each(response, function(i, v) {
                        element += '<tr data-id="' + v.id + '" data-name="' + v.name + '" data-name-en="' + v.name_en + '">'
                                + '<td>' + (i+1) + '</td>'
                                + '<td>'+ disabledText(v.is_suspend) +'</td>'
                                + '<td>' + v.name_en + '</td>'
                                + '<td class="text-right">'+ renderEditOrRecoverButton(v.is_suspend, 'btn-sunit-edit') +'</td>'
                                + '<td class="text-right">'+ renderDeleteOrSuspend(v.is_suspend) +'</td>'
                                + '</tr>';
                    });
                    $(content).find('.table tbody').append(element);
                    $.LoadingOverlay('hide');
                }
            }).done(function(data) {
                popover_initialize('btn-tmp-delete');
                popover_disadble('btn-suspend');
                popover_recover('btn-recover');
            });
        }

        function single_unit_form()
        {
            $('form[name="single-unit"]').validate({
                rules: {
                    name: 'required',
                    name_en: 'required'
                },
                messages: {
                    name: '<?php echo __('TXT_MESSAGE_REQUIRED'); ?>',
                    name_en: '<?php echo __('TXT_MESSAGE_REQUIRED'); ?>'
                },
                submitHandler: function(form) {
                    $.LoadingOverlay('show');
                    var target = $(form).find('button').attr('data-target');
                    var url = '<?php echo $this->Url->build('/single-units/create'); ?>';
                    if (target === 'edit') {
                        url = '<?php echo $this->Url->build('/single-units/edit/'); ?>' + $(form).find('button').attr('data-id');
                    }
                    $.post(url, $(form).serialize(), function(data) {
                        $('.modal-single-unit').find('input[type=text]').val('');
                        single_unit_list();
                        $(form).find('button').text('<?php echo __d('product_detail', 'TXT_MODAL_PACKAGE_REGISTER') ?>').attr('data-target', 'new').removeAttr('data-id');
                    });
                    return false;
                }
            });
        }

         // single unit add
         $('body').on('click', '.add_single_unit', function(e) {
             $.LoadingOverlay('show');
             single_unit_list()
             $('.modal-single-unit').modal('show');
             single_unit_form();
         });

         // sigle unit edit
         $('body').on('click', '.btn-sunit-edit', function(e) {
             var tr = $(this).closest('tr');
             var form = $('.modal-single-unit').find('.form-single-unit');
             var param = {
                 id: $(tr).attr('data-id'),
                 name: $(tr).attr('data-name'),
                 name_en: $(tr).attr('data-name-en')
             };

             $(form).find('input[name="name"]').val(param.name);
             $(form).find('input[name="name_en"]').val(param.name_en);
             $('.modal').find('.btn_sunit_save').attr('data-id', param.id);
             $('.modal').find('.btn_sunit_save').attr('data-target', 'edit');
             $('.modal').find('.btn_sunit_save').text('<?= __('TXT_UPDATE') ?>');
         });

         // single unit delete
         $('body').on('click', '.modal-single-unit .confirm-yes', function(e) {
             var id = $(this).closest('tr').attr('data-id');
             delete_record('<?php echo $this->Url->build('/single-units/delete/'); ?>' + id, 'single-unit');
         });

         // single unit disable
        $('body').on('click', '.modal-single-unit .btn-suspend-confirm-yes', function(e) {
            var params = {
                id: $(this).closest('tr').attr('data-id'),
                is_suspend: 0
            }
            disable_record('<?php echo $this->Url->build('/single-units/update-suspend/'); ?>', params, 'single-unit');
        });

        // single unit  recover
        $('body').on('click', '.modal-single-unit .btn-recover-confirm-yes', function(e) {
            var params = {
                id: $(this).closest('tr').attr('data-id'),
                is_suspend: 1,
            }
            recover_record('<?php echo $this->Url->build('/single-units/update-suspend/'); ?>', params, 'single-unit');
        });

         // append data to single unit select option
         $('body').on('click', '.btn-save-sng', function(e) {
             $.LoadingOverlay('show');
             $.get('<?php echo $this->Url->build('/single-units/get-list'); ?>', function(data) {
                 if (data != null && data != undefined) {
                     var element = '';
                     var response = data.data;
                     $.each(response, function(i, v) {
                         element += '<option value="' + v.id + '">' + v.name_en + '</option>';
                     });
                     $('body').find('#single-unit-id option').remove();
                     $('body').find('#single-unit-id').append(element);
                 }
             }).done(function(data) {
                 $.LoadingOverlay('hide');
                 $('.modal').modal('hide');
             });
         });

        function packing_form()
        {
            $('form[name="packing"]').validate({
                rules: {
                    name: 'required',
                    name_en: 'required',
                    unit_name: 'required',
                    unit_name_en: 'required',
                },
                messages: {
                    name: '<?php echo __('TXT_MESSAGE_REQUIRED'); ?>',
                    name_en: '<?php echo __('TXT_MESSAGE_REQUIRED'); ?>',
                    unit_name: '<?php echo __('TXT_MESSAGE_REQUIRED'); ?>',
                    unit_name_en: '<?php echo __('TXT_MESSAGE_REQUIRED'); ?>'
                },
                errorClass: 'error-message',
                submitHandler: function(form) {
                    $.LoadingOverlay('show');
                    var target = $(form).find('button').attr('data-target');
                    var url = '<?php echo $this->Url->build('/packings/create'); ?>';
                    if (target === 'edit') {
                        url = '<?php echo $this->Url->build('/packings/edit/'); ?>' + $(form).find('button').attr('data-id');
                    }
                    $.post(url, $(form).serialize(), function(data) {
                        $('.form-packings-list').find('input[type=text]').val('');
                        packing_list();
                        $(form).find('button').text('<?php echo __d('product_detail', 'TXT_MODAL_PACKAGE_REGISTER') ?>').attr('data-target', 'new').removeAttr('data-id');
                    });
                    return false;
                }
            });
        }

        function packing_list()
        {
            $.get('<?php echo $this->Url->build('/packings/get-list'); ?>', function(data) {
                if (data != null && data != undefined) {
                    var content = $('body').find('.modal-packings-list');
                    $(content).find('.table tbody tr').remove();
                    var element = '';
                    var response = data.data;
                    $.each(response, function(i, v) {
                        element += '<tr data-id="' + v.id + '" data-name="' + v.name + '" data-name-en="' + v.name_en + '" data-unit-name="' + v.unit_name + '" data-unit-name-en="' + v.unit_name_en + '">'
                                + '<td>' + (i+1) + '</td>'
                                + '<td>'+ disabledText(v.is_suspend) +'</td>'
                                + '<td>' + v.name + '</td>'
                                + '<td>' + v.unit_name + '</td>'
                                + '<td class="text-right">'+ renderEditOrRecoverButton(v.is_suspend, 'btn-packing-edit') +'</td>'
                                + '<td class="text-right">'+ renderDeleteOrSuspend(v.is_suspend) +'</td>'
                                + '</tr>';
                    });
                    $(content).find('.table tbody').append(element);
                    $.LoadingOverlay('hide');
                }
            }).done(function(data) {
                popover_initialize('btn-tmp-delete');
                popover_disadble('btn-suspend');
                popover_recover('btn-recover');
            });
        }

        // temperature edit
        $('body').on('click', '.btn-tmp-edit', function(e) {
            var tr = $(this).closest('tr');
            var form = $('.modal').find('form .form-group');
            var param = {
                id: $(tr).attr('data-id'),
                name_en: $(tr).attr('data-name-en'),
                name: $(tr).attr('data-name'),
                range: $(tr).attr('data-range')
            };

            $(form).find('input[name="name"]').val(param.name);
            $(form).find('input[name="name_en"]').val(param.name_en);
            $(form).find('input[name="temperature_range"]').val(param.range);
            $('.modal').find('.btn_tmp_save').attr('data-id', param.id);
            $('.modal').find('.btn_tmp_save').attr('data-target', 'edit');
            $('.modal').find('.btn_tmp_save').text('<?= __('TXT_UPDATE') ?>');
        });

        // append data to temperature select option
        $('body').on('click', '.btn-save-tmp', function(e) {
            $.LoadingOverlay('show');
            $.get('<?php echo $this->Url->build('/logistic-temperatures/get-list'); ?>', function(data) {
                if (data != null && data != undefined) {
                    var element = '';
                    var response = data.data;
                    $.each(response, function(i, v) {
                        element += '<option value="' + v.id + '">' + v.name_en + '</option>';
                    });
                    $('body').find('#logistic-tempearaturer-id option').remove();
                    $('body').find('#logistic-tempearaturer-id').append(element);
                }
            }).done(function(data) {
                $.LoadingOverlay('hide');
                $('.modal').modal('hide');
            });
        });

        // temperature
        $('body').on('click', '.add_temperaturer', function(e) {
            $.LoadingOverlay('show');
            temperaturer_form();
            $('.modal-temperature').modal('show');
            temperaturer_list();
        });

        // temperature delete
        $('body').on('click', '.modal-temperature .confirm-yes', function(e) {
            var id = $(this).closest('tr').attr('data-id');
            delete_record('<?php echo $this->Url->build('/logistic-temperatures/delete/'); ?>' + id, 'temperature');
        });

        // temperature disable
        $('body').on('click', '.modal-temperature .btn-suspend-confirm-yes', function(e) {
            var params = {
                id: $(this).closest('tr').attr('data-id'),
                is_suspend: 0
            }
            disable_record('<?php echo $this->Url->build('/logistic-temperatures/update-suspend/'); ?>', params, 'temperature');
        });

        // temperature recover
        $('body').on('click', '.modal-temperature .btn-recover-confirm-yes', function(e) {
            var params = {
                id: $(this).closest('tr').attr('data-id'),
                is_suspend: 1,
            }
            recover_record('<?php echo $this->Url->build('/logistic-temperatures/update-suspend/'); ?>', params, 'temperature');
        });

        function temperaturer_list()
        {
            $.get('<?php echo $this->Url->build('/logistic-temperatures/get-list'); ?>', function(data) {
                if (data != null && data != undefined) {
                    var content = $('body').find('.modal-temperature');
                    $(content).find('.table tbody tr').remove();
                    var element = '';
                    var response = data.data;
                    $.each(response, function(i, v) {
                        var name = (LOCALE === '_en') ? v.name_en : v.name;
                        element += '<tr data-id="' + v.id + '" data-name="' + v.name + '" data-name-en="' + v.name_en + '" data-range="' + v.temperature_range + '">'
                                + '<td>' + (i+1) + '</td>'
                                + '<td>'+ disabledText(v.is_suspend) +'</td>'
                                + '<td>' + name + '</td>'
                                + '<td>' + v.temperature_range + '</td>'
                                + '<td class="text-right">'+ renderEditOrRecoverButton(v.is_suspend, 'btn-tmp-edit') +'</td>'
                                + '<td class="text-right">'+ renderDeleteOrSuspend(v.is_suspend) +'</td>'
                                + '</tr>';
                    });
                    $(content).find('.table tbody').append(element);
                    $.LoadingOverlay('hide');
                }
            }).done(function(data) {
                popover_initialize('btn-tmp-delete');
                popover_disadble('btn-suspend');
                popover_recover('btn-recover');
            });
        }

        function renderEditOrRecoverButton(is_suspend, edit_class)
        {
            var html = '';
            if (is_suspend) {
                html += '<button type="button" data-toggle="popover" class="btn btn-recover btn-sm">'+'<?php echo BTN_ICON_UNDO; ?>'+'</button>';
            } else {
                html += '<button type="button" class="btn btn-primary btn-sm '+ edit_class +'">'+'<?php echo BTN_ICON_EDIT; ?>'+'</button>'
            }
            return html;
        }

        function renderDeleteOrSuspend(is_suspend)
        {
            var html = '';
            if (is_suspend) {
                html += '<button type="button" data-toggle="popover" class="btn btn-danger btn-sm btn-tmp-delete">'+'<?php echo BTN_ICON_DELETE; ?>'+'</button>';
            } else {
                html += '<button type="button" data-toggle="popover" class="btn btn-suspend btn-sm">'+'<?php echo BTN_ICON_STOP; ?>'+'</button>'
            }
            return html;
        }

        function temperaturer_form()
        {
            $('form[name="temperaturer"]').validate({
                rules: {
                    name: 'required',
                    name_en: 'required',
                    temperature_range: 'required'
                },
                messages: {
                    name: '<?php echo __('TXT_MESSAGE_REQUIRED'); ?>',
                    name_en: '<?php echo __('TXT_MESSAGE_REQUIRED'); ?>',
                    temperature_range: '<?php echo __('TXT_MESSAGE_REQUIRED'); ?>'
                },
                errorClass: 'error-message',
                submitHandler: function(form) {
                    $.LoadingOverlay('show');
                    var target = $(form).find('button').attr('data-target');
                    var url = '<?php echo $this->Url->build('/logistic-temperatures/create'); ?>';
                    if (target === 'edit') {
                        url = '<?php echo $this->Url->build('/logistic-temperatures/edit/'); ?>' + $(form).find('button').attr('data-id');
                    }
                    $.post(url, $(form).serialize(), function(data) {
                        $('.form-temperature').find('input[type="text"]').val('');
                        temperaturer_list();
                        $(form).find('button').text('<?= __('TXT_ADD') ?>').attr('data-target', 'new').removeAttr('data-id');
                    });
                    return false;
                }
            });
        }

        $('.modal').on('shown.bs.modal, hidden.bs.modal', function (e) {
            var button = $('.modal').find('button[type="submit"]');
            $('.modal').find('.form-inline label').remove();
            $('.modal').find('form input[type="text"]').val('');
            $('.modal').find('.form-group input').removeClass('error-message');
            $('.modal').find('.row input').removeClass('error-message');
            $('.modal').find('label[class="error-message"]').remove();
            $('.modal').find('.table>tbody tr').remove();
            $(button).attr({
                'data-target': 'new',
                'data-id': 0
            }).text('<?php echo __d('product_detail', 'TXT_MODAL_PACKAGE_REGISTER') ?>');
        });

        function popover_initialize(class_name)
        {
            var template = '<p><?php echo __('MSG_CONFIRM_DELETE'); ?></p><div class="text-center">'
                        + '<button class="btn btn-default confirm-no" type="button"><?php echo __('BTN_CANCEL'); ?></button>'
                        + '<button class="btn btn-default confirm-yes" type="button"><?php echo __('BTN_DELETE'); ?></button></div>';
            $('body').find('.' + class_name).popover({
                placement: 'left',
                html: 'true',
                content: template,
                toggle: 'popover',
                trigger: 'focus'
            });
        }

        function popover_disadble(class_name)
        {
            var template = '<p><?php echo __('MSG_CONFIRM_SUSPEND'); ?></p><div class="text-center">'
                + '<button class="btn btn-default btn-suspend-confirm-no" type="button"><?php echo __('BTN_CANCEL'); ?></button>&nbsp;&nbsp;&nbsp;&nbsp;'
                + '<button class="btn btn-primary btn-suspend-confirm-yes" type="button"><?php echo __('BTN_SUSPEND'); ?></button></div>';
            $('body').find('.' + class_name).popover({
                placement: 'left',
                html: 'true',
                content: template,
                toggle: 'popover',
                trigger: 'focus'
            });
        }

        function popover_recover(class_name)
        {
            var template = '<p><?php echo __('MSG_CONFIRM_UNDO_SUSPEND'); ?></p><div class="text-center">'
                + '<button class="btn btn-default btn-recover-confirm-no" type="button"><?php echo __('BTN_CANCEL'); ?></button>&nbsp;&nbsp;&nbsp;&nbsp;'
                + '<button class="btn btn-primary btn-recover-confirm-yes" type="button"><?php echo __('BTN_RECOVER'); ?></button></div>';
            $('body').find('.' + class_name).popover({
                placement: 'left',
                html: 'true',
                content: template,
                toggle: 'popover',
                trigger: 'focus'
            });
        }

        function delete_record(url, type)
        {
            $.post(url, function(data) {
                if (data != null && data != undefined) {
                    if (data.message === 'success') {
                        $.LoadingOverlay('show');
                        switch (type) {
                            case 'pack-size' :
                                pack_size_list();
                                break;
                            case 'single-unit' :
                                single_unit_list();
                                break;
                            case 'packing' :
                                packing_list();
                                break;
                            case 'temperature' :
                                temperaturer_list();
                                break;
                        }
                    } else {
                        alert('Warning ! This record use another place!');
                    }
                }
            }, 'json');
        }

        function disable_record(url, params, type)
        {
            $.post(url, params, function(data) {
                if (data != null && data != undefined) {
                    if (data.message === 'success') {
                        $.LoadingOverlay('show');
                        switch (type) {
                            case 'pack-size' :
                                pack_size_list();
                                break;
                            case 'single-unit' :
                                single_unit_list();
                                break;
                            case 'packing' :
                                packing_list();
                                break;
                            case 'temperature' :
                                temperaturer_list();
                                break;
                        }
                    } else {
                        alert('Warning ! This record use another place!');
                    }
                }
            }, 'json');
        }

        function recover_record(url, params, type)
        {
            $.post(url, params, function(data) {
                if (data != null && data != undefined) {
                    if (data.message === 'success') {
                        $.LoadingOverlay('show');
                        switch (type) {
                            case 'pack-size' :
                                pack_size_list();
                                break;
                            case 'single-unit' :
                                single_unit_list();
                                break;
                            case 'packing' :
                                packing_list();
                                break;
                            case 'temperature' :
                                temperaturer_list();
                                break;
                        }
                    } else {
                        alert('Warning ! This record use another place!');
                    }
                }
            }, 'json');
        }

        function disabledText(is_suspend)
        {
            var suspend = '&nbsp;';
            if (is_suspend) {
                suspend = '<p class="btn btn-warning btn-xs" disabled>' + '<?php echo  __('TXT_SUSPEND'); ?>'+ '</p>';
            }

            return suspend;
        }
    })();
</script>
