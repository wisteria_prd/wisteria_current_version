<?php
echo $this->Form->create($data, [
    'class' => 'form-horizontal',
    'novalidate' => true,
    'autocomplete' => 'off',
    'id' => 'formBasicRegister',
]);
?>
    <div class="form-group">
        <label for="userBasicEmail" class="col-md-4 control-label">comment</label>
        <div class="col-md-8">
            <?php
                echo $this->Form->select('role_id', $roles , [
                    'label' => false,
                    'class' => 'form-control',
                    'empty' => false
                ]);
            ?>
            <p class="help-block help-tips" id="error_userBasicEmail"></p>
        </div>
    </div>
    <div class="form-group">
        <label for="userBasicEmail" class="col-md-4 control-label">Permissions</label>
        <div class="col-md-8">
            <?php
            pr($roles);
                echo $this->Form->select('role_id', $roles , [
                    'label' => false,
                    'class' => 'form-control',
                    'empty' => false
                ]);
                echo $this->Form->checkbox('role_action_id', ['hiddenField' => false]);
            ?>
            <p class="help-block help-tips" id="error_userBasicEmail"></p>
        </div>
    </div>
<?php
if ($this->request->action == 'edit') {
    $submit_txt = __('Update');
} else {
    $submit_txt = __('Create');
}
?>
<?= $this->Form->button($submit_txt, ['class' => 'btn btn-primary pull-right']) ?>
<?= $this->Form->end() ?>