<div class="row">
    <div class="col-md-8 col-md-offset-2">
    <?php
    echo $this->Form->create('null', [
        'class' => 'form-horizontal form-register-reply',
        'autocomplete' => 'off',
        'type' => 'file',
    ]);
    echo $this->Form->hidden('image', [
        'class' => 'image-file-name'
    ]);
    $this->Form->templates([
        'inputContainer' => '{{content}}'
    ]);
    ?>
        <div class="form-group">
            <label for="urgent" class="col-sm-2"><?= __('Urgent') ?></label>
            <div class="col-sm-8">
                <div class="checkbox">
                    <label>
                        <?php
                            echo $this->Form->checkbox('is_urgent', [
                                'id' => 'urgent',
                                'label' => false
                            ]);
                        ?>
                    </label>
                </div>
                <span class="help-block help-tips" id="error-urgent"></span>
            </div>
        </div>
        <div class="form-group">
            <label for="user_id" class="col-sm-2"><?= __('In Charge') ?></label>
            <div class="col-sm-8">
                <?php
                echo $this->Form->select('receivers.0.user_id', $users, [
                    'id' => 'receiver_id',
                    'class' => 'form-control receiver_id',
                    'label' => false,
                    'empty' => ['' => __('All Users')]
                ]);
                ?>
                <span class="help-block help-tips" id="error-receiver_id"></span>
            </div>
        </div>
        <div class="form-group">
            <label for="title" class="col-sm-2"><?= __('Title') ?></label>
            <div class="col-sm-8">
                <?php
                echo $this->Form->input('title', [
                    'id' => 'title',
                    'class' => 'form-control',
                    'label' => false
                ]);
                ?>
                <span class="help-block help-tips" id="error-title"></span>
            </div>
        </div>
        <div class="form-group">
            <label for="message_category_id" class="col-sm-2"><?= __('Category') ?></label>
            <div class="col-sm-8">
                <?php
                echo $this->Form->select('message_category_id', $categories, [
                    'id' => 'message_category_id',
                    'class' => 'form-control',
                    'label' => false
                ]);
                ?>
                <span class="help-block help-tips" id="error-message_category_id"></span>
            </div>
            <div class="col-sm-1">
                <button type="button" class="btn btn-info"><span class="glyphicon glyphicon-plus"></span></button>
            </div>
        </div>
        <div class="form-group">
            <label for="description" class="col-sm-2"><?= __('Remarks') ?></label>
            <div class="col-sm-10">
                <?php
                echo $this->Form->input('description', [
                    'type' => 'textarea',
                    'id' => 'description',
                    'class' => 'form-control',
                    'label' => false
                ]);
                ?>
                <span class="help-block help-tips" id="error-description"></span>
            </div>
        </div>
        <div class="form-group">
            <label for="image" class="col-sm-2"><?= __('Photos') ?></label>
            <div class="col-sm-10">
                <div class="dropzone message-image-upload"></div>
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-6 text-right">
                <button type="button" class="btn btn-default btn-cancel"><?= __('Cancel') ?></button>
            </div>
            <div class="col-sm-6">
                <button type="button" class="btn btn-primary btn-register"><?= __('Register') ?></button>
            </div>
        </div>
    <?= $this->Form->end() ?>
    </div>
</div>