<?php $action = $this->request->action; ?>
<section id="register_form">
    <?php
    echo $this->Form->create($data, [
        'role' => 'form',
        'class' => 'form-horizontal seller-product',
        'name' => 'seller_brand_form',
        'onsubmit' => 'return false;',
    ]);
    if (!$data->isNew()) {
        echo $this->Form->hidden('id');
    }
    ?>
    <div class="form-group">
        <label class="col-sm-2 control-label">
            <?php
            echo __('SELLER_BRANDS_TXT_SUPPLIER') . $this->Comment->formAsterisk(); ?>
        </label>
        <div class="col-md-4 col-lg-4 col-sm-4 custom-select">
            <?php
            $seller = [];
            if (!$data->isNew()) {
                $seller_name = '';
                switch ($data->seller->type) {
                    case TYPE_MANUFACTURER:
                        $seller_name = $this->Comment->getFieldByLocal($data->seller->manufacturer, $locale);
                        break;

                    default:
                        $seller_name = $this->Comment->getFieldByLocal($data->seller->supplier, $locale);
                }
                $seller = [
                    $data->seller_id => $seller_name,
                ];
            }
            echo $this->Form->select('seller_id', $seller, [
                'class' => 'form-control',
                'label' => false,
                'required' => false,
                'id' => 'seller-id',
                'empty' => ['' => __('SELLER_BRANDS_TXT_SELECT_SUPPLIER')],
            ]); ?>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label">
            <?php
            echo __('SELLER_BRANDS_TXT_MANUFACTURER') . $this->Comment->formAsterisk(); ?>
        </label>
        <div class="col-md-4 col-lg-4 col-sm-4 custom-select">
            <?php
            $manuf = [];
            if (!$data->isNew() && isset($manufacturer)) {
                $manuf = [
                    $manufacturer->id => $this->Comment->getFieldByLocal($manufacturer, $locale),
                ];
            }
            echo $this->Form->select('manufacturer_id', $manuf, [
                'class' => 'form-control',
                'label' => false,
                'required' => false,
                'id' => 'manufacturer-id',
                'empty' => ['' => __('SELLER_BRANDS_TXT_SELECT_MANUFACTURER')],
                'value' => !$data->isNew() ? $data->product_brand->manufacturer_id : '',
            ]);
            ?>
        </div>
    </div>
    <div class="form-group">
        <div class="row" style="margin: 0;">
            <label class="col-sm-2 control-label">
                <?php
                echo __('SELLER_BRANDS_TXT_PRODUCT_BRAND') ?>
            </label>
            <div class="col-sm-4 chk-all">
                <div class="checkbox">
                    <label>
                        <?php
                        echo $this->Form->checkbox('all', [
                            'value' => 'all',
                            'hiddenField' => false,
                            'class' => 'check-all',
                            'checked' => isset($seller_brands) ? true : false,
                        ]);
                        echo '&nbsp;' . __('TXT_ALL');
                        ?>
                    </label>
                </div>
            </div>
        </div>
        <div class="row" style="margin: 0;">
            <div class="col-sm-4 col-sm-offset-2 cb-wrap">
                <?php
                if (!$data->isNew()) {
                    echo $this->element('SellerBrands/product_brands_list', [
                        'seller_brands' => $seller_brands,
                        'data' => $data,
                    ]);
                }
                ?>
            </div>
        </div>
    </div>
    <div class="form-group">
        <?php
        echo $this->ActionButtons->btnSaveCancel('SellerBrands', $action, null, null, null, 'btn-submit'); ?>
    </div>
    <?php echo $this->Form->end(); ?>
</section>

<script>
    $(function() {
        $('#seller-id').select2({
            width: '100%',
            minLength: 0,
            ajax: {
                url: BASE + 'Sellers/getListofSellersByAjax',
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    let query = {
                        keyword: params.term,
                        type: $('body').find('select[name="type"]').val()
                    };
                    return query;
                },
                processResults: function (data) {
                    let dataSource = [];
                    if (data.data === null && data.data === 'undefined') {
                        return false;
                    }
                    $.each(data.data, function(i, v) {
                        var name = '';
                        switch (v.type) {
                            case MANUFACTURER_TYPE:
                                name = getNameByLocal(LOCALE, v.manufacturer);
                                break;
                            case SUPPLIER:
                                name = getNameByLocal(LOCALE, v.supplier);
                                break;
                        }
                        dataSource.push({
                            id: v.id,
                            text: name
                        });
                    });
                    return { results: dataSource };
                }
            }
        });

        $('#manufacturer-id').select2({
            width: '100%',
            minLength: 0,
            ajax: {
                url: BASE + 'Manufacturers/getListOfManufacturersByAjax',
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    let query = {
                        keyword: params.term,
                        type: $('body').find('select[name="type"]').val()
                    };
                    return query;
                },
                processResults: function (data) {
                    let dataSource = [];
                    if (data.data === null && data.data === 'undefined') {
                        return false;
                    }
                    $.each(data.data, function(i, v) {
                        dataSource.push({
                            id: v.id,
                            text: getNameByLocal(LOCALE, v)
                        });
                    });
                    return { results: dataSource };
                }
            }
        });

        $('#manufacturer-id').on('select2:select', function (e) {
            var manufacturer_id = $(this).val();
            var params = {
                type: 'GET',
                url: BASE + 'SellerBrands/getProductBrandByManufacturerId',
                data: { manufacturer_id: manufacturer_id },
                beforeSend: function() {
                    $.LoadingOverlay('show');
                }
            };
            ajaxRequest(params, SESSION_TIMEOUT, function(data) {
                if ((data === null) && (data === 'undefined')) {
                    return false;
                }
                $('body').find('.cb-wrap').html(data);
            });
        });

        $('body').on('click', '.btn-submit', function(e) {
            var form = $('body').find('form[name="seller_brand_form"]');
            var params = {
                type: 'POST',
                url: BASE + 'SellerBrands/saveOrUpdate',
                data: $(form).serialize(),
                beforeSend: function() {
                    $.LoadingOverlay('show');
                    $(form).find('.error-message').remove();
                }
            };
            ajaxRequest(params, SESSION_TIMEOUT, function(data) {
                if (data.message === MSG_ERROR) {
                    showMessageError(form, data.data);
                    if ('product_brand_id' in data.data) {
                        var msg = '<label class="error-message">' + data.data.product_brand_id._required + '</label>';
                        $('body').find('.cb-wrap').append(msg);
                    }
                    return false;
                }
                location.href = BASE + 'SellerBrands';
            });
        });

        $('body').on('click', '.check-all', function () {
            $('input:checkbox').not(this).prop('checked', this.checked);
        });

        $('body').on('click', '.chk-brands', function () {
            if ($(this).prop('checked') !== true) {
                $('.check-all').prop('checked', false);
            }
        });
    });
</script>
