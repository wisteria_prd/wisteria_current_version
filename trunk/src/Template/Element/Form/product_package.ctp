<?php
$action = $this->request->action; ?>
<section id="create_form">
    <div class="row">
        <div class="col-sm-8 col-sm-offset-2">
            <?php
            echo $this->Form->create($product_package, [
                'role' => 'form',
                'class' => 'form-horizontal form-product-package',
                'name' => 'common_form',
                'autocomplete' => 'off',
            ]);
            $this->Form->templates([
                'inputContainer' => '{{content}}'
            ]); ?>
            <div class="form-group">
                <label class="col-sm-3 control-label">
                    <?php
                    echo __d('product_package', 'TXT_OLD_CODE') ?>
                </label>
                <div class="col-sm-6">
                    <?php
                    echo $this->Form->text('code', [
                        'class' => 'form-control',
                        'placeholder' => __d('product_package', 'TXT_ENTER_OLD_CODE'),
                        'label' => false,
                        'required' => false
                    ]); ?>
                    <span class="help-block help-tips" id="error-code"></span>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">
                    <?php
                    echo __('P_PACKAGE_NAME') . $this->Comment->formAsterisk(); ?>
                </label>
                <div class="col-sm-6">
                    <?php
                    echo $this->Form->input('name', [
                        'class' => 'form-control',
                        'placeholder' => __('P_PACKAGE_ENTER_PRODUCT_PACKAGE'),
                        'label' => false,
                        'required' => false
                    ]); ?>
                    <span class="help-block help-tips" id="error-name"></span>
                </div>
            </div>
            <div class="form-group custom-select">
                <label class="col-sm-3 control-label">
                    <?php
                    echo __('P_PACKAGE_CURRENCY') . $this->Comment->formAsterisk() ?>
                </label>
                <div class="col-sm-6">
                    <?php
                    echo $this->Form->select('currency_id', $currencies, [
                        'class' => 'form-control',
                        'empty' => ['' => __('P_PACKAGE_SELECT_CURRENCY')],
                        'label' => false,
                        'required' => false
                    ]); ?>
                    <span class="help-block help-tips" id="error-currency_id"></span>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">
                    <?php
                    echo __('P_PACKAGE_UNIT_PRICE') . $this->Comment->formAsterisk() ?>
                </label>
                <div class="col-sm-6">
                    <?php
                    echo $this->Form->input('unit_price', [
                        'class' => 'form-control',
                        'placeholder' => __('P_PACKAGE_ENTER_UNIT_PRICE'),
                        'label' => false,
                        'required' => false
                    ]); ?>
                    <span class="help-block help-tips" id="error-unit_price"></span>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">
                    <?php
                    echo __d('product_package', 'TXT_DESCRIPTION') ?>
                </label>
                <div class="col-sm-6">
                    <?php
                    echo $this->Form->input('description', [
                        'type' => 'textarea',
                        'class' => 'form-control',
                        'placeholder' => __d('product_package', 'TXT_ENTER_DESCRIPTION'),
                        'label' => false,
                        'required' => false
                    ]); ?>
                    <span class="help-block help-tips" id="error-remark"></span>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">
                    <?php
                    echo __d('product_package', 'TXT_REMARKS') ?>
                </label>
                <div class="col-sm-6">
                    <?php
                    echo $this->Form->input('remark', [
                        'type' => 'textarea',
                        'class' => 'form-control',
                        'placeholder' => __d('product_package', 'TXT_ENTER_REMARK'),
                        'label' => false,
                        'required' => false
                    ]); ?>
                    <span class="help-block help-tips" id="error-remark"></span>
                </div>
            </div>
            <div class="form-group">
                <?php
                echo $this->ActionButtons->btnSaveCancel('ProductPackages', $action); ?>
            </div>
            <?php echo $this->Form->end(); ?>
        </div>
    </div>
</section>

<script>
    (function() {
        $('#btn-register').on('click', function() {
            $('.help-block').empty().hide();
            var data = $('.form-product-package').serialize();
            $.ajax({
                url: $('.form-product-package').attr('action'),
                type: 'POST',
                dataType: 'JSON',
                data: data,
                cache: false,
                beforeSend: function() {
                    $.LoadingOverlay('show');
                },
                success: function(response) {
                    $.LoadingOverlay('hide');
                    if (response.status === 1) {
                        window.location.href = '<?php echo $this->Url->build('/product-packages/index/'); ?>';
                    } else if (response.status === 0) {
                        $.each(response.message, function(key, elem) {
                            var validate = $('#error-' + key);
                            $.each(elem, function(k, v) {
                                validate.removeClass('help-tips')
                                    .addClass('error-tips')
                                    .text(v)
                                    .show();
                            });
                        });
                    }
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    $.LoadingOverlay('hide');
                    if (errorThrown === 'Forbidden') {
                        if (confirm('<?= __('TXT_SESSION_TIMEOUT') ?>')) {
                            location.reload();
                        }
                    }
                }
            });
        });
    })();
</script>
