<?php $action = $this->request->action; ?>
<section id="create_form">
    <div class="row">
        <div class="col-sm-12 text-center">
            <h2 style="margin-bottom: 30px;"><?= __('TXT_IMPORT_INFO') ?></h2>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <?php
            echo $this->Form->create($import, [
                'role' => 'form',
                'class' => 'form-horizontal form-inter-import',
                'name' => 'common_form'
            ]);

            $this->Form->templates([
                'inputContainer' => '{{content}}'
            ]);

            $epd = ''; //expense_payment_date
            if ($import->expense_payment_date) {
                $epd = date('Y-m-d', strtotime($import->expense_payment_date));
            }
            $tpd = ''; //tax_payment_date
            if ($import->tax_payment_date) {
                $tpd = date('Y-m-d', strtotime($import->tax_payment_date));
            }
            $sod = ''; //ship_out_date
            if ($import->ship_out_date) {
                $sod = date('Y-m-d', strtotime($import->ship_out_date));
            }
            $ard = ''; //arrival_date
            if ($import->arrival_date) {
                $ard = date('Y-m-d', strtotime($import->arrival_date));
            }
            $ccd = ''; //custom_clearerance_date
            if ($import->custom_clearerance_date) {
                $ccd = date('Y-m-d', strtotime($import->custom_clearerance_date));
            }
            $rcd = ''; //receiving_date
            if ($import->receiving_date) {
                $rcd = date('Y-m-d', strtotime($import->receiving_date));
            }
            ?>
            <div class="form-group custom-select">
                <label class="control-label col-md-3 col-md-offset-2" for="">
                    <?php echo __d('import_international_product', 'TXT_PURCHASE_TYPE') . $this->Comment->formAsterisk() ?>
                </label>
                <div class="col-md-4">
                    <?php
                    $default = '';
                    $btn = '';
                    if ($action == 'create') {
                        $default = CUSTOMER_TYPE_NORMAL;
                        $btn = ' data-type="purchase_id"';
                    }
                    echo $this->Form->select('type', [
                        NORMAL_TYPE => __d('import_international_product', 'TXT_NORMAL'),
                        TYPE_SAMPLE => __d('import_international_product', 'TXT_SAMPLE'),
                    ], [
                        'label' => false,
                        'class' => 'form-control import-types',
                        'empty' => ['' => __d('import_international_product', 'TXT_SELECT_PURCHASE_TYPE')],
                        'default' => $default
                    ]);
                    ?>
                    <span class="help-block help-tips" id="error-type"></span>
                </div>
            </div>
            <div class="wrappers-id">
                <?php if ($action == 'create'): ?>
                <div class="form-group row-wid custom-select">
                    <label class="control-label col-md-3 col-md-offset-2" for="">
                        <?php echo __d('import_international_product', 'TXT_OF#') . $this->Comment->formAsterisk() ?>
                    </label>
                    <div class="col-md-4">
                        <?php
                        echo $this->Form->select('purchase_imports.0.purchase_id', $purchases, [
                            'label' => false,
                            'class' => 'form-control selected-id',
                            'empty' => ['' => __d('import_international_product', 'TXT_SELECT_OF')]
                        ]); ?>
                        <span class="help-block help-tips errors" id="error-purchase_id-0"></span>
                    </div>
                    <div class="col-md-1 field-no-padding-left">
                        <button<?= $btn ?> type="button" class="btn btn-sm btn-primary btn-add-more form-btn-add">
                            <i class="fa fa-plus" aria-hidden="true"></i>
                        </button>
                    </div>
                </div>
                <?php endif; ?>
                <?php
                if ($action == 'edit' && !empty($import->purchase_imports)) {
                    $idx = 0;
                    $button_class = '';
                    $input = '';
                    $err = '';
                    $icon = '';
                    $button_attr = '';
                    foreach ($import->purchase_imports as $imp) {
                        $label = '&nbsp;';
                        if ($idx == 0) {
                            $button_class = ' btn-add-more btn-primary';
                            $icon = '<i class="fa fa-plus" aria-hidden="true"></i>';
                        } else {
                            $button_class = '  btn-remove-id btn-delete';
                            $icon = '<i class="fa fa-trash" aria-hidden="true"></i>';
                        }

                        if (!empty($imp->sale)) {
                            $button_attr = ' data-type="sale_id"';
                            if ($idx == 0) {
                                $label = __('TXT_SALE_ID');
                            }
                            $input = $this->Form->select('purchase_imports.'.$idx.'.sale_id', $data, [
                                'label' => false,
                                'class' => 'form-control selected-id',
                                'empty' => ['' => __('TXT_SELECT_SALE_ID')],
                                'default' => $imp->sale->sale_number
                            ]);
                            $err = '<span class="help-block help-tips errors" id="error-sale_id-'.$idx.'"></span>';
                        } else {
                            $button_attr = ' data-type="purchase_id"';
                            if ($idx == 0) {
                                $label = __d('import_international_product', 'TXT_OF#');
                            }
                            $input = $this->Form->select('purchase_imports.'.$idx.'.purchase_id', $data, [
                                'label' => false,
                                'class' => 'form-control selected-id',
                                'empty' => ['' => __d('import_international_product', 'TXT_SELECT_OF')],
                                'default' => $imp->purchase->purchase_number
                            ]);
                            $err = '<span class="help-block help-tips errors" id="error-purchase_id-'.$idx.'"></span>';
                        }

                        $p = '<div class="form-group row-wid custom-select">';
                            $p .= '<label class="control-label col-md-3 col-md-offset-2" for="">'.$label.'</label>';
                            $p .= '<div class="col-md-4">';
                                $p .= $input;
                                $p .= $err;
                            $p .= '</div>';
                            $p .= '<div class="col-md-1 field-no-padding-left">';
                                $p .= '<button'.$button_attr.' type="button" class="btn btn-sm form-btn-add'.$button_class.'">';
                                    $p .= $icon;
                                $p .= '</button>';
                            $p .= '</div>';
                        $p .= '</div>';

                        echo $p;

                        $idx++;
                    }
                }
                ?>
            </div>
            <div class="form-group custom-select">
                <label class="control-label col-md-3 col-md-offset-2" for="">
                    <?php echo __d('import_international_product', 'TXT_CURRENCY') . $this->Comment->formAsterisk() ?>
                </label>
                <div class="col-md-4">
                    <?php
                    echo $this->Form->select('currency_id', $currencies, [
                        'type' => 'text',
                        'label' => false,
                        'class' => 'form-control',
                        'empty' => ['' => __d('import_international_product', 'TXT_SELECT_CURRENCY')]
                    ]); ?>
                    <span class="help-block help-tips" id="error-currency_id"></span>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-3 col-md-offset-2" for="">
                    <?php echo __d('import_international_product', 'TXT_IMPORT_EXPENSE') ?>
                </label>
                <div class="col-md-4">
                    <?php
                    echo $this->Form->input('import_expenses', [
                        'type' => 'text',
                        'label' => false,
                        'class' => 'form-control',
                        'placeholder' => __d('import_international_product', 'TXT_ENTER_IMPORT_EXPENSE')
                    ]); ?>
                    <span class="help-block help-tips" id="error-import_expenses"></span>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-3 col-md-offset-2" for="">
                    <?php echo __d('import_international_product', 'TXT_EXPENSE_PAYMENT_DATE') ?>
                </label>
                <div class="col-md-4">
                    <div class="input-group with-datepicker">
                        <?php
                        echo $this->Form->input('expense_payment_date', [
                            'class' => 'form-control datepicker',
                            'label' => false,
                            'placeholder' => __d('import_international_product', 'TXT_ENTER_EXPENSE_PAYMENT_DATE'),
                            'type' => 'text',
                            'value' => $epd
                        ]); ?>
                        <div class="input-group-btn">
                            <button type="button" class="btn btn-default trigger-calendar" aria-label="Calendar">
                                <span class="glyphicon glyphicon-calendar"></span>
                            </button>
                        </div>
                    </div>
                    <span class="help-block help-tips" id="error-expense_payment_date"></span>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-3 col-md-offset-2" for="">
                    <?php echo __d('import_international_product', 'TXT_IMPORT_TAX') . $this->Comment->formAsterisk() ?>
                </label>
                <div class="col-md-4">
                    <?php
                    echo $this->Form->input('import_tax', [
                        'type' => 'text',
                        'label' => false,
                        'class' => 'form-control',
                        'placeholder' => __d('import_international_product', 'TXT_ENTER_IMPORT_TAX')
                    ]); ?>
                    <span class="help-block help-tips" id="error-import_tax"></span>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-3 col-md-offset-2" for="">
                    <?php echo __d('import_international_product', 'TXT_IMPORT_TAX_PAYMENT_DATE') ?>
                </label>
                <div class="col-md-4">
                    <div class="input-group with-datepicker">
                        <?php
                        echo $this->Form->input('tax_payment_date', [
                            'class' => 'form-control datepicker',
                            'label' => false,
                            'placeholder' => __d('import_international_product', 'TXT_ENTER_IMPORT_TAX_PAYMENT_DATE'),
                            'type' => 'text',
                            'value' => $tpd
                        ]); ?>
                        <div class="input-group-btn">
                            <button type="button" class="btn btn-default trigger-calendar" aria-label="Calendar">
                                <span class="glyphicon glyphicon-calendar"></span>
                            </button>
                        </div>
                    </div>
                    <span class="help-block help-tips" id="error-tax_payment_date"></span>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-3 col-md-offset-2" for="">
                    <?php echo __d('import_international_product', 'TXT_EXPECTED_SHIPPING_DATE') ?>
                </label>
                <div class="col-md-4">
                    <div class="input-group with-datepicker">
                        <?php
                        echo $this->Form->input('ship_out_date', [
                            'class' => 'form-control datepicker',
                            'label' => false,
                            'placeholder' => __d('import_international_product', 'TXT_ENTER_EXPECTED_SHIPPING_DATE'),
                            'type' => 'text',
                            'value' => $sod
                        ]); ?>
                        <div class="input-group-btn">
                            <button type="button" class="btn btn-default trigger-calendar" aria-label="Calendar">
                                <span class="glyphicon glyphicon-calendar"></span>
                            </button>
                        </div>
                    </div>
                    <span class="help-block help-tips" id="error-ship_out_date"></span>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-3 col-md-offset-2" for="">
                    <?php echo __d('import_international_product', 'TXT_EXPECTED_ARRIVAL_DATE') ?>
                </label>
                <div class="col-md-4">
                    <div class="input-group with-datepicker">
                        <?php
                        echo $this->Form->input('arrival_date', [
                            'class' => 'form-control datepicker',
                            'label' => false,
                            'placeholder' => __d('import_international_product', 'TXT_ENTER_EXPECTED_ARRIVAL_DATE'),
                            'type' => 'text',
                            'value' => $ard
                        ]) ?>
                        <div class="input-group-btn">
                            <button type="button" class="btn btn-default trigger-calendar" aria-label="Calendar">
                                <span class="glyphicon glyphicon-calendar"></span>
                            </button>
                        </div>
                    </div>
                    <span class="help-block help-tips" id="error-arrival_date"></span>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-3 col-md-offset-2" for="">
                    <?php echo __d('import_international_product', 'TXT_EXPECTED_CUSTOM_CLEARANCE_DATE') ?>
                </label>
                <div class="col-md-4">
                    <div class="input-group with-datepicker">
                        <?php
                        echo $this->Form->input('custom_clearerance_date', [
                            'class' => 'form-control datepicker',
                            'label' => false,
                            'placeholder' => __d('import_international_product', 'TXT_ENTER_EXPECTED_CUSTOM_CLEARANCE_DATE'),
                            'type' => 'text',
                            'value' => $ccd
                        ]) ?>
                        <div class="input-group-btn">
                            <button type="button" class="btn btn-default trigger-calendar" aria-label="Calendar">
                                <span class="glyphicon glyphicon-calendar"></span>
                            </button>
                        </div>
                    </div>
                    <span class="help-block help-tips" id="error-custom_clearerance_date"></span>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-3 col-md-offset-2" for="">
                    <?php echo __d('import_international_product', 'TXT_EXPECTED_RECEIVING_DATE') ?>
                </label>
                <div class="col-md-4">
                    <div class="input-group with-datepicker">
                        <?php
                        echo $this->Form->input('receiving_date', [
                            'class' => 'form-control datepicker',
                            'label' => false,
                            'placeholder' => __d('import_international_product', 'TXT_ENTER_EXPECTED_RECEIVING_DATE'),
                            'type' => 'text',
                            'value' => $rcd
                        ]); ?>
                        <div class="input-group-btn">
                            <button type="button" class="btn btn-default trigger-calendar" aria-label="Calendar">
                                <span class="glyphicon glyphicon-calendar"></span>
                            </button>
                        </div>
                    </div>
                    <span class="help-block help-tips" id="error-receiving_date"></span>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-3 col-md-offset-2" for="">
                    <?php echo __d('import_international_product', 'TXT_TRACKING') . $this->Comment->formAsterisk() ?>
                </label>
                <div class="col-md-4">
                    <?php
                    echo $this->Form->input('courier_name', [
                        'type' => 'text',
                        'label' => false,
                        'class' => 'form-control',
                        'placeholder' => __d('import_international_product', 'TXT_ENTER_TRACKING')
                    ]); ?>
                    <span class="help-block help-tips" id="error-courier_name"></span>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-3 col-md-offset-2" for="">
                    <?php echo __d('import_international_product', 'TXT_DOCUMENT') ?>
                </label>
                <div class="col-md-4">
                    <div class="row" style="margin-left: 0; margin-right: 0;">
                        <div class="col-sm-10 uploaded-file" style="padding-left: 0">
                            <?php
                            if ($action == 'edit' && !empty($import->medias)) {
                                $y = 0;
                                $md = '';
                                foreach ($import->medias as $media) {
                                    $cls = '';
                                    if ($y >= 1) {
                                        $cls = ' new-row-pdf';
                                    }
                                    $md .= '<div class="pdf-files file-uploaded'.$cls.'" id="pdf-'.$y.'">';
                                        $md .= '<input type="hidden" name="medias['.$y.'][file_name]" value="'.$media->file_name.'" class="file-name">';
                                        $md .= '<input type="hidden" name="medias['.$y.'][document_type]" value="'.$media->document_type.'" class="document-type">';
                                        $md .= '<div class="media-file-type">'.$media->document_type.'</div>';
                                        $md .= '<span class="glyphicon glyphicon-remove icon-remove-file"></span> ';
                                    $md .= '</div>';
                                    $y++;
                                }

                                echo $md;
                            }
                            ?>
                        </div>
                        <div class="col-md-2 text-right" style="padding-right: 0">
                            <button type="button" class="btn btn-sm btn-primary btn-add-pdf form-btn-add">
                                <i class="fa fa-plus" aria-hidden="true"></i>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <?= $this->ActionButtons->btnSaveCancel('ImportInternationalProducts', $action); ?>
            </div>
            <?= $this->Form->end(); ?>
        </div>
    </div>
</section>
<!-- modal pdf upload -->
<?= $this->HtmlModal->modalHeader('modalPDFUpload', __('TXT_DOCUMENT_UPLOAD')) ?>
<div class="row">
    <div class="col-md-12">
        <?php
        echo $this->Form->create('Medias',[
            'autocomplete' => 'off',
            'class' => 'form-pdf-upload form-horizontal',
            'type' => 'file'
        ]);
        $this->Form->templates([
            'inputContainer' => '{{content}}'
        ]); ?>
        <div class="form-group">
            <label for="" class="control-label col-md-3"><?= __('TXT_TYPE') ?><?= $this->Comment->formAsterisk() ?></label>
            <div class="col-md-7">
                <?php
                    echo $this->Form->select('document_type', $files, [
                        'label' => false,
                        'class' => 'form-control',
                        'empty' => false
                    ]);
                    echo $this->Form->hidden('tmp', ['class' => 'file-index', 'value' => '']);
                    echo $this->Form->hidden('old_file', ['class' => 'old-pdf-file', 'value' => '']);
                ?>
                <span class="help-block help-tips" id="error-document_type"></span>
            </div>
        </div>
        <div class="form-group">
            <label for="" class="control-label col-md-3"><?= __('TXT_PDF_FILE') ?><?= $this->Comment->formAsterisk() ?></label>
            <div class="col-md-7">
                <?php
                    echo $this->Form->input('file', [
                        'type' => 'file',
                        'label' => false
                    ]);
                ?>
                <span class="help-block help-tips" id="error-file"></span>
            </div>
        </div>
        <?= $this->Form->end() ?>
    </div>
</div>
<div class="row">
    <div class="col-md-6 text-right">
        <button type="button" class="btn btn-default btn-sm btn-width" data-dismiss="modal"><?= __('TXT_CANCEL') ?></button>
    </div>
    <div class="col-md-6 text-left">
        <button type="button" class="btn btn-primary btn-sm btn-width pdf-upload-file"><?= __('TXT_UPLOAD') ?></button>
    </div>
</div>
<?= $this->HtmlModal->modalFooter(); ?>
<!-- /end modal pdf upload -->
<?php
    echo $this->Html->css([
        'bootstrap-datetimepicker.min',
        'jquery-ui',
        'intlTelInput',
    ]);
?>
<?php
    echo $this->Html->script([
        'jquery.form.min'
    ]);
?>
<script>
    (function() {
        $('.datepicker').datetimepicker({
            format: 'YYYY-MM-DD',
            minDate: '2007-01-01'
        });

        $('.trigger-calendar').click(function() {
            $(this).parents('.with-datepicker').find('.datepicker').focus();
        });

        $('body').on('change', '.import-types', function () {
            if ($(this).val() !== '') {
                $.LoadingOverlay('show');
                var url = '<?= $this->Url->build(['action' => 'importType']) ?>';
                var data = {type: $(this).val()};
                ajax_request_get(url, data, function (response) {
                    $.LoadingOverlay('hide');
                    if (response) {
                        $('.wrappers-id').empty().html(response);
                    }
                }, 'html');
            }
        });

        $('body').on('click', '.btn-add-more', function () {
            var type = $(this).attr('data-type');
            var item = $('.row-wid').first().clone();
            item.find('label').text('');
            item.find('button')
                .removeClass('btn-primary btn-add-more')
                .addClass('btn-remove-id btn-delete')
                .attr('data-type', type)
                .html('<i class="fa fa-trash" aria-hidden="true"></i>');
            item.find('select option').first().prop('selected', true);
            $('.wrappers-id').append(item);
            $('.selected-id').change();
            updateIndex(type);
        });

        $('body').on('click', '.btn-remove-id', function () {
            var type = $(this).attr('data-type');
            $(this).parents('.row-wid').remove();
            updateIndex(type);
            $('.selected-id').change();
        });

        function updateIndex(type) {
            $('.selected-id').each(function(i, v) {
                $(this).attr('name', 'purchase_imports[' + i + ']['+type+']');
            });
            $('.errors').each(function(i, v) {
                $(this).attr('id', 'error-'+type+'-' + i);
            });
        }

        $('body').on('change', '.selected-id', function(e) { //clone and disabled previous selection
            e.preventDefault();
            // enable all options
            $('option[disabled]').prop('disabled', false);

            $('.selected-id').each(function() {
                if (this.value !== '') {
                    $('.selected-id').not(this).find('option[value="' + this.value + '"]').prop('disabled', true);
                }
            });
        });

        $('#btn-register').on('click', function() {
            $('.help-block').empty().hide();
            var data = $('.form-inter-import').serialize();
            $.ajax({
                url: $('.form-inter-import').attr('action'),
                type: 'POST',
                dataType: 'JSON',
                data: data,
                cache: false,
                beforeSend: function() {
                    $.LoadingOverlay('show');
                },
                success: function(response) {
                    $.LoadingOverlay('hide');
                    if (response.status === 1) {
                        window.location.href = '<?php echo $this->Url->build('/import-international-products/index/'); ?>';
                    } else if (response.status === 0) {
                        $.each(response.message, function(key, elem) {
                            var validate = $('#error-' + key);
                            $.each(elem, function(k, v) {
                                if (key == 'id') {
                                    validate = $('#error-' + k);
                                }
                                validate.removeClass('help-tips')
                                    .addClass('error-tips')
                                    .text(v)
                                    .show();
                            });
                        });
                    }
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    $.LoadingOverlay('hide');
                    if (errorThrown === 'Forbidden') {
                        if (confirm('<?= __('TXT_SESSION_TIMEOUT') ?>')) {
                            location.reload();
                        }
                    }
                }
            });
        });

        $('body').on('click', '.btn-add-pdf', function () {
            var newFile = '';
            var className = ' file-uploaded';
            if ($('.uploaded-file').children('.file-uploaded').length >= 1) {
                className = ' file-uploaded new-row-pdf';
            }

            newFile += '<div class="pdf-files'+className+'">';
                newFile += '<div class="media-file-type"><?= __('Add File') ?></div>';
                newFile += '<span class="glyphicon glyphicon-remove icon-remove-file"></span> ';
                newFile += '<input type="hidden" name="medias[0][file_name]" value="" class="file-name" />';
                newFile += '<input type="hidden" name="medias[0][document_type]" value="" class="document-type">';
            newFile += '</div>';
            $('.uploaded-file').append(newFile);
            updateIndexFile();
        });

        $('body').on('click', '.pdf-files', function () {
            $('.file-index').val($(this).attr('id'));
            $('#file').replaceWith($('#file').val('').clone(true));
            $('.old-pdf-file').val($(this).find('.file-name').val());
            var isEdit = $(this).find('.document-type').val();
            if (isEdit != '') {
                $('select[name="document_type"]').val(isEdit).change();
            }
            $('#modalPDFUpload').modal('show');
        });

        $('body').on('click', '.pdf-upload-file', function () {
            uploadImage();
        });

        function updateIndexFile() {
            if ($('.file-name').length) {
                $('.file-name').each(function (i, v) {
                    $(this).attr('name', 'medias[' + i + '][file_name]');
                });
            }
            if ($('.document-type').length) {
                $('.document-type').each(function (i, v) {
                    $(this).attr('name', 'medias[' + i + '][document_type]');
                });
            }
            if ($('.pdf-files').length) {
                $('.pdf-files').each(function (i, v) {
                    $(this).attr('id', 'pdf-' + i);
                });
            }
        }

        $('body').on('click', '.icon-remove-file', function (e) {
            e.stopPropagation();
            var fileName = $(this).parents('.pdf-files').attr('data-name');
            $('.' + fileName).remove();
            $(this).parents('.pdf-files').remove();
            updateIndexFile();
        });

        function uploadImage() {
            $('.help-block').empty().hide();
            $('.form-pdf-upload').ajaxForm({
                url: '<?php echo $this->Url->build('/import-international-products/upload/'); ?>',
                type: 'POST',
                dataType: 'JSON',
                cache: false,
                contentType: false,
                processData: false,
                beforeSend: function () {
                    $.LoadingOverlay('show');
                },
                success: function (response) {
                    $.LoadingOverlay('hide');
                    if (response.status === 0) {
                        $.each(response.message, function(key, elem) {
                            $('#error-' + key)
                                .addClass('error-tips')
                                .text(elem)
                                .show();
                        });
                    } else {
                        var indexId = $('.file-index').val();
                        var doc_type = $('select[name="document_type"]').val();
                        var selectedType = $('select[name="document_type"]').find('option:selected').text();
                        $('#' + indexId).find('.document-type').val(doc_type);
                        $('#' + indexId).find('.media-file-type').text(selectedType);
                        $('#' + indexId).find('.file-name').val(response.file.new_name);
                        $('.close').click();
                    }
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    $.LoadingOverlay('hide');
                    if (errorThrown === 'Forbidden') {
                        if (confirm('<?= __('TXT_SESSION_TIMEOUT') ?>')) {
                            location.reload();
                        }
                    }
                }
            }).submit();
        }
    })();
</script>
