<?php $action = $this->request->action; ?>
<div class="row">
    <div class="col-sm-10 col-sm-offset-1">
        <section id="create_form">
    <?php
    echo $this->Form->create($medical_corp, [
        'role' => 'form',
        'class' => 'form-horizontal form-medical-corp',
        'name' => 'common_form'
    ]);

    $this->Form->templates([
        'inputContainer' => '{{content}}'
    ]);
    ?>
    <div class="form-group">
        <label class="col-sm-2 control-label"><?= __('TXT_OLD_CODE') ?></label>
        <div class="col-lg-4 col-md-4 col-sm-6">
            <?php echo $this->Form->input('code', [
                'type' => 'text',
                'class' => 'form-control',
                'placeholder' => __('TXT_ENTER_OLD_CODE'),
                'label' => false,
                'required' => false
            ]); ?>
            <span class="help-block help-tips" id="error-code_old"></span>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label"><?= __('TXT_NAME') ?><?= $this->Comment->formAsterisk() ?></label>
        <div class="col-md-4 col-lg-4 col-sm-6">
            <?php echo $this->Form->input('name_en', [
                'type' => 'text',
                'class' => 'form-control',
                'placeholder' => __('TXT_ENTER_NAME_ENGLISH'),
                'label' => false,
                'required' => false
            ]); ?>
            <span class="help-block help-tips" id="error-name_en"></span>
        </div>
        <div class="col-md-4 col-lg-4 col-sm-6">
            <?php echo $this->Form->input('name', [
                'type' => 'text',
                'class' => 'form-control',
                'placeholder' => __('TXT_ENTER_NAME'),
                'label' => false,
                'required' => false
            ]); ?>
            <span class="help-block help-tips" id="error-name"></span>
        </div>
    </div>
    <div class="form-group custom-select">
        <label class="col-sm-2 control-label"><?= __('TXT_POSTAL_CODE') ?><?php //echo $this->Comment->formAsterisk() ?></label>
        <div class="col-lg-4 col-md-4 col-sm-6">
            <?php echo $this->Form->input('postal_code', [
                'type' => 'text',
                'class' => 'form-control',
                'placeholder' => __('TXT_ENTER_POSTAL_CODE'),
                'label' => false,
                'required' => false
            ]); ?>
            <span class="help-block help-tips" id="error-postal_code"></span>
        </div>
        <div class="col-lg-4 col-sm-6 col-md-4">
            <?php
            echo $this->Form->select('country_id', $countries, [
                'class' => 'form-control select-countries',
                'empty' => ['' => __('TXT_SELECT_COUNTRY')],
                'label' => false,
                'required' => false
            ]);
            ?>
            <span class="help-block help-tips" id="error-country"></span>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label"></label>
        <div class="col-lg-4 col-sm-4 col-md-6">
            <?php echo $this->Form->input('building_en', [
                'type' => 'text',
                'class' => 'form-control',
                'placeholder' => __('TXT_ENTER_BUILDING_IN_EN'),
                'label' => false,
                'required' => false
            ]); ?>
            <span class="help-block help-tips" id="error-building_en"></span>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-4">
            <?php echo $this->Form->input('building', [
                'type' => 'text',
                'class' => 'form-control',
                'placeholder' => __('TXT_ENTER_BUILDING_IN_JP'),
                'label' => false,
                'required' => false
            ]); ?>
            <span class="help-block help-tips" id="error-building"></span>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label"></label>
        <div class="col-lg-4 col-sm-4 col-md-6">
            <?php echo $this->Form->input('street_en', [
                'type' => 'text',
                'class' => 'form-control',
                'placeholder' => __('TXT_ENTER_STREET_IN_EN'),
                'label' => false,
                'required' => false
            ]); ?>
            <span class="help-block help-tips" id="error-street_en"></span>
        </div>
        <div class="col-lg-4 col-sm-6 col-md-4">
            <?php echo $this->Form->input('street', [
                'type' => 'text',
                'class' => 'form-control',
                'placeholder' => __('TXT_ENTER_STREET_IN_JP'),
                'required' => false,
                'label' => false
            ]); ?>
            <span class="help-block help-tips" id="error-street"></span>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label"></label>
        <div class="col-lg-4 col-sm-6 col-md-4">
            <?php echo $this->Form->input('city_en', [
                'type' => 'text',
                'class' => 'form-control',
                'placeholder' => __('TXT_ENTER_CITY_EN'),
                'label' => false,
                'required' => false
            ]); ?>
            <span class="help-block help-tips" id="error-city_en"></span>
        </div>
        <div class="col-lg-4 col-sm-4 col-md-6">
            <?php echo $this->Form->input('city', [
                'type' => 'text',
                'class' => 'form-control',
                'placeholder' => __('TXT_ENTER_CITY_JP'),
                'label' => false,
                'required' => false
            ]); ?>
            <span class="help-block help-tips" id="error-city"></span>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label"></label>
        <div class="col-lg-4 col-sm-6 col-md-4">
            <?php echo $this->Form->input('prefecture_en', [
                'type' => 'text',
                'class' => 'form-control',
                'placeholder' => __('TXT_ENTER_PREFECTURE_IN_EN'),
                'required' => false,
                'label' => false
            ]); ?>
            <span class="help-block help-tips" id="error-prefecture_en"></span>
        </div>
        <div class="col-lg-4 col-sm-6 col-md-4">
            <?php echo $this->Form->input('prefecture', [
                'type' => 'text',
                'class' => 'form-control',
                'placeholder' => __('TXT_ENTER_PREFECTURE_IN_JP'),
                'label' => false,
                'required' => false
            ]); ?>
            <span class="help-block help-tips" id="error-prefecture"></span>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label"><?= __('STR_USER_PHONE') ?></label>
        <div class="col-lg-3 col-sm-6 col-md-3 c-lg">
            <?php
            $number = '';
            if ($action == 'edit' && !empty($medical_corp->info_details)) {
                $i = 0;
                foreach ($medical_corp->info_details as $phone) {
                    if (!empty($phone->phone) && $i == 0) {
                        $number = $phone->phone;
                        break;
                    }
                }
            }
            echo $this->Form->input('phone', [
                'type' => 'tel',
                'class' => 'form-control multiple-phone',
                'placeholder' => __('STR_ENTER_PHONE'),
                'required' => false,
                'value' => $number,
                'label' => false,
                'id' => 'phone-0'
            ]);
            echo $this->Form->hidden('info_details.0.phone', ['class' => 'multiple-phone-number']);
            ?>
            <input type="hidden" class="phone-type" value="<?= TYPE_MEDICAL_CORP ?>" name="info_details[0][type]" />
            <span class="help-block help-tips" id="error-phone"></span>
        </div>
        <div class="col-sm-1">
            <?php
            echo $this->Form->button('<i class="fa fa-plus" aria-hidden="true"></i>', [
                'type' => 'button',
                'class' => 'btn btn-sm btn-primary btn-phone-add form-btn-add',
                'escape' => false,
            ]);
            ?>
        </div>
    </div>
    <div class="phone-box-wrap">
        <?php
        if ($action == 'edit' && !empty($medical_corp->info_details)) {
            $idx = 0;
            foreach ($medical_corp->info_details as $phone) {
                if ($idx == 0) {
                    $idx++;
                    continue;
                } else {
                    if (!empty($phone->phone)) {
                        $multiple_tel = '<div class="form-group">';
                        $multiple_tel .= '<label class="col-sm-2 control-label"></label>';
                        $multiple_tel .= '<div class="col-lg-3 col-sm-6 col-md-3 c-lg">';
                        $multiple_tel .= '<input type="tel" class="form-control multiple-phone" value="'.$phone->phone.'" id="phone-'.$idx.'" placeholder="'.__('STR_ENTER_PHONE').'" name="phone" aria-invalid="false">';
                        $multiple_tel .= '<input type="hidden" class="multiple-phone-number" value="'.$phone->phone.'" name="info_details['.$idx.'][phone]">';
                        $multiple_tel .= '<input type="hidden" class="phone-type" value="'.TYPE_MEDICAL_CORP.'" name="info_details['.$idx.'][type]" />';
                        $multiple_tel .= '</div><div class="col-sm-1">';
                        $multiple_tel .= '<button type="button" class=" btn btn-delete btn-sm form-btn-add btn-remove-phone"><i class="fa fa-trash" aria-hidden="true"></i></button>';
                        $multiple_tel .= '</div></div>';
                        echo $multiple_tel;
                    }
                }
                $idx++;
            }
        }
        ?>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label"><?= __('Fax') ?></label>
        <div class="col-lg-4 col-sm-6 col-md-4">
            <?php echo $this->Form->input('fax', [
                'type' => 'text',
                'class' => 'form-control',
                'placeholder' => __('STR_ENTER_FAX'),
                'label' => false,
                'required' => false
            ]); ?>
            <span class="help-block help-tips" id="error-fax"></span>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label"><?= __('Url') ?></label>
        <div class="col-lg-4 col-sm-6 col-md-4">
            <?php echo $this->Form->input('url', [
                'type' => 'text',
                'class' => 'form-control',
                'placeholder' => __('STR_ENTER_URL'),
                'label' => false,
                'required' => false
            ]); ?>
            <span class="help-block help-tips" id="error-url"></span>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label"><?= __('STR_USER_EMAIL') ?></label>
        <div class="col-lg-4 col-md-4 col-sm-4">
            <?php
            echo $this->Form->input('info_details.0.email', [
                'type' => 'email',
                'class' => 'form-control multiple-mail',
                'placeholder' => __('STR_ENTER_EMAIL'),
                'label' => false,
                'required' => false,
                'id' => false
            ]);
            ?>
            <input type="hidden" class="mail-type" value="<?= TYPE_MEDICAL_CORP ?>" name="info_details[0][type]" />
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label"><?= __('STR_RECEIVER_NAME') ?></label>
        <div class="col-lg-4 col-sm-6 col-md-4">
            <?php echo $this->Form->input('reciever_name', [
                'type' => 'text',
                'class' => 'form-control',
                'placeholder' => __('STR_ENTER_RECEIVER'),
                'required' => false,
                'label' => false
            ]); ?>
        </div>
    </div>
    <div class="form-group custom-select">
        <label class="col-sm-2 control-label"><?= __('STR_PAYMENT_CAT') ?></label>
        <div class="col-lg-3 col-sm-3 col-md-3 c-lg">
            <?php echo $this->Form->input('payment_category_id', [
                'type' => 'select',
                'class' => 'form-control field-payment',
                'label' => false,
                'value' => isset($data) ? $data->payment_category_id : '',
                'options' => $payment_list,
                'empty' => ['' => __('TXT_SELECT_PAYMENT')]
            ]); ?>
        </div>
        <div class="col-lg-1 col-md-1 col-sm-1">
            <?php
            echo $this->Form->button('<i class="fa fa-plus" aria-hidden="true"></i>', [
                'type' => 'button',
                'class' => 'btn btn-sm btn-primary btn-payment-cats form-btn-add',
                'escape' => false,
            ]);
            ?>
        </div>
    </div>
    <div class="form-group custom-select">
        <label class="col-sm-2 control-label"><?= __('STR_INVOICE_DELIVERY') ?></label>
        <div class="col-lg-3 col-sm-3 col-md-3 c-lg">
            <?php echo $this->Form->input('invoice_delivery_method_id', [
                'type' => 'select',
                'class' => 'form-control field-invoice',
                'label' => false,
                'value' => isset($data) ? $data->invoice_delivery_method_id : '',
                'options' => $invoice_list,
                'empty' => ['' => __('TXT_SELECT_INVOICE')]
            ]); ?>
        </div>
        <div class="col-lg-1 col-md-1 col-sm-1">
            <?php
            echo $this->Form->button('<i class="fa fa-plus" aria-hidden="true"></i>', [
                'type' => 'button',
                'class' => 'btn btn-sm btn-primary btn-invoice-methods form-btn-add',
                'escape' => false,
            ]);
            ?>
        </div>
    </div>
    <div class="form-group">
        <?= $this->ActionButtons->btnSaveCancel('MedicalCorporations', $action); ?>
    </div>
    <div class="clearfix"></div>
    <?php
    echo $this->Form->hidden('country_code', [
        'value' => isset($data) ? substr($data->phone, 1, 3): 1,
    ]);
    ?>
    <?php echo $this->Form->end(); ?>
</section>
    </div>
</div>
<!--Modal Payment Category-->
<?php echo $this->element('Modal/payment_cat'); ?>

<!--Modal Invoice Delivery Method-->
<?php echo $this->element('Modal/invoice_method'); ?>

<!--Modal delete-->
<?php echo $this->element('Modal/delete'); ?>

<script>
    (function() {
        //get payment modal list function
        function get_payment() {
            $.ajax({
                url: '<?= $this->Url->build(['controller' => 'PaymentCategories', 'action' => 'getList']) ?>',
                type: 'GET',
                dataType: 'HTML',
                beforeSend: function() {
                    $.LoadingOverlay('show');
                },
                success: function(response) {
                    $.LoadingOverlay('hide');
                    clearForm();
                    if (response) {
                        $('.payment-table tbody').empty().append(response);
                        $('.modal-payment-cat').modal('show');

                        //clear close button class.
                        //this class will use to get payment dropdown refress if there's change or add new payment.
                        //otherwise just close modal without refresh.
                        $('.btn-close-payment-cat').removeClass('btn-refresh-payment');

                        $('.btn-add-payment-cat').text('<?= __('TXT_ADD') ?>');
                    }
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    $.LoadingOverlay('hide');
                    if (errorThrown === 'Forbidden') {
                        if (confirm('<?= __('TXT_SESSION_TIMEOUT') ?>')) {
                            location.reload();
                        }
                    }
                }
            });
        }

        //get invoice modal list function
        function get_invoice() {
            $.ajax({
                url: '<?= $this->Url->build(['controller' => 'InvoiceDeliveryMethods', 'action' => 'getList']) ?>',
                type: 'GET',
                dataType: 'HTML',
                beforeSend: function() {
                    $.LoadingOverlay('show');
                },
                success: function(response) {
                    $.LoadingOverlay('hide');
                    clearForm();
                    if (response) {
                        $('.invoice-table tbody').empty().append(response);
                        $('.modal-invoice-method').modal('show');

                        //clear close button class.
                        //this class will use to get invoice dropdown refress if there's change or add new invoice.
                        //otherwise just close modal without refresh.
                        $('.btn-close-invoice-method').removeClass('btn-refresh-invoice');

                        $('.btn-add-invoice-method').text('<?= __('TXT_ADD') ?>');
                    }
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    $.LoadingOverlay('hide');
                    if (errorThrown === 'Forbidden') {
                        if (confirm('<?= __('TXT_SESSION_TIMEOUT') ?>')) {
                            location.reload();
                        }
                    }
                }
            });
        }

        //get payment on modal
        $('body').on('click', '.btn-payment-cats', function () {
            get_payment();
        });

        //get invoice on modal
        $('body').on('click', '.btn-invoice-methods', function () {
            get_invoice();
        });

        function clearForm() {
            $('.clear').val('');
        }

        function reNumbering(className) {
            $('.' + className + ' tbody tr').each(function (i, v) {
                $(this).find('td').first().text(i+1);
            });
        }

        function updatingData(id, className, nameJp, nameEn) {
            var result = '<tr class="'+className+'-'+id+'">';
                result += '<td></td>';
                result += '<td class="name-jp">'+nameJp+'</td>';
                result += '<td class="name-en">'+nameEn+'</td>';
                result += '<td>';
                    result += '<button type="button" class="btn btn-primary btn-sm btn-edit-'+className+'" data-id="'+id+'"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button>&nbsp;&nbsp;';
                    result += '<button type="button" class="btn btn-delete btn-sm btn-delete-'+className+'" data-id="'+id+'"><i class="fa fa-trash" aria-hidden="true"></i></button>';
                result += '</td>';
            return result;
        }

        //add edit payment on form
        $('body').on('click', '.btn-edit-payment', function() {
            var tr = $(this).parents('tr');
            $('.payment-name-en').val(tr.find('td.name-en').text());
            $('.payment-name-jp').val(tr.find('td.name-jp').text());
            $('.payment-id').val($(this).attr('data-id'));

            $('.btn-add-payment-cat').text('<?= __('TXT_UPDATE') ?>');
        });

        //create or update payment category
        $('body').on('click', '.btn-add-payment-cat', function() {
            var data = $('.form-payment-category').serialize();
            var id = $('.payment-id').val();
            var url = '<?= $this->Url->build(['controller' => 'PaymentCategories', 'action' => 'create']) ?>';
            if (id !== '') {
                url = '<?= $this->Url->build(['controller' => 'PaymentCategories', 'action' => 'edit']) ?>';
            }
            $.ajax({
                url: url,
                type: 'POST',
                dataType: 'JSON',
                data: data,
                cache: false,
                beforeSend: function() {
                    $.LoadingOverlay('show');
                },
                success: function(response) {
                    $.LoadingOverlay('hide');
                    if (response.status === 1) {
                        var result = updatingData(response.data.id, 'payment', response.data.name, response.data.name_en);
                        if (id !== '') {
                            $('.payment-' + id).replaceWith(result);
                        } else {
                            $('.payment-table tbody').prepend(result);
                        }
                        clearForm();
                        $('.btn-add-payment-cat').text('<?= __('TXT_ADD') ?>');

                        //clear close button class.
                        //this class will use to get payment dropdown refress if there's change or add new payment.
                        //otherwise just close modal without refresh.
                        $('.btn-close-payment-cat').addClass('btn-refresh-payment');
                    } else if (response.status === 0) {
                        $.each(response.message, function(key, elem) {
                            var validate = $('#error-' + key);
                            $.each(elem, function(k, v) {
                                validate.removeClass('help-tips')
                                    .addClass('error-tips')
                                    .text(v)
                                    .show();
                            });
                        });
                    }
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    $.LoadingOverlay('hide');
                    if (errorThrown === 'Forbidden') {
                        if (confirm('<?= __('TXT_SESSION_TIMEOUT') ?>')) {
                            location.reload();
                        }
                    }
                },
                complete: function () {
                    reNumbering('payment-table');
                }
            });
        });

        //confirm delete payment category
        $('body').on('click', '.btn-delete-payment', function() {
            var id = $(this).attr('data-id');
            $('.delete-item').val(id);
            $('.btn-delete-item').attr('id', 'btn-delete-payment-yes');
            $('#modal_delete').modal('show');
        });

        function removeItem(url, data, itemRow) {
            $.ajax({
                url: url,
                type: 'POST',
                dataType: 'JSON',
                data: data,
                cache: false,
                beforeSend: function() {
                    $.LoadingOverlay('show');
                },
                success: function(response) {
                    $.LoadingOverlay('hide');
                    if (response.status === 1) {
                        $('.' + itemRow).remove();
                    } else if (response.status === 0) {
                        alert(response.message);
                    }
                    $('.close-modal').click();
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    $('.close-modal').click();
                    $.LoadingOverlay('hide');
                    if (errorThrown === 'Forbidden') {
                        if (confirm('<?= __('TXT_SESSION_TIMEOUT') ?>')) {
                            location.reload();
                        }
                    }
                },
                complete: function () {
                    reNumbering('payment-table');
                }
            });
        }

        //delete payment category
        $('body').on('click', '#btn-delete-payment-yes', function() {
            var data = $('.form-delete').serialize();
            var id = $('.delete-item').val();
            var url = '<?= $this->Url->build(['controller' => 'PaymentCategories', 'action' => 'delete']) ?>';
            var itemRow = 'payment-' + id;
            removeItem(url, data, itemRow);
        });
//==================================== INVOICE DELIVERY METHOD =============================================
        //add edit invoice on form
        $('body').on('click', '.btn-edit-invoice', function() {
            var tr = $(this).parents('tr');
            $('.invoice-name-en').val(tr.find('td.name-en').text());
            $('.invoice-name-jp').val(tr.find('td.name-jp').text());
            $('.invoice-id').val($(this).attr('data-id'));

            $('.btn-add-invoice-method').text('<?= __('TXT_UPDATE') ?>');
        });

        //create or update invoice delivery method
        $('body').on('click', '.btn-add-invoice-method', function() {
            var data = $('.form-invoice-method').serialize();
            var id = $('.invoice-id').val();
            var url = '<?= $this->Url->build(['controller' => 'InvoiceDeliveryMethods', 'action' => 'create']) ?>';
            if (id !== '') {
                url = '<?= $this->Url->build(['controller' => 'InvoiceDeliveryMethods', 'action' => 'edit']) ?>';
            }
            $.ajax({
                url: url,
                type: 'POST',
                dataType: 'JSON',
                data: data,
                cache: false,
                beforeSend: function() {
                    $.LoadingOverlay('show');
                },
                success: function(response) {
                    $.LoadingOverlay('hide');
                    if (response.status === 1) {
                        var result = updatingData(response.data.id, 'invoice', response.data.name, response.data.name_en);
                        if (id !== '') {
                            $('.invoice-' + id).replaceWith(result);
                        } else {
                            $('.invoice-table tbody').prepend(result);
                        }
                        clearForm();
                        $('.btn-add-invoice-method').text('<?= __('TXT_ADD') ?>');
                        //clear close button class.
                        //this class will use to get invoice dropdown refress if there's change or add new invoice.
                        //otherwise just close modal without refresh.
                        $('.btn-close-invoice-method').addClass('btn-refresh-invoice');
                    } else if (response.status === 0) {
                        $.each(response.message, function(key, elem) {
                            var validate = $('#error-' + key);
                            $.each(elem, function(k, v) {
                                validate.removeClass('help-tips')
                                    .addClass('error-tips')
                                    .text(v)
                                    .show();
                            });
                        });
                    }
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    $.LoadingOverlay('hide');
                    if (errorThrown === 'Forbidden') {
                        if (confirm('<?= __('TXT_SESSION_TIMEOUT') ?>')) {
                            location.reload();
                        }
                    }
                },
                complete: function () {
                    reNumbering('invoice-table');
                }
            });
        });

        //confirm delete invoice delivery method
        $('body').on('click', '.btn-delete-invoice', function() {
            var id = $(this).attr('data-id');
            $('.delete-item').val(id);
            $('.btn-delete-item').attr('id', 'btn-delete-invoice-yes');
            $('#modal_delete').modal('show');
        });

        function removeItem(url, data, itemRow) {
            $.ajax({
                url: url,
                type: 'POST',
                dataType: 'JSON',
                data: data,
                cache: false,
                beforeSend: function() {
                    $.LoadingOverlay('show');
                },
                success: function(response) {
                    $.LoadingOverlay('hide');
                    if (response.status === 1) {
                        $('.' + itemRow).remove();
                    } else if (response.status === 0) {
                        alert(response.message);
                    }
                    $('.close-modal').click();
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    $('.close-modal').click();
                    $.LoadingOverlay('hide');
                    if (errorThrown === 'Forbidden') {
                        if (confirm('<?= __('TXT_SESSION_TIMEOUT') ?>')) {
                            location.reload();
                        }
                    }
                },
                complete: function () {
                    reNumbering('invoice-table');
                }
            });
        }

        //delete invoice delivery method
        $('body').on('click', '#btn-delete-invoice-yes', function() {
            var data = $('.form-delete').serialize();
            var id = $('.delete-item').val();
            var url = '<?= $this->Url->build(['controller' => 'InvoiceDeliveryMethods', 'action' => 'delete']) ?>';
            var itemRow = 'invoice-' + id;
            removeItem(url, data, itemRow);
        });

        function getRefreshDropdown(url, className) {
            $.ajax({
                url: url,
                type: 'GET',
                dataType: 'HTML',
                beforeSend: function() {
                    $.LoadingOverlay('show');
                },
                success: function(response) {
                    $.LoadingOverlay('hide');
                    if (response) {
                        $('.' + className).empty().html(response);
                    }
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    $.LoadingOverlay('hide');
                    if (errorThrown === 'Forbidden') {
                        if (confirm('<?= __('TXT_SESSION_TIMEOUT') ?>')) {
                            location.reload();
                        }
                    }
                }
            });
        }

        //////////// GET PAYMENT REFRESH DROPDOWN ////////////////
        $('body').on('click', '.btn-refresh-payment', function () {
            var url = '<?= $this->Url->build(['controller' => 'PaymentCategories', 'action' => 'getDropdown']) ?>';
            getRefreshDropdown(url, 'field-payment');
        });

        //////////// GET INVOICE METHOD REFRESH DROPDOWN ////////////////
        $('body').on('click', '.btn-refresh-invoice', function () {
            var url = '<?= $this->Url->build(['controller' => 'InvoiceDeliveryMethods', 'action' => 'getDropdown']) ?>';
            getRefreshDropdown(url, 'field-invoice');
        });

        // change dial code of country
        $('body').on('click', '.country', function() {
            changeCountryCode($(this));
        });

        //update country code
        // updateContryCode('<?= $action ?>', '<?= TYPE_MEDICAL_CORP ?>');

        $('body').on('click', '.btn-phone-add', function() {
            addPhone('phone', '<?= TYPE_MEDICAL_CORP ?>', '<?= __('STR_ENTER_PHONE') ?>');
        });

        $('body').on('click', '.btn-remove-phone', function() {
            $(this).parents('.form-group').remove();
            updatePhoneIndex('phone', $(this));
        });

        $('body').on('input', '.multiple-phone', function() {
            var $this = $(this);
            removeLeadingZero('phone', $this);
        });

        $('#btn-register').on('click', function() {
            $('.help-block').empty().hide();
            var data = $('.form-medical-corp').serializeArray();
            $.ajax({
                url: $('.form-medical-corp').attr('action'),
                type: 'POST',
                dataType: 'JSON',
                data: data,
                cache: false,
                beforeSend: function() {
                    $.LoadingOverlay('show');
                },
                success: function(response) {
                    $.LoadingOverlay('hide');
                    if (response.status === 1) {
                        window.location.href = '<?php echo $this->Url->build('/medical-corporations/index/'); ?>';
                    } else if (response.status === 0) {
                        $.each(response.message, function(key, elem) {
                            var validate = $('#error-' + key);
                            $.each(elem, function(k, v) {
                                validate.removeClass('help-tips')
                                    .addClass('error-tips')
                                    .text(v)
                                    .show();
                            });
                        });
                    }
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    $.LoadingOverlay('hide');
                    if (errorThrown === 'Forbidden') {
                        if (confirm('<?= __('Session timeout. Please login again.') ?>')) {
                            location.reload();
                        }
                    }
                }
            });
        });
    })();
</script>
<?php
    echo $this->Html->css([
        'intlTelInput',
    ]);
    echo $this->Html->script([
        'intlTelInput.min',
        'utils',
    ], ['block' => 'script']);
