
<?php
$local = $this->request->session()->read('tb_field');
$prd_detail = [];
if (isset($data->id)) {
    $dt = $this->Comment->brandAndProducts($product_detail, $local);
    $dt .= $this->Comment->sizeAndUnit($product_detail, $local);
    $prd_detail = [
        $product_detail->id => $dt,
    ];
}
$action = $this->request->action; ?>
<div class="row">
    <div class="col-sm-10 col-sm-offset-1">
        <section id="register_form">
    <?php
    echo $this->Form->create($data, [
        'role' => 'form',
        'class' => 'form-horizontal form-wrap',
        'name' => 'w_standard_price_form',
        'onsubmit' => 'return false;',
    ]);
    $this->Form->templates([
        'inputContainer' => '<div class="col-lg-4 col-sm-4 col-md-4 custom-select">{{content}}</div>',
    ]);
    $price_list = [];
    $currency_list = [];

    if (isset($data1)) {
        if ($data1) {
            $i = 0;
            foreach ($data1 as $key => $value) {
                $price_list[$i] = $value->standard_price;
                $currency_list[$i] = $value->currency_id;
            }
        }
    }
    $p_list = '';
    if ($price_list) {
        $p_list = implode(',', $price_list);
    }
    $c_list = '';
    if ($currency_list) {
        $c_list = implode(',', $currency_list);
    }
    echo $this->Form->hidden('std_price', ['value' => $p_list]);
    echo $this->Form->hidden('currency_list', ['value' => $c_list]);
    ?>
    <div class="form-group">
        <label class="col-sm-2 control-label">
            <?php echo __d('w_standard_price', 'TXT_MANUFACTURER') . $this->Comment->formAsterisk() ?>
        </label>
        <?php
        $manuf = [];
        if ($manufacturers_list) {
            foreach ($manufacturers_list as $m) {
                $manuf[$m->id] = $this->Comment->nameEnOrJp($m->name, $m->name_en, $en);
            }
        }
        echo $this->Form->input('manufacturer_id', [
            'type' => 'select',
            'class' => 'form-control',
            'label' => false,
            'required' => false,
            'options' => $manuf,
            'empty' => ['' => __d('w_standard_price', 'TXT_SELECT_MANUFACTURER')],
            'default' => isset($data->id) ? $data->product_detail->product->product_brand->manufacturer->id : ''
        ]);
        ?>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label">
            <?php echo __d('w_standard_price', 'TXT_PRODUCT_BRAND') ?><?= $this->Comment->formAsterisk() ?>
        </label>
        <?php
        echo $this->Form->input('product_brand_id', [
            'type' => 'select',
            'class' => 'form-control',
            'label' => false,
            'required' => false,
            'options' => isset($data->id) ? $brands : [],
            'empty' => ['' => __d('w_standard_price', 'TXT_SELECT_PRODUCT_BRAND')],
            'default' => isset($data->id) ? $data->product_detail->product->product_brand->id : ''
        ]); ?>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label">
            <?php echo __d('w_standard_price', 'TXT_PRODUCT') ?><?= $this->Comment->formAsterisk() ?>
        </label>
        <?php
        echo $this->Form->input('product_id', [
            'type' => 'select',
            'class' => 'form-control',
            'label' => false,
            'required' => false,
            'options' => isset($data->id) ? $products : [],
            'empty' => ['' => __d('w_standard_price', 'TXT_SELECT_PRODUCT')],
            'default' => isset($data->id) ? $data->product_detail->product->id : ''
        ]); ?>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label">
            <?php echo __('TXT_PRODUCT_DETAIL') . $this->Comment->formAsterisk(); ?>
        </label>
        <?php
        echo $this->Form->input('product_detail_id', [
            'type' => 'select',
            'class' => 'form-control',
            'label' => false,
            'required' => false,
            'options' => isset($data->id) ? $prd_detail : ['' => ''],
            'empty' => ['' => __('TXT_SELECT_PRODUCT_DETAIL')]
        ]); ?>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label">
            <?php echo __d('w_standard_price', 'TXT_STANDARD_UNIT_PRICE') . $this->Comment->formAsterisk() ?>
        </label>
        <?php
        echo $this->Form->input('currency_id[0]', [
            'type' => 'select',
            'class' => 'form-control',
            'label' => false,
            'required' => false,
            'options' => $currencies,
            'empty' => ['' =>  __d('w_standard_price', 'TXT_SELECT_CURRENCY')]
        ]); ?>
        <?php
        echo $this->Form->input('standard_price[0]', [
            'type' => 'number',
            'class' => 'form-control',
            'label' => false,
            'required' => false,
            'value' => '',
            'placeholder' => __d('w_standard_price', 'TXT_ENTER_STANDARD_UNIT_PRICE'),
        ]); ?>
        <?php
        echo $this->Form->input('<i class="fa fa-plus" aria-hidden="true"></i>', [
            'type' => 'button',
            'class' => 'btn btn-sm btn-primary std-price-add form-btn-add',
            'escape' => false,
            'label' => false,
            'templates' => [
                'inputContainer' => '{{content}}',
            ]
        ]); ?>
    </div>
    <div class="std-price-wrap"></div>
    <div class="form-group">
        <?= $this->ActionButtons->btnSaveCancel('WStandardPrices', $action); ?>
    </div>
    <?php echo $this->Form->end(); ?>
</section>
    </div>
</div>
<script>
    (function() {
        get_list();

        $('#manufacturer-id, #product-brand-id, #product-id, #currency-id-0, #product-detail-id').select2();

        $('body').on('click', '#btn-register', function(e) {
            $.LoadingOverlay('show');
            var form = $('body').find('form[name="w_standard_price_form"]');
            var data = $(form).serialize();
            var url = $(form).attr('action');

            $.post(url, data, function(data) {
                $('body').find('.error-message').remove();
                if (data.message === '<?php echo MSG_ERROR; ?>') {
                    $.each(data.data, function(i, v) {
                        var message = '<label class="error-message">' + v + '</label>';
                        var content = $(form).find('select[name="' + i + '"]').closest('div');

                        if (i === 'standard_price') {
                            $.each(data.data.standard_price, function(i1, v1) {
                                var message1 = '<label class="error-message">' + v1 + '</label>';
                                var content1 = $(form).find('input[name="standard_price['+ i1 +']"]').closest('div');
                                $(content1).append(message1);
                            });
                        }

                        if (i === 'currency_id') {
                            $.each(data.data.currency_id, function(i2, v2) {
                                var message2 = '<label class="error-message">' + v2 + '</label>';
                                var content2 = $(form).find('select[name="currency_id['+ i2 +']"]').closest('div');
                                $(content2).append(message2);
                            });
                        }

                        $(content).append(message);
                    });
                } else {
                    location.href = '<?php echo $this->Url->build('/w-standard-prices/index/'); ?>';
                }
            }, 'json').done(function(data) {
                $.LoadingOverlay('hide');
            });
        });

        $('body').on('click', '.std-price-add', function(e) {
            var content = $('body').find('.std-price-wrap');
            var index = $(content).find('.form-group').length;
            var options = $('#currency-id-0').html();
            var element = '<div class="form-group">' +
                          '<label class="col-sm-2 control-label"></label>' +
                          '<div class="col-lg-4 col-sm-4 col-md-4 custom-select"><select class="form-control" id="currency-id-'+(index+1)+'" name="currency_id['+(index+1)+']">' + options + '</select></div>' +
                          '<div class="col-lg-4 col-sm-4 col-md-4"><input type="number" name="standard_price['+(index+1)+']" class="form-control" placeholder="<?= __('TXT_ENTER_STANDARD_PRICE') ?>" value=""></div>' +
                          '<button class=" btn btn-delete btn-sm form-btn-add"><i class="fa fa-trash" aria-hidden="true"></i></button>' +
                          '</div>';

            $(content).append(element);
            $('body').find('#currency-id-' + (index+1)).select2();
        });

        $('body').on('click', '.btn-delete', function(e) {
            $(this).closest('.form-group').remove();
        });

        $('body').on('change', '#manufacturer-id', function(e) {
            $.LoadingOverlay('show');
             $('#product-brand-id, #product-id, #product-detail-id').empty();
            var id = $(this).val();
            var url = '<?php echo $this->Url->build('/w-standard-prices/get-brand-by-manufacturer-id/'); ?>'

            if (id !== '') {
                $.get(url + id, function(data) {
                    add_options('#product-brand-id', data.data, data.en, '<?= __('TXT_SELECT_PRODUCT_BRAND') ?>');
                }, 'json').done(function() {
                    $.LoadingOverlay('hide');
                });
            }
        });

        $('body').on('change', '#product-brand-id', function(e) {
            $.LoadingOverlay('show');
            $('#product-id').empty();
            var id = $(this).val();
            var url = '<?php echo $this->Url->build('/w-standard-prices/get-product-by-brand-id/'); ?>'

            if (id !== '') {
                $.get(url + id, function(data) {
                    add_options('#product-id', data.data, data.en, '<?= __('TXT_SELECT_PRODUCT_NAME') ?>');
                }, 'json').done(function() {
                    $.LoadingOverlay('hide');
                });
            }
        });

        $('body').on('change', '#product-id', function(e) {
            $.LoadingOverlay('show');
            $('#product-detail-id').empty();
            var id = $(this).val();
            var url = '<?php echo $this->Url->build('/w-standard-prices/get-product-detail-by-product-id/'); ?>'

            if (id !== '') {
                $.get(url + id, function(data) {
                    var element = '<option value=""><?= __('TXT_SELECT_PRODUCT_DETAIL') ?></option>';
                    if (data) {
                        $.each (data.data, function(i, v) {
                            var name = productDetails(v, data.en);
                            element += '<option value="' + v.id + '">' + name + '</option>';
                        });
                    }
                    $('#product-detail-id').empty().append(element);
                }, 'json').done(function() {
                    $.LoadingOverlay('hide');
                });
            }
        });

        function add_options(content, data, en, defaultText)
        {
            var element = '<option value="">' + defaultText + '</option>';
            if (data !== null && data !== 'undefined') {
                $.each (data, function(i, v) {
                    element += '<option value="' + v.id + '">' + nameEnOrJp(en, v) + '</option>';
                });
            }
            $(content).empty();
            $(content).append(element);
        }

        function get_list()
        {
            if ('<?php echo $this->request->action; ?>' === 'edit') {
                var prices = $('body').find('input[type="hidden"][name="std_price"]').val();
                var currencies = $('body').find('input[type="hidden"][name="currency_list"]').val();

                if (prices !== null) {
                    prices = prices.split(',');
                }
                if (currencies) {
                    currencies = currencies.split(',');
                }
                if (currencies.length > 0 && prices.length > 0) {
                    var content = $('body').find('.std-price-wrap');
                    var index = 0;
                    var options = $('#currency-id-0').html();

                    $.each(prices, function(i, v) {
                        if (i == 0) {
                            $('body').find('#standard-price-0').val(v);
                            $('body').find('#currency-id-0').val(currencies[i]);
                        } else {
                            var element = '<div class="form-group">' +
                                      '<label class="col-sm-2 control-label"></label>' +
                                      '<div class="col-lg-4 col-sm-4 col-md-4"><input type="number" name="standard_price['+ index +']" class="form-control" placeholder="<?= __('TXT_ENTER_STANDARD_PRICE') ?>" value="'+ v +'"></div>' +
                                      '<div class="col-lg-4 col-sm-4 col-md-4 custom-select"><select class="form-control" id="currency-id-'+ index +'" name="currency_id['+ index +']">' + options + '</select></div>' +
                                      '</div>';
                            $(content).append(element);
                            $('body').find('select[name="currency_id['+ index +']"]').val(currencies[i]);
                            $('body').find('#currency-id-' + index).select2();
                        }
                        index++;
                    });
                }
            }
        }
    })();
</script>
