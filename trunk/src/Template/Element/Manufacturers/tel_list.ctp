<?php if (isset($manufacturer) && $manufacturer->info_details): ?>
    <?php foreach ($manufacturer->info_details as $key => $value):
        if ($key === 0) {
            continue;
        }
        $tel = '';
        $tel_extension = '';
        if ($value) {
            $tel = $value->tel;
            $tel_extension = $value->tel_extension;
        }
        ?>
        <div class="form-group">
            <label class="col-sm-2 control-label"></label>
            <div class="col-md-4 col-lg-4 col-sm-4">
                <?php
                echo $this->Form->tel('info_details[' . $key . '].tel', [
                    'class' => 'form-control',
                    'placeholder' => __('TXT_ENTER_PHONE'),
                    'default' => $tel
                ]);
                echo $this->Form->hidden('info_details[' . $key . '].type', [
                    'value' => TYPE_MANUFACTURER,
                ]);
                ?>
            </div>
            <div class="col-md-4 col-lg-4 col-sm-4">
                <?php
                echo $this->Form->text('info_details[' . $key . '].tel_extension', [
                    'class' => 'form-control',
                    'placeholder' => __('STR_TEL_EXT'),
                    'default' => $tel_extension
                ]);
                ?>
            </div>
            <div class="col-sm-1">
                <?php
                echo $this->Form->button('<i class="fa fa-trash" aria-hidden="true"></i>', [
                    'type' => 'button',
                    'class' => ' btn btn-delete btn-sm form-btn-add btn-remove-field',
                    'escape' => false,
                ]);
                ?>
            </div>
        </div>
    <?php endforeach; ?>
<?php endif; ?>
