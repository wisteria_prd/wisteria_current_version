
<?php
$action = $this->request->action;
?>
<div class="row">
    <div class="col-sm-10 col-sm-offset-1">
        <section id="register_form">
            <?php
            echo $this->Form->create($manufacturer, [
                'role' => 'form',
                'class' => 'form-horizontal form-manufacturer',
                'name' => 'common_form',
                'onsubmit' => 'return false;',
            ]);
            if (!$manufacturer->isNew()) {
                echo $this->Form->hidden('id');
            }
            ?>
            <div class="form-group" id="manufacturer-code">
                <label class="col-sm-2 control-label">
                    <?php
                    echo __('MANUFACTURERS_TXT_OLD_CODE'); ?>
                </label>
                <div class="col-md-4 col-lg-4 col-sm-4">
                    <?php
                    echo $this->Form->text('code', [
                        'class' => 'form-control',
                        'placeholder' => __('TXT_ENTER_OLD_CODE'),
                    ]); ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">
                    <?php
                    echo __d('manufacturer', 'TXT_COLUMN_MANUFACTURER') . $this->Comment->formAsterisk(); ?>
                </label>
                <div class="col-md-4 col-lg-4 col-sm-4">
                    <?php
                    echo $this->Form->text('name_en', [
                        'class' => 'form-control',
                        'placeholder' => __('TXT_ENTER_MANUFACTURER_NAME_EN'),
                    ]); ?>
                </div>
                <div class="col-md-4 col-lg-4 col-sm-4">
                    <?php
                    echo $this->Form->text('name', [
                        'class' => 'form-control',
                        'placeholder' => __('TXT_ENTER_MANUFACTURER_NAME'),
                    ]); ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">
                    <?php
                    echo __('MANUFACTURERS_TXT_NICKNAME'); ?>
                </label>
                <div class="col-md-4 col-lg-4 col-sm-4">
                    <?php
                    echo $this->Form->text('nickname_en', [
                        'class' => 'form-control',
                        'placeholder' => __('TXT_ENTER_MANUFACTURER_NICKNAME_EN'),
                    ]); ?>
                </div>
                <div class="col-md-4 col-lg-4 col-sm-4">
                    <?php
                    echo $this->Form->text('nickname', [
                        'class' => 'form-control',
                        'placeholder' => __('TXT_ENTER_MANUFACTURER_NICKNAME'),
                    ]); ?>
                </div>
            </div>
            <div class="form-group custom-select">
                <label class="col-sm-2 control-label">
                    <?php
                    echo __('MANUFACTURERS_TXT_ADDRESS'); ?>
                </label>
                <div class="col-lg-4 col-md-4 col-sm-4">
                    <?php
                    echo $this->Form->text('postal_code', [
                        'class' => 'form-control',
                        'placeholder' => __('STR_ENTER_POSTAL_CODE'),
                    ]); ?>
                </div>
                <div class="col-lg-4 col-sm-4 col-md-4">
                    <?php
                    echo $this->Form->select('country_id', $countries, [
                        'class' => 'form-control select-countries',
                        'empty' => ['' => __('TXT_SELECT_COUNTRY')],
                    ]); ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">&nbsp;</label>
                <div class="col-lg-4 col-sm-4 col-md-4">
                    <?php
                    echo $this->Form->text('building_en', [
                        'class' => 'form-control',
                        'placeholder' => __('TXT_ENTER_BUILDING_IN_EN'),
                    ]); ?>
                </div>
                <div class="col-lg-4 col-sm-4 col-md-4">
                    <?php
                    echo $this->Form->text('building', [
                        'class' => 'form-control',
                        'placeholder' => __('TXT_ENTER_BUILDING_IN_JP'),
                    ]); ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">&nbsp;</label>
                <div class="col-lg-4 col-sm-4 col-md-4">
                    <?php
                    echo $this->Form->text('street_en', [
                        'class' => 'form-control',
                        'placeholder' => __('TXT_ENTER_STREET_IN_EN'),
                    ]); ?>
                </div>
                <div class="col-lg-4 col-sm-4 col-md-4">
                    <?php
                    echo $this->Form->text('street', [
                        'class' => 'form-control',
                        'placeholder' => __('TXT_ENTER_STREET_IN_JP'),
                    ]); ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">&nbsp;</label>
                <div class="col-lg-4 col-sm-4 col-md-4">
                    <?php
                    echo $this->Form->text('city_en', [
                        'class' => 'form-control',
                        'placeholder' => __('TXT_CITY_EN'),
                    ]); ?>
                </div>
                <div class="col-lg-4 col-sm-4 col-md-4">
                    <?php
                    echo $this->Form->text('city', [
                        'class' => 'form-control',
                        'placeholder' => __('TXT_CITY_JP'),
                    ]); ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">&nbsp;</label>
                <div class="col-lg-4 col-sm-4 col-md-4">
                    <?php
                    echo $this->Form->text('prefecture_en', [
                        'class' => 'form-control',
                        'placeholder' => __('TXT_ENTER_PREFECTURE_IN_EN'),
                    ]); ?>
                </div>
                <div class="col-lg-4 col-sm-4 col-md-4">
                    <?php
                    echo $this->Form->text('prefecture', [
                        'class' => 'form-control',
                        'placeholder' => __('TXT_ENTER_PREFECTURE_IN_JP'),
                    ]); ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">
                    <?php
                    echo __('MANUFACTURERS_TXT_TEL'); ?>
                </label>
                <div class="col-md-4 col-lg-4 col-sm-4">
                    <?php
                    $tel = '';
                    $tel_extension = '';
                    if ($manufacturer->info_details) {
                        $tel = $manufacturer->info_details[0]->tel;
                        $tel_extension = $manufacturer->info_details[0]->tel_extension;
                    }
                    echo $this->Form->tel('info_details[0].tel', [
                        'class' => 'form-control multiple-tel',
                        'placeholder' => __('TXT_ENTER_TEL'),
                        'value' => $tel,
                    ]);
                    echo $this->Form->hidden('info_details[0].type', [
                        'value' => TYPE_MANUFACTURER,
                    ]);
                    ?>
                </div>
                <div class="col-md-4 col-lg-4 col-sm-4">
                    <?php
                    echo $this->Form->text('info_details[0].tel_extension', [
                        'class' => 'form-control',
                        'placeholder' => __('STR_TEL_EXT'),
                        'value' => $tel_extension,
                    ]); ?>
                </div>
                <div class="col-sm-1">
                    <?php
                    echo $this->Form->button('<i class="fa fa-plus" aria-hidden="true"></i>', [
                        'type' => 'button',
                        'class' => 'btn btn-sm btn-primary btn-tel-add form-btn-add',
                        'escape' => false,
                    ]); ?>
                </div>
            </div>
            <div class="tel-box-wrap">
                <?php
                echo $this->element('Manufacturers/tel_list'); ?>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">
                    <?php
                    echo __('MANUFACTURERS_TXT_FAX'); ?>
                </label>
                <div class="col-lg-4 col-sm-4 col-md-4">
                    <?php
                    echo $this->Form->text('fax', [
                        'class' => 'form-control',
                        'placeholder' =>__('TXT_ENTER_FAX'),
                    ]); ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">
                    <?php
                    echo __('MANUFACTURERS_TXT_EMAIL'); ?>
                </label>
                <div class="col-lg-3 col-md-3 col-sm-3 c-lg">
                    <?php
                    $email = '';
                    if ($manufacturer->info_details) {
                        $email = $manufacturer->info_details[0]->email;
                    }
                    echo $this->Form->email('info_details[0].email', [
                        'class' => 'form-control',
                        'default' => $email,
                        'placeholder' => __('TXT_ENTER_EMAIL'),
                    ]);
                    echo $this->Form->hidden('info_details[0].type', [
                        'value' => TYPE_MANUFACTURER,
                    ]);
                    ?>
                </div>
                <div class="col-sm-1">
                    <?php
                    echo $this->Form->button('<i class="fa fa-plus" aria-hidden="true"></i>', [
                        'type' => 'button',
                        'class' => 'btn btn-sm btn-primary btn-email-add form-btn-add',
                        'escape' => false,
                    ]); ?>
                </div>
            </div>
            <div class="email-box-wrap">
                <?php
                // display list of emails
                echo $this->element('Manufacturers/email_list'); ?>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">
                    <?php
                    echo __('MANUFACTURERS_TXT_URL'); ?>
                </label>
                <div class="col-lg-4 col-sm-4 col-md-4">
                    <?php
                    echo $this->Form->text('url', [
                        'class' => 'form-control',
                        'placeholder' => __('TXT_ENTER_URL'),
                    ]); ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">
                    <?php
                    echo __('MANUFACTURERS_TXT_SHOP_URL'); ?>
                </label>
                <div class="col-lg-4 col-sm-4 col-md-4">
                    <?php
                    echo $this->Form->text('online_shop_url', [
                        'type' => 'text',
                        'class' => 'form-control',
                        'placeholder' => __('TXT_ENTER_ONLINESHOP_URL'),
                    ]); ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">
                    <?php
                    echo __('MANUFACTURERS_TXT_DIRECT_SALES'); ?>
                </label>
                <div class="col-md-4 col-lg-4 col-sm-4">
                    <?php
                    $checked = false;
                    if (($manufacturer->seller) && ($manufacturer->seller->is_manufacture_seller)) {
                        $checked = true;
                    }
                    echo $this->Form->checkbox('is_manufacture_seller', [
                        'label' => false,
                        'required' => false,
                        'id' => 'is-seller',
                        'hiddenField' => false,
                        'checked' =>$checked,
                    ]); ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">
                    <?php
                    echo __('MANUFACTURERS_TXT_REMARKS'); ?>
                </label>
                <div class="col-lg-8 col-md-8 col-sm-8">
                    <?php
                    echo $this->Form->textarea('remark', [
                        'class' => 'form-control',
                        'placeholder' => __('TXT_ENTER_REMARK'),
                    ]); ?>
                </div>
            </div>
            <div class="form-group">
                <?php
                echo $this->ActionButtons->btnSaveCancel('Manufacturers', $action); ?>
            </div>
            <?php
            echo $this->Form->end(); ?>
        </section>
    </div>
</div>
<?php
echo $this->Html->css([
    'intlTelInput',
]);
echo $this->Html->script([
    'intlTelInput.min',
    'utils',
    'jquery.validate.min',
    'manufacturers/create',
], ['block' => 'script']);
