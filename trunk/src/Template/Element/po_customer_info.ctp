<div class="row row-top-space">
    <div class="col-md-8">
        <div class="seller-info">
            <h3>
                <?= $this->Comment->nameEnOrJp($c_name, $c_name_en, $local); ?>
            </h3>
            <div class="row invoice-form-row">
                <div class="col-md-3"><p><?= __('TXT_ADDRESS') ?></p></div>
                <div class="col-md-9">
                    <p class="c-info c-address"><?= nl2br($address) ?></p>
                </div>
            </div>
            <div class="row invoice-form-row">
                <div class="col-md-3">
                    <p><?= __('TXT_EMAIL') ?></p>
                </div>
                <div class="col-md-4">
                    <p class="c-info c-email"><?= $email ?></p>
                </div>
            </div>
            <div class="row invoice-form-row">
                <div class="col-md-3">
                    <p><?= __('TXT_TEL') ?></p>
                </div>
                <div class="col-md-9">
                    <p class="c-info c-phone"><?= $phone ?> / <?= __('TXT_FAX') ?> <?= $fax ?></p>
                </div>
            </div>
        </div>
    </div>
</div>