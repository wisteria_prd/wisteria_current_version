
<div class="col-md-3 pull-left custom-select">
    <?php
    echo $this->Form->select('displays', [
            '10' => __('DISPLAY_ITEM {0}', [10]),
            '20' => __('DISPLAY_ITEM {0}', [20]),
            '50' => __('DISPLAY_ITEM {0}', [50]),
            '100' => __('DISPLAY_ITEM {0}', [100]),
        ],
        [
            'class' => 'form-control display-data',
            'id' => false,
            'label' => false,
            'value' => $this->request->query('displays') ? $this->request->query('displays') : 10
        ]);
    ?>
</div>
