
<?php $action = $this->request->action; ?>
<div class="row">
    <div class="col-sm-10 col-sm-offset-1">
        <section id="register_form">
            <?php
            echo $this->Form->create($supplier, [
                'role' => 'form',
                'class' => 'form-horizontal',
                'name' => 'supplier_form',
            ]);
            if (!$supplier->isNew()) {
                echo $this->Form->hidden('id');
            }
            ?>
            <div class="form-group">
                <label class="col-sm-2 control-label">
                    <?php
                    echo __d('supplier', 'TXT_OLD_CODE'); ?>
                </label>
                <div class="col-md-4 col-lg-4 col-sm-4">
                    <?php
                    echo $this->Form->text('code', [
                        'class' => 'form-control',
                        'placeholder' => __d('supplier', 'TXT_SELECT_OLD_CODE'),
                    ]); ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">
                    <?php
                    echo __('TXT_SUPPLIER') . $this->Comment->formAsterisk() ?>
                </label>
                <div class="col-md-4 col-lg-4 col-sm-4">
                    <?php
                    echo $this->Form->text('name_en', [
                        'class' => 'form-control',
                        'placeholder' => __('SUPPLIER_ENTER_SUPPLIER_NAME_EN'),
                    ]); ?>
                </div>
                <div class="col-md-4 col-lg-4 col-sm-4">
                    <?php
                    echo $this->Form->text('name', [
                        'class' => 'form-control',
                        'placeholder' => __('SUPPLIER_ENTER_SUPPLIER_NAME_JP'),
                    ]); ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">
                    <?php
                    echo __('SUPPLIER_NICKNAME') ?>
                </label>
                <div class="col-md-4 col-lg-4 col-sm-4">
                    <?php
                    echo $this->Form->text('nickname_en', [
                        'class' => 'form-control',
                        'placeholder' => __('SUPPLIER_ENTER_NICKNAME_EN'),
                    ]); ?>
                </div>
                <div class="col-md-4 col-lg-4 col-sm-4">
                    <?php
                    echo $this->Form->text('nickname', [
                        'class' => 'form-control',
                        'placeholder' => __('SUPPLIER_ENTER_NICKNAME_JP'),
                    ]); ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">
                    <?php
                    echo __('SUPPLIER_ADDRESS') ?>
                </label>
                <div class="col-md-4 col-lg-4 col-sm-4">
                    <?php
                    echo $this->Form->text('postal_code', [
                        'class' => 'form-control',
                        'placeholder' => __('SUPPLIER_ENTER_POSTAL_CODE'),
                    ]); ?>
                </div>
                <div class="col-md-4 col-lg-4 col-sm-4 custom-select">
                    <?php
                    echo $this->Form->select('country_id', $countries, [
                        'class' => 'form-control',
                        'empty' => ['' => __('SUPPLIER_SELECT_COUNTRY')],
                    ]);
                    ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label"></label>
                <div class="col-md-4 col-lg-4 col-sm-4">
                    <?php
                    echo $this->Form->text('building_en', [
                        'class' => 'form-control',
                        'placeholder' => __('SUPPLIER_ENTER_BUILDING_EN'),
                    ]);
                    ?>
                </div>
                <div class="col-md-4 col-lg-4 col-sm-4">
                    <?php
                    echo $this->Form->text('building', [
                        'class' => 'form-control',
                        'placeholder' => __('SUPPLIER_ENTER_BUILDING_JP'),
                    ]);
                    ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label"></label>
                <div class="col-md-4 col-lg-4 col-sm-4">
                    <?php
                    echo $this->Form->text('street_en', [
                        'class' => 'form-control',
                        'placeholder' => __('SUPPLIER_ENTER_STREET_EN'),
                    ]);
                    ?>
                </div>
                <div class="col-md-4 col-lg-4 col-sm-4">
                    <?php
                    echo $this->Form->text('street', [
                        'class' => 'form-control',
                        'placeholder' => __('SUPPLIER_ENTER_STREET_JP'),
                    ]);
                    ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label"></label>
                <div class="col-md-4 col-lg-4 col-sm-4">
                    <?php
                    echo $this->Form->text('city_en', [
                        'class' => 'form-control',
                        'placeholder' => __('SUPPLIER_ENTER_CITY_EN'),
                    ]);
                    ?>
                </div>
                <div class="col-md-4 col-lg-4 col-sm-4">
                    <?php
                    echo $this->Form->text('city', [
                        'class' => 'form-control',
                        'placeholder' => __('SUPPLIER_ENTER_CITY_JP'),
                    ]);
                    ?>
                </div>
            </div>
            <div class="form-group custom-select">
                <label class="col-sm-2 control-label">&nbsp;</label>
                <div class="col-md-4 col-lg-4 col-sm-4">
                    <?php
                    echo $this->Form->text('prefecture_en', [
                        'class' => 'form-control',
                        'placeholder' => __('SUPPLIER_ENTER_PREFECTURE_EN'),
                    ]);
                    ?>
                </div>
                <div class="col-md-4 col-lg-4 col-sm-4">
                    <?php
                    echo $this->Form->text('prefecture', [
                        'class' => 'form-control',
                        'placeholder' => __('SUPPLIER_ENTER_PREFECTURE_JP'),
                    ]);
                    ?>
                </div>
            </div>

            <!--- sleh  -->

            <div class="form-group">
                <label class="col-sm-2 control-label">
                    <?php echo __('SUPPLIER_TEL') ?>
                </label>
                <div class="col-md-4 col-lg-4 col-sm-4">
                    <?php
                    $tel = '';
                    $tel_extension = '';
                    if (isset($supplier->info_details) && $supplier->info_details) {
                        $tel = $supplier->info_details[0]->tel;
                        $tel_extension = $supplier->info_details[0]->tel_extension;
                    }
                    echo $this->Form->tel('info_details[0].tel', [
                        'class' => 'form-control multiple-tel',
                        'placeholder' => __('SUPPLIER_ENTER_TEL'),
                        'value' => $tel,
                    ]);
                    echo $this->Form->hidden('info_details[0].type', [
                        'value' => TYPE_SUPPLIER,
                    ]);
                    ?>
                </div>
                <div class="col-md-4 col-lg-4 col-sm-4">
                    <?php
                    echo $this->Form->text('info_details[0].tel_extension', [
                        'class' => 'form-control',
                        'placeholder' => __('STR_TEL_EXT'),
                        'value' => $tel_extension,
                    ]);
                    ?>
                </div>
                <div class="col-sm-1">
                    <?php
                    echo $this->Form->button('<i class="fa fa-plus" aria-hidden="true"></i>', [
                        'type' => 'button',
                        'class' => 'btn btn-sm btn-primary btn-tel-add form-btn-add',
                        'escape' => false,
                    ]);
                    ?>
                </div>
            </div>
            <div class="tel-box-wrap">
                <?php echo $this->element('Suppliers/tel_list'); ?>
            </div>

            <!-- lsdfslj -->
            <div class="form-group">
                <label class="col-sm-2 control-label">
                    <?php echo __('SUPPLIER_FAX') ?>
                </label>
                <div class="col-md-4 col-lg-4 col-sm-4">
                    <?php
                    echo $this->Form->text('fax', [
                        'class' => 'form-control',
                        'placeholder' => __('SUPPLIER_ENTER_FAX'),
                    ]);
                    ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">
                    <?php echo __('SUPPLIER_EMAIL') ?>
                </label>
                <div class="col-md-4 col-lg-4 col-sm-4">
                    <div class="col-sm-10 no-p-l">
                        <?php
                        $email = '';
                        if (isset($supplier->info_details) && $supplier->info_details) {
                            $email = $supplier->info_details[0]->email;
                        }
                        echo $this->Form->email('info_details[0].email', [
                            'class' => 'form-control',
                            'placeholder' => __('SUPPLIER_ENTER_EMAIL'),
                            'default' => isset($supplier) ? $email : null,
                        ]);
                        echo $this->Form->hidden('info_details[0].type', [
                            'value' => TYPE_SUPPLIER,
                        ]);
                        ?>
                    </div>
                    <div class="col-sm-2">
                        <?php
                        echo $this->Form->button('<i class="fa fa-plus" aria-hidden="true"></i>', [
                            'type' => 'button',
                            'class' => 'btn btn-sm btn-primary btn-email form-btn-add',
                            'escape' => false,
                        ]);
                        ?>
                    </div>
                </div>
            </div>
            <div class="email-box-wrap">
                <?php echo $this->element('Suppliers/email_list') ?>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">
                    <?php echo __('SUPPLIER_URL') ?>
                </label>
                <div class="col-md-4 col-lg-4 col-sm-4">
                    <?php
                    echo $this->Form->text('url', [
                        'class' => 'form-control',
                        'placeholder' => __('SUPPLIER_ENTER_URL'),
                    ]);
                    ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">
                    <?php echo __('SUPPLIER_SHOP_URL') ?>
                </label>
                <div class="col-md-4 col-lg-4 col-sm-4">
                    <?php
                    echo $this->Form->text('online_shop_url', [
                        'class' => 'form-control',
                        'placeholder' => __('SUPPLIER_ENTER_ONLINE_URL'),
                    ]);
                    ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">
                    <?php echo __('SUPPLIER_REMARK') ?>
                </label>
                <div class="col-md-4 col-lg-4 col-sm-4 textarea-wrap">
                    <?php
                    echo $this->Form->textarea('remark', [
                        'class' => 'form-control',
                        'placeholder' => __('SUPPLIER_ENTER_REMARK'),
                    ]);
                    ?>
                </div>
            </div>
            <div class="form-group">
                <?php echo $this->ActionButtons->btnSaveCancel('Suppliers', $action); ?>
            </div>
            <?php echo $this->Form->end(); ?>
        </section>
    </div>
</div>
<?php
echo $this->Html->css([
    'intlTelInput',
]);
echo $this->Html->script([
    'intlTelInput.min',
    'utils',
    'suppliers/suppliers',
], ['blog' => false]);
