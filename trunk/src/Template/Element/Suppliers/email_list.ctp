<?php if (isset($supplier) && $supplier->info_details): ?>
    <?php foreach ($supplier->info_details as $key => $value): ?>
        <?php if (($value->email !== null) && !empty($value->email) && ($key != 0)): ?>
            <div class="form-group">
                <label class="col-sm-2 control-label"></label>
                <div class="col-md-4 col-lg-4 col-sm-4">
                    <div class="col-sm-10 no-p-l">
                        <?php
                        echo $this->Form->email('info_details[' . $key . '].email', [
                            'class' => 'form-control',
                            'placeholder' => __('SUPPLIER_ENTER_EMAIL'),
                            'default' => $value->email
                        ]);
                        echo $this->Form->hidden('info_details[' . $key . '].type', [
                            'value' => TYPE_SUPPLIER,
                        ]);
                        ?>
                    </div>
                    <div class="col-sm-2">
                        <?php
                        echo $this->Form->button('<i class="fa fa-trash" aria-hidden="true"></i>', [
                            'type' => 'button',
                            'class' => ' btn btn-delete btn-sm form-btn-add btn-remove-field',
                            'escape' => false,
                        ]); ?>
                    </div>
                </div>
            </div>
        <?php endif; ?>
    <?php endforeach; ?>
<?php endif; ?>
