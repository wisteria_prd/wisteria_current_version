<?php
if (isset($seller_brands)) :
    $index = 0;
    foreach ($seller_brands as $key => $value): ?>
        <div class="checkbox">
            <label>
                <?php
                echo $this->Form->checkbox('product_brand_id[' . $index . ']', [
                    'hiddenField' => false,
                    'class' => 'chk-brands',
                    'value' => $value->product_brand_id,
                    'checked' => true,
                    'disabled' => ($data->id == $value->id) ? true : false,
                ]);
                echo $this->Comment->getFieldByLocal($value->product_brand, $locale);
                $index++;
                ?>
            </label>
        </div>
        <?php
    endforeach;
endif; ?>