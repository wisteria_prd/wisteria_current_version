
<style>
.custom-wrap {
    padding: 0px;
    margin: 0px;
}

.custom-wrap li {
    list-style: none;
    margin-bottom: 6px;
}

.custom-wrap li span {
    font-size: inherit;
    font-weight: initial;
    line-height: inherit;
}

.custom-wrap li span:first-child {
    float: left;
    cursor: pointer;
}

.custom-wrap li span:last-child {
    float: right;
}

.custom-wrap li::after {
    content: '';
    display: block;
    clear: both;
}

.custom-wrap li .add-pdf-file,
.custom-wrap li .rm-pdf-file {
    cursor: pointer;
}
</style>

<?php
$options = [];
$currentStatus = '';
$step = 0;
$buttonName = __('TXT_SEND_PO');

echo $this->Form->create(null, [
    'class' => 'form-horizontal',
    'onsubmit' => 'return false;',
    'name' => 'sale_status',
]);

switch ($data->status) {
    case PO_STATUS_REQUESTED_SHIPPING :
        $step = 2;
        $currentStatus = __('TXT_REQUESTED_SHIPMENT');
        $buttonName = __('TXT_SUBMIT');
        $options = [
            PO_STATUS_REQUESTED_CC => __('TXT_DELIVERING'),
            STATUS_CANCEL => __('BTN_CANCEL'),
        ];
        break;
    case PO_STATUS_REQUESTED_CC :
        $step = 3;
        $currentStatus = __('TXT_DELIVERING');
        $buttonName = __('TXT_SUBMIT');
        $options = [
            PO_STATUS_DELIVERED => __('TXT_DELIVERED'),
        ];
        break;
    case PO_STATUS_DELIVERED :
        $step = 4;
        $currentStatus = __('TXT_DELIVERED');
        $buttonName = __('TXT_SUBMIT');
        $options = [
            STATUS_COMPLETED => __('TXT_COMPLETE'),
        ];
        break;
    case STATUS_COMPLETED :
        $currentStatus = __('TXT_COMPLETE');
        $step = 5;
        break;
    default :
        $step = 1;
        $currentStatus = __('TXT_DRAFT');
        $options = [
            PO_STATUS_REQUESTED_SHIPPING => __('TXT_REQUESTED_SHIPMENT'),
            STATUS_CANCEL => __('BTN_CANCEL'),
        ];
        echo $this->Form->hidden('issue_date', [
            'value' => date('Y-m-d'),
        ]);
}

$this->Form->templates([
    'inputContainer' => '<div class="col-sm-10">{{content}}</div>',
]);
echo $this->Form->hidden('id', ['value' => $data->id]);
echo $this->Form->hidden('step', ['value' => $step]);
echo $this->Form->hidden('action', ['value' => $this->request->action]);
echo $this->Form->hidden('po_type', ['value' => 'normal']);
?>
<div class="form-group" style="margin-bottom: 10px;">
    <label class="col-sm-5 control-label text-left" style="padding-top: 0px;"><?= __('TXT_CURRENT_STATUS') ?></label>
</div>
<div class="form-group">
    <div class="col-sm-4">
        <label class="control-label status label-draft" style="width: 100%; text-align: center !important; padding: 7px 0px;">
            <?= $currentStatus ?>
        </label>
    </div>
    <?php
    if ($data->status !== STATUS_COMPLETED) {
        echo '<div class="col-sm-2" style="position: relative;"><span class="glyphicon glyphicon-arrow-right arrow-draft"></span></div>';
        echo $this->Form->input('status', [
            'class' => 'form-control',
            'label' => false,
            'disabled' => ($data->status === STATUS_CANCEL) ? true : false,
            'options' => $options,
            'templates' => [
                'inputContainer' => '<div class="col-sm-6">{{content}}</div>',
            ]
        ]);
    } else {
        echo '<span>After 7days change to ‘completed’ auto</span>';
    }
    ?>
</div>
<div class="status-wrap">
    <?php
    switch ($step) {
        case 2 :
            // Delivery Form
            $import = null;
            $importId = -1;

            if ($data->purchase_imports) {
                $import = $data->purchase_imports[0]->import;
                $importId = $import->id;
            }

            echo $this->Form->create(null, [
                'class' => 'form-horizontal',
                'role' => 'form',
                'name' => 'delivery_form',
            ]);
            $this->Form->templates([
                'inputContainer' => '<div class="col-sm-1"></div><div class="col-sm-6">{{content}}</div>',
            ]);
            echo $this->Form->hidden('import_id', [
                'value' => $importId,
                'id' => 'import-id',
            ]);
            echo '<div class="form-group">' .
                '<label class="col-sm-5 control-label text-left">' . __('TXT_TRACKING_NUMBER') . '</label>';
            echo $this->Form->input('tracking', [
                'label' => false,
                'required' => false,
                'class' => 'form-control',
                'placeholder' => __('TXT_TRACKING_NUMBER'),
                'value' => $import ? $import->tracking : null,
            ]);
            echo '</div>';

            echo '<div class="form-group">' .
                '<label class="col-sm-5 control-label text-left">' . __('TXT_DELIVERY_DATE') . '</label>';
            echo $this->Form->input('delivery_date', [
                'label' => false,
                'required' => false,
                'class' => 'form-control',
                'placeholder' => __('TXT_DELIVERY_DATE'),
                'value' => $import ? date('Y-m-d', strtotime($import->delivery_date)) : date('Y-m-d'),
            ]);
            echo '</div>';

            echo '<div class="form-group"><div class="col-sm-12 text-right">';
            echo $this->Form->button(__('TXT_UPDATE'), [
                'type' => 'button',
                'class' => 'btn btn-sm btn-primary btn-update-delivery',
            ]);
            echo '</div></div>';

            // list pdf file
            $element = '';
            $fileName = '';
            $docType = '';
            $className = 'browse-file';
            $type = 'new';
            $id = 0;

            if ($medias) {
                $className = 'browse-file-edit';
                $type = 'edit';

                foreach ($medias as $key => $value) {
                    if ($key != 0) {
                        $element .= '<li data-id="' . $value->id . '" data-name="' . $value->file_name . '" data-document-type="' . $value->document_type . '" data-type="' . $type . '"><span class="label label-success ' . $className . '"><i class="fa fa-times rm-pdf-file" aria-hidden="true"></i>&nbsp;&nbsp;Browse File</span><span>&nbsp;</span></li>';
                    } else {
                        $fileName = $value->file_name;
                        $id = $value->id;
                        $docType = $value->document_type;
                    }
                }
            }

            echo '<div class="form-group">' .
            '<label class="col-sm-6 control-label text-left">' . __('TXT_CUSTOM_CLEARANCE') . '</label>' .
            '<div class="col-sm-6 col-md-6"><ul class="custom-wrap">' .
            '<li data-document-type="' . $docType . '" data-name="' . $fileName . '" data-id="' . $id . '" data-type="' . $type . '"><span class="label label-success ' . $className . '">&nbsp;&nbsp;Browse File</span><span class="label label-primary add-pdf-file">Add</span></li>' .
            $element . '</ul></div></div>';
            break;
    }
    ?>
</div>
<div class="form-group">
    <div class="col-sm-6">
        <?php
        if (($step == 3)) {
            $li = '';
            if ($data->purchase_imports) {
                $import = $data->purchase_imports[0]->import->id;
                $li = '&nbsp&nbsp<span class="label label-success" style="font-size: small;">' . $import;
            }
            echo __('TXT_IMPORT_INFO') . $li;
        }
        if (($step == 4)) {
            $li = '';
            if ($data->purchase_payments) {
                $payment = $data->purchase_payments[0]->payment->id;
                $li = '&nbsp&nbsp<span class="label label-success" style="font-size: small;">' . $payment;
            }
            echo __('TXT_PAYMENT_INFO') . $li;
        }
        ?>
    </div>
    <div class="col-sm-6">
        <?php
        $disabled = false;
        if ((($data->status === PO_STATUS_REQUESTED_CC) && ((count($data->purchase_imports) == 0))) ||
            (($data->status === PO_STATUS_DELIVERED) && ((count($data->purchase_payments) == 0))) ||
            ($data->status === STATUS_CANCEL)) {
            $disabled = true;
        }
        if ($step != 5) {
            echo $this->Form->button($buttonName, [
                'type' => 'button',
                'disabled' => $disabled,
                'class' => 'btn btn-primary btn-sm btn-register pull-right',
                'data-step' => $step,
            ]);
        }
        ?>
    </div>
</div>
<?php echo $this->Form->end(); ?>