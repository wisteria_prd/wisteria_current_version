<div class="row row-top-space">
    <div class="col-md-8">
        <div class="doctor-info">
            <div class="row invoice-form-row">
                <div class="col-md-3">
                    <p><?= __('TXT_CONTACT') ?></p>
                </div>
                <div class="col-md-4 custom-select">
                    <p class="c-info c-doctor-name"><?= $doctor_name ?></p>
                </div>
            </div>
        </div>
    </div>
</div>