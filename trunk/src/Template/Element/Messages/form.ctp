<style>
    .message-receivers-id .select2-container {
        width: 100% !important;
    }
</style>

<div class="row">
    <div class="col-md-8 col-md-offset-2">
        <?php
        echo $this->Form->create($message, [
            'class' => 'form-horizontal',
            'autocomplete' => 'off',
            'type' => 'file',
        ]);
        echo $this->Form->hidden('id');
        echo $this->Form->hidden('external_id', [
            'value' => $this->request->query('external_id'),
        ]);
        echo $this->Form->hidden('type', [
            'value' => $this->request->query('type'),
        ]);
        $this->Form->templates([
            'inputContainer' => '{{content}}',
            'inputContainerError' => '{{content}}{{error}}',
        ]);
        ?>
        <div class="form-group">
            <label class="col-sm-2">
                <?php echo __('COMMENT_TXT_STATUS') ?>
            </label>
            <div class="col-sm-8 custom-select">
                <?php
                $status = array_map('__', unserialize(MESSAGE_STATUS_TYPE));
                unset($status[0]);
                if ($this->request->action === 'register') {
                  unset($status[3]);
                }
                echo $this->Form->select('status', $status, [
                    'label' => false,
                    'required' => false,
                    'class' => 'form-control',
                ]);
                ?>
            </div>
        </div>
        <div class="form-group">
            <label for="user_id" class="col-sm-2">
                <?php echo __('TXT_IN_CHARGE') ?>
            </label>
            <div class="col-sm-8">
                <?php
                echo $this->Form->input('is_sent_to_all', [
                    'type' => 'checkbox',
                    'label' => ' To all',
                    'class' => 'is_sent_to_all',
                ]); ?>
                <div class="message-receivers-id">
                    <?php
                    echo $this->Form->select('message_receivers', $users, [
                        'multiple' => 'multiple',
                        'label' => false,
                        'required' => false,
                        'class' => 'form-control receiver_id',
                        'empty' => ['' => __('COMMENT_TXT_SELECT_USER')]
                    ]); ?>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label for="title" class="col-sm-2">
                <?php echo __('COMMENT_TXT_SUBJECT') ?>
            </label>
            <div class="col-sm-8">
                <?php
                echo $this->Form->text('title', [
                    'label' => false,
                    'required' => false,
                    'class' => 'form-control',
                    'placeholder' => __('COMMENT_TXT_ENTER_SUBJECT'),
                ]);
                ?>
            </div>
        </div>
        <div class="form-group custom-select">
            <label for="message_category_id" class="col-sm-2">
                <?php echo __('STR_COMMENTS_CATEGORY') ?>
            </label>
            <div class="col-sm-8">
                <?php
                echo $this->Form->select('message_category_id', $categories, [
                    'id' => 'message_category_id',
                    'class' => 'form-control',
                    'required' => false,
                    'empty' => ['' => __('COMMENT_TXT_SELECT_CATEGORY')]
                ]);
                ?>
            </div>
            <div class="col-sm-1">
                <?php
                echo $this->Form->button('<span class="glyphicon glyphicon-plus"></span>', [
                    'type' => 'button',
                    'escape' => false,
                    'class' => 'btn btn-primary btn-sm btn-add-category'
                ]);
                ?>
            </div>
        </div>
        <div class="form-group">
            <label for="description" class="col-sm-2">
                <?php echo __('COMMENT_TXT_COMMENT') ?>
            </label>
            <div class="col-sm-8">
                <?php
                echo $this->Form->textarea('description', [
                    'label' => false,
                    'required' => false,
                    'class' => 'form-control',
                    'placeholder' => __('COMMENT_TXT_ENTER_COMMENT'),
                ]);
                ?>
            </div>
        </div>
        <div class="form-group">
            <label for="image" class="col-sm-2">
                <?php echo __('COMMENT_TXT_FILE_UPLOAD') ?>
            </label>
            <div class="col-sm-10 file-wrap">
                <?php echo $this->Form->file('file', array(
                    'style' => 'display:none',
                    'id' => 'btn-comment-image',
                    'accept' => 'image/*, .pdf, .docx',
                )) ?>
                <button type="button" style="width: 120px;height: 25px;" onclick="document.getElementById('btn-comment-image').click()">
                    <?php echo __('COMMENT_TXT_CHOOSE_UPLOAD'); ?>
                </button>
              <span class="text-right comment-no-file-selected">
                    <?php
                    if ($message->file) {
                        echo $message->file;
                    } else {
                        echo __('COMMENT_TXT_NO_FILE_SELECTED');
                     } ?>
                 </span>
                <div class="img-preview" style="display: <?php echo empty($message->image) ? 'none' : 'block'?>;">
                    <?php
                    if (empty($message->image)) {
                        echo '<img id="imgPreview" src="#"/>';
                    } else {
                        echo $this->Html->image('/img/uploads/messages/' . $message->image, [
                            'id' => 'imgPreview',
                        ]);
                    }
                    ?>
                </div>
            </div>
        </div>
        <div class="form-group">
            <div class="col-lg-12 col-sm-12 col-md-12 text-center">
                <?php
                echo $this->Form->button(__('TXT_CANCEL'), [
                    'type' => 'button',
                    'class' => 'btn btn-default btn-sm btn-cancel btn-width',
                    'onclick' => 'window.history.back(1);',
                ]);
                echo '&nbsp;&nbsp;';
                echo $this->Form->button($this->request->action === 'register' ? __('TXT_REGISTER') : __('TXT_UPDATE'), [
                    'class' => 'btn btn-primary btn-sm btn-width',
                ]);
                ?>
            </div>
        </div>
        <?php echo $this->Form->end() ?>
    </div>
</div>
<?php echo $this->element('Modal/Message/category'); ?>
<?php
if ($selected_receivers) {
    $selected_receivers = Cake\Utility\Hash::extract($this->request->data('message_receivers'), '{n}.user_id');
    //$selected_receivers = implode(',', $selected_receivers);
}
if ($this->request->data('is_sent_to_all')) {
    $selected_receivers = [];
}
echo $this->Html->script([
    'jquery.validate.min',
], [['block' => 'script']]);
?>

<script>
    var COMMENT_TXT_SELECT_USER = '<?php echo __('COMMENT_TXT_SELECT_USER'); ?>';
    var selected_receivers = <?php echo json_encode($selected_receivers); ?>;
    (function(e) {
        // UPLOAD IMAGE
        $('#btn-comment-image').on('change', function() {
            $('.comment-no-file-selected').text($(this).val());
        });

        // OPEN MODAL
        $('body').on('click', '.btn-add-category', function (e) {
           $.LoadingOverlay('show');
           getCategoryList();
           $('.modal-message-category-list').modal('show');
           formCategory();
        });

        // EDIT CATEGOEY
        $('body').on('click', '.btn-edit-message-category', function(e) {
            var tr = $(this).closest('tr');
            var form = $('.modal-message-category-list').find('.form-modal-message-category-list');
            var param = {
                id: $(tr).attr('data-id'),
                name: $(tr).attr('data-name'),
                name_en: $(tr).attr('data-name-en'),
            };

            $(form).find('input[name="name"]').val(param.name);
            $(form).find('input[name="name_en"]').val(param.name_en);
            $('.modal').find('.btn-add-new-category').attr('data-id', param.id);
            $('.modal').find('.btn-add-new-category').attr('data-target', 'edit');
            $('.modal').find('.btn-add-new-category').text('<?= __('TXT_UPDATE') ?>');
        });

        // Delete
        $('body').on('click', '.modal-message-category-list .btn-confirm-delete-message-category', function(e) {
            var id = $(this).closest('tr').attr('data-id');
            $.post('<?php echo $this->Url->build('/MessageCategories/delete/'); ?>' + id, function(data) {
                if (data != null && data != undefined) {
                    if (data.message === 'success') {
                        $.LoadingOverlay('show');
                        getCategoryList();
                    } else {
                        alert('Warning ! This record use another place!');
                    }
                }
            }, 'json');
        });

        function getCategoryList()
        {
            $.get(BASE + 'MessageCategories/get-list', function(data) {
                if (data != null && data != undefined) {
                    var content = $('body').find('.modal-message-category-list');
                    $(content).find('.table tbody tr').remove();
                    var element = '';
                    var response = data.data;

                    $.each(response, function(i, v) {
                        var SYSTEM_TYPE = '<?php echo $this->request->session()->read('user_group.affiliation_class'); ?>';
                        if (SYSTEM_TYPE === '<?php echo USER_CLASS_WISTERIA; ?>') {
                            var name = (v.name) ? v.name : v.name_en;
                        } else {
                            var name = (v.name_en) ? v.name_en : v.name;
                        }

                        element += '<tr data-id="' + v.id + '" data-name="' + v.name + '" data-name-en="' + v.name_en + '">'
                                + '<td>' + (i+1) + '</td>'
                                + '<td>' + name + '</td>'
                                + '<td class="text-right">'+ renderEdit() +'</td>'
                                + '<td class="text-right">'+ renderDelete() +'</td>'
                                + '</tr>';
                    });
                    $(content).find('.table tbody').append(element);
                    $.LoadingOverlay('hide');
                }
            }).done(function(data) {
                pop_delete('btn-delete-message-category');
            });
        }

        function formCategory()
        {
            $('form[name="message-category"]').validate({
                rules: {
                    name: 'required',
                    name_en: 'required'
                },
                messages: {
                    name: MSG_REQUIRED ,
                    name_en: MSG_REQUIRED
                },
                errorClass: 'error-message',
                submitHandler: function(form) {
                    $.LoadingOverlay('show');
                    var target = $(form).find('button').attr('data-target');
                    var url = BASE + 'MessageCategories/create';
                    if (target === 'edit') {
                        url =  BASE + 'MessageCategories/edit/' + $(form).find('button').attr('data-id');
                    }
                    $.post(url, $(form).serialize(), function(data) {
                        $('.form-modal-message-category-list').find('input[type="text"]').val('');
                        getCategoryList();
                        $('.form-modal-message-category-list').find('.btn-add-new-category').text('<?php echo __d('message_category', 'REGISTER'); ?>').attr('data-target', 'new').removeAttr('data-id');
                    });
                    return false;
                }
            });
        }

        function pop_delete()
        {
            var template = '<p><?php echo __('MSG_CONFIRM_DELETE'); ?></p><div class="text-center">'
                        + '<button class="btn btn-default confirm-no" type="button"><?php echo __('BTN_CANCEL'); ?></button>'
                        + '<button class="btn btn-default confirm-yes btn-confirm-delete-message-category" type="button"><?php echo __('BTN_DELETE'); ?></button></div>';
            $('body').find('.' + 'btn-delete-message-category').popover({
                placement: 'left',
                html: 'true',
                content: template,
                toggle: 'popover',
                trigger: 'focus'
            });
        }

        function renderEdit()
        {
            var html = '';
             html += '<button type="button" class="btn btn-primary btn-sm btn-edit-message-category">'+'<?php echo BTN_ICON_EDIT; ?>'+'</button>'
            return html;
        }

        function renderDelete()
        {
            var html = '';
            html += '<button type="button" data-toggle="popover" class="btn btn-danger btn-sm btn-delete-message-category">'+'<?php echo BTN_ICON_DELETE; ?>'+'</button>';
            return html;
        }

    })();
</script>
