<?php
$replies = $replies->nest('id', 'parent_id')->toList();
?>

<?php if ($replies): ?>
<div class="row row-top-space">
    <div class="col-md-7">
        <table class="table table-borderless">
            <?php foreach ($replies as $reply): ?>
            <tr>
                <td width="150px;">
                    <p><?php echo h($reply->user->lastname . ' ' . $reply->user->firstname) ?></p>
                    <p><?php echo date('Y-m-d', strtotime($reply->created)) ?></p>
                </td>
                <td style="vertical-align: text-top;">
                    <?php echo h($reply->description) ?>
                </td>
                <td width="1%">
                    <?php
                    if ($reply->mime_type) {
                        echo $this->Html->link('<i class="fa fa-file"></i>', '/img/uploads/replies/'. $reply->file, [
                            'target' => '__blank',
                            'class' => 'btn btn-sm btn-primary',
                            'escape' => false,
                        ]);
                    } ?>
                </td>
                <td  width="1%">
                    <?php
                    if (($reply->user_id === $auth['id']) || $this->request->session()->read('user_group.role') === USER_ROLE_ADMIN): ?>
                        <?php
                        echo $this->Form->button(BTN_ICON_DELETE, [
                            'type' => 'button',
                            'class' => 'btn btn-sm btn-danger btn-primary btn-delete-comment',
                            'data-id' => $reply->id,
                        ]); ?>
                    <?php endif; ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </td>
    </table>

    </div>
</div>
<?php endif; ?>
