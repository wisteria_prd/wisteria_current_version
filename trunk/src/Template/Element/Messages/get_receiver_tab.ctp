<?php if ($data):
    $MESSAGE_STATUS_TYPE = unserialize(MESSAGE_STATUS_TYPE);
    ?>
    <?php foreach ($data as $key => $item):
        $status = isset($MESSAGE_STATUS_TYPE[$item->status]) ? $MESSAGE_STATUS_TYPE[$item->status] : $item->status;
        ?>
        <tr data-id="<?php echo $item->id ?>">
            <td><?php echo ($key + 1) ?></td>
            <td><?php echo __($status); ?>
            </td>
            <td><?php
            echo $this->Text->truncate(
                h($item->title),
                10,
                [
                'ellipsis' => '...',
                'exact' => false
                ]
            ); ?>
            </td>
            <td><?php
            echo $this->Text->truncate(
                h($item->description),
                100,
                [
                'ellipsis' => '...',
                'exact' => false
                ]
            ); ?>
            </td>
            <td><?php echo h($item->message_category->name_en) ?></td>
            <td><?php echo date('Y-m-d', strtotime($item->created)) ?></td>
            <td>
            <?php
            $totalReply = 0;
            if ($item->replies) {
                $totalReply = $item->replies[0]->total_reply;
            }
            echo $totalReply;
            ?>
            </td>
            <td>
                <?php
                echo $this->Html->link('<i class="fa fa-reply" aria-hidden="true"></i>', [
                    'controller' => 'Messages',
                    'action' => 'view',
                    '?' => [
                        'external_id' => $id,
                        'msg_id' => $item->id,
                        'type' => $type,
                    ]
                ], [
                    'class' => 'btn btn-sm btn-primary',
                    'role' => 'button',
                    'escape' => false,
                ]);
                ?>
                <?php
                if (($item->user_id == $auth['id']) ||
                        ($this->request->session()->read('user_group.role') === USER_ROLE_ADMIN)) {
                    echo $this->Html->link(BTN_ICON_EDIT, [
                        'controller' => 'Messages',
                        'action' => 'edit',
                        $item->id
                    ], [
                        'class' => 'btn btn-primary btn-sm',
                        'role' => 'button',
                        'escape' => false,
                    ]);
                    echo '&nbsp;';
                    echo $this->Form->button(BTN_ICON_DELETE, [
                        'type' => 'button',
                        'data-id' => $item->id,
                        'class' => 'btn btn-delete btn-sm btn-suspend-msg',
                        'escape' => false,
                    ]);
                }
                ?>
            </td>
        </tr>
    <?php endforeach; ?>
<?php endif; ?>
