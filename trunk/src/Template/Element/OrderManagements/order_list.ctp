
<table class="table table-striped">
    <thead>
        <tr>
            <th v-for="column in columns" class="white-space">
                {{ column | capitalize }}
                <template v-if="(column !== '') && (column !== '#')">
                    <i class="fa fa-sort-desc" aria-hidden="true"></i>
                </template>
            </th>
        </tr>
    </thead>
    <tbody>
        <tr v-for="row in data">
            <th>{{ row.row_number }}</th>
            <td v-if="(row.customer_type !== '<?= CUSTOMER_TYPE_CONTRACT ?>') && (row.total_amount < row.w_invoice_amount)">
                <?= $this->Form->button(BTN_ICON_YEN, [
                    'type' => 'button',
                    'class' => 'btn-yen btn btn-sm btn-danger',
                    'escape' => false,
                    'v-on:click' => 'link(row, $event)',
                ]) ?>
            </td>
            <td v-else-if="(row.customer_type !== '<?= CUSTOMER_TYPE_CONTRACT ?>') && (row.total_amount == row.w_invoice_amount)">
                <?= $this->Form->button(BTN_ICON_YEN, [
                    'type' => 'button',
                    'class' => 'btn-yen btn btn-sm btn-warning',
                    'escape' => false,
                    'v-on:click' => 'link(row, $event)',
                ]) ?>
            </td>
            <td v-else-if="(row.customer_type !== '<?= CUSTOMER_TYPE_CONTRACT ?>') && (row.total_amount > row.w_invoice_amount)">
                <?= $this->Form->button(BTN_ICON_YEN, [
                    'type' => 'button',
                    'class' => 'btn-yen btn btn-sm btn-primary',
                    'escape' => false,
                    'v-on:click' => 'link(row, $event)',
                ]) ?>
            </td>
            <td v-else>&nbsp;</td>
            <td>{{ row.status }}</td>
            <td style="white-space: nowrap;">{{ row.type }}</td>
            <td>{{ row.supplier }}</td>
            <td>{{ row.order_date }}</td>
            <td>{{ row.order }}</td>
            <td>{{ row.delivery_to }}</td>
            <td>{{ row.product }}</td>
            <td>{{ row.amount }}</td>
            <td style="white-space: nowrap;" v-if="(row.is_kind === true)"><input type="checkbox" disabled="disabled"/>
                {{ row.currency_formate }}
            </td>
            <td style="white-space: nowrap;" v-else>
                {{ row.currency_formate }}
            </td>
            <td v-html="row.paying" v-if="(row.customer_type === '<?= CUSTOMER_TYPE_CONTRACT ?>')">
                {{ row.paying }}
            </td>
            <td>{{ row.user }}</td>
            <td v-html="row.deliveryIcon" class="white-space">
                {{ row.delivery_icon }}
            </td>
            <td class="btn-wrap">
                <?php
                echo $this->Form->button(BTN_ICON_PDF, [
                    'type' => 'button',
                    'class' => 'btn btn-primary btn-sm',
                    'escape' => false,
                ]); ?>
                <button type="button" class="btn btn-primary btn-sm btn_comment" data-id="vue">
                    <b class="data-id" style="display:none">{{ row.id }}</b>
                    <b class="data-type" style="display:none">{{ row.type }}</b>
                    <span><i aria-hidden="true" class="fa fa-comments-o"></i></span>
                </button>
                <?php
                echo $this->Form->button(BTN_ICON_EDIT, [
                    'type' => 'button',
                    'class' => 'btn btn-primary btn-sm',
                    'v-if' => 'row.suspend === false',
                    'escape' => false,
                ]);
                echo $this->Form->button(BTN_ICON_UNDO, [
                    'type' => 'button',
                    'class' => 'btn btn-suspend btn-sm',
                    'v-else' => 'row.suspend === true',
                    'escape' => false,
                    'data-name' => 'recover',
                    'v-on:click' => 'modalPopUp(row, $event)',
                ]);
                echo $this->Form->button(BTN_ICON_STOP, [
                    'type' => 'button',
                    'class' => 'btn btn-suspend btn-sm',
                    'data-name' => 'suspend',
                    'escape' => false,
                    'v-if' => 'row.suspend === false',
                    'v-on:click' => 'modalPopUp(row, $event)',
                ]);
                echo $this->Form->button(BTN_ICON_DELETE, [
                    'type' => 'button',
                    'class' => 'btn btn-delete btn-sm',
                    'data-name' => 'delete',
                    'escape' => false,
                    'v-else' => 'row.suspend === true',
                    'v-on:click' => 'modalPopUp(row, $event)',
                ]);
                ?>
            </td>
        </tr>
    </tbody>
</table>