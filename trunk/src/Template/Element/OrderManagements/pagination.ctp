
<div class="row row-top-space" style="padding-bottom: 20px;">
    <div class="col-md-3 pull-left custom-select">
        <?php
        echo $this->Form->select('displays', [
            '10' => __('DISPLAY_ITEM {0}', [10]),
            '20' => __('DISPLAY_ITEM {0}', [20]),
            '50' => __('DISPLAY_ITEM {0}', [50]),
            '100' => __('DISPLAY_ITEM {0}', [100]),
        ], [
            'class' => 'form-control display-data',
            'id' => false,
            'label' => false,
        ]); ?>
    </div>
    <div class="col-md-9">
        <nav class="pull-right">
            <ul class="pagination pagination-sm custom-pagination">
                <li class="first">
                    <?php
                    echo $this->Html->link('First', 'javascript:void(0);', [
                        '@click' => 'getOrderList(pagination.first)',
                    ]); ?>
                </li>
                <li class="prev" v-bind:class="[{disabled: !pagination.prev_page}]">
                    <a href="javascript:void(0);" @click="getOrderList(pagination.prev_page)">
                        <span class="glyphicon glyphicon-chevron-left"></span>
                    </a>
                </li>
                <template v-for="page in pagination.page_count">
                    <li v-bind:class="[(pagination.current_page == page) ? 'active' : '']">
                        <a href="javascript:void(0);" @click="clickLink(page)">
                            {{ page }}
                        </a>
                    </li>
                </template>
                <li class="next" v-bind:class="[{disabled: !pagination.next_page}]">
                    <a href="javascript:void(0);" @click="getOrderList(pagination.next_page)">
                        <span class="glyphicon glyphicon-chevron-right"></span>
                    </a>
                </li>
                <li class="last" v-bind:class="[{disabled: !pagination.last}]">
                    <?php
                    echo $this->Html->link('Last', 'javascript:void(0);', [
                        '@click' => 'getOrderList(pagination.last)',
                    ]); ?>
                </li>
            </ul>
        </nav>
    </div>
</div
