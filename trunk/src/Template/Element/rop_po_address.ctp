<div class="col-md-5">
    <div class="company-invoice-logo">
        <h4><?= $supplier_name ?></h4>
    </div>
    <p><?= h($addr) ?></p>
    <?php
        if ($local == '') {
            echo '<p>' . $jp_addr2 . '</p>';
        } else {
            echo '<p>' . $en_addr2 . '</p>';
        }
    ?>
    <P><?= __('TXT_ADDRESS_TEL') ?> <?= $tel ?> / <?= __('TXT_ADDRESS_FAX') ?> <?= $top_fax ?></P>
    <?php
        if ($is_medipro == 'Sales') {
            echo '<p>UEN No. 201403392G</p>';
        }
    ?>
</div>