<?php
if (!isset($params['escape']) || $params['escape'] !== false) {
    $message = h($message);
}
?>
<div class="message success " onclick="this.classList.add('hidden')">
    <p class="alert alert-success"><?php echo $message; ?> </p>
</div>
