<?php
if (!isset($params['escape']) || $params['escape'] !== false) {
    $message = h($message);
}
?>
<div class="message error">
    <p class="alert alert-danger"><?php echo $message; ?> </p>
</div>
