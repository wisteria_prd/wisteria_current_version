
<tr data-index="0">
    <td>
        <div class="form-group">
            <?php
            echo $this->Form->select('pricing_range_managements[0].price_type', [
                DISCOUNT_PRICE_TYPE => __d('pricing', 'TXT_DISCOUNT_PRICE_TYPE'),
                CLINIC_PRICE_TYPE => __d('pricing', 'TXT_CLINIC_PRICE_TYPE'),
                FOC_PRICE_TYPE => __d('pricing', 'TXT_FOC_PRICE_TYPE'),
            ], [
                'class' => 'form-control price-type',
                'label' => false,
                'required' => false,
                'empty' => ['' => __d('pricing', 'TXT_SELECT_PRICE_TYPE')],
            ]); ?>
        </div>
    </td>
    <td>
        <div class="form-group">
            <?php
            echo $this->Form->select('pricing_range_managements[0].range_type', [
                TYPE_AMOUNT => __d('pricing', 'TXT_AMOUNT'),
                TYPE_QUANTITY => __d('pricing', 'TXT_QUANTITY'),
            ], [
                'class' => 'form-control range-type',
                'label' => false,
                'required' => false,
                'empty' => ['' => __d('pricing', 'TXT_SELECT_RANG_TYPE')],
            ]); ?>
        </div>
    </td>
    <td>
        <div class="form-group">
            <?php
            echo $this->Form->select('pricing_range_managements[0].value_type', [
                TYPE_AMOUNT => __d('pricing', 'TXT_AMOUNT'),
                TYPE_PERCENT => __d('pricing', 'TXT_PERCENT'),
            ], [
                'class' => 'form-control value-type',
                'label' => false,
                'required' => false,
                'empty' => ['' => __d('pricing', 'TXT_SELECT_VALUE_TYPE')],
            ]); ?>
        </div>
    </td>
    <td>
        <div class="form-group">
            <?php
            echo $this->Form->select('pricing_range_managements[0].currency_id', $currencies, [
                'class' => 'form-control currency-id',
                'label' => false,
                'required' => false,
                'empty' => ['' => __d('pricing', 'TXT_SELECT_CURRENCY')],
                'disabled' => true,
            ]); ?>
        </div>
    </td>
    <td>
        <div class="form-group">
            <?php
            echo $this->Form->number('pricing_range_managements[0].range_from', [
                'class' => 'form-control',
                'label' => false,
                'required' => false,
                'placeholder' => __d('pricing', 'TXT_ENTER_RANG_FROM'),
            ]); ?>
        </div>
    </td>
    <td>
        <div class="form-group">
            <?php
            echo $this->Form->number('pricing_range_managements[0].range_to', [
                'class' => 'form-control',
                'label' => false,
                'required' => false,
                'placeholder' => __d('pricing', 'TXT_ENTER_RANG_TO'),
            ]); ?>
        </div>
    </td>
    <td>
        <div class="form-group">
            <?php
            echo $this->Form->number('pricing_range_managements[0].value', [
                'class' => 'form-control',
                'label' => false,
                'required' => false,
                'placeholder' => __d('pricing', 'TXT_ENTER_VALUE'),
            ]); ?>
        </div>
    </td>
    <td>
        <?php
        echo $this->Form->button('<i class="fa fa-plus" aria-hidden="true"></i>', [
            'type' => 'button',
            'class' => 'btn btn-sm btn-primary btn-add-row',
            'escape' => false,
        ]); ?>
    </td>
</tr>
