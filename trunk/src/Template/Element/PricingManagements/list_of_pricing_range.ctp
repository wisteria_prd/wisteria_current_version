
<?php if ($data->pricing_range_managements): ?>
    <?php foreach ($data->pricing_range_managements as $key => $value): ?>
        <tr data-index="<?php echo $key; ?>">
            <td>
                <div class="form-group">
                    <?php
                    echo $this->Form->select('pricing_range_managements[' . $key . '].price_type', [
                        DISCOUNT_PRICE_TYPE => __d('pricing', 'TXT_DISCOUNT_PRICE_TYPE'),
                        CLINIC_PRICE_TYPE => __d('pricing', 'TXT_CLINIC_PRICE_TYPE'),
                        FOC_PRICE_TYPE => __d('pricing', 'TXT_FOC_PRICE_TYPE'),
                    ], [
                        'class' => 'form-control price-type',
                        'label' => false,
                        'required' => false,
                        'empty' => ['' => __d('pricing', 'TXT_SELECT_PRICE_TYPE')],
                        'default' => $value->price_type,
                    ]); ?>
                </div>
            </td>
            <td>
                <div class="form-group">
                    <?php
                    echo $this->Form->select('pricing_range_managements[' . $key . '].range_type', [
                        TYPE_AMOUNT => __d('pricing', 'TXT_AMOUNT'),
                        TYPE_QUANTITY => __d('pricing', 'TXT_QUANTITY'),
                    ], [
                        'class' => 'form-control range-type',
                        'label' => false,
                        'required' => false,
                        'empty' => ['' => __d('pricing', 'TXT_SELECT_RANG_TYPE')],
                        'default' => $value->range_type,
                    ]); ?>
                </div>
            </td>
            <td>
                <div class="form-group">
                    <?php
                    echo $this->Form->select('pricing_range_managements[' . $key . '].value_type', [
                        TYPE_AMOUNT => __d('pricing', 'TXT_AMOUNT'),
                        TYPE_PERCENT => __d('pricing', 'TXT_PERCENT'),
                    ], [
                        'class' => 'form-control value-type',
                        'label' => false,
                        'required' => false,
                        'empty' => ['' => __d('pricing', 'TXT_SELECT_VALUE_TYPE')],
                        'default' => $value->value_type,
                    ]); ?>
                </div>
            </td>
            <td>
                <div class="form-group">
                    <?php
                    echo $this->Form->select('pricing_range_managements[' . $key . '].currency_id', $currencies, [
                        'class' => 'form-control currency-id',
                        'label' => false,
                        'required' => false,
                        'empty' => ['' => __d('pricing', 'TXT_SELECT_CURRENCY')],
                        'disabled' => true,
                        'default' => $value->currency_id ? $value->currency_id : '',
                    ]); ?>
                </div>
            </td>
            <td>
                <div class="form-group">
                    <?php
                    echo $this->Form->number('pricing_range_managements[' . $key . '].range_from', [
                        'class' => 'form-control',
                        'label' => false,
                        'required' => false,
                        'placeholder' => __d('pricing', 'TXT_ENTER_RANG_FROM'),
                        'value' => $value->range_from,
                    ]); ?>
                </div>
            </td>
            <td>
                <div class="form-group">
                    <?php
                    echo $this->Form->number('pricing_range_managements[' . $key . '].range_to', [
                        'class' => 'form-control',
                        'label' => false,
                        'required' => false,
                        'placeholder' => __d('pricing', 'TXT_ENTER_RANG_TO'),
                        'value' => $value->range_to,
                    ]); ?>
                </div>
            </td>
            <td>
                <div class="form-group">
                    <?php
                    echo $this->Form->number('pricing_range_managements[' . $key . '].value', [
                        'class' => 'form-control',
                        'label' => false,
                        'required' => false,
                        'placeholder' => __d('pricing', 'TXT_ENTER_VALUE'),
                        'value' => $value->value,
                    ]); ?>
                </div>
            </td>
            <td>
                <?php
                if ($key == 0) {
                    echo $this->Form->button('<i class="fa fa-plus" aria-hidden="true"></i>', [
                        'type' => 'button',
                        'class' => 'btn btn-sm btn-primary btn-add-row',
                        'escape' => false,
                    ]);
                } else {
                    echo $this->Form->button('<i class="fa fa-trash" aria-hidden="true"></i>', [
                        'type' => 'button',
                        'class' => 'btn btn-delete btn-sm',
                        'escape' => false,
                    ]);
                }
                ?>
            </td>
        </tr>
    <?php endforeach; ?>
<?php endif; ?>
