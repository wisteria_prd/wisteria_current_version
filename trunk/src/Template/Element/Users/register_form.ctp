
<div class="modal-content">
    <div class="modal-body">
        <div class="row">
            <div class="col-sm-12">
                <div class="form-section" style="padding-top: 20px;">
                    <?php
                    echo $this->Form->create($user, [
                        'class' => 'form-horizontal',
                        'novalidate' => true,
                        'autocomplete' => 'off',
                        'onsubmit' => 'return false;',
                    ]);
                    echo $this->Form->hidden('target', [
                        'value' => $target,
                    ]);
                    if ($user) {
                        echo $this->Form->hidden('id');
                    }
                    ?>
                    <div class="form-wrapper">
                        <div class="form-group custom-select">
                            <label for="userClass" class="col-md-3 control-label">
                                <?php echo __d('user', 'TXT_ORGANIZATION') ?>
                                <i class="required-indicator">＊</i>
                            </label>
                            <div class="col-md-8">
                                <?php
                                $classifies = [
                                    USER_CLASS_WISTERIA => __d('user', 'TXT_WISTERIA'),
                                    USER_CLASS_MEDIPRO => __d('user', 'TXT_MEDIPRO'),
                                ];
                                if ($this->request->session()->read('user_group.affiliation_class') === USER_CLASS_MEDIPRO) {
                                    $classifies = [
                                        USER_CLASS_MEDIPRO => __d('user', 'TXT_MEDIPRO'),
                                    ];
                                }
                                echo $this->Form->select('affiliation_class', $classifies, [
                                    'empty' => ['' => __('STR_USER_SELECT_ORGANIZATION')],
                                    'class' => 'form-control user-affiliation-class',
                                    'id' => 'affiliation-class',
                                    'value' => $user->isNew() ? $this->request->session()->read('user_group.affiliation_class') : $user->user_group->affiliation_class,
                                ]);
                                ?>
                            </div>
                        </div>
                        <div class="form-group custom-select">
                            <label for="userClass" class="col-md-3 control-label">
                                <?php
                                echo __d('user', 'TXT_USER_ROLE'); ?>
                                <i class="required-indicator">＊</i>
                            </label>
                            <div class="col-md-8 select-user-role">
                                <?php
                                // display user role depends on affiliation_class
                                $affiliation_class = $this->request->session()->read('user_group.affiliation_class');
                                if (!$user->isNew()) {
                                    $affiliation_class = $user->user_group->affiliation_class;
                                }
                                echo $this->element('Users/select_user_role', [
                                    'affiliation_class' => $affiliation_class,
                                    'userGroups' => $userGroups,
                                ]);
                                ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="userBasicName" class="col-md-3 control-label">
                                <?php
                                echo __d('user', 'TXT_USER_NAME_JP'); ?>
                                <i class="required-indicator">＊</i>
                            </label>
                            <div class="col-md-4">
                                <?php
                                echo $this->Form->text('lastname', [
                                    'class' => 'form-control',
                                    'placeholder' => __('STR_LNAME_JP'),
                                    'id' => 'userBasicLName',
                                ]);
                                ?>
                            </div>
                            <div class="col-md-4">
                                <?php
                                echo $this->Form->text('firstname', [
                                    'class' => 'form-control',
                                    'placeholder' => __('STR_FNAME_JP'),
                                    'id' => 'userBasicFName',
                                ]);
                                ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="userBasicNameKana" class="col-md-3 control-label">
                                <?php
                                echo __d('user', 'TXT_USER_NAME_EN'); ?>
                                <i class="required-indicator">＊</i>
                            </label>
                            <div class="col-md-4">
                                <?php
                                echo $this->Form->text('lastname_en', [
                                    'class' => 'form-control',
                                    'placeholder' => __('STR_LNAME_EN'),
                                    'id' => 'userBasicLNameK',
                                ]);
                                ?>
                            </div>
                            <div class="col-md-4">
                                <?php
                                echo $this->Form->text('firstname_en', [
                                    'class' => 'form-control',
                                    'placeholder' => __('STR_FNAME_EN'),
                                    'id' => 'userBasicFNameK',
                                ]);
                                ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="userBasicEmail" class="col-md-3 control-label">
                                <?php echo __('STR_USER_EMAIL') ?>
                                <i class="required-indicator">＊</i>
                            </label>
                            <div class="col-md-8">
                                <?php
                                echo $this->Form->email('email', [
                                    'class' => 'form-control',
                                    'placeholder' => __d('user', 'TXT_ENTER_EMAIL'),
                                    'id' => 'userBasicEmail',
                                ]);
                                ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="userBasicPhone" class="col-md-3 control-label">
                                <?php echo __('STR_USER_PHONE') ?>
                            </label>
                            <div class="col-md-8">
                                <?php
                                echo $this->Form->text('mobile_phone', [
                                    'class' => 'form-control',
                                    'placeholder' => __d('user', 'TXT_ENTER_MOBILE'),
                                    'id' => 'userBasicPhone',
                                ]);
                                ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="userDepartment" class="col-md-3 control-label">
                                <?php
                                echo __d('user', 'TXT_DEPARTMENT'); ?>
                                <i class="required-indicator">＊</i>
                            </label>
                            <div class="col-md-8">
                                <?php
                                echo $this->Form->text('department', [
                                    'class' => 'form-control',
                                    'placeholder' => __('STR_ENTER_DEPARTMENT'),
                                    'id' => 'userDepartment',
                                ]);
                                ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="userPosition" class="col-md-3 control-label">
                                <?= __('STR_USER_POSITION') ?>
                            </label>
                            <div class="col-md-8">
                                <?php
                                echo $this->Form->text('position', [
                                    'class' => 'form-control',
                                    'placeholder' => __('STR_ENTER_POSITION'),
                                    'id' => 'userPosition',
                                ]);
                                ?>
                            </div>
                        </div>
                        <div class="form-group custom-select">
                            <label for="userOtherStatusType" class="col-md-3 control-label">
                                <?= __('STR_USER_STATUS') ?>
                            </label>
                            <div class="col-md-8">
                                <?php
                                $status = [
                                    STATUS_ACTIVE => __('STR_USER_STATUS_RECOVER'),
                                    STATUS_INACTIVE => __('STR_USER_STATUS_SUSPEND'),
                                ];
                                echo $this->Form->select('status', $status, [
                                    'class' => 'form-control',
                                    'empty' => false,
                                ]);
                                ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="userClinicDepartment" class="col-md-3 control-label">
                                <?= __('TXT_PASSWORD') ?>
                            </label>
                            <div class="col-md-8 email-inner" style="padding-top: 7px">
                                <?php
                                if ($this->request->query('target') === 'edit') : ?>
                                    <p><?= __('STR_USER_EDIT_ALERT_MAIL') ?></p>
                                <?php
                                else : ?>
                                    <p><?= __('STR_USER_ALERT_MAIL') ?></p>
                                <?php
                                endif; ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="userOtherRemarks" class="col-md-3 control-label">
                                <?php
                                echo __d('user', 'TXT_REMARKS'); ?>
                            </label>
                            <div class="col-md-8">
                                <?php
                                echo $this->Form->textarea('remarks', [
                                    'class' => 'form-control',
                                    'placeholder' => __d('user', 'TXT_ENTER_REMARK'),
                                    'id' => 'userOtherRemarks',
                                ]);
                                ?>
                            </div>
                        </div>
                    </div>
                    <?php echo $this->Form->end(); ?>
                </div>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <div class="btn-inline">
            <?php
            echo $this->Form->button(__('BTN_CANCEL'), [
                'class' => 'btn btn-default btn-sm modal-close',
                'type' => 'button',
                'data-dismiss' => 'modal',
            ]);
            ?>
        </div>
        <div class="btn-inline">
            <?php
            echo $this->Form->button($this->request->query('target') === 'edit' ? __('BTN_REGISTER_NEXT_UPDATE') : __('BTN_REGISTER_NEXT'), [
                'class' => 'btn btn-primary btn-sm btn-save',
                'type' => 'button',
            ]);
            ?>
        </div>
    </div>
</div>
