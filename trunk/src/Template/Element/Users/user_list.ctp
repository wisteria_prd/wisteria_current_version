<div class="table-responsive data-tb-list">
<table class="table table-hover table-striped">
    <thead>
        <tr>
            <th>#</th>
            <th>&nbsp;</th>
            <th>
                <?php
                echo $this->Paginator->sort('Users.lastname', __('USER_USERNAME')); ?>
                <span><i class="fa fa-sort-desc" aria-hidden="true"></i></span>
            </th>
            <th>
                <?php
                echo $this->Paginator->sort('UserGroups.name', __('STR_USER_ROLE')); ?>
                <span><i class="fa fa-sort-desc" aria-hidden="true"></i></span>
            </th>
            <th>
                <?php
                echo $this->Paginator->sort('Users.email', __('USER_EMAIL')); ?>
                <span><i class="fa fa-sort-desc" aria-hidden="true"></i></span>
            </th>
            <th>
                <?php
                echo $this->Paginator->sort('Users.modified', __('DATE_MODIFIED')); ?>
                <span><i class="fa fa-sort-desc" aria-hidden="true"></i></span>
            </th>
            <th>
                <?php
                echo $this->Paginator->sort('Users.created', __('DATE_CREATED')); ?>
                <span><i class="fa fa-sort-desc" aria-hidden="true"></i></span>
            </th>
            <th class="actions"></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($users as $key => $user): ?>
        <tr data-id="<?php echo $user->id; ?>">
            <td><?php echo $key + $this->Paginator->counter('{{start}}'); ?></td>
            <td class="text-danger">
                <?php if ($user->status === STATUS_INACTIVE) : ?>
                    <p class="btn btn-warning btn-xs" disabled>
                    <?php echo __('TXT_SUSPEND') ?>
                    </p>
                <?php endif; ?>
            </td>
            <td><?php echo h($user->lastname) . ' ' . h($user->firstname); ?></td>
            <td>
                <?php
                if ($session->read('user_group.affiliation_class') == USER_CLASS_WISTERIA) {
                    echo h($user->user_group->name);
                } else {
                    echo h($user->user_group->name_en);
                }
                ?>
            </td>
            <td><?php echo h($user->email); ?></td>
            <td><?php echo h($user->modified->format('Y/m/d')); ?></td>
            <td><?php echo h($user->created->format('Y/m/d')); ?></td>
            <td class="actions text-right">
                <?php
                // CHECK USER STATUS
                if ($user->status === STATUS_ACTIVE) {
                    echo $this->Form->button(BTN_ICON_EDIT, [
                        'type' => 'button',
                        'class' => 'btn btn-primary btn-sm create-user',
                        'id' => false,
                        'data-id' => $user->id,
                        'data-group' => $user->user_group_id,
                        'data-target' => TARGET_EDIT,
                    ]);
                    echo '&nbsp;';
                    // CHECH USER ROLE IS ADMIN
                    if (($user->id === $this->request->session()->read('Auth.User.id')) ||
                            ($user->user_group->role === USER_ROLE_ADMIN)) {
                        echo $this->Form->button(BTN_ICON_STOP, [
                            'class' => 'btn btn-suspend btn-sm',
                            'data-status' => STATUS_INACTIVE,
                            'disabled' => true,
                        ]);
                    } else {
                        echo $this->Form->button(BTN_ICON_STOP, [
                            'class' => 'btn btn-warning btn-sm btn-change-status btn-restore',
                            'data-status' => STATUS_INACTIVE,
                        ]);
                    }
                } else {
                    echo $this->Form->button(BTN_ICON_UNDO, [
                        'class' => 'btn btn-recover btn-sm btn-restore',
                        'data-status' => STATUS_ACTIVE,
                        'escape' => false
                    ]);
                    echo '&nbsp;';
                    echo $this->Form->button(BTN_ICON_DELETE, [
                        'class' => 'btn btn-danger btn-sm delete-user',
                        'data-status' => STATUS_DELETED,
                    ]);
                }
                ?>
            </td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>
</div>

<script>
$(document).ready(function() {
    $('body').on('click', '.btn-restore', function(e) {
        var status = $(this).attr('data-status');
        var modal = $('#modal_suspend');
        var message = '';
        switch (status) {
            case 'Inactive':
                $('#btn_suspend_yes').text('<?= __('BTN_SUSPEND') ?>');
                message = '<?= __('MSG_CONFIRM_SUSPEND') ?>';
                break;
            case 'Deleted':
                message = '<?= __('MSG_CONFIRM_DELETE') ?>';
                $('#btn_suspend_yes').text('<?= __('BTN_DELETE') ?>');
                break;
             case 'Active':
                $('#btn_suspend_yes').text('<?= __('BTN_RECOVER') ?>');
                message = '<?= __('MSG_CONFIRM_UNDO_SUSPEND') ?>';
                break;
        };

        $(modal).find('.modal-body p').html(message);
        $(modal).find('#btn_suspend_yes').attr('data-id', $(this).closest('tr').attr('data-id'));
        $(modal).find('#btn_suspend_yes').attr('data-status', status);
        $(modal).modal('show');
    });

    $('body').on('click', '#btn_suspend_yes', function(e) {
        var id = $(this).attr('data-id');
        var status = $(this).attr('data-status');
        request_change_status(id, status);
    });

    function request_change_status(id, status)
    {
        $.post('<?php echo $this->Url->build('/users/change-status/'); ?>' + id, {status: status}, function(data) {
            location.reload();
        });
    }
});
</script>
