<?php
$user_role = [];
$txt = '';
if ($affiliation_class === USER_CLASS_WISTERIA) {
    foreach ($userGroups as $key => $value) {
        if ($value->affiliation_class !== USER_CLASS_WISTERIA) {
            continue;
        }
        switch ($value->role) {
            case USER_ROLE_ADMIN:
                if ($value->affiliation_class === USER_CLASS_WISTERIA) {
                    $txt = __d('user', 'TXT_W_ADMIN');
                }
                break;

            case USER_ROLE_CUSTOMER_SUPPORT:
                $txt = __d('user', 'TXT_W_CUSTOMER_SUPPORT');
                break;

            case USER_ROLE_MANAGER_SALE:
                $txt = __d('user', 'TXT_W_MANAGER');
                break;

            case USER_ROLE_SALE:
                $txt = __d('user', 'TXT_W_SALE');
                break;
        }
        $user_role[] = [
            'value' => $value->id,
            'text' => $txt,
        ];
    }
} else {
    foreach ($userGroups as $key => $value) {
        if ($value->affiliation_class !== USER_CLASS_MEDIPRO) {
            continue;
        }
        switch ($value->role) {
            case USER_ROLE_ADMIN:
                if ($value->affiliation_class === USER_CLASS_MEDIPRO) {
                    $txt = __d('user', 'TXT_MP_ADMIN');
                }
                break;

            case USER_ROLE_LOGISTIC:
                $txt = __d('user', 'TXT_MP_LOGISTIC');
                break;
        }
        $user_role[] = [
            'value' => $value->id,
            'text' => $txt,
        ];
    }
}
echo $this->Form->select('user_group_id', $user_role, [
    'empty' => ['' => __('USER_SELECT_ROLE')],
    'class' => 'form-control user-role',
    'id' => 'role',
]);