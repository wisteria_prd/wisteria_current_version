<div class="modal fade" id="detail-list" data-backdrop="static" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body"></div>
            <div class="modal-footer">
                <div class="text-center">
                    <button type="button" class="btn btn-default btn-width btn-sm btn-custom-close btn-close-modal" data-dismiss="modal"><?= __('TXT_CLOSE') ?></button>
                </div>
            </div>
        </div>
    </div>
</div>
