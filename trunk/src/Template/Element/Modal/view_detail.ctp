
<div class="modal fade" id="modal_detail" data-backdrop="static" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body"></div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default btn-width btn-sm btn-close-modal" data-dismiss="modal"><?= __('PRODUCTS_TXT_CANCEL') ?></button>
            </div>
        </div>
    </div>
</div>
