<div class="modal fade" id="modal-product-list" data-backdrop="static" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <div class="product-list-content"></div>
            </div>
            <div class="modal-footer">
                <div class="row">
                    <div class="col-sm-6 text-right">
                        <button type="button" class="btn btn-default md-footer-btn btn-sm btn-close-modal" data-dismiss="modal"><?= __('TXT_CANCEL') ?></button>
                    </div>
                    <div class="col-sm-6 text-left">
                        <button type="button" class="btn btn-primary md-footer-btn btn-sm" id="select-product"><?= __('TXT_YES') ?></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
