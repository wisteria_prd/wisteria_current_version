<div class="modal fade modal-payment-cat" tabindex="-1" role="dialog" data-backdrop="static">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close btn-close-payment-cat" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title text-center"><?= __('TXT_PAYMENT_CAT') ?></h4>
            </div>
            <div class="modal-body fixed-modal-height">
                <div class="form-wrapper">
                    <label><?= __('TXT_PAYMENT_CAT') ?></label>
                    <?php
                    echo $this->Form->create('PaymentCategories', [
                    'class' => 'form-horizontal form-payment-category',
                    'role' => 'form',
                    ]);
                    echo $this->Form->hidden('id', ['class' => 'payment-id clear', 'value' => '']);
                    ?>
                    <div class="form-group">
                        <div class="col-sm-6 field-small-padding-right">
                            <?php echo $this->Form->input('name', [
                                'type' => 'text',
                                'label' => false,
                                'class' => 'form-control payment-name-jp clear',
                                'required' => false,
                                'placeholder' => __('TXT_ENTER_PAYMENT_CAT_NAME_JP'),
                                'id' => false
                            ]); ?>
                            <span class="help-block help-tips" id="error-name"></span>
                        </div>
                        <div class="col-sm-6 field-small-padding-left">
                            <?php echo $this->Form->input('name_en', [
                                'type' => 'text',
                                'label' => false,
                                'class' => 'form-control payment-name-en clear',
                                'required' => false,
                                'placeholder' => __('TXT_ENTER_PAYMENT_CAT_NAME_EN'),
                                'id' => false
                            ]); ?>
                            <span class="help-block help-tips" id="error-name_en"></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-12 text-center">
                            <?php echo $this->Form->button(__('TXT_ADD'), [
                                'type' => 'button',
                                'class' => 'btn btn-double-width btn-primary btn-sm btn-add-payment-cat'
                            ]); ?>
                        </div>
                    </div>
                    <?php echo $this->Form->end(); ?>
                </div>
                <table class="table table-striped custom-table-space payment-table">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th width="38%"><?= __('TXT_NAME') ?></th>
                            <th width="38%"><?= __('TXT_NAME_ENGLISH') ?></th>
                            <th>&nbsp;</th>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
            <div class="modal-footer">
                <div class="text-center">
                    <button type="button" class="btn btn-default btn-width btn-sm btn-close-modal btn-close-payment-cat" data-dismiss="modal"><?= __('TXT_CLOSE') ?></button>
                </div>
            </div>
        </div>
    </div>
</div>
