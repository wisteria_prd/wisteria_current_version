<div class="modal fade modal-invoice-method" tabindex="-1" role="dialog" data-backdrop="static">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close btn-close-invoice-method" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title text-center"><?= __('TXT_INVOICE_DELIVERY') ?></h4>
            </div>
            <div class="modal-body fixed-modal-height">
                <div class="form-wrapper">
                    <label><?= __('TXT_INVOICE_DELIVERY') ?></label>
                    <?php
                    echo $this->Form->create('InvoiceDeliveryMethods', [
                    'class' => 'form-horizontal form-invoice-method',
                    'role' => 'form',
                    ]);
                    echo $this->Form->hidden('id', ['class' => 'invoice-id clear', 'value' => '']);
                    ?>
                    <div class="form-group">
                        <div class="col-sm-6 field-small-padding-right">
                            <?php echo $this->Form->input('name', [
                                'type' => 'text',
                                'label' => false,
                                'class' => 'form-control invoice-name-jp clear',
                                'required' => false,
                                'placeholder' => __('TXT_ENTER_INVOICE_NAME_JP'),
                                'id' => false
                            ]); ?>
                        </div>
                        <div class="col-sm-6 field-small-padding-left">
                            <?php echo $this->Form->input('name_en', [
                                'type' => 'text',
                                'label' => false,
                                'class' => 'form-control invoice-name-en clear',
                                'required' => false,
                                'placeholder' => __('TXT_ENTER_INVOICE_NAME_EN'),
                                'id' => false
                            ]); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-12 text-center">
                            <?php echo $this->Form->button(__('TXT_ADD'), [
                                'type' => 'button',
                                'class' => 'btn btn-double-width btn-primary btn-sm btn-add-invoice-method'
                            ]); ?>
                        </div>
                    </div>
                    <?php echo $this->Form->end(); ?>
                </div>
                <table class="table table-striped custom-table-space invoice-table">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th width="38%"><?= __('TXT_NAME') ?></th>
                            <th width="38%"><?= __('TXT_NAME_ENGLISH') ?></th>
                            <th>&nbsp;</th>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
            <div class="modal-footer">
                <div class="text-center">
                    <button type="button" class="btn btn-default btn-width btn-sm btn-close-modal btn-close-invoice-method" data-dismiss="modal"><?= __('TXT_CLOSE') ?></button>
                </div>
            </div>
        </div>
    </div>
</div>
