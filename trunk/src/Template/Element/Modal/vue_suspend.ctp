
<div class="modal fade" id="modal_suspend" data-backdrop="static" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <p class="text-center confirm-suspend-text"></p>
                <?php
                    echo $this->Form->create(null, [
                        'role' => 'form',
                        'class' => 'form-horizontal form-suspend-item',
                        'type' => 'post'
                    ]);
                    echo $this->Form->hidden('id', ['class' => 'suspend-item-id', 'value' => '']);
                    echo $this->Form->hidden('is_suspend', ['class' => 'is-suspend-item', 'value' => '']);
                    echo $this->Form->end();
                ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default btn-sm btn-width" data-dismiss="modal"><?=__('BTN_CANCEL') ?></button>
                <button type="button" class="btn btn-primary btn-sm btn-width" id="btn_suspend_yes" v-on:click="suspendAndRecover"><?= __('BTN_SUSPEND') ?></button>
            </div>
        </div>
    </div>
</div>