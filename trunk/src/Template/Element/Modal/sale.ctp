
<div class="modal fade" id="modal-po-ms-to-s" data-backdrop="static" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title text-center"><?= __('TXT_CREATE') ?></h4>
            </div>
            <div class="modal-body"></div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default btn-sm btn-width" data-dismiss="modal"><?= __('TXT_CANCEL') ?></button>
                <?php echo $this->Form->button(__('TXT_REGISTER'), [
                    'class' => 'btn btn-primary btn-sm btn-width btn-register',
                    'data-type' => 'register',
                ]); ?>
            </div>
        </div>
    </div>
</div>
