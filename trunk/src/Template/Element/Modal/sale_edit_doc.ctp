
<div class="modal fade" id="edit-docs" data-backdrop="static" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title text-center"><?php echo __('TXT_EDIT_DOCUMENT') ?></h4>
            </div>
            <div class="modal-body"></div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default btn-sm btn-width" data-dismiss="modal">
                    <?= __('TXT_CANCEL') ?>
                </button>
                <button class="btn btn-primary btn-sm btn-width btn-submit-doc-pdf" type="button">
                    <?= __('TXT_REGISTER_SAVE') ?>
                </button>
            </div>
        </div>
    </div>
</div>
