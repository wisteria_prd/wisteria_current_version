
<div class="modal fade modal-society" tabindex="-1" role="dialog" data-backdrop="static">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title text-center"><?= __('STR_SOCIETY') ?></h4>
            </div>
            <div class="modal-body">
                <div class="form-wrapper">
                    <?php echo $this->Form->create('Societies', [
                    'url' => [
                        'controller' => 'societies',
                        'action' => 'create',
                    ],
                    'class' => 'form-horizontal',
                    'name' => 'society',
                    'role' => 'form',
                    ]); ?>
                    <div class="form-group">
                        <div class="col-md-6"><label><?= __('TXT_SOCIETY_NAME') ?></label></div>
                    </div>
                    <div class="col-lg-9 col-md-9" style="padding: 0;">
                        <div class="form-group">
                            <?php
                            echo $this->Form->input('name', [
                                'type' => 'text',
                                'label' => false,
                                'class' => 'form-control',
                                'required' => false,
                                'placeholder' => __('NAME'),
                                'templates' => [
                                    'inputContainer' => '<div class="col-sm-6" style="padding-right: 0;">{{content}}</div>',
                                ],
                            ]);
                            echo $this->Form->input('name_en', [
                                'type' => 'text',
                                'label' => false,
                                'class' => 'form-control',
                                'required' => false,
                                'placeholder' => __('TXT_NAME_ENGLISH'),
                                'templates' => [
                                    'inputContainer' => '<div class="col-sm-6">{{content}}</div>',
                                ],
                            ]);
                            ?>
                        </div>
                        <div class="form-group">
                            <?php
                            echo $this->Form->input('url', [
                                'type' => 'text',
                                'label' => false,
                                'class' => 'form-control',
                                'required' => false,
                                'placeholder' => __('STR_ENTER_URL'),
                                'templates' => [
                                    'inputContainer' => '<div class="col-sm-12">{{content}}</div>',
                                ],
                            ]);
                            ?>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3">
                        <div class="form-group">
                            <div class="col-sm-3">
                            <?php echo $this->Form->button(__('STR_ADD'), [
                                'class' => 'btn btn-width btn-primary btn-sm btn_tmp_save',
                                'data-target' => 'new',
                            ]); ?>
                            </div>
                        </div>
                    </div>
                    <?php echo $this->Form->end(); ?>
                </div>
                <div class="table-wrapper">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>&nbsp;</th>
                                <th>#</th>
                                <th><?= __('NAME') ?></th>
                                <th><?= __('TXT_NAME_ENGLISH') ?></th>
                                <th>&nbsp;</th>
                            </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default btn-width btn-sm" data-dismiss="modal"><?= __('BTN_CANCEL') ?></button>
                <button type="button" class="btn btn-primary btn-width btn-sm btn-save-tmp"><?= __('OK') ?></button>
            </div>
        </div>
    </div>
</div>
