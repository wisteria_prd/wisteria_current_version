
<div class="modal fade modal-temperature" tabindex="-1" role="dialog" data-backdrop="static">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title text-center">
                    <?php
                    echo __d('logistic_temperature', 'TXT_MODAL_TITLE'); ?>
                </h4>
            </div>
            <div class="modal-body fixed-modal-height">
                <div class="row">
                    <div class="col-md-12">
                        <?php echo $this->Form->create('LogisticTemperatures', [
                        'url' => [
                            'controller' => 'logistic-temperatures',
                            'action' => 'create',
                        ],
                        'class' => 'form-horizontal form-temperature',
                        'name' => 'temperaturer',
                        'role' => 'form',
                        ]); ?>
                        <label>
                            <?php
                            echo __d('logistic_temperature', 'TXT_LOGISTIC_TEMPERATURE'); ?>
                        </label>
                        <div class="form-group">
                            <?php
                            echo $this->Form->input('name_en', [
                                'type' => 'text',
                                'label' => false,
                                'class' => 'form-control',
                                'required' => false,
                                'placeholder' => __d('logistic_temperature', 'TXT_ENTER_LT_EN'),
                                'templates' => [
                                    'inputContainer' => '<div class="col-sm-6 field-small-padding-right">{{content}}</div>',
                                ],
                            ]);
                            echo $this->Form->input('name', [
                                'type' => 'text',
                                'label' => false,
                                'class' => 'form-control',
                                'required' => false,
                                'placeholder' => __d('logistic_temperature', 'TXT_ENTER_LT_JP'),
                                'templates' => [
                                    'inputContainer' => '<div class="col-sm-6 field-small-padding-left">{{content}}</div>',
                                ],
                            ]);
                            ?>
                        </div>
                        <div class="form-group">
                            <?php
                            echo $this->Form->input('temperature_range', [
                                'type' => 'text',
                                'label' => false,
                                'class' => 'form-control',
                                'required' => false,
                                'placeholder' => __d('logistic_temperature', 'TXT_ENTER_LT_RNAGE'),
                                'templates' => [
                                    'inputContainer' => '<div class="col-sm-12">{{content}}</div>',
                                ],
                            ]);
                            ?>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 text-center">
                                <?php
                                echo $this->Form->button(__d('logistic_temperature', 'TXT_REGISTER'), [
                                    'class' => 'btn btn-double-width btn-primary btn-sm btn_tmp_save',
                                    'data-target' => 'new',
                                ]); ?>
                            </div>
                        </div>
                        <?php echo $this->Form->end(); ?>
                    </div>
                </div>

                <table class="table table-striped custom-table-space">
                    <thead>
                        <tr>
                            <th>#</th>
                            <td>&nbsp;</th>
                            <th style="width: 30%;">
                                <?php
                                echo __d('logistic_temperature', 'TXT_LOGISTIC_TEMPERATURE'); ?>
                            </th>
                            <th>
                                <?php
                                echo __d('logistic_temperature', 'TXT_LT_RANGE'); ?>
                            </th>
                            <th>&nbsp;</th>
                            <th>&nbsp;</th>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>

            </div>
            <div class="modal-footer">
                <div class="text-center">
                    <button type="button" class="btn btn-default btn-width btn-sm btn-close-modal" data-dismiss="modal"><?= __('TXT_CLOSE') ?></button>
                </div>
                <!--<button type="button" class="btn btn-primary btn-width btn-sm btn-save-tmp"><?= __('TXT_YES') ?></button>-->
            </div>
        </div>
    </div>
</div>
