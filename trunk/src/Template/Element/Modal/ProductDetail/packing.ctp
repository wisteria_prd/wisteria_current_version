
<div class="modal fade modal-packings-list" tabindex="-1" role="dialog" data-backdrop="static">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title text-center">
                    <?php echo __d('product_detail', 'TXT_MODAL_PACKAGE_TITLE') ?>
                </h4>
            </div>
            <div class="modal-body fixed-modal-height">
                <div class="row">
                    <div class="col-sm-12">
                        <?php echo $this->Form->create('packings', [
                        'url' => [
                            'controller' => 'packings',
                            'action' => 'create',
                        ],
                        'class' => 'form-packings-list',
                        'name' => 'packing',
                        'role' => 'form',
                        ]);
                        $this->Form->templates([
                            'inputContainer' => '{{content}}'
                        ]); ?>
                        <div class="row">
                            <div class="col-sm-6 field-small-padding-right">
                                <label for="name">
                                    <?php echo __d('product_detail', 'MODAL_PACKAGE_PACKING') ?>
                                </label>
                                <?php
                                echo $this->Form->input('name', [
                                    'type' => 'text',
                                    'label' => false,
                                    'class' => 'form-control',
                                    'required' => false,
                                    'placeholder' => __d('product_detail', 'MODAL_PACKAGE_ENTER_PACKAGE_EN'),
                                ]); ?>
                            </div>
                            <div class="col-sm-6 field-small-padding-left">
                                <label for="name_en">&nbsp;</label>
                                <?php
                                echo $this->Form->input('name_en', [
                                    'type' => 'text',
                                    'label' => false,
                                    'class' => 'form-control',
                                    'required' => false,
                                    'placeholder' => __d('product_detail', 'MODAL_PACKAGE_ENTER_PACKAGE_JP')
                                ]); ?>
                            </div>
                        </div>
                        <div class="row" style="margin-top: 10px; margin-bottom: 10px;">
                            <div class="col-sm-6 field-small-padding-right">
                                <label for="unit_name">
                                    <?php echo __d('product_detail', 'MODAL_PACKAGE_UNIT') ?>
                                </label>
                                <?php
                                echo $this->Form->input('unit_name', [
                                    'type' => 'text',
                                    'label' => false,
                                    'class' => 'form-control',
                                    'required' => false,
                                    'placeholder' => __d('product_detail', 'MODAL_PACKAGE_ENTER_UNIT_PACK_SIZE_EN')
                                ]); ?>
                            </div>
                            <div class="col-sm-6 field-small-padding-left">
                                <label for="unit_name_en">&nbsp;</label>
                                <?php
                                echo $this->Form->input('unit_name_en', [
                                    'type' => 'text',
                                    'label' => false,
                                    'class' => 'form-control',
                                    'required' => false,
                                    'placeholder' => __d('product_detail', 'MODAL_PACKAGE_ENTER_UNIT_PACK_SIZE_JP')
                                ]); ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 text-center">
                                <?php
                                echo $this->Form->button(__d('product_detail', 'MODAL_PACKAGE_REGISTER'), [
                                    'type' => 'submit',
                                    'class' => 'btn btn-double-width btn-primary btn-sm btn_packing_save',
                                    'data-target' => 'new',
                                ]); ?>
                            </div>
                        </div>
                        <?php echo $this->Form->end(); ?>
                    </div>
                </div>
                <table class="table table-striped custom-table-space">
                    <thead>
                        <tr>
                            <th>#</th>
                            <td>&nbsp;</th>
                            <th width="28%">
                                <?php echo __d('product_detail', 'MODAL_PACKAGE_PACKING') ?>
                            </th>
                            <th>
                                <?php echo __d('product_detail', 'MODAL_PACKAGE_UNIT') ?>
                            </th>
                            <th>&nbsp;</th>
                            <td>&nbsp;</th>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
            <div class="modal-footer">
                <div class="text-center">
                    <button type="button" class="btn btn-default btn-width btn-sm btn-close-modal" data-dismiss="modal"><?= __('TXT_CLOSE') ?></button>
                </div>
<!--                <button type="button" class="btn btn-primary btn-width btn-sm btn-save-packsize"><?= __('TXT_YES') ?></button>-->
            </div>
        </div>
    </div>
</div>
