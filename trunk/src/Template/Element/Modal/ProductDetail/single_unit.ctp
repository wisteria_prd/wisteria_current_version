
<div class="modal fade modal-single-unit" tabindex="-1" role="dialog" data-backdrop="static">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title text-center"><?= __d('product_detail', 'MODAL_SINGLE_UNIT_TXT_REGISTER_AND_EDIT_SELECTION_OF_SINGLE_UNIT') ?></h4>
            </div>
            <div class="modal-body fixed-modal-height">
                <div class="form-wrapper">
                    <label><?= __d('product_detail', 'MODAL_SINGLE_UNIT_TXT_SINGLE_UNIT') ?></label>
                    <?php echo $this->Form->create('SingleUnits', [
                    'url' => [
                        'controller' => 'single-units',
                        'action' => 'create',
                    ],
                    'class' => 'form-horizontal form-single-unit',
                    'name' => 'single-unit',
                    'role' => 'form',
                    ]); ?>
                    <div class="col-sm-6 field-small-padding-left">
                        <?php echo $this->Form->input('name_en', [
                            'type' => 'text',
                            'label' => false,
                            'class' => 'form-control',
                            'required' => false,
                            'placeholder' => __d('product_detail', 'MODAL_SINGLE_UNIT_TXT_ENTER_SINGLE_UNIT_EN'),
                            'template' => [
                                'inputContainer' => '{{content}}',
                            ],
                        ]); ?>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-6 field-small-padding-right">
                            <?php echo $this->Form->input('name', [
                                'type' => 'text',
                                'label' => false,
                                'class' => 'form-control',
                                'required' => false,
                                'placeholder' => __d('product_detail', 'MODAL_SINGLE_UNIT_TXT_ENTER_SINGLE_UNIT_JP'),
                                'template' => [
                                    'inputContainer' => '{{content}}',
                                ],
                            ]); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-12 text-center">
                            <?php echo $this->Form->button(__d('product_detail', 'MODAL_SINGLE_UNIT_TXT_REGISTER'), [
                                'class' => 'btn btn-double-width btn-primary btn-sm btn_sunit_save',
                                'data-target' => 'new',
                            ]); ?>
                        </div>
                    </div>
                    <?php echo $this->Form->end(); ?>
                </div>
                <table class="table table-striped custom-table-space">
                    <thead>
                        <tr>
                            <th>#</th>
                            <td>&nbsp;</th>
                            <th width="38%"><?= __d('product_detail', 'MODAL_SINGLE_UNIT_TXT_SINGLE_UNIT') ?></th>
                            <th>&nbsp;</th>
                            <th>&nbsp;</th>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
            <div class="modal-footer">
                <div class="text-center">
                    <button type="button" class="btn btn-default btn-width btn-sm btn-close-modal" data-dismiss="modal"><?= __('TXT_CLOSE') ?></button>
                </div>
                <!--<button type="button" class="btn btn-primary btn-width btn-sm btn-save-sng"><?= __('TXT_YES') ?></button>-->
            </div>
        </div>
    </div>
</div>
