<div class="modal fade" id="modal_delete" data-backdrop="static" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <p class="text-center"><?= __('MSG_CONFIRM_DELETE') ?></p>
                <?php
                    echo $this->Form->create(null, [
                        'role' => 'form',
                        'class' => 'form-horizontal form-delete',
                        'type' => 'post'
                    ]);
                    echo $this->Form->hidden('id', ['class' => 'delete-item', 'value' => '']);
                    echo $this->Form->end();
                ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default btn-sm btn-width close-modal" data-dismiss="modal"><?= __('BTN_CANCEL') ?></button>
                <button type="button" class="btn btn-primary btn-sm btn-width btn-delete-item" id="btn_delete_yes"><?= __('BTN_DELETE') ?></button>
            </div>
        </div>
    </div>
</div>
