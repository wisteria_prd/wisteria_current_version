<div class="modal fade msg-category modal-message-category-list" data-backdrop="static" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <?php
                echo $this->Form->button('<span aria-hidden="true">&times;</span>', [
                    'type' => 'button',
                    'class' => 'close modal-close',
                    'data-dismiss' => 'modal',
                    'aria-label' => 'Close',
                    'escape' => false,
                ]);
                ?>
                <h4 class="modal-title text-center"><?= __d('message_category', 'REGISTER_AND_EDIT_SELECTION_OF_CATEGORY') ?></h4>
            </div>
            <div class="modal-body">
                <?php
                echo $this->Form->create(null, [
                    'role' => 'form',
                    'onsubmit' => 'return false;',
                    'autocomplete' => 'off',
                    'name' => 'message-category',
                    'class' => 'form-horizontal form-modal-message-category-list',
                ]);
                ?>
                <div class="form-group">
                    <label class="col-sm-4 control-label" style="white-space: nowrap;">
                        <?php echo __d('message_category', 'CATEGORY'); ?>
                    </label>
                    <div class="col-sm-4">
                        <?php
                        echo $this->Form->text('name_en', [
                            'class' => 'form-control',
                            'placeholder' => __d('message_category', 'ENTER_CATEGORY_EN'),
                        ]);
                        ?>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-4 control-label" style="white-space: nowrap;"></label>
                    <div class="col-sm-4">
                        <?php
                        echo $this->Form->text('name', [
                            'class' => 'form-control',
                            'placeholder' =>  __d('message_category', 'ENTER_CATEGORY_JP'),
                        ]);
                        ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 text-center">
                        <?php echo $this->Form->button(__d('message_category', 'REGISTER'), [
                            'class' => 'btn btn-double-width btn-primary btn-sm btn-add-new-category',
                            'data-target' => 'new',
                        ]); ?>
                    </div>
                </div>
                <?php echo $this->Form->end(); ?>

                <table class="table table-striped custom-table-space">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th><?= __d('message_category', 'CATEGORY') ?></th>
                            <th width="1%">&nbsp;</th>
                            <td  width="1%">&nbsp;</th>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
            <div class="modal-footer">
                <div class="col-md-12 text-center">
                    <?php
                    echo $this->Html->link(__('TXT_CANCEL'), 'javascript:void(0);', [
                        'class' => 'btn btn-default btn-sm btn-width modal-close',
                        'data-dismiss' => 'modal',
                        'aria-label' => 'Close',
                    ]);
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>
