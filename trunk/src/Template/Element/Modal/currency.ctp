<div class="modal fade" id="modal-currencies-list" data-backdrop="static" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
                
            </div>
            <div class="modal-footer">
                <div class="text-center">
                    <button type="button" class="btn btn-default btn-width md-footer-btn btn-sm btn-close-modal" data-dismiss="modal"><?= __('閉じる') ?></button>
                </div>
            </div>
        </div>
    </div>
</div>