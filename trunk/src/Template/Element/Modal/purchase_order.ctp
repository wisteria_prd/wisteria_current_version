
<div class="modal fade" id="modal-po-ms-to-s" data-backdrop="static" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title text-center"><?= __('TXT_CREATE') ?></h4>
            </div>
            <div class="modal-body">
                <?php
                    echo $this->Form->create(null, [
                        'class' => 'form-add-po form-horizontal',
                        'autocomplete' => 'off',
                        'name' => 'purchase_order',
                        'onsubmit' => 'return false;',
                    ]);
                    $this->Form->templates([
                        'inputContainer' => '{{content}}'
                    ]);
                ?>
                <div class="form-group">
                    <label for="" class="control-label col-md-3 col-md-offset-1"><?= __('TXT_SELLER') ?></label>
                    <div class="col-md-7 custom-select">
                        <?php echo $this->Form->input('seller_id', [
                            'type' => 'select',
                            'class' => 'form-control',
                            'label' => false,
                            'required' => false,
                            'options' => $sellers,
                        ]); ?>
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="control-label col-md-3 col-md-offset-1"><?= __('TXT_CURRENCY') ?></label>
                    <div class="col-md-7 custom-select">
                        <?php echo $this->Form->input('currency_id', [
                            'type' => 'select',
                            'class' => 'form-control',
                            'label' => false,
                            'required' => false,
                            'options' => $currencies,
                        ]); ?>
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="control-label col-md-3 col-md-offset-1"><?= __('TXT_TYPE') ?></label>
                    <div class="col-md-7 custom-select">
                        <?php echo $this->Form->input('type', [
                            'type' => 'select',
                            'class' => 'form-control',
                            'label' => false,
                            'required' => false,
                            'options' => $type,
                        ]); ?>
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="control-label col-md-3 col-md-offset-1"><?= __('TXT_ISSUE_DATE') ?><?= $this->Comment->formAsterisk() ?></label>
                    <div class="col-md-7">
                        <div class="input-group">
                            <?= $this->Form->input('issue_date', [
                                'class' => 'form-control',
                                'label' => false,
                                'required' => false,
                                'id' => 'datepicker',
                            ]) ?>
                            <div class="input-group-btn">
                                <button type="button" class="btn btn-default" aria-label="Calendar" id="trigger-calendar">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group custom-select">
                    <label for="" class="control-label col-md-3 col-md-offset-1"><?= __('TXT_AFFILIATION_CLASS') ?><?= $this->Comment->formAsterisk() ?></label>
                    <div class="col-md-7">
                        <?= $this->Form->input('affiliation_class', [
                            'type' => 'select',
                            'class' => 'form-control',
                            'label' => false,
                            'required' => false,
                            'options' => $affiliation_class,
                        ]) ?>
                    </div>
                </div>
                <?php echo $this->Form->end(); ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default btn-sm btn-width" data-dismiss="modal"><?= __('TXT_CANCEL') ?></button>
                <?php echo $this->Form->button(__('TXT_REGISTER'), [
                    'class' => 'btn btn-primary btn-sm btn-width btn-register',
                    'data-type' => 'register',
                ]); ?>
            </div>
        </div>
    </div>
</div>
