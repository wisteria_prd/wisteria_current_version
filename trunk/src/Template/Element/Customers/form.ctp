
<style>
    .facility-wrap .form-group {
        margin-bottom: 0px;
    }
    .facility-wrap .panel-body > div:first-child {
        margin-bottom: 15px;
    }
    .panel-wrap {
        margin-right: 15px;
        padding-right: 0px;
    }
</style>
<?php $action = $this->request->action; ?>
<div class="container">
    <div class="row">
        <section id="register_form">
            <?php
            echo $this->Form->create($customer, [
                'role' => 'form',
                'class' => 'form-horizontal form-customer',
                'name' => 'common_form',
                'onsubmit' => 'return false;',
            ]);
            if (!$customer->isNew()) {
                echo $this->Form->hidden('id');
            }
            ?>
                <div class="form-group custom-select">
                    <label class="col-sm-2 control-label">
                        <?= __('TXT_CLINIC_STATUS') ?>
                    </label>
                    <div class="col-lg-4 col-md-4 col-sm-4">
                        <?= $this->Form->select('type', $types, [
                            'class' => 'form-control',
                            'empty' => ['' => __('TXT_SELECT_CLINIC_STATUS')],
                        ]) ?>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">
                        <?= __('TXT_OLD_CODE') ?>
                    </label>
                    <div class="col-lg-4 col-md-4 col-sm-4">
                        <?= $this->Form->text('code', [
                            'class' => 'form-control',
                            'placeholder' => __('TXT_ENTER_OLD_CODE'),
                        ]) ?>
                    </div>
                </div>
                <?php
                if (false) : ?>
                <div class="form-group custom-select">
                    <label class="col-sm-2 control-label">
                        <?= __('TXT_MEDICAL_CORPORATIONS') ?>
                    </label>
                    <div class="col-lg-4 col-md-4 col-sm-4">
                        <?= $this->Form->select('medical_corporation_id', [], [
                            'class' => 'form-control',
                            'id' => 'medical-name-id',
                            'empty' => ['' => __('TXT_SELECT_MEDICAL_CORPORATIONS')],
                        ]) ?>
                    </div>
                </div>
                <?php
                endif; ?>
                <div class="form-group">
                    <label class="col-sm-2 control-label">
                        <?= __('TXT_CLINIC') . $this->Comment->formAsterisk() ?>
                    </label>
                    <div class="col-lg-4 col-md-4 col-sm-4">
                        <?= $this->Form->text('name_en', [
                            'class' => 'form-control',
                            'placeholder' => __('TXT_ENTER_CLINIC_NAME_EN'),
                        ]) ?>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-4">
                        <?= $this->Form->text('name', [
                            'class' => 'form-control',
                            'placeholder' => __('TXT_ENTER_CLINIC_NAME_JP'),
                        ]) ?>
                    </div>
                </div>
                <div class="form-group custom-select">
                    <label class="col-sm-2 control-label">
                        <?= __('TXT_POSTAL_CODE') ?>
                    </label>
                    <div class="col-lg-4 col-md-4 col-sm-4">
                        <?= $this->Form->text('postal_code', [
                            'class' => 'form-control',
                            'placeholder' => __('TXT_ENTER_POSTAL_CODE'),
                        ]) ?>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-4 custom-select">
                        <?= $this->Form->select('country_id', $countries, [
                            'class' => 'form-control',
                            'empty' => ['' => __('TXT_SELECT_COUNTRY')],
                        ]) ?>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">&nbsp;</label>
                    <div class="col-lg-4 col-md-4 col-sm-4">
                        <?= $this->Form->text('building_en', [
                            'class' => 'form-control',
                            'placeholder' => __('TXT_ENTER_BUILDING_IN_EN'),
                        ]) ?>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-4">
                        <?= $this->Form->text('building', [
                            'class' => 'form-control',
                            'placeholder' => __('TXT_ENTER_BUILDING_IN_JP'),
                        ]) ?>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">&nbsp;</label>
                    <div class="col-lg-4 col-md-4 col-sm-4">
                        <?= $this->Form->text('street_en', [
                            'class' => 'form-control',
                            'placeholder' => __('TXT_ENTER_STREET_IN_EN'),
                        ]) ?>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-4">
                        <?= $this->Form->text('street', [
                            'class' => 'form-control',
                            'placeholder' => __('TXT_ENTER_STREET_IN_JP'),
                        ]) ?>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">&nbsp;</label>
                    <div class="col-lg-4 col-md-4 col-sm-4">
                        <?= $this->Form->text('city_en', [
                            'class' => 'form-control',
                            'placeholder' => __('TXT_ENTER_CITY_EN'),
                        ]) ?>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-4">
                        <?= $this->Form->text('city', [
                            'class' => 'form-control',
                            'placeholder' => __('TXT_ENTER_CITY_JP'),
                        ]) ?>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">&nbsp;</label>
                    <div class="col-lg-4 col-md-4 col-sm-4">
                        <?= $this->Form->text('prefecture_en', [
                            'class' => 'form-control',
                            'placeholder' => __('TXT_ENTER_PREFECTURE_IN_EN'),
                        ]) ?>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-4">
                        <?= $this->Form->text('prefecture', [
                            'class' => 'form-control',
                            'placeholder' => __('TXT_ENTER_PREFECTURE_IN_JP'),
                        ]) ?>
                    </div>
                </div>
                <?php if (false) : ?>
                <div class="phone-box-wrap">
                    <?= $this->element('Customers/phone_list') ?>
                </div>
                <?php endif; ?>
                <div class="form-group">
                    <label class="col-sm-2 control-label">
                        <?php echo __('SUPPLIER_TEL') ?>
                    </label>
                    <div class="col-md-4 col-lg-4 col-sm-4">
                        <?php
                        $tel = '';
                        $tel_extension = '';
                        if (isset($customer->info_details) && $customer->info_details) {
                            $tel = $customer->info_details[0]->tel;
                            $tel_extension = $customer->info_details[0]->tel_extension;
                        }
                        echo $this->Form->tel('info_details[0].tel', [
                            'class' => 'form-control multiple-tel',
                            'placeholder' => __('SUPPLIER_ENTER_TEL'),
                            'value' => $tel,
                        ]);
                        echo $this->Form->hidden('info_details[0].type', [
                            'value' => TYPE_CUSTOMER,
                        ]);
                        ?>
                    </div>
                    <div class="col-md-4 col-lg-4 col-sm-4">
                        <?php
                        echo $this->Form->text('info_details[0].tel_extension', [
                            'class' => 'form-control',
                            'placeholder' => __('STR_TEL_EXT'),
                            'value' => $tel_extension,
                        ]);
                        ?>
                    </div>
                    <div class="col-sm-1">
                        <?php
                        echo $this->Form->button('<i class="fa fa-plus" aria-hidden="true"></i>', [
                            'type' => 'button',
                            'class' => 'btn btn-sm btn-primary btn-tel-add form-btn-add',
                            'escape' => false,
                        ]);
                        ?>
                    </div>
                </div>
                <div class="tel-box-wrap">
                    <?php echo $this->element('Customers/tel_list'); ?>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">
                        <?= __('Fax') ?>
                    </label>
                    <div class="col-lg-4 col-md-4 col-sm-4">
                        <?= $this->Form->text('fax', [
                            'class' => 'form-control',
                            'placeholder' => __('STR_ENTER_FAX'),
                        ]) ?>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">
                        <?= __('STR_USER_EMAIL') ?>
                    </label>
                    <div class="col-lg-4 col-md-4 col-sm-4">
                        <?php
                        $email = '';
                        if ($customer->info_details) {
                            foreach ($customer->info_details as $key => $value) {
                                if (($value->email === null) || empty($value->email)) {
                                    continue;
                                }
                                $email = $value->email;
                            }
                        }
                        echo $this->Form->email('info_details[0].email', [
                            'class' => 'form-control',
                            'placeholder' => __('STR_ENTER_EMAIL'),
                            'default' => $email,
                        ]); ?>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">
                        <?php
                        echo __d('customer', 'TXT_URL'); ?>
                    </label>
                    <div class="col-lg-4 col-md-4 col-sm-4">
                        <?= $this->Form->text('url', [
                            'class' => 'form-control',
                            'placeholder' => __('STR_ENTER_URL'),
                        ]) ?>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">
                        <?= __('STR_RECEIVER_NAME') ?>
                    </label>
                    <div class="col-lg-4 col-md-4 col-sm-4">
                        <?= $this->Form->text('reciever_name', [
                            'class' => 'form-control',
                            'placeholder' => __('STR_ENTER_RECEIVER'),
                        ]) ?>
                    </div>
                </div>
                <div class="form-group custom-select">
                    <label class="col-sm-2 control-label">
                        <?= __('STR_PAYMENT_CAT') . $this->Comment->formAsterisk() ?>
                    </label>
                    <div class="col-sm-4 col-lg-4">
                        <div class="col-sm-10 no-p-l">
                            <?php
                            $paymentList = [];
                            if ($payments) {
                                foreach ($payments as $key => $value) {
                                    $paymentList[] = [
                                        'value' => $value->id,
                                        'text' => $value->name . ' ' . $value->name_en,
                                    ];
                                }
                            }
                            echo $this->Form->select('payment_category_id', $paymentList, [
                                'class' => 'form-control field-payment',
                                'default' => isset($customer) ? $customer->payment_category_id : '',
                                'empty' => ['' => __('TXT_SELECT_PAYMENT')]
                            ]);
                            ?>
                        </div>
                        <div class="col-sm-2">
                            <?= $this->Form->button('<i class="fa fa-ellipsis-v" aria-hidden="true"></i>', [
                                'type' => 'button',
                                'class' => 'btn btn-sm btn-primary btn-payment-cats form-btn-add',
                                'escape' => false,
                            ]) ?>
                        </div>
                    </div>
                </div>
                <div class="form-group custom-select">
                    <label class="col-sm-2 control-label">
                        <?= __('STR_INVOICE_DELIVERY') . $this->Comment->formAsterisk() ?>
                    </label>
                    <div class="col-lg-4 col-md-4 col-sm-4">
                        <div class="col-sm-10 no-p-l">
                            <?php
                            $invoiceList = [];
                            if ($invoices) {
                                foreach ($invoices as $key => $value) {
                                    $invoiceList[] = [
                                        'value' => $value->id,
                                        'text' => $value->name . ' ' . $value->name_en,
                                    ];
                                }
                            }
                            echo $this->Form->select('invoice_delivery_method_id', $invoiceList, [
                                'class' => 'form-control field-invoice',
                                'default' => isset($customer) ? $customer->invoice_delivery_method_id : '',
                                'empty' => ['' => __('TXT_SELECT_INVOICE')]
                            ]);
                            ?>
                        </div>
                        <div class="col-sm-2">
                            <?= $this->Form->button('<i class="fa fa-ellipsis-v" aria-hidden="true"></i>', [
                                'type' => 'button',
                                'class' => 'btn btn-sm btn-primary btn-invoice-methods form-btn-add',
                                'escape' => false,
                            ]) ?>
                        </div>

                    </div>
                </div>
                <div class="form-group custom-select">
                    <label class="col-sm-2 control-label">
                        <?= __('STR_SUBSIDIARY_TYPE') ?>
                    </label>
                    <div class="col-lg-4 col-md-4 col-sm-4">
                        <div class="subsidiary-wrapper">
                            <div class="col-sm-10 no-p-l">
                                <?php
                                // pr($customer);
                                $customer_subs = [];
                                if (isset($customer) && $customer->customer_subsidiaries) {
                                    $subsidiary = $customer->customer_subsidiaries[0];
                                    $customer_subs[] = [
                                        'value' => $subsidiary->subsidiary_id,
                                        'text' => $subsidiary->subsidiary->name . ' ' . $subsidiary->subsidiary->name_en,
                                    ];
                                }
                                echo $this->Form->select('customer_subsidiaries[0].subsidiary_id', $customer_subs, [
                                    'class' => 'form-control select-subsidiary',
                                    'empty' => ['' => __('STR_SELECT_SUBSIDIARY_TYPE')],
                                    'value' => (isset($customer) && $customer->customer_subsidiaries) ? $subsidiary->subsidiary_id : null,
                                ]);
                                ?>
                            </div>
                            <div class="col-sm-2">
                                <?= $this->Form->button('<i class="fa fa-plus" aria-hidden="true"></i>', [
                                    'type' => 'button',
                                    'class' => 'btn btn-sm btn-primary btn-add-sub form-btn-add',
                                    'escape' => false,
                                ]) ?>
                            </div>
                        </div>
                        <div class="sub-box-wrap">
                            <?php
                            $customer = isset($customer) ? $customer : null;
                            echo $this->element('Customers/subsidiary_list', [$customer]);
                            ?>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-4">
                        <div class="facility-wrap">
                            <div class=" panel-wrap">
                                <div class="panel panel-default">
                                    <div class="panel-body">
                                        <div>
                                            <?php
                                            $clinic_doctors = [];
                                            $checked = '';
                                            if (isset($customer) && $customer->clinic_doctors) {
                                                $clinics = $customer->clinic_doctors[0];
                                                $clinic_doctors[] = [
                                                    'value' => $clinics->doctor_id,
                                                    'text' => $clinics->doctor->first_name_en . ' ' . $clinics->doctor->last_name_en,
                                                ];
                                                $checked = 'checked';
                                            }
                                            ?>
                                            <?php
                                            echo $this->Form->select('clinic_doctors[0].doctor_id', $clinic_doctors, [
                                                'class' => 'form-control doctor-select',
                                                'empty' => __('TXT_SELECT_DOCTOR'),
                                                'value' => (isset($customer) && $customer->clinic_doctors) ? $clinics->doctor_id : null,
                                            ]); ?>
                                        </div>
                                        <div>
                                            <?php
                                            echo $this->Form->text('clinic_doctors[0].position', [
                                                'class' => 'form-control doctor-position',
                                                'placeholder' => __('STR_ENTER_POSITION'),
                                                'default' => $clinic_doctors ? $clinic_doctors[0]['value'] : null,
                                            ]); ?>
                                        </div>
                                        <div class="radio">
                                            <label class="hidden" id="rdo-1">
                                                <input type="radio" name="is_priority" <?= !empty($checked) ? ' checked' : '' ?>/>
                                                <?= __('STR_PRIORITY') ?>
                                            </label>
                                            <?php
                                            echo $this->Form->hidden('clinic_doctors[0].is_priority', [
                                                'value' => !empty($checked) ? 1 : 0,
                                            ]); ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-1">
                        <?= $this->Form->button('<i class="fa fa-plus" aria-hidden="true"></i>', [
                            'type' => 'button',
                            'class' => 'btn btn-sm btn-primary btn-add-position form-btn-add',
                            'escape' => false,
                        ]) ?>
                    </div>
                    <div class="clinic-doctor-list custom-select">
                        <?php
                        echo $this->element('Customers/clinic_doctor_list');
                        ?>
                    </div>
                </div>
                <div class="form-group">
                    <?= $this->ActionButtons->btnSaveCancel('Customers', $action); ?>
                </div>
            <?= $this->Form->end(); ?>
        </section>
    </div>
</div>
<style>
    .subsidiary-wrapper .select2-selection {
        margin-bottom: 15px;
    }
</style>
<script>
    $(function() {
        // ADD MULTIPLE TELS
        $('body').on('click', '.btn-tel-add', function(e) {
            var index = $('body').find('.tel-box-wrap .form-group').length + 1;
            var content = '<div class="form-group">' +
                    '<label class="col-sm-2 control-label"></label>' +
                    '<div class="col-md-4 col-lg-4 col-sm-4">' +
                        '<input type="tel" placeholder="' + TXT_ENTER_TEL + '" class="form-control" name="info_details[' + index + '][tel]"/>' +
                        '<input type="hidden" name="info_details[' + index + '][type]" value="<?php echo TYPE_CUSTOMER; ?>"/>' +
                    '</div>' +
                    '<div class="col-md-4 col-lg-4 col-sm-4">' +
                        '<input type="text" placeholder="' + STR_TEL_EXT + '" class="form-control" name="info_details[' + index + '][tel_extension]"/>' +
                    '</div>' +
                    '<div class="col-sm-1">' +
                        '<button type="button" class=" btn btn-delete btn-sm form-btn-add btn-remove-field">' +
                            '<i class="fa fa-trash" aria-hidden="true"></i>' +
                        '</button>' +
                    '</div>' +
                '</div>';
            $('body').find('.tel-box-wrap').append(content);
        });
        // REMOVE EMAIL, TEL
        $('body').on('click', '.btn-remove-field', function(e) {
            $(this).closest('.form-group').remove();
        });

        getSubsidiaries();
        getCustomers();

        // Add multiple positions and doctors
        $('body').on('click', '.btn-add-position', function() {
            var index = $('body').find('.facility-wrap .panel').length;
            var content = '<div class="facility-wrap">';
                content += '<div class="col-lg-4 col-lg-offset-6">' +
                                '<div class="panel-wrap">' +
                                    '<div class="panel panel-default">' +
                                        '<div class="panel-body">' +
                                            '<div><select name="clinic_doctors[' + index + '][doctor_id]"class="form-control doctor-select">' +
                                                '<option value><?= __('TXT_SELECT_FACILITY_NAME') ?></option>' +
                                            '</select></div>' +
                                            '<div><input type="text" name="clinic_doctors[' + index + '][position]" placeholder="<?= __('STR_ENTER_POSITION') ?>" class="form-control doctor-position"></div>' +
                                            '<div class="radio">' +
                                                '<label>' +
                                                    '<input type="radio" name="is_priority"><?= __('STR_PRIORITY') ?>' +
                                                    '<input type="hidden" name="clinic_doctors[' + index + '][is_priority]">' +
                                                '</label>' +
                                            '</div>' +
                                        '</div>' +
                                    '</div>' +
                                '</div>' +
                            '</div>';
                    content += '<div class="col-sm-1">' +
                        '<button type="button" class="btn btn-rm-position btn-delete">' +
                            '<i class="fa fa-trash" aria-hidden="true"></i>' +
                        '</div>';
                    content += '</div>';
            $('#rdo-1').removeClass('hidden');
            $('body').find('.clinic-doctor-list').append(content);
            getCustomers();
        });

        $('body').on('click', '.btn-rm-position', function() {
            $(this).parents('.facility-wrap').remove();
        });

        $('body').on('click', '.btn-add-sub', function(e) {
            var content = $('body').find('.sub-box-wrap');
            var index = $(content).find('.form-group').length + 1
            var element = '<div class="subsidiary-wrapper">';
            element += '<div class="col-sm-10 no-p-l">' +
                                '<select class="form-control select-subsidiary" name="customer_subsidiaries[' + index + '][subsidiary_id]">' +
                                    '<option value><?= __('STR_SELECT_SUBSIDIARY_TYPE') ?></option>' +
                                '</select>' +
                            '</div>';
            element += '<div class="col-sm-2">' +
                            '<button type="button" class="btn btn-delete btn-sm form-btn-add btn-remove-subsidiary">' +
                                '<i class="fa fa-trash" aria-hidden="true"></i>' +
                            '</button>' +
                        '</div>';
            element += '</div>';
            $(content).append(element);
            getSubsidiaries();
        });
        $('body').on('click', '.btn-remove-subsidiary', function() {
            $(this).parents('.subsidiary-wrapper').remove();
        });

        //get payment on modal
        $('body').on('click', '.btn-payment-cats', function () {
            var params = {
                type: 'GET',
                url: '<?= $this->Url->build('/payment-categories/get-list') ?>',
                beforeSend: function() {
                    $.LoadingOverlay('show');
                }
            };
            ajaxRequest(params, SESSION_TIMEOUT, function(data) {
                $('body').prepend(data);
                $('body').find('.modal-payment-cat').modal('show');
            });
        });

        //get invoice on modal
        $('body').on('click', '.btn-invoice-methods', function () {
            var params = {
                type: 'GET',
                url: '<?= $this->Url->build('/invoice-delivery-methods/get-list') ?>',
                beforeSend: function() {
                    $.LoadingOverlay('show');
                }
            };
            ajaxRequest(params, SESSION_TIMEOUT, function(data) {
                $('body').prepend(data);
                $('body').find('.modal-invoice-method').modal('show');
            });
        });

        $('body').on('click', '.btn-phone-add', function() {
            var index = $('body').find('.phone-box-wrap .form-group').length + 1;
            var content = '<div class="form-group">' +
                        '<label class="col-sm-2 control-label"></label>' +
                        '<div class="ccol-lg-4 col-md-4 col-sm-4 no-p-l">' +
                            '<div class="col-sm-10">' +
                                '<input type="text" placeholder="<?= __('TXT_ENTER_PHONE') ?>" class="form-control" name="info_details[' + index + '][phone]"/>' +
                                '<input type="hidden" name="info_details[' + index + '][type]" value="<?= TYPE_CUSTOMER ?>"/>' +
                            '</div>' +
                            '<div class="col-sm-2">' +
                                '<button type="button" class=" btn btn-delete btn-sm form-btn-add btn-rm-position"><i class="fa fa-trash" aria-hidden="true"></i></button>' +
                            '</div>' +
                        '</div>' +
                    '</div>';
            $('body').find('.phone-box-wrap').append(content);
        });

        $('body').on('click', '.btn-remove-phone', function() {
            $(this).parents('.form-group').remove();
            var inx = $('.facility-wrap').find('.form-group').length;
            if (inx == 1) {
                $('#rdo-1').addClass('hidden');
            }
            updatePhoneIndex('phone', $(this));
        });

        $('body').on('input', '.multiple-phone', function() {
            var $this = $(this);
            removeLeadingZero('phone', $this);
        });

        $('#btn-register').on('click', function() {
            var form = $('body').find('.form-customer');
            var options = {
                type: 'POST',
                dataType: 'json',
                url: '<?= $this->Url->build('/customers/save-and-update/') ?>',
                data: $(form).serialize(),
                beforeSend: function() {
                    $.LoadingOverlay('show');
                    $(form).find('.error-message').remove();
                }
            };
            ajaxRequest(options, SESSION_TIMEOUT, function(data) {
                if (data.message === '<?= MSG_ERROR ?>') {
                    validation(form, data.data);
                    return false;
                }
                location.href = '<?= $this->Url->build('/customers') ?>'
            });
        });

        $('body').on('change', 'input:radio', function(e) {
            $('body').find('.panel-body input:hidden').val(0);
            $(this).closest('label').find('input:hidden').val(1);
        });

        function getSubsidiaries()
        {
            $('body').find('.select-subsidiary').select2({
                width: '100%',
                minLength: 0,
                ajax: {
                    url: '<?= $this->Url->build('/subsidiaries/getAutocompleteList') ?>',
                    dataType: 'json',
                    delay: 250,
                    data: function (params) {
                        var query = {
                            keyword: params.term,
                        };
                        return query;
                    },
                    processResults: function (data) {
                        var dataSource = [];
                        if (data.data === null && data.data === 'undefined') {
                            return false;
                        }
                        $.each(data.data, function(i, v) {
                            dataSource.push({
                                id: v.id,
                                text: v.name + ' ' + v.name_en
                            });
                        });
                        return { results: dataSource };
                    }
                }
            });
        }

        function getCustomers()
        {
            $('body').find('.doctor-select').select2({
                width: '100%',
                minLength: 0,
                ajax: {
                    url: '<?= $this->Url->build('/doctors/get-doctor-list/') ?>',
                    dataType: 'json',
                    delay: 250,
                    data: function (params) {
                        var query = {
                            keyword: params.term,
                        };
                        return query;
                    },
                    processResults: function (data) {
                        var dataSource = [];
                        if (data.data === null && data.data === 'undefined') {
                            return false;
                        }
                        $.each(data.data, function(i, v) {
                            dataSource.push({
                                id: v.id,
                                text: v.last_name_en + ' ' + v.first_name_en
                            });
                        });
                        return { results: dataSource };
                    }
                }
            });
        }

        function validation(form, data)
        {
            if ((data === null) && (data === 'undefined')) {
                return false;
            }
            $.each(data, function(i, v) {
                var message = '<label class="error-message">' + v._empty + '</label>';
                var content = $(form).find('[name="' + i + '"]');
                if (i === 'clinic_doctors') {
                    $.each(v, function(i1, v1) {
                        var panel = $('body').find('.facility-wrap');
                        var content1 = $(panel).find('.panel-wrap:eq(' + i1 + ')');
                        if (v1.doctor_id) {
                            var msg_customer = '<label class="error-message">' + v1.doctor_id._empty + '</label>';
                            $(content1).find('select').closest('div').append(msg_customer);
                        }
                        if (v1.position) {
                            var msg_position = '<label class="error-message">' + v1.position._empty + '</label>';
                            $(content1).find('input[type="text"]').closest('div').append(msg_position);
                        }
                    });
                }
                $(content).closest('div').append(message);
            });
        }
    });
</script>
