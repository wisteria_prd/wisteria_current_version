
<?php if (isset($customer) && isset($customer->customer_subsidiaries)): ?>
    <?php foreach ($customer->customer_subsidiaries as $key => $value): ?>
        <?php if ($key != 0): ?>
            <div class="subsidiary-wrapper">
                <div class="col-sm-10 no-p-l">
                    <?php
                    $subsidiary = [
                        'value' => $value->id,
                        'text' => $value->subsidiary->name . ' ' . $value->subsidiary->name_en,
                    ];
                    echo $this->Form->select('customer_subsidairies[' . $key . '].subsidiary_id', [$subsidiary], [
                        'class' => 'form-control select-subsidiary',
                        'empty' => ['' => __('STR_SELECT_SUBSIDIARY_TYPE')],
                        'value' => isset($customer) ? $value->id : null,
                    ]);
                    ?>
                </div>
                <div class="col-sm-2">
                    <?= $this->Form->button('<i class="fa fa-trash" aria-hidden="true"></i>', [
                        'type' => 'button',
                        'class' => ' btn btn-delete btn-sm form-btn-add btn-remove-subsidiary',
                        'escape' => false,
                    ]) ?>
                </div>
            </div>
        <?php endif; ?>
    <?php endforeach; ?>
<?php endif; ?>
