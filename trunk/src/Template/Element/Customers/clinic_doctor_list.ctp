<?php if (isset($customer) && $customer->clinic_doctors): ?>
    <?php foreach ($customer->clinic_doctors as $key => $value): if ($key != 0): ?>
        <div class="facility-wrap">
            <div class="col-lg-4 col-lg-offset-6">
                <div class="panel-wrap">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div>
                                <?= $this->Form->select('clinic_doctors[' . $key . '].doctor_id', [[
                                    'text' => $value->doctor->first_name_en . ' ' . $value->doctor->last_name_en,
                                    'value' => $value->doctor_id,
                                ]], [
                                    'class' => 'form-control doctor-select',
                                    'empty' => __('TXT_SELECT_FACILITY_NAME'),
                                    'default' => $value->doctor_id,
                                ]) ?>
                            </div>
                            <div>
                                <?= $this->Form->text('clinic_doctors[' . $key . '].position', [
                                    'class' => 'form-control doctor-position',
                                    'placeholder' => __('STR_ENTER_POSITION'),
                                    'default' => $value->position,
                                ]) ?>
                            </div>
                            <div class="radio">
                                <label>
                                    <input type="radio" name="is_priority" <?= ($value->is_priority) ? 'checked="checked"' : '' ?>/>
                                    <?= $this->Form->hidden('clinic_doctors[' . $key . '].is_priority', [
                                        'value' => ($value->is_priority) ? 1 : 0,
                                    ]) ?>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-1">
                <?= $this->Form->button('<i class="fa fa-trash" aria-hidden="true"></i>', [
                     'type' => 'button',
                     'class' => 'btn btn-rm-position btn-delete',
                     'escape' => false,
                ]) ?>
            </div>
        </div>
    <?php endif; endforeach; ?>
<?php endif; ?>
