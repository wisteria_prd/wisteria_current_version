
<section>
    <?php
    $result_set = $this->Paginator->counter('{{count}}');
    //END OF CURREN PAGE RECORD
    $end = $this->Paginator->counter('{{end}}');
    //START OF CURRENT PAGE RECORD
    $start = $this->Paginator->counter('{{start}}');
    echo __('DISPLAY_ITEM_INFO {0} DISPLAY_TOTAL {1}', [
        $start . __('SIGN_HYPHEN') . $end, $result_set
    ]);
    ?>
</section>
