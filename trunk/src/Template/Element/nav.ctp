<?php
$affiliation_class = $this->request->session()->read('Auth.User.affiliation_class');
$actived = $this->request->session()->read('Auth.User.activated');
?>
<nav class="navbar-admin navbar navbar-default navbar-fixed-top">
    <div class="container-fluid">
        <div class="navbar-header">
            <?php
            $home_url = '#';
            if ($actived == 1) {
                $home_url = [
                    'controller' => 'Home',
                    'action' => 'index'
                ];
            }
            echo $this->Html->link(
                $this->Html->image('/img/wisteria_logo.png',['alt' => __('SYSTEM_SERVICE_NAME'), 'class' => 'admin_logo']),
                $home_url,
                [
                    'class' => 'navbar-brand',
                    'escape' => false,
                ]
            );
            echo '<p>'. __('SYSTEM_SERVICE_NAME') . '</p>';
            ?>
        </div>
        <ul class="nav navbar-nav navbar-right">

            <?php
            $menuControls = $menuControls->nest('id', 'parent_id')->toList();
            echo $this->Navigation->show($menuControls);
            ?>

            <li class="dropdown">
                <?php $name = $this->request->session()->read('Auth.User.lastname').' '.$this->request->session()->read('Auth.User.firstname'); ?>
                <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <?php echo $name ?> <span class="caret"></span>
                </a>
                <ul class="dropdown-menu">
                    <li>
                        <a href="javascript:void(0);" class="change-password">
                            <?php echo __('パスワード変更'); ?>
                        </a>
                    </li>
                    <li>
                        <?php
                        echo $this->Html->link(__('ログアウト'), [
                            'controller' => 'Users',
                            'action' => 'logout'
                        ]);
                        ?>
                    </li>
                </ul>
            </li>
        </ul>
    </div>
</nav>