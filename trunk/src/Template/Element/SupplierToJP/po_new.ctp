
<style>
.custom-wrap {
    padding: 0px;
    margin: 0px;
}

.custom-wrap li {
    list-style: none;
    margin-bottom: 6px;
}

.custom-wrap li span {
    font-size: inherit;
    font-weight: initial;
    line-height: inherit;
}

.custom-wrap li span:first-child {
    float: left;
    cursor: pointer;
}

.custom-wrap li span:last-child {
    float: right;
}

.custom-wrap li::after {
    content: '';
    display: block;
    clear: both;
}

.custom-wrap li .add-pdf-file,
.custom-wrap li .rm-pdf-file {
    cursor: pointer;
}
</style>

<?php
$options = [];
$currentStatus = '';
$step = 0;
$buttonName = __('TXT_REGISTER');
$btn_pdf = ' style="display:none;"';

echo $this->Form->create(null, [
    'class' => 'form-horizontal',
    'onsubmit' => 'return false;',
    'name' => 'sale_status',
]);

switch ($data->status) {
    case PO_STATUS_REQUESTED_SHIPPING :
        $step = 4;
        $currentStatus = __('TXT_REQUESTED_SHIPMENT');
        $buttonName = __('TXT_RECEIVE_PAYMENT');
        $options = [
            PO_STATUS_REQUESTED_CC => __('TXT_DELIVERING'),
        ];
        break;
    case PO_STATUS_CUSTOM_CLEARANCE :
        $step = 5;
        $buttonName = __('TXT_DOWNLOAD');
        $currentStatus = __('TXT_CUSTOM_CLEARANCE');
        $options = [
            PO_STATUS_REQUESTED_CC => __('TXT_DELIVERING'),
        ];
        break;
    case PO_STATUS_REQUESTED_CC :
        $step = 6;
        $btn_pdf = ' style="display:block;"';
        $buttonName = __('TXT_SEND_PO');
        $currentStatus = __('TXT_DELIVERING');
        $options = [
            PO_STATUS_DELIVERED => __('TXT_DELIVERED'),
        ];
        break;
    case PO_STATUS_DELIVERED :
        $step = 7;
        $currentStatus = __('TXT_DELIVERED');
        break;
    default :
        $step = 3;
        $currentStatus = __('TXT_DRAFT');
        $options = [
            PO_STATUS_REQUESTED_SHIPPING => __('TXT_REQUESTED_SHIPMENT'),
            STATUS_CANCEL => __('TXT_CANCEL'),
        ];
        echo $this->Form->hidden('issue_date', [
            'value' => $data->issue_date ? date('Y-m-d', strtotime($data->issue_date)) : date('Y-m-d'),
        ]);
}

$this->Form->templates([
    'inputContainer' => '<div class="col-sm-10">{{content}}</div>',
]);
echo $this->Form->hidden('id', ['value' => $data->id]);
echo $this->Form->hidden('step', ['value' => $step]);
echo $this->Form->hidden('action', ['value' => $this->request->action]);
?>
<div class="form-group" style="margin-bottom: 10px;">
    <label class="col-sm-5 control-label text-left" style="padding-top: 0px;"><?= __('TXT_CURRENT_STATUS') ?></label>
</div>
<div class="form-group">
    <div class="col-sm-4">
        <label class="control-label status label-draft" style="width: 100%; text-align: center !important; padding: 7px 0px;">
            <?= $currentStatus ?>
        </label>
    </div>
    <?php
    if ($data->status !== PO_STATUS_DELIVERED) {
        echo '<div class="col-sm-2" style="position: relative;"><span class="glyphicon glyphicon-arrow-right arrow-draft"></span></div>';
        echo $this->Form->input('status', [
            'class' => 'form-control',
            'label' => false,
            'disabled' => ($data->status === STATUS_CANCEL) ? true : false,
            'options' => $options,
            'templates' => [
                'inputContainer' => '<div class="col-sm-6">{{content}}</div>',
            ]
        ]);
    } else {
        echo '<span>After 7days change to ‘completed’ auto</span>';
    }
    ?>
</div>
<div class="status-wrap">
    <?php
    switch ($step) {
        case 6 :
            // Delivery Form
            $delivery = null;
            $deliveryId = -1;

            if ($data->sale_deliveries) {
                $delivery = $data->sale_deliveries[0]->delivery;
                $deliveryId = $delivery->id;
            }

            echo $this->Form->create(null, [
                'class' => 'form-horizontal',
                'role' => 'form',
                'name' => 'delivery_form',
            ]);
            $this->Form->templates([
                'inputContainer' => '<div class="col-sm-1"></div><div class="col-sm-6">{{content}}</div>',
            ]);
            echo $this->Form->hidden('delivery_id', [
                'value' => $deliveryId,
                'id' => 'delivery-id',
            ]);
            echo '<div class="form-group">' .
                '<label class="col-sm-5 control-label text-left">' . __('TXT_TRACKING_NUMBER') . '</label>';
            echo $this->Form->input('tracking', [
                'label' => false,
                'required' => false,
                'class' => 'form-control',
                'placeholder' => __('TXT_TRACKING_NUMBER'),
                'value' => $delivery ? $delivery->tracking : null,
            ]);
            echo '</div>';

            echo '<div class="form-group">' .
                '<label class="col-sm-5 control-label text-left">' . __('TXT_DELIVERY_DATE') . '</label>';
            echo $this->Form->input('delivery_date', [
                'label' => false,
                'required' => false,
                'class' => 'form-control',
                'value' => $delivery ? date('Y-m-d', strtotime($delivery->delivery_date)) : date('Y-m-d'),
            ]);
            echo '</div>';

            echo '<div class="form-group"><div class="col-sm-12 text-right">';
            echo $this->Form->button(__('TXT_UPDATE'), [
                'type' => 'button',
                'class' => 'btn btn-sm btn-primary btn-update-delivery',
            ]);
            echo '</div></div>';

            echo $this->Form->end();

            // list pdf file
            $element = '';
            $fileName = '';
            $docType = '';
            $className = 'browse-file';
            $type = 'new';
            $id = 0;

            if ($medias) {
                $className = 'browse-file-edit';
                $type = 'edit';

                foreach ($medias as $key => $value) {
                    if ($key != 0) {
                        $element .= '<li data-id="' . $value->id . '" data-name="' . $value->file_name . '" data-document-type="' . $value->document_type . '" data-type="' . $type . '"><span class="label label-success ' . $className . '"><i class="fa fa-times rm-pdf-file" aria-hidden="true"></i>&nbsp;&nbsp;Browse File</span><span>&nbsp;</span></li>';
                    } else {
                        $fileName = $value->file_name;
                        $id = $value->id;
                        $docType = $value->document_type;
                    }
                }
            }

            echo '<div class="form-group">' .
            '<label class="col-sm-6 control-label text-left">' . __('TXT_CUSTOM_CLEARANCE') . '</label>' .
            '<div class="col-sm-6 col-md-6"><ul class="custom-wrap">' .
            '<li data-document-type="' . $docType . '" data-name="' . $fileName . '" data-id="' . $id . '" data-type="' . $type . '"><span class="label label-success ' . $className . '">&nbsp;&nbsp;Browse File</span><span class="label label-primary add-pdf-file">Add</span></li>' .
            $element . '</ul></div></div>';

            break;
    }
    ?>
</div>
<div class="form-group">
    <div class="col-sm-6">
        <?php
        if (($step == 3)) {
            // requested packing
            $payment = null;
            if ($payments) {
                $payment = $payments[0];
            }
            echo __('TXT_PAYMENT_INFO') . '&nbsp&nbsp<span class="label label-success" style="font-size: small;">' . $payment . '</span>';
        }
        if (($step == 5)) {
            echo $this->Form->button(__('TXT_UPDATE'), [
                'type' => 'button',
                'class' => 'btn btn-sm btn-primary update-sub-status',
            ]);
        }
        ?>
    </div>
    <div class="col-sm-6">
        <?php
        $disabled = false;
        if ((count($payments) == 0) || ($data->status === STATUS_CANCEL)) {
            $disabled = true;
        }

        if (($step == 6) && (empty($data->sale_deliveries))) {
            $disabled = true;
        }

        if ($step != 7) {
            echo $this->Form->button($buttonName, [
                'type' => 'submit',
                'disabled' => $disabled,
                'class' => 'btn btn-primary btn-sm btn-register pull-right',
                'data-step' => $step,
            ]);
        }
        ?>
    </div>
</div>
<?php
echo $this->Form->end();
echo $this->ActionButtons->btnPdfDownload('SaleReports', 'w-of-jp-pdf', $data->id, $btn_pdf);
?>