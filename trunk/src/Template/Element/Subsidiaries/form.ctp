
<?php $action = $this->request->action; ?>
<div class="row">
    <div class="col-sm-10 col-sm-offset-1">
        <section id="register_form">
            <?php
            echo $this->Form->create($data, [
                'role' => 'form',
                'class' => 'form-horizontal form-subsidiary',
                'name' => 'subsidiary_form'
            ]);
            $this->Form->templates([
                'inputContainer' => '{{content}}'
            ]);
            echo $this->Form->hidden('id');
            ?>
            <div class="form-group">
                <label class="col-sm-2 control-label">
                    <?php
                    echo __d('subsidiary', 'TXT_STATUS'); ?>
                </label>
                <div class="col-lg-4 col-md-4 col-sm-4 custom-select">
                    <?php
                    echo $this->Form->select('type', [
                        TYPE_NEW => __d('subsidiary', 'TXT_NEW'),
                        NORMAL_TYPE => __d('subsidiary', 'TXT_NORMAL'),
                        TYPE_CONTRACT => __d('subsidiary', 'TXT_CONTRACT'),
                        TYPE_BAD => __d('subsidiary', 'TXT_BAD'),
                        TYPE_AGENT => __d('subsidiary', 'TXT_AGENT'),
                    ], [
                        'class' => 'form-control',
                        'empty' => ['' => __d('subsidiary', 'TXT_SELECT_STATUS')],
                    ]); ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">
                    <?php
                    echo __d('subsidiary', 'TXT_OLD_CODE'); ?>
                </label>
                <div class="col-lg-4 col-md-4 col-sm-4">
                    <?php
                    echo $this->Form->text('code', [
                        'class' => 'form-control',
                        'placeholder' => __('TXT_ENTER_OLD_CODE'),
                    ]); ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">
                    <?php
                    echo __('STR_SUBSIDIARY_TYPE') .  $this->Comment->formAsterisk(); ?>
                </label>
                <div class="col-lg-4 col-md-4 col-sm-4">
                    <?php
                    echo $this->Form->text('name_en', [
                        'class' => 'form-control',
                        'placeholder' => __('TXT_ENTER_SUBSIDIARY_NAME_EN'),
                    ]);
                    ?>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4">
                    <?php
                    echo $this->Form->text('name', [
                        'class' => 'form-control',
                        'placeholder' => __('TXT_ENTER_SUBSIDIARY_NAME_JP'),
                        'required' => false
                    ]);
                    ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">
                    <?php
                    echo __d('subsidiary', 'TXT_ADDRESS'); ?>
                </label>
                <div class="col-lg-4 col-md-4 col-sm-4">
                    <?php
                    echo $this->Form->text('postal_code', [
                        'class' => 'form-control',
                        'placeholder' => __('STR_ENTER_POSTAL_CODE'),
                    ]);
                    ?>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 custom-select">
                    <?php
                    echo $this->Form->select('country_id', $countries, [
                        'class' => 'form-control select-countries',
                        'empty' => ['' => __('TXT_SELECT_COUNTRY')],
                        'label' => false,
                    ]);
                    ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">&nbsp;</label>
                <div class="col-lg-4 col-md-4 col-sm-4">
                    <?php
                    echo $this->Form->text('building_en', [
                        'class' => 'form-control',
                        'placeholder' => __('TXT_ENTER_BUILDING_IN_EN'),
                    ]);
                    ?>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4">
                    <?php
                    echo $this->Form->text('building', [
                        'class' => 'form-control',
                        'placeholder' => __('TXT_ENTER_BUILDING_IN_JP'),
                    ]);
                    ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">&nbsp;</label>
                <div class="col-lg-4 col-md-4 col-sm-4">
                    <?php
                    echo $this->Form->input('street_en', [
                        'type' => 'text',
                        'class' => 'form-control',
                        'placeholder' => __('TXT_ENTER_STREET_IN_EN'),
                        'label' => false,
                        'required' => false
                    ]);
                    ?>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4">
                    <?php
                    echo $this->Form->text('street', [
                        'class' => 'form-control',
                        'placeholder' => __('TXT_ENTER_STREET_IN_JP'),
                    ]);
                    ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">&nbsp;</label>
                <div class="col-lg-4 col-md-4 col-sm-4">
                    <?php
                    echo $this->Form->text('city_en', [
                        'class' => 'form-control',
                        'placeholder' => __('TXT_ENTER_CITY_EN'),
                    ]);
                    ?>
                </div>
                    <div class="col-lg-4 col-md-4 col-sm-4">
                        <?php
                        echo $this->Form->text('city', [
                            'class' => 'form-control',
                            'placeholder' => __('TXT_ENTER_CITY_JP'),
                        ]);
                        ?>
                    </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">&nbsp;</label>
                <div class="col-lg-4 col-md-4 col-sm-4">
                    <?php
                    echo $this->Form->text('prefecture_en', [
                        'class' => 'form-control',
                        'placeholder' => __('TXT_PREFECTURE_EN'),
                    ]);
                    ?>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4">
                    <?php
                    echo $this->Form->text('prefecture', [
                        'class' => 'form-control',
                        'placeholder' => __('TXT_ENTER_PREFECTURE_IN_JP'),
                    ]); ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">
                    <?php echo __('SUPPLIER_TEL') ?>
                </label>
                <div class="col-md-4 col-lg-4 col-sm-4">
                    <?php
                    $tel = '';
                    $tel_extension = '';
                    if (isset($data->info_details) && $data->info_details) {
                        $tel = $data->info_details[0]->tel;
                        $tel_extension = $data->info_details[0]->tel_extension;
                    }
                    echo $this->Form->tel('info_details[0].tel', [
                        'class' => 'form-control multiple-tel',
                        'placeholder' => __('SUPPLIER_ENTER_TEL'),
                        'value' => $tel,
                    ]);
                    echo $this->Form->hidden('info_details[0].type', [
                        'value' => TYPE_SUBSIDIARY,
                    ]);
                    ?>
                </div>
                <div class="col-md-4 col-lg-4 col-sm-4">
                    <?php
                    echo $this->Form->text('info_details[0].tel_extension', [
                        'class' => 'form-control',
                        'placeholder' => __('STR_TEL_EXT'),
                        'value' => $tel_extension,
                    ]);
                    ?>
                </div>
                <div class="col-sm-1">
                    <?php
                    echo $this->Form->button('<i class="fa fa-plus" aria-hidden="true"></i>', [
                        'type' => 'button',
                        'class' => 'btn btn-sm btn-primary btn-tel-add form-btn-add',
                        'escape' => false,
                    ]);
                    ?>
                </div>
            </div>
            <div class="tel-box-wrap">
                <?php echo $this->element('Subsidiaries/tel_list'); ?>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">
                    <?php
                    echo __d('subsidiary', 'TXT_FAX'); ?>
                </label>
                <div class="col-lg-4 col-md-4 col-sm-4">
                    <?php
                    echo $this->Form->text('fax', [
                        'class' => 'form-control',
                        'placeholder' => __('STR_ENTER_FAX'),
                    ]);
                    ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">
                    <?php
                    echo __d('subsidiary', 'TXT_EMAIL'); ?>
                </label>
                <div class="col-lg-4 col-md-4 col-sm-4">
                    <?php
                    echo $this->Form->email('info_details.0.email', [
                        'class' => 'form-control',
                        'placeholder' => __('STR_ENTER_EMAIL'),
                        'id' => false
                    ]);
                    echo $this->Form->hidden('info_details[0][type]', [
                        'value' => TYPE_SUBSIDIARY,
                    ]);
                    ?>
                </div>
                <?php
                echo $this->Form->button('<i class="fa fa-plus" aria-hidden="true"></i>', [
                    'type' => 'button',
                    'class' => 'btn btn-sm btn-primary btn-email-add form-btn-add',
                    'escape' => false,
                ]);
                ?>
            </div>
            <div class="email-box-wrap">
                <?php echo $this->element('Subsidiaries/email_list'); ?>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">
                    <?php
                    echo __d('subsidiary', 'TXT_URL'); ?>
                </label>
                <div class="col-lg-4 col-md-4 col-sm-4">
                    <?php echo $this->Form->text('url', [
                        'class' => 'form-control',
                        'placeholder' => __('STR_ENTER_URL'),
                    ]); ?>
                </div>
            </div>
            <div class="form-group custom-select">
                <label class="col-sm-2 control-label">
                    <?php
                    echo __d('subsidiary', 'TXT_PAYMENT_TERM'); ?>
                </label>
                <div class="col-lg-4 col-md-4 col-sm-4">
                    <?php echo $this->Form->select('payment_category_id', $payment_list, [
                        'class' => 'form-control field-payment',
                        'label' => false,
                        'default' => isset($customer) ? $customer->payment_category_id : '',
                        'empty' => ['' => __('TXT_SELECT_PAYMENT')]
                    ]); ?>
                </div>
                <?php
                echo $this->Form->button('<i class="fa fa-plus" aria-hidden="true"></i>', [
                    'type' => 'button',
                    'class' => 'btn btn-sm btn-primary btn-payment-cats form-btn-add',
                    'escape' => false,
                ]);
                ?>
            </div>
            <div class="form-group custom-select">
                <label class="col-sm-2 control-label">
                    <?php
                    echo __d('subsidiary', 'TXT_INVOICING_BY'); ?>
                </label>
                <div class="col-lg-4 col-md-4 col-sm-4">
                    <?php echo $this->Form->select('invoice_delivery_method_id', $invoice_list, [
                        'class' => 'form-control field-invoice',
                        'label' => false,
                        'default' => isset($customer) ? $customer->invoice_delivery_method_id : '',
                        'empty' => ['' => __('STR_INVOICE_SELECT_DELIVERY')]
                    ]); ?>
                </div>
                <?php
                echo $this->Form->button('<i class="fa fa-plus" aria-hidden="true"></i>', [
                    'type' => 'button',
                    'class' => 'btn btn-sm btn-primary btn-invoice-methods form-btn-add',
                    'escape' => false,
                ]);
                ?>
            </div>
            <?php if (false) : ?>
            <div class="form-group custom-select">
                <label class="col-sm-2 control-label">
                    <?php echo __('STR_SUBSIDIARY_TYPE') ?>
                </label>
                <div class="col-lg-4 col-md-4 col-sm-4">
                    <?php
                    echo $this->Form->select('type', $types, [
                        'class' => 'form-control',
                        'empty' => ['' => __d('subsidiary', 'TXT_SELECT_CLINIC')],
                        'id' => 'select-type',
                    ]);
                    ?>
                </div>
            </div>
            <?php endif; ?>
            <div class="form-group">
                <label class="col-sm-2 control-label">si_fax</label>
                <div class="col-lg-4 col-md-4 col-sm-4">
                    <?php
                    echo $this->Form->text('si_fax', [
                        'class' => 'form-control',
                        'placeholder' => 'si_fax',
                    ]);
                    ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">si_email</label>
                <div class="col-lg-4 col-md-4 col-sm-4">
                    <?php
                    echo $this->Form->email('si_email', [
                        'class' => 'form-control',
                        'placeholder' => 'si_email',
                    ]);
                    ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">si_postal_service</label>
                <div class="col-lg-4 col-md-4 col-sm-4">
                    <?php
                    echo $this->Form->text('si_postal_service', [
                        'class' => 'form-control',
                        'placeholder' => 'si_postal_service',
                    ]);
                    ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">si_courier_service</label>
                <div class="col-lg-4 col-md-4 col-sm-4">
                    <?php
                    echo $this->Form->text('si_courier_service', [
                        'class' => 'form-control',
                        'placeholder' => 'si_courier_service',
                    ]);
                    ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">si_faxandemail</label>
                <div class="col-lg-4 col-md-4 col-sm-4">
                    <?php
                    echo $this->Form->email('si_faxandemail', [
                        'class' => 'form-control',
                        'placeholder' => 'si_faxandemail',
                    ]);
                    ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">si_faxandpostal_service</label>
                <div class="col-lg-4 col-md-4 col-sm-4">
                    <?php
                    echo $this->Form->text('si_faxandpostal_service', [
                        'class' => 'form-control',
                        'placeholder' => 'si_faxandpostal_service',
                    ]);
                    ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">si_emailandpostal_service</label>
                <div class="col-lg-4 col-md-4 col-sm-4">
                    <?php
                    echo $this->Form->text('si_emailandpostal_service', [
                        'class' => 'form-control',
                        'placeholder' => 'si_emailandpostal_service',
                    ]);
                    ?>
                </div>
            </div>
            <div class="form-group custom-select">
                <label class="col-sm-2 control-label"></label>
                <div class="col-lg-4 col-md-4 col-sm-4">
                    <?php
                    echo $this->Form->select('external_id', [], [
                        'class' => 'form-control',
                        'id' => 'person-name',
                        'empty' => ['' => __('TXT_SELECT_TYPE')],
                        'default' => $this->request->query('external_id'),
                        'readonly' => true
                    ]);
                    ?>
                </div>
                <?php
                echo $this->Form->button('<i class="fa fa-plus" aria-hidden="true"></i>', [
                    'type' => 'button',
                    'class' => 'btn btn-sm btn-primary form-btn-add btn-add-c',
                    'escape' => false,
                ]);
                ?>
            </div>
            <div class="customer-wrap">
                <?php
                if ($data->customer_subsidiaries) {
                    echo $this->element('Subsidiaries/customer_list');
                }
                ?>
            </div>

            <div class="clearfix"></div>
            <div class="form-group">
                <?php
                echo $this->ActionButtons->btnSaveCancel('Subsidiaries', $action);
                ?>
            </div>
            <?php echo $this->Form->end(); ?>
        </section>
    </div>
</div>
<?php
echo $this->Html->script([
    'subsidiary',
], ['block' => 'script']);
