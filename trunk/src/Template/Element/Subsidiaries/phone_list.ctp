<?php if (isset($data) && $data->info_details): ?>
    <?php foreach ($data->info_details as $key => $value): ?>
        <?php if (($value->phone !== null) && !empty($value->phone) && ($key != 0)): ?>
            <div class="form-group">
                <label class="col-sm-4 control-label"></label>
                <div class="col-sm-7 col-md-7">
                    <?php
                    echo $this->Form->tel('info_details[' . $key . '].phone', [
                        'class' => 'form-control',
                        'placeholder' => __('TXT_ENTER_PHONE'),
                        'default' => $value->phone
                    ]);
                    echo $this->Form->hidden('info_details[' . $key . '].type', [
                        'value' => TYPE_SUBSIDIARY,
                    ]);
                    ?>
                </div>
                <?= $this->Form->button('<i class="fa fa-trash" aria-hidden="true"></i>', [
                    'type' => 'button',
                    'class' => ' btn btn-delete btn-sm form-btn-add btn-remove-field',
                    'escape' => false,
                ]) ?>
            </div>
        <?php endif; ?>
    <?php endforeach; ?>
<?php endif; ?>
