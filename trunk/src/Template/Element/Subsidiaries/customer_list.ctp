<?php if (isset($data) && $data->customer_subsidiaries): ?>
    <?php foreach ($data->customer_subsidiaries as $key => $value): ?>
        <div class="form-group custom-select">
            <label class="col-sm-2 control-label"></label>
            <div class="col-lg-4 col-md-4 col-sm-4">
                <?php
                echo $this->Form->select('customer_subsidiaries[' . $key . '].customer_id', [
                    [
                        'value' => $value->customer_id,
                        'text' => $value->customer->full_name,
                    ]
                ], [
                    'class' => 'form-control',
                ]);
                ?>
            </div>
            <?php
            echo $this->Form->button('<i class="fa fa-trash" aria-hidden="true"></i>', [
                'type' => 'button',
                'class' => ' btn btn-delete btn-sm form-btn-add btn-remove-field',
                'escape' => false,
            ]);
            ?>
        </div>
    <?php endforeach; ?>
<?php endif; ?>
