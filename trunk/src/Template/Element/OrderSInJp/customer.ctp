<div class="row row-top-space">
    <div class="col-md-10">
        <div class="seller-info" id="seller-id" data-id="<?= $sale->seller->id ?>">
            <div class="row invoice-form-row">
                <div class="col-md-12">
                    <h3 class="c-name" style="display: inline;">
                        <?php
                        // display value from table sales, for hidden update value take from table customers.
                        $btn_enable = '';
                        echo h($sale->customer_name);
                        $customer = $this->Comment->getFieldByLocal($sale->customer, $local);
                        if ($sale->customer_name === $customer) {
                            $btn_enable = ' disabled="disabled"';
                        }
                        ?>
                    </h3>
                    <button<?= $btn_enable ?> class="btn btn-sm btn-primary btn-update-customer-info check-customer-name" style="display: inline; margin-left: 15px;" data-cid="<?= $sale->customer->id ?>" data-type="name" type="button">
                        <?= __('マスタとデータ不一致')?><br/><?= __('マスタのデータを表示する') ?>
                    </button>
                </div>
            </div>
            <?= $this->Form->hidden('customer_name', [
                'class' => 'customer-name',
                'value' => $customer,
            ]); ?>
            <div class="row invoice-form-row">
                <div class="col-md-2">
                    <p><?= __('TXT_ADDRESS') ?></p>
                </div>
                <div class="col-md-10">
                    <?php
                    $c_address = '';
                    list($address, $jp_addr2, $en_addr2, $phone, $fax) = $this->Comment->addressFormat($local, $sale->customer, true);
                    if (mb_substr($address, 0, 1) == '〒') {
                        if ($sale->customer) {
                            $c_address .= $address . $jp_addr2;
                            $address .= "<br />" . $jp_addr2;
                        }
                    } else {
                        $c_address .= $address . $en_addr2;
                        $address .= "<br />" . $en_addr2;
                    }

                    $s_address = $sale->customer_address;
                    $s_address = str_replace(["\r\n", '<br />', '<br>'], [null, null, null], $s_address);
                    echo '<span class="c-address" style="float:left;">' . nl2br($sale->customer_address) . '</span>';
                    //display value from table sales, for hidden update value take from table customers.
                    echo $this->Form->hidden('customer_address', [
                        'value' => $address,
                        'class' => 'customer-address',
                    ]);

                    $enable = '';
                    if ($c_address == $s_address) {
                        $enable = ' disabled="disabled"';
                    }
                    ?>
                    <span<?= $enable ?> class="btn btn-sm btn-primary btn-update-customer-info check-customer-address" style="float:left; margin-left: 15px;" data-cid="<?= $sale->customer->id ?>" data-type="address" role="button">
                    <?= __('マスタとデータ不一致')?><br/><?= __('マスタのデータを表示する') ?>
                    </span>
                </div>
            </div>
            <div class="row invoice-form-row">
                <div class="col-md-2 custom-select">
                    <div class="invoice-form-label"><?= __('TXT_TEL') ?></div>
                </div>
                <div class="col-md-3 custom-select">
                    <?php
                    $emails = [];
                    $phones = [];
                    if ($sale->customer->info_details) {
                        foreach ($sale->customer->info_details as $info) {
                            if ($info->email) {
                                $emails[$info->email] = $info->email;
                            }
                            if ($info->phone) {
                                $phones[$info->phone] = $info->phone;
                            } else if ($info->tel) {
                                $phones[$info->tel] = $info->tel;
                            }
                        }
                    }

                    echo $this->Form->select('customer_tel', $phones, [
                        'class' => 'form-control select-tel',
                        'label' => false,
                        'default' => $sale->customer_tel,
                        'templates' => [
                            'inputContainer' => '{{content}}',
                        ],
                        'empty' => ['' => __('TXT_SELECT_TEL')]
                    ]);
                    $enable = '';
                    if ($sale->customer_fax == $fax) {
                        $enable = ' disabled="disabled"';
                    }
                    ?>
                </div>
                <div class="col-md-1" style="padding-right: 0;">
                    <div class="invoice-form-label">/ <?= __('TXT_FAX') ?></div>
                </div>
                <div class="col-md-3 field-small-padding-right">
                    <?= $this->Form->input('customer_fax', [
                        'type' => 'text',
                        'class' => 'form-control txt-fax',
                        'label' => false,
                        'placeholder' => __('TXT_ENTER_FAX'),
                        'value' => $sale->customer_fax,
                        'templates' => [
                            'inputContainer' => '{{content}}',
                        ],
                    ]); ?>
                </div>
                <div class="field-small-padding-left col-md-3">
                    <button<?= $enable ?> class="btn btn-sm btn-primary btn-update-customer-info check-customer-fax" data-cid="<?= $sale->customer->id ?>" data-type="fax" type="button">
                    <?= __('マスタとデータ不一致')?><br/><?= __('マスタのデータを表示する') ?>
                    </button>
                </div>
            </div>
            <div class="row invoice-form-row">
                <div class="col-md-2"><div class="invoice-form-label"><?= __('TXT_EMAIL') ?></div></div>
                <div class="col-md-3 custom-select">
                    <?= $this->Form->select('customer_email', $emails, [
                        'class' => 'form-control select-email',
                        'label' => false,
                        'default' => $sale->customer_email,
                        'templates' => [
                            'inputContainer' => '{{content}}',
                        ],
                        'empty' => ['' => __('TXT_SELECT_EMAIL')]
                    ]);
                    ?>
                </div>
            </div>
            <div class="row invoice-form-row">
                <div class="col-md-offset-2 col-md-9">
                    <?php
                    echo $this->Form->button(__('TXT_UPDATE'), [
                        'type' => 'button',
                        'class' => 'btn btn-primary btn-sm btn-add-info',
                    ]);
                    ?>
                    &nbsp;
                    <span class="hide info-status" id="customer-info"></span>
                </div>
            </div>
        </div>
    </div>
</div>