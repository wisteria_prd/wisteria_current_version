<?php
echo $this->Form->hidden('customer_address', [
    'value' => $sale->customer_address,
]);
echo $this->Form->hidden('customer_name', [
    'value' => $sale->customer_address,
]);
?>
<div class="row row-top-space">
    <div class="col-md-10">
        <div class="seller-info" id="seller-id" data-id="<?= $sale->seller->id ?>">
            <div class="row invoice-form-row">
                <div class="col-md-12">
                    <div class="dropdown">
                        <?php
                        $customerName = $this->Comment->getFieldByLocal($sale->customer, $local);
                        echo $this->Form->button('<span class="s_name">' . h($customerName) . '</span>' . '&nbsp;<span class="caret"></span>', [
                            'id' => 'customer-sub',
                            'class' => 'btn btn-default',
                            'data-toggle' => 'dropdown',
                            'aria-haspopup' => true,
                            'aria-expanded' => false,
                            'escape' => false,
                        ]);
                        ?>
                        <ul class="dropdown-menu select-subsidiary" aria-labelledby="customer-sub">
                            <?php if ($sale->customer->customer_subsidiaries) :
                                foreach ($sale->customer->customer_subsidiaries as $item) : ?>
                                <li data-id="<?= $item->subsidiary_id ?>">
                                    <a href="javascript:void(0);">
                                        <?= $this->Comment->getFieldByLocal($item->subsidiary, $local) ?>
                                    </a>
                                </li>
                                <?php endforeach;
                            else : ?>
                                <li data-id="-1"><a href="javascript:void(0);">Not Found</a></li>
                            <?php endif; ?>
                        </ul>
                        <button disabled="disabled" class="btn btn-sm btn-primary btn-update-customer-info check-customer-name" style="display: inline; margin-left: 15px;" data-cid="<?= $sale->customer->id ?>" data-type="name" type="button">
                            <?= __('マスタとデータ不一致')?><br/><?= __('マスタのデータを表示する') ?>
                        </button>
                    </div>
                </div>
            </div>
            <div class="row invoice-form-row">
                <div class="col-md-2">
                    <p><?= __('TXT_ADDRESS') ?></p>
                </div>
                <div class="col-md-10">
                    <?php
                    echo '<span class="c-address" style="float:left;">' . nl2br($sale->customer_address) . '</span>';
                    ?>
                    <span disabled="disabled" class="btn btn-sm btn-primary btn-update-customer-info check-customer-address" style="float:left; margin-left: 15px;" data-cid="<?= $sale->customer->id ?>" data-type="address" role="button">
                    <?= __('マスタとデータ不一致')?><br/><?= __('マスタのデータを表示する') ?>
                    </span>
                </div>
            </div>
            <div class="row invoice-form-row">
                <div class="col-md-2 custom-select">
                    <div class="invoice-form-label"><?= __('TXT_TEL') ?></div>
                </div>
                <div class="col-md-3 custom-select">
                    <?php
                    $phones = [];
                    if ($sale->customer_tel) {
                        $phones[] = [
                            'text' => $sale->customer_tel,
                            'value' => $sale->customer_tel,
                        ];
                    }
                    echo $this->Form->select('customer_tel', $phones, [
                        'class' => 'form-control select-tel',
                        'label' => false,
                        'default' => $sale->customer_tel,
                        'empty' => ['' => __('TXT_SELECT_TEL')],
                        'value' => $sale->customer_tel ? $sale->customer_tel : '',
                    ]);
                    ?>
                </div>
                <div class="col-md-1" style="padding-right: 0;">
                    <div class="invoice-form-label">/ <?= __('TXT_FAX') ?></div>
                </div>
                <div class="col-md-3 field-small-padding-right">
                    <?= $this->Form->input('customer_fax', [
                        'type' => 'text',
                        'class' => 'form-control txt-fax',
                        'label' => false,
                        'placeholder' => __('TXT_ENTER_FAX'),
                        'value' => $sale->customer_fax,
                        'templates' => [
                            'inputContainer' => '{{content}}',
                        ],
                    ]); ?>
                </div>
                <div class="field-small-padding-left col-md-3">
                    <button disabled="disabled" class="btn btn-sm btn-primary btn-update-customer-info check-customer-fax" data-cid="<?= $sale->customer->id ?>" data-type="fax" type="button">
                    <?= __('マスタとデータ不一致')?><br/><?= __('マスタのデータを表示する') ?>
                    </button>
                </div>
            </div>
            <div class="row invoice-form-row">
                <div class="col-md-2"><div class="invoice-form-label"><?= __('TXT_EMAIL') ?></div></div>
                <div class="col-md-3 custom-select">
                    <?php
                    $emails = [];
                    if ($sale->customer_email) {
                        $emails[] = [
                            'text' => $sale->customer_email,
                            'value' => $sale->customer_email,
                        ];
                    }
                    echo $this->Form->select('customer_email', $emails, [
                        'class' => 'form-control select-email',
                        'label' => false,
                        'default' => $sale->customer_email,
                        'empty' => ['' => __('TXT_SELECT_EMAIL')],
                        'value' => $sale->customer_email ? $sale->customer_email : '',
                    ]);
                    ?>
                </div>
            </div>
            <div class="row invoice-form-row">
                <div class="col-md-offset-2 col-md-9">
                    <?= $this->Form->button(__('TXT_UPDATE'), [
                        'type' => 'button',
                        'class' => 'btn btn-primary btn-sm update-subsidiary',
                    ]) ?>
                </div>
            </div>
        </div>
    </div>
</div>