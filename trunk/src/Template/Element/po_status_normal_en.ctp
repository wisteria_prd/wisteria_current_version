
<style>
.custom-wrap {
    padding: 0px;
    margin: 0px;
}

.custom-wrap li {
    list-style: none;
    margin-bottom: 6px;
}

.custom-wrap li span {
    font-size: inherit;
    font-weight: initial;
    line-height: inherit;
}

.custom-wrap li span:first-child {
    float: left;
    cursor: pointer;
}

.custom-wrap li span:last-child {
    float: right;
}

.custom-wrap li::after {
    content: '';
    display: block;
    clear: both;
}

.custom-wrap li .add-pdf-file,
.custom-wrap li .rm-pdf-file {
    cursor: pointer;
}
</style>
<?php
$options = [
    STATUS_PAID => $statusList[STATUS_PAID],
    STATUS_CANCEL => $statusList[STATUS_CANCEL],
];
$step = 0;
$buttonName = __('TXT_SUBMIT');
echo $this->Form->create(null, [
    'class' => 'form-horizontal',
    'onsubmit' => 'return false;',
    'name' => 'sale_status',
]);
// CHECK STATUS CHANGE
$currentStatus = $statusList[$sale->status];
switch ($sale->status) {
    case PO_STATUS_ISSUE_ROP_OR_WINV :
        $step = 2;
        break;
    case STATUS_PAID :
        $step = 3;
        break;
    case PO_STATUS_REQUESTED_SHIPPING :
        $step = 4;
        break;
    case PO_STATUS_CUSTOM_CLEARANCE :
        $step = 5;
        break;
    case PO_STATUS_REQUESTED_CC :
        $step = 6;
        break;
    case PO_STATUS_DELIVERED :
        $step = 7;
        break;
    case PO_STATUS_COMPLETED :
        $step = 8;
        break;
    default :
        $step = 1;
        echo $this->Form->hidden('issue_date', [
            'value' => $sale->issue_date ? date('Y-m-d', strtotime($sale->issue_date)) : date('Y-m-d'),
        ]);
}
$this->Form->templates([
    'inputContainer' => '<div class="col-sm-10">{{content}}</div>',
]);
echo $this->Form->hidden('id', ['value' => $sale->id]);
echo $this->Form->hidden('step', ['value' => $step]);
echo $this->Form->hidden('action', ['value' => $this->request->action]);
echo $this->Form->hidden('po_type', ['value' => 'normal']);
?>
<?php // CHECK CUSTOMER CONTRACT TYPE AND SALE STATUS PAID ?>
<?php if ($step >= 3) : ?>
    <div class="form-group" style="margin-bottom: 10px;">
        <label class="col-sm-4 control-label text-left" style="padding-top: 0px;">
            <?= __('TXT_CURRENT_STATUS') ?>
        </label>
        <label class="col-sm-2 col-md-2"></label>
        <label class="col-sm-5 col-md-5" style="margin-bottom: 0px;">
            <?= __('TXT_SUB_STATUS') ?>
        </label>
    </div>
    <div class="form-group">
        <div class="col-sm-4">
            <label class="control-label status label-draft" style="width: 100%; text-align: center !important; padding: 7px 0px;">
                <?= $currentStatus ?>
            </label>
        </div>
        <div class="col-sm-2" style="position: relative;">
            <span class="glyphicon glyphicon-arrow-right arrow-draft"></span>
        </div>
        <div class="col-sm-6 col-md-6">
            <?php
            echo $this->Form->input('sub_status', [
                'type' => 'select',
                'label' => false,
                'required' => false,
                'class' => 'form-control',
                'value' => $sale->sub_status,
                'options' => [
                    '' => __('TXT_SELECT_STATUS'),
                    STATUS_PACKED => __('TXT_PACKED'),
                    STATUS_SHIPPED => __('TXT_SHIPPED'),
                ],
                'templates' => [
                    'inputContainer' => '{{content}}',
                ]
            ]);
            ?>
        </div>
    </div>
    <div class="form-group">
        <div class="col-sm-6"></div>
        <div class="col-sm-6">
            <?php
            echo $this->Form->button($buttonName, [
                'type' => 'submit',
                'class' => 'btn btn-primary update-sub-status pull-right',
                'data-step' => $step,
            ]);
            ?>
        </div>
    </div>
<?php else : ?>
    <div class="form-group" style="margin-bottom: 10px;">
        <label class="col-sm-4 control-label text-left" style="padding-top: 0px;">
            <?= __('TXT_CURRENT_STATUS') ?>
        </label>
    </div>
    <div class="form-group">
        <div class="col-sm-4">
            <label class="control-label status label-draft" style="width: 100%; text-align: center !important; padding: 7px 0px;">
                <?= $currentStatus ?>
            </label>
        </div>
    </div>
<?php endif; ?>
<?php echo $this->Form->end(); ?>
