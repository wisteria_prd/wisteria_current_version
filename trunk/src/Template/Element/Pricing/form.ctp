
<div class="col-sm-12">
    <?php
    echo $this->Form->create($entity, [
        'role' => 'form',
        'class' => 'form-horizontal from-pricing',
        'name' => 'common_form'
    ]);
    $this->Form->templates([
        'inputContainer' => '{{content}}'
    ]); ?>
    <div class="form-group custom-select">
        <label class="col-sm-2 control-label">
            <?php
            echo __d('pricing', 'TXT_EXTERNAL_TYPE') . $this->Comment->formAsterisk(); ?>
        </label>
        <div class="col-sm-4">
            <?php
            echo $this->Form->select('external_id', [], [
                'class' => 'form-control',
                'label' => false,
                'required' => false,
                'id' => 'seller-id',
                'empty' => ['' => __d('pricing', 'TXT_SELECT_SUPPLIER')],
            ]); ?>
        </div>
    </div>
    <div class="form-group custom-select">
        <label class="col-sm-2 control-label">
            <?php
            echo __d('pricing', 'TXT_SUPPLIER') . $this->Comment->formAsterisk(); ?>
        </label>
        <div class="col-sm-4">
            <?php
            echo $this->Form->select('type', [], [
                'class' => 'form-control',
                'label' => false,
                'required' => false,
                'empty' => ['' => __d('pricing', 'TXT_SELECT_SUPPLIER')],
            ]); ?>
        </div>
    </div>
    <div class="form-group custom-select">
        <label class="col-sm-2 control-label">
            <?php echo __d('pricing', 'TXT_MANUFACTURER') . $this->Comment->formAsterisk() ?>
        </label>
        <div class="col-sm-4">
            <?php
            echo $this->Form->select('type', [], [
                'class' => 'form-control',
                'label' => false,
                'required' => false,
                'empty' => ['' => __d('pricing', 'TXT_SELECT_MANUFACTURER')],
                'id' => 'pricing-type'
            ]); ?>
        </div>
    </div>
    <div class="form-group custom-select">
        <label class="col-sm-2 control-label">
            <?php echo __d('pricing', 'TXT_PRODUCT') . $this->Comment->formAsterisk() ?>
        </label>
        <div class="col-sm-4">
            <?php
            echo $this->Form->input('type', [
                'class' => 'form-control',
                'label' => false,
                'required' => false,
                'placeholder' => ['' => __d('pricing', 'TXT_ENTER_PRODUCT_NAME')],
            ]); ?>
        </div>
    </div>
    <div class="form-group custom-select">
        <label class="col-sm-2 control-label">
            <?php echo __d('pricing', 'TXT_PRICE_TYPE') . $this->Comment->formAsterisk() ?>
        </label>
        <div class="col-sm-4">
            <?php
            echo $this->Form->select('type', [
                'class' => 'form-control',
                'label' => false,
                'required' => false,
                'empty' => ['' => __d('pricing', 'TXT_SELECT_PRICE_TYPE')],
                'id' => 'pricing-type'
            ]); ?>
        </div>
    </div>
    <div class="form-group custom-select">
        <label class="col-sm-2 control-label">
            <?php echo __d('pricing', 'TXT_PRICE_CURRENCY') . $this->Comment->formAsterisk() ?>
        </label>
        <div class="col-sm-4">
            <?php
            echo $this->Form->select('type', [
                'class' => 'form-control',
                'label' => false,
                'required' => false,
                'empty' => ['' => __d('pricing', 'TXT_SELECT_CURRENCY')],
                'id' => 'pricing-type'
            ]); ?>
        </div>
    </div>
    <div class="form-group custom-select">
        <label class="col-sm-2 control-label">
            <?php echo __d('pricing', 'TXT_PRICE') . $this->Comment->formAsterisk() ?>
        </label>
        <div class="col-sm-4">
            <?php
            echo $this->Form->input('type', [
                'class' => 'form-control',
                'label' => false,
                'required' => false,
                'placeholder' => ['' => __d('pricing', 'TXT_SELECT_ENTER_PRICE')],
            ]); ?>
        </div>
    </div>
    <div class="form-group custom-select">
        <label class="col-sm-2 control-label">
            <?php echo __d('pricing', 'TXT_RANGE_TYPE') . $this->Comment->formAsterisk() ?>
        </label>
        <div class="col-sm-4">
            <?php
            echo $this->Form->select('type', [
                'class' => 'form-control',
                'label' => false,
                'required' => false,
                'empty' => ['' => __d('pricing', 'TXT_SELECT_RANG_TYPE')],
                'id' => 'pricing-type'
            ]); ?>
        </div>
    </div>
    <div class="form-group custom-select">
        <label class="col-sm-2 control-label">
            <?php echo __d('pricing', 'TXT_RANGE_CURRENCY') . $this->Comment->formAsterisk() ?>
        </label>
        <div class="col-sm-4">
            <?php
            echo $this->Form->select('type', [
                'class' => 'form-control',
                'label' => false,
                'required' => false,
                'empty' => ['' => __d('pricing', 'TXT_SELECT_RANG_CURRENCY')],
                'id' => 'pricing-type'
            ]); ?>
        </div>
    </div>
    <div class="form-group custom-select">
        <label class="col-sm-2 control-label">
            <?php echo __d('pricing', 'TXT_RANGE_FROM') . $this->Comment->formAsterisk() ?>
        </label>
        <div class="col-sm-4">
            <?php
            echo $this->Form->input('type', [
                'class' => 'form-control',
                'label' => false,
                'required' => false,
                'placeholder' => ['' => __d('pricing', 'TXT_ENTER_RANG_FROM')],
            ]); ?>
        </div>
    </div>
    <div class="form-group custom-select">
        <label class="col-sm-2 control-label">
            <?php echo __d('pricing', 'TXT_RANGE_TO') . $this->Comment->formAsterisk() ?>
        </label>
        <div class="col-sm-4">
            <?php
            echo $this->Form->input('type', [
                'class' => 'form-control',
                'label' => false,
                'required' => false,
                'placeholder' => ['' => __d('pricing', 'TXT_ENTER_RANG_TO')],
            ]); ?>
        </div>
    </div>

    <div class="form-group">
        <div class="col-sm-12 text-center">
            <?php echo $this->Form->button(__d('pricing', 'TXT_CANCEL'), [
                'type' => 'button',
                'class' => 'btn btn-default form_btn btn-width btn-sm',
                'id' => 'btn-clear',
            ]); ?>
            &nbsp; &nbsp;
            <?php
            echo $this->Form->button(__d('pricing', 'TXT_REGISTER'), [
                'type' => 'submit',
                'class' => 'btn btn-primary form_btn btn-width btn-sm',
                'id' => 'btn-register',
            ]);
            ?>
        </div>
    </div>
    <?php
    echo $this->Form->end(); ?>
</div>

<script>
    $(function(e) {
        $('select[name="external_id"]').select2({
            width: '100%',
            minLength: 0,
            ajax: {
                url: BASE + 'PersonInCharges/getPersonInChargeByType',
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    let query = {
                        keyword: params.term,
                        type: $('body').find('select[name="type"]').val()
                    };
                    return query;
                },
                processResults: function (data) {
                    let dataSource = [];
                    if (data.data === null && data.data === 'undefined') {
                        return false;
                    }
                    $.each(data.data, function(i, v) {
                        dataSource.push({
                            id: v.id,
                            text: v.full_name
                        });
                    });
                    return { results: dataSource };
                }
            }
        });
    });
</script>
