
<div class="paginator">
    <ul class="pagination">
        <?= $this->Paginator->first('<< ' . __('BTN_FIRST')) ?>
        <?= $this->Paginator->prev('< ' . __('BTN_PREV')) ?>
        <?= $this->Paginator->numbers() ?>
        <?= $this->Paginator->next(__('BTN_NEXT') . ' >') ?>
        <?= $this->Paginator->last(__('BTN_LAST') . ' >>') ?>
    </ul>
    <p>
    <?php
    $page = $this->Paginator->counter('{{page}}');
    $pages = $this->Paginator->counter('{{pages}}');
    $current = $this->Paginator->counter('{{current}}');
    $count = $this->Paginator->counter('{{count}}');
    echo $this->Paginator->counter([
        'format' => __('PGN_PAGE {0} PGN_OF {1}: {2} PGN_REGCORD_OF {3}', [
            $page, $pages, $current, $count
        ])
    ]);
    ?>
    </p>
</div>