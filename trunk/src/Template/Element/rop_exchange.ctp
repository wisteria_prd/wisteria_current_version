<div class="row row-bottom-space">
    <div class="col-md-10 col-md-offset-1">
        <?php
        echo $this->Form->create(null, [
            'autocomplete' => 'off',
            'class' => 'form-exchange'
        ]
        );
        echo $this->Form->hidden('id', [
            'value' => $data->id,
            'id' => 'sale-exchange-id'
        ]); //sale id
        ?>
        <table class="table-xrate">
            <tr>
                <td>
                    <div class="input-group with-datepicker">
                        <?= $this->Form->input('currency_date', [
                            'class' => 'form-control x-currency-date',
                            'placeholder' => __('yyyy/m/d'),
                            'value' => !empty($data->currency_date) ? date('Y/m/d', strtotime($data->currency_date)) : date('Y/m/d'),
                            'label' => false
                        ]);
                        ?>
                        <div class="input-group-btn">
                            <button type="button" class="btn btn-default trigger-calendar" aria-label="Calendar">
                                <span class="glyphicon glyphicon-calendar"></span>
                            </button>
                        </div>
                    </div>
                </td>
                <td>&nbsp; 付 &nbsp;</td>
                <td>
                    <?= __('三菱東京UFJ銀行TTSレート') ?>
                </td>
                <td>
                    &nbsp; 1<?= $data->currency->code ?>&nbsp;
                </td>
                <td>
                    <?= $this->Form->input('currency_exchange_rate', [
                        'class' => 'form-control x-currency-format',
                        'value' => $data->currency_exchange_rate ? $this->Number->currency($data->currency_exchange_rate, null, ['pattern' => '#,###']) : '',
                        'placeholder' => __('#,##0'),
                        'label' => false
                    ]);
                    ?>
                </td>
                <td>
                    &nbsp; 円 &nbsp;
                </td>
                <td>
                    <?php
                    echo $this->Form->button(__('TXT_UPDATE'), [
                        'type' => 'button',
                        'class' => 'btn btn-primary btn-sm btn-update-xrate',
                    ]);
                    ?>
                </td>
            </tr>
            <tr>
                <td>
                    <span class="help-block help-tips" id="error-currency_date"></span>
                </td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>
                    <span class="help-block help-tips" id="error-currency_exchange_rate"></span>
                </td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
        </table>
        <?= $this->Form->end(); ?>
    </div>
</div>