
<style>
.custom-wrap {
    padding: 0px;
    margin: 0px;
}

.custom-wrap li {
    list-style: none;
    margin-bottom: 6px;
}

.custom-wrap li span {
    font-size: inherit;
    font-weight: initial;
    line-height: inherit;
}

.custom-wrap li span:first-child {
    float: left;
    cursor: pointer;
}

.custom-wrap li span:last-child {
    float: right;
}

.custom-wrap li::after {
    content: '';
    display: block;
    clear: both;
}

.custom-wrap li .add-pdf-file,
.custom-wrap li .rm-pdf-file {
    cursor: pointer;
}
</style>
<?php
$options = [];
$step = 0;
$buttonName = __('TXT_SUBMIT');
echo $this->Form->create(null, [
    'class' => 'form-horizontal',
    'onsubmit' => 'return false;',
    'name' => 'sale_status',
]);
// CHECK STATUS CHANGE
$currentStatus = $statusList[$sale->status];
switch ($sale->status) {
    case PO_STATUS_ISSUE_ROP_OR_WINV :
        $step = 2;
        $options = [
            STATUS_PAID => $statusList[STATUS_PAID],
            STATUS_CANCEL => $statusList[STATUS_CANCEL],
        ];
        break;
    case STATUS_PAID :
        $step = 3;
        $buttonName = __('TXT_SEND_PO');
        $options = [
            PO_STATUS_REQUESTED_SHIPPING => $statusList[PO_STATUS_REQUESTED_SHIPPING],
        ];
        break;
    case PO_STATUS_REQUESTED_SHIPPING :
        $step = 4;
        $buttonName = __('TXT_SUBMIT');
        $options = [
            PO_STATUS_CUSTOM_CLEARANCE => $statusList[PO_STATUS_CUSTOM_CLEARANCE],
        ];
        break;
    case PO_STATUS_CUSTOM_CLEARANCE :
        $step = 5;
        $options = [
            PO_STATUS_REQUESTED_CC => $statusList[PO_STATUS_REQUESTED_CC],
        ];
        break;
    case PO_STATUS_REQUESTED_CC :
        $step = 6;
        $options = [
            PO_STATUS_DELIVERED => $statusList[PO_STATUS_DELIVERED],
        ];
        break;
    case PO_STATUS_DELIVERED :
        $step = 7;
        $options = [
            STATUS_COMPLETED => $statusList[STATUS_COMPLETED],
        ];
        break;
    case PO_STATUS_COMPLETED :
        $step = 8;
        break;
    default :
        $step = 1;
        $options = [
            PO_STATUS_ISSUE_ROP_OR_WINV => $statusList[PO_STATUS_ISSUE_ROP_OR_WINV],
            STATUS_CANCEL => $statusList[STATUS_CANCEL],
        ];
        echo $this->Form->hidden('issue_date', [
            'value' => $sale->issue_date ? date('Y-m-d', strtotime($sale->issue_date)) : date('Y-m-d'),
        ]);
}
$this->Form->templates([
    'inputContainer' => '<div class="col-sm-10">{{content}}</div>',
]);
echo $this->Form->hidden('id', ['value' => $sale->id]);
echo $this->Form->hidden('step', ['value' => $step]);
echo $this->Form->hidden('action', ['value' => $this->request->action]);
echo $this->Form->hidden('po_type', ['value' => 'normal']);
?>
<?php // CHECK STATUS PAID ?>
<?php if (($sale->status === STATUS_PAID) && ($sale->sub_status !== STATUS_SHIPPED)) : ?>
    <div class="form-group" style="margin-bottom: 10px;">
        <label class="col-sm-4 control-label text-left" style="padding-top: 0px;">
            <?= __('TXT_CURRENT_STATUS') ?>
        </label>
        <label class="col-sm-2 col-md-2"></label>
        <label class="col-sm-5 col-md-5"><?= __('TXT_PAYMENT_INFO') ?></label>
    </div>
    <div class="form-group">
        <div class="col-sm-4">
            <label class="control-label status label-draft" style="width: 100%; text-align: center !important; padding: 7px 0px;">
                <?= $currentStatus ?>
            </label>
        </div>
        <div class="col-sm-2" style="position: relative;">
            <span class="glyphicon glyphicon-arrow-right arrow-draft"></span>
        </div>
        <div class="col-sm-6 col-md-6">
            <?php
            if ($payments) {
                echo '<ul class="no-padding payment-list">';
                echo '<li><span class="label label-success">' . $payments[0] . '</span></li>&nbsp;';
                echo '</ul>';
            }
            ?>
        </div>
    </div>
<?php else : ?>
    <div class="form-group" style="margin-bottom: 10px;">
        <label class="col-sm-4 control-label text-left" style="padding-top: 0px;"><?= __('TXT_CURRENT_STATUS') ?></label>
        <label class="col-sm-2 col-md-2"></label>
        <label class="col-sm-5 col-md-5">
            <?php echo ($step !== 8) ?  __('TXT_SELECT_NEXT_STATUS') : ''; ?>
        </label>
    </div>
    <div class="form-group">
        <div class="col-sm-4">
            <label class="control-label status label-draft" style="width: 100%; text-align: center !important; padding: 7px 0px;">
                <?= $currentStatus ?>
            </label>
        </div>
        <div class="col-sm-2" style="position: relative;">
            <span class="glyphicon glyphicon-arrow-right arrow-draft"></span>
        </div>
        <div class="col-sm-6 col-md-6">
            <?php
            if ($step !== 8) {
                echo $this->Form->input('status', [
                    'type' => 'select',
                    'label' => false,
                    'class' => 'form-control',
                    'options' => $options,
                    'templates' => [
                        'inputContainer' => '{{content}}'
                    ],
                ]);
            } else {
                echo '<span>After 7days change to ‘completed’ auto</span>';
            }
            ?>
        </div>
    </div>
<?php endif; ?>
<div class="status-wrap">
    <?php
    // CHECK STATUS CHANGE
    switch ($step) {
        // APPLIES CUSTOM CLEARANE STATUS
        case 5 :
            $delivery = null;
            $deliveryId = -1;
            // CHECK SALE_DELIVERIES IS NOT EMPTY
            if ($sale->sale_deliveries) {
                $delivery = $sale->sale_deliveries[0]->delivery;
                $deliveryId = $delivery->id;
            }
            echo $this->Form->create(null, [
                'class' => 'form-horizontal',
                'role' => 'form',
                'name' => 'delivery_form',
            ]);
            $this->Form->templates([
                'inputContainer' => '<div class="col-sm-1"></div><div class="col-sm-6">{{content}}</div>',
            ]);
            echo $this->Form->hidden('delivery_id', [
                'value' => $deliveryId,
                'id' => 'delivery-id',
            ]);
            // FORM GROUP
            echo '<div class="form-group">' .
                '<label class="col-sm-5 control-label text-left">' . __('TXT_TRACKING_NUMBER') . '</label>';
            echo $this->Form->input('tracking', [
                'label' => false,
                'required' => false,
                'class' => 'form-control',
                'placeholder' => __('TXT_TRACKING_NUMBER'),
                'value' => $delivery ? $delivery->tracking : null,
            ]);
            echo '</div>';
            // FORM GROUP
            echo '<div class="form-group">' .
                '<label class="col-sm-5 control-label text-left">' . __('TXT_DELIVERY_DATE') . '</label>';
            echo $this->Form->input('delivery_date', [
                'label' => false,
                'required' => false,
                'class' => 'form-control',
                'placeholder' => __('TXT_DELIVERY_DATE'),
                'value' => $delivery ? date('Y-m-d', strtotime($delivery->delivery_date)) : date('Y-m-d'),
            ]);
            echo '</div>';
            // FORM GROUP
            echo '<div class="form-group"><div class="col-sm-12 text-right">';
            echo $this->Form->button(__('TXT_UPDATE'), [
                'type' => 'button',
                'class' => 'btn btn-sm btn-primary btn-update-delivery',
            ]);
            echo '</div></div>';
            // LIST PDF FILES
            $element = '';
            $fileName = '';
            $docType = '';
            $className = 'browse-file';
            $type = 'new';
            $id = 0;
            // CHECK MEDIA IS NOT EMPTY
            if ($medias) {
                $className = 'browse-file-edit';
                $type = 'edit';
                foreach ($medias as $key => $value) {
                    if ($key != 0) {
                        $element .= '<li data-id="' . $value->id . '" data-name="' . $value->file_name . '" data-document-type="' . $value->document_type . '" data-type="' . $type . '"><span class="label label-success ' . $className . '"><i class="fa fa-times rm-pdf-file" aria-hidden="true"></i>&nbsp;&nbsp;Browse File</span><span>&nbsp;</span></li>';
                    } else {
                        $fileName = $value->file_name;
                        $id = $value->id;
                        $docType = $value->document_type;
                    }
                }
            }
            echo '<div class="form-group">' .
            '<label class="col-sm-6 control-label text-left">' . __('TXT_CUSTOM_CLEARANCE') . '</label>' .
            '<div class="col-sm-6 col-md-6"><ul class="custom-wrap">' .
            '<li data-document-type="' . $docType . '" data-name="' . $fileName . '" data-id="' . $id . '" data-type="' . $type . '"><span class="label label-success ' . $className . '">&nbsp;&nbsp;Browse File</span><span class="label label-primary add-pdf-file">Add</span></li>' .
            $element . '</ul></div></div>';
            break;
    }
    ?>
</div>
<div class="form-group">
    <div class="col-sm-6"></div>
    <div class="col-sm-6">
        <?php
        if (((STATUS_PAID !== $sale->status) || ($sale->sub_status === STATUS_SHIPPED))
            && ($step != 8)) {
            echo $this->Form->button($buttonName, [
                'type' => 'submit',
                'class' => 'btn btn-primary btn-register pull-right',
                'data-step' => $step,
            ]);
        }
        ?>
    </div>
</div>
<?php echo $this->Form->end(); ?>
