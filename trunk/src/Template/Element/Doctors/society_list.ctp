<?php if (isset($doctor->doctor_societies)): ?>
    <div class="form-group">
        <div class="col-lg-12 col-sm-12 col-md-12">
            <label class="col-sm-4 control-label"></label>
            <div class="col-sm-8">
                <ul class="email-wrap">
                <?php foreach ($doctor->doctor_societies as $key => $value): ?>
                    <li data-id="<?= $value->id ?>"><?= $value->society->name ?></li>
                <?php endforeach; ?>
                </ul>
            </div>
        </div>
    </div>
<?php endif; ?>
