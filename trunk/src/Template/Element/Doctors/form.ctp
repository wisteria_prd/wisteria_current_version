
<style>
    .facility-wrap .form-group {
        margin-bottom: 0px;
    }
    .facility-wrap .panel-body > div:first-child {
        margin-bottom: 15px;
    }
</style>
<?php $action = $this->request->action; ?>
<section id="register_form">
    <div class="col-lg-2 col-md-2 col-sm-2">
        <div class="dropzone dropzone-profile"></div>
    </div>
    <?php
    echo $this->Form->create($doctor, [
        'role' => 'form',
        'class' => 'form-horizontal doctor-form',
        'name' => 'doctor_form',
        'type' => 'file',
    ]);
    echo $this->Form->hidden('id');
    $profile = '';
    if ($doctor->isNew(false)) {
        // Filter profile image from media
        $media = new \Cake\Collection\Collection($doctor->medias);
        $media = $media->filter(function ($value, $index) {
            return ($value->status == 1);
        })->toArray();
        if ($media) {
            $profile = $media[0]->file_name;
        }
    }
    echo $this->Form->hidden('media-profile', [
        'class' => 'media-profile',
        'value' => $profile,
    ]);
    ?>
    <div class="col-lg-10 col-md-10 col-sm-10">
        <div class="form-group">
            <label class="col-sm-2 control-label">
                <?php
                echo __('TXT_DOCTORE') . $this->Comment->formAsterisk() ?>
            </label>
            <div class="col-md-4 col-lg-4 col-sm-4">
                <?php
                echo $this->Form->text('last_name_en', [
                    'class' => 'form-control',
                    'placeholder' => __('DOCTOR_TXT_ENTER_LAST_NAME_EN'),
                ]) ?>
            </div>
            <div class="col-md-4 col-lg-4 col-sm-4">
                <?php
                echo $this->Form->text('last_name', [
                    'class' => 'form-control',
                    'placeholder' => __('DOCTOR_TXT_ENTER_LAST_NAME_JP'),
                ]) ?>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label"></label>
            <div class="col-md-4 col-lg-4 col-sm-4">
                <?php
                echo $this->Form->text('first_name_en', [
                    'class' => 'form-control',
                    'placeholder' => __('DOCTOR_TXT_ENTER_FIRST_NAME_EN'),
                ]) ?>
            </div>
            <div class="col-md-4 col-lg-4 col-sm-4">
                <?php
                echo $this->Form->text('first_name', [
                    'class' => 'form-control',
                    'placeholder' => __('DOCTOR_TXT_ENTER_FIRST_NAME_JP'),
                ]) ?>
            </div>
        </div>
        <div class="form-group">
           <label class="col-sm-2 control-label">
               <?php
               echo __('DOCTOR_TXT_MAIDEN_NAME') ?>
           </label>
           <div class="col-md-4 col-lg-4 col-sm-4">
               <?php
               echo $this->Form->text('maiden_name', [
                   'class' => 'form-control',
                   'placeholder' => __('DOCTOR_TXT_ENTER_MAIDEN_NAME'),
               ]) ?>
           </div>
        </div>
        <div class="form-group custom-select">
            <label class="col-sm-2 control-label">
                <?php
                echo __('DOCTOR_TXT_GENDER') . $this->Comment->formAsterisk() ?>
            </label>
            <div class="col-md-4 col-lg-4 col-sm-4">
                <?php
                echo $this->Form->select('gender', $gender, [
                    'class' => 'form-control',
                    'empty' => __('DOCTOR_TXT_SELECT_GENDER'),
                ]) ?>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">
                <?php
                echo __('DOCTOR_TXT_DATE_OF_BIRTH') ?>
            </label>
            <div class="col-md-4 col-lg-4 col-sm-4">
                <?php
                echo $this->Form->text('dob', [
                    'class' => 'form-control',
                    'placeholder' => __('DOCTOR_TXT_ENTER_DATE_OF_BIRTH'),
                ]) ?>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">
                <?php
                echo __('SUPPLIER_TEL') ?>
            </label>
            <div class="col-md-4 col-lg-4 col-sm-4">
                <?php
                $tel = '';
                $tel_extension = '';
                if (isset($doctor->info_details) && $doctor->info_details) {
                    $tel = $doctor->info_details[0]->tel;
                    $tel_extension = $doctor->info_details[0]->tel_extension;
                }
                echo $this->Form->tel('info_details[0].tel', [
                    'class' => 'form-control multiple-tel',
                    'placeholder' => __('SUPPLIER_ENTER_TEL'),
                    'value' => $tel,
                ]);
                echo $this->Form->hidden('info_details[0].type', [
                    'value' => TYPE_CUSTOMER,
                ]);
                ?>
            </div>
            <div class="col-md-4 col-lg-4 col-sm-4">
                <?php
                echo $this->Form->text('info_details[0].tel_extension', [
                    'class' => 'form-control',
                    'placeholder' => __('STR_TEL_EXT'),
                    'value' => $tel_extension,
                ]); ?>
            </div>
            <div class="col-sm-1">
                <?php
                echo $this->Form->button('<i class="fa fa-plus" aria-hidden="true"></i>', [
                    'type' => 'button',
                    'class' => 'btn btn-sm btn-primary btn-tel-add form-btn-add',
                    'escape' => false,
                ]); ?>
            </div>
        </div>
        <div class="tel-box-wrap">
            <?php
            echo $this->element('Doctors/tel_list'); ?>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label"><?= __('DOCTOR_TXT_EMAIL') ?></label>
            <div class="col-md-3 col-lg-3 col-sm-3">
                <?php
                $info = new \stdClass();
                $info->email = '';
                if (isset($doctor) && $doctor->info_details) {
                    $info->email = $doctor->info_details[0]->email;
                }
                echo $this->Form->email('info_details[0].email', [
                    'class' => 'form-control',
                    'placeholder' => __('DOCTOR_TXT_ENTER_EMAIL_ADDRESS'),
                    'default' => isset($info->email) ? $info->email : null,
                ]);
                echo $this->Form->hidden('info_details[0].type', [
                    'value' => TYPE_DOCTOR,
                ]);
                ?>
            </div>
            <div class="col-md-1">
                <?php
                echo $this->Form->button('<i class="fa fa-plus" aria-hidden="true"></i>', [
                    'type' => 'button',
                    'class' => 'btn btn-sm btn-primary btn-email-add form-btn-add',
                    'escape' => false,
                ]) ?>
            </div>
        </div>
        <div class="email-box-wrap">
            <?php
            // Display Phone List
            $doctor = isset($doctor) ? $doctor : [];
            echo $this->element('Doctors/email_list', [$doctor]);
            ?>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">
                <?php
                echo __('DOCTOR_TXT_SOCIETY') ?>
            </label>
            <div class="col-sm-4">
                <?php
                echo $this->Form->button('<i class="fa fa-plus" aria-hidden="true"></i>', [
                    'type' => 'button',
                    'class' => 'btn btn-sm btn-primary btn-society form-btn-add',
                    'escape' => false,
                ]) ?>
            </div>
        </div>
        <div class="society-list">
            <?php
            // Display Society List
            $doctor = isset($doctor) ? $doctor : [];
            echo $this->element('Doctors/society_list', [$doctor]);
            ?>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">
                <?php
                echo __('DOCTOR_TXT_UNIVERSITY') ?>
            </label>
            <div class="col-sm-4">
                <?php
                echo $this->Form->text('university', [
                    'class' => 'form-control',
                    'placeholder' => __('DOCTOR_TXT_ENTER_UNIVERSITY'),
                ]) ?>
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-2 control-label">
                <?php
                echo __('DOCTOR_TXT_CAREER') ?>
            </label>
            <div class="col-lg-8 col-sm-8 col-md-8 textarea-wrap">
                <?php
                echo $this->Form->textarea('career', [
                    'class' => 'form-control',
                    'placeholder' => __d('doctor', 'TXT_ENTER_CAREER'),
                ]) ?>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">
                <?php
                echo __('DOCTOR_TXT_REMARKS') ?>
            </label>
            <div class="col-lg-8 col-sm-8 col-md-8 textarea-wrap">
                <?php
                echo $this->Form->textarea('remarks', [
                    'class' => 'form-control',
                    'placeholder' => __('DOCTOR_TXT_ENTER_REMARKS'),
                ]) ?>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">
                <?php
                echo __('DOCTOR_TXT_DOCTOR_CERTIFICATE') ?>
            </label>
            <div class="col-lg-8 col-sm-8 col-md-8 textarea-wrap">
                <div class="dropzone dropzone-wrap">
                    <?php
                    $files = [];
                    if(isset($doctor->medias)) {
                        foreach ($doctor->medias as $key => $file) {
                            if (!$file->status) {
                                $files[] = $file->file_name;
                            }
                        }
                    }
                    $files1 = implode(',', $files);
                    echo $this->Form->hidden('files', [
                        'class' => 'form-control media-file',
                        'value' => $files1
                    ]);
                    ?>
                    <div class="bottom-class text-center">
                        <div class="upload-file">
                            <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="form-group">
        <?php
        echo $this->ActionButtons->btnSaveCancel('Doctors', $action); ?>
    </div>
    <?php
    echo $this->Form->end(); ?>
    </div>
</section>
<div class="modal fade" id="modal-tick" data-backdrop="static" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-body"></div>
            <div class="modal-footer">
                <?php
                echo $this->Form->button(__('TXT_NO'), [
                    'type' => 'button',
                    'class' => 'btn btn-default btn-sm btn-width',
                    'data-dismiss' => 'modal',
                ]);
                echo $this->Form->button(__('TXT_YES'), [
                    'type' => 'button',
                    'class' => 'btn btn-primary btn-sm btn-width',
                    'id' => 'btn_priority',
                ]);
                ?>
            </div>
        </div>
    </div>
</div>

<script>
    (function(e) {
        PDFJS.disableWorker = true;
        var file_array = [];
        var priority_array = [];
        var filename = $('.media-file').val();
        var img_profile = $('.media-profile').val();
        if (filename !== '') {
            file_array = filename.split(',');
        }

        Dropzone.autoDiscover = false;
        dropzone();
        load_form();
        createValidation();

        $('body').find('.customer-select').select2({
            width: '100%',
            minLength: 0,
            cache: false,
            ajax: {
                url: '<?= $this->Url->build(['action' => 'getCustomerList']) ?>',
                dataType: 'json',
                delay: 250,
                cache: false,
                data: function (params) {
                    var query = {
                        keyword: params.term,
                        type: 'public'
                    };
                    return query;
                },
                processResults: function (data) {
                    var dataSource = [];
                    if (data.data === null && data.data === 'undefined') {
                        return false;
                    }
                    $.each(data.data, function(i, v) {
                        dataSource.push({
                            id: v.id,
                            text: v.name + ' ' + v.name_en
                        });
                    });
                    return { results: dataSource };
                }
            }
        });

        $('#dob').datetimepicker({
            format: 'YYYY-MM-DD',
            minDate: '2007-01-01'
        });

        $('.upload-file').on('click', function() {
            $(this).parent().parent().trigger('click');
        });

         // add multiple tels
         $('body').on('click', '.btn-tel-add', function(e) {
             var index = $('body').find('.tel-box-wrap .form-group').length + 1;
             var content = '<div class="form-group">' +
                     '<label class="col-sm-2 control-label"></label>' +
                     '<div class="col-md-4 col-lg-4 col-sm-4">' +
                         '<input type="tel" placeholder="<?php echo __('TXT_ENTER_TEL'); ?>" class="form-control" name="info_details[' + index + '][tel]"/>' +
                         '<input type="hidden" name="info_details[' + index + '][type]" value="<?php echo TYPE_DOCTOR ?>"/>' +
                     '</div>' +
                     '<div class="col-md-4 col-lg-4 col-sm-4">' +
                         '<input type="text" placeholder="<?php echo __('STR_TEL_EXT'); ?>" class="form-control" name="info_details[' + index + '][tel_extension]"/>' +
                     '</div>' +
                     '<div class="col-sm-1">' +
                         '<button type="button" class="btn btn-delete btn-sm form-btn-add btn-remove-field">' +
                             '<i class="fa fa-trash" aria-hidden="true"></i>' +
                         '</button>' +
                     '</div>' +
                 '</div>';
             $('body').find('.tel-box-wrap').append(content);
         });

        // remove emial, remove tel
        $('body').on('click', '.btn-remove-field, .btn-rm-position', function(e) {
            $(this).closest('.form-group').remove();
        });

        // add multiple emails
        $('body').on('click', '.btn-email-add', function(e) {
            var index = $('body').find('.email-box-wrap .form-group').length + 1;
            var content = '<div class="form-group">' +
                            '<label class="col-sm-2 control-label"></label>' +
                            '<div class="col-sm-4">' +
                                '<input type="email" placeholder="<?= __('TXT_ENTER_EMAIL') ?>" class="form-control" name="info_details[' + index + '][email]"/>' +
                                '<input type="hidden" name="info_details[' + index + '][type]" value="<?= TYPE_DOCTOR ?>"/>' +
                            '</div>'+
                            '<button type="button" class=" btn btn-delete btn-sm form-btn-add btn-remove-field"><i class="fa fa-trash" aria-hidden="true"></i></button>' +
                    '</div>';
            $('body').find('.email-box-wrap').append(content);
            createValidation();
        });

        // add societies
        $('body').on('click', '.btn-society', function(e) {
            var li = $('body').find('.society-list ul li');
            var ids = [];
            if ((li !== null) && (li !== 'undefined')) {
                $.each(li, function(i, v) {
                    ids.push($(this).attr('data-id'));
                });
            }
            var options = {
                type: 'GET',
                url: '<?= $this->Url->build('/societies/get-form') ?>',
                data: { 'ids': JSON.stringify(ids) },
                beforeSend: function() {
                    $.LoadingOverlay('show');
                }
            };
            ajaxRequest(options, function(data) {
                $('body').prepend(data);
                $('body').find('.modal-society').modal('show');
            });
        });

        $('body').on('click', '.checked-society', function(e) {
            var content = $('body').find('.modal-society');
            var selected = $(content).find(':checkbox:checked');
            if ((selected === null) && (selected === 'undefined')) {
                return false;
            }
            var element = '<div class="form-group"><div class="col-lg-12 col-sm-12 col-md-12">' +
                        '<label class="col-sm-4 control-label"></label>' +
                        '<div class="col-sm-8">' +
                            '<ul class="email-wrap">';
            $.each(selected, function(i, v) {
                element += '<li data-id="' + $(this).val() + '">' + $(this).attr('data-name') + '</li>';
            });
            element += '</div></div></div>';
            $('body').find('.society-list').html(element);
        });

        $('body').on('click', '.dropzone-wrap .dz-image', function(e) {
            var index = $(this).closest('.dz-preview').index('.dz-preview');
            var num = $(this).closest('.dz-preview').attr('data-tick');
            var message = '<p class="text-center">Do you want to set this image to priority?</p>';
            if(num == 1) {
                message = '<p class="text-center">Do you want to unchecked this image?</p>';
            }
            $('#modal-tick').find('#btn_priority').attr('data-index', index-1);
            $('#modal-tick').find('#btn_priority').attr('data-tick', num);
            $('#modal-tick').find('.modal-body').html('').append(message);
            $('#modal-tick').modal('show');
        });

        // Sort
        $('.dropzone-wrap').sortable({
            items: '.dz-preview',
            cursor: 'move',
            opacity: 0.5,
            containment: '.dropzone-wrap',
            distance: 20,
            tolerance: 'pointer',
            update: function (e, ui) {
                update_file();
            }
        });

        $('#btn-register').on('click', function(e) {
            e.preventDefault();
            var ids = [];
            var form = $('form[name="doctor_form"]');
            var li = $('body').find('.society-list ul li');
            if ((li !== null) && (li !== 'undefined')) {
                $.each(li, function(i, v) {
                    ids.push($(this).attr('data-id'));
                });
            }
            var params = {
                data: $(form).serialize(),
                societies: ids
            };
            $.ajax({
                url: '<?= $this->Url->build('/doctors/save-and-update/') ?>',
                type: 'POST',
                dataType: 'json',
                cache: false,
                data: params,
                beforeSend: function() {
                    $.LoadingOverlay('show');
                    $(form).find('.error-message').remove();
                }
            })
            .done(function(data) {
                if ((data === null) && (data !== 'undefined')) {
                    return false;
                }
                if (data.status === 1) {
                    location.href = '<?php echo $this->Url->build('/doctors/index/'); ?>';
                } else {
                    validation(form, data.data);
                }
            })
            .fail(function(jqXHR, textStatus, errorThrown) {
                $.LoadingOverlay('hide');
                if (errorThrown === 'Forbidden') {
                    if (confirm('<?= __('Session timeout. Please login again.') ?>')) {
                        location.reload();
                    }
                }
            })
            .always(function() {
                $.LoadingOverlay('hide');
            });
        });

        function validation(form, data)
        {
            if ((data === null) && (data === 'undefined')) {
                return false;
            }
            $.each(data, function(i, v) {
                var message = '<label class="error-message">' + v._empty + '</label>';
                var content = $(form).find('[name="' + i + '"]');
                if (i === 'clinic_doctors') {
                    $.each(v, function(i1, v1) {
                        var panel = $('body').find('.facility-wrap');
                        var content1 = $(panel).find('.form-group:eq(' + i1 + ')');
                        if (v1.customer_id) {
                            var msg_customer = '<label class="error-message">' + v1.customer_id._empty + '</label>';
                            $(content1).find('select').closest('div').append(msg_customer);
                        }
                        if (v1.position) {
                            var msg_position = '<label class="error-message">' + v1.position._empty + '</label>';
                            $(content1).find('input[type="text"]').closest('div').append(msg_position);
                        }
                    });
                }
                $(content).closest('div').append(message);
            });
        }

        // set priority of image
        function set_priority()
        {
            var img_list = [];
            var content = $('body').find('.dropzone-wrap').find('.dz-preview[data-tick=1]');
            if (content) {
                $.each(content, function(i, v) {
                    var file_name = $(this).find('img') ? $(this).find('img').attr('alt') : $(this).attr('data-name');
                    img_list.push(file_name);
                });
            }
            $('body').find('.priority-file').val(img_list);
        }

        function dropzone()
        {
            // Dropzone certificate upload
            $('.dropzone-wrap').dropzone({
                url: '<?= $this->Url->build('/medias/upload') ?>',
                paramName: 'file',
                acceptedFiles: '.png, .gif, .jpg, .jpeg, .PNG, .GIF, .JPG, .JPEG, .pdf, .csv',
                dictDefaultMessage: '<?php echo __('DOCTOR_TXT_DRAG_AND_DROP_FILE_HERE_OR_CLICK_THE_BUTTON_TO_UPLOAD'); ?>',
                dictCancelUpload: '',
                dictCancelUploadConfirmation: '',
                previewTemplate: preview_template(),
                maxFilesize: 100,
                init: function () {
                    thisDropzone = this;
                    for (var index = 0; index < file_array.length; index++) {
                        var is_priority = ($.inArray(file_array[index], priority_array) != -1) ? '1' : '0';
                        var mockFile = {
                            name: file_array[index],
                            type: file_array[index].split('.').pop().toLowerCase(),
                            size: 75,
                            path: BASE + 'img/uploads/' + file_array[index]
                        };
                        thisDropzone.options.addedfile.call(thisDropzone, mockFile);
                        thisDropzone.options.thumbnail.call(thisDropzone, mockFile, mockFile.path);
                        var content = mockFile.previewElement;
                        $(content).attr('data-tick', is_priority);

                        if (is_priority === '1') {
                            var element = '<div class="img-tick"><span><i class="fa fa-check" aria-hidden="true"></i></span></div>';
                            $(content).find('.dz-image').append(element);
                        }
                        if (mockFile.type === 'pdf') {
                            var canvas_id = 'canvas-' + index;
                            var full_path = location.protocol + '//' + location.host + mockFile.path;
                            $(content).find('.dz-image').append('<canvas id="' + canvas_id + '">');
                            read_pdf_file(content, full_path, canvas_id, index);
                            $(content).find('.dz-image img').css({'width': '0','height': '0'});
                        }
                    }
                    $('.dz-progress').hide();

                    thisDropzone.on('success', function (file, result, events) {
                        var extension = file.name.split('.').pop().toLowerCase();
                        var canvas = file.previewElement.querySelector('canvas');
                        var fileName = file.previewElement.querySelector('.dz-image');
                        $(file.previewElement).attr('data-tick', '0');
                        $(fileName).attr('data-name', JSON.parse(result));
                        file.previewElement.querySelector('img').alt = JSON.parse(result);

                        if (extension === 'pdf') {
                            var content = file.previewElement;
                            $(content).find('.dz-image img').css({
                                'width': '0',
                                'height': '0'
                            });
                            $(content).find('.dz-image').append('<canvas></canvas>');
                            convert_pdf_to_canvas(file, content);
                        }

                        file_array.push(JSON.parse(result));
                        $('.media-file').val(file_array);
                        if ($('.media-file').val()) {
                            update_file();
                        }
                    });

                    thisDropzone.on('removedfile', function(file) {
                        update_file();
                        if ($('.media-file').val() !== '') {
                            $('.dz-default').hide();
                        } else {
                            $('.dz-default').show();
                        }
                    });
                }
            });

            // Dropzone profile upload
            $('.dropzone-profile').dropzone({
                url: '<?php echo $this->Url->build('/medias/upload'); ?>',
                paramName: 'profile',
                acceptedFiles: '.png, .gif, .jpg, .jpeg, .PNG, .GIF, .JPG, .JPEG',
                dictDefaultMessage: "<?php echo __('TXT_PROFILE_UPLOAD'); ?>", //aphostrophe  problem for single quote
                dictCancelUpload: '',
                dictCancelUploadConfirmation: '',
                maxFilesize: 100,
                previewTemplate: preview_template(),
                maxFiles: 1,
                init: function () {
                    thisDropzone = this;
                    if (img_profile) {
                        var mockFile = {
                            name: img_profile,
                            type: img_profile.split('.').pop().toLowerCase(),
                            size: 75,
                            path: BASE + 'img/uploads/' + img_profile
                        };
                        thisDropzone.options.addedfile.call(thisDropzone, mockFile);
                        thisDropzone.options.thumbnail.call(thisDropzone, mockFile, mockFile.path);
                    }
                    $('.dz-progress').hide();

                    thisDropzone.on('success', function (file, result, events) {
                        file.previewElement.querySelector('.dz-remove').remove();
                        var remove_link = '<a class="dz-remove" href="javascript:undefined;" data-dz-remove=""><span><i class="fa fa-trash-o" aria-hidden="true"></i></span></a>';
                        $(file.previewElement).append(remove_link);
                        file.previewElement.querySelector('img').alt = JSON.parse(result);
                        $(file.previewElement).find('.dz-remove').html('').append('<span><i class="fa fa-trash-o" aria-hidden="true"></i></span>');
                        $('.media-profile').val(JSON.parse(result));
                        update_file();
                    });

                    thisDropzone.on('removedfile', function(file) {
                        $('.media-profile').val('');
                    });
                }
            });
        }

        // Remove profile
        $('body').on('click', '.dz-remove span', function(e) {
            var content = $(this).closest('.dropzone');
            $(content).removeClass('dz-started')
                    .find('.dz-preview').remove();
        });

        // update file after sort
        function update_file() {
            var new_file = [];
            var img_list = $('.dropzone-wrap').find('.dz-image');
            $.each(img_list, function(i, v) {
                var file_name = $(this).find('img').attr('alt');
                new_file.push(file_name);
            });
            if (new_file) {
                $('.media-file').val(new_file);
            }
        }

        // read pdf file from server
        function read_pdf_file(content, path, id, index)
        {
            PDFJS.getDocument(path).promise.then(function (pdf) {
                pdf.getPage(1).then(function getPageHelloWorld(page) {
                    var scale = 1.5;
                    var viewport = page.getViewport(scale);
                    var canvas = document.getElementById(id);
                    var context = canvas.getContext('2d');
                    canvas.height = viewport.height;
                    canvas.width = viewport.width;
                    var renderContext = {
                        canvasContext: context,
                        viewport: viewport
                    };
                    page.render(renderContext);
                });
            });
        }

        // convert pdf file to canvas
        function convert_pdf_to_canvas(file, content)
        {
            PDFJS.disableWorker = true;
            fileReader = new FileReader();
            fileReader.onload = function(ev) {
                PDFJS.getDocument(fileReader.result).then(function getPdfHelloWorld(file) {
                    file.getPage(1).then(function getPageHelloWorld(page) {
                        var scale = 1.5;
                        var viewport = page.getViewport(scale);
                        var canvas = $(content).find('.dz-image').find('canvas')[0];
                        var context = canvas.getContext('2d');
                        canvas.height = viewport.height;
                        canvas.width = viewport.width;
                        var task = page.render({
                            canvasContext: context,
                            viewport: viewport
                        });
                     });
                 });
             };
            fileReader.readAsArrayBuffer(file);
        }

        // dropzone preview template
        function preview_template()
        {
            var element = '<div class="dz-preview ui-sortable-handle dz-image-preview">'
                        + '<div class="dz-image"><img data-dz-thumbnail/></div><a class="dz-remove" href="javascript:undefined;" data-dz-remove=""><span><i class="fa fa-trash-o" aria-hidden="true"></i></span></a></div></div>';
            return element;
        }

        function load_form()
        {
            $('form[name="doctor_form"]').validate({
                rules: {
                    'position[]': {
                        required: true,
                    },
                    'email[]' : {
                        email: true,
                        required: false,
                    }
                },
                messages: {
                    'position[]': '<?= __('TXT_MESSAGE_REQUIRED'); ?>'
                },
                ignore: '',
                errorClass: 'error-message',
                submitHandler: function(form) {
                    form.submit();
                }
            });
        }

        function createValidation() {
            $('.doctor-position').each(function() {
                $(this).rules('remove');
                $(this).rules('add', {
                    required: true,
                    messages: {
                        required: '<?= __('TXT_MESSAGE_REQUIRED'); ?>'
                    }
                });
            });
            $('input[type="email"]').each(function() {
                $(this).rules('remove');
                $(this).rules('add', {
                    email: true,
                    required: false,
                    messages: {
                        required: '<?= __('TXT_MESSAGE_REQUIRED'); ?>'
                    }
                });
            });
        }

        function ajaxRequest(params, callback)
        {
            $.ajax(params).done(function(data) {
                if (data === null && data === 'undefined') {
                    return false;
                }
                if (typeof callback === 'function') {
                    callback(data);
                }
            }).fail(function(jqXHR, textStatus, errorThrown) {
                if (errorThrown === 'Forbidden') {
                    if (confirm('<?= __('TXT_SESSION_TIMEOUT') ?>')) {
                        location.reload();
                    }
                }
            }).always(function(data) {
                $.LoadingOverlay('hide');
            });
        }
    })();
</script>

<?php
echo $this->Html->css([
    'bootstrap-datetimepicker.min',
    'jquery-ui',
]);
echo $this->Html->script([
    'jquery-sortable-min',
    'processing-api.min',
    'pdf',
    'dropzone.min',
    'jquery.validate.min',
], ['block' => 'script']);
