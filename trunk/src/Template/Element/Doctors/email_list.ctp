<?php if (isset($doctor) && $doctor->info_details): ?>
    <?php foreach ($doctor->info_details as $key => $value): ?>
        <?php if (($value->email !== null) && !empty($value->email) && ($key != 0)): ?>
            <div class="form-group">
                <div class="col-lg-12 col-sm-12 col-md-12">
                    <label class="col-sm-4 control-label"></label>
                    <div class="col-sm-7">
                        <?php
                        echo $this->Form->tel('info_details[' . $key . '].email', [
                            'class' => 'form-control',
                            'placeholder' => __('TXT_ENTER_PHONE'),
                            'default' => $value->email
                        ]);
                        echo $this->Form->hidden('info_details[' . $key . '].type', [
                            'value' => TYPE_DOCTOR,
                        ]);
                        ?>
                    </div>
                    <?= $this->Form->button('<i class="fa fa-trash" aria-hidden="true"></i>', [
                        'type' => 'button',
                        'class' => ' btn btn-delete btn-sm form-btn-add btn-remove-field',
                        'escape' => false,
                    ]) ?>
                </div>
            </div>
        <?php endif; ?>
    <?php endforeach; ?>
<?php endif; ?>
