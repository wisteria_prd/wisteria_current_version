
<div class="search-section" style="margin-bottom: 20px;">
    <?php
    echo $this->Form->create('MedicalCorporations', [
        'role' => 'form',
        'class' => 'form-inline form-search',
        'type' => 'get',
        'name' => 'form_search'
    ]);
    echo $this->Form->hidden('displays', ['value' => $displays]);
    echo $this->Form->input('keyword', [
        'name' => 'keyword',
        'placeholder' => __('USER_SEARCH_KEYWORD'),
        'class' => 'form-control',
        'label' => false,
        'value' => $this->request->query('keyword') ? $this->request->query('keyword') : null,
        'autocomplete' => 'off',
        'templates' => [
            'inputContainer' => '<div class"form-group">{{content}}</div>',
        ]
    ]);
    echo $this->Form->end();
    ?>
</div>
<div class="total-pagination" style="padding-bottom: 20px;">
    <?php echo $this->element('display_info'); ?>
</div>
<div class="row search-section" style="margin-bottom: 20px;">
    <?php echo $this->element('display_number'); ?>
    <?= $this->ActionButtons->btnRegisterNew('MedicalCorporations', []) ?>
    <div class="clearfix"></div>
</div>
<div class="data-tb-list">
    <?php if ($data): ?>
    <table class="table table-striped">
        <thead>
            <tr>
                <th>#</th>
                <th>&nbsp;</th>
                <th class="white-space">
                    <?php echo $this->Paginator->sort('code_old', __('TXT_OLD_CODE')); ?>
                    <i class="fa fa-sort-desc" aria-hidden="true"></i>
                </th>
                <th class="white-space">
                    <?php echo $this->Paginator->sort('name_en', __('TXT_NAME')); ?>
                    <i class="fa fa-sort-desc" aria-hidden="true"></i>
                </th>
                <th class="white-space">
                    <?php echo $this->Paginator->sort('reciever_name', __('TXT_RECEIVER_NAME')); ?>
                    <i class="fa fa-sort-desc" aria-hidden="true"></i>
                </th>
                <th class="white-space">
                    <?php echo $this->Paginator->sort('payment_category_id', __('TXT_PAYMENT_CAT')); ?>
                    <i class="fa fa-sort-desc" aria-hidden="true"></i>
                </th>
                <th class="white-space">
                    <?php echo $this->Paginator->sort('invoice_delivery_id', __('TXT_INVOICE_DELIVERY')); ?>
                    <i class="fa fa-sort-desc" aria-hidden="true"></i>
                </th>
                <th class="white-space"><?= __('TBL_PERSON_IN_CHARGE') ?></th>
                <th colspan="3">&nbsp;</th>
            </tr>
        </thead>
        <tbody>
            <?php
            $numbering = $this->Paginator->counter('{{start}}');
            foreach ($data as $item) :
            ?>
            <tr data-id="<?= h($item->id); ?>">
                <th scope="row"><?php echo $numbering; $numbering++; ?></th>
                <td>
                    <?= $this->ActionButtons->disabledText($item->is_suspend) ?>
                </td>
                <td><?php echo h($item->code); ?></td>
                <td><?= h($item->name_en); ?></td>
                <td><?= h($item->reciever_name); ?></td>
                <td><?= $payment_list[$item->payment_category_id]; ?></td>
                <td><?= $invoice_list[$item->invoice_delivery_method_id]; ?></td>
                <td>
                    <?php
                    echo $this->Comment->btnPersonInCharge($item->id, count($item->person_in_charges), TYPE_MEDICAL_CORP);
                    ?>
                </td>
                <td width="1%">
                    <?php echo $this->Form->button('<i class="fa fa-comments-o" aria-hidden="true"></i>', [
                        'class' => 'btn btn-primary btn-sm btn_comment',
                        'data-id' => $item->id,
                        'escape' => false,
                    ]); ?>
                </td>
                <?php
                echo $this->ActionButtons->btnAction($item->id, $item->is_suspend, 'medicalCorporations');
                ?>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <?php endif; ?>
</div>
<div class="row row-top-space">
    <?php echo $this->element('display_number'); ?>
    <?= $this->element('next_prev') ?>
    <div class="clearfix"></div>
</div>

<!--Modal Detail-->
<?php echo $this->element('Modal/view_detail'); ?>
<!--Modal Comment-->
<?php echo $this->element('/Modal/message') ?>
<!--Modal delete-->
<?php echo $this->element('Modal/delete'); ?>
<!--Modal Suspend-->
<?php echo $this->element('Modal/suspend'); ?>

<script>
    (function(e) {
        //auto complete
        $('body').find('#keyword').typeahead({
            minLength: 0,
            autoSelect: true,
            source:  function (query, process) {
                return $.get('<?= $this->Url->build(['action' => 'getMedialCorBySearch']) ?>', {
                        keyword: query
                    }, function (data) {
                        var optionData = [];
                        if (data.data !== null && data.data !== 'undefined') {
                            $.each(data.data, function(i, v) {
                                if (v.name !== '') {
                                    optionData.push(v.name);
                                }
                                if (v.name_en !== '') {
                                    optionData.push(v.name_en);
                                }
                            });
                        }
                        return process(optionData);
                    }, 'json');
            }
        });

        $('body').on('click', '.typeahead li', function(e) {
            $('body').find('.form-search').submit();
        });

        $('.btn_comment').click(function (e) {
            e.stopPropagation();
            let params = {
                id: $(this).closest('tr').attr('data-id'),
                type: '<?php echo TYPE_MEDICAL_CORP ?>',
                referer: '<?php echo $this->request->controller; ?>'
            };
            let options = {
                url: BASE + 'Messages/getMessageList',
                type: 'GET',
                data: params,
                beforeSend: function () {
                    $.LoadingOverlay('show');
                }
            };
            ajaxRequest(options, SESSION_TIMEOUT, function (data) {
                $('body').prepend(data);
                $('body').find('.modal-message').modal('show');
            });
        });

        //filter message list search to specific
        $('body').on('click', '.btn-to-individual', function() {
            $(this).removeClass('btn-detault').addClass('btn-primary');
            $('body').find('.btn-to-all').removeClass('btn-primary').addClass('btn-default');
            var toAll = $('.to-individual').val();
            if (parseInt(toAll) === 0) {
                $('.to-all').val(0);
                $('.to-individual').val(1);
            } else {
                $('.to-all').val(1);
                $('.to-individual').val(0);
            }
            messagesSearch(listUrlSearch);
        });

        $('body').on('click', '.btn-search', function() {
            messagesSearch(listUrlSearch);
        });

        $('body').on('click', '.btn-suspend-msg', function() {
            var id = $(this).data('id');
            var data = $('#form-message-status-' + id).serialize();
            var suspendUrl = '<?= $this->Url->build(['controller' => 'Messages', 'action' => 'suspend']) ?>';
            suspendMessage(id, suspendUrl, data);
        });

        $('body').on('click', '.data-tb-list .table>tbody>tr', function (e) {
            $.LoadingOverlay('show');
            $.get('<?php echo $this->Url->build('/medical-corporations/view/'); ?>' + $(this).closest('tr').data('id'), function (data) {
                $.LoadingOverlay('hide');
                $('#modal_detail').find('.modal-body').html(data);
                $('#modal_detail').modal('show');
            }, 'html');
        });

        $('body').on('click', '.table td a, .table td select', function(e) {
            e.stopPropagation();
        });

        //modal confirm suspend
        $('body').on('click', '.btn_suspend', function (e) {
            e.stopPropagation();
            $('.suspend-item-id').val($(this).attr('data-id'));
            $('.is-suspend-item').val($(this).attr('data-target'));
            var confirm = '';
            if (parseInt($(this).attr('data-target')) === 1) {
                $('#btn_suspend_yes').text('<?= __('BTN_SUSPEND') ?>');
                confirm = '<?= __('MSG_CONFIRM_SUSPEND') ?>';
            } else {
                $('#btn_suspend_yes').text('<?= __('BTN_RECOVER') ?>');
                confirm = '<?= __('MSG_CONFIRM_UNDO_SUSPEND') ?>';
            }
            $('.confirm-suspend-text').text(confirm);
            $('#modal_suspend').modal('show');
        });

        //modal change suspend status
        $('body').on('click', '#btn_suspend_yes', function (e) {
            var data = $('.form-suspend-item').serialize();
            var url = '<?php echo $this->Url->build('/medical-corporations/update-suspend/'); ?>';
            var txtTimout = '<?php echo __('TXT_SESSION_TIMEOUT') ?>';
            statusSuspend(url, data, txtTimout);
        });

        //modal delete confirm
        $('body').on('click', '#btn_delete', function (e) {
            e.stopPropagation();
            $('#btn_delete_yes').text('<?= __('BTN_DELETE') ?>');
            $('.delete-item').val($(this).attr('data-id'));
            $('#modal_delete').modal('show');
        });

        //modal delete item.
        $('body').on('click', '#btn_delete_yes', function() {
            var data = $('.form-delete').serialize();
            var url = '<?= $this->Url->build('/medical-corporations/delete') ?>';
            itemDelete(url, data, '<?php echo __('TXT_SESSION_TIMEOUT') ?>');
        });

        $('body').on('click', '.tt-selectable', function (e) {
            $('form[name="form_search"]').submit();
        });

        pageOutOfRange(<?= $paging ?>, '<?= $this->request->query('page') ?>');
        var maxPage = parseInt($('#maxPage').val());
        updatePagination(maxPage);
    })();
</script>
