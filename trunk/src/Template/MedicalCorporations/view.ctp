
<table class="table table-striped" id="tb-detail">
    <tbody>
        <tr>
            <td><?= __('TXT_NAME') ?></td>
            <td><?= h($data->name); ?></td>
            <td><?= h($data->name_en); ?></td>
        </tr>
        <tr>
            <td><?= __('TXT_PHONE') ?></td>
            <td colspan="2"><?= h($data->phone); ?></td>
        </tr>
        <tr>
            <td>Fax</td>
            <td colspan="2"><?= h($data->fax); ?></td>
        </tr>
        <tr>
            <td><?= __('TXT_EMAIL') ?></td>
            <td colspan="2">
                <?php
                if ($data->info_details) {
                    $element = '<ul class="email-wrap">';
                    foreach ($data->info_details as $item) {
                        $element .= '<li><a href="mailto:' . h($item->email) . '" target="_blank">' . h($item->email) . '</a></li>';
                    }
                    echo $element . '</ul>';
                }
                ?>
            </td>
        </tr>
        <tr>
            <td><?= __('TXT_OLD_CODE') ?></td>
            <td colspan="2"><?= h($data->code) ?></td>
        </tr>
        <tr>
            <td><?= __('TXT_RECEIVER_NAME') ?></td>
            <td colspan="2"><?= h($data->reciever_name) ?></td>
        </tr>
        <tr>
            <td><?= __('TXT_PAYMENT') ?></td>
            <td colspan="2"><?= h($data->payment_name) ?></td>
        </tr>
        <tr>
            <td><?= __('TXT_INVOICE') ?></td>
            <td colspan="2"><?= h($data->invoice_name); ?></td>
        </tr>
        <tr>
            <td>URL</td>
            <td colspan="2">
                <a href="<?= h($data->url) ?>" target="_blank"><?= h($data->url) ?></a>
            </td>
        </tr>
        <tr>
            <td><?= __('TXT_ADDRESS') ?></td>
            <td style="width: 200px;">
                <?php echo h($data->building_en) . ', ' . h($data->street_en) . ', ' . h($data->city_en) ?><br>
                <?php echo h($data->prefecture_en) . ', ' . h($data->country_en) . ', ' . h($data->postal_code) ?>
            </td>
            <td>
                <?php
                $country = '';
                if ($data->country) {
                    $country = $data->country->$name . ', ';
                }
                echo h($country . $data->postal_code);
                ?>
                <?php echo h($data->prefecture) . ', ' . h($data->city); ?>
                <?php echo h($data->street) . ', ' . h($data->building); ?>
            </td>
        </tr>
        <tr>
            <td><?= __('TXT_MODIFIED') ?></td>
            <td colspan="2"><?= date('Y-m-d', strtotime($data->modified)) ?></td>
        </tr>
        <tr>
            <td><?= __('TXT_CREATED') ?></td>
            <td colspan="2"><?= date('Y-m-d', strtotime($data->created)) ?></td>
        </tr>
        <tr>
            <td><?php echo __('CUSTOMERS_TXT_PERSON_IN_CHARGE'); ?> :</td>
            <td colspan="2">
                <?php
                $TXT_PERSON_IN_CHARGE_NUMBER = '';
                if (count($data->person_in_charges) < 2) {
                    $TXT_PERSON_IN_CHARGE_NUMBER = __('CUSTOMERS_TXT_PERSON_IN_CHARGE_NUMBER_SINGULAR');
                } else {
                    $TXT_PERSON_IN_CHARGE_NUMBER = __('CUSTOMERS_TXT_PERSON_IN_CHARGE_NUMBER_PLURAL');
                }
                echo $this->Html->link(count($data->person_in_charges).' '.$TXT_PERSON_IN_CHARGE_NUMBER, [
                    'controller' => 'personInCharges',
                    'action' => (count($data->person_in_charges) ? 'index' : 'add'),
                    '?' => [
                        'external_id' => $data->id,
                        'filter_by' => TYPE_MEDICAL_CORP,
                        'des' => TYPE_MEDICAL_CORP,
                    ]
                ], [
                    'class' => 'btn btn-default text-center btn-sm',
                ]); ?>
            </td>
        </tr>
    </tbody>
</table>
