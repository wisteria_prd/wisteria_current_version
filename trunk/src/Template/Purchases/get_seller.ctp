<?php
    echo $this->Form->create(null, [
        'class' => 'form-add-po form-horizontal',
        'autocomplete' => 'off',
        'name' => 'common_form'
    ]);
?>
<div class="form-group custom-select">
    <label for="" class="control-label col-md-3 col-md-offset-1"><?= __('Seller') ?></label>
    <div class="col-md-7">
        <?php
        $slr = [];
        if ($sellers) {
            $name = 'name' . $en;
            foreach ($sellers as $seller) {
                if ($seller->type == TYPE_MANUFACTURER && $seller->is_manufacture_seller == 1) {
                    if ($seller->manufacturer) {
                        $slr[$seller->id] = $seller->manufacturer->$name;
                    }
                }

                if ($seller->type == TYPE_SUPPLIER) {
                    if ($seller->supplier) {
                        $slr[$seller->id] = $seller->supplier->$name;
                    }
                }
            }
        }

        echo $this->Form->select('seller_id', $slr, [
            'class' => 'form-control',
            'label' => false,
            'required' => false,
            'id' => 'select-seller',
            'empty' => ['' => __('Select Seller...')]
        ]);
        ?>

        <span class="help-block help-tips" id="error-seller_id"></span>
    </div>
</div>
<div class="form-group">
    <label for="" class="control-label col-md-3 col-md-offset-1"><?= __('Issue Date') ?></label>
    <div class="col-md-7">
        <div class="input-group">
            <?= $this->Form->input('date', ['class' => 'form-control', 'label' => false, 'id' => 'datepicker']) ?>
            <div class="input-group-btn">
                <button type="button" class="btn btn-default" aria-label="Calendar" id="trigger-calendar">
                    <span class="glyphicon glyphicon-calendar"></span>
                </button>
            </div>
        </div>
    </div>
</div>
<div class="form-group">
    <label for="" class="control-label col-md-3 col-md-offset-1"><?= __('Currency') ?></label>
    <div class="col-md-7">
        <?php
            $options = ['USD', 'SGD', 'JPY'];
        ?>
        <?= $this->Form->select('currency', $options, ['class' => 'form-control', 'label' => false, 'empty' => false]) ?>
    </div>
</div>
<div class="form-group">
    <div class="col-md-3 col-md-offset-4">
        <button type="button" class="btn btn-primary submit-po"><?= __('Create') ?></button>
    </div>
</div>
<?php
    echo $this->Form->end();
?>