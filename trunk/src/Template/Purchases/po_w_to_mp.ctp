
<?php
$id = '';
$emails = [];
$phones = [];
$faxs = [];

$local = $this->request->session()->read('tb_field');
$this->assign('title', 'PO Details');
?>
<div class="row">
    <div class="col-md-12">
        <ol class="breadcrumb">
            <li><?= $this->Html->link(__('HOME'), ['controller' => 'users', 'action' => 'index']) ?></li>
            <li><?= $this->Html->link(__('Purchase Order'), ['action' => 'index']) ?></li>
            <li class="active"><?php echo $this->fetch('title'); ?></li>
        </ol>
    </div>
</div>
<?php //Temporary not use //echo $this->element('Form/po'); ?>
<div class="row">
    <div class="col-md-6">
        <h1><?= __('Order Form') ?></h1>
        <p><strong><?= __('Order Form No.') ?></strong> [purchases.po_number]</p>
        <p><strong><?= __('Issue Date:') ?></strong> [purchases.issue_date]</p>
    </div>
    <div class="col-md-6 text-right">
        <div class="company-invoice-logo text-center">
            Logo Here
        </div>
        <p>3791 Jalan Bukit Merah, #10-17 E-Centre @ Redhill Singapore 159471</p>
        <P>Tel: +65-6274-0433 / Fax: +65-6274-0477</P>
        <P>UEN No: 201403392G / Reg No: 201403392G</P>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <hr class="hr-invoiced">
    </div>
</div>
<div class="row row-top-space">
    <div class="col-md-8">
        <div class="seller-info">
            <h3>
                <?php
                switch ($data->seller->type) {
                    case TYPE_SUPPLIER:
                        $id = $data->seller->supplier->id;
                        echo $data->seller->supplier->name . $local;
                        break;
                    default :
                        $id = $data->seller->manufacturer->id;
                        echo $data->seller->manufacturer->name . $local;
                }
                ?>
            </h3>
            <div class="row">
                <div class="col-md-2"><p><?= __('TXT_ADDRESS') ?></p></div>
                <div class="col-md-10">
                    <?php
                    switch ($data->seller->type) {
                        case TYPE_SUPPLIER:
                            echo $this->Comment->address($local, $data->seller->supplier);
                            break;
                        default :
                            echo $this->Comment->address($local, $data->seller->manufacturer);
                    }
                    ?>
                </div>
            </div>
            <div class="row invoice-form-row">
                <div class="col-md-2"><div class="invoice-form-label"><?= __('TXT_CONTACT') ?></div></div>
                <div class="col-md-4 custom-select">
                    <?= $this->Form->select('seller_id', $person, [
                        'class' => 'form-control invoice-contact',
                        'value' => $id,
                        'label' => false
                    ]) ?>
                </div>
            </div>
            <div class="row invoice-form-row">
                <div class="col-md-2"><div class="invoice-form-label"><?= __('TXT_EMAIL') ?></div></div>
                <div class="col-md-4 custom-select">
                    <?php
                        $options = ['xxs@email.com', 'xxs@email.com', 'xxs@email.com', 'xxs@email.com'];
                    ?>
                    <?= $this->Form->select('email', $options, [
                        'class' => 'form-control invoice-email',
                        'label' => false,
                    ]) ?>
                </div>
            </div>
            <div class="row invoice-form-row">
                <div class="col-md-2">
                    <div class="invoice-form-label"><?= __('Tel:') ?></div>
                </div>
                <div class="col-md-4">
                    <?php
                        $options = ['+xx-xx-xx', '+xx-xx-xx', '+xx-xx-xx', '+xx-xx-xx'];
                    ?>
                    <?= $this->Form->select('tel', $options, ['class' => 'form-control invoice-tel', 'label' => false]) ?>
                    
                </div>
                <div class="col-md-1" style="padding-right: 0;">
                    <div class="invoice-form-label">/ <?= __('Fax:') ?></div>
                </div>
                <div class="col-md-4">
                    <?= $this->Form->select('fax', $options, ['class' => 'form-control invoice-tel', 'label' => false]) ?>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row row-invoice-incharge">
    <div class="col-md-offset-8 col-md-4">
        <p>Order in Charge: #xxx</p>
        <p>Email: #xxx</p>
        <p>Mobile: #xxx</p>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <table class="table table-striped table-bordered table-invoice">
            <thead>
                <tr>
                    <th><?= __('No') ?></th>
                    <th width="35%"><?= __('Description') ?></th>
                    <th><?= __('Unit Price (Cur.)') ?></th>
                    <th><?= __('Qty') ?></th>
                    <th><?= __('Amount (Cur.)') ?></th>
                    <th width="5%"><?= __('Delete') ?></th>
                </tr>                
            </thead>
            <tbody>
                <tr>
                    <td class="modal-invoice">1</td>
                    <td class="modal-invoice">Description</td>
                    <td class="modal-invoice"><label class="label-discounted"><?= __('Discounted') ?></label> 240$</td>
                    <td class="modal-invoice">5</td>
                    <td class="modal-invoice">24,000$</td>
                    <td>
                        <button type="button" class="btn btn-danger">
                            <span class="glyphicon glyphicon-trash"></span>
                        </button>
                    </td>
                </tr>
                <tr>
                    <td class="modal-invoice">2</td>
                    <td class="modal-invoice"><label class="label-foc-child">FOC</label> Description 2</td>
                    <td class="modal-invoice">225$</td>
                    <td class="modal-invoice">12</td>
                    <td class="modal-invoice">25,000</td>
                    <td>
                        <button type="button" class="btn btn-danger">
                            <span class="glyphicon glyphicon-trash"></span>
                        </button>
                    </td>
                </tr>                
                <tr>
                    <td colspan="2">Remarks:</td>
                    <td colspan="2" class="table-text-center">52,000$</td>
                    <td>34,000$</td>
                    <td class="invoice-blank-column"></td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
<div class="row">
    <div class="col-md-4">
        <p>Medical Professionals Singapore Pte. Ltd. Peter Lim, Managing Director</p>
    </div>
    <div class="col-md-2">
        <p><?= __('Date:') ?></p>
    </div>
</div>
<div class="row">
    <div class="col-md-4">
        <p class="company-invoice-signature">Digital Signature</p>
    </div>
    <div class="col-md-2">
        <div class="input-group with-datepicker">
            <?= $this->Form->input('date', ['class' => 'form-control datepicker', 'label' => false]) ?>
            <div class="input-group-btn">
                <button type="button" class="btn btn-default trigger-calendar" aria-label="Calendar">
                    <span class="glyphicon glyphicon-calendar"></span>
                </button>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="row">
            <div class="col-md-5 col-md-offset-7">
                <!--<button type="button" class="btn btn-default btn-cancel" style="margin-right: 10px;"><?= __('Cancel') ?></button>-->
                <button type="button" class="btn btn-primary btn-register"><?= __('TXT_SAVE_AS_DRAFT') ?></button>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-4">
        <hr class="signature-line">
    </div>
    <div class="col-md-2">
        <hr class="signature-line">
    </div>
</div>
<!-- modal new item -->
<?= $this->HtmlModal->modalHeader('modalAddItem', __('TXT_PO_ADD_ITEM'), ' modal-lg') ?>
<div class="row">
    <div class="col-md-12">
        <?php
        echo $this->Form->create(isset($user) ? $user : '', [
                'class' => 'form-po-settings form-horizontal',
                'autocomplete' => 'off'
            ]
        );
        $this->Form->templates([
            'inputContainer' => '{{content}}'
        ]);
        ?>
        <div class="form-group">
            <div class="col-md-3 col-md-offset-1">
                <div class="checkbox">
                    <label>
                        <?= $this->Form->input('manual', [
                            'type' => 'checkbox',
                            'label' => false]
                        ) ?>
                        <?= __('TXT_MANUALLY_REGISTER_FOC') ?>
                    </label>
                </div>
            </div>
            <div class="col-md-5">
                <?php
                echo $this->Form->select('role', unserialize(FOC_FOR_SELECTBOX), [
                    'label' => false,
                    'class' => 'form-control',
                    'empty' => false
                ]);
                ?>
            </div>
        </div>
        <div class="form-group custom-select">
            <label for="" class="col-md-3 col-md-offset-1"><?= __('TXT_PRODUCT_NAME') ?></label>
            <div class="col-md-5">
                <?php
                    echo $this->Form->select('product', [], [
                        'type' => 'text',
                        'label' => false,
                        'id' => 'product-list',
                        'class' => 'form-control',
                        'empty' => false
                    ]);
                ?>
            </div>
            <div class="col-md-2">
                <button type="button" class="btn btn-primary btn-product-list"><?= __('TXT_PRODUCT_LIST') ?></button>
            </div>
        </div>
        <div class="form-group">
            <div class="col-md-5 col-md-offset-4">
                <?= $this->Form->textarea('remark', [
                    'class' => 'form-control',
                    'rows' => 5,
                    'placeholder' => __('STR_ENTER_REMARK'),
                ]); ?>
            </div>
        </div>
        <div class="form-group">
            <label for="" class="col-md-3 col-md-offset-1"><?= __('TXT_QUANTITY') ?></label>
            <div class="col-md-5">
                <?php
                    echo $this->Form->input('quantity', [
                        'type' => 'text',
                        'label' => false,
                        'class' => 'form-control quantity',
                        'placeholder' => __('TXT_ENTER_QUANTITY')
                    ]);
                ?>
            </div>
        </div>
        <div class="form-group">
            <label for="" class="col-md-3 col-md-offset-1"><?= __('TXT_UNIT_PRICE') ?></label>
            <div class="col-md-5">
                <?php
                    echo $this->Form->input('unit_price', [
                        'type' => 'text',
                        'label' => false,
                        'class' => 'form-control unit-price',
                        'disabled' => true,
                        'placeholder' => __('TXT_ENTER_UNIT_PRICE')
                    ]);
                ?>
            </div>
            <div class="col-md-2">
                <button type="button" class="btn btn-primary popup-discount"><?= __('TXT_VIEW_DISCOUNT') ?></button>
            </div>
        </div>
        <div class="form-group">
            <div class="col-md-5 col-md-offset-4">
                <div class="checkbox">
                    <label>
                        <?= $this->Form->input('manual', ['type' => 'checkbox', 'class' => 'chk-register-foc', 'label' => false]) ?> <?= __('Manually register FOC') ?>
                    </label>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label for="" class="col-md-3 col-md-offset-1"><?= __('TXT_EXPECTED_RECEIVING_DATE') ?></label>
            <div class="col-md-5">
                <div class="input-group with-datepicker">
                    <?= $this->Form->input('date', ['class' => 'form-control datepicker', 'label' => false]) ?>
                    <div class="input-group-btn">
                        <button type="button" class="btn btn-default trigger-calendar" aria-label="Calendar">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-group">
            <div class="col-md-offset-1 col-md-3">
                <button type="button" class="btn btn-default" data-dismiss="modal"><?= __('TXT_CANCEL') ?></button>
            </div>
            <div class="col-md-offset-3 col-md-3">
                <button type="button" class="btn btn-primary"><?= __('TXT_REGISTER') ?></button>
            </div>
        </div>
        <?= $this->Form->end() ?>
    </div>
</div>
<?= $this->HtmlModal->modalFooter(); ?>
<!-- /end modal new item -->
<!-- modal discount conditions -->
<?= $this->HtmlModal->modalHeader('modalDiscountCondition', __('Discount Conditions')) ?>
<div class="row">
    <div class="col-md-12">
        <table class="table table-striped table-view-discount">
            <thead>
                <tr>
                    <th>#</th>
                    <th><?= __('Discount Type') ?></th>
                    <th><?= __('Range Type') ?></th>
                    <th><?= __('From') ?></th>
                    <th><?= __('To') ?></th>
                    <th>&nbsp;</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>1</td>
                    <td>FOC</td>
                    <td>Quantity</td>
                    <td class="qty-from">21</td>
                    <td class="qty-to">25</td>
                    <td><button type="button" class="btn btn-danger foc-preview" data-toggle="popover" data-trigger="hover">View FOC</button></td>
                </tr>
                <tr>
                    <td>2</td>
                    <td>FOC</td>
                    <td>Quantity</td>
                    <td class="qty-from">26</td>
                    <td class="qty-to">29</td>
                    <td><button type="button" class="btn btn-danger foc-preview" data-toggle="popover" data-trigger="hover">View FOC</button></td>
                </tr>
                <tr>
                    <td>3</td>
                    <td>FOC</td>
                    <td>Quantity</td>
                    <td class="qty-from">30</td>
                    <td class="qty-to">34</td>
                    <td><button type="button" class="btn btn-danger foc-preview" data-toggle="popover" data-trigger="hover">View FOC</button></td>
                </tr>
                <tr>
                    <td>4</td>
                    <td>FOC</td>
                    <td>Quantity</td>
                    <td class="qty-from">32</td>
                    <td class="qty-to">40</td>
                    <td><button type="button" class="btn btn-danger foc-preview" data-toggle="popover" data-trigger="hover">View FOC</button></td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
<div class="row">
    <div class="col-md-12 text-center">
        <button type="button" class="btn btn-default" data-dismiss="modal"><?= __('Close') ?></button>
    </div>
</div>
<?= $this->HtmlModal->modalFooter(); ?>
<!-- /end modal discount conditions -->
<?php
    echo $this->Html->css([
        'bootstrap-datetimepicker.min',
        'jquery-ui',
        'intlTelInput',
    ]);
?>
<script>
    $(function() {
        var qty_highlight;

        $('body').find('select[name="seller_id"], select[name="email"]').select2();

        $('.datepicker').datetimepicker({
           format: 'YYYY-MM-DD'
        });

        $('.trigger-calendar').click(function() {
            $(this).parents('.with-datepicker').find('.datepicker').focus();
        });

        GetDataList();

        function GetDataList() {
            var url = '<?php echo $this->Url->build('/product-details/product-list'); ?>';
            $.ajax({
                url: url,
                type: 'get',
                dataType: 'json',
                contentType: 'application/json; charset=utf-8'
            }).done(function (data) {
                var response = data.data;
                var element = '<option value="" data-pid=""><?= __('Search...') ?></option>';
                var pid;
                if (response != null && response != undefined) {
                    $.each(response, function (i, v) {
                        pid = v.product.id;
                        var product = v.product.name<?= $this->request->session()->read('tb_field') ?> + ' ';
                        product += '(' + v.pack_size;
                        product += v.tb_pack_sizes.name<?= $this->request->session()->read('tb_field') ?> + ' x ';
                        product += v.single_unit_size;
                        product += v.single_unit.name<?= $this->request->session()->read('tb_field') ?> + ')';

                        element += '<option value="'+ v.id +'">'+ product +'</option>';
                    });
                    $('#product-list').attr('data-pid', pid);
                    $('#product-list').append(element).select2({
                        width: '100%'
                    });
                }
            });
        }

        $('body').on('click', '.modal-invoice', function() {
            $('#modalAddItem').modal({
                backdrop: 'static',
                keyboard: false
            });
        });

        $('body').on('click', '.chk-register-foc', function() {
            if ($(this).is(':checked')) {
                $('.unit-price').removeAttr('disabled', true);
            } else {
                $('.unit-price').attr('disabled', true);
            }
        });

        //view discount conditions
        $('body').on('click', '.popup-discount', function() {
            qty_highlight = $('.quantity').val();
            $('#modalDiscountCondition').on('shown.bs.modal', function() {
                $('.table-view-discount tbody tr').each(function() {
                    $(this).removeClass('row-highlight');
                    var from = parseInt($(this).find('.qty-from').text());
                    var to = parseInt($(this).find('.qty-to').text());
                    if (qty_highlight >= from && qty_highlight <= to) {
                        $(this).addClass('row-highlight');
                    }
                });
            }).modal({
                backdrop: 'static',
                keyboard: false
            });
        });

        var data = '<table class="table">';
        data += '<thead><tr><th>Product Brand</th><th>Product</th><th>Type</th><th>Value</th></tr></thead>';
        data += '<tbody><tr><td>Brand A</td><td>Product A (Packaging)</td><td>Fixed Quantity</td><td>100</td></tr>';
        data += '<tr><td>Brand A</td><td>Product A (Packaging)</td><td>Fixed Quantity</td><td>100</td></tr></tbody>';
        data += '</table>';
        var tmp = '<div class="popover" role="tooltip"><div class="arrow"></div><h3 class="popover-title"></h3>';
        tmp += '<div class="popover-content"></div></div>';
        var options = {
            placement: 'left',
            title: '<?= __('FOC Product List') ?>',
            html: true,
            content: data,
            container: 'body',
            template: tmp
        };
        $('.foc-preview').popover(options);

        //remove this function when doing real life data
        remainingTable();
        function remainingTable() {
            var idx;
            for (i = 0; i < 18; i++) {
                idx = i + 3;
                var tbRow = '<tr class="modal-invoice">';
                tbRow += '<td>' + idx + '</td>';
                tbRow += '<td>&nbsp;</td>';
                tbRow += '<td>&nbsp;</td>';
                tbRow += '<td>&nbsp;</td>';
                tbRow += '<td>&nbsp;</td>';
                tbRow += '<td>&nbsp;</td>';
                tbRow += '</tr>';
                $('.table-invoice tbody tr:last').before(tbRow);
            }
        }

        $('body').on('click', '.btn-register', function () {
            location.href = '<?= $this->Url->build(['action' => 'draft']) ?>';
        });
        $('body').on('click', '.btn-cancel', function () {
            location.href = '<?= $this->Url->build(['action' => 'index']) ?>';
        });
    });
</script>