
<?php
ini_set('memory_limit', '-1');

// create new PDF document
// $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
$pdf = new TCPDF('', 'mm', 'A4', true, 'UTF-8');
// Reset the encoding forced from tcpdf
mb_internal_encoding('UTF-8');

$font_jp = TCPDF_FONTS::addTTFfont(WWW_ROOT.'fonts/ipag.ttf');
//$pdf->SetFont($font1, '', 14);

// language
if ($this->request->session()->read('tb_field') === 'en') {
    $language = 'Helvetica';
} else {
    $language = $font_jp;
}
// test language
$language = $font_jp;

ob_clean(); // cleaning the buffer before Output()

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Nick');
$pdf->SetTitle('Purchase Order Form');
$pdf->SetSubject('Purchase Order Form');
$pdf->SetKeywords('');

// Configure header and footer
$pdf->SetPrintHeader(false);
$pdf->SetPrintFooter(false);

// set margins
$pdf->SetMargins(13, PDF_MARGIN_TOP, 13);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
    require_once(dirname(__FILE__).'/lang/eng.php');
    $pdf->setLanguageArray($l);
}
// set default font subsetting mode
$pdf->setFontSubsetting(true);
$pdf->SetFont('Helvetica', '', 8);

// page padding
$page_padding_top = 10;
$page_padding_left = 10;

$pdf->AddPage();
// header page
$pdf->SetFont($language, '', 8);

$pdf->SetFillColor(255,255,255);
//$pdf->MultiCell($width = 80, $height = 60, $text = 'Main column', $border = 1, $align = 'L', $fill=true, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = 60, $valign = 'T');

// header title
$pdf->SetFont($language, '', 20);
$pdf->Cell(170, '', '同意書', 0, false, 'C');
$pdf->ln();
$pdf->ln();
$pdf->ln();

$pdf->SetFont($language, '', 12);
$pdf->Cell('', '', 'この度、Dermaheal SRを使用するにあたり、', 0, false, 'L');
$pdf->ln();
$pdf->Cell('', '', '医療法人社団　ラナンキュラス会　麗ビューティー皮フ科クリニック', 0, false, 'L');
$pdf->ln();
$pdf->Cell('', '', '河原　麗         医師により、本品は大腸菌にて産生させたヒト遺伝子組み換えEGF（上皮細胞', 0, false, 'L');
$pdf->ln();
$pdf->Cell('', '', '成長因子）、IGF（インスリン様成長因子）、FGF（繊維芽細胞成長因子）を主成分とする注射剤', 0, false, 'L');
$pdf->ln();
$pdf->Cell('', '', 'であり、万が一、未知の宿主である大腸菌による疾患に罹患する恐れがある旨の説明を受け納得', 0, false, 'L');
$pdf->ln();
$pdf->Cell('', '', 'いたしました。', 0, false, 'L');
$pdf->ln();
$pdf->Cell('', '', 'ここに、Dermaheal SRの治療に同意いたします。', 0, false, 'L');
$pdf->ln();
$pdf->ln();
$pdf->ln();

$pdf->Cell(70, 7, '同意日　：', 0, false, 'R');
$pdf->Cell(35, 7, '平成', 0, false, 'C');
$pdf->Cell(20, 7, '年', 0, false, 'L');
$pdf->Cell(20, 7, '月', 0, false, 'L');
$pdf->Cell(20, 7, '日', 0, false, 'L');
$pdf->ln();
$pdf->Cell(70, 7, '氏　名　：', 0, false, 'R');

$pdf->Output();
