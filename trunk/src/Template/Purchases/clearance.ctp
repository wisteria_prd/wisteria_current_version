<?php $this->assign('title', 'PO Clearance'); ?>

<div class="row">
    <div class="col-md-6">
        <h1><?= __('Order Form') ?></h1>
        <p><strong><?= __('Order Form No.') ?></strong> [purchases.po_number]</p>
        <p><strong><?= __('Issue Date:') ?></strong> [purchases.issue_date]</p>
    </div>
    <div class="col-md-6 text-right">
        <div class="company-invoice-logo text-center">
            Logo Here
        </div>
        <p>3791 Jalan Bukit Merah, #10-17 E-Centre @ Redhill Singapore 159471</p>
        <P>Tel: +65-6274-0433 / Fax: +65-6274-0477</P>
        <P>UEN No: 201403392G / Reg No: 201403392G</P>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <hr class="hr-invoiced">
    </div>
</div>
<div class="row row-top-space">
    <div class="col-md-8">
        <div class="seller-info">
            <h3><?= __('Seller Name') ?></h3>
            <div class="row">
                <div class="col-md-2">
                    <p><?= __('Address 1') ?></p>
                </div>
                <div class="col-md-10">
                    #1
                </div>
            </div>
            <div class="row">
                <div class="col-md-2">
                    <p><?= __('Address 2') ?></p>
                </div>
                <div class="col-md-10">
                    #2
                </div>
            </div>
            <div class="row">
                <div class="col-md-2">
                    <p><?= __('Address 3') ?></p>
                </div>
                <div class="col-md-10">
                    #3
                </div>
            </div>
            <div class="row">
                <div class="col-md-2">
                    <div><?= __('Contact:') ?></div>
                </div>
                <div class="col-md-10">
                    <p>Mr. Daroath</p>
                </div>
            </div>
            <div class="row">
                <div class="col-md-2">
                    <div><?= __('Email:') ?></div>
                </div>
                <div class="col-md-10">
                    <p>daroath.ouk.bitex@gmail.com</p>
                </div>
            </div>
            <div class="row">
                <div class="col-md-2">
                    <div><?= __('Tel:') ?></div>
                </div>
                <div class="col-md-10">
                    <p>081 620 630 / <?= __('Fax:') ?> 081 620 630</p>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row row-invoice-incharge">
    <div class="col-md-offset-8 col-md-4">
        <p>Order in Charge: #xxx</p>
        <p>Email: #xxx</p>
        <p>Mobile: #xxx</p>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <table class="table table-striped table-bordered table-invoice">
            <thead>
                <tr>
                    <th><?= __('No') ?></th>
                    <th width="35%"><?= __('Description') ?></th>
                    <th><?= __('Unit Price (Cur.)') ?></th>
                    <th><?= __('Qty') ?></th>
                    <th><?= __('Amount (Cur.)') ?></th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td class="modal-invoice">1</td>
                    <td class="modal-invoice">Description</td>
                    <td class="modal-invoice"><label class="label-discounted"><?= __('Discounted') ?></label> 240$</td>
                    <td class="modal-invoice">5</td>
                    <td class="modal-invoice">24,000$</td>
                </tr>
                <tr>
                    <td class="modal-invoice">2</td>
                    <td class="modal-invoice"><label class="label-foc-child">FOC</label> Description 2</td>
                    <td class="modal-invoice">225$</td>
                    <td class="modal-invoice">12</td>
                    <td class="modal-invoice">25,000</td>
                </tr>
                <tr>
                    <td colspan="2">Remarks:</td>
                    <td colspan="2" class="table-text-center">52,000$</td>
                    <td>34,000$</td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
<div class="row">
    <div class="col-md-4">
        <p>Medical Professionals Singapore Pte. Ltd. Peter Lim, Managing Director</p>
    </div>
    <div class="col-md-2">
        <p><?= __('Date:') ?></p>
    </div>
</div>
<div class="row">
    <div class="col-md-4">
        <p class="company-invoice-signature">Digital Signature</p>
    </div>
    <div class="col-md-2">
        <p class="text-center">12/06/2017</p>
    </div>
</div>
<div class="row">
    <div class="col-md-4">
        <hr class="signature-line-approving">
    </div>
    <div class="col-md-2">
        <hr class="signature-line-approving">
    </div>
</div>
<div class="payment" style="top: -75px;">
    <div class="row">
        <div class="col-md-5 col-md-push-7">
            <div class="row">
                <div class="col-md-offset-3 col-md-4">
                    <?= __('Current Status') ?>
                </div>
            </div>
            <div class="row row-top-small-space">
                <div class="col-md-offset-3 col-md-4">
                    <label data-status="draft" class="status label-draft"><?= __('Custom Clearance') ?></label>
                    <span class="glyphicon glyphicon-arrow-right arrow-draft" style="top: -3px;"></span>
                </div>
                <div class="col-md-5 text-right">
                    <?php
                        $options = ['Inspecting', 'Cancel'];
                    ?>
                    <?= $this->Form->select('display', $options, ['class' => 'form-control change-invoice-status', 'label' => false]) ?>
                </div>
            </div>
            <div class="row row-top-small-space">
                <div class="col-md-4 col-md-push-8 text-right">
                    <button type="button" class="btn btn-primary btn-submit-approving"><?= __('Submit') ?></button>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- modal new item -->
<?= $this->HtmlModal->modalHeader('modalAddItem', __('TXT_PO_ADD_ITEM'), ' modal-lg') ?>
<div class="row">
    <div class="col-md-12">
        <?php
            echo $this->Form->create(isset($user) ? $user : '', [
                    'class' => 'form-po-settings form-horizontal',
                    'autocomplete' => 'off'
                ]
            );
            $this->Form->templates([
                'inputContainer' => '{{content}}'
            ]);
        ?>
        <div class="form-group">
            <div class="col-md-3 col-md-offset-1">
                <div class="checkbox">
                    <label>
                        <?= $this->Form->input('manual', ['type' => 'checkbox', 'label' => false]) ?> <?= __('Manually register FOC') ?>
                    </label>
                </div>
            </div>
            <div class="col-md-5">
                <?php
                    $options = ['PO', 'Brand', 'Product'];
                    echo $this->Form->select('role', $options, [
                        'label' => false,
                        'class' => 'form-control',
                        'empty' => false
                    ]);
                ?>
            </div>
        </div>
        <div class="form-group">
            <label for="" class="col-md-3 col-md-offset-1"><?= __('Product') ?></label>
            <div class="col-md-5">
                <?php
                    echo $this->Form->input('product', [
                        'type' => 'text',
                        'label' => false,
                        'class' => 'form-control',
                        'placeholder' => __('Search Keyword')
                    ]);
                ?>
            </div>
            <div class="col-md-2">
                <button type="button" class="btn btn-primary btn-product-list"><?= __('Product List') ?></button>
            </div>
        </div>
        <div class="form-group">
            <div class="col-md-5 col-md-offset-4">
                <textarea rows="5" class="form-control"></textarea>
            </div>
        </div>
        <div class="form-group">
            <label for="" class="col-md-3 col-md-offset-1"><?= __('Quantity') ?></label>
            <div class="col-md-5">
                <?php
                    echo $this->Form->input('quantity', [
                        'type' => 'text',
                        'label' => false,
                        'class' => 'form-control quantity',
                        'placeholder' => __('Input Quantity')
                    ]);
                ?>
            </div>
        </div>
        <div class="form-group">
            <label for="" class="col-md-3 col-md-offset-1"><?= __('Unit Price') ?></label>
            <div class="col-md-5">
                <?php
                    echo $this->Form->input('unit_price', [
                        'type' => 'text',
                        'label' => false,
                        'class' => 'form-control unit-price',
                        'disabled' => true,
                        'placeholder' => __('xxx')
                    ]);
                ?>
            </div>
            <div class="col-md-2">
                <button type="button" class="btn btn-primary popup-discount"><?= __('View Discount') ?></button>
            </div>
        </div>
        <div class="form-group">
            <div class="col-md-5 col-md-offset-4">
                <div class="checkbox">
                    <label>
                        <?= $this->Form->input('manual', ['type' => 'checkbox', 'class' => 'chk-register-foc', 'label' => false]) ?> <?= __('Manually register FOC') ?>
                    </label>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label for="" class="col-md-3 col-md-offset-1"><?= __('Expected Receiving Date') ?></label>
            <div class="col-md-5">
                <div class="input-group with-datepicker">
                    <?= $this->Form->input('date', ['class' => 'form-control datepicker', 'label' => false]) ?>
                    <div class="input-group-btn">
                        <button type="button" class="btn btn-default trigger-calendar" aria-label="Calendar">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-group">
            <div class="col-md-offset-1 col-md-3">
                <button type="button" class="btn btn-default" data-dismiss="modal"><?= __('Cancel') ?></button>
            </div>
            <div class="col-md-offset-3 col-md-3">
                <button type="button" class="btn btn-primary"><?= __('Submit') ?></button>
            </div>
        </div>
        <?= $this->Form->end() ?>
    </div>
</div>
<?= $this->HtmlModal->modalFooter(); ?>
<!-- /end modal new item -->
<!-- modal discount conditions -->
<?= $this->HtmlModal->modalHeader('modalDiscountCondition', __('Discount Conditions')) ?>
<div class="row">
    <div class="col-md-12">
        <table class="table table-striped table-view-discount">
            <thead>
                <tr>
                    <th>#</th>
                    <th><?= __('Discount Type') ?></th>
                    <th><?= __('Range Type') ?></th>
                    <th><?= __('From') ?></th>
                    <th><?= __('To') ?></th>
                    <th>&nbsp;</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>1</td>
                    <td>FOC</td>
                    <td>Quantity</td>
                    <td class="qty-from">21</td>
                    <td class="qty-to">25</td>
                    <td><button type="button" class="btn btn-danger foc-preview" data-toggle="popover" data-trigger="hover">View FOC</button></td>
                </tr>
                <tr>
                    <td>2</td>
                    <td>FOC</td>
                    <td>Quantity</td>
                    <td class="qty-from">26</td>
                    <td class="qty-to">29</td>
                    <td><button type="button" class="btn btn-danger foc-preview" data-toggle="popover" data-trigger="hover">View FOC</button></td>
                </tr>
                <tr>
                    <td>3</td>
                    <td>FOC</td>
                    <td>Quantity</td>
                    <td class="qty-from">30</td>
                    <td class="qty-to">34</td>
                    <td><button type="button" class="btn btn-danger foc-preview" data-toggle="popover" data-trigger="hover">View FOC</button></td>
                </tr>
                <tr>
                    <td>4</td>
                    <td>FOC</td>
                    <td>Quantity</td>
                    <td class="qty-from">32</td>
                    <td class="qty-to">40</td>
                    <td><button type="button" class="btn btn-danger foc-preview" data-toggle="popover" data-trigger="hover">View FOC</button></td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
<div class="row">
    <div class="col-md-12 text-center">
        <button type="button" class="btn btn-default" data-dismiss="modal"><?= __('Close') ?></button>
    </div>
</div>
<?= $this->HtmlModal->modalFooter(); ?>
<!-- /end modal discount conditions -->
<?php
    echo $this->Html->css([
        'bootstrap-datetimepicker.min',
        'jquery-ui',
        'intlTelInput',
    ]);
?>
<script>
    $(document).ready(function() {
        var qty_highlight;
        $('.datepicker').datetimepicker({
            format: 'YYYY-MM-DD',
            minDate: '2007-01-01'
        });

        var msg = '<p><?= __('Are you sure to change the status to Inspecting') ?></p>';
        msg += '<div class="text-center">';
        msg += '<button class="btn btn-default confirm-yes" type="button">Yes</button>';
        msg += '<button class="btn btn-default confirm-no" type="button">No</button>';
        msg += '</div>';
        var tmp = '<div class="popover" role="tooltip"><div class="arrow"></div><h3 class="popover-title"></h3>';
        tmp += '<div class="popover-content"></div></div>';
        var options = {
            placement: 'left',
            title: '<?= __('Confirm Custom Clearance') ?>',
            html: true,
            content: msg,
            container: 'body',
            template: tmp
        };
        $('.btn-submit-approving').popover(options);

        $('.trigger-calendar').click(function() {
            $(this).parents('.with-datepicker').find('.datepicker').focus();
        });

        $('body').on('click', '.add-new-file', function() {
            $('#modalPDFUpload').modal({
                backdrop: 'static',
                keyboard: false
            });
        });

        $('body').on('click', '.modal-invoice', function() {
            $('#modalAddItem').modal({
                backdrop: 'static',
                keyboard: false
            });
        });

        $('body').on('click', '.confirm-no', function() {
            $('.btn-submit-approving').click();
        });

        $('body').on('click', '.confirm-yes', function() {
            location.href='<?= $this->Url->build(['action' => 'inspecting']) ?>';
            $('.btn-submit-approving').click();
        });

        $('body').on('click', '.chk-register-foc', function() {
            if ($(this).is(':checked')) {
                $('.unit-price').removeAttr('disabled', true);
            } else {
                $('.unit-price').attr('disabled', true);
            }
        });

        //view discount conditions
        $('body').on('click', '.popup-discount', function() {
            qty_highlight = $('.quantity').val();
            $('#modalDiscountCondition').on('shown.bs.modal', function() {
                $('.table-view-discount tbody tr').each(function() {
                    $(this).removeClass('row-highlight');
                    var from = parseInt($(this).find('.qty-from').text());
                    var to = parseInt($(this).find('.qty-to').text());
                    if (qty_highlight >= from && qty_highlight <= to) {
                        $(this).addClass('row-highlight');
                    }
                });
            }).modal({
                backdrop: 'static',
                keyboard: false
            });
        });

        var data = '<table class="table">';
        data += '<thead><tr><th>Product Brand</th><th>Product</th><th>Type</th><th>Value</th></tr></thead>';
        data += '<tbody><tr><td>Brand A</td><td>Product A (Packaging)</td><td>Fixed Quantity</td><td>100</td></tr>';
        data += '<tr><td>Brand A</td><td>Product A (Packaging)</td><td>Fixed Quantity</td><td>100</td></tr></tbody>';
        data += '</table>';
        var tmp = '<div class="popover" role="tooltip"><div class="arrow"></div><h3 class="popover-title"></h3>';
        tmp += '<div class="popover-content"></div></div>';
        var options = {
            placement: 'left',
            title: '<?= __('FOC Product List') ?>',
            html: true,
            content: data,
            container: 'body',
            template: tmp
        };
        $('.foc-preview').popover(options);

        //remove this function when doing real life data
        remainingTable();
        function remainingTable() {
            var idx;
            for (i = 0; i < 18; i++) {
                idx = i + 3;
                var tbRow = '<tr class="modal-invoice">';
                tbRow += '<td>' + idx + '</td>';
                tbRow += '<td>&nbsp;</td>';
                tbRow += '<td>&nbsp;</td>';
                tbRow += '<td>&nbsp;</td>';
                tbRow += '<td>&nbsp;</td>';
                tbRow += '</tr>';
                $('.table-invoice tbody tr:last').before(tbRow);
            }
        }
    });
</script>
