
<?php
$this->assign('title', 'PO Normal');
$userGroup = $this->request->session()->read('user_group');
$local = $this->request->session()->read('tb_field');
$fax = '';

//Total Amount
$totalAmount = 0;
?>
<div class="row">
    <div class="col-md-12">
        <ol class="breadcrumb">
            <li><?= $this->Html->link(__('HOME'), ['controller' => 'users', 'action' => 'index']) ?></li>
            <li><?= $this->Html->link('To MP', ['action' => 'index']) ?></li>
            <li class="active">PO</li>
        </ol>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <h1><?= __('TXT_PO_FORM') ?></h1>
        <p><strong><?= __('TXT_ORDER_FORM_NUMBER') ?></strong>&nbsp;<?= $data->sale_number ?></p>
        <p>
            <strong><?= __('TXT_ISSUE_DATE') ?>:</strong>&nbsp;
            <?php
            if ($data->issue_date) {
                echo date('Y年m月d日 ', strtotime($data->issue_date));
            }
            ?>
        </p>
    </div>
    <div class="col-md-6 text-right">
        <div class="company-invoice-logo text-center">
            Logo Here
        </div>
        <p>3791 Jalan Bukit Merah, #10-17 E-Centre @ Redhill Singapore 159471</p>
        <P>Tel: +65-6274-0433 / Fax: +65-6274-0477</P>
        <P>UEN No: 201403392G / Reg No: 201403392G</P>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <hr class="hr-invoiced">
    </div>
</div>
<div class="row row-top-space">
    <div class="col-md-8">
        <div class="seller-info">
            <h3>
                <?php
                $sellerName = '';
                switch ($data->seller->type) {
                    case TYPE_SUPPLIER:
                        $id = $data->seller->supplier->id;
                        $fax = $data->seller->supplier->fax;
                        $sellerName = $data->seller->supplier->name . $local;
                        break;
                    default :
                        $id = $data->seller->manufacturer->id;
                        $fax = $data->seller->manufacturer->fax;
                        $sellerName = $data->seller->manufacturer->name . $local;
                }
                echo $sellerName;
                ?>
            </h3>
            <div class="row">
                <div class="col-md-3"><p><?= __('TXT_ADDRESS') ?></p></div>
                <div class="col-md-9">
                    <?php
                    switch ($data->seller->type) {
                        case TYPE_SUPPLIER:
                            echo $this->Comment->address($local, $data->seller->supplier);
                            break;
                        default :
                            echo $this->Comment->address($local, $data->seller->manufacturer);
                    }
                    ?>
                </div>
            </div>
            <div class="row invoice-form-row">
                <div class="col-md-3"><div class="invoice-form-label"><?= __('TXT_CONTACT') ?></div></div>
                <div class="col-md-4">
                    <?php
                    echo $this->Form->input('seller_id', [
                        'type' => 'text',
                        'class' => 'form-control select-seller',
                        'readonly' => 'readonly',
                        'label' => false,
                        'value' => $sellerName,
                    ]);
                    ?>
                </div>
            </div>
            <div class="row invoice-form-row">
                <div class="col-md-3"><div class="invoice-form-label"><?= __('TXT_EMAIL') ?></div></div>
                <div class="col-md-4">
                    <?php echo $this->Form->input('email', [
                        'type' => 'text',
                        'class' => 'form-control',
                        'label' => false,
                        'readonly' => 'readonly',
                        'value' => $data->email,
                        'templates' => [
                            'inputContainer' => '{{content}}',
                        ],
                    ]); ?>
                </div>
            </div>
            <div class="row invoice-form-row">
                <div class="col-md-3 custom-select">
                    <div class="invoice-form-label"><?= __('TXT_TEL') ?></div>
                </div>
                <div class="col-md-4">
                    <?php echo $this->Form->input('tel', [
                        'type' => 'text',
                        'class' => 'form-control select-tel',
                        'label' => false,
                        'readonly' => 'readonly',
                        'value' => $data->tel,
                        'templates' => [
                            'inputContainer' => '{{content}}',
                        ],
                    ]); ?>
                </div>
                <div class="col-md-1" style="padding-right: 0;">
                    <div class="invoice-form-label">/ <?= __('TXT_FAX') ?></div>
                </div>
                <div class="col-md-4">
                    <?php echo $this->Form->input('fax', [
                        'type' => 'text',
                        'class' => 'form-control txt-fax',
                        'label' => false,
                        'readonly' => 'readonly',
                        'placeholder' => __('TXT_ENTER_FAX'),
                        'value' => $fax,
                        'templates' => [
                            'inputContainer' => '{{content}}',
                        ],
                    ]); ?>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row row-invoice-incharge">
    <div class="col-md-offset-8 col-md-4">
        <p>Order in Charge: #xxx</p>
        <p>Email: #xxx</p>
        <p>Mobile: #xxx</p>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <table class="table table-striped table-bordered table-po">
            <thead>
                <tr>
                    <th><?= __('No') ?></th>
                    <th width="35%"><?= __('TXT_DESCRIPTION') ?></th>
                    <th><?= __('TXT_UNIT_PRICE') ?></th>
                    <th><?= __('TXT_QUANTITY') ?></th>
                    <th colspan="2"><?= __('TXT_AMOUNT') ?></th>
                </tr>
            </thead>
            <tbody>

                <?php
                if ($saleDetail) {
                    $num = 0;

                    if ($data->sale_details[0]->count <= 20) {
                        for ($i = 0; $i <= ((20 - $data->sale_details[0]->count) + count($saleDetail)); $i++) {
                            $num += 1;

                            if (($i + 1) <= count($saleDetail)) {
                                $item = $saleDetail[$i];
                                $childs = $item->children;
                                $amount1 = intval($item->unit_price) * intval($item->quantity);
                                $totalAmount += $amount1;
                                $is_discount = ($item->is_discount == 1) ? '<label class="label-foc-child no-margin">Discounted</label>&nbsp;' : null;
                                $packClass = 'label-success';
                                if ($item->sale_stock_details) {
                                    $packClass = 'label-danger';
                                }

                                // display parent record
                                echo '<tr data-id="' . $item->id . '" data-type="parent" data-target="edit">' .
                                '<td class="text-center">' . $num . '</td>' .
                                '<td>'
                                . $this->Comment->POProductDetailName($item) .
                                '<label class="' . $packClass . ' label-pack no-margin pull-right">Pack</label>' .
                                '</td>' .
                                '<td>' . $is_discount . $data->currency->code . ' ' . $this->Comment->numberFormat($item->unit_price) . '</td>' .
                                '<td>' . $item->quantity . '</td>' .
                                '<td colspan="2">' . $data->currency->code . ' ' . $this->Comment->numberFormat((intval($item->unit_price) * intval($item->quantity))) . '</td></tr>';

                                // display parent record
                                if ($childs) {
                                    foreach ($childs as $key => $value) {
                                        $is_discount1 = ($value->is_discount == 1) ? '<label class="label-foc-child no-margin">Discounted</label>&nbsp;' : null;
                                        $num += 1;
                                        $amount2 = intval($value->unit_price) * intval($value->quantity);
                                        $totalAmount += $amount2;
                                        $packClass1 = 'label-success';
                                        if ($value->sale_stock_details) {
                                            $packClass1 = 'label-danger';
                                        }

                                        echo '<tr data-id="' . $value->id . '" data-type="child" data-target="edit">' .
                                        '<td class="text-center">' . $num . '</td>' .
                                        '<td><label class="label-foc-child no-margin">FOC</label>&nbsp;'
                                        . $this->Comment->POProductDetailName($value) .
                                        '<label class="'. $packClass1 . ' label-pack no-margin pull-right">Pack</label>' .
                                        '</td>' .
                                        '<td>' . $is_discount1 . $data->currency->code . ' ' . $this->Comment->numberFormat($value->unit_price) . '</td>' .
                                        '<td>' . $value->quantity . '</td>' .
                                        '<td colspan="2">' . $data->currency->code . ' ' . $this->Comment->numberFormat((intval($value->unit_price) * intval($value->quantity))) . '</td></tr>';
                                    }
                                }
                            } else {
                                echo '<tr data-id="" data-target="new" data-type="">' .
                                '<td class="text-center">' . $num . '</td>' .
                                '<td>&nbsp;</td>' .
                                '<td>&nbsp;</td>' .
                                '<td>&nbsp;</td>' .
                                '<td colspan="2">&nbsp;</td></tr>';
                            }
                        }
                    } else {
                        foreach ($saleDetail as $key => $value) {
                            $num += 1;
                            $amount3 = intval($value->unit_price) * intval($value->quantity);
                            $totalAmount += $amount3;
                            $childs = $value->children;
                            $packClass3 = 'label-success';
                            if ($value->sale_stock_details) {
                                $packClass3 = 'label-danger';
                            }

                            // display parent record
                            echo '<tr data-id="' . $value->id . '" data-type="parent" data-target="edit">' .
                            '<td class="text-center">' . $num . '</td>' .
                            '<td>'
                            . $this->Comment->POProductDetailName($value) .
                            '<label class="' . $packClass3 . ' label-pack no-margin pull-right">Pack</label>' .
                            '</td>' .
                            '<td>' . $data->currency->code . ' ' . $this->Comment->numberFormat($value->unit_price) . '</td>' .
                            '<td>' . $value->quantity . '</td>' .
                            '<td colspan="2">' . $data->currency->code . ' ' . $this->Comment->numberFormat((intval($value->unit_price) * intval($value->quantity))) . '</td></tr>';

                            // display parent record
                            if ($childs) {
                                foreach ($childs as $key1 => $value1) {
                                    $num += 1;
                                    $amount4 = intval($value1->unit_price) * intval($value1->quantity);
                                    $totalAmount += $amount4;
                                    $packClass4 = 'label-success';
                                    if ($value1->sale_stock_details) {
                                        $packClass4 = 'label-danger';
                                    }

                                    echo '<tr data-id="' . $value1->id . '" data-type="child" data-target="edit">' .
                                    '<td class="text-center">' . $num . '</td>' .
                                    '<td><label class="label-foc-child no-margin">FOC</label>&nbsp;'
                                    . $this->Comment->POProductDetailName($value1) .
                                    '<label class="' . $packClass4 . ' label-pack no-margin pull-right">Pack</label>' .
                                    '</td>' .
                                    '<td>' . $data->currency->code . ' ' . $this->Comment->numberFormat($value1->unit_price) . '</td>' .
                                    '<td>' . $value1->quantity . '</td>' .
                                    '<td colspan="2">' . $data->currency->code . ' ' . $this->Comment->numberFormat((intval($value1->unit_price) * intval($value1->quantity))) . '</td></tr>';
                                }
                            }
                        }
                    }
                } else {
                    for ($i=1; $i<=20; $i++) {
                        echo '<tr data-target="new" data-id="" data-type="">' .
                            '<td>' . $i . '</td>' .
                            '<td>&nbsp;</td>' .
                            '<td>&nbsp;</td>' .
                            '<td>&nbsp;</td>' .
                            '<td colspan="2">&nbsp;</td>' .
                            '</tr>';
                    }
                }
                ?>

                <!-- Table Footer -->
                <tr class="table-po-footer">
                    <td colspan="2" style="vertical-align: top;"><b>Remark :</b></td>
                    <td colspan="2" class="table-text-right"><b><?= __('TXT_TOTAL') ?> :</b></td>
                    <td colspan="2" style="border: 1px solid #ddd !important;">
                        <?php
                        echo $data->currency->code . ' ' . $this->Comment->numberFormat($totalAmount);
                        ?>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
<div class="row">
    <div class="col-md-8 col-sm-8">
        <div class="row">
            <div class="col-md-8">
                <p>Medical Professionals Singapore Pte. Ltd. Peter Lim, Managing Director</p>
                <p class="company-invoice-signature">Digital Signature</p>
                <hr class="signature-line">
            </div>
            <div class="col-md-4">
                <p>Date:</p>
                <div class="input-group with-datepicker">
                    <div class="input text"><input type="text" name="date" class="form-control datepicker" id="date"></div>            <div class="input-group-btn">
                        <button type="button" class="btn btn-default trigger-calendar" aria-label="Calendar">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </button>
                    </div>
                </div>
                <p></p>
                <hr class="signature-line">
            </div>
        </div>
    </div>
    <div class="col-md-4 col-sm-4">
        <?= $this->element('po_status_normal') ?>
    </div>
</div>

<!--Modal Add New SaleStockDetails-->
<div class="modal fade modal-pack" role="dialog" data-backdrop="static">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Add Sale Stock Detail</h4>
            </div>
            <div class="modal-body modal-body-fix"></div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default btn-sm btn-width close-modal" data-dismiss="modal"><?= __('BTN_CANCEL') ?></button>
                <button type="button" class="btn btn-primary btn-sm btn-width btn-pack-submit"><?= __('TXT_REGISTER') ?></button>
            </div>
        </div>
    </div>
</div>
<!--Modal Show Update Sub-status Success-->
<div class="modal fade" id="modal-sub-status" role="dialog" data-backdrop="static">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-body"><?= __('TXT_UPDATE_SUB_STATUS') ?></div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary btn-sm btn-width close-modal" data-dismiss="modal"><?= __('TXT_YES') ?></button>
            </div>
        </div>
    </div>
</div>
<!--Modal Upload PDF File-->
<div class="modal fade" id="upload-pdf" role="dialog" data-backdrop="static">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"><?= __('TXT_DOCUMENT_UPLOAD') ?></h4>
            </div>
            <div class="modal-body">
                <?php
                echo $this->Form->create(null, [
                    'onsubmit' => 'return false;',
                    'role' => 'form',
                    'class' => 'form-horizontal',
                    'type' => 'file',
                    'name' => 'pdf-form',
                ]);
                $this->Form->templates([
                    'inputContainer' => '<div class="col-sm-8">{{content}}</div>',
                ]);
                echo $this->Form->hidden('seller_id', ['value' => $data->id]);
                ?>
                <div class="form-group">
                    <label class="col-sm-4 control-label"><?= __('TXT_TYPE') ?></label>
                    <?php
                    echo $this->Form->input('document_type', [
                        'type' => 'select',
                        'class' => 'form-control',
                        'label' => false,
                        'required' => false,
                        'options' => $typePdf,
                        'id' => false,
                    ]);
                    ?>
                </div>
                <div class="form-group">
                    <label class="col-sm-4 control-label"><?= __('TXT_PDF_FILE') ?></label>
                    <?php
                    echo $this->Form->input('file', [
                        'type' => 'file',
                        'class' => 'form-control',
                        'label' => false,
                        'required' => false,
                        'options' => $typePdf,
                    ]);
                    ?>
                </div>
                <?php echo $this->Form->end(); ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default btn-sm btn-width close-modal" data-dismiss="modal"><?= __('BTN_CANCEL') ?></button>
                <button type="button" class="btn btn-primary btn-sm btn-width btn-upload-file"><?= __('TXT_DOCUMENT_UPLOAD') ?></button>
            </div>
        </div>
    </div>
</div>

<?php
echo $this->Html->css([
    'bootstrap-datetimepicker.min',
    'jquery-ui',
    'intlTelInput',
]);
?>

<style>
    .label-non-pack {
        padding: 3px 5px;
        font-size: 12px;
        border-radius: 2px;
        width: 60px;
        text-align: center;
        color: #fff;
        cursor: pointer;
    }
</style>

<script>
    $(function() {
        var local = '<?= $this->request->session()->read('tb_field') ?>';
        var optionData = [];

        $('.datepicker, #delivery-date').datetimepicker({
            format: 'YYYY-MM-DD'
        });

        $('.trigger-calendar').click(function() {
            $(this).parents('.with-datepicker').find('.datepicker').focus();
        });

        $('body').on('click', '.btn-update-delivery', function(e) {
            $.LoadingOverlay('show');
            var url = '<?= $this->Url->build('/deliveries/create/') ?>';
            var form = $('body').find('form[name="sale_status"]');
            var user_group = JSON.parse('<?= json_encode($userGroup) ?>');
            var params = {
                tracking: $(form).find('#tracking').val(),
                delivery_date: $(form).find('#delivery-date').val(),
                sale_id: '<?= $data->id ?>',
                affiliation_class: user_group.affiliation_class,
                id: $(form).find('#delivery-id').val()
            };

            ajax_request_post(url, params, function(data) {
                $(form).find('.error-message').remove();

                if (data.message === '<?= MSG_ERROR ?>') {
                    $.each(data.data, function(i, v) {
                        var msg = '<label class="error-message">' + v + '</label>';
                        $(form).find('input[name="' + i + '"]').closest('.col-sm-6').append(msg);
                    });
                } else {
                    $('body').find('.btn-register').removeAttr('disabled', true);
                }
            }, '<?= __('TXT_SESSION_TIMEOUT') ?>');
        });

        $('body').on('click', '.browse-file-edit', function(e) {
            var modal = $('body').find('#upload-pdf');
            var params = {
                document_type: $(this).closest('li').attr('data-document-type'),
                name: $(this).closest('li').attr('data-name'),
                type: $(this).closest('li').attr('data-type')
            };
            var fileName = '<h4 class="file-name text-center"><span class="label label-success">' + params.name + '</span></h4>';

            $(modal).find('select[name="document_type"]').val(params.document_type);
            $(modal).find('form').append(fileName);
            $(modal).modal('show');
        });

        $('#upload-pdf').on('hidden.bs.modal', function (e) {
            $(this).find('select[name="document_type"]').val('');
            $(this).find('.file-name').remove();
        });

        $('body').on('click', '.custom-wrap li .browse-file', function(e) {
            var index = $(this).closest('li').index();

            $('body').find('#upload-pdf').modal('show');
            $('body').find('#upload-pdf .btn-upload-file').attr('data-index', index);
        });

        $('body').on('click', '.rm-pdf-file', function(e) {
            e.stopPropagation();
            var li = $(this).closest('.custom-wrap li').index();

            if (li == 0) {
                $(this).closest('li').find('span:first').remove();
            } else {
                $(this).closest('li').remove();
            }
            optionData = $.grep(optionData, function(value, index) {
                return value.index != li;
            });
        });

        $('body').on('click', '.add-pdf-file', function(e) {
            e.stopPropagation();
            var content = $(this).closest('.custom-wrap');
            var li = '<li data-type="new"><span class="label label-success browse-file"><i class="fa fa-times rm-pdf-file" aria-hidden="true"></i>&nbsp;&nbsp;Browse File</span><span>&nbsp;</span></li>';
            $(content).append(li);
        });

        $('body').on('click', '.btn-upload-file', function(e) {
            var form = $('body').find('form[name="pdf-form"]');
            var index = $(this).attr('data-index');
            var arr = {
                file: $(form).find('#file')[0].files[0],
                external_id: $(form).find('input[name="seller_id"]').val(),
                document_type: $(form).find('select[name="document_type"]').val(),
                index: index
            };
            optionData.push(arr);
            $('body').find('#upload-pdf').modal('hide');
        });

        $('body').on('click', '.label-pack', function(e) {
            e.stopPropagation();
            $.LoadingOverlay('show');
            var url = '<?= $this->Url->build('/sale-stock-details/get-data/') ?>';
            var params = {
                id: $(this).closest('tr').attr('data-id')
            };

            ajax_request_get(url, params, function(data) {
                var content = $('body').find('.modal-pack');
                $(content).find('.modal-body').html(data);
                $(content).modal('show');
                $(content).find('select[name="stock_id[0]"]').select2({width: '100%'});
                var status = $(content).find('input[name="sale_status"]').val();

                if ((status === '<?= PO_STATUS_CUSTOM_CLEARANCE ?>') || (status === '<?= PO_STATUS_DELIVERED ?>') || (status === '<?= PO_STATUS_REQUESTED_CC ?>')) {
                    $(content).find('.btn-pack-submit').attr('disabled', true);
                }
            }, 'html', '<?= __('TXT_SESSION_TIMEOUT') ?>');
        });

        $('body').on('click', '.btn-stock-rm', function(e) {
            $(this).closest('.form-group').remove();
        });

        $('body').on('click', '.btn-stock-add', function(e) {
            var content = $('body').find('.modal-pack .main-wrap');
            var index = $(content).find('.form-group').length + 1;
            var stock_id = $('body').find('.modal-pack select[name="stock_id[0]"]').html();
            var element = '<div class="form-group">'
                        + '<label for="" class="col-md-4"></label>'
                        + '<div class="col-md-3 custom-select no-padding-r">'
                        + '<select class="form-control" id="stock-id-' + index + '" name="stock_id[' + index + ']">' + stock_id + '</select></div>'
                        + '<div class="col-md-3 no-padding-r"><input type="number" class="form-control" name="quantity[' + index + ']" placeholder="<?= __('TXT_ENTER_QUANTITY') ?>"/></div>'
                        + '<div class="col-md-2"><button class="btn btn-delete btn-stock-rm"><i class="fa fa-trash" aria-hidden="true"></i></button></div>'
                        + '</div>';

            $(content).append(element);
            $(content).find('#stock-id-' + index).select2({width: '100%'});
        });

        $('body').on('click', '.btn-pack-submit', function(e) {
            $.LoadingOverlay('show');
            var form = $('body').find('.modal-pack form');
            var url = '<?= $this->Url->build('/sale-stock-details/create/') ?>';

            ajax_request_post(url, $(form).serialize(), function(data) {
                var response = data.data;
                $('body').find('#warning-msg').empty();
                $(form).find('.error-message').remove();

                if (data.message === '<?= MSG_ERROR ?>') {
                    var stock_id = response.stock_id;
                    var quantity = response.quantity;

                    // error stock_id
                    $.each(stock_id, function(i, v) {
                        var label = '<label class="error-message">' + v + '</label>';
                        $(form).find('select[name="stock_id[' + i + ']"]').closest('div').append(label);
                    });

                    // error quantity
                    $.each(quantity, function(i, v) {
                        var label = '<label class="error-message">' + v + '</label>';
                        $(form).find('input[name="quantity[' + i + ']"]').closest('div').append(label);
                    });
                } else if (data.message === '<?= MSG_WARNING ?>') {
                    $('body').find('.modal-pack #warning-msg').html('<strong>' + '<?= __('TXT_INVALID_LOT_QUANTITY') ?>' + '</strong>');
                } else {
                    location.reload();
                }
            }, '<?= __('TXT_SESSION_TIMEOUT') ?>')
        });

        $('body').on('click', '.btn-register', function() {
            $.LoadingOverlay('show');
            var form = $('body').find('form[name="sale_status"]');
            var affiliationClass = '<?= $userGroup->affiliation_class ?>';
            var url = '<?= $this->Url->build(['action' => 'updateStatus']) ?>';
            var step = parseInt($(this).attr('data-step')) + 1;

            $(form).find('input[name="step"]').val(step);
            $(form).find('button[type="submit"]').attr('data-step', step);

            ajax_request_post(url, $(form).serialize(), function(data) {
                if (data.message === '<?= MSG_SUCCESS ?>') {
                    renderStep(form, data);
                }
            }, '<?= __('TXT_SESSION_TIMEOUT') ?>');
        });

        $('body').on('click', '.update-sub-status', function(e) {
            $.LoadingOverlay('show');
            var params = {
                subStatus: $('body').find('form[name="sale_status"] #sub-status').val(),
                id: '<?= $data->id ?>'
            };
            var url = '<?= $this->Url->build(['action' => 'updateSubStatus']) ?>';

            ajax_request_post(url, params, function(data) {
                if (data.message === '<?= MSG_SUCCESS ?>') {
                    $('body').find('#modal-sub-status').modal('show');
                }
            }, '<?= __('TXT_SESSION_TIMEOUT') ?>');
        });

        /**
        * Function add_option
        * use for add option to select dropdown
        * @param object data
        * @returns return string
        */
        function add_option(data)
        {
           var element = '';

           if (data !== null && data !== undefined) {
               $.each(data, function(i, v) {
                   element += '<option value="' + v.key + '">' + v.value + '</option>';
               });
           }

           return element;
        }

        /**
         * use for render html to po footer
         * @param {html} form
         * @param {object} data
         * @returns {string}
         */
        function renderStep(form, data)
        {
            var buttonSubStatus = '<button class="btn btn-sm btn-primary update-sub-status"><?= __('TXT_UPDATE') ?></button>';

            switch(parseInt(data.data.step)) {
                case 2 :
                    // Change status Requested Packing to Paid
                    var element = '<div class="form-group">' +
                                '<label class="col-sm-4 control-label text-left"><?= __('TXT_SUB_STATUS') ?></label>' +
                                '<label class="col-sm-2"></label>' +
                                '<label class="col-sm-5 control-label text-left"><?= __('TXT_PAYMENT_INFO') ?></label></div>' +
                                '<div class="form-group"><div class="col-sm-5">' +
                                '<select class="form-control" name="sub_status" id="sub-status">' +
                                '<option><?= __('TXT_SELECT_STATUS') ?></option>' +
                                '<option value="<?= STATUS_PACKED ?>"><?= __('TXT_PACKED') ?></option>' +
                                '<option value="<?= STATUS_SHIPPED ?>"><?= __('TXT_SHIPPED') ?></option></select></div>' +
                                '<div class="col-sm-1"></div>' +
                                '<div class="col-sm-6"><ul class="no-padding payment-list">' + add_payment(data.data.payments) + '</ul></div></div>' +
                                '</div>';

                    $(form).find('.label-draft').text('<?= __('TXT_REQUEST_PACKING') ?>');
                    $(form).find('.btn-register').text('<?= __('TXT_SUBMIT') ?>');
                    $(form).find('#status option:first').remove();
                    $(form).find('#status').prepend('<option selected="selected" value="<?= STATUS_PAID ?>"><?= __('TXT_PAID') ?></option>');
                    $(form).find('.status-wrap').empty().html(element);
                    $(form).find('.form-group:last div:first').append(buttonSubStatus);
                    break;
                case 3 :
                    // Change status Paid to Requested Shipment
                    $(form).find('.label-draft').text('<?= __('TXT_PAID') ?>');
                    $(form).find('.btn-register').text('<?= __('TXT_SEND_PO') ?>');
                    $(form).find('#status').empty().append('<option selected="selected" value="<?= PO_STATUS_REQUESTED_SHIPPING ?>"><?= __('TXT_REQUESTED_SHIPMENT') ?></option>');

                    // remove payment list
                    $(form).find('.status-wrap .form-group:eq(0) label:not(:first)').remove();
                    $(form).find('.status-wrap .form-group:eq(1) div:not(:first)').remove();
                    break;
                case 4 :
                    // Change status Request Shipment to Applied Custom Clearance
                    $(form).find('.label-draft').text('<?= __('TXT_REQUESTED_SHIPMENT') ?>');
                    $(form).find('.btn-register').text('<?= __('TXT_SUBMIT') ?>');
                    $(form).find('#status').empty().append('<option selected="selected" value="<?= PO_STATUS_CUSTOM_CLEARANCE ?>"><?= __('TXT_CUSTOM_CLEARANCE') ?></option>');
                    break;
                case 5 :
                   // Change status Applied Custom Clearance Approval
                    var saleDelivery = JSON.parse('<?= json_encode($saleDeliveries) ?>');
                    var arr1 = {
                        tracking: '',
                        delivery_date: '',
                    };

                    if (saleDelivery.length > 0) {
                        arr1.tracking = saleDelivery[0].delivery.tracking;
                        arr1.delivery_date = saleDelivery[0].delivery.delivery_date;
                        $(form).find('.status-wrap').append('<input type="hidden" name="delivery-id"/>')
                    }

                    var element1 = '<div class="form-group">' +
                            '<label class="col-sm-6 control-label text-left"><?= __('TXT_CUSTOM_CLEARANCE') ?></label>' +
                            '<div class="col-sm-6 col-md-6"><ul class="custom-wrap">' + add_pdf_file(data) + '</ul></div>' +
                            '</div>';
                    var element = '<div class="form-group">' +
                            '<label class="col-sm-5 control-label text-left"><?= __('TXT_TRACKING_NUMBER') ?></label>' +
                            '<div class="col-sm-1"></div>' +
                            '<div class="col-sm-6"><input type="text" name="tracking" value="' + arr1.tracking + '" class="form-control" placeholder="<?= __('TXT_TRACKING_NUMBER') ?>" id="tracking"></div></div>'+
                            '<div class="form-group">' +
                            '<label class="col-sm-5 control-label text-left"><?= __('TXT_DELIVERY_DATE') ?></label>' +
                            '<div class="col-sm-1"></div>' +
                            '<div class="col-sm-6"><input type="text" value="' + arr1.delivery_date + '" name="delivery_date" class="form-control" id="delivery-date"></div></div>' +
                            '<div class="form-group"><div class="col-sm-12 text-right"><button type="button" class="btn btn-sm btn-primary btn-update-delivery"><?= __('TXT_UPDATE') ?></button></div></div>' + element1;

                    $(form).find('.status-wrap').prepend(element);
                    $(form).find('.label-draft').text('<?= __('TXT_CUSTOM_CLEARANCE') ?>');
                    $(form).find('.btn-register').text('<?= __('TXT_SEND_PO') ?>');
                    $(form).find('#status').empty().append('<option selected="selected" value="<?= PO_STATUS_REQUESTED_CC ?>"><?= __('TXT_DELIVERING') ?></option>');
                    $(form).find('#delivery-date').datetimepicker({
                        format: 'YYYY-MM-DD'
                    });
                    break;
                case 6 :
                    // Change status Delivering to Delivered
                    $(form).find('.label-draft').text('<?= __('TXT_DELIVERING') ?>');
                    $(form).find('#status').empty().append('<option selected="selected" value="<?= PO_STATUS_DELIVERED ?>"><?= __('TXT_DELIVERED') ?></option>');
                    $(form).find('.status-wrap').empty();
                    $('body').find('.update-sub-status').remove();
                    break;
                case 7 :
                    // Change status Delivered to Completed
                    $(form).find('.label-draft').text('<?= __('TXT_DELIVERED') ?>');
                    $(form).find('.form-group:eq(1) div:not(:first)').remove();
                    $(form).find('.form-group:eq(1)').append('<span>After 7days change to ‘completed’ auto</span>');
                    $(form).find('button[type="submit"]').remove();
                    break;
            }
        }

        /**
        * use for add payment to po footer
         * @param {object} data
         * @returns {string}
         * */
        function add_payment(data)
        {
            var element = '';

            if (data !== null && data !== 'undefined') {
                $.each(data, function(i, v) {
                    element += '<li><span class="label label-success">' + v + '</span></li>&nbsp;';
                });
            }

            return element;
        }

        /**
         * use for add pdf file to po footer
         * @param {object} data
         * @returns {string}
         * */
        function add_pdf_file(data)
        {
            var element = '<li data-type="new">' +
                            '<span class="label label-success browse-file">&nbsp;&nbsp;Browse File</span><span>&nbsp;</span>' +
                            '<span class="label label-primary add-pdf-file">Add</span>' +
                            '</li>';

            if (data.data.medias !== null && data.data.medias !== 'undefined') {
                $.each(data.data.medias, function(i, v) {
                    if (i == 0) {
                        element = '<li data-id="' + v.id + '" data-name="' + v.file_name + '" data-document-type="' + v.document_type + '" data-type="edit">' +
                            '<span class="label label-success browse-file-edit">&nbsp;&nbsp;Browse File</span><span>&nbsp;</span>' +
                            '<span class="label label-primary add-pdf-file">Add</span>' +
                            '</li>';
                    } else {
                        element += '<li data-id="' + v.id + '" data-name="' + v.file_name + '" data-document-type="' + v.document_type + '" data-type="edit">' +
                            '<span class="label label-success browse-file-edit"><i class="fa fa-times rm-pdf-file" aria-hidden="true"></i>&nbsp;&nbsp;Browse File</span><span>&nbsp;</span>' +
                            '</li>';
                    }
                });
            }

            return element;
        }
    });
</script>