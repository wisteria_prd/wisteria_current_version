
<?php
ini_set('memory_limit', '-1');

// create new PDF document
// $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
$pdf = new TCPDF('', 'mm', 'A4', true, 'UTF-8');
// Reset the encoding forced from tcpdf
mb_internal_encoding('UTF-8');

$font_jp = TCPDF_FONTS::addTTFfont(WWW_ROOT.'fonts/ipag.ttf');
//$pdf->SetFont($font1, '', 14);

// language
if ($this->request->session()->read('tb_field') === 'en') {
    $language = 'Helvetica';
} else {
    $language = $font_jp;
}
// test language
$language = $font_jp;

ob_clean(); // cleaning the buffer before Output()

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Nick');
$pdf->SetTitle('Purchase Order Form');
$pdf->SetSubject('Purchase Order Form');
$pdf->SetKeywords('');

// Configure header and footer
$pdf->SetPrintHeader(false);
$pdf->SetPrintFooter(false);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
    require_once(dirname(__FILE__).'/lang/eng.php');
    $pdf->setLanguageArray($l);
}
// set default font subsetting mode
$pdf->setFontSubsetting(true);
$pdf->SetFont('Helvetica', '', 8);

// page padding
$page_padding_top = 10;
$page_padding_left = 10;

$pdf->AddPage();
// header page
$pdf->SetFont($language, '', 8);

$pdf->SetFillColor(255,255,255);
//$pdf->MultiCell($width = 80, $height = 60, $text = 'Main column', $border = 1, $align = 'L', $fill=true, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = 60, $valign = 'T');

// header title
$getY = $pdf->getY();
$pdf->SetFont($language, '', 12);
$pdf->Cell(190, 15, '〔別紙第１号様式〕', 0, false, 'L');
$pdf->ln();
$pdf->Cell(190, 10, '※（　医療機器　）輸入報告書', 0, false, 'C');
$pdf->ln();

$pdf->SetFont($language, '', 11);
$pdf->MultiCell($width = 190, $height = 10, $text = 'Date', $border = 0, $align = 'R', $fill=true, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = 10, $valign = 'B');
$pdf->ln();

$pdf->SetFont($language, '', 12);
$pdf->Cell(7, 6, '', 0, false, 'L');
$pdf->Cell(183, 6, '厚生労働大臣殿', 0, false, 'L');
$pdf->ln();

$complex_cell_border = array(
   'T' => array('width' => 0, 'color' => array(255,255,255), 'dash' => 0, 'cap' => 'butt'),
   'R' => array('width' => 0, 'color' => array(255,255,255), 'dash' => 0, 'cap' => 'round'),
   'B' => array('width' => 0, 'color' => array(0,0,0), 'dash' => 0, 'cap' => 'square'),
   'L' => array('width' => 0, 'color' => array(255,255,255), 'dash' => 0, 'cap' => 'butt'),
);

$pdf->SetFont($language, '', 8.5);
$address = '輸入者（受取人）氏名（法人に'. "\n";
$address .= 'あっては名称及び代表者の氏名）';
$pdf->MultiCell($width = 48, $height = 8, $text = $address, $border = 0, $align = 'L', $fill=true, $ln = 0, $x = 82, $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = 8, $valign = 'M');
$order = '受注';
$pdf->MultiCell($width = 55, $height = 8, $text = $order, $border = 0, $align = 'C', $fill = true, $ln = 0, $x = 130, $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = 8, $valign = 'M');

$arrContextOptions=array(
    'ssl' => array(
        'verify_peer' => false,
        'verify_peer_name' => false,
    ),
);
$path = $this->Url->build('/', true) .'img/u_plus_329e.png';
$icon_u_plus_329e = file_get_contents($path, false, stream_context_create($arrContextOptions));
$pdf->Image('@' . $icon_u_plus_329e, 194, 53, 4, 4, 'PNG', '', '', false, '', '', false, false, '', false, false, false);
//$pdf->MultiCell($width = 70, $height = 8, $text = '㊞  ', $border = $complex_cell_border, $align = 'R', $fill = true, $ln = 0, $x = 130, $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = 8, $valign = 'M');
$pdf->ln();

$complex_cell_border = array(
   'T' => array('width' => 0, 'color' => array(0,0,0), 'dash' => 0, 'cap' => 'butt'),
   'R' => array('width' => 0, 'color' => array(255,255,255), 'dash' => 0, 'cap' => 'round'),
   'B' => array('width' => 0, 'color' => array(0,0,0), 'dash' => 0, 'cap' => 'square'),
   'L' => array('width' => 0, 'color' => array(255,255,255), 'dash' => 0, 'cap' => 'butt'),
);
$pdf->SetFont($language, '', 8.5);
$address = '住所（法人にあっては主たる事務' . "\n";
$address .= '所の所在地）';
$pdf->MultiCell($width = 48, $height = 8, $text = $address, $border = 0, $align = 'L', $fill=true, $ln = 0, $x = 82, $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = 8, $valign = 'M');
$pdf->SetFont($language, '', 9);
$location = 'ては名称及び代表' . "\n";
$location .= 'ては名称及び代表12';
$pdf->MultiCell($width = 70, $height = 8, $text = $location, $border = $complex_cell_border, $align = 'C', $fill = true, $ln = 0, $x = 130, $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = 8, $valign = 'M');
$pdf->ln();

$complex_cell_border = array(
   'T' => array('width' => 0, 'color' => array(0,0,0), 'dash' => 0, 'cap' => 'butt'),
   'R' => array('width' => 0, 'color' => array(255,255,255), 'dash' => 0, 'cap' => 'round'),
   'B' => array('width' => 0, 'color' => array(0,0,0), 'dash' => 0, 'cap' => 'square'),
   'L' => array('width' => 0, 'color' => array(255,255,255), 'dash' => 0, 'cap' => 'butt'),
);
$pdf->SetFont($language, '', 8.5);
$address = '営業所等（貨物の送付先）の名称';
$pdf->MultiCell($width = 48, $height = 4, $text = $address, $border = 0, $align = 'L', $fill=true, $ln = 0, $x = 82, $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = 4, $valign = 'M');
$pdf->SetFont($language, '', 7);
$location = 'ては名称及び代表12';
$pdf->MultiCell($width = 70, $height = 4, $text = $location, $border = $complex_cell_border, $align = 'C', $fill = true, $ln = 0, $x = 130, $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = 4, $valign = 'M');
$pdf->ln();

$complex_cell_border = array(
   'T' => array('width' => 0, 'color' => array(0,0,0), 'dash' => 0, 'cap' => 'butt'),
   'R' => array('width' => 0, 'color' => array(255,255,255), 'dash' => 0, 'cap' => 'round'),
   'B' => array('width' => 0, 'color' => array(0,0,0), 'dash' => 0, 'cap' => 'square'),
   'L' => array('width' => 0, 'color' => array(255,255,255), 'dash' => 0, 'cap' => 'butt'),
);
$pdf->SetFont($language, '', 8.5);
$address = '同所在地 ';
$pdf->MultiCell($width = 48, $height = 4, $text = $address, $border = 0, $align = 'R', $fill = true, $ln = 0, $x = 82, $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = 4, $valign = 'M');
$pdf->SetFont($language, '', 9);
$location = '同上';
$pdf->MultiCell($width = 70, $height = 4, $text = $location, $border = $complex_cell_border, $align = 'C', $fill = true, $ln = 0, $x = 130, $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = 4, $valign = 'M');
$pdf->ln();

$pdf->SetFont($language, '', 8.5);
$address = '担当者名 ';
$pdf->MultiCell($width = 48, $height = 4, $text = $address, $border = 0, $align = 'R', $fill = true, $ln = 0, $x = 82, $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = 4, $valign = 'M');
$pdf->SetFont($language, '', 9);
$location = '電話 ' . ' 電話 ' . '077-569-5509';
$pdf->MultiCell($width = 70, $height = 4, $text = $location, $border = $complex_cell_border, $align = 'C', $fill = true, $ln = 0, $x = 130, $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = 4, $valign = 'M');
$pdf->ln();

$complex_cell_border = array(
   'T' => array('width' => 0, 'color' => array(0,0,0), 'dash' => 0, 'cap' => 'butt'),
   'R' => array('width' => 0, 'color' => array(255, 255, 255), 'dash' => 0, 'cap' => 'round'),
   'B' => array('width' => 0, 'color' => array(0,0,0), 'dash' => 0, 'cap' => 'butt'),
   'L' => array('width' => 0, 'color' => array(255, 255, 255), 'dash' => 0, 'cap' => 'butt'),
);
$pdf->SetFont($language, '', 8.5);
$address = 'Ｅメール ';
$pdf->MultiCell($width = 48, $height = 4, $text = $address, $border = 0, $align = 'R', $fill = true, $ln = 0, $x = 82, $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = 4, $valign = 'M');
$pdf->SetFont($language, '', 9);
$location = 'clinic@rei-beauty.com';
$pdf->MultiCell($width = 70, $height = 4, $text = $location, $border = $complex_cell_border, $align = 'C', $fill = false, $ln = 0, $x = 130, $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = 4, $valign = 'M');
$pdf->ln();


$pdf->MultiCell($width = 190, $height = 2, $text = ' ', $border = 0, $align = 'C', $fill = false, $ln = '', $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = 2, $valign = 'M');
$pdf->ln();
// Product table
$pdf->SetFont($language, '', 10);
$complex_cell_border = array(
   'T' => array('width' => 0, 'color' => array(0,0,0), 'dash' => 0, 'cap' => 'butt'),
   'R' => array('width' => 0, 'color' => array(0,0,0), 'dash' => 0, 'cap' => 'round'),
   'B' => array('width' => 0, 'color' => array(0,0,0), 'dash' => 0, 'cap' => 'butt'),
   'L' => array('width' => 0, 'color' => array(0,0,0), 'dash' => 0, 'cap' => 'butt'),
);
$pdf->MultiCell($width = 95, $height = 6, $text = '品　　　　　名', $border = $complex_cell_border, $align = 'C', $fill = false, $ln = '', $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = 6, $valign = 'M');
$pdf->MultiCell($width = 30, $height = 6, $text = '数　　量', $border = $complex_cell_border, $align = 'C', $fill = false, $ln = '', $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = 6, $valign = 'M');
$pdf->MultiCell($width = 65, $height = 6, $text = '業許可等の有無', $border = $complex_cell_border, $align = 'C', $fill = false, $ln = '', $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = 6, $valign = 'M');
$pdf->ln();
// Row product details
$products = 'BELOTERO BALANCE (1.0ml x 1)' . "\n";
$quantities = '10' . "\n";
$remarks = '□※（　　　　　　）製造販売業' . "\n";
$pdf->MultiCell($width = 95, $height = 30, $text = $products, $border = $complex_cell_border, $align = 'C', $fill = false, $ln = '', $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = '', $valign = 'T');
$pdf->MultiCell($width = 30, $height = 30, $text = $quantities, $border = $complex_cell_border, $align = 'L', $fill = false, $ln = '', $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = '', $valign = 'T');
$pdf->MultiCell($width = 65, $height = 30, $text = $remarks, $border = $complex_cell_border, $align = 'L', $fill = false, $ln = '', $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = '', $valign = 'T');
$pdf->ln();

$purpose_of_import = '①治験（企業）用、②臨床試験（医師）用、③試験研究・社内見本用、④展示用、' . "\n";
$purpose_of_import .= '⑤個人用、⑥医療従事者個人用、⑦再輸入品・返送品用、⑧自家消費用、' . "\n";
$purpose_of_import .= '⑨その他（　　　　　　　　　　　　　　　　　　　　　　）';
$pdf->SetFont($language, '', 10);
$pdf->MultiCell($width = 30, $height = 15, $text = '輸入の目的', $border = $complex_cell_border, $align = 'L', $fill = false, $ln = '', $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = 15, $valign = 'M');
$pdf->SetFont($language, '', 9);
$pdf->MultiCell($width = 160, $height = 15, $text = $purpose_of_import, $border = $complex_cell_border, $align = 'L', $fill = false, $ln = '', $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = 15, $valign = 'M');
$pdf->ln();

$matters = '☑上記輸入の目的のために使用するもので、他に販売、貸与又は授与するものではありません。' . "\n";
$matters .= '☑（個人用又は医療従事者個人用の場合）厚生労働省ホームページの「個人輸入において注意すべき' . "\n";
$matters .= '医薬品等について」を輸入前に確認し、輸入後も随時確認します。' . "\n";
$matters .= '□（試験研究・社内見本用の場合）人又は人の診断の目的には使用しません。';
$pdf->SetFont($language, '', 10);
$pdf->MultiCell($width = 30, $height = 18, $text = '  誓約事項', $border = $complex_cell_border, $align = 'L', $fill = false, $ln = '', $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = 18, $valign = 'M');
$pdf->SetFont($language, '', 9);
$pdf->MultiCell($width = 160, $height = 18, $text = $matters, $border = $complex_cell_border, $align = 'L', $fill = false, $ln = '', $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = 18, $valign = 'M');
$pdf->ln();

$pdf->SetFont($language, '', 11);
$pdf->MultiCell($width = 190, $height = 6, $text = '製造業者名及び国名', $border = $complex_cell_border, $align = 'C', $fill = false, $ln = '', $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = 6, $valign = 'M');
$pdf->ln();

$manufacturer = 'MERZ AESTHETICS, Inc.（メルツ エステティクス社） / Germany（ドイツ） →　RAD　＆　BELOTERO
ANTEIS S.A.　　アンテイス社　　　　ジュネーブ/スイス　→　エセリス系
MERZ Pharmaceuticals, LLC社（メルツファーマシューティカルズ社） / Germany(ドイツ)　→　Xeomin　＆　Bocouture';
$pdf->SetFont($language, '', 10);
$pdf->MultiCell($width = 190, $height = 15, $text = $manufacturer, $border = $complex_cell_border, $align = 'C', $fill = false, $ln = '', $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = 15, $valign = 'M');
$pdf->ln();

$pdf->SetFont($language, '', 12);
$pdf->MultiCell($width = 45, $height = 7, $text = '輸入年月日', $border = $complex_cell_border, $align = 'C', $fill = false, $ln = '', $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = 7, $valign = 'M');
$pdf->MultiCell($width = 50, $height = 7, $text = 'AWB、B/L等の番号', $border = $complex_cell_border, $align = 'C', $fill = false, $ln = '', $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = 7, $valign = 'M');
$pdf->MultiCell($width = 95, $height = 7, $text = '到着空港、到着港又は蔵置場所', $border = $complex_cell_border, $align = 'C', $fill = false, $ln = '', $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = 7, $valign = 'M');
$pdf->ln();

$pdf->SetFont($language, '', 10);
$pdf->SetTextColor(255,0,0);
$pdf->MultiCell($width = 45, $height = 13, $text = '平成 29年 月 日', $border = $complex_cell_border, $align = 'C', $fill = false, $ln = '', $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = 13, $valign = 'M');
$pdf->MultiCell($width = 50, $height = 13, $text = '', $border = $complex_cell_border, $align = 'C', $fill = false, $ln = '', $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = 13, $valign = 'M');
$pdf->MultiCell($width = 95, $height = 13, $text = 'TNT通関サービス株式会社東京ロジスティクスセンター　保税蔵置所（１BWD9）', $border = $complex_cell_border, $align = 'C', $fill = false, $ln = '', $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = 13, $valign = 'M');
$pdf->ln();

$pdf->SetTextColor(0,0,0);
$pdf->SetFont($language, '', 12);
$pdf->MultiCell($width = 10, $height = 18, $text = '備考', $border = $complex_cell_border, $align = 'C', $fill = false, $ln = '', $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = 18, $valign = 'M');
$pdf->SetFont($language, '', 9);
$pdf->MultiCell($width = 180, $height = 18, $text = '（再輸入品・返送品用の場合）再輸入・返送に至った理由及び今後の措置について記載すること。', $border = $complex_cell_border, $align = 'L', $fill = false, $ln = '', $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = 18, $valign = 'T');
$pdf->ln();

$pdf->SetTextColor(0,0,0);
$pdf->SetFont($language, '', 12);
$pdf->MultiCell($width = 10, $height = 45, $text = '厚生労働省確認欄', $border = $complex_cell_border, $align = 'C', $fill = false, $ln = '', $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = 45, $valign = 'M');
$pdf->MultiCell($width = 180, $height = 45, $text = '', $border = $complex_cell_border, $align = 'L', $fill = false, $ln = '', $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = 45, $valign = 'T');
$pdf->SetFont($language, '', 11);
$pdf->MultiCell($width = 180, $height = 6, $text = '特記事項', $border = 0, $align = 'L', $fill = false, $ln = '', $x = 20, $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = 6, $valign = 'T');
$pdf->ln();
$pdf->ln();
$pdf->ln();
$pdf->ln();
$pdf->ln();
$pdf->SetTextColor(255,0,0);
$pdf->MultiCell($width = 180, $height = 5, $text = '厚生労働省関東信越厚生局', $border = 0, $align = 'L', $fill = false, $ln = '', $x = 105, $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = 5, $valign = 'T');
$pdf->ln();
$pdf->SetTextColor(0,0,0);
$pdf->MultiCell($width = 180, $height = 5, $text = '薬事監視専門官', $border = 0, $align = 'L', $fill = false, $ln = '', $x = 105, $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = 5, $valign = 'T');
$pdf->ln();
$pdf->MultiCell($width = 180, $height = 5, $text = '毒物劇物監視員', $border = 0, $align = 'L', $fill = false, $ln = '', $x = 105, $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = 5, $valign = 'T');
$pdf->Image('@' . $icon_u_plus_329e, 191, 253, 4, 4, 'PNG', '', '', false, '', '', false, false, '', false, false, false);
//$pdf->writeHTMLCell(10, 5, 10, 10, $this->Html->image('u_plus_329e.png'));
//$pdf->SetFont('courierB', '', 10);
//$pdf->MultiCell($width = 10, $height = 5, $text = '㊞', $border = 0, $align = 'L', $fill = false, $ln = '', $x = 183, $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = 5, $valign = 'T');
$pdf->ln();

// note text in the document
$pdf->SetFont($language, '', 7);
$pdf->MultiCell($width = 13, $height = 4, $text = '（注）', $border = 0, $align = 'R', $fill = false, $ln = '', $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = 4, $valign = 'T');
$pdf->MultiCell($width = 5, $height = 4, $text = '１.', $border = 0, $align = 'L', $fill = false, $ln = '', $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = 4, $valign = 'T');
$pdf->MultiCell($width = 175, $height = 4, $text = '※（　）欄には，医薬品，医薬部外品，化粧品，医療機器，体外診断用医薬品、再生医療等製品，毒物，劇物の別を', $border = 0, $align = 'L', $fill = false, $ln = '', $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = 4, $valign = 'T');
$pdf->ln();
$pdf->MultiCell($width = 13, $height = 4, $text = '', $border = 0, $align = 'L', $fill = false, $ln = '', $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = 4, $valign = 'T');
$pdf->MultiCell($width = 5, $height = 4, $text = '', $border = 0, $align = 'L', $fill = false, $ln = '', $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = 4, $valign = 'T');
$pdf->MultiCell($width = 175, $height = 4, $text = '記入すること。', $border = 0, $align = 'L', $fill = false, $ln = '', $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = 4, $valign = 'T');
$pdf->ln();
$pdf->MultiCell($width = 13, $height = 4, $text = '', $border = 0, $align = 'L', $fill = false, $ln = '', $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = 4, $valign = 'T');
$pdf->MultiCell($width = 5, $height = 4, $text = '２.', $border = 0, $align = 'L', $fill = false, $ln = '', $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = 4, $valign = 'T');
$pdf->MultiCell($width = 175, $height = 4, $text = 'この様式の大きさは日本工業規格Ａ４とすること。', $border = 0, $align = 'L', $fill = false, $ln = '', $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = 4, $valign = 'T');

$pdf->Output();
