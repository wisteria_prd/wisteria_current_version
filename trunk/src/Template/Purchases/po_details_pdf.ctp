
<?php
ini_set('memory_limit', '-1');

// create new PDF document
// $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
$pdf = new TCPDF('', 'mm', 'A4', true, 'UTF-8');
// Reset the encoding forced from tcpdf
mb_internal_encoding('UTF-8');

$font_jp = TCPDF_FONTS::addTTFfont(WWW_ROOT.'fonts/ipag.ttf');
//$pdf->SetFont($font1, '', 14);

// language
if ($this->request->session()->read('tb_field') === 'en') {
    $language = 'Helvetica';
} else {
    $language = $font_jp;
}
// test language
$language = 'Helvetica';

ob_clean(); // cleaning the buffer before Output()

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Nick');
$pdf->SetTitle('Purchase Order Form');
$pdf->SetSubject('Purchase Order Form');
$pdf->SetKeywords('');

// Configure header and footer
$pdf->SetPrintHeader(false);
$pdf->SetPrintFooter(true);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
    require_once(dirname(__FILE__).'/lang/eng.php');
    $pdf->setLanguageArray($l);
}
// set default font subsetting mode
$pdf->setFontSubsetting(true);
$pdf->SetFont('Helvetica', '', 8);

// page padding
$page_padding_top = 10;
$page_padding_left = 10;

$pdf->AddPage();
// header page
$pdf->SetFont($language, '', 8);

$pdf->SetFillColor(255,255,255);
//$pdf->MultiCell($width = 80, $height = 60, $text = 'Main column', $border = 1, $align = 'L', $fill=true, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = 60, $valign = 'T');

// get current vertical position
$getY = $pdf->getY();
$text = '<span style="font-size: 14px;">Order Form</span>' . '<br>';
$text .= '<b>Order Form No. </b>' . '[purchases.po_number]' . '<br>';
$text .= '<b>Issue Date: </b>' . '[purchases.issue_date]';
$pdf->writeHTMLCell(70, '', '', 24, $text, 0, 0, 1, true, 'L', true);

$arrContextOptions=array(
    'ssl' => array(
        'verify_peer' => false,
        'verify_peer_name' => false,
    ),
);
$path = $this->Url->build('/', true) .'img/wisteria_logo.png';
$data = file_get_contents($path, false, stream_context_create($arrContextOptions));
//$type = pathinfo($path, PATHINFO_EXTENSION);
//$base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);
//$pdf->cell(50, 10, $data);

// Wisteria main logo
$pdf->Image('@' . $data, 147, 10, 18, 15, 'PNG', '', 'T', false, 100, '', false, false, 0, false, false, false);

$address = '3791 Jalan Bukit Merah, #10-17 E-Centre @ Redhill Singapore 159471'. "\n";
$address .= 'Tel: +65-6274-0433 / Fax: +65-6274-0477' . "\n";
$address .= 'UEN No: 201403392G / Reg No: 201403392G';
$pdf->MultiCell($width = 120, $height = 10, $text = $address, $border = 0, $align = 'R', $fill=true, $ln = 0, $x = 80, $y = 26, $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = 60, $valign = 'T');

$style = array('width' => 1, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(128, 128, 128));
$pdf->Line($page_padding_left, 38, 200, 38, $style);
//$pdf->ln();
//$pdf->ln();
$text = '<br><div style="width: 120px; border: 1px solid #807d7d;"><br>';
$text .= '&nbsp;&nbsp;&nbsp;<span style="font-size: 14px;">Seller Name</span>' . '<br>';
$text .= '&nbsp;&nbsp;&nbsp;Address 1' . ' #' . '<br>';
$text .= '&nbsp;&nbsp;&nbsp;Address 2' . ' #' . '<br>';
$text .= '&nbsp;&nbsp;&nbsp;Address 3' . ' #' . '<br>';
$text .= '&nbsp;&nbsp;&nbsp;<span>Contact: </span>' . '<span>#</span>' . '<br>';
$text .= '&nbsp;&nbsp;&nbsp;<span>Email: </span>' . '<span>#</span>' . '<br>';
$text .= '&nbsp;&nbsp;&nbsp;<span>Tel: </span>' . '<span>#</span>' . '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
$text .= '&nbsp;&nbsp;&nbsp;<span>Fax: </span>' . '<span>#</span>' . '<br>';
$text .= '</div>';
$pdf->writeHTMLCell(120, 45, '', 38, $text, '', '', 1, true, 'L', true);

$pdf->ln();

// Products Details
$pdf->SetLineStyle(array('width' => 0.1, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(115, 115, 115)));
$pdf->SetFont('Helvetica', 'B', 8);
$pdf->Cell(15, 6, 'No', 1, false, 'L');
$pdf->Cell(90, 6, 'Description', 1, false, 'L');
$pdf->Cell(30, 6, 'Unit Price (Cur.)', 1, false, 'L');
$pdf->Cell(25, 6, 'Qty', 1, false, 'L');
$pdf->Cell(30, 6, 'Amount (Cur.)', 1, false, 'L');

$pdf->ln();
$pdf->SetFont('Helvetica', '', 8);
for ($i = 0; $i < 20; $i++) {
    $pdf->Cell(15, 6, $i + 1, 1, false, 'L');
    $pdf->Cell(90, 6, '', 1, false, 'L');
    $pdf->Cell(30, 6, '', 1, false, 'L');
    $pdf->Cell(25, 6, '', 1, false, 'L');
    $pdf->Cell(30, 6, '', 1, false, 'L');
    $pdf->ln();
}
$pdf->Cell(105, 6, 'Remarks:', 1, false, 'L');
$pdf->Cell(55, 6, '52,000$', 1, false, 'L');
$pdf->Cell(30, 6, '34,000$', 1, false, 'L');

$pdf->ln();

$pdf->Cell(120, 12, 'Medical Professionals Singapore Pte. Ltd. Peter Lim, Managing Director', 0 ,false. 'L');
$pdf->Cell(40, 12, 'Date: 2017-12-13', 0 ,false. 'L');
$pdf->Output();
