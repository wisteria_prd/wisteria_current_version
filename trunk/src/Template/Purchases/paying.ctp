<?php $this->assign('title', 'PO Paying'); ?>
<div class="row">
    <div class="col-md-12">
        <ol class="breadcrumb">
            <li><?= $this->Html->link(__('HOME'), ['controller' => 'users', 'action' => 'index']) ?></li>
            <li><?= $this->Html->link(__('Purchase Order'), ['action' => 'index']) ?></li>
            <li class="active"><?php echo $this->fetch('title'); ?></li>
        </ol>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <h1><?= __('Order Form') ?></h1>
        <p><strong><?= __('Order Form No.') ?></strong> [purchases.po_number]</p>
        <p><strong><?= __('Issue Date:') ?></strong> [purchases.issue_date]</p>
    </div>
    <div class="col-md-6 text-right">
        <div class="company-invoice-logo text-center">
            Logo Here
        </div>
        <p>3791 Jalan Bukit Merah, #10-17 E-Centre @ Redhill Singapore 159471</p>
        <P>Tel: +65-6274-0433 / Fax: +65-6274-0477</P>
        <P>UEN No: 201403392G / Reg No: 201403392G</P>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <hr class="hr-invoiced">
    </div>
</div>
<div class="row row-top-space">
    <div class="col-md-8">
        <div class="seller-info">
            <h3><?= __('Seller Name') ?></h3>
            <div class="row">
                <div class="col-md-2">
                    <p><?= __('Address 1') ?></p>
                </div>
                <div class="col-md-10">
                    #1
                </div>
            </div>
            <div class="row">
                <div class="col-md-2">
                    <p><?= __('Address 2') ?></p>
                </div>
                <div class="col-md-10">
                    #2
                </div>
            </div>
            <div class="row">
                <div class="col-md-2">
                    <p><?= __('Address 3') ?></p>
                </div>
                <div class="col-md-10">
                    #3
                </div>
            </div>
            <div class="row">
                <div class="col-md-2">
                    <div><?= __('Contact:') ?></div>
                </div>
                <div class="col-md-10">
                    <p>Mr. Daroath</p>
                </div>
            </div>
            <div class="row">
                <div class="col-md-2">
                    <div><?= __('Email:') ?></div>
                </div>
                <div class="col-md-10">
                    <p>daroath.ouk.bitex@gmail.com</p>
                </div>
            </div>
            <div class="row">
                <div class="col-md-2">
                    <div><?= __('Tel:') ?></div>
                </div>
                <div class="col-md-10">
                    <p>081 620 630 / <?= __('Fax:') ?> 081 620 630</p>
                </div>
            </div>
        </div>        
    </div>    
</div>
<div class="row row-invoice-incharge">
    <div class="col-md-offset-8 col-md-4">
        <p>Order in Charge: #xxx</p>
        <p>Email: #xxx</p>
        <p>Mobile: #xxx</p>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <table class="table table-striped table-bordered table-invoice">
            <thead>
                <tr>
                    <th><?= __('No') ?></th>
                    <th width="35%"><?= __('Description') ?></th>
                    <th><?= __('Unit Price (Cur.)') ?></th>
                    <th><?= __('Qty') ?></th>
                    <th><?= __('Amount (Cur.)') ?></th>
                </tr>                
            </thead>
            <tbody>
                <tr>
                    <td class="modal-invoice">1</td>
                    <td class="modal-invoice">Description</td>
                    <td class="modal-invoice"><label class="label-discounted"><?= __('Discounted') ?></label> 240$</td>
                    <td class="modal-invoice">5</td>
                    <td class="modal-invoice">24,000$</td>
                </tr>
                <tr>
                    <td class="modal-invoice">2</td>
                    <td class="modal-invoice"><label class="label-foc-child">FOC</label> Description 2</td>
                    <td class="modal-invoice">225$</td>
                    <td class="modal-invoice">12</td>
                    <td class="modal-invoice">25,000</td>
                </tr>                
                <tr>
                    <td colspan="2">Remarks:</td>
                    <td colspan="2" class="table-text-center">52,000$</td>
                    <td>34,000$</td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
<div class="row">
    <div class="col-md-4">
        <p>Medical Professionals Singapore Pte. Ltd. Peter Lim, Managing Director</p>
    </div>
    <div class="col-md-2">
        <p><?= __('Date:') ?></p>
    </div>
</div>
<div class="row">
    <div class="col-md-4">
        <p class="company-invoice-signature">Digital Signature</p>
    </div>
    <div class="col-md-2">
        <p class="text-center">12/06/2017</p>
    </div>    
</div>
<div class="row">
    <div class="col-md-4">
        <hr class="signature-line-approving">
    </div>
    <div class="col-md-2">
        <hr class="signature-line-approving">
    </div>
</div>
<div class="payment">
    <div class="row">
        <div class="col-md-5 col-md-push-7">
            <div class="row">
                <div class="col-md-3">
                    <?= __('Current Status') ?>
                </div>
            </div>
            <div class="row row-top-small-space">
                <div class="col-md-4">
                    <label data-status="draft" class="status label-draft"><?= __('Paying') ?></label>
                    <span class="glyphicon glyphicon-arrow-right arrow-draft"></span>
                </div>
                <div class="col-md-4">
                    <?php
                        $options = ['Paid', 'Cancel'];
                    ?>
                    <?= $this->Form->select('display', $options, ['class' => 'form-control change-invoice-status', 'label' => false]) ?>
                </div>
            </div>
            <div class="row row-top-small-space">
                <div class="col-sm-4"><?= __('Payment Info:') ?></div>
                <div class="col-sm-4 payments">

                </div>
                <div class="col-sm-4">
                    <button class="btn btn-primary form-btn-add add-new-payment" type="button"><i class="fa fa-plus" aria-hidden="true"></i></button>
                </div>
            </div>
            <div class="row row-top-small-space">
                <div class="col-md-3 col-md-push-9 text-right">
                    <button type="button" class="btn btn-primary btn-submit-approving" data-toggle="popover" data-trigger="click"><?= __('Submit') ?></button>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- modal new item -->
<?= $this->HtmlModal->modalHeader('modalAddItem', __('TXT_PO_ADD_ITEM'), ' modal-lg') ?>
<div class="row">
    <div class="col-md-12">
        <?php
            echo $this->Form->create(isset($user) ? $user : '', [
                    'class' => 'form-po-settings form-horizontal',
                    'autocomplete' => 'off'
                ]
            );
            $this->Form->templates([
                'inputContainer' => '{{content}}'
            ]);
        ?>
        <div class="form-group">
            <div class="col-md-3 col-md-offset-1">
                <div class="checkbox">
                    <label>
                        <?= $this->Form->input('manual', ['type' => 'checkbox', 'label' => false]) ?> <?= __('Manually register FOC') ?>
                    </label>
                </div>
            </div>
            <div class="col-md-5">
                <?php
                    $options = ['PO', 'Brand', 'Product'];
                    echo $this->Form->select('role', $options, [
                        'label' => false,
                        'class' => 'form-control',
                        'empty' => false
                    ]);
                ?>
            </div>
        </div>
        <div class="form-group">
            <label for="" class="col-md-3 col-md-offset-1"><?= __('Product') ?></label>
            <div class="col-md-5">
                <?php
                    echo $this->Form->input('product', [
                        'type' => 'text',
                        'label' => false,
                        'class' => 'form-control',
                        'placeholder' => __('Search Keyword')
                    ]);
                ?>
            </div>
            <div class="col-md-2">
                <button type="button" class="btn btn-primary btn-product-list"><?= __('Product List') ?></button>
            </div>
        </div>
        <div class="form-group">
            <div class="col-md-5 col-md-offset-4">
                <textarea rows="5" class="form-control"></textarea>
            </div>
        </div>
        <div class="form-group">
            <label for="" class="col-md-3 col-md-offset-1"><?= __('Quantity') ?></label>
            <div class="col-md-5">
                <?php
                    echo $this->Form->input('quantity', [
                        'type' => 'text',
                        'label' => false,
                        'class' => 'form-control quantity',
                        'placeholder' => __('Input Quantity')
                    ]);
                ?>
            </div>
        </div>
        <div class="form-group">
            <label for="" class="col-md-3 col-md-offset-1"><?= __('Unit Price') ?></label>
            <div class="col-md-5">
                <?php
                    echo $this->Form->input('unit_price', [
                        'type' => 'text',
                        'label' => false,
                        'class' => 'form-control unit-price',
                        'disabled' => true,
                        'placeholder' => __('xxx')
                    ]);
                ?>
            </div>
            <div class="col-md-2">
                <button type="button" class="btn btn-primary popup-discount"><?= __('View Discount') ?></button>
            </div>
        </div>
        <div class="form-group">
            <div class="col-md-5 col-md-offset-4">
                <div class="checkbox">
                    <label>
                        <?= $this->Form->input('manual', ['type' => 'checkbox', 'class' => 'chk-register-foc', 'label' => false]) ?> <?= __('Manually register FOC') ?>
                    </label>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label for="" class="col-md-3 col-md-offset-1"><?= __('Expected Receiving Date') ?></label>
            <div class="col-md-5">
                <div class="input-group with-datepicker">
                    <?= $this->Form->input('date', ['class' => 'form-control datepicker', 'label' => false]) ?>
                    <div class="input-group-btn">
                        <button type="button" class="btn btn-default trigger-calendar" aria-label="Calendar">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-group">
            <div class="col-md-offset-1 col-md-3">
                <button type="button" class="btn btn-default" data-dismiss="modal"><?= __('Cancel') ?></button>
            </div>
            <div class="col-md-offset-3 col-md-3">
                <button type="button" class="btn btn-primary"><?= __('Submit') ?></button>
            </div>
        </div>
        <?= $this->Form->end() ?>
    </div>
</div>
<?= $this->HtmlModal->modalFooter(); ?>
<!-- /end modal new item -->
<!-- modal discount conditions -->
<?= $this->HtmlModal->modalHeader('modalDiscountCondition', __('Discount Conditions')) ?>
<div class="row">
    <div class="col-md-12">
        <table class="table table-striped table-view-discount">
            <thead>
                <tr>
                    <th>#</th>
                    <th><?= __('Discount Type') ?></th>
                    <th><?= __('Range Type') ?></th>
                    <th><?= __('From') ?></th>
                    <th><?= __('To') ?></th>
                    <th>&nbsp;</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>1</td>
                    <td>FOC</td>
                    <td>Quantity</td>
                    <td class="qty-from">21</td>
                    <td class="qty-to">25</td>
                    <td><button type="button" class="btn btn-danger foc-preview" data-toggle="popover" data-trigger="hover">View FOC</button></td>
                </tr>
                <tr>
                    <td>2</td>
                    <td>FOC</td>
                    <td>Quantity</td>
                    <td class="qty-from">26</td>
                    <td class="qty-to">29</td>
                    <td><button type="button" class="btn btn-danger foc-preview" data-toggle="popover" data-trigger="hover">View FOC</button></td>
                </tr>
                <tr>
                    <td>3</td>
                    <td>FOC</td>
                    <td>Quantity</td>
                    <td class="qty-from">30</td>
                    <td class="qty-to">34</td>
                    <td><button type="button" class="btn btn-danger foc-preview" data-toggle="popover" data-trigger="hover">View FOC</button></td>
                </tr>
                <tr>
                    <td>4</td>
                    <td>FOC</td>
                    <td>Quantity</td>
                    <td class="qty-from">32</td>
                    <td class="qty-to">40</td>
                    <td><button type="button" class="btn btn-danger foc-preview" data-toggle="popover" data-trigger="hover">View FOC</button></td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
<div class="row">
    <div class="col-md-12 text-center">
        <button type="button" class="btn btn-default" data-dismiss="modal"><?= __('Close') ?></button>
    </div>
</div>
<?= $this->HtmlModal->modalFooter(); ?>
<!-- /end modal discount conditions -->
<!-- modal Register Payment -->
<?= $this->HtmlModal->modalHeader('modalPaymentInfo', __('Payment Information'), ' modal-lg') ?>
<?php
    echo $this->Form->create('User',[
        'autocomplete' => 'off',
        'class' => 'form-custom-clearance form-horizontal',
    ]);
    $this->Form->templates([
        'inputContainer' => '{{content}}'
    ]);
?>
<div class="form-group">
    <label class="control-label col-md-3 col-md-offset-2" for=""><?= __('Exist payment:') ?></label>
    <div class="col-md-4">
        <?php
            $options = ['New', 'Payment A', 'Payment B'];
            echo $this->Form->select('role', $options, [
                'label' => false,
                'class' => 'form-control',
                'empty' => false
            ]);
        ?>
    </div>
</div>
<div class="form-group">
    <label class="control-label col-md-3 col-md-offset-2" for=""><?= __('Bank Charge') ?></label>
    <div class="col-md-4">
        <?php
            echo $this->Form->input('role', [
                'type' => 'text',
                'label' => false,
                'class' => 'form-control'
            ]);
        ?>
    </div>
</div>
<div class="form-group">
    <label class="control-label col-md-3 col-md-offset-2" for=""><?= __('Bank Charge Date') ?></label>
    <div class="col-md-4">
        <div class="input-group with-datepicker">
            <?= $this->Form->input('date', ['class' => 'form-control datepicker', 'label' => false, 'placeholder' => __('Select a date')]) ?>
            <div class="input-group-btn">
                <button type="button" class="btn btn-default trigger-calendar" aria-label="Calendar">
                    <span class="glyphicon glyphicon-calendar"></span>
                </button>
            </div>
        </div>
    </div>
</div>
<div class="form-group">
    <label class="control-label col-md-3 col-md-offset-2" for=""><?= __('Product Amount') ?></label>
    <div class="col-md-4">
        <?php
            echo $this->Form->input('role', [
                'type' => 'text',
                'label' => false,
                'class' => 'form-control'
            ]);
        ?>
    </div>
</div>
<div class="form-group">
    <label class="control-label col-md-3 col-md-offset-2" for=""><?= __('Import Expenses') ?></label>
    <div class="col-md-4">
        <?php
            echo $this->Form->input('role', [
                'type' => 'text',
                'label' => false,
                'class' => 'form-control'
            ]);
        ?>
    </div>
</div>
<div class="form-group">
    <label class="control-label col-md-3 col-md-offset-2" for=""><?= __('Paid Amount') ?></label>
    <div class="col-md-4">
        <?php
            echo $this->Form->input('role', [
                'type' => 'text',
                'label' => false,
                'class' => 'form-control'
            ]);
        ?>
    </div>
</div>
<div class="form-group">
    <label class="control-label col-md-3 col-md-offset-2" for=""><?= __('Currency') ?></label>
    <div class="col-md-4">
        <?php
        echo $this->Form->select('currency_id', $currencies, [
            'class' => 'form-control',
            'label' => false,
            'required' => false,
            'id' => 'select-currency',
            'empty' => ['' => __('Select Currency...')]
        ]);
        ?>
        <span class="help-block help-tips" id="error-currency_id"></span>
    </div>
</div>
<div class="form-group">
    <label class="control-label col-md-3 col-md-offset-2" for=""><?= __('Currency rate oc ap') ?></label>
    <div class="col-md-4">
        <?php
            echo $this->Form->input('role', [
                'type' => 'text',
                'label' => false,
                'class' => 'form-control'
            ]);
        ?>
    </div>
</div>
<div class="form-group">
    <label class="control-label col-md-3 col-md-offset-2" for=""><?= __('Currency rate extra ap') ?></label>
    <div class="col-md-4">
        <?php
            echo $this->Form->input('role', [
                'type' => 'text',
                'label' => false,
                'class' => 'form-control'
            ]);
        ?>
    </div>
</div>
<div class="form-group">
    <label class="control-label col-md-3 col-md-offset-2" for=""><?= __('Currency rate jpy ap') ?></label>
    <div class="col-md-4">
        <?php
            echo $this->Form->input('role', [
                'type' => 'text',
                'label' => false,
                'class' => 'form-control'
            ]);
        ?>
    </div>
</div>
<div class="form-group">
    <label class="control-label col-md-3 col-md-offset-2" for=""><?= __('Payment Code') ?></label>
    <div class="col-md-4">
        <?php
            echo $this->Form->input('role', [
                'type' => 'text',
                'label' => false,
                'class' => 'form-control'
            ]);
        ?>
    </div>
</div>
<div class="additional-info">
    <div class="form-group additional-item">
        <label class="control-label col-md-3 col-md-offset-2" for=""><?= __('Additional') ?></label>
        <div class="col-md-2 field-small-padding-right">
            <?php
                echo $this->Form->input('role', [
                    'type' => 'text',
                    'label' => false,
                    'class' => 'form-control',
                    'placeholder' => __('Description')
                ]);
            ?>
        </div>
        <div class="col-md-2 field-small-padding-left">
            <?php
                echo $this->Form->input('role', [
                    'type' => 'text',
                    'label' => false,
                    'class' => 'form-control',
                    'placeholder' => __('Amount')
                ]);
            ?>
        </div>
        <div class="col-md-2 field-no-padding-left">
            <button class="btn btn-primary form-btn-add add-more-amount" type="button"><i class="fa fa-plus" aria-hidden="true"></i></button>
        </div>
    </div>
</div>
<div class="row row-top-space">
    <div class="col-md-2 col-md-offset-3">
        <label for="" class="control-label"><?= __('PDF File') ?></label>
    </div>
    <div class="col-md-4">
        <div class="pdf-files file-uploaded">
            <span class="glyphicon glyphicon-remove-circle icon-remove-file"></span> Filename.pdf
        </div>
    </div>
</div>
<div class="row row-top-space">
    <div class="col-md-2 col-md-offset-3">
        <label for="" class="control-label"><?= __('PDF File') ?></label>
    </div>
    <div class="col-md-4">
        <div class="pdf-files file-uploaded">
            <span class="glyphicon glyphicon-remove-circle icon-remove-file"></span> Filename.pdf
        </div>
    </div>
    <div class="col-md-2 field-no-padding-left">
        <button class="btn btn-primary form-btn-add add-new-file" type="button"><i class="fa fa-plus" aria-hidden="true"></i></button>
    </div>
</div>
<div class="row row-top-space">
    <div class="col-md-6 text-right">
        <button type="button" class="btn btn-default" data-dismiss="modal"><?= __('Cancel') ?></button>
    </div>
    <div class="col-md-6">
        <button type="button" class="btn btn-primary btn-pay-now" data-toggle="popover" data-trigger="click"><?= __('Submit') ?></button>
    </div>
</div>
<?= $this->Form->end() ?>
<?= $this->HtmlModal->modalFooter(); ?>
<!-- /end modal custom clearance -->
<!-- modal pdf upload -->
<?= $this->HtmlModal->modalHeader('modalPDFUpload', __('Document Upload')) ?>
<div class="row">
    <div class="col-md-12">
        <?php
            echo $this->Form->create('User',[
                'autocomplete' => 'off',
                'class' => 'form-pdf-upload form-horizontal',
                'onsubmit' => 'return false'
            ]);
            $this->Form->templates([
                'inputContainer' => '{{content}}'
            ]);
        ?>
        <div class="form-group">
            <label for="" class="control-label col-md-3"><?= __('Type') ?></label>
            <div class="col-md-7">
                <?php
                    $options = ['PDF', 'JPEG'];
                    echo $this->Form->select('type', $options, [
                        'label' => false,
                        'class' => 'form-control',
                        'empty' => false
                    ]);
                ?>
            </div>
        </div>
        <div class="form-group">
            <label for="" class="control-label col-md-3"><?= __('PDF File') ?></label>
            <div class="col-md-7">
                <?php
                    echo $this->Form->input('file', [
                        'type' => 'file',
                        'label' => false
                    ]);
                ?>
            </div>
        </div>
        <?= $this->Form->end() ?>
    </div>
</div>
<div class="row">
    <div class="col-md-12 text-center">
        <button type="button" class="btn btn-primary" data-dismiss="modal"><?= __('Upload') ?></button>
    </div>
</div>
<?= $this->HtmlModal->modalFooter(); ?>
<!-- /end modal pdf upload -->
<?php
    echo $this->Html->css([
        'bootstrap-datetimepicker.min',
        'jquery-ui',
        'intlTelInput',
    ]);
?>
<script>
    $(document).ready(function() {
        var qty_highlight;
        $('.datepicker').datetimepicker({
           format: 'YYYY-MM-DD'
        });

        $('body').on('click', '.add-new-payment', function () {
            var cls = '';
            if ($('.button-issue').length) {
                cls = ' row-top-space';
            }
            var item = '<div class="row button-issue'+cls+'">';
                item += '<div class="col-sm-12">';
                item += '<span class="glyphicon glyphicon-remove-circle icon-remove-issue"></span>';
                item += '<button type="button" class="btn btn-default btn-add-payment">Payment Id</button>';
                item += '</div>';
                item += '</div>';

            $('.payments').append(item);
        });

        $('body').on('click', '.icon-remove-issue', function () {
            $(this).closest('.button-issue').remove();
        });

        $('body').on('click', '.btn-add-payment', function() {
            $('#modalPaymentInfo').modal({
                backdrop: 'static',
                keyboard: false
            });
        });

        $('body').on('click', '.add-more-amount', function () {
            var item = $('.additional-item').first().clone();
            item.find('label').html('&nbsp;');
            item.find('button').removeClass('add-more-amount btn-primary').addClass('btn-delete form-btn-add remove-more-amount').html('<i class="fa fa-trash" aria-hidden="true"></i>');
            $('.additional-info').append(item);
        });

        $('body').on('click', '.remove-more-amount', function () {
            $(this).parents('.additional-item').remove();
        });

        var msg = '<p><?= __('Are you sure to change the status to Paid?') ?></p>';
        msg += '<div class="text-center">';
        msg += '<button class="btn btn-default confirm-yes" type="button">Yes</button>';
        msg += '<button class="btn btn-default confirm-no" type="button">No</button>';
        msg += '</div>';
        var tmp = '<div class="popover" role="tooltip"><div class="arrow"></div><h3 class="popover-title"></h3>';
        tmp += '<div class="popover-content"></div></div>';
        var options = {
            placement: 'top',
            title: '<?= __('Payment Information') ?>',
            html: true,
            content: msg,
            container: 'body',
            template: tmp
        };
        $('.btn-submit-approving').popover(options);

        $('body').on('click', '.confirm-no', function() {
            $('.btn-submit-approving').click();
        });

        $('body').on('click', '.confirm-yes', function() {
            location.href='<?= $this->Url->build(['action' => 'paid']) ?>';
            $('.btn-submit-approving').click();
        });

        $('body').on('click', '.add-new-file', function() {
            $('#modalPDFUpload').modal({
                backdrop: 'static',
                keyboard: false
            });
        });

        $('.trigger-calendar').click(function() {
            $(this).parents('.with-datepicker').find('.datepicker').focus();
            //$('.datepicker').focus();
        });

        $('body').on('click', '.modal-invoice', function() {
            $('#modalAddItem').modal({
                backdrop: 'static',
                keyboard: false
            });
        });

        $('body').on('click', '.chk-register-foc', function() {
            if ($(this).is(':checked')) {
                $('.unit-price').removeAttr('disabled', true);
            } else {
                $('.unit-price').attr('disabled', true);
            }
        });

        //view discount conditions
        $('body').on('click', '.popup-discount', function() {
            qty_highlight = $('.quantity').val();
            $('#modalDiscountCondition').on('shown.bs.modal', function() {
                $('.table-view-discount tbody tr').each(function() {
                    $(this).removeClass('row-highlight');
                    var from = parseInt($(this).find('.qty-from').text());
                    var to = parseInt($(this).find('.qty-to').text());
                    if (qty_highlight >= from && qty_highlight <= to) {
                        $(this).addClass('row-highlight');
                    }
                });
            }).modal({
                backdrop: 'static',
                keyboard: false
            });
        });

        var data = '<table class="table">';
        data += '<thead><tr><th>Product Brand</th><th>Product</th><th>Type</th><th>Value</th></tr></thead>';
        data += '<tbody><tr><td>Brand A</td><td>Product A (Packaging)</td><td>Fixed Quantity</td><td>100</td></tr>';
        data += '<tr><td>Brand A</td><td>Product A (Packaging)</td><td>Fixed Quantity</td><td>100</td></tr></tbody>';
        data += '</table>';
        var tmp = '<div class="popover" role="tooltip"><div class="arrow"></div><h3 class="popover-title"></h3>';
        tmp += '<div class="popover-content"></div></div>';
        var options = {
            placement: 'left',
            title: '<?= __('FOC Product List') ?>',
            html: true,
            content: data,
            container: 'body',
            template: tmp
        };
        $('.foc-preview').popover(options);

        //remove this function when doing real life data
        remainingTable();
        function remainingTable() {            
            var idx;
            for (i = 0; i < 18; i++) {
                idx = i + 3;
                var tbRow = '<tr class="modal-invoice">';
                tbRow += '<td>' + idx + '</td>';
                tbRow += '<td>&nbsp;</td>';
                tbRow += '<td>&nbsp;</td>';
                tbRow += '<td>&nbsp;</td>';
                tbRow += '<td>&nbsp;</td>';
                tbRow += '</tr>';
                $('.table-invoice tbody tr:last').before(tbRow);
            }
        }
    });
</script>