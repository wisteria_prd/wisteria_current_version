<?php $this->assign('title', 'PO Inspecting'); ?>
<div class="row">
    <div class="col-md-12">
        <ol class="breadcrumb">
            <li><?= $this->Html->link(__('HOME'), ['controller' => 'users', 'action' => 'index']) ?></li>
            <li><?= $this->Html->link(__('Purchase Order'), ['action' => 'index']) ?></li>
            <li class="active"><?php echo $this->fetch('title'); ?></li>
        </ol>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <h1><?= __('Order Form') ?></h1>
        <p><strong><?= __('Order Form No.') ?></strong> [purchases.po_number]</p>
        <p><strong><?= __('Issue Date:') ?></strong> [purchases.issue_date]</p>
    </div>
    <div class="col-md-6 text-right">
        <div class="company-invoice-logo text-center">
            Logo Here
        </div>
        <p>3791 Jalan Bukit Merah, #10-17 E-Centre @ Redhill Singapore 159471</p>
        <P>Tel: +65-6274-0433 / Fax: +65-6274-0477</P>
        <P>UEN No: 201403392G / Reg No: 201403392G</P>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <hr class="hr-invoiced">
    </div>
</div>
<div class="row row-top-space">
    <div class="col-md-8">
        <div class="seller-info">
            <h3><?= __('Seller Name') ?></h3>
            <div class="row">
                <div class="col-md-2">
                    <p><?= __('Address 1') ?></p>
                </div>
                <div class="col-md-10">
                    #1
                </div>
            </div>
            <div class="row">
                <div class="col-md-2">
                    <p><?= __('Address 2') ?></p>
                </div>
                <div class="col-md-10">
                    #2
                </div>
            </div>
            <div class="row">
                <div class="col-md-2">
                    <p><?= __('Address 3') ?></p>
                </div>
                <div class="col-md-10">
                    #3
                </div>
            </div>
            <div class="row">
                <div class="col-md-2">
                    <div><?= __('Contact:') ?></div>
                </div>
                <div class="col-md-10">
                    <p>Mr. Daroath</p>
                </div>
            </div>
            <div class="row">
                <div class="col-md-2">
                    <div><?= __('Email:') ?></div>
                </div>
                <div class="col-md-10">
                    <p>daroath.ouk.bitex@gmail.com</p>
                </div>
            </div>
            <div class="row">
                <div class="col-md-2">
                    <div><?= __('Tel:') ?></div>
                </div>
                <div class="col-md-10">
                    <p>081 620 630 / <?= __('Fax:') ?> 081 620 630</p>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row row-invoice-incharge">
    <div class="col-md-offset-8 col-md-4">
        <p>Order in Charge: #xxx</p>
        <p>Email: #xxx</p>
        <p>Mobile: #xxx</p>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <table class="table table-striped table-bordered table-invoice">
            <thead>
                <tr>
                    <th><?= __('No') ?></th>
                    <th width="35%"><?= __('Description') ?></th>
                    <th><?= __('Unit Price (Cur.)') ?></th>
                    <th><?= __('Qty') ?></th>
                    <th><?= __('Amount (Cur.)') ?></th>
                </tr>                
            </thead>
            <tbody>
                <tr>
                    <td class="modal-invoice">1</td>
                    <td class="modal-invoice">
                        <div class="r-description">
                            <div class="r-desc-left">
                                Description
                            </div>
                            <div class="r-desc-right">
                                <button type="button" class="btn btn-primary btn-sm btn-add-inspecting">Inspected</button>
                            </div>
                        </div>
                    </td>
                    <td class="modal-invoice"><label class="label-discounted"><?= __('Discounted') ?></label> 240$</td>
                    <td class="modal-invoice">5</td>
                    <td class="modal-invoice">24,000$</td>
                </tr>
                <tr>
                    <td class="modal-invoice">2</td>
                    <td class="modal-invoice">
                        <div class="r-description">
                            <div class="r-desc-left">
                                <label class="label-foc-child">FOC</label> Description 2
                            </div>
                            <div class="r-desc-right">
                                <button type="button" class="btn btn-primary btn-sm btn-add-inspecting">Inspected</button>
                            </div>
                        </div>                         
                    </td>
                    <td class="modal-invoice">225$</td>
                    <td class="modal-invoice">12</td>
                    <td class="modal-invoice">25,000</td>
                </tr>
                <tr>
                    <td colspan="2">Remarks:</td>
                    <td colspan="2" class="table-text-center">52,000$</td>
                    <td>34,000$</td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
<div class="row">
    <div class="col-md-4">
        <p>Medical Professionals Singapore Pte. Ltd. Peter Lim, Managing Director</p>
    </div>
    <div class="col-md-2">
        <p><?= __('Date:') ?></p>
    </div>
</div>
<div class="row">
    <div class="col-md-4">
        <p class="company-invoice-signature">Digital Signature</p>
    </div>
    <div class="col-md-2">
        <p class="text-center">12/06/2017</p>
    </div>
</div>
<div class="row">
    <div class="col-md-4">
        <hr class="signature-line-approving">
    </div>
    <div class="col-md-2">
        <hr class="signature-line-approving">
    </div>
</div>
<div class="payment">
    <div class="row">
        <div class="col-md-5 col-md-push-7">
            <div class="row">
                <div class="col-md-3">
                    <?= __('Current Status') ?>
                </div>
            </div>
            <div class="row row-top-small-space">
                <div class="col-md-4">
                    <label data-status="draft" class="status label-draft"><?= __('Approved') ?></label>
                    <span class="glyphicon glyphicon-arrow-right arrow-draft"></span>
                </div>
                <div class="col-md-4">
                    <?php
                        $options = ['Finished', 'Cancel'];
                    ?>
                    <?= $this->Form->select('display', $options, ['class' => 'form-control change-invoice-status', 'label' => false]) ?>
                </div>
            </div>
            <div>
                <div class="row row-top-small-space">
                    <div class="col-sm-4"><?= __('Purchase Issue Info:') ?></div>
                    <div class="col-sm-4 issues">
                        
                    </div>
                    <div class="col-sm-4">
                        <button class="btn btn-primary form-btn-add add-new-issue" type="button"><i class="fa fa-plus" aria-hidden="true"></i></button>
                    </div>
                </div>
            </div>
            <div class="row row-top-small-space">
                <div class="col-md-3 col-md-push-9 text-right">
                    <button type="button" class="btn btn-primary btn-submit-approving" data-toggle="popover" data-trigger="click"><?= __('Submit') ?></button>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- modal new item -->
<?= $this->HtmlModal->modalHeader('modalAddItem', __('TXT_PO_ADD_ITEM'), ' modal-lg') ?>
<div class="row">
    <div class="col-md-12">
        <?php
            echo $this->Form->create(isset($user) ? $user : '', [
                    'class' => 'form-po-settings form-horizontal',
                    'autocomplete' => 'off'
                ]
            );
            $this->Form->templates([
                'inputContainer' => '{{content}}'
            ]);
        ?>
        <div class="form-group">
            <div class="col-md-3 col-md-offset-1">
                <div class="checkbox">
                    <label>
                        <?= $this->Form->input('manual', ['type' => 'checkbox', 'label' => false]) ?> <?= __('Manually register FOC') ?>
                    </label>
                </div>
            </div>
            <div class="col-md-5">
                <?php
                    $options = ['PO', 'Brand', 'Product'];
                    echo $this->Form->select('role', $options, [
                        'label' => false,
                        'class' => 'form-control',
                        'empty' => false
                    ]);
                ?>
            </div>
        </div>
        <div class="form-group">
            <label for="" class="col-md-3 col-md-offset-1"><?= __('Product') ?></label>
            <div class="col-md-5">
                <?php
                    echo $this->Form->input('product', [
                        'type' => 'text',
                        'label' => false,
                        'class' => 'form-control',
                        'placeholder' => __('Search Keyword')
                    ]);
                ?>
            </div>
            <div class="col-md-2">
                <button type="button" class="btn btn-primary btn-product-list"><?= __('Product List') ?></button>
            </div>
        </div>
        <div class="form-group">
            <div class="col-md-5 col-md-offset-4">
                <textarea rows="5" class="form-control"></textarea>
            </div>
        </div>
        <div class="form-group">
            <label for="" class="col-md-3 col-md-offset-1"><?= __('Quantity') ?></label>
            <div class="col-md-5">
                <?php
                    echo $this->Form->input('quantity', [
                        'type' => 'text',
                        'label' => false,
                        'class' => 'form-control quantity',
                        'placeholder' => __('Input Quantity')
                    ]);
                ?>
            </div>
        </div>
        <div class="form-group">
            <label for="" class="col-md-3 col-md-offset-1"><?= __('Unit Price') ?></label>
            <div class="col-md-5">
                <?php
                    echo $this->Form->input('unit_price', [
                        'type' => 'text',
                        'label' => false,
                        'class' => 'form-control unit-price',
                        'disabled' => true,
                        'placeholder' => __('xxx')
                    ]);
                ?>
            </div>
            <div class="col-md-2">
                <button type="button" class="btn btn-primary popup-discount"><?= __('View Discount') ?></button>
            </div>
        </div>
        <div class="form-group">
            <div class="col-md-5 col-md-offset-4">
                <div class="checkbox">
                    <label>
                        <?= $this->Form->input('manual', ['type' => 'checkbox', 'class' => 'chk-register-foc', 'label' => false]) ?> <?= __('Manually register FOC') ?>
                    </label>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label for="" class="col-md-3 col-md-offset-1"><?= __('Expected Receiving Date') ?></label>
            <div class="col-md-5">
                <div class="input-group with-datepicker">
                    <?= $this->Form->input('date', ['class' => 'form-control datepicker', 'label' => false]) ?>
                    <div class="input-group-btn">
                        <button type="button" class="btn btn-default trigger-calendar" aria-label="Calendar">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-group">
            <div class="col-md-offset-1 col-md-3">
                <button type="button" class="btn btn-default" data-dismiss="modal"><?= __('Cancel') ?></button>
            </div>
            <div class="col-md-offset-3 col-md-3">
                <button type="button" class="btn btn-primary"><?= __('Submit') ?></button>
            </div>
        </div>
        <?= $this->Form->end() ?>
    </div>
</div>
<?= $this->HtmlModal->modalFooter(); ?>
<!-- /end modal new item -->
<!-- modal discount conditions -->
<?= $this->HtmlModal->modalHeader('modalDiscountCondition', __('Discount Conditions')) ?>
<div class="row">
    <div class="col-md-12">
        <table class="table table-striped table-view-discount">
            <thead>
                <tr>
                    <th>#</th>
                    <th><?= __('Discount Type') ?></th>
                    <th><?= __('Range Type') ?></th>
                    <th><?= __('From') ?></th>
                    <th><?= __('To') ?></th>
                    <th>&nbsp;</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>1</td>
                    <td>FOC</td>
                    <td>Quantity</td>
                    <td class="qty-from">21</td>
                    <td class="qty-to">25</td>
                    <td><button type="button" class="btn btn-danger foc-preview" data-toggle="popover" data-trigger="hover">View FOC</button></td>
                </tr>
                <tr>
                    <td>2</td>
                    <td>FOC</td>
                    <td>Quantity</td>
                    <td class="qty-from">26</td>
                    <td class="qty-to">29</td>
                    <td><button type="button" class="btn btn-danger foc-preview" data-toggle="popover" data-trigger="hover">View FOC</button></td>
                </tr>
                <tr>
                    <td>3</td>
                    <td>FOC</td>
                    <td>Quantity</td>
                    <td class="qty-from">30</td>
                    <td class="qty-to">34</td>
                    <td><button type="button" class="btn btn-danger foc-preview" data-toggle="popover" data-trigger="hover">View FOC</button></td>
                </tr>
                <tr>
                    <td>4</td>
                    <td>FOC</td>
                    <td>Quantity</td>
                    <td class="qty-from">32</td>
                    <td class="qty-to">40</td>
                    <td><button type="button" class="btn btn-danger foc-preview" data-toggle="popover" data-trigger="hover">View FOC</button></td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
<div class="row">
    <div class="col-md-12 text-center">
        <button type="button" class="btn btn-default" data-dismiss="modal"><?= __('Close') ?></button>
    </div>
</div>
<?= $this->HtmlModal->modalFooter(); ?>
<!-- /end modal discount conditions -->
<!-- modal pdf upload -->
<?= $this->HtmlModal->modalHeader('modalPDFUpload', __('Document Upload')) ?>
<div class="row">
    <div class="col-md-12">
        <?php
            echo $this->Form->create('User',[
                'autocomplete' => 'off',
                'class' => 'form-pdf-upload form-horizontal',
                'onsubmit' => 'return false'
            ]);
            $this->Form->templates([
                'inputContainer' => '{{content}}'
            ]);
        ?>
        <div class="form-group">
            <label for="" class="control-label col-md-3"><?= __('Type') ?></label>
            <div class="col-md-7">
                <?php
                    $options = ['PDF', 'JPEG'];
                    echo $this->Form->select('type', $options, [
                        'label' => false,
                        'class' => 'form-control',
                        'empty' => false
                    ]);
                ?>
            </div>
        </div>
        <div class="form-group">
            <label for="" class="control-label col-md-3"><?= __('PDF File') ?></label>
            <div class="col-md-7">
                <?php
                    echo $this->Form->input('file', [
                        'type' => 'file',
                        'label' => false
                    ]);
                ?>
            </div>
        </div>
        <?= $this->Form->end() ?>
    </div>
</div>
<div class="row">
    <div class="col-md-12 text-center">
        <button type="button" class="btn btn-primary" data-dismiss="modal"><?= __('Upload') ?></button>
    </div>
</div>
<?= $this->HtmlModal->modalFooter(); ?>
<!-- /end modal pdf upload -->
<!-- modal issue -->
<?= $this->HtmlModal->modalHeader('modalPurchaseIssue', __('Purchase Issues')) ?>
<div class="row">
    <div class="col-md-12">
        <?php
            echo $this->Form->create('User',[
                'autocomplete' => 'off',
                'class' => 'form-pdf-upload form-horizontal',
                'onsubmit' => 'return false',
                'name' => 'common_form'
            ]);
            $this->Form->templates([
                'inputContainer' => '{{content}}'
            ]);
        ?>
        <div class="form-group custom-select">
            <label for="" class="control-label col-md-3"><?= __('Product') ?></label>
            <div class="col-md-7">
                <?php
                    echo $this->Form->select('type', [], [
                        'label' => false,
                        'class' => 'form-control',
                        'empty' => false,
                        'id' => 'product-list'
                    ]);
                ?>
            </div>
        </div>
        <div class="form-group">
            <label for="" class="control-label col-md-3"><?= __('Quantity') ?></label>
            <div class="col-md-7">
                <?php
                    echo $this->Form->input('file', [
                        'type' => 'text',
                        'label' => false,
                        'class' => 'form-control'
                    ]);
                ?>
            </div>
        </div>
        <?= $this->Form->end() ?>
    </div>
</div>
<div class="row">
    <div class="col-md-6 text-right">
        <button type="button" class="btn btn-default btn-sm btn-width" data-dismiss="modal"><?= __('Cancel') ?></button>
    </div>
    <div class="col-md-6 text-left">
        <button type="button" class="btn btn-primary btn-sm btn-width"><?= __('Submit') ?></button>
    </div>
</div>
<?= $this->HtmlModal->modalFooter(); ?>
<!-- /end modal issue -->
<!-- modal add inspecting -->
<?= $this->HtmlModal->modalHeader('modalAddInspecting', __('Product Name'), ' modal-lg') ?>
<div class="row">
    <div class="col-md-12">
        <div class="row">
            <div class="col-sm-6">
                <div class="row">
                    <div class="col-sm-6 field-small-padding-right">
                        <?php
                            echo $this->Form->input('file', [
                                'type' => 'text',
                                'label' => false,
                                'class' => 'form-control',
                                'placeholder' => 'Batch Number'
                            ]);
                        ?>
                    </div>
                    <div class="col-sm-6 field-small-padding-left">
                        <?php
                            echo $this->Form->input('file', [
                                'type' => 'text',
                                'label' => false,
                                'class' => 'form-control',
                                'placeholder' => 'Expiry'
                            ]);
                        ?>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 damages">
                <div class="row row-damage">
                    <div class="col-sm-3">
                        <label class="control-label" style="margin-top: 7px;">Damage A</label>
                    </div>
                    <div class="col-sm-7">
                        <?php
                            echo $this->Form->input('file', [
                                'type' => 'text',
                                'label' => false,
                                'class' => 'form-control',
                                'placeholder' => 'Quantity'
                            ]);
                        ?>
                    </div>
                    <div class="col-sm-2 field-no-padding-left">
                        <button class="btn btn-primary form-btn-add add-new-damage" type="button"><i class="fa fa-plus" aria-hidden="true"></i></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row row-top-space">
    <div class="col-sm-12">
        <table class="table table-striped table-damages">
            <thead>
                <tr>
                    <th>#</th>
                    <th><?= __('Batch') ?></th>
                    <th><?= __('Expiry') ?></th>
                    <th><?= __('Damage') ?></th>
                    <th><?= __('Quantity') ?></th>
                    <th><?= __('Total Qty') ?></th>
                    <th width="11.5%">&nbsp;</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>1</td>
                    <td>xxxxxx</td>
                    <td>2017-06-15</td>
                    <td>A<br />B<br />C<br />D<br /></td>
                    <td>3<br />4<br />5<br />1<br /></td>
                    <td>13</td>
                    <td>
                        <a href="#" class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                        &nbsp;&nbsp;
                        <button class="btn btn-delete btn-sm" id="btn_delete" type="submit"><i class="fa fa-trash" aria-hidden="true"></i></button>
                    </td>
                </tr>
                <tr>
                    <td>2</td>
                    <td>xxxxx</td>
                    <td>2017-06-15</td>
                    <td>A<br />B<br />C<br />D<br /></td>
                    <td>5<br />4<br />5<br />1<br /></td>
                    <td>15</td>
                    <td>
                        <a href="#" class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                        &nbsp;&nbsp;
                        <button class="btn btn-delete btn-sm" id="btn_delete" type="submit"><i class="fa fa-trash" aria-hidden="true"></i></button>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
<div class="row">
    <div class="col-md-6 text-right">
        <button type="button" class="btn btn-default btn-sm btn-width" data-dismiss="modal"><?= __('Cancel') ?></button>
    </div>
    <div class="col-md-6 text-left">
        <button type="button" class="btn btn-primary btn-sm btn-width"><?= __('Register') ?></button>
    </div>
</div>
<?= $this->HtmlModal->modalFooter(); ?>
<!-- /end modal add inspecting -->
<?php
    echo $this->Html->css([
        'bootstrap-datetimepicker.min',
        'jquery-ui',
        'intlTelInput',
    ]);
?>
<script>
    $(document).ready(function() {
        var qty_highlight;
        $('.datepicker').datetimepicker({
           format: 'YYYY-MM-DD'
        });

        $('body').on('click', '.btn-add-issue', function () {
            $('#modalPurchaseIssue').modal({
                backdrop: 'static',
                keyboard: false
            });
        });

        GetDataList();

        function GetDataList() {
            var url = '<?php echo $this->Url->build('/product-details/product-list'); ?>';
            $.ajax({
                url: url,
                type: 'get',
                dataType: 'json',
                contentType: 'application/json; charset=utf-8'
            }).done(function (data) {
                var response = data.data;
                var element = '<option value="" data-pid=""><?= __('Search...') ?></option>';
                var pid;
                if (response != null && response != undefined) {
                    $.each(response, function (i, v) {
                        pid = v.product.id;
                        var product = v.product.name<?= $this->request->session()->read('tb_field') ?> + ' ';
                        product += '(' + v.pack_size;
                        product += v.tb_pack_sizes.name<?= $this->request->session()->read('tb_field') ?> + ' x ';
                        product += v.single_unit_size;
                        product += v.single_unit.name<?= $this->request->session()->read('tb_field') ?> + ')';

                        element += '<option value="'+ v.id +'">'+ product +'</option>';
                    });
                    $('#product-list').attr('data-pid', pid);
                    $('#product-list').append(element).select2({
                        width: '100%'
                    });
                }
            });
        }

        $('body').on('click', '.btn-add-inspecting', function (e) {
            e.stopPropagation();
            $('#modalAddInspecting').modal({
                backdrop: 'static',
                keyboard: false
            });
        });

        $('body').on('click', '.add-new-issue', function () {
            var cls = '';
            if ($('.button-issue').length) {
                cls = ' row-top-space';
            }
            var item = '<div class="row button-issue'+cls+'">';
                item += '<div class="col-sm-12">';
                item += '<span class="glyphicon glyphicon-remove-circle icon-remove-issue"></span>';
                item += '<button type="button" class="btn btn-default btn-add-issue">Issue Id</button>';
                item += '</div>';
                item += '</div>';

            $('.issues').append(item);
        });

        $('body').on('click', '.icon-remove-issue', function () {
            $(this).closest('.button-issue').remove();
        });

        $('body').on('click', '.add-new-damage', function () {
            var item = $('.row-damage').first().clone();
            item.find('.col-sm-4:eq(0)').html('&nbsp;');
            item.addClass('row-top-space');
            item.find('button').removeClass('add-new-damage btn-primary').addClass('btn-delete remove-damage').html('<i class="fa fa-trash" aria-hidden="true"></i>');
            $('.damages').append(item);
        });

        $('body').on('click', '.remove-damage', function () {
            $(this).parents('.row-damage').remove();
        });

        var msg = '<p><?= __('Are you sure to change the status to XXX') ?></p>';
        msg += '<div class="text-center">';
        msg += '<button class="btn btn-default confirm-yes" type="button">Yes</button>';
        msg += '<button class="btn btn-default confirm-no" type="button">No</button>';
        msg += '</div>';
        var tmp = '<div class="popover" role="tooltip"><div class="arrow"></div><h3 class="popover-title"></h3>';
        tmp += '<div class="popover-content"></div></div>';
        var options = {
            placement: 'left',
            title: '<?= __('Confirm Inspecting') ?>',
            html: true,
            content: msg,
            container: 'body',
            template: tmp
        };
        $('.btn-submit-approving').popover(options);


        $('body').on('click', '.confirm-no', function() {
            $('.btn-submit-approving').click();
        });

        $('body').on('click', '.confirm-yes', function() {
            $('.btn-submit-approving').click();
            alert('Demo Finished!');
        });

        $('body').on('click', '.add-new-file', function() {
            $('#modalPDFUpload').modal({
                backdrop: 'static',
                keyboard: false
            });
        });

        $('.trigger-calendar').click(function() {
            $(this).parents('.with-datepicker').find('.datepicker').focus();
            //$('.datepicker').focus();
        });

        $('body').on('click', '.modal-invoice', function() {
            $('#modalAddItem').modal({
                backdrop: 'static',
                keyboard: false
            });
        });

        $('body').on('click', '.chk-register-foc', function() {
            if ($(this).is(':checked')) {
                $('.unit-price').removeAttr('disabled', true);
            } else {
                $('.unit-price').attr('disabled', true);
            }
        });

        //view discount conditions
        $('body').on('click', '.popup-discount', function() {
            qty_highlight = $('.quantity').val();
            $('#modalDiscountCondition').on('shown.bs.modal', function() {
                $('.table-view-discount tbody tr').each(function() {
                    $(this).removeClass('row-highlight');
                    var from = parseInt($(this).find('.qty-from').text());
                    var to = parseInt($(this).find('.qty-to').text());
                    if (qty_highlight >= from && qty_highlight <= to) {
                        $(this).addClass('row-highlight');
                    }
                });
            }).modal({
                backdrop: 'static',
                keyboard: false
            });
        });

        var data = '<table class="table">';
        data += '<thead><tr><th>Product Brand</th><th>Product</th><th>Type</th><th>Value</th></tr></thead>';
        data += '<tbody><tr><td>Brand A</td><td>Product A (Packaging)</td><td>Fixed Quantity</td><td>100</td></tr>';
        data += '<tr><td>Brand A</td><td>Product A (Packaging)</td><td>Fixed Quantity</td><td>100</td></tr></tbody>';
        data += '</table>';
        var tmp = '<div class="popover" role="tooltip"><div class="arrow"></div><h3 class="popover-title"></h3>';
        tmp += '<div class="popover-content"></div></div>';
        var options = {
            placement: 'left',
            title: '<?= __('FOC Product List') ?>',
            html: true,
            content: data,
            container: 'body',
            template: tmp
        };
        $('.foc-preview').popover(options);

        //remove this function when doing real life data
        remainingTable();
        function remainingTable() {
            var idx;
            for (i = 0; i < 18; i++) {
                idx = i + 3;
                var tbRow = '<tr class="modal-invoice">';
                tbRow += '<td>' + idx + '</td>';
                tbRow += '<td>&nbsp;</td>';
                tbRow += '<td>&nbsp;</td>';
                tbRow += '<td>&nbsp;</td>';
                tbRow += '<td>&nbsp;</td>';
                tbRow += '</tr>';
                $('.table-invoice tbody tr:last').before(tbRow);
            }
        }
    });
</script>