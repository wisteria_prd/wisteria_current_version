
<?php
ini_set('memory_limit', '-1');

// create new PDF document
// $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
$pdf = new TCPDF('', 'mm', 'A4', true, 'UTF-8');
// Reset the encoding forced from tcpdf
mb_internal_encoding('UTF-8');

$font_jp = TCPDF_FONTS::addTTFfont(WWW_ROOT.'fonts/ipag.ttf');
//$pdf->SetFont($font1, '', 14);

// language
if ($this->request->session()->read('tb_field') === 'en') {
    $language = 'Helvetica';
} else {
    $language = $font_jp;
}
// test language
$language = $font_jp;

ob_clean(); // cleaning the buffer before Output()

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Nick');
$pdf->SetTitle('Purchase Order Form');
$pdf->SetSubject('Purchase Order Form');
$pdf->SetKeywords('');

// Configure header and footer
$pdf->SetPrintHeader(false);
$pdf->SetPrintFooter(true);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
    require_once(dirname(__FILE__).'/lang/eng.php');
    $pdf->setLanguageArray($l);
}
// set default font subsetting mode
$pdf->setFontSubsetting(true);
$pdf->SetFont('Helvetica', '', 8);

// page padding
$page_padding_top = 10;
$page_padding_left = 10;

$pdf->AddPage();
// header page
$pdf->SetFont($language, '', 8);

$pdf->SetFillColor(255,255,255);
//$pdf->MultiCell($width = 80, $height = 60, $text = 'Main column', $border = 1, $align = 'L', $fill=true, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = 60, $valign = 'T');

// header title
$getY = $pdf->getY();
$pdf->SetFont($language, '', 20);
$pdf->Cell(190, 10, '委任状', 0, false, 'C');
$pdf->ln();
$pdf->Cell(190, 40, '厚生労働大臣　殿', 0, false, 'L');
$pdf->ln();


$pdf->SetFont($language, '', 12);
$pdf->Cell(20, 4, '【受任者】', 0, false, 'L');
$pdf->ln();

$pdf->Cell(36, 5, '所在地：', 0, false, 'R');

$address = '〒103-0025'. "\n";
$address .= '東京都中央区日本橋茅場町1-4-6　木村實業第２ビル４Ｆ' . "\n";
$address .= 'TEL：03-4588-1847　　　　FAX：03-4588-1848' . "\n";
$address .= '株式会社ウィステリア     入月弘美';
$pdf->MultiCell($width = 120, $height = 40, $text = $address, $border = 0, $align = 'L', $fill=true, $ln = 0, $x = 45, $y = 65, $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = 60, $valign = 'T');
$pdf->ln();

$text = '私は上記の者を代理人と定め、医療機器および医薬品等の輸入に必要な薬監証明の取得に関す'. "\n";
$text .= 'る一切の権限を委任します。';
$pdf->MultiCell($width = 180, $height = 20, $text = $text, $border = 0, $align = 'L', $fill=true, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = 60, $valign = 'T');
$pdf->ln();

$text = '平成　29年　月　日';
$pdf->MultiCell($width = 170, $height = 10, $text = $text, $border = 0, $align = 'R', $fill=true, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = 60, $valign = 'T');
$pdf->ln();

$pdf->Cell(40, 5, '【委任者】', 0, false, 'L');
$pdf->ln();

$text = '所属医療機関名 ：   a' . "\n";
$text .= '医　　　師 ：       a' . "\n";
$text .= '所　　　在 ：       a';
$pdf->MultiCell($width = 150, $height = 20, $text = $text, $border = 0, $align = 'L', $fill=true, $ln = 0, $x = 27, $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = 60, $valign = 'T');
$pdf->ln();

$pdf->Output();
