
<?php
$local = $this->request->session()->read('tb_field');
$breadcrumb = [
    '' => $breadcrumbs
];
echo $this->Comment->breadCrumb($breadcrumb);
?>
<div class="search-section" style="margin-bottom: 20px;">
    <?php
    echo $this->Form->create('PurchaseOrders', [
        'role' => 'form',
        'class' => 'form-inline form-search',
        'type' => 'get',
        'name' => 'form_search'
    ]);
    echo $this->Form->input('keyword', [
        'name' => 'keyword',
        'placeholder' => __('USER_SEARCH_KEYWORD'),
        'class' => 'form-control',
        'label' => false,
        'value' => $this->request->query('keyword'),
        'autocomplete' => 'off',
        'templates' => [
            'inputContainer' => '<div class"form-group">{{content}}</div>',
        ]
    ]);
    echo $this->Form->submit('submit', [
        'style' => 'display: none',
    ]);
    echo $this->Form->end();
    ?>
</div>
<div class="total-pagination" style="padding-bottom: 20px;">
    <?php echo $this->element('display_info'); ?>
</div>
<div class="row search-section" style="margin-bottom: 20px;">
    <?php echo $this->element('display_number'); ?>
    <div class="col-sm-6 pull-right" style="text-align: right;">
        <?php
        echo $this->Form->button(__('BTN_REGISTER'), [
            'type' => 'button',
            'class' => 'btn btn-sm btn-primary btn-width btn-register-po',
            'data-type' => 'register',
        ]);
        ?>
    </div>
    <div class="clearfix"></div>
</div>
<div class="data-tb-list">
    <?php if ($data): ?>
    <table class="table table-striped table-pos">
        <thead>
            <tr>
                <th>#</th>
                <th>&nbsp;</th>
                <th>
                    <?php echo $this->Paginator->sort('status', __('TXT_STATUS')); ?>
                    <i class="fa fa-sort-desc" aria-hidden="true"></i>
                </th>
                <th class="white-space">
                    <?php echo $this->Paginator->sort('issue_date', __('TXT_ORDER_DATE')); ?>
                    <i class="fa fa-sort-desc" aria-hidden="true"></i>
                </th>
                <th class="white-space">
                    <?php echo $this->Paginator->sort('Users.lastname' . $local, __('TXT_PO_BY')); ?>
                    <i class="fa fa-sort-desc" aria-hidden="true"></i>
                </th>
                <th class="white-space">
                    <?php echo $this->Paginator->sort('Seller.id', __('TXT_SELLER')); ?>
                    <i class="fa fa-sort-desc" aria-hidden="true"></i>
                </th>
                <th class="white-space"><?= __('TXT_AMOUNT') ?></th>
                <th colspan="5">&nbsp;</th>
                <th>&nbsp;</th>
            </tr>
        </thead>
        <tbody>
            <?php
            $numbering = $this->Paginator->counter('{{start}}');
            foreach ($data as $key => $value) :
            ?>
            <tr data-id="<?= $value->id ?>">
                <th><?php echo $numbering; $numbering++; ?></th>
                <td>
                    <?php if ($value->is_suspend == IS_SUSPENDED) : ?>
                        <p class="btn btn-warning btn-xs" disabled><?php echo __('TXT_LB_DISABLED'); ?></p>
                    <?php endif; ?>
                </td>
                <td><?= $value->status ?></td>
                <td><?= date('Y-m-d', strtotime($value->issue_date)) ?></td>
                <td><?= $value->user->lastname . $local . ' ' . $value->user->firstname . $local ?></td>
                <td>
                    <?php
                    if ($value->seller->type === TYPE_MANUFACTURER) {
                        echo $value->seller->manufacturer->name . $local;
                    } else {
                        echo $value->seller->supplier->name . $local;
                    }
                    ?>
                </td>
                <td>
                    <?php
                    $total = 0;
                    if ($value->purchase_details) {
                        $total = $value->purchase_details[0]->total_amount;
                    }
                    echo $value->currency->code . ' ' . $this->Comment->numberFormat($total);
                    ?>
                </td>
                <td width="1%">
                    <div class="btn-group">
                        <button class="btn btn-primary btn-sm dropdown-toggle btn-edit-doc" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <?= __('TXT_PO_EDIT_DOC') ?>
                        </button>
                        <ul class="dropdown-menu">
                            <li><a href="#">Action A</a></li>
                            <li><a href="#">Action B</a></li>
                            <li><a href="#">Action C</a></li>
                        </ul>
                    </div>
                </td>
                <td width="1%">
                    <?php
                    echo $this->Form->button(__('TXT_PO_DOWNLOAD_PDF'), [
                        'type' => 'button',
                        'class' => 'btn btn-primary btn-sm',
                        'id' => false
                    ]);
                    ?>
                </td>
                <td width="1%">
                    <?php
                    echo $this->Form->button(__('TXT_PO_DOWNLOAD_INVOICE'), [
                        'type' => 'button',
                        'class' => 'btn btn-primary btn-sm',
                        'id' => false
                    ]);
                    ?>
                </td>
                <td>
                    <?php echo $this->Form->button('<i class="fa fa-comments-o" aria-hidden="true"></i>', [
                        'class' => 'btn btn-primary btn-sm btn_comment',
                        'data-id' => $value->id,
                        'escape' => false,
                    ]); ?>
                </td>
                <td data-target="<?= $value->is_suspend ? 1 : 0; ?>">
                    <?php
                    if ($value->is_suspend == 1) {
                        echo $this->Form->button(BTN_ICON_UNDO, [
                            'class' => 'btn btn-recover btn-sm btn_suspend',
                            'data-name' => 'recover',
                            'escape' => false
                        ]);
                    } else {
                        echo $this->Form->Button(BTN_ICON_EDIT, [
                            'class' => 'btn btn-primary btn-sm btn-edit',
                            'escape' => false
                        ]);
                    }
                    ?>
                </td>
                <td data-target="<?= $value->is_suspend ? 1 : 0; ?>">
                    <?php
                    if ($value->is_suspend == 1) {
                        echo $this->Form->button(BTN_ICON_DELETE, [
                            'class' => 'btn btn-delete btn-sm',
                            'id' => 'btn_delete',
                            'data-name' => 'delete',
                            'escape' => false
                        ]);
                    } else {
                        echo $this->Form->button(BTN_ICON_STOP, [
                            'class' => 'btn btn-suspend btn-sm btn_suspend',
                            'data-name' => 'suspend',
                            'escape' => false
                        ]);
                    }
                    ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <?php endif; ?>
</div>
<div class="row row-top-space">
    <?php echo $this->element('display_number'); ?>
    <div class="clearfix"></div>
</div>

<!--Modal Register-->
<?= $this->element('Modal/purchase_order'); ?>
<!--Modal Detail-->
<?= $this->element('Modal/view_detail'); ?>
<!--Modal Comment-->
<?= $this->element('/Modal/message') ?>
<!--Modal Suspend-->
<?= $this->element('Modal/suspend'); ?>
<!--Modal delete-->
<?= $this->element('Modal/delete'); ?>

<?php
echo $this->Html->css([
    'bootstrap-datetimepicker.min',
]);
?>
<script>
    (function(e) {
        //auto complete
        $('body').find('#keyword').typeahead({
            minLength: 0,
            autoSelect: true,
            source:  function (query, process) {
                return $.get('<?= $this->Url->build('/purchases/getPurchaseNumberBySearch/') ?>', {
                        keyword: query
                    }, function (data) {
                        var optionData = [];
                        if (data.data !== null && data.data !== 'undefined') {
                            $.each(data.data, function(i, v) {
                                optionData.push(v.purchase_number);
                            });
                        }
                        return process(optionData);
                    }, 'json');
            }
        });

        $('body').on('click', '.typeahead li', function(e) {
            $('body').find('.form-search').submit();
        });

        $('#seller-id, #currency-id, #type, #affiliation-class').select2({
            width: '100%',
        });

        $('.btn_comment').click(function (e) {
            e.stopPropagation();
            let params = {
                id: $(this).closest('tr').attr('data-id'),
                type: '<?php echo TYPE_PO_TYPE_MP_TO_S ?>',
                referer: '<?php echo $this->request->controller; ?>'
            };
            let options = {
                url: BASE + 'Messages/getMessageList',
                type: 'GET',
                data: params,
                beforeSend: function () {
                    $.LoadingOverlay('show');
                }
            };
            ajaxRequest(options, SESSION_TIMEOUT, function (data) {
                $('body').prepend(data);
                $('body').find('.modal-message').modal('show');
            });
        });

        //filter message list search to specific
        $('body').on('click', '.btn-to-individual', function() {
            $(this).removeClass('btn-detault').addClass('btn-primary');
            $('body').find('.btn-to-all').removeClass('btn-primary').addClass('btn-default');
            var toAll = $('.to-individual').val();
            if (parseInt(toAll) === 0) {
                $('.to-all').val(0);
                $('.to-individual').val(1);
            } else {
                $('.to-all').val(1);
                $('.to-individual').val(0);
            }
            messagesSearch(listUrlSearch);
        });

        $('body').on('click', '.btn-search', function() {
            messagesSearch(listUrlSearch);
        });

        $('body').on('click', '.btn-suspend-msg', function() {
            var id = $(this).data('id');
            var data = $('#form-message-status-' + id).serialize();
            var suspendUrl = '<?= $this->Url->build(['controller' => 'Messages', 'action' => 'suspend']) ?>';
            suspendMessage(id, suspendUrl, data);
        });

        $('#datepicker').datetimepicker({
           format: 'YYYY-MM-DD'
        });

        $('body').on('click', '#trigger-calendar', function () {
            $('#datepicker').focus();
        });

        $('body').on('click', '.table>tbody>tr>td a', function(e) {
            e.stopPropagation();
        });

        $('body').on('click', '.btn-register', function(e) {
            $.LoadingOverlay('show');
            var type = $(this).attr('data-type');
            var form = $('body').find('form[name="purchase_order"]');
            var url = '<?= $this->Url->build(['action' => 'create']); ?>';

            if (type === 'edit') {
                var id = $(this).attr('data-id');
                url = '<?= $this->Url->build('/purchases/create/'); ?>' + id;
            }

            $.post(url, $(form).serialize(), function(data) {
                var response = data.data;
                $(form).find('.error-message').remove();

                if (response !== null && response !== undefined) {
                    $.each(response, function(i, v) {
                        var message = '<label class="error-message">' + v + '</label>';
                        var content = $(form).find('[name="' + i + '"]');

                        $(content).closest('.col-md-7').append(message);
                    });
                } else {
                    console.log(data);
                    // location.href = '<?= $this->Url->build('/purchases/po-detail/') ?>';
                }
            }, 'json')
            .done(function(data) {
                $.LoadingOverlay('hide');
            })
            .fail(function(jqXHR, textStatus, errorThrown) {
                if (errorThrown === 'Forbidden') {
                    if (confirm('<?= __('Session timeout. Please login again.') ?>')) {
                        location.reload();
                    }
                }
            })
            .always(function() {
                $.LoadingOverlay('hide');
            });
        });

        $('body').on('click', '.btn-edit', function(e) {
            $.LoadingOverlay('show');
            var id = $(this).closest('tr').attr('data-id');
            var url = '<?= $this->Url->build('/purchases/edit/') ?>' + id;
            var form = $('body').find('form[name="purchase_order"]');

            $.get(url, function(data) {
                var response = data.data;
                if (response !== null && response !== undefined) {
                    var issue_date = $.datepicker.formatDate('yy-mm-dd', new Date(response.issue_date));

                    $(form).find('#seller-id').val(response.seller_id).trigger('change');
                    $(form).find('#currency-id').val(response.currency_id).trigger('change');
                    $(form).find('#type').val(response.type).trigger('change');
                    $(form).find('#datepicker').val(issue_date);
                    $(form).find('#affiliation-class').val(response.affiliation_class).trigger('change');
                }
            }, 'json')
            .done(function(data) {
                $.LoadingOverlay('hide');
                $('#modal-po-ms-to-s').modal({
                    backdrop: 'static',
                    keyboard: false
                });
                $('#modal-po-ms-to-s').find('.btn-register').attr({
                    'data-type': 'edit',
                    'data-id': id
                }).text('<?= __('TXT_UPDATE') ?>');
            })
            .fail(function(jqXHR, textStatus, errorThrown) {
                if (errorThrown === 'Forbidden') {
                    if (confirm('<?= __('Session timeout. Please login again.') ?>')) {
                        location.reload();
                    }
                }
            })
            .always(function() {
                $.LoadingOverlay('hide');
            });
        });

        $('body').on('click', '.btn-register-po', function() {
            $('#modal-po-ms-to-s').modal({
                backdrop: 'static',
                keyboard: false
            });
            $('#modal-po-ms-to-s').find('.btn-register').attr('data-type', 'register').text('<?= __('BTN_REGISTER') ?>');
        });

        $('body').on('click', '.btn_suspend', function (e) {
            e.stopPropagation();
            var content = $('#modal_suspend');
            var status = $(this).attr('data-name');
            var txt = '';

            switch (status) {
                case 'suspend':
                    $('#btn_suspend_yes').text('<?= __('BTN_SUSPEND') ?>');
                    txt = '<?= __('MSG_CONFIRM_SUSPEND') ?>';
                    break;
                case 'delete':
                    txt = '<?= __('MSG_CONFIRM_DELETE') ?>';
                    $('#btn_suspend_yes').text('<?= __('BTN_DELETE') ?>');
                    break;
                 case 'recover':
                    $('#btn_suspend_yes').text('<?= __('BTN_RECOVER') ?>');
                    txt = '<?= __('MSG_CONFIRM_UNDO_SUSPEND') ?>';
                    break;
            };

            $(content).find('#btn_suspend_yes').data('id', $(this).closest('tr').data('id'));
            $(content).find('#btn_suspend_yes').data('target', $(this).closest('td').data('target'));
            $(content).find('.modal-body p').html(txt);
            $(content).modal('show');
        });

//        $('body').on('click', '.data-tb-list .table>tbody>tr', function (e) {
//            $.LoadingOverlay('show');
//            $.get('<?php echo $this->Url->build('/manufacturers/view/'); ?>' + $(this).closest('tr').data('id'), function (data) {
//                $.LoadingOverlay('hide');
//                $('#modal_detail').find('.modal-body').html(data);
//                $('#modal_detail').modal('show');
//            }, 'html');
//        });

        $('body').on('click', '#btn_suspend_yes', function (e) {
            var params = {
                id: $(this).data('id'),
                is_suspend: $(this).data('target')
            };
            var url = '<?php echo $this->Url->build('/purchases/update-suspend/'); ?>';

            $.post(url, params, function (data) {
                if (data.message === 'success') {
                    location.reload();
                }
            }, 'json');
        });

        $('body').on('click', '#btn_delete', function (e) {
            e.stopPropagation();
            var content = $('#modal_delete');

            $(content).find('#btn_delete_yes').attr('data-id', $(this).closest('tr').data('id'));
            $(content).modal('show');
        });

        $('body').on('click', '#btn_delete_yes', function (e) {
            var url = '<?php echo $this->Url->build('/purchases/delete/'); ?>';

            $.post(url, {id: $(this).data('id')}, function (data) {
                if (data.message === 'success') {
                    location.reload();
                }
            }, 'json');
        });
    })();
</script>
