<?php $this->assign('title', 'PO Edit'); ?>
<?php
$breadcrumb = [
    'po/index/' => __('発注') . ' ' . __('発注一覧'),
    '' => __('編集'),
];
echo $this->Comment->breadCrumb($breadcrumb);
echo $this->element('Form/po');