
<?php
ini_set('memory_limit', '-1');

// create new PDF document
// $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
$pdf = new TCPDF('', 'mm', 'A4', true, 'UTF-8');
// Reset the encoding forced from tcpdf
mb_internal_encoding('UTF-8');

$font_jp = TCPDF_FONTS::addTTFfont(WWW_ROOT.'fonts/ipag.ttf');
//$pdf->SetFont($font1, '', 14);

// language
if ($this->request->session()->read('tb_field') === 'en') {
    $language = 'Helvetica';
} else {
    $language = $font_jp;
}
// test language
$language = $font_jp;

ob_clean(); // cleaning the buffer before Output()

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Nick');
$pdf->SetTitle('Purchase Order Form');
$pdf->SetSubject('Purchase Order Form');
$pdf->SetKeywords('');

// Configure header and footer
$pdf->SetPrintHeader(false);
$pdf->SetPrintFooter(false);

// set margins
$pdf->SetMargins(20, PDF_MARGIN_TOP, 20);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
    require_once(dirname(__FILE__).'/lang/eng.php');
    $pdf->setLanguageArray($l);
}
// set default font subsetting mode
$pdf->setFontSubsetting(true);
$pdf->SetFont('Helvetica', '', 8);

// page padding
$page_padding_top = 10;
$page_padding_left = 10;

$pdf->AddPage();
// header page
$pdf->SetFont($language, '', 8);

$pdf->SetFillColor(255,255,255);
//$pdf->MultiCell($width = 80, $height = 60, $text = 'Main column', $border = 1, $align = 'L', $fill=true, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = 60, $valign = 'T');

// header title
$getY = $pdf->getY();
$pdf->SetFont($language, '', 12);
$pdf->Cell(170, 10, '〔別紙参考様式５〕', 0, false, 'L');
$pdf->ln();

$pdf->SetFont($language, '', 14);
$pdf->Cell(170, 10, '必要理由書', 0, false, 'C');
$pdf->ln();

$pdf->SetFont($language, '', 11);
$pdf->MultiCell($width = 170, $height = 10, $text = 'Date', $border = 0, $align = 'R', $fill=true, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = 10, $valign = 'M');
$pdf->ln();

$pdf->SetFont($language, '', 12);
$pdf->Cell(170, 10, '厚生労働大臣　殿', 0, false, 'L');
$pdf->ln();

$pdf->SetFont($language, '', 11);
$pdf->MultiCell($width = 25, $height = 10, $text = '輸入者名：', $border = 0, $align = 'L', $fill=true, $ln = 0, $x = 120, $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = 10, $valign = 'M');
$pdf->MultiCell($width = 30, $height = 10, $text = '輸入者名', $border = 0, $align = 'L', $fill=true, $ln = 0, $x = 145, $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = 10, $valign = 'M');
$arrContextOptions=array(
    'ssl' => array(
        'verify_peer' => false,
        'verify_peer_name' => false,
    ),
);
$path = $this->Url->build('/', true) .'img/u_plus_329e.png';
$icon_u_plus_329e = file_get_contents($path, false, stream_context_create($arrContextOptions));
$pdf->Image('@' . $icon_u_plus_329e, 170, 70, 4, 4, 'PNG', '', '', false, '', '', false, false, '', false, false, false);
$pdf->ln();

$pdf->SetFont($language, '', 11);
$pdf->MultiCell($width = 170, $height = 10, $text = '１． 治療上必要な理由', $border = 0, $align = 'L', $fill=true, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = 10, $valign = 'T');
$pdf->ln();

// list product
$pdf->SetTextColor(255,0,0);
$pdf->SetFont($language, '', 11);
$pdf->MultiCell($width = 110, $height = 5, $text = 'BELOTERO BALANCE (1.0ml x 1)', $border = 0, $align = 'L', $fill=true, $ln = 0, $x = 25, $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = 5, $valign = 'M');
$pdf->MultiCell($width = 40, $height = 5, $text = '10 箱', $border = 0, $align = 'C', $fill=true, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = 5, $valign = 'M');
$pdf->ln();
$pdf->MultiCell($width = 110, $height = 5, $text = 'BELOTERO BALANCE (1.0ml x 1)', $border = 0, $align = 'L', $fill=true, $ln = 0, $x = 25, $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = 5, $valign = 'M');
$pdf->MultiCell($width = 40, $height = 5, $text = '10 箱', $border = 0, $align = 'C', $fill=true, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = 5, $valign = 'M');
$pdf->ln();
$pdf->ln();

$pdf->SetTextColor(0,0,0);
$pdf->SetFont($language, '', 11);
$text = '今回輸入する当該製品はヨーロッパでは既に医療機器として承認されており、既に米国および'. "\n";
$text .= '欧州において市販されており、ＣＥを取得済みである。欧州における臨床試験成績は数多く'. "\n";
$text .= '公表されており、これらの報告から改善効果は満足できるものであると共に安全性が高いこと'. "\n";
$text .= 'が判断できる。'. "\n";
$text .= 'また、改善効果の持続性はコラーゲン製剤よりも長く、患者への侵襲回数を減じることが可能'. "\n";
$text .= 'である。'. "\n";
$text .= '顔面の変形や窪みの修復をその用途とし当院では輸入している。'. "\n";
$text .= '臨床的安全性・有効性、または効果、患者への負担を配慮するため、とても適していると考え'. "\n";
$text .= 'られる。'. "\n";
$text .= '治療上緊急性があり、国内に代替品が流通していない。'. "\n";
$text .= 'これらの理由から輸入したい。';
$pdf->MultiCell($width = 180, $height = 20, $text = $text, $border = 0, $align = 'L', $fill=true, $ln = 0, $x = 18, $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = '', $valign = 'T');
$pdf->ln();

$pdf->SetFont($language, '', 11);
$text = '今回輸入する当該製品はヨーロッパでは既に医療機器として承認されており、既に米国および'. "\n";
$text .= '欧州において市販されており、ＣＥを取得済みである。欧州における臨床試験成績は数多く'. "\n";
$text .= '公表されており、これらの報告から改善効果は満足できるものであると共に安全性が高いこと'. "\n";
$text .= 'これらの理由から輸入したい。';
$pdf->MultiCell($width = 180, $height = '', $text = $text, $border = 0, $align = 'L', $fill=true, $ln = '', $x = 18, $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = '', $valign = 'T');
$pdf->ln();
//$pdf->ln();
$pdf->Cell(170, 6, '', 0, false, 'L');
$pdf->ln();

$text = '<div>・１名の患者に対し<span style="color: red">約１箱（１本）</span>が必要相当量となります。</div>';
$pdf->writeHTMLCell(170, '', $x = 17, $y = '', $text, 0, 0, 0, true, 'L', true);

//$pdf->MultiCell($width = '', $height = '', $text = '・１名の患者に対し', $border = 0, $align = 'L', $fill=true, $ln = 0, $x = 18, $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = '', $valign = 'M');
//$pdf->SetTextColor(255,0,0);
//$pdf->MultiCell($width = '', $height = '', $text = '約１箱（１本）', $border = 0, $align = 'L', $fill=true, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = '', $valign = 'M');
//$pdf->SetTextColor(0,0,0);
//$pdf->MultiCell($width = '', $height = '', $text = 'が必要相当量となります。', $border = 0, $align = 'L', $fill=true, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = '', $valign = 'M');
$pdf->ln();

$pdf->SetTextColor(255,0,0);
$pdf->SetFont($language, '', 11);
$pdf->MultiCell($width = 90, $height = '', $text = 'BELOTERO BALANCE (1.0ml x 1)', $border = 0, $align = 'L', $fill=true, $ln = 0, $x = 25, $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = '', $valign = 'M');
$pdf->MultiCell($width = 25, $height = '', $text = '待機患者：', $border = 0, $align = 'L', $fill=true, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = '', $valign = 'M');
$pdf->MultiCell($width = 25, $height = '', $text = '10 箱', $border = 0, $align = 'R', $fill=true, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = '', $valign = 'M');
$pdf->ln();
$pdf->MultiCell($width = 90, $height = '', $text = 'BELOTERO BALANCE (1.0ml x 1)', $border = 0, $align = 'L', $fill=true, $ln = 0, $x = 25, $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = '', $valign = 'M');
$pdf->MultiCell($width = 25, $height = '', $text = '待機患者：', $border = 0, $align = 'L', $fill=true, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = '', $valign = 'M');
$pdf->MultiCell($width = 25, $height = '', $text = '10 箱', $border = 0, $align = 'R', $fill=true, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = '', $valign = 'M');
$pdf->ln();
$pdf->ln();

$pdf->SetTextColor(0,0,0);
$pdf->SetFont($language, '', 11);
$pdf->MultiCell($width = 170, $height = 10, $text = '２．医師の責任', $border = 0, $align = 'L', $fill=true, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = 10, $valign = 'T');
$pdf->ln();

$pdf->SetTextColor(0,0,0);
$pdf->SetFont($language, '', 11);
$text = '今回輸入する当該製品はヨーロッパで市販されているものであるが、国内では薬事法上無許可'. "\n";
$text .= 'であり、本品は医師の責任の元に使用されるもので、一切の責任は医師である私が負うもので'. "\n";
$text .= 'あります。'. "\n";
$text .= '当該商品は顔面の修復治療に使用するために輸入するものであり、販売・譲渡は一切いたしま'. "\n";
$text .= 'せん。'. "\n";
$text .= 'また、注文・支払いの方法については、間違いなく私個人で行っております。';
$pdf->MultiCell($width = 180, $height = 20, $text = $text, $border = 0, $align = 'L', $fill=true, $ln = 0, $x = 18, $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = '', $valign = 'T');
$pdf->ln();

$pdf->Output();
