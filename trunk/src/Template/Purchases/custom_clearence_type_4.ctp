
<?php
ini_set('memory_limit', '-1');

// create new PDF document
// $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
$pdf = new TCPDF('', 'mm', 'A4', true, 'UTF-8');
// Reset the encoding forced from tcpdf
mb_internal_encoding('UTF-8');

$font_jp = TCPDF_FONTS::addTTFfont(WWW_ROOT.'fonts/ipag.ttf');
//$pdf->SetFont($font1, '', 14);

// language
if ($this->request->session()->read('tb_field') === 'en') {
    $language = 'Helvetica';
} else {
    $language = $font_jp;
}
// test language
$language = $font_jp;

ob_clean(); // cleaning the buffer before Output()

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Nick');
$pdf->SetTitle('Purchase Order Form');
$pdf->SetSubject('Purchase Order Form');
$pdf->SetKeywords('');

// Configure header and footer
$pdf->SetPrintHeader(false);
$pdf->SetPrintFooter(false);

// set margins
$pdf->SetMargins(13, PDF_MARGIN_TOP, 13);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
    require_once(dirname(__FILE__).'/lang/eng.php');
    $pdf->setLanguageArray($l);
}
// set default font subsetting mode
$pdf->setFontSubsetting(true);
$pdf->SetFont('Helvetica', '', 8);

// page padding
$page_padding_top = 10;
$page_padding_left = 10;

$pdf->AddPage();
// header page
$pdf->SetFont($language, '', 8);

$pdf->SetFillColor(255,255,255);
//$pdf->MultiCell($width = 80, $height = 60, $text = 'Main column', $border = 1, $align = 'L', $fill=true, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = 60, $valign = 'T');

// header title
$pdf->SetFont($language, '', 20);
$pdf->Cell(170, '', '治療同意書', 0, false, 'C');
$pdf->ln();
$pdf->ln();

$pdf->SetFont($language, '', 11);
$pdf->Cell(7, '', '１．', 0, false, 'L');

$pdf->SetTextColor(0,0,0);
$pdf->SetFont($language, '', 11);
$text = '治療に使用するXeominの成分について'. "\n";
$text .= '精製されたA型ボツリヌス毒素（タンパク質）を有効成分とする薬剤です。'. "\n";
$text .= 'A型ボツリヌス毒素は、神経伝達物質であるアセチルコリンの働きを抑制します。'. "\n";
$text .= '本剤を筋肉に注射することにより、筋肉の収縮を一時的に抑制し「しわ」や多汗を改善することがで'. "\n";
$text .= 'きます。'. "\n";
$text .= 'また、本製剤にはヒト血清アルブミンを含有しており、感染症伝播のリスクを完全に排除することは'. "\n";
$text .= 'できません。';
$pdf->MultiCell($width = 180, $height = '', $text = $text, $border = 0, $align = 'L', $fill=true, $ln = 0, $x = 20, $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = '', $valign = 'T');

$pdf->ln();
$pdf->Cell(7, 6, '', 0, false, 'L');
$pdf->ln();

$pdf->SetFont($language, '', 11);
$pdf->Cell(7, '', '２．', 0, false, 'L');
$pdf->SetTextColor(0,0,0);
$pdf->SetFont($language, '', 11);
$text = 'Xeominによる副作用について'. "\n";
$text .= 'Xeominで治療した患者様から次のような副作用が報告されています。'. "\n";
$text .= 'その主たるものは、頭痛、眼瞼下垂（上まぶたが開きにくい状態）、そうよう感（かゆみ）などです。'. "\n";
$text .= 'この薬剤を他の適応症に使用し、因果関係が否定できない死亡例の報告があります。'. "\n";
$text .= '本剤を使用し体調に変化が現れた場合は、医師にご相談ください。';
$pdf->MultiCell($width = 183, $height = '', $text = $text, $border = 0, $align = 'L', $fill=true, $ln = 0, $x = 20, $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = '', $valign = 'T');

$pdf->ln();
$pdf->Cell(7, 6, '', 0, false, 'L');
$pdf->ln();

$pdf->SetFont($language, '', 11);
$pdf->Cell(7, '', '３．', 0, false, 'L');
$pdf->SetTextColor(0,0,0);
$pdf->SetFont($language, '', 11);
$text = 'Xeominによる治療を受ける際の注意点'. "\n";
$text .= '[投与前]'. "\n";
$text .= '＊妊娠の可能性のある婦人は、本薬の投与中、および最終月経２回の月経を経るまでは避妊してくだ'. "\n";
$text .= 'さい。'. "\n";
$text .= '（妊娠中の投与に関する安全性は確立されていない）'. "\n";
$text .= '＊男性は、投与中および最終投与から少なくとも３か月は避妊をしてください。'. "\n";
$text .= '（精子形成期間に投与されることを避けるため）'. "\n";
$text .= '＊３か月間は避妊をすること。（精子生成過程での安全性が証明されていないため）'. "\n";
$text .= '[投与後]'. "\n";
$text .= '＊注射当日は強く揉んだり擦ったりしないでください。'. "\n";
$text .= '＊本剤の注射部位やその周辺が上がった下がったりした感じ、腫れ、まぶたが重くなった感じ、'. "\n";
$text .= '一時的な表情の変化、頭痛などを感じることがありますが、1週間から１カ月で焼失してゆきますの'. "\n";
$text .= '＊注射部位に内出血をおこすことがあります。'. "\n";
$text .= '＊脱力感、筋力低下、めまい、視力低下が現れることがあるので、自動車の運転等の危険を伴う機械の'. "\n";
$text .= '操作はご注意ください。';
$pdf->MultiCell($width = 183, $height = '', $text = $text, $border = 0, $align = 'L', $fill=true, $ln = 0, $x = 20, $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = '', $valign = 'T');

$pdf->ln();
$pdf->Cell(7, 12, '', 0, false, 'L');
$pdf->ln();

$pdf->SetTextColor(0,0,0);
$pdf->SetFont($language, '', 11);
$pdf->Cell(7, 7, '', 0, false, 'C');
$pdf->Cell(15, 7, '私は', 0, false, 'C');
$pdf->SetFont($language, '', 7);
$pdf->Cell(85, 7, '医療法人社団　ラナンキュラス会　麗ビューティー皮フ科クリニック', 0, false, 'L');
$pdf->SetFont($language, '', 11);
$pdf->Cell(40, 7, '河原　麗', 0, false, 'C');
$pdf->Cell(30, 7, '医師により', 0, false, 'L');
$pdf->ln();

$pdf->SetTextColor(0,0,0);
$pdf->SetFont($language, '', 12);
$text = '本剤　Xeominを使用した治療について口頭においての説明および説明文の交付を受け、';
$pdf->MultiCell($width = 183, $height = 10, $text = $text, $border = 0, $align = 'L', $fill=true, $ln = 0, $x = 20, $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = 10, $valign = 'M');
$pdf->ln();
$text = 'その内容について十分に理解しましたので、本剤　Xeominで治療を受けることに同意します。';
$pdf->MultiCell($width = 183, $height = 10, $text = $text, $border = 0, $align = 'L', $fill=true, $ln = 0, $x = 20, $y = '', $reseth = true, $stretch = false, $ishtml = false, $autopadding = false, $maxh = 10, $valign = 'M');
$pdf->ln();

$pdf->Cell(70, 7, '同意日　：', 0, false, 'R');
$pdf->Cell(35, 7, '平成', 0, false, 'C');
$pdf->Cell(20, 7, '年', 0, false, 'L');
$pdf->Cell(20, 7, '月', 0, false, 'L');
$pdf->Cell(20, 7, '日', 0, false, 'L');
$pdf->ln();
$pdf->Cell(70, 7, '氏　名　：', 0, false, 'R');

$pdf->Output();
