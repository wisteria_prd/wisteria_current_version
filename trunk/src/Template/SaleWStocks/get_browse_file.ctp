<section class="modal-wrapper">
    <div class="modal fade" id="upload-pdf" role="dialog" data-backdrop="static">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <?php
                    echo $this->Form->button('<span aria-hidden="true">&times;</span>', [
                        'type' => 'button',
                        'class' => 'close modal-close',
                        'data-dismiss' => 'modal',
                        'aria-label' => 'Close',
                        'escape' => false,
                    ]);
                    ?>
                    <h4 class="modal-title"><?= __('TXT_DOCUMENT_UPLOAD') ?></h4>
                </div>
                <div class="modal-body">
                    <?php
                    echo $this->Form->create(null, [
                        'onsubmit' => 'return false;',
                        'role' => 'form',
                        'class' => 'form-horizontal',
                        'type' => 'file',
                        'name' => 'pdf-form',
                    ]);
                    $this->Form->templates([
                        'inputContainer' => '<div class="col-sm-8">{{content}}</div>',
                    ]);
                    echo $this->Form->hidden('seller_id', ['value' => $sale->id]);
                    echo $this->Form->hidden('media_id', [
                        'value' => $media ? $media->id : null,
                    ]);
                    ?>
                    <div class="form-group">
                        <label class="col-sm-4 control-label"><?= __('TXT_TYPE') ?></label>
                        <div class="col-sm-8">
                            <?php
                            echo $this->Form->select('document_type', $typePdf, [
                                'class' => 'form-control',
                                'id' => false,
                                'value' => $media ? $media->document_type : null,
                            ]);
                            ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label"><?= __('TXT_PDF_FILE') ?></label>
                        <?php
                        echo $this->Form->input('file', [
                            'type' => 'file',
                            'class' => 'form-control',
                            'label' => false,
                            'required' => false,
                            'options' => $typePdf,
                        ]);
                        ?>
                    </div>
                    <?php echo $this->Form->end(); ?>
                </div>
                <div class="modal-footer">
                    <?php
                    echo $this->Form->button(__('BTN_CANCEL'), [
                        'type' => 'button',
                        'class' => 'btn btn-default btn-sm btn-width modal-close',
                        'data-dismiss' => 'modal',
                        'aria-label' => 'Close',
                    ]);
                    echo $this->Form->button(__('TXT_DOCUMENT_UPLOAD'), [
                        'type' => 'button',
                        'class' => 'btn btn-primary btn-sm btn-width btn-upload-file',
                    ]);
                    ?>
                </div>
            </div>
        </div>
    </div>
</section>