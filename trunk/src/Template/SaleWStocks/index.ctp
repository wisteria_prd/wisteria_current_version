<div class="search-section" style="margin-bottom: 20px;">
    <?php
    echo $this->Form->create('Sales', [
        'role' => 'form',
        'class' => 'form-inline form-search',
        'type' => 'get',
        'name' => 'form_search'
    ]);
    echo $this->Form->hidden('displays', [
        'value' => empty($this->request->query('displays')) ? 10 : $this->request->query('displays')
    ]);
    echo $this->Form->input('keyword', [
        'name' => 'keyword',
        'placeholder' => __('USER_SEARCH_KEYWORD'),
        'class' => 'form-control',
        'label' => false,
        'value' => $this->request->query('keyword'),
        'autocomplete' => 'off',
        'templates' => [
            'inputContainer' => '<div class"form-group">{{content}}</div>',
        ]
    ]);
    echo $this->Form->submit('submit', [
        'style' => 'display: none',
    ]);
    echo $this->Form->end();
    ?>
</div>
<div class="total-pagination" style="padding-bottom: 20px;">
    <?php echo $this->element('display_info'); ?>
</div>
<div class="row search-section" style="margin-bottom: 20px;">
    <?php echo $this->element('display_number'); ?>
    <div class="col-sm-6 pull-right" style="text-align: right;">
        <?= $this->Form->button(__('BTN_REGISTER'), [
            'type' => 'button',
            'class' => 'btn btn-sm btn-primary btn-width btn-register-po',
            'data-target' => TARGET_NEW,
        ]) ?>
    </div>
    <div class="clearfix"></div>
</div>
<div class="data-tb-list">
    <?php if ($sales): ?>
    <table class="table table-striped table-pos">
        <thead>
            <tr>
                <th>#</th>
                <th>&nbsp;</th>
                <th class="white-space">
                    <?php echo $this->Paginator->sort('status', __('TXT_SALE_STATUS')); ?>
                    <i class="fa fa-sort-desc" aria-hidden="true"></i>
                </th>
                <th class="white-space">
                    <?php echo $this->Paginator->sort('order_date', __('TXT_SALE_ORDER_DATE')); ?>
                    <i class="fa fa-sort-desc" aria-hidden="true"></i>
                </th>
                <th class="white-space">
                    <?php echo $this->Paginator->sort('rop_number', __('TXT_ROP')); ?>
                    <i class="fa fa-sort-desc" aria-hidden="true"></i>
                </th>
                <th class="white-space">
                    <?php echo $this->Paginator->sort('sale_number', __('TXT_PO')); ?>
                    <i class="fa fa-sort-desc" aria-hidden="true"></i>
                </th>
                <th class="jp-max-character">
                    <?php echo $this->Paginator->sort('Customers.name' . $local, __('TXT_CLINIC_NAME')); ?>
                    <i class="fa fa-sort-desc" aria-hidden="true"></i>
                </th>
                <th class="white-space">
                    <?php echo $this->Paginator->sort('Sellers.id', __('TXT_SALE_SUPPLIER_NAME')); ?>
                    <i class="fa fa-sort-desc" aria-hidden="true"></i>
                </th>
                <th class="white-space"><?= __('TXT_AMOUNT') ?></th>
                <th colspan="4">&nbsp;</th>
            </tr>
        </thead>
        <tbody>
            <?php
            $numbering = $this->Paginator->counter('{{start}}');
            foreach ($sales as $key => $value) :
            ?>
            <tr data-id="<?= $value->id ?>" data-target="<?= TARGET_EDIT ?>" class="btn-register-po">
                <th><?php echo $numbering; $numbering++; ?></th>
                <td><?= $this->ActionButtons->disabledText($value->is_suspend) ?></td>
                <td><?= __($this->Comment->textHumanize($value->status)) ?></td>
                <td>
                    <?php
                    if ($value->order_date) {
                        echo date('m/d/Y', strtotime($value->order_date));
                    }
                    ?>
                </td>
                <td><?= $value->rop_number ?></td>
                <td><?= $value->sale_number; ?></td>
                <td><?= $this->Comment->getFieldByLocal($value->customer, $local); ?></td>
                <td>
                    <?php
                    if ($value->seller->manufacturer) {
                        echo $this->Comment->nameEnOrJp($value->seller->manufacturer->nickname, $value->seller->manufacturer->nickname_en, $local);
                    } else {
                        echo $this->Comment->nameEnOrJp($value->seller->supplier->nickname, $value->seller->supplier->nickname_en, $local);
                    }
                    ?>
                </td>
                <td style="white-space: nowrap;">
                    <?php
                    $total = 0;
                    if ($value->sale_details) {
                        $total = $value->sale_details[0]->total_amount;
                    }
                    echo $this->Comment->currencyEnJpFormat($local, $total, $value->currency);
                    ?>
                </td>
                <td width="1%">
                    <?php
                    echo $this->Html->link(__('TXT_BUTTON_SALE_W_STOCK_ROP'), [
                        'action' => 'detail', $value->id
                    ], [
                        'class' => 'btn btn-sm btn-primary',
                        'style' => 'width: 100%;',
                    ]);
                    if ($value->status === PO_STATUS_DRAFF && !empty($value->sale_details)) {
                        echo $this->Form->button(__('TXT_BUTTON_FAX_ROP'), [
                            'type' => 'button',
                            'class' => 'btn btn-sm btn-primary btn-fax-rop',
                            'style' => 'width: 100%; margin-top: 5px;',
                        ]);
                    }
                    ?>
                </td>
                <td width="1%">
                    <?= $this->Html->link(__('TXT_OF_EDIT'), [
                        'action' => 'po', $value->id
                    ], [
                        'class' => 'btn btn-sm btn-primary',
                        'style' => 'margin-bottom: 5px;'
                    ]);
                    ?>
                    <?= $this->Html->link(__('TXT_OF_PDF'), [
                        'controller' => 'sale-reports',
                        'action' => 'w-of-jp-pdf', $value->id,
                    ], [
                        'class' => 'btn btn-sm btn-primary',
                        'target' => '_blank',
                    ]);
                    ?>
                </td>
                <!--<td width="1%">-->
                    <?php
//                    echo$this->Html->link(__('TXT_CC_EDIT'), [
//                        'controller' => 'sales',
//                        'action' => 'edit-doc', $value->id, '?' => ['customer_id' => $value->customer->id]
//                    ], [
//                        'class' => 'btn btn-primary btn-sm btn-edit-doc',
//                        'style' => 'margin-bottom: 5px;'
//                    ]);
//                    echo $this->Html->link(__('TXT_CC_PDF'), [
//                        'controller' => 'sale-reports',
//                        'action' => 'custom-clearence-pdf', $value->id,
//                    ], [
//                        'class' => 'btn btn-primary btn-sm',
//                        'target' => '_blank',
//                    ]);
                    ?>
                <!--</td>-->
                <td>
                    <?php echo $this->Form->button('<i class="fa fa-comments-o" aria-hidden="true"></i>', [
                        'class' => 'btn btn-primary btn-sm btn_comment',
                        'data-id' => $value->id,
                        'escape' => false,
                    ]); ?>
                </td>
                <td data-target="<?= $value->is_suspend ? 1 : 0; ?>">
                    <?php
                    if ($value->is_suspend == 1) {
                        echo $this->Form->button(BTN_ICON_UNDO, [
                            'class' => 'btn btn-recover btn-sm btn_suspend',
                            'data-name' => 'recover',
                            'escape' => false,
                            'style' => 'margin-bottom: 5px;'
                        ]);
                    }

                    if ($value->is_suspend == 1) {
                        echo $this->Form->button(BTN_ICON_DELETE, [
                            'class' => 'btn btn-delete btn-sm',
                            'id' => 'btn_delete',
                            'data-name' => 'delete',
                            'escape' => false
                        ]);
                    } else {
                        echo $this->Form->button(BTN_ICON_STOP, [
                            'class' => 'btn btn-suspend btn-sm btn_suspend',
                            'data-name' => 'suspend',
                            'escape' => false
                        ]);
                    }
                    ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <?php endif; ?>
</div>
<div class="row row-top-space" style="padding-bottom: 20px;">
    <?php echo $this->element('display_number'); ?>
    <?= $this->element('next_prev') ?>
    <div class="clearfix"></div>
</div>

<!--Modal Detail-->
<?= $this->element('Modal/view_detail'); ?>
<!--Modal Comment-->
<?= $this->element('/Modal/message') ?>
<!--Modal Suspend-->
<?= $this->element('Modal/suspend'); ?>
<!--Modal delete-->
<?= $this->element('Modal/delete'); ?>
<!--Modal edit pdf document-->
<?= $this->element('Modal/sale_edit_doc'); ?>

<?php
echo $this->Html->css([
    'bootstrap-datetimepicker.min',
]);
?>
<script>
    $(function(e) {
        //auto complete
        $('body').find('#keyword').typeahead({
            minLength: 0,
            autoSelect: true,
            source:  function (query, process) {
                return $.get('<?= $this->Url->build(['action' => 'getAllSaleBySearch']) ?>', {
                        keyword: query
                    }, function (data) {
                        var optionData = [];
                        if (data.data !== null && data.data !== 'undefined') {
                            $.each(data.data, function(i, v) {
                                optionData.push(v.rop_number);
                                optionData.push(v.sale_number);
                            });
                        }
                        return process(optionData);
                    }, 'json');
            }
        });

        $('body').on('click', '.typeahead li', function(e) {
            $('body').find('.form-search').submit();
        });

        $('body').on('click', '.btn-fax-rop', function(e) {
            e.stopPropagation();
            var options = {
                url: '<?= $this->Url->build(['action' => 'getFaxRop']) ?>',
                type: 'GET',
                dataType: 'html',
                data: { id: $(this).closest('tr').attr('data-id') },
                beforeSend: function() {
                    $.LoadingOverlay('show');
                }
            };
            ajaxRequest(options, function(data) {
                $('body').append(data);
                $('body').find('.fax-rop').modal('show');
            });
        });

        $('.btn_comment').click(function (e) {
            e.stopPropagation();
            let params = {
                id: $(this).closest('tr').attr('data-id'),
                type: '<?php echo TRADE_TYPE_SALE_W_STOCK ?>',
                referer: '<?php echo $this->request->controller; ?>'
            };
            let options = {
                url: BASE + 'Messages/getMessageList',
                type: 'GET',
                data: params,
                beforeSend: function () {
                    $.LoadingOverlay('show');
                }
            };
            ajaxRequest(options, SESSION_TIMEOUT, function (data) {
                $('body').prepend(data);
                $('body').find('.modal-message').modal('show');
            });
        });

        //filter message list search to specific
        $('body').on('click', '.btn-to-individual', function() {
            $(this).removeClass('btn-detault').addClass('btn-primary');
            $('body').find('.btn-to-all').removeClass('btn-primary').addClass('btn-default');
            var toAll = $('.to-individual').val();
            if (parseInt(toAll) === 0) {
                $('.to-all').val(0);
                $('.to-individual').val(1);
            } else {
                $('.to-all').val(1);
                $('.to-individual').val(0);
            }
            messagesSearch(listUrlSearch);
        });

        $('body').on('click', '.btn-search', function() {
            messagesSearch(listUrlSearch);
        });

        $('body').on('click', '.btn-suspend-msg', function() {
            var id = $(this).data('id');
            var data = $('#form-message-status-' + id).serialize();
            var suspendUrl = '<?= $this->Url->build(['controller' => 'Messages', 'action' => 'suspend']) ?>';
            suspendMessage(id, suspendUrl, data);
        });

        $('body').on('click', '#trigger-calendar', function () {
            $('#datepicker').focus();
        });

        $('body').on('click', '#trigger-order-date', function () {
            $('#order-date').focus();
        });

        $('body').on('click', '.table>tbody>tr>td a', function(e) {
            e.stopPropagation();
        });

        $('body').on('click', '.btn-register-po', function() {
            var params = {
                target: $(this).attr('data-target'),
                id: null
            };
            if (params.target === '<?= TARGET_EDIT ?>') {
                params.id = $(this).attr('data-id');
            }
            var options = {
                url: '<?= $this->Url->build('/sale-w-stocks/get-data') ?>',
                type: 'GET',
                dataType: 'html',
                data: params,
                beforeSend: function() {
                    $.LoadingOverlay('show');
                }
            };
            ajaxRequest(options, function(data) {
                $('body').prepend(data);
                var content =  $('body').find('#modal-po-ms-to-s');
                $(content).modal('show');
                $(content).find('.btn-register').text('<?= __('BTN_REGISTER') ?>');
                $(content).find('#seller-id, #currency-id').select2({
                    width: '100%'
                });
            });
        });

        $('body').on('click', '.btn_suspend', function (e) {
            e.stopPropagation();
            var content = $('#modal_suspend');
            var status = $(this).attr('data-name');
            var txt = '';

            switch (status) {
                case 'suspend':
                    $('#btn_suspend_yes').text('<?= __('BTN_SUSPEND') ?>');
                    txt = '<?= __('MSG_CONFIRM_SUSPEND') ?>';
                    break;
                case 'delete':
                    txt = '<?= __('MSG_CONFIRM_DELETE') ?>';
                    $('#btn_suspend_yes').text('<?= __('BTN_DELETE') ?>');
                    break;
                 case 'recover':
                    $('#btn_suspend_yes').text('<?= __('BTN_RECOVER') ?>');
                    txt = '<?= __('MSG_CONFIRM_UNDO_SUSPEND') ?>';
                    break;
            };

            $(content).find('#btn_suspend_yes').data('id', $(this).closest('tr').data('id'));
            $(content).find('#btn_suspend_yes').data('target', $(this).closest('td').data('target'));
            $(content).find('.modal-body p').html(txt);
            $(content).modal('show');
        });

        $('body').on('click', '#btn_suspend_yes', function (e) {
            var params = {
                id: $(this).data('id'),
                is_suspend: $(this).data('target')
            };
            var url = '<?= $this->Url->build('/sales/update-suspend/'); ?>';

            ajax_request_post(url, params, function(data) {
                if (data.message === 'success') {
                    location.reload();
                }
            }, '<?= __('TXT_SESSION_TIMEOUT') ?>');
        });

        $('body').on('click', '#btn_delete', function (e) {
            e.stopPropagation();
            var content = $('#modal_delete');

            $(content).find('#btn_delete_yes').attr('data-id', $(this).closest('tr').data('id'));
            $(content).modal('show');
        });

        $('body').on('click', '#btn_delete_yes', function (e) {
            var url = '<?= $this->Url->build('/sales/delete/'); ?>';

            ajax_request_post(url, {id: $(this).data('id')}, function(data) {
                if (data.message === 'success') {
                    location.reload();
                }
            }, '<?= __('TXT_SESSION_TIMEOUT') ?>');
        });

        $('body').on('change', '.select-person-in-charge', function () {
            if ($(this).val() !== '') {
                var incharge = $(this).find('option:selected').text();
                $('.in-charge-name').val(incharge);
            }
        });

        $('.btn-edit-doc').on('click', function(e) {
            e.preventDefault();
            $.LoadingOverlay('show');
            var that = this;
            ajax_request_get($(that).attr('href'), {}, function(data) {
                $('#edit-docs').find('.modal-body').append(data);
                $('#edit-docs').modal('show');
            });
        });

        $('#edit-docs').on('hidden.bs.modal', function () {
            var that = this;
            $(that).find('.modal-body').empty();
        });
        var maxPage = parseInt($('#maxPage').val());
        updatePagination(maxPage);

        function ajaxRequest(params, callback)
        {
            $.ajax(params).done(function(data) {
                if (data === null && data === 'undefined') {
                    return false;
                }
                if (typeof callback === 'function') {
                    callback(data);
                }
            }).fail(function(jqXHR, textStatus, errorThrown) {
                if (errorThrown === 'Forbidden') {
                    if (confirm('<?= __('TXT_SESSION_TIMEOUT') ?>')) {
                        location.reload();
                    }
                }
            }).always(function(data) {
                $.LoadingOverlay('hide');
            });
        }
    });
</script>
