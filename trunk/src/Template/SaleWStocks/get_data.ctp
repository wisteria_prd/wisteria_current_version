
<section class="modal-wrapper">
    <div class="modal fade" id="modal-po-ms-to-s" data-backdrop="static" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <?php
                    echo $this->Form->button('<span aria-hidden="true">&times;</span>', [
                        'type' => 'button',
                        'class' => 'close modal-close',
                        'data-dismiss' => 'modal',
                        'aria-label' => 'Close',
                        'escape' => false,
                    ]);
                    ?>
                    <h4 class="modal-title text-center"><?= __('TXT_CREATE') ?></h4>
                </div>
                <div class="modal-body">
                    <?php
                    echo $this->Form->create($data ? $data : null, [
                        'class' => 'form-add-po form-horizontal',
                        'autocomplete' => 'off',
                        'name' => 'sale_w_stock',
                        'onsubmit' => 'return false;',
                    ]);
                    $this->Form->templates([
                        'inputContainer' => '{{content}}'
                    ]);
                    echo $this->Form->hidden('target', ['value' => $target]);
                    if ($target === TARGET_EDIT) {
                        echo $this->Form->hidden('id', ['value' => $data->id]);
                    }
                    ?>
                    <div class="form-group">
                        <label for="" class="control-label col-md-3"><?= __('TXT_SELLER') ?></label>
                        <div class="col-md-7 custom-select">
                            <?php
                            if (($this->request->controller === 'SaleWStocks') || ($this->request->controller === 'Sales')) {
                                $options = [];
                            } else {
                                $options = [
                                    ['value' => __('TXT_SELLER')],
                                ];
                            }
                            if ($sellers) {
                                foreach ($sellers as $key => $seller) {
                                    if ($seller->type === TYPE_MANUFACTURER) {
                                        $text = $seller->manufacturer->name;
                                    } else {
                                        $text = $seller->supplier->name;
                                    }
                                    $options[] = [
                                        'text' => $text,
                                        'value' => $seller->id,
                                    ];
                                }
                            }
                            echo $this->Form->select('seller_id', $options, [
                                'class' => 'form-control',
                                'default' => $seller ? $seller->id : null,
                                'empty' => false,
                            ]);
                            ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="control-label col-md-3"><?= __('TXT_CURRENCY') ?></label>
                        <div class="col-md-7 custom-select">
                            <?= $this->Form->select('currency_id', $currencies, [
                                'class' => 'form-control',
                                'default' => $data ? $data->currency_id : null,
                            ]) ?>
                        </div>
                    </div>
                    <div class="form-group custom-select">
                        <label for="" class="control-label col-md-3"><?= __('TXT_TO_CUSTOMER') ?></label>
                        <div class="col-md-7">
                            <?php
                            $options = [];
                            if ($data) {
                                $options[0] = [
                                    'value' => $data->customer_id,
                                    'text' => $data->customer->name . ' ' . $data->customer->name_en,
                                ];
                            }
                            echo $this->Form->select('customer_id', $options, [
                                'class' => 'form-control customer-id',
                                'default' => $data ? $data->customer_id : '',
                            ]);
                            ?>
                        </div>
                    </div>
                    <div class="customer-sub-wrap">
                        <div class="form-group custom-select">
                            <label class="control-label col-md-3"><?= __('STR_SUBSIDIARY_TYPE') ?></label>
                            <div class="col-md-7">
                                <?php
                                $subOptions = [];
                                if ($subsidiary) {
                                    $name = $subsidiary->name . $local;
                                    if (($name === '') || ($name === null)) {
                                        switch ($local) {
                                            case LOCAL_EN:
                                                $name = $subsidiary->name_en;
                                                break;
                                            default:
                                                $name = $subsidiary->name;
                                                break;
                                        }
                                    }
                                    $subOptions[0] = [
                                        'value' => $subsidiary->id,
                                        'text' => $name,
                                    ];
                                }
                                echo $this->Form->select('subsidiary_id', $subOptions, [
                                    'class' => 'form-control',
                                    'id' => 'subsidiary-id',
                                    'default' => $subOptions ? $subsidiary->id : '',
                                ]);
                                ?>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="control-label col-md-3"><?= __('TXT_SALE_ORDER_DATE') ?></label>
                        <div class="col-md-7">
                            <div class="input-group">
                                <?= $this->Form->text('order_date', [
                                    'id' => 'order-date',
                                    'class' => 'form-control order-date',
                                    'value' => $data ? date('Y-m-d', strtotime($data->order_date)) : date('Y-m-d'),
                                ]) ?>
                                <div class="input-group-btn">
                                    <button type="button" class="btn btn-default" aria-label="Calendar" id="trigger-order-date">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="control-label col-md-3"><?= __('TXT_ROP_ISSUE_DATE') ?></label>
                        <div class="col-md-7">
                            <div class="input-group">
                                <?= $this->Form->text('rop_issue_date', [
                                    'class' => 'form-control',
                                    'id' => 'datepicker',
                                    'value' => $data ? date('Y-m-d', strtotime($data->rop_issue_date)) : date('Y-m-d'),
                                ]) ?>
                                <div class="input-group-btn">
                                    <button type="button" class="btn btn-default" aria-label="Calendar" id="trigger-calendar">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="control-label col-md-3"><?= __('TXT_PREFER_SHIPMENT') ?></label>
                        <div class="col-sm-7">
                          <div class="checkbox">
                            <label>
                                <?= $this->Form->checkbox('is_priority', ['hiddenField' => false]) ?>
                            </label>
                          </div>
                        </div>
                      </div>
                    <?php echo $this->Form->end(); ?>
                </div>
                <div class="modal-footer">
                    <?php
                    echo $this->Form->button(__('TXT_CANCEL'), [
                        'type' => 'button',
                        'class' => 'btn btn-default btn-sm btn-width modal-close',
                        'data-dismiss' => 'modal',
                        'aria-label' => 'Close',
                    ]);
                    echo $this->Form->button(__('TXT_REGISTER'), [
                        'type' => 'button',
                        'class' => 'btn btn-primary btn-sm btn-width btn-register',
                    ]);
                    ?>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(function(e) {
            $('body').find('#currency-id').val(1).change();

            $('#datepicker, #order-date').datetimepicker({
                format: 'YYYY-MM-DD'
            });

            $('body').find('.customer-id').select2({
                width: '100%',
                minLength: 0,
                cache: false,
                ajax: {
                    url: '<?= $this->Url->build(['action' => 'getCustomerList']) ?>',
                    dataType: 'json',
                    delay: 250,
                    cache: false,
                    data: function (params) {
                        var query = {
                            keyword: params.term,
                            type: 'public'
                        };
                        return query;
                    },
                    processResults: function (data) {
                        var dataSource = [];
                        if (data.data === null && data.data === 'undefined') {
                            return false;
                        }
                        $.each(data.data, function(i, v) {
                            dataSource.push({
                                id: v.id,
                                text: v.name + ' ' + v.name_en
                            });
                        });
                        return { results: dataSource };
                    }
                }
            });

            $('body').find('#subsidiary-id').select2({
                width: '100%',
                minLength: 0,
                cache: false,
                ajax: {
                    url: '<?= $this->Url->build('/customer-subsidiaries/get-autocomplete-by-customer-id') ?>',
                    dataType: 'json',
                    delay: 250,
                    cache: false,
                    data: function (params) {
                        var query = {
                            customer_id: $('body').find('.customer-id').val(),
                            keyword: params.term
                        };
                        return query;
                    },
                    processResults: function (data) {
                        var dataSource = [];
                        if (data.data === null && data.data === 'undefined') {
                            return false;
                        }
                        $.each(data.data.data, function(i, v) {
                            var name = v.subsidiary.name + data.data.local;
                            if ((name === '') || (name === null)) {
                                switch(data.data.local) {
                                    case '_en':
                                        name = v.subsidiary.name_en;
                                        break;
                                    default:
                                        name = v.subsidiary.name;
                                        break;
                                }
                            }
                            dataSource.push({
                                id: v.subsidiary_id,
                                text: name
                            });
                        });
                        return { results: dataSource };
                    }
                }
            });

            $('body').on('change', '.customer-id', function(e) {
                $('body').find('#subsidiary-id').empty();
            });

            $('.btn-register').click(function(e) {
                e.preventDefault();
                var form = $('body').find('form[name="sale_w_stock"]');
                var params = {
                    target: '<?= $target ?>',
                    url: '<?= $this->Url->build('/sale-w-stocks/create-and-update/') ?>'
                };
                var options = {
                    type: 'POST',
                    dataType: 'json',
                    url: params.url,
                    data: $(form).serialize(),
                    cache: false,
                    beforeSend: function() {
                        $.LoadingOverlay('show');
                    }
                };
                ajaxRequest(options, function(data) {
                    var response = data.data;
                    $(form).find('.error-message').remove();
                    if (response === null && response === 'undefined') {
                        return false;
                    }
                    if (data.message === '<?= MSG_ERROR ?>') {
                        $.each(response, function(i, v) {
                            var message = '<label class="error-message">' + v + '</label>';
                            var content = $(form).find('[name="' + i + '"]');
                            $(content).closest('.col-md-7').append(message);
                        });
                    } else {
                        if (params.target === '<?= TARGET_EDIT ?>') {
                            location.reload();
                        } else {
                            location.href = '<?= $this->Url->build(['action' => 'detail']); ?>' + '/' + response.id;
                        }
                    }
                });
            });

            function ajaxRequest(params, callback)
            {
                $.ajax(params).done(function(data) {
                    if (data === null && data === 'undefined') {
                        return false;
                    }
                    if (typeof callback === 'function') {
                        callback(data);
                    }
                }).fail(function(jqXHR, textStatus, errorThrown) {
                    if (errorThrown === 'Forbidden') {
                        if (confirm('<?= __('TXT_SESSION_TIMEOUT') ?>')) {
                            location.reload();
                        }
                    }
                }).always(function(data) {
                    $.LoadingOverlay('hide');
                });
            }
        });
    </script>
</section>
