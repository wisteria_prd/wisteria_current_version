
<section class="modal-wrapper">
    <div class="modal fade" id="delivery-detail" role="dialog" data-backdrop="static">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <?php
                    echo $this->Form->button('<span aria-hidden="true">&times;</span>', [
                        'type' => 'button',
                        'class' => 'close modal-close',
                        'data-dismiss' => 'modal',
                        'aria-label' => 'Close',
                        'escape' => false,
                    ]);
                    ?>
                    <h4 class="modal-title"><?= __('TXT_PO_ADD_ITEM') ?></h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <?php
                            echo $this->Form->create(null, [
                                'class' => 'form-po-settings form-horizontal',
                                'autocomplete' => 'off',
                                'onsubmit' => 'return false;',
                            ]);
                            echo $this->Form->hidden('delivery_id', [
                                'value' => $delivery ? $delivery->id : null,
                                'id' => 'delivery-id',
                            ]);
                            $this->Form->templates([
                                'inputContainer' => '{{content}}'
                            ]);
                            $options = [];
                            if ($stocks) {
                                foreach ($stocks as $key => $value) {
                                    $expire = date('Y-m-d', strtotime($value->import_detail->expiry_date));
                                    $options[] = [
                                        'value' => $value->id,
                                        'text' => $value->lot_number . ', Exp: ' . $expire,
                                        'data-lot' => $value->lot_number,
                                        'data-expire' => $expire,
                                    ];
                                }
                            }
                            ?>
                            <div class="form-group">
                                <label for="" class="col-md-4"><?= __('TXT_PRODUCT') ?></label>
                                <div class="col-md-8">
                                    <?php
                                    $product = '';
                                    if (!empty($saleDetail->brand_name)) {
                                        $product = $saleDetail->brand_name . ' ';
                                    }
                                    if (!empty($saleDetail->product_name)) {
                                        $product .= $saleDetail->product_name . ' ';
                                    }
                                    if (!empty($saleDetail->single_unit_value) && !empty($saleDetail->pack_size_value)) {
                                        $sgun = $saleDetail->single_unit_name;
                                        $psn = $saleDetail->pack_size_name;
                                        if ($saleDetail->pack_size_value > 1) {
                                            $psn = $this->Pluralize->pluralize($psn);
                                        }
                                        $product .= '(' .
                                            $saleDetail->pack_size_value .
                                            $sgun . ' x ' .
                                            $saleDetail->pack_size_value .
                                            $psn . ') ';
                                    } elseif (empty($saleDetail->single_unit_value) && !empty($saleDetail->pack_size_value)) {
                                        $psn = $saleDetail->pack_size_name;
                                        if ($saleDetail->pack_size > 1) {
                                            $psn = $this->Pluralize->pluralize($psn);
                                        }
                                        $product .= '(' .
                                            $saleDetail->pack_size_value .
                                            $psn . ') ';
                                    } elseif (empty($saleDetail->pack_size_value) && !empty($saleDetail->single_unit_value)) {
                                        $sgun = $saleDetail->single_unit_name;
                                        if ($saleDetail->single_unit_size > 1) {
                                            $sgun = $this->Pluralize->pluralize($sgun);
                                        }
                                        $product .= '(' .
                                            $saleDetail->single_unit_value .
                                            $sgun . ') ';
                                    }
                                    if (!empty($saleDetail->description)) {
                                        $product .= '(' . $saleDetail->description . ')';
                                    }
                                    echo $product;
                                    ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="" class="col-md-4"><?= __('TXT_QUANTITY') ?></label>
                                <div class="col-md-5"><?php echo $saleDetail->quantity; ?></div>
                            </div>
                            <div class="form-group custom-select">
                                <label for="" class="col-md-4"><?= __('TXT_LOT_EXPIRED') ?></label>
                                <div class="col-md-3 custom-select no-padding-r">
                                    <?php
                                    $value1 = null;
                                    $id1 = null;
                                    if ($deliveryDetails->toArray()) {
                                        $value1 = $deliveryDetails->toArray()[0]->stock_id;
                                        $id1 = $deliveryDetails->toArray()[0]->id;
                                    }
                                    echo $this->Form->select('stock_id', $options, [
                                        'name' => 'stock_id',
                                        'class' => 'form-control',
                                        'id' => 'stock-id-0',
                                        'value' => $value1,
                                    ]);
                                    echo $this->Form->hidden('id', [
                                        'value' => $id1,
                                    ]);
                                    ?>
                                </div>
                                <div class="col-md-3 no-padding-r">
                                    <?php
                                    $value2 = null;
                                    if ($deliveryDetails->toArray()) {
                                        $value2 = $deliveryDetails->toArray()[0]->quantity;
                                    }
                                    echo $this->Form->input('quantity', [
                                        'type' => 'number',
                                        'name' => 'quantity[0]',
                                        'label' => false,
                                        'class' => 'form-control quantity',
                                        'placeholder' => __('TXT_ENTER_QUANTITY'),
                                        'value' => $value2,
                                        'id' => false,
                                    ]);
                                    ?>
                                </div>
                                <div class="col-md-2">
                                    <?= $this->Form->button('<i class="fa fa-plus" aria-hidden="true"></i>', [
                                        'type' => 'button',
                                        'class' => 'btn btn-primary btn-stock-add',
                                        'escape' => false,
                                    ]); ?>
                                </div>
                            </div>
                            <div class="main-wrap">
                                <?php if ($deliveryDetails) :
                                    foreach ($deliveryDetails as $key => $value) : ?>
                                <?php if ($key != 0) : ?>
                                <div class="form-group custom-select">
                                    <label for="" class="col-md-4"></label>
                                    <div class="col-md-3 custom-select no-padding-r">
                                        <?php
                                        echo $this->Form->select('stock_id', $options, [
                                            'name' => 'stock_id',
                                            'class' => 'form-control',
                                            'id' => 'stock-id-' . ($key+1),
                                            'value' => $value->stock_id,
                                        ]);
                                        echo $this->Form->hidden('id', [
                                            'value' => $value->id,
                                        ]);
                                        ?>
                                    </div>
                                    <div class="col-md-3 no-padding-r">
                                        <?php
                                        echo $this->Form->input('quantity', [
                                            'type' => 'number',
                                            'name' => 'quantity['.$key.']',
                                            'label' => false,
                                            'class' => 'form-control quantity',
                                            'placeholder' => __('TXT_ENTER_QUANTITY'),
                                            'value' => $value->quantity,
                                            'id' => false,
                                        ]);
                                        ?>
                                    </div>
                                    <div class="col-md-2">
                                        <?= $this->Form->button('<i class="fa fa-trash" aria-hidden="true"></i>', [
                                            'type' => 'button',
                                            'class' => 'btn btn-delete btn-stock-rm',
                                            'escape' => false,
                                        ]); ?>
                                    </div>
                                </div>
                                <?php endif; endforeach; endif; ?>
                            </div>
                            <?= $this->Form->end() ?>
                        </div>
                        <p class="error-message text-center"></p>
                    </div>
                </div>
                <div class="modal-footer">
                    <?php
                    echo $this->Form->button(__('BTN_CANCEL'), [
                        'type' => 'button',
                        'class' => 'btn btn-default btn-sm btn-width modal-close',
                        'data-dismiss' => 'modal',
                        'aria-label' => 'Close',
                    ]);
                    echo $this->Form->button(__('TXT_REGISTER'), [
                        'type' => 'button',
                        'class' => 'btn btn-primary btn-sm btn-width btn-save',
                    ]);
                    ?>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(function(e) {
            var ids = [];
            var content = $('#delivery-detail');
            $(content).find('select[name="stock_id"]').select2({ width: '100%' });

            $(content).find('.btn-stock-add').click(function(e) {
                var main = $(content).find('.main-wrap');
                var index = $(main).find('.form-group').length + 1;
                var stockOptions = $(content).find('#stock-id-0').html();
                var element = '<div class="form-group">'
                            + '<input type="hidden" name="id"/><label for="" class="col-md-4"></label>'
                            + '<div class="col-md-3 custom-select no-padding-r">'
                            + '<select class="form-control" id="stock-id-' + index + '" name="stock_id">' + stockOptions + '</select></div>'
                            + '<div class="col-md-3 no-padding-r"><input type="number" class="form-control" name="quantity[' + index + ']" placeholder="<?= __('TXT_ENTER_QUANTITY') ?>"/></div>'
                            + '<div class="col-md-2"><button class="btn btn-delete btn-stock-rm"><i class="fa fa-trash" aria-hidden="true"></i></button></div>'
                            + '</div>';
                $(main).append(element);
                $(main).find('#stock-id-' + index).select2({ width: '100%' });
            });

            $(content).on('click', '.btn-stock-rm', function(e) {
                var id = $(this).closest('.form-group').find('input[type="hidden"]').val();
                ids.push(id);
                $(this).closest('.form-group').remove();
            });

            $(content).find('.btn-save').click(function(e) {
                var params = [];
                var selected = $(content).find('select[name="stock_id"]');
                $.each(selected, function(i, v) {
                    params.push({
                        'stock_id': $(this).find('option:selected').val(),
                        'quantity': $(this).closest('.form-group').find('input[type="number"]').val(),
                        'delivery_id': $(content).find('#delivery-id').val(),
                        'lot': $(this).find('option').attr('data-lot'),
                        'expiry_date': $(this).find('option').attr('data-expire'),
                        'product_detail_id': '<?= $saleDetail->product_detail_id ?>',
                        'id': $(this).closest('.form-group').find('input[type="hidden"]').val()
                    });
                });
                var options = {
                    type: 'POST',
                    url: '<?= $this->Url->build('/delivery-details/create/') ?>',
                    dataType: 'json',
                    data: {
                        data: JSON.stringify(params),
                        quantity: '<?= $saleDetail->quantity ?>',
                        delete: ids,
                    },
                    beforeSend: function() {
                        $.LoadingOverlay('show');
                    }
                };
                ajaxRequest(options, function(data) {
                    if (data.message === '<?= MSG_SUCCESS ?>') {
                        location.reload();
                    } else {
                        $(content).find('.error-message').text(data.data);
                    }
                });
            });

            function ajaxRequest(params, callback)
            {
                $.ajax(params).done(function(data) {
                    if (data === null && data === 'undefined') {
                        return false;
                    }
                    if (typeof callback === 'function') {
                        callback(data);
                    }
                }).fail(function(jqXHR, textStatus, errorThrown) {
                    if (errorThrown === 'Forbidden') {
                        if (confirm('<?= __('TXT_SESSION_TIMEOUT') ?>')) {
                            location.reload();
                        }
                    }
                }).always(function(data) {
                    $.LoadingOverlay('hide');
                });
            }
        });
    </script>
</section>
