<?php
namespace App\View\Helper;

use Cake\View\Helper;
use Cake\Routing\Router;

class SwitchStatusHelper extends Helper
{
    public $helpers = ['Html', 'Form'];

    public function render()
    {
        $script = "<script>
            $(function() {
                $('body').on('click', 'input:checkbox', function (e) {
                    $(this).parents('form').submit();
                });
            })
        </script>";
        $status = $this->request->query('inactive');
        $checkBox = $this->Form->checkbox('inactive', [
            'hiddenField' => false,
            'id' => 'disable-status',
            'checked' => $status ? $status : '',
            'style' => 'margin-left: 10px;',
        ]);
        $labelCheckBox = $this->Html->tag('label', __('USER_DISPLAY_SUSPEND_RECORD') , array(
          'for' => 'disable-status',
        ));
        $divCheckBox = $this->Html->tag('div', $checkBox.$labelCheckBox, array(
            'class' => 'checkbox'
        ));
        $result = $this->Html->tag('div', $divCheckBox, array(
            'class' => 'form-group',
        ));
        echo $script;
        echo $result;
    }

}
