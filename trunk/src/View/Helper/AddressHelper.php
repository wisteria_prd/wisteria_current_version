<?php
namespace App\View\Helper;

use Cake\View\Helper;

class AddressHelper extends Helper
{
    public $helpers = ['Html'];

    public function format($data = null, $locale =  null)
    {
        if ($data === null) {
            return false;
        }
        if ($locale === LOCAL_EN) {
            return $this->address_en($data);
        } else {
            return $this->address_jp($data);
        }
    }

    private function address_jp($data = null)
    {
        // address format: country + postal_code + prefecture + city + street + building
        $address = '';
        if (!empty($data->country)) {
            $address = $data->country->name;
        }
        if (!empty($data->postal_code)) {
            $address .= '  〒' . $data->postal_code;
        }
        if (!empty($data->prefecture)) {
            $address .= ' ' . $data->prefecture;
        }
        if (!empty($data->city)) {
            $address .= '〒' . $data->city;
        }
        if (!empty($data->street)) {
            $address .= '  ' . $data->street;
        }
        if (!empty($data->building)) {
            $address .= '  ' . $data->building;
        }
        return $address;
    }

    private function address_en($data = null)
    {
        // address format: building + street + city + prefecture + country + postal_code
        $address = [];
        if (!empty($data->building_en)) {
            array_push($address, $data->building_en);
        }
        if (!empty($data->street_en)) {
            array_push($address, $data->street_en);
        }
        if (!empty($data->city_en)) {
            array_push($address, $data->city_en);
        }
        if (!empty($data->prefecture_en)) {
            array_push($address, $data->prefecture_en);
        }
        if (!empty($data->country)) {
            array_push($address, $data->country->name_en);
        }
        if (!empty($data->postal_code_en)) {
            array_push($address, $data->postal_code_en);
        }
        return implode(', ', $address);
    }
}
