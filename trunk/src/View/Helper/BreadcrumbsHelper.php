<?php
namespace App\View\Helper;

use Cake\View\Helper;
use Cake\Collection\Collection;

class BreadcrumbsHelper extends Helper
{
    public $helpers = ['Html'];
    public $breadcrumb = '';
    private $element = '<div class="row"><div class="col-md-12"><ol class="breadcrumb">';

    public function getList($data = null)
    {
        if ($data === null) {
            return false;
        }
        $element = '';
        if ($data) {
            foreach ($data as $key => $value) {
                $element .= '<li class="dd-item" data-id="' . $value->id . '" data-name="' . $value->name . '" data-name_en="' . $value->name_en . '" data-controller="' . $value->controller . '" data-action="' . $value->action . '"  data-url="' . $value->url . '" data-params="' . $value->params . '"><div class="dd-handle">' . $value->name . ' | ' .$value->name_en . '</div>';
                $element .= $this->createSub($value->children);
                $element .= '</li>';
            }
        }
        return $element;
    }

    public function createSub($data = null)
    {
        $element = '';
        if ($data) {
            $element .= '<ol class="dd-list">';
            foreach ($data as $key => $value) {
                $element .= '<li class="dd-item" data-id="' . $value->id . '" data-name="' . $value->name . '" data-name_en="' . $value->name_en . '" data-controller="' . $value->controller . '" data-action="' . $value->action . '"  data-url="' . $value->url . '" data-params="' . $value->params . '"><div class="dd-handle">' . $value->name . ' | ' .$value->name_en . '</div>';
                $element .= $this->createSub($value->children);
                $element .= '</li>';
            }
            $element .= '</ol>';
        }
        return $element;
    }

    public function setData($data = null)
    {
        if ($data === null) {
            return false;
        }
        $this->breadcrumb = $data;
    }

    public function getData()
    {
        return $this->breadcrumb;
    }

    public function show()
    {
        $data = $this->getData();
        if (!$data) {
            return false;
        }
        if (isset($data->breadcrumb_details) && !empty($data->breadcrumb_details)) {
            foreach($data->breadcrumb_details as $key => $value) {
                $this->element .= $this->checkCurrentUrl($value);
            }
        } else {
            $this->element .= $this->checkCurrentUrl($data);
        }
        $this->element .= '</ol></div></div>';
        echo $this->element;
    }

    private function checkCurrentUrl($data = null)
    {
        $active_class = '';
        if (!empty($data->controller) && !empty($data->action)) {
            $params = [];
            if (!empty($data->param)) {
                if (strpos($data->param, ',')) {
                    $arr = explode(',', $data->param);
                    foreach ($arr as $key => $val) {
                        $arr2 = explode('=', $val);
                        $params[$arr2[0]] = $arr2[1];
                    }
                } else {
                    $arr = explode('=', $data->param);
                    $params[$arr[0]] = $arr[1];
                }
            }
            if ($params) {
                $link = $this->Html->link($data->name, [
                    'controller' => $data->controller,
                    'action' => $data->action,
                    '?' => $params,
                ]);
            } else {
                $link = $this->Html->link($data->name, [
                    'controller' => $data->controller,
                    'action' => $data->action,
                ]);
            }
        } else {
            $link = isset($data->name) ? $data->name : '';
        }

        // Check current URL with breadcrumbs
        if (isset($data->controller) && !empty($data->controller) && isset($data->action) && !empty($data->action)) {
            if (($data->action === $this->request->action) && ($data->controller === $this->request->controller)) {
                $active_class = 'active';
                $link = $data->name;
            }
        }
        
        $element = '<li class="' . $active_class . '">' . $link . '</li>';
        return $element;
    }
}
