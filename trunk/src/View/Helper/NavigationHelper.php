<?php
namespace App\View\Helper;

use Cake\View\Helper;
use Cake\Collection\Collection;

class NavigationHelper extends Helper
{
    public $helpers = ['Html'];
    private $element;

    public function show($menuControls = null)
    {
        if ($menuControls) {
            $menuControls = new Collection($menuControls);
            $menuControls = $menuControls->each(function ($value, $key) {
                // Checking sub menu
                if ($value->children) {
                    $this->element .= '<li class="dropdown" data-id="' . $value->id . '">';
                    $this->element .= '<a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">' . $this->getFieldByLocal($value->menu) . '<span class="caret"></span></a>';
                    $this->element .= $this->getSubMenu($value->children);
                    $this->element .= '</li>';
                } else {
                    $this->element .= '<li data-id="' . $value->id . '">';
                    $this->element .= $this->Html->link($this->getFieldByLocal($value->menu), $this->menuLink($value->menu->url));
                    $this->element .= '</li>';
                }
            });
        }
        return $this->element;
    }

    private function getSubMenu($child)
    {
        $element = '';
        if (!$child) {
            return $element;
        }
        $element .= '<ul class="dropdown-menu multi-level" role="menu" aria-labelledby="dropdownMenu">';
        foreach ($child as $key => $value) {
            if ($value->children) {
                $element .= '<li class="dropdown-submenu" data-id="' . $value->id . '">';
                $element .= $this->Html->link($this->getFieldByLocal($value->menu), $this->menuLink($value->menu->url), [
                    'class' => 'dropdown-toggle',
                    'data-toggle' => 'dropdown',
                    'aria-expanded' => false,
                ]);
            } else {
                $element .= '<li data-id="' . $value->id . '">';
                $element .= $this->Html->link($this->getFieldByLocal($value->menu), $this->menuLink($value->menu->url));
            }
            $element .= $this->getSubMenu($value->children);
            $element .= '</li>';
        }
        $element .= '</ul>';
        return $element;
    }

    private function getFieldByLocal($data)
    {
        $local = $this->request->session()->read('tb_field');
        $name = '';
        if ($data) {
            $field = 'name' . $local;
            $name = $data[$field];
            if (!empty($name) || ($name !== null)) {
                return $name;
            }
            switch ($local) {
                case LOCAL_EN :
                    $name = $data->name;
                    break;
                default :
                    $name = $data->name_en;
            }
        }
        return $name;
    }

    private function menuLink($url = null)
    {
        if ($url === null) {
            return false;
        }
        if (substr($url, 0, 1) === '/') {
            return $url;
        } else {
            return '/' . $url;
        }
    }
}
