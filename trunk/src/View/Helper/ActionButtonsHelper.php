<?php
namespace App\View\Helper;

use Cake\View\Helper;

class ActionButtonsHelper extends Helper
{
    public $helpers = ['Html', 'Form'];

    public function btnRegisterNew($controller, $params = [], $action = 'create')
    {
        $output = '<div class="col-sm-3 pull-right" style="text-align: right;">';
        $output .= $this->Html->link(__('TXT_REGISTER_NEW'), [
            'controller' => $controller,
            'action' => $action,
            '?' => $params
            ], [
                'class' => 'btn btn-sm btn-primary btn-width',
            ]);
        $output .= '</div>';
        return $output;
    }

    /**
     *
     * @param integer $id
     * @param boolean $is_suspend
     * @param string $controller_name
     * @param string $action_link default='edit'
     * @return html
     */
    public function btnAction ($id, $is_suspend, $controller_name, $action_link = 'edit')
    {
        $td = '<td width="1%">';
        if ($is_suspend == 1) {
            $td .= $this->Form->button(BTN_ICON_UNDO, [
                'class' => 'btn btn-recover btn-sm btn_suspend',
                'data-name' => 'recover',
                'data-target' => 0,
                'data-id' => $id,
                'escape' => false,
            ]);
        } else {
            $td .= $this->Html->link(BTN_ICON_EDIT, [
                'controller' => $controller_name,
                'action' => $action_link, $id
                ], [
                'class' => 'btn btn-primary btn-sm',
                'escape' => false
            ]);
        }
        $td .= '</td><td width="1%">';
        if ($is_suspend == 1) {
            $td .= $this->Form->button(BTN_ICON_DELETE, [
                'class' => 'btn btn-delete btn-sm',
                'id' => 'btn_delete',
                'data-id' => $id,
                'escape' => false
            ]);
        } else {
            $td .= $this->Form->button(BTN_ICON_STOP, [
                'class' => 'btn btn-suspend btn-sm btn_suspend',
                'data-name' => 'suspend',
                'data-target' => 1,
                'data-id' => $id,
                'escape' => false
            ]);
        }
        $td .= '</td>';

        return $td;
    }

    /**
     *
     * @param integer $id
     * @param string $controller_name
     * @param string $action_link default = 'edit'
     * @return html string
     */
    public function btnActionNoSuspend ($id, $controller_name, $params, $action_link = 'edit')
    {
        $td = '<td width="1%">';
            $td .= $this->Html->link(BTN_ICON_EDIT, [
                'controller' => $controller_name,
                'action' => $action_link, $id,
                '?' => $params
                ], [
                'class' => 'btn btn-primary btn-sm',
                'escape' => false
            ]);
        $td .= '</td><td width="1%">';
            $td .= $this->Form->button(BTN_ICON_DELETE, [
                'class' => 'btn btn-delete btn-sm',
                'id' => 'btn_delete',
                'data-id' => $id,
                'escape' => false
            ]);
        $td .= '</td>';

        return $td;
    }


    /**
     * button register new
     * @param integer $id
     * @param string $class_name
     * @return html string
     */
    public function btnRegisterNewNoLink($id, $class_name = 'register-new-item')
    {
        $output = '<div class="col-sm-3 pull-right" style="text-align: right;">';
        $output .= $this->Form->button(__('TXT_REGISTER_NEW'), [
            'type' => 'button',
            'class' => 'btn btn-sm btn-primary btn-width ' . $class_name,
            'data-id' => $id
        ]);
        $output .= '</div>';
        return $output;
    }

    /**
     *
     * @param string $controller controller name
     * @param string $action save|update
     * @param string $cancel_id default=btn-cancel
     * @param string $reg_id default=btn-register
     * @param string $cancel_class default='' button cancel additional class name
     * @param string $reg_class default='' button register additional class name
     * @return string html
     */
    public function btnSaveCancel($controller, $action = 'register', $cancel_id = 'btn-cancel', $reg_id = 'btn-register', $cancel_class = '', $reg_class = '', $action_cancel = 'index', $params = [])
    {
        $output = '<div class="col-lg-12 col-sm-12 col-md-12 text-center">';
        $output .= $this->Html->link(__('TXT_CANCEL'), [
                    'controller' => $controller,
                    'action' => $action_cancel,
                    '?' => $params
                ], [
                    'class' => 'btn btn-default form_btn btn-sm btn-width ' . $cancel_class,
                    'id' => $cancel_id,
                ]);
        $output .= '&nbsp;&nbsp;';
        $output .= $this->Form->button(($action === 'edit') ? __('TXT_UPDATE') : __('TXT_REGISTER_SAVE'), [
                'type' => 'button',
                'class' => 'btn btn-primary form_btn btn-sm btn-width ' . $reg_class,
                'id' => $reg_id,
            ]);
        $output .= '<div class="clearfix"></div>';
        $output .= '</div>';
        return $output;
    }

    /**
     * disabled text 
     * @param boolean $is_suspend
     * @return string|html
     */
    public function disabledText($is_suspend)
    {
        $suspend = '&nbsp;';
        if ($is_suspend) {
            $suspend = '<p class="btn btn-warning btn-xs" disabled>'. __('TXT_SUSPEND') . '</p>';
        }
        
        return $suspend;
    }
    
    public function btnPdfDownload($ctrl, $action, $id, $display = ' style="display: none;"')
    {
        $btn = '<div style="margin-top: 2em;"></div>';
        $btn .= '<div class="invoice-footer"'.$display.'>';
        $btn .= $this->Html->link(__('TXT_PO_DOWNLOAD_INVOICE'),
            [
                'controller' => $ctrl,
                'action' => $action, $id,
            ],
            [
                'class' => 'btn btn-sm btn-primary btn-width',
                'id' => 'btn-invoice-pdf',
                'target' => '_blank'
            ]);
        $btn .= '</div>';

        return $btn;
    }
}
