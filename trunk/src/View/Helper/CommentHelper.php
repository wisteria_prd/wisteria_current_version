<?php
namespace App\View\Helper;

use Cake\Utility\Inflector;
use Cake\View\Helper;
use Cake\Routing\Router;
use Cake\I18n\Number;

class CommentHelper extends Helper
{
    public $helpers = ['Pluralize', 'Html'];

    public function singularize($string) {
        return Inflector::singularize($string);
    }

    public function textHumanize($string)
    {
        return Inflector::humanize($string);
    }

    public function filterIsMain($data, $value)
    {
        if ($data) {
            foreach ($data as $key => $item) {
                if ($item->is_main == $value) {
                    return $item;
                }
            }
        }

        return false;
    }

    public function countNum($data, $type)
    {
        $str = '';
        switch ($type) {
            case TYPE_CUSTOMER :
                $str = count($data) . ' Clinic';
                if (count($data) >= 2) {
                    $str = count($data) . ' ' . $this->Pluralize->pluralize('Clinic');
                }
                break;
            case TYPE_MEDICAL_CORP :
                $str = count($data) . ' MC';
                if (count($data) >= 2) {
                    $str = count($data) . ' ' . $this->Pluralize->pluralize('MC');
                }
                break;
        }

        return $str;
    }

    public function breadCrumb($data)
    {
        $help = '<div class="row"><div class="col-md-12"><ol class="breadcrumb"><li><a href="'.Router::url('/').'">HOME</a></li>';
        if ($data) {
            foreach ($data as $key => $item) {
                if ($key !== '') {
                    $help .= '<li><a href="' . Router::url('/') . $key . '">' . $item . '</a></li>';
                } else {
                    $help .= '<li class="active">' . $item . '</li>';
                }
            }
        }
        $help .= '</ol></div></div>';

        return $help;
    }

    /**
     *
     * @param integer $external_id
     * @param integer $number
     * @param string $filter
     * @param string $class default "btn btn-default btn-sm"
     * @return string
     */
    public function btnPersonInCharge($external_id, $number, $filter, $class = 'btn btn-default btn-sm')
    {
        $btn_name = __('TXT_NUMBER_PERSON_IN_CHARGE');
        $url = [
            'controller' => 'personInCharges',
            'action' => 'index',
            '?' => [
                'external_id' => $external_id,
                'filter_by' => $filter,
                'des' => $filter,
            ]
        ];

        if ($number == 0) {
            $url['action'] = 'add';
        } else {
            if ($number >= 2) {
                $btn_name = __('TXT_NUMBER_PEOPLE_IN_CHARGE');
            }
        }

        return $this->Html->link($number . ' ' . $btn_name, $url, [
            'class' => $class,
        ]);
    }

    /**
     * common button for create link or list link
     * @params integer $external_id array index 0 required
     * @params integer $number array index 1 required
     * @params string $filter array index 2 required
     * @params string $controller array index 3 required.
     * @params string $name button name array index 4 required.
     * @params string $class array index 5 required. Default "btn btn-default btn-sm"
     * @return string
     */
    public function btnCommonCreateList($params = [])
    {
        $btn_name = ucfirst($params[4]);
        $url = [
            'controller' => $params[3],
            'action' => 'index',
            '?' => [
                'external_id' => $params[0],
                'filter_by' => $params[2],
                'des' => $params[2],
            ]
        ];

        if ($params[1] == 0) {
            $url['action'] = 'create';
        } else {
            if ($params[1] >= 2) {
                $btn_name = ucfirst($this->Pluralize->pluralize($params[4]));
            }
        }

        return $this->Html->link($params[1] . ' ' . __($btn_name), $url, [
            'class' => $params[5],
        ]);
    }

    public function getQueryString($url)
    {
        $arr = explode('?', $url);
        return end($arr);
    }

    public function getFieldName()
    {
        $field = 'name' . $this->request->session()->read('tb_field');
        return $field;
    }

    /**
     *
     * @param integer $total_record
     * @param $total_record : get total records from pagination
     * @return string
     */
    public function displayTotalRecords($total_record)
    {
        $display_item = '';
        if ($total_record < 10) {
            $display_item = '1~' . $total_record;
        } else {
            $display_item = $this->request->query('displays') ? $this->request->query('displays') : 10 . '~' . $total_record;
        }
        $help = 'Display Item: ' .$display_item . ' (Total: ' . $total_record . ')';

        return $help;
    }

    public function floatFormat($value)
    {
        return Number::format($value, ['places' => 2]);
    }

    public function numberFormat($value)
    {
        if ($value) {
            return Number::format($value, ['pattern' => '#,###.00']);
        } else {
            return '0.00';
        }
    }

    public function productDetailName($item, $local)
    {
        $name = $item->pack_size;
        if ($item->tb_pack_sizes) {
            $name .= ' ' . $this->getFieldByLocal($item->tb_pack_sizes, $local);
        }
        $name .= ' x ' . $item->single_unit_size;
        if ($item->single_unit) {
            $name .= ' ' . $this->getFieldByLocal($item->single_unit, $local);
        }
        return $name;
    }

    /**
     * Function address
     * using for display address for Suppliers or Manufacturer
     * @param string $local local store session
     * @param object $data retrieve from Database
     * @return string
     */
    public function address($local, $data)
    {
        $address = '';

        if ($local == '_en') {
            $address = $data->building_en . ', ' . $data->street_en . ', ' . $data->city_en . ', ';
            $address .= $data->prefecture_en . ', ' . $data->country . ', ' . $data->postal_code;
        } else {
            $address = $data->country . ', ' . $data->postal_code . ', ' . $data->prefecture . ', ';
            $address .= $data->city . ', ' . $data->street . ', ' . $data->building;
        }

        return $address;
    }

    public function getMenuList($data)
    {
        $element = '';
        $button = '<div class="mn-remove mn-icon"><i class="fa fa-trash-o" aria-hidden="true"></i></div>';
        if ($data) {
            foreach ($data as $key => $value) {
                $element .= '<li class="dd-item" data-menu-id="' . $value->menu->id . '"><div class="dd-handle">' . $value->menu->name . ' | ' .$value->menu->name_en . '</div>' . $button;
                $element .= $this->createSub($value->children, $button);
                $element .= '</li>';
            }
        }

        return $element;
    }

    /**
     * Function createSub
     * use for create sub menu
     * @param object $data menu array object
     * @return string display child menu
     */
    public function createSub($data, $button)
    {
        $element = '';
        if ($data) {
            $element .= '<ol class="dd-list">';
            foreach ($data as $key => $value) {
                $element .= '<li class="dd-item" data-user-group-id="' . $value->user_group_id . '" data-menu-control="' . $value->id . '" data-menu-id="' . $value->menu->id . '"><div class="dd-handle">' . $value->menu->name_en . ' ' .$value->menu->name . '</div>' . $button;
                $element .= $this->createSub($value->children, $button);
                $element .= '</li>';
            }
            $element .= '</ol>';
        }
        return $element;
    }

    /**
     * product and product details drop down box.
     * @param string $en value='' or '_en'
     * @param array object $products
     * @param integer $selected_id the default selected option when edit
     * @param string $action default = ''
     * @return html string
     */
    public function productDetailsList($en, $products, $selected_id = null, $action = '')
    {
        $output = '<option value="" data-pid="">' . __d('import_detail', 'TXT_SELECT_PRODUCT') . '</option>';
        if ($products) {
            foreach ($products as $product) {
                $product_id = $product->id;
                $value = $this->brandAndProducts($product, $en, true);
                if ($product->product_details) {
                    foreach ($product->product_details as $p) {
                        $value .= $this->sizeAndUnit($p, $en);
                        if ($action == 'edit') {
                            if ($selected_id == $p->id) {
                                $output .= '<option selected="selected" value="' . $p->id . '" data-pid="'.$product_id.'">' . $value . '</option>';
                            }
                        }
                        $output .= '<option value="' . $p->id . '" data-pid="'.$product_id.'">' . $value . '</option>';
                    }
                }
            }
        }

        return $output;
    }

    /**
     * product and product details drop down box. Seller Pricing based.
     * @param string $en value='' or '_en'
     * @param array object $products
     * @param integer $selected_id the default selected option when edit
     * @param string $action default = ''
     * @return html string
     */
    public function productDetailsLists($en, $product_details, $selected_id = null, $action = '')
    {
        $output = '<option value="" data-spid="">' . __('TXT_SELECT_PRODUCT_NAME') . '</option>';
        if ($product_details) {
            foreach ($product_details as $product) {
                $p_name = $product->product->name;
                if (empty($p_name)) {
                    $p_name = $product->product->name_en;
                }

                $detail = $p_name . ' ';
                if (!empty($product->single_unit) && !empty($product->tb_pack_sizes)) {
                    $sgun = $this->isSingleUnit($en, $product);
                    $psn = $this->isPackSize($en, $product);
                    $detail .= '(' .
                        $product->single_unit_size .
                        $sgun . ' x ' .
                        $product->pack_size .
                        $psn . ') ';
                } else if (empty($product->single_unit) && !empty($product->tb_pack_sizes)) {
                    $psn = $this->isPackSize($en, $product);
                    $detail .= '(' .
                        $product->pack_size .
                        $psn . ') ';
                } else if (!empty($product->single_unit) && empty($product->tb_pack_sizes)) {
                    $sgun = $this->isSingleUnit($en, $product);
                    $detail .= '(' .
                        $product->single_unit_size .
                        $sgun . ') ';
                }

                if (!empty($product->description)) {
                    $detail .= '(' . $product->description . ')';
                }

                $output .= '<option value="' . $product->id . '" data-spid="'.$product->product->seller_products[0]->id.'">' . $detail . '</option>';
            }
        }

        return $output;
    }


    /**
     * product and product details drop down box.
     * @param string $en value='' or '_en'
     * @param array object $products
     * @param integer $selected_id the default selected option when edit
     * @param string $action default = ''
     * @return html string
     */
    public function productDetailsListWithBrand($en, $products, $selected_id = null, $action = null)
    {
        $str = '';
        $name = 'name' . $en;

        $output = '<option value="" data-pid="" data-pn="" data-psv="" data-psn="" data-psuv="" data-psun="" data-pk="" data-bn="">' . __('TXT_PRODUCT_SEARCH') . '</option>';

        if ($products) {
            foreach ($products as $key => $product) {
                $product_name = '';
                $brand_name = '';

                // brand name
                if (!empty($product->product_brand->name)) {
                    $brand_name = $product->product_brand->name;
                    $str = $product->product_brand->name . ' ';
                } else {
                    $brand_name = $product->product_brand->name_en;
                    $str = $product->product_brand->name_en . ' ';
                }

                // product name
                if (!empty($product->name)) {
                    $product_name = $product->name;
                    $str .= $product->name . ' ';
                } else {
                    $product_name = $product->name_en;
                    $str .= $product->name_en . ' ';
                }

                // product details
                if ($product->product_details) {
                    foreach ($product->product_details as $key1 => $value1) {
                        $psize = $this->isPackSizeName($en, $value1->tb_pack_sizes[$name]);
                        $sgname = $this->isSingleUnitName($en, $value1->single_unit[$name]);

                        // single unit and pack size
                        if (!empty($value1->single_unit_size) && !empty($value1->pack_size)) {
                            $str .= '(' . $value1->single_unit_size . ' ' . $sgname . ' x ' . $value1->pack_size_value . ' ' . $psize . ') ';
                        } elseif (!empty($value1->pack_size)) {
                            $str .= '(' . $value1->pack_size . ' ' . $psize . ') ';
                        } elseif (!empty($value1->single_unit_size)) {
                            $str .= '(' . $data->single_unit_size . $sgname . ') ';
                        }

                        // description
                        if (!empty($value1->description)) {
                            $str .= '(' . $value1->description . ')';
                        }

                        if (($action !== null) && ($action === 'edit')) {
                            if ($selected_id == $value1->id) {
                                $output .= '<option selected="selected" ' .
                                    'value="' . $value1->id . '" '.
                                    'data-pid="' . $product->id . '" ' .
                                    'data-pn="' . $product_name . '" ' .
                                    'data-psv="' . $value1->pack_size . '" ' .
                                    'data-psn="' . $psize . '" ' .
                                    'data-psuv="' . $value1->single_unit_size . '" ' .
                                    'data-psun="' . $sgname . '" ' .
                                    'data-pk="' . $value1->packing_id . '" ' .
                                    'data-dsc="' . $value1->description . '" ' .
                                    'data-bn="' . $brand_name . '">' . $str . '</option>';
                            }
                        }
                        $output .= '<option ' .
                                    'value="' . $value1->id . '" '.
                                    'data-pid="' . $product->id . '" ' .
                                    'data-pn="' . $product_name . '" ' .
                                    'data-psv="' . $value1->pack_size . '" ' .
                                    'data-psn="' . $psize . '" ' .
                                    'data-psuv="' . $value1->single_unit_size . '" ' .
                                    'data-psun="' . $sgname . '" ' .
                                    'data-pk="' . $value1->packing_id . '" ' .
                                    'data-dsc="' . $value1->description . '" ' .
                                    'data-bn="' . $brand_name . '">' . $str . '</option>';
                    }
                }
            }
        }

        return $output;
    }

    public function productDetailsListByProductId($local, $products)
    {
        $options = [];
        $str = '';
        $name = 'name' . $local;

        if (!$products) {
            return false;
        }
        foreach ($products as $key => $product) {
            // brand name
            if (!empty($product->product_brand->name)) {
                $brand_name = $product->product_brand->name;
                $str = $product->product_brand->name . ' ';
            } else {
                $brand_name = $product->product_brand->name_en;
                $str = $product->product_brand->name_en . ' ';
            }
            // product name
            if (!empty($product->name)) {
                $product_name = $product->name;
                $str .= $product->name . ' ';
            } else {
                $product_name = $product->name_en;
                $str .= $product->name_en . ' ';
            }
            // product details
            if ($product->product_details) {
                foreach ($product->product_details as $key1 => $value1) {
                    $psize = $this->isPackSizeName($local, $value1->tb_pack_sizes[$name]);
                    $sgname = $this->isSingleUnitName($local, $value1->single_unit[$name]);
                    // single unit and pack size
                    if (!empty($value1->single_unit_size) && !empty($value1->pack_size)) {
                        $str .= '(' . $value1->single_unit_size . ' ' . $sgname . ' x ' . $value1->pack_size_value . ' ' . $psize . ') ';
                    } elseif (!empty($value1->pack_size)) {
                        $str .= '(' . $value1->pack_size . ' ' . $psize . ') ';
                    } elseif (!empty($value1->single_unit_size)) {
                        $str .= '(' . $data->single_unit_size . $sgname . ') ';
                    }
                    // description
                    if (!empty($value1->description)) {
                        $str .= '(' . $value1->description . ')';
                    }
                    $options[] = [
                        'text' => $str,
                        'value' => $value1->id,
                        'data-pid' => $product->id,
                        'data-pn' => $product_name,
                        'data-psv' => $value1->pack_size,
                        'data-psn' => $psize,
                        'data-psuv' => $value1->single_unit_size,
                        'data-psun' => $sgname,
                        'data-pk' => $value1->packing_id,
                        'data-dsc' => $value1->description,
                        'data-bn' => $brand_name,
                    ];
                }
            }
        }
        return $options;
    }

    public function dropdownMonth($months, $alias = 'sale_date')
    {
        $month_list = [];
        if ($months) {
            foreach ($months as $month) {
                $d = date('Y-m', strtotime($month->issue_date));
                $month_list[$d] = $month->$alias;
            }
        }
        return $month_list;
    }

    public function sellerDropdown($sellers, $en, $default = '', $type = '')
    {
        $sl = '<option value="" data-type="" data-otherid="">' . __('TXT_SELECT_SELLER') . '</option>';
        if ($sellers) {
            //$name = 'name' . $en;
            foreach ($sellers as $seller) {
                if ($seller->type == TYPE_MANUFACTURER && $seller->is_manufacture_seller == 1) {
                    if ($seller->manufacturer) {
                        $name = $this->nameEnOrJp($seller->manufacturer->name, $seller->manufacturer->name_en, $en);
//                        if ($seller->id == $default && $type == TYPE_MANUFACTURER) {
//                            $sl .= '<option selected="selected" value="' . $seller->id . '" data-type="'.TYPE_MANUFACTURER.'" data-otherid="'.$seller->manufacturer->id.'">' . $seller->manufacturer->$name . '</option>';
//                        } else {
//                            $sl .= '<option value="' . $seller->id . '" data-type="'.TYPE_MANUFACTURER.'" data-otherid="'.$seller->manufacturer->id.'">' . $seller->manufacturer->$name . '</option>';
//                        }
                        if ($seller->id == $default && $type == TYPE_MANUFACTURER) {
                            $sl .= '<option selected="selected" value="' . $seller->id . '" data-type="'.TYPE_MANUFACTURER.'" data-otherid="'.$seller->manufacturer->id.'">' . $name . '</option>';
                        } else {
                            $sl .= '<option value="' . $seller->id . '" data-type="'.TYPE_MANUFACTURER.'" data-otherid="'.$seller->manufacturer->id.'">' . $name . '</option>';
                        }
                    }
                }

                if ($seller->type == TYPE_SUPPLIER) {
                    if ($seller->supplier) {
                        $name = $this->nameEnOrJp($seller->supplier->name, $seller->supplier->name_en, $en);
//                        if ($seller->id == $default && $type == TYPE_SUPPLIER) {
//                            $sl .= '<option selected="selected" value="' . $seller->id . '" data-type="'.TYPE_SUPPLIER.'" data-otherid="'.$seller->supplier->id.'">' . $seller->supplier->$name . '</option>';
//                        } else {
//                            $sl .= '<option value="' . $seller->id . '" data-type="'.TYPE_SUPPLIER.'" data-otherid="'.$seller->supplier->id.'">' . $seller->supplier->$name . '</option>';
//                        }
                        if ($seller->id == $default && $type == TYPE_SUPPLIER) {
                            $sl .= '<option selected="selected" value="' . $seller->id . '" data-type="'.TYPE_SUPPLIER.'" data-otherid="'.$seller->supplier->id.'">' . $name . '</option>';
                        } else {
                            $sl .= '<option value="' . $seller->id . '" data-type="'.TYPE_SUPPLIER.'" data-otherid="'.$seller->supplier->id.'">' . $name . '</option>';
                        }
                    }
                }
            }
        }
        return $sl;
    }

    /**
     * suppliers or manufacturers filter by sales or purchases
     * @param array object $sellers
     * @param string $en
     * @param integer $default
     * @param string $type
     * @return html string
     */
    public function sellerDropdownById($sellers, $en, $default = '', $type = '')
    {
        $sl = '<option value="" data-type="" data-otherid="">' . __('PAYMENT_PURCHASE_TXT_SELECT_SUPPLIER') . '</option>';
        if ($sellers) {
            foreach ($sellers as $seller) {
                if ($seller->seller->type == TYPE_MANUFACTURER && $seller->seller->is_manufacture_seller == 1) {
                    if ($seller->seller->manufacturer) {
                        if ($seller->seller->id == $default && $type == TYPE_MANUFACTURER) {
                            $sl .= '<option selected="selected" value="' . $seller->seller->id . '" data-type="'.TYPE_MANUFACTURER.'" data-otherid="'.$seller->seller->manufacturer->id.'">' .$this->nameEnOrJp($seller->seller->manufacturer->name, $seller->seller->manufacturer->name_en, $en). '</option>';
                        } else {
                            $sl .= '<option value="' . $seller->seller->id . '" data-type="'.TYPE_MANUFACTURER.'" data-otherid="'.$seller->seller->manufacturer->id.'">' . $this->nameEnOrJp($seller->seller->manufacturer->name, $seller->seller->manufacturer->name_en, $en) . '</option>';
                        }
                    }
                }

                if ($seller->seller->type == TYPE_SUPPLIER) {
                    if ($seller->seller->supplier) {
                        if ($seller->seller->id == $default && $type == TYPE_SUPPLIER) {
                            $sl .= '<option selected="selected" value="' . $seller->seller->id . '" data-type="'.TYPE_SUPPLIER.'" data-otherid="'.$seller->seller->supplier->id.'">' . $this->nameEnOrJp($seller->seller->supplier->name, $seller->seller->supplier->name_en, $en) . '</option>';
                        } else {
                            $sl .= '<option value="' . $seller->seller->id . '" data-type="'.TYPE_SUPPLIER.'" data-otherid="'.$seller->seller->supplier->id.'">' . $this->nameEnOrJp($seller->seller->supplier->name, $seller->seller->supplier->name_en, $en) . '</option>';
                        }
                    }
                }
            }
        }
        return $sl;
    }

    /**
     * form label mark as required field.
     * @return html string
     */
    public function formAsterisk()
    {
        return '  <i class="required-indicator">＊</i>';
    }

    /**
     * display product detail name in PO list
     * @param object $data
     * @return string Product Name + Brand Name + Product Detail + Description
     */
    public function POProductDetailName($data)
    {
        $productDetailName = '';

        if (!empty($data->brand_name)) {
            $productDetailName .= $data->brand_name . ' ';
        }

        if (!empty($data->product_name)) {
            $productDetailName .= $data->product_name . ' ';
        }

        $productDetailName .= $this->sizeAndUnit($data, $this->request->session()->read('tb_field'));

        return $productDetailName;
    }

    /**
     * display product brand, products, and product details as patterns:
     * if have both single_unit_size, pack_size, n description:
     * P1. Brand Product (1ml x 1Syringe) (description)
     * Other patterns r:
     * P2. Brand Product (1Syringe) (description)
     * P3. Brand Product (description)
     * b. No product_detail record is also ok
     * Then, display is
     * P4. Brand Product
     * @param object $data
     * @return string
     */
    public function productAndDetails($data, $en)
    {
        $productDetailName = '';

        if (!empty($data->product->product_brand)) {
            if ($data->product->product_brand->name) {
                $productDetailName .= $data->product->product_brand->name . ' ';
            } else {
                $productDetailName .= $data->product->product_brand->name_en . ' ';
            }
        }

        if (!empty($data->product)) {
            if ($data->product->name) {
                $productDetailName .= $data->product->name . ' ';
            } else {
                $productDetailName .= $data->product->name_en . ' ';
            }
        }

        $productDetailName .= $this->sizeAndUnit($data, $en);

        return $productDetailName;
    }

    /**
     * Single Product and Brand with multiple product details as dropdown.
     * if have both single_unit_size, pack_size, n description:
     * P1. Brand Product (1ml x 1Syringe) (description)
     * Other patterns r:
     * P2. Brand Product (1Syringe) (description)
     * P3. Brand Product (description)
     * b. No product_detail record is also ok
     * Then, display is
     * P4. Brand Product
     * @param object $data
     * @return string
     */
    public function singleProductAndDetails($data, $en)
    {
        $productDetailName = '';
        $details = [];

        $name = 'name' . $en;
        if (!empty($data->product_brand)) {
            if ($data->product_brand->name) {
                $productDetailName .= $data->product_brand->name . ' ';
            } else {
                $productDetailName .= $data->product_brand->name_en . ' ';
            }
        }

        if (!empty($data)) {
            if ($data->name) {
                $productDetailName .= $data->name . ' ';
            } else {
                $productDetailName .= $data->name_en . ' ';
            }
        }

        if ($data->product_details) {
            foreach ($data->product_details as $dt) {
                $productDetailName .= $this->sizeAndUnit($dt, $en);

                $details[$dt->id] = $productDetailName;
            }
        }

        return $details;
    }

    /**
     * Single Product and Brand with multiple product details as dropdown.
     * if have both single_unit_size, pack_size, n description:
     * P1. Brand Product (1ml x 1Syringe) (description)
     * Other patterns r:
     * P2. Brand Product (1Syringe) (description)
     * P3. Brand Product (description)
     * b. No product_detail record is also ok
     * Then, display is
     * P4. Brand Product
     * @param object $data
     * @return string
     */
    public function singleProductAndDetailsAsSelectOptions($data, $en)
    {
        $productDetailName = '';
        $output = '<option value="">' . __('TXT_SELECT_PRODUCT_DETAIL') . '</option>';

        if (!empty($data->product_brand)) {
            if ($data->product_brand->name) {
                $productDetailName .= $data->product_brand->name . ' ';
            } else {
                $productDetailName .= $data->product_brand->name_en . ' ';
            }
        }

        if (!empty($data)) {
            if ($data->name) {
                $productDetailName .= $data->name . ' ';
            } else {
                $productDetailName .= $data->name_en . ' ';
            }
        }

        if ($data->product_details) {
            foreach ($data->product_details as $dt) {
                $productDetailName .= $this->sizeAndUnit($dt, $en);
                $output .= '<option value="' . $dt->id . '">' . $productDetailName . '</option>';
            }
        }

        return $output;
    }

    /**
     * all Products and Brands with multiple product details as dropdown.
     * if have both single_unit_size, pack_size, n description:
     * P1. Brand Product (1ml x 1Syringe) (description)
     * Other patterns r:
     * P2. Brand Product (1Syringe) (description)
     * P3. Brand Product (description)
     * b. No product_detail record is also ok
     * Then, display is
     * P4. Brand Product
     * @param object $data
     * @return string
     */
    public function allProductAndDetailsAsSelectOptions($data, $en, $option = null)
    {
        $output = '<option data-pid="" value="">' . __('TXT_SELECT_PRODUCT_DETAIL') . '</option>';
        if ($option !== null) {
            $output = '<option data-pid="" value="">' . $option . '</option>';
        }
        $name = 'name' . $en;

        if ($data) {
            foreach ($data as $dt) {
                $productDetailName = '';
                if (!empty($dt->product->product_brand)) {
                    if ($dt->product->product_brand->name) {
                        $productDetailName .= $dt->product->product_brand->name . ' ';
                    } else {
                        $productDetailName .= $dt->product->product_brand->name_en . ' ';
                    }
                }

                if (!empty($dt->product)) {
                    if ($dt->product->name) {
                        $productDetailName .= $dt->product->name . ' ';
                    } else {
                        $productDetailName .= $dt->product->name_en . ' ';
                    }
                }
                $productDetailName .= $this->sizeAndUnit($dt, $en);
                $output .= '<option data-pid="' . $dt->product->id . '" value="' . $dt->id . '">' . $productDetailName . '</option>';
            }
        }

        return $output;
    }

    /**
     * all Products and Brands with multiple product details as dropdown.
     * if have both single_unit_size, pack_size, n description:
     * P1. Brand Product (1ml x 1Syringe) (description)
     * Other patterns r:
     * P2. Brand Product (1Syringe) (description)
     * P3. Brand Product (description)
     * b. No product_detail record is also ok
     * Then, display is
     * P4. Brand Product
     * @param object $data
     * @return string
     */
    public function allProductAndDetailsAsSelectOptionAndPackageDetails($data, $data_package, $en)
    {
        $output = '<option value="" data-type="" data-pid="">' . __('TXT_SEARCH') . '</option>';

        if ($data) {
            foreach ($data as $product) {
                $details = '';
                if (!empty($product->product_brand)) {
                    $details .= $this->nameEnOrJp($product->product_brand->name, $product->product_brand->name_en, $en) . ' ';
                }

                $details .= $this->nameEnOrJp($product->name, $product->name_en, $en) . ' ';

                $detail_id = '';
                if ($product->product_details) {
                    foreach ($product->product_details as $dt) {
                        $detail_id = $dt->id;
                        $details .= $this->sizeAndUnit($dt, $en);
                    }
                }
                $output .= '<option value="' . $detail_id . '" data-type="'.TYPE_PRODUCT_DETAILS.'" data-pid="' . $product->id . '">' . $details . '</option>';
            }
        }

        if ($data_package) {
            foreach ($data_package as $package) {
                $output .= '<option value="' . $package->id . '" data-type="'.PRICING_TYPE_PACKAGE.'" data-pid="'.$package->id.'">' . $package->name . '</option>';
            }
        }

        return $output;
    }

    public function getFieldByLocal($data, $local)
    {
        if ($data) {
            $name = '';
            if (!empty($data->name_en) && ($local === LOCAL_EN)) {
                $name = $data->name_en;
            } else {
                $name = $data->name;
            }
            if (empty($name)) {
                $name = $data->name_en;
            }
            return $name;
        }
    }

    private function isPluralConvert($en, $text = '')
    {
        if ($en == '_en') {
            $except = ['l', 'ml', 'cc', 'cm', '', 'mm', 'g', 'mg', 'kg'];
            if (!in_array($text, $except)) {
                return true;
            }
            return false;
        }
        return false;
    }

    private function isSingleUnit($en, $dt)
    {
        $name = 'name' . $en;
        $sgun = $dt->single_unit->$name;
        if ($dt->single_unit_size >= 2) {
            if ($this->isPluralConvert($en, $sgun)) {
                $sgun = $this->Pluralize->pluralize($sgun);
            }
        }
        return $sgun;
    }

    private function isPackSize($en, $dt)
    {
        $name = 'name' . $en;
        $psn = $dt->tb_pack_sizes->$name;
        if ($dt->pack_size >= 2) {
            if ($this->isPluralConvert($en, $psn)) {
                $psn = $this->Pluralize->pluralize($psn);
            }
        }
        return $psn;
    }

    private function isSingleUnitName($en, $text)
    {
        if ($this->isPluralConvert($en, $text)) {
            $text = $this->Pluralize->pluralize($text);
        }
        return $text;
    }

    private function isPackSizeName($en, $text)
    {
        if ($this->isPluralConvert($en, $text)) {
            $text = $this->Pluralize->pluralize($text);
        }
        return $text;
    }

    public function nameEnOrJp($name_jp, $name_en, $en = '')
    {
        $new_name = '';
        if ($en == '_en') {
            $new_name = $name_en;
            if ($new_name == '') {
                $new_name = $name_jp;
            }
        } else {
            $new_name = $name_jp;
            if ($new_name == '') {
                $new_name = $name_en;
            }
        }

        return h($new_name);
    }

    public function brandAndProducts($product, $en, $isProduct = false)
    {
        $details = '';
        if ($isProduct) {
            if (!empty($product->product_brand)) {
                $details .= $this->nameEnOrJp($product->product_brand->name, $product->product_brand->name_en, $en) . ' ';
            }

            $details .= $this->nameEnOrJp($product->name, $product->name_en, $en) . ' ';
        } else {
            if (!empty($product->product->product_brand)) {
                $details .= $this->nameEnOrJp($product->product->product_brand->name, $product->product->product_brand->name_en, $en) . ' ';
            }

            if (!empty($product->product)) {
                $details .= $this->nameEnOrJp($product->product->name, $product->product->name_en, $en) . ' ';
            }
        }

        return $details;
    }

    public function sizeAndUnit($dt, $en)
    {
        $details = '';
        if (!empty($dt->single_unit) && !empty($dt->tb_pack_sizes)) {
            $sgun = $this->isSingleUnit($en, $dt);
            $psn = $this->isPackSize($en, $dt);
            $details .= '(' .
                $dt->single_unit_size .
                $sgun . ' x ' .
                $dt->pack_size .
                $psn . ') ';
        } else if (empty($dt->single_unit) && !empty($dt->tb_pack_sizes)) {
            $psn = $this->isPackSize($en, $dt);
            $details .= '(' .
                $dt->pack_size .
                $psn . ') ';
        } else if (empty($dt->tb_pack_sizes) && !empty($dt->single_unit)) {
            $sgun = $this->isSingleUnit($en, $dt);
            $details .= '(' .
                $dt->single_unit_size .
                $sgun . ') ';
        }

        if (!empty($dt->description)) {
            $details .= '(' . $dt->description . ')';
        }

        return $details;
    }

    /**
     * Format Japaneses and Other countries with break line.
     * @param string $en
     * @param integer $value
     * @param string $code
     * @return string
     */
    public function currencyEnJpFormat($en, $value, $code)
    {
        $currency = '';
        $name = 'code'.$en;
        if ($en == LOCAL_EN) {
            $currency .= $code->$name . ' ';
            $currency .= $this->isJK($code->code_en, $value);
        } else {
            $currency .= $this->isJK($code->code_en, $value) . ' ';
            $currency .= $code->$name;
        }
        return $currency;
    }

    /**
     * Format Japaneses and Other countries with single line.
     * @param string $en
     * @param integer $value
     * @param string $code
     * @return string
     */
    public function currencyEnJpFormatS($en, $value, $code)
    {
        $currency = '';
        $name = 'code'.$en;
        if ($en == LOCAL_EN) {
            //$currency .= $code->$name . ' ';
            $currency .= $this->isJK('JPY', $value);
        } else {
            $currency .= $this->isJK('JPY', $value);
            $currency .= $code;
        }
        return $currency;
    }

    /**
     * Both suppliers and manufacturers combination as drop down.
     * @param object $suppliers
     * @param string $en
     * @param integer $selected default = null|suppler id for default select.
     * @return string
     */
    public function supplierDropdown($suppliers, $en, $selected = null)
    {
        $output = '<option value="" data-type="" data-otherid="">' . __('TXT_SELECT_SUPPLIER') . '</option>';
        foreach ($suppliers as $supplier) {
            if ($supplier->manufacturer) {
                $sl = ($selected == $supplier->id) ? ' selected="selected"' : '';
                $output .= '<option value="' . $supplier->id . '"'. $sl .' data-type="'.TYPE_MANUFACTURER.'" data-otherid="'.$supplier->manufacturer->id.'">' .
                $this->nameEnOrJp($supplier->manufacturer->name, $supplier->manufacturer->name_en, $en) .
                '</option>';
            } else if ($supplier->supplier) {
                $sl = ($selected == $supplier->id) ? ' selected="selected"' : '';
                $output .= '<option value="' . $supplier->id . '"'. $sl .' data-type="'.TYPE_SUPPLIER.'" data-otherid="'.$supplier->supplier->id.'">' .
                $this->nameEnOrJp($supplier->supplier->name, $supplier->supplier->name_en, $en) .
                '</option>';
            }
        }
        return $output;
    }

    /**
     * check if Currency of type is Japan or Korea
     * @param string $currency
     * @param double $number
     * @return integer|double
     */
    public function isJK($currency, $number)
    {
        if ($number === null) {
            $number = 0;
        }
        if ($currency == 'JPY' || $currency == 'KRW') {
            if (intval($number) > 0) {
                return Number::format($number, ['pattern' => '#,###']);
            }
            return 0;
        }
        if (intval($number) > 0) {
            return Number::format($number, ['pattern' => '#,###.00']);
        }
        return '0.00';
    }

    public function addressFormat($en, $data, $isSingapore = '')
    {
        $address = '';
        $jp_addr2 = '';
        $en_addr2 = '';
        $fax = $data->fax;
        $tel = '';

        if ($en == '_en') {
            if ($data->building_en || $data->street_en || $data->city_en) {
                $address .= $this->addressEnFormat($data, $isSingapore);
                $en_addr2 .= $this->addressEnFormat2($data, $isSingapore);
            } else {
                $address .= $this->addressJpFormat($data);
                $jp_addr2 .= $this->addressJpFormat2($data);
            }
        } else {
            if ($data->building || $data->street || $data->city) {
                $address .= $this->addressJpFormat($data);
                $jp_addr2 .= $this->addressJpFormat2($data);
            } else {
                $address .= $this->addressEnFormat($data, $isSingapore);
                $en_addr2 .= $this->addressEnFormat2($data, $isSingapore);
            }
        }

        if ($data->info_details) {
            if ($data->info_details[0]->phone) {
                $tel .= $data->info_details[0]->phone;
            } else {
                $tel .= $data->info_details[0]->tel;
            }

        }

        return [$address, $jp_addr2, $en_addr2, $tel, $fax];
    }

    /**
     * use for format currency currently is JPY currency
     * @param string currency type
     * @param int $value currency
     * @return string
     */
    public function exchangeRateFormat($label, $value)
    {
        $rate = '0';
        if ($value > 0) {
            $rate = Number::format($value, ['pattern' => '#,###']);
        }
        return $rate . ' ' . $label;
    }

    /*
     * Japan address format first line
     */
    private function addressJpFormat($data)
    {
        $address = '〒';
        $address .= $data->postal_code . '　';
        if ($data->prefecture) {
            $address .= $data->prefecture;
        }

        if ($data->city) {
            $address .= $data->city;
        }

        return $address;
    }

    /**
     * Japan address format second line.
     * @param type $data
     */
    private function addressJpFormat2($data)
    {
        $address = '';
        if ($data->street) {
            $address .= $data->street;
        }

        if ($data->building) {
            $address .= '　' . $data->building;
        }
        return $address;
    }

    /**
     * English address format second line.
     * @param type $data
     * @param boolean $isSingapore
     * @return type
     */
    private function addressEnFormat($data, $isSingapore)
    {
        $address = '';
        if ($isSingapore == TYPE_MP) {
            if ($data->building_en) {
                $address .= $data->building_en . ', ';
            }
            if ($data->street_en) {
                $address .= $data->street_en;
            }
        } else {
            if ($data->building_en) {
                $address .= $data->building_en . ', ';
            }
            if ($data->street_en) {
                $address .= $data->street_en . ', ';
            }
            if ($data->city_en) {
                $address .= $data->city_en . ', ';
            }
        }

        return rtrim($address, ',');
    }

    /**
     * English address second line.
     * @param object $data
     * @return string
     */
    private function addressEnFormat2($data, $isSingapore)
    {
        $address = '';
        if ($isSingapore == TYPE_MP) {
            if ($data->country) {
                $address .= $data->country->name_en . ' ';
            }
            $address .= $data->postal_code;
        } else {
            if ($data->prefecture_en) {
                $address .= $data->prefecture_en;
            }
            if ($data->country) {
                $address .= $data->country->name_en . ' ';
            }
            $address .= $data->postal_code;
        }

        return $address;
    }
}
