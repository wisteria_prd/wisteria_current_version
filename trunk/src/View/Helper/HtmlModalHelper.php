<?php
namespace App\View\Helper;

use Cake\View\Helper;

class HtmlModalHelper extends Helper
{
    public $helpers = ['Html'];

    public function modalHeader($id, $title = '', $size = '') {
        $help = '
            <div class="modal fade" id="' . $id . '" role="dialog" data-backdrop="static">
                <div class="modal-dialog' . $size . '" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title">' . $title . '</h4>
                        </div>
                        <div class="modal-body">
        ';

        return $help;
    }

    public function modalNoHeader($id, $size = '') {
        $help = '
            <div class="modal fade" id="' . $id . '" role="dialog" data-backdrop="static">
                <div class="modal-dialog' . $size . '" role="document">
                    <div class="modal-content">
                        <div class="modal-body">
        ';

        return $help;
    }

    public function modalFooter() {
        $help = '
                        </div>
                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
        ';
        
        return $help;
    }

    public function modalWithFooter($properties = [], $is_ok = true, $is_cancel = true)
    {
        $help = '</div>';
        $help .= '<div class="modal-footer">';
        $help .= '<div class="row">';
        $help .= '<div class="col-sm-12 text-center">';
        if ($is_cancel) {
            $help .= '<button type="button" id="'.$properties['cancel_id'].'" class="btn btn-sm'.$properties['cancel_class'].'" data-dismiss="modal">'.$properties['cancel_text'].'</button>';
            $help .= '&nbsp;&nbsp;';
        }
        if ($is_ok) {
            $help .= '<button type="button" id="'.$properties['ok_id'].'" class="btn btn-sm'.$properties['ok_class'].'">'.$properties['ok_text'].'</button>';
        }
        $help .= '</div></div></div>';
        $help .= '</div><!-- /.modal-content -->';
        $help .= '</div><!-- /.modal-dialog -->';
        $help .= '</div><!-- /.modal -->';
        return $help;
    }
}