<?php
namespace App\View\Helper;

use Cake\View\Helper;
use Cake\Routing\Router;

class CurrencyHelper extends Helper
{
    public $helpers = ['Html'];
    public $codeName = [
        'JPY' => array(
            'name' => '日本円',
            'code_en' => 'JPY',
            'code_jp' => '円',
        ),
        'USD' => array(
            'name' => '米国ドル',
            'code_en' => 'USD',
            'code_jp' => 'ドル',
        ),
        'EUR' => array(
            'name' => 'ユーロ',
            'code_en' => 'EUR',
            'code_jp' => 'ユーロ',
        ),
        'GBP' => array(
            'name' => '英国ポンド',
            'code_en' => 'GBP',
            'code_jp' => 'ポンド',
        ),
        'SGD' => array(
            'name' => '星国ドル',
            'code_en' => 'SGD',
            'code_jp' => 'Sドル',
        ),
        'KRW' => array(
            'name' => '韓国ウォン',
            'code_en' => 'KRW',
            'code_jp' => 'ウォン',
        ),
        'CHF' => array(
            'name' => 'スイスフラン',
            'code_en' => 'CHF',
            'code_jp' => 'Sフラン',
        ),
    ];

    public function format($codeEn = null, $amount = null)
    {
        $format = '';
        switch ($codeEn) {
            case 'USD':
            case 'EUR':
            case 'SGD':
            case 'GBP':
                $format = number_format($amount, 2);
                break;

            case 'KRW':
            case 'JPY':
                $format = number_format($amount);
                break;
        }
        return $this->checkLanguageAndGetPrice($codeEn, $format);
    }

    private function checkLanguageAndGetPrice($codeEn, $format)
    {
        $result = '';
        if ($this->request->session()->read('tb_field') === '_en') {
            $result = $this->codeName[$codeEn]['code_en'] . ' '. $format;
        } else {
            $result = $format. ' '. $this->codeName[$codeEn]['code_jp'];
        }
        return $result;
    }
}
