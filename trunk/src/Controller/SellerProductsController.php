<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Network\Exception\NotFoundException;

class SellerProductsController extends AppController
{
    private $locale;

    public function initialize()
    {
        parent::initialize();
        $this->locale = $this->request->session()->read('tb_field');
    }

    public function index()
    {
        $data = $this->SellerProducts->newEntity();
        $sellerBrands = [];
        if ($this->request->query('seller_brand_id')) {
            $this->loadModel('SellerBrands');
            $sellerBrands = $this->SellerBrands->findById($this->request->query('seller_brand_id'))
                ->contain([
                    'SellerProducts' => [
                        'ProductDetails' => [
                            'SingleUnits',
                            'PackSizes',
                        ],
                    ],
                    'ProductBrands',
                ])
                ->first();
        }
        $sellers = ['' => 'Seller Name'] + $this->getSellerList();
        $data1 = [
            'data' => $data,
            'locale' => $this->locale,
            'sellers' => $sellers,
            'sellerBrands' => $sellerBrands,
        ];
        $this->set($data1);
    }

    public function saveOrUpdate()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->viewBuilder()->layout('ajax');
        $this->autoRender = false;
        $this->response->type('json');
        $this->loadModel('SellerBrands');
        $seller_brand = $this->SellerBrands->find()
            ->where([
                'seller_id' => $this->request->data['seller_id'],
                // 'product_brand_id' => $this->request->data['brand_id']
            ])
            ->first();
        pr($seller_brand);
        exit;
        $data = [];
        if ($this->request->data['ids']) {
            foreach($this->request->data['ids'] as $key => $value) {
                $data[] = [
                    'product_detail_id' => $value,
                    'seller_brand_id' => $this->request->data['seller_id'],
                ];
            }
        }
        // Delete records based on seller_id
        $this->SellerProducts->deleteAll([
            'seller_brand_id' =>  $this->request->data['seller_id'],
        ]);
        // Save new records
        $entities = $this->SellerProducts->newEntities($data);
        if ($this->SellerProducts->saveMany($entities)) {
            $this->response->body(json_encode([
                'status' => 1,
                'message' => MSG_SUCCESS,
            ]));
            return $this->response;
        }
    }

    public function getSellerList()
    {
        $this->loadModel('Sellers');
        $field_name = 'name' . $this->local;
        $data = $this->Sellers->getSellerList($field_name);
        return $data;
    }

    public function getListBrand()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->autoRender = false;
        $this->viewBuilder()->layout('ajax');
        $this->response->type('json');
        $this->loadModel('ProductBrands');
        $data = $this->ProductBrands->getListofBrandsBySellerId($this->request->query('id'));
        if (!$data) {
            $this->response->body(json_encode([
                'status' => 0,
                'message' => MSG_ERROR,
                'data' => $data,
            ]));
            return $this->response;
        }
        $this->response->body(json_encode([
            'status' => 1,
            'message' => MSG_SUCCESS,
            'data' => $data,
        ]));
        return $this->response;
    }

    public function getProductByBrandId()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->autoRender = false;
        $this->viewBuilder()->layout('ajax');
        $this->response->type('json');
        $this->loadModel('Products');
        $data = $this->Products->findByProductBrandId($this->request->query('id'))
            ->contain([
                'ProductDetails' => [
                    'SingleUnits',
                    'PackSizes',
                ]
            ])
            ->all();
        if (!$data) {
            $this->response->body(json_encode([
                'status' => 0,
                'message' => MSG_ERROR,
                'data' => $data,
            ]));
            return $this->response;
        }
        $this->response->body(json_encode([
            'status' => 1,
            'message' => MSG_SUCCESS,
            'data' => $data,
        ]));
        return $this->response;
    }

    public function getBySellerBrandId()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->autoRender = false;
        $this->viewBuilder()->layout('ajax');
        $this->response->type('json');
        $data = $this->SellerProducts->getBySellerBrandId($this->request->query('id'));
        if (!$data) {
            return $this->response->body(json_encode([
                'status' => 0,
                'message' => MSG_ERROR,
                'data' => $data,
            ]));
        }
        return $this->response->body(json_encode([
            'status' => 1,
            'message' => MSG_SUCCESS,
            'data' => $data,
        ]));
    }

    private function getManufacturerList($field_name)
    {
        $this->loadModel('Manufacturers');
        $data = $this->Manufacturers->getManufacturerList('name' . $this->local, $field_name);
        return $data;
    }

    private function getSupplierList($field_name)
    {
        $this->loadModel('Suppliers');
        $data = $this->Suppliers->getSupplierList('name' . $this->local, $field_name);
        return $data;
    }
}
