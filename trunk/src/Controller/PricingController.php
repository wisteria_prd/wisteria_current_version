<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Network\Exception\NotFoundException;
use Cake\Collection\Collection;
use Cake\Utility\Hash;

class PricingController extends AppController
{
    public $locale;

    public function initialize()
    {
        parent::initialize();
        $this->loadModel('Countries');
        $this->locale = $this->request->session()->read('tb_field');
    }

    public function index_old()
    {
        $this->loadModel('Products');
        $this->loadModel('ProductPackages');
        $this->loadModel('Customers');
        $this->loadModel('Currencies');
        $products = $this->Products->productDetailsDropdown();
        $packages = $this->ProductPackages->find()
            ->where(['ProductPackages.is_suspend' => 0])
            ->all();
        $prc = $this->CustomConstant->pricingType();
        $customers = $this->Customers->customerDropdown();
        $currencies = $this->Currencies->getDropdown();
        $data = [
            'packages' => $packages,
            'locale' => $this->locale,
            'prc' => $prc,
            'customers' => $customers,
            'currencies' => $currencies,
        ];
        $this->set($data);
    }

    public function index()
    {
        $options = [];
        $mId = $this->request->query('filter_manufacturer');
        $bId = $this->request->query('filter_brand');
        $pId = $this->request->query('filter_product');
        $this->loadModel('ProductDetails');
        $this->loadModel('ProductPackages');
        $this->loadModel('Manufacturers');
        $this->loadModel('ProductBrands');
        $this->loadModel('Products');
        $manufacturers = $this->Manufacturers->getAvailableList();
        $brands = $this->ProductBrands->getAvailableListByManufacturerId($mId);
        $products = $this->Products->getAvailableListByProductBrandId($bId);
        // $productDetails = $this->ProductDetails->find()->where(['ProductDetails.product_id' => $pId])->first();
        $options = [];
        // SET PAGE LIMIT ON PAGINATION
        $display = PAGE_NUMBER;
        if (!empty($this->request->query('displays'))) {
            $display = $this->request->query('displays');
        }
        $this->paginate = [
            'conditions' => $options,
            'limit' => $display,
        ];

        $query = $this->paginate($this->Pricing);
        foreach ($query->toArray() as &$value) {
            switch ($value->type) {
                case 'product_package':
                    $data2 = $this->Pricing->find()
                    ->contain([
                        'ProductPackages' => [
                            'ProductPackageDetails' => function($q) {
                                return $q->group('product_package_id')
                                ->contain([
                                    'ProductDetails' => [
                                        'Products' => [
                                            'ProductBrands' => [
                                                'SellerBrands' => [
                                                    'Sellers'
                                                ]
                                            ]
                                        ]
                                    ]
                                ]);
                            }
                        ]
                    ])
                    ->where([
                        'Pricing.id' => $value->id,
                    ])->first()->toArray();
                    $value->get_result = isset($data2['product_package']['product_package_details'][0]) ? $data2['product_package']['product_package_details'][0] : [];
                    break;

                case 'product_detail':
                    $data2 = $this->Pricing->find()
                    ->contain([
                        'ProductDetails' => [
                            'Products' => [
                                'ProductBrands' => [
                                    'SellerBrands' => [
                                        'Sellers'
                                    ]
                                ]
                            ]
                        ]
                    ])
                    ->where([
                        'Pricing.id' => $value->id,
                    ])->first()->toArray();
                    $value->get_result = $data2;
                    break;
            }
        }
        $data = [
            'data' => $query,
            'locale' => $this->locale,
            'display' => $display,
            'paging' => $this->request->param('paging')['Pricing']['pageCount'],
            'manufacturers' => $manufacturers,
            'brands' => $brands,
            'products' => $products,
        ];
       $this->set($data);
    }

    public function getPrice()
    {
        if ($this->request->is('ajax')) {
            $type = $this->request->query('type');

            $data = $this->Pricing->find()
                ->contain([
                    'Currencies',
                    'ClinicPricing'
                ])
                ->where([
                    'Pricing.external_id' => $this->request->query('external_id'),
                    'Pricing.type' => $type
                ]);

            $this->set(compact('data'));
            $this->set('_serialize', ['data']);
        }
        $this->viewBuilder()->layout('ajax');
    }

    public function getSinglePrice()
    {
        if ($this->request->is('ajax')) {
            $id = $this->request->query('inserted_id');
            $data = $this->Pricing->find()
                ->contain([
                    'Currencies',
                    'ClinicPricing'
                ])
                ->where([
                    'Pricing.id' => $id
                ])->first();

            $this->set(compact('data'));
            $this->set('_serialize', ['data']);
        }
        $this->viewBuilder()->layout('ajax');
    }

    public function create()
    {
        $entity = $this->Pricing->newEntity();
        if ($this->request->is('post')) {
            $response = $this->Pricing->patchEntity($entity, $this->request->data);
            if ($this->Pricing->save($response)) {
                return $this->redirect('/pricing');
            }
        }
        $data = [
            'entity' => $entity,
        ];
        $this->set($data);
    }

    public function edit()
    {
        if ($this->request->is('ajax')) {
            $this->autoRender = false;
            $this->response->disableCache();
            $this->response->type('json');
            $this->viewBuilder()->layout(false);

            $request = $this->request->data;
            $pricing = $this->Pricing->get($this->request->data['id']);
            list($error, $msg, $optional) = $this->validateCustomers($request);

            if ($optional == false) {
                $associate = ['associated' => ['ClinicPricing'], 'validate' => false];
                $pricing = $this->Pricing->patchEntity($pricing, $request, $associate);
                $pricing->kind = TYPE_CLINIC;
            } else {
                unset($request['clinic_pricing']);
                $pricing = $this->Pricing->patchEntity($pricing, $request, ['associated' => []]);
                $pricing->kind = TYPE_GENERAL;
            }

            if (empty($pricing->errors()) && empty($error)) {
                $this->loadModel('ClinicPricing');
                $this->ClinicPricing->deleteAll(['pricing_id' => $this->request->data['id']]);

                $this->Pricing->save($pricing);
                $this->response->body(json_encode([
                    'status' => 1,
                    'type' => $pricing->type,
                    'external_id' => $pricing->external_id
                ]));
                return $this->response;
            }

            $this->response->body(json_encode([
                'status' => 0,
                'clinic' => $msg,
                'message' => $pricing->errors()]
            ));
            return $this->response;
        }
    }

    public function getFoc()
    {
        if ($this->request->is('ajax')) {
            $pricing_id = $this->request->query('pricing_id');

            $this->loadModel('FocDiscounts');
            $discounts = $this->FocDiscounts->find()
                ->contain([
                    'ProductDetails' => [
                        'queryBuilder' => function ($q) {
                            return $q->where(['ProductDetails.is_suspend' => 0]);
                        },
                        'Products' => [
                            'ProductBrands',
                            'queryBuilder' => function ($q) {
                                return $q->where(['Products.is_suspend' => 0]);
                            }
                        ],
                        'SingleUnits',
                        'PackSizes',
                    ],
                ])
                ->where([
                    'FocDiscounts.external_id' => $pricing_id,
                    'FocDiscounts.type' => TYPE_PRICING
                ]);

            $this->loadModel('Products');
            $detail_list = $this->Products->productDetailsDropdown();

            $en = $this->request->session()->read('tb_field');
            $this->set(compact('discounts', 'detail_list', 'en'));
            $this->set('_serialize', ['discounts', 'detail_list', 'en']);
        }
        $this->viewBuilder()->layout('ajax');
    }

    public function priceNameList()
    {
        if ($this->request->is('ajax')) {
            $type = $this->request->query('type');
            $en = $this->request->session()->read('tb_field');
            $products = [];
            switch ($type) {
                case PRICING_TYPE_PACKAGE:
                    $this->loadModel('ProductPackages');
                    $products = $this->ProductPackages->packagesDropdown(null, 'all');
                    break;
                case TYPE_PRODUCT_DETAILS:
                    $this->loadModel('ProductDetails');
                    $products = $this->ProductDetails->detailsDropdown();
                    break;
                case TYPE_PRODUCT:
                    $this->loadModel('Products');
                    $products = $this->Products->productsDropdown(null, null, 'all');
                    break;
            }

            $this->set(compact('products', 'en', 'type'));
            $this->set('_serialize', ['products', 'en', 'type']);
        }
        $this->viewBuilder()->layout('ajax');
    }

    public function view()
    {
        if ($this->request->is('ajax')) {
            $this->loadModel('ProductDetails');

            $products = $this->ProductDetails->find()
                ->contain([
                    'Products.ProductBrands.Manufacturers',
                    'LogisticTemperatures',
                    'PackSizes',
                    'SingleUnits'
                ])
                ->where([
                    'ProductDetails.is_suspend' => 0,
                    'ProductDetails.id' => $this->request->query('detail_id')
                ])
                ->first();

            $en = $this->request->session()->read('tb_field');

            $this->set(compact('products', 'en'));
            $this->set('_serialize', ['products', 'en']);
        }

        $this->viewBuilder()->layout('ajax');
    }

    public function delete()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->viewBuilder()->layout('ajax');
        $this->autoRender = false;
        $this->response->type('json');
        $this->response->disableCache();
        $data = $this->Pricing->get($this->request->data['id']);
        if ($this->Pricing->delete($data)) {
            $this->response->body(json_encode([
                'status' => 1,
                'message' => MSG_SUCCESS,
            ]));

            return $this->response;
        }
    }

    public function productAdvancedSearch()
    {
        if ($this->request->is('ajax')) {
            $this->loadModel('Manufacturers');
            $en = $this->request->session()->read('tb_field');
            $manufacturers = $this->Manufacturers->manufacturerDropdown(null, 'all');
            $product_id = $this->request->query('product_id');
            $pricing = false;
            if ($this->request->query('pricing')) {
                $pricing = true;
            }

            $this->loadModel('Products');
            $products = $this->Products->productsDropdown(null, null, 'all');
            $this->set(compact('products', 'product_id'));

            $this->set(compact('manufacturers', 'en', 'pricing'));
            $this->set('_serialize', ['manufacturers', 'en', 'pricing']);
        }

        $this->viewBuilder()->layout('ajax');
    }

    public function productDetailList()
    {
        if ($this->request->is('ajax')) {
            $this->loadModel('ProductDetails');

            $product_id = $this->request->query('product_id');
            $product_name = $this->request->query('product_name');

            $product_details = $this->ProductDetails->find()
            ->contain([
                'PackSizes',
                'SingleUnits'
            ])
            ->where([
                'ProductDetails.is_suspend' => 0,
                'ProductDetails.product_id' => $product_id
            ]);

            $en = $this->request->session()->read('tb_field');

            $pricing = false;
            if ($this->request->query('pricing')) {
                $pricing = true;
            }

            $this->set(compact('product_details', 'en', 'product_name', 'pricing'));
            $this->set('_serialize', ['product_details', 'en', 'product_name']);
        }

        $this->viewBuilder()->layout('ajax');
    }

    public function getBetweenQuantity()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->viewBuilder()->layout('ajax');
        $this->loadModel('FocDiscounts');
        $quantity = $this->request->query('quantity');
        $externalId = $this->request->query('externalId');
        $data = $this->Pricing->getListQtyInBetween($quantity);
        $focDiscounts = $this->FocDiscounts->getByTypeAndExternalId(TYPE_PRICING, $externalId);

        if ($focDiscounts) {
            $focDiscounts = new Collection($focDiscounts);
        }

        $this->set(compact('data', 'focDiscounts'));
    }

    /**
     * validate on adding multiple selection.
     * @param array $request
     * @return null on optional or return empty array on validate or not.
     */
    private function validateCustomers($request)
    {
        $error = [];
        $msg = [];
        $optional = false;

        if (count($request['clinic_pricing']) > 1) {
            $i = 0;
            foreach ($request['clinic_pricing'] as $clinic) {
                if (empty($clinic['customer_id'])) {
                    $error[$i] = true;
                    $msg[$i] = __('TXT_MESSAGE_REQUIRED');
                }
                $i++;
            }
        } else {
            if (empty($request['clinic_pricing'][0]['customer_id'])) {
                $optional = true;
            }
        }
        return [$error, $msg, $optional]; //optional no validate is needed.
    }

    public function changeOfStatus()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->viewBuilder()->layout('ajax');
        $this->autoRender = false;
        $this->response->disableCache();
        $this->response->type('json');
        $data = $this->Pricing->get($this->request->data['id']);
        $data->is_suspend = $this->request->data['status'];
        if ($this->Pricing->save($data)) {
            $this->response->body(json_encode([
                'status' => 1,
                'message' => MSG_SUCCESS,
            ]));

            return $this->response;
        }
    }

    public function getStatus()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->viewBuilder()->layout('ajax');
        $pricing = $this->Pricing->get($this->request->query('id'));
        $data = [
            'manufacturer' => $pricing,
            'status' => $this->request->query('status'),
        ];

        $this->set($data);
    }

    public function getDelete()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->viewBuilder()->layout('ajax');
        $pricing = $this->Pricing->get($this->request->query('id'));
        $data = [
            'data' => $pricing,
        ];

        $this->set($data);
    }
}
