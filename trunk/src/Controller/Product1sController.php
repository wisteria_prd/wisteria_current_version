<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\Datasource\ConnectionManager;

class Product1sController extends AppController
{
    /**
     * This function is temporary not use due to auto complete change to ReportController under function product.
     * @deprecated since version phase 2
     * @return type
     */
    public function loadName()
    {
        if ($this->request->is('ajax')) {
            $this->autoRender = false;
            $this->response->disableCache();
            $this->response->type('json');
            $this->viewBuilder()->layout(false);

            $keyword = trim($this->request->query('keyword'));
            $query = $this->Product1s->find()
                ->hydrate(false)
                ->join([
                    'table' => 'sale1s',
                    'alias' => 'Sale1s',
                    'type' => 'LEFT',
                    'conditions' => 'Sale1s.product_code = Product1s.code',
                ])
                ->select(['Product1s.name', 'Product1s.manufacturer'])
                ->group(['Product1s.name'])
                ->order(['Product1s.name']);

            $results = $query->where(['Product1s.name LIKE ' => '%' . $keyword . '%'])
                ->orWhere(['Product1s.manufacturer LIKE ' => '%' . $keyword . '%'])
                ->orWhere(['Sale1s.product_name LIKE ' => '%' . $keyword . '%']);

            $this->response->body(json_encode($results->toArray()));
            return $this->response;
        }
    }

    public function upload()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->autoRander = false;
        if (isset($this->request->data['file'])) {
            $this->loadComponent('ExtractCsv');
            $query = $this->current_db();
            if ($query) {
                $fields = $query->toArray();
                $fields_name = array_keys($fields);

                $data = $this->ExtractCsv->executeExtract($this->request->data['file'], $fields_name);
                if (!empty($data)) {
                    $conn = ConnectionManager::get('default');
                    $conn->execute('TRUNCATE TABLE product1s');
                    $entities = $this->Product1s->newEntities($data);
                    $result = $this->Product1s->saveMany($entities);
                    if ($result) {
                        // save log
                        $this->loadModel('Logs');
                        $log_entity = $this->Logs->newEntity(['type' => ucfirst(TYPE_PRODUCT)]);
                        $this->Logs->save($log_entity);

                        echo json_encode(['success' => 1, 'message' => __('TXT_MESSAGE_CREATE')]);
                        exit();
                    }
                } else {
                    $response = '';
                    $message = $this->ExtractCsv->msg;
                    if ($message == 'not contain field name') {
                        $response = __('not contain field name');
                    } elseif ($message == 'contain too much field') {
                        $response = __('contain too much field');
                    } elseif ($message == 'cotain less field') {
                        $response = __('cotain less field');
                    } else {
                        $response = __('invalid field');
                    }
                    echo json_encode(['success' => 0, 'message' => $response]);
                    exit;
                }
            } else {
                echo json_encode(['success' => 0, 'message' => __('An unexpected SYSTEM error happened. Please retry CSV uploading or contact a system administrator.')]);
                exit;
            }
        }
        echo json_encode(['success' => 0, 'message' => __('TXT_MESSAGE_ERROR')]);
    }

    //do this way to get column name. currently cake document cannot get column with schema.
    protected function current_db()
    {
        $query = $this->Product1s->find();
        $fields = $query
            ->select()
            ->first();
        if (!empty($fields)) {
            return $fields;
        }
        $fake_data = [
            'code' => 'fake',
            'name' => 'fake',
            'manufacturer' => '1205',
            'sort' => 1,
            'not_display' => 1
        ];
        $entity = $this->Product1s->newEntity($fake_data);
        $entity = $this->Product1s->patchEntity($entity, $fake_data, ['validate' => false]);
        if ($this->Product1s->save($entity)) {
            $query = $this->Product1s->find();
            $fields = $query
                ->select()
                ->first();
            return $fields;
        }
        return false;
    }

    public function test()
    {
        $this->autoRender = false;
        // $this->Products->deleteAll();
        $conn = ConnectionManager::get('default');
        $conn->execute('TRUNCATE TABLE Product1s');
    }
}