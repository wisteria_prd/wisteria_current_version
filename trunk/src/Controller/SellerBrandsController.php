<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Network\Exception\NotFoundException;

class SellerBrandsController extends AppController
{
    private $locale;

    public function initialize() {
        parent::initialize();
        $this->locale = 'name' . $this->request->session()->read('tb_field');
    }

    public function index()
    {
        $this->loadModel('Manufacturers');
        $this->loadModel('Suppliers');
        $options = [];
        if ($this->request->query('brand_name')) {
            $options['OR']['ProductBrands.name LIKE '] = '%' . trim($this->request->query('brand_name')) . '%';
            $options['OR']['ProductBrands.name_en LIKE '] = '%' . trim($this->request->query('brand_name')) . '%';
        }
        if ($this->request->query('mf_name')) {
            $options['Suppliers.id'] = $this->request->query('mf_name');
        }
        if ($this->request->query('sp_name')) {
            $options['Manufacturers.id'] = $this->request->query('sp_name');
        }
        $display = PAGE_NUMBER;
        if (!empty($this->request->query('displays'))) {
            $display = $this->request->query('displays');
        }
        if ($this->request->query('inactive')) {
            $options['SellerBrands.is_suspend'] = 1;
        }
        $this->paginate = [
            'conditions' => $options,
            'limit' => $display,
            'contain' => [
                'ProductBrands',
                'Sellers' => ['Manufacturers', 'Suppliers'],
                'SellerProducts' => [
                    'joinType' => 'LEFT',
                    'Products',
                ],
            ],
            'sortWhitelist' => [
                'Manufacturers.name',
                'Manufacturers.name_en',
                'Suppliers.name',
                'Suppliers.name_en',
                'ProductBrands.name',
                'ProductBrands.name_en',
            ],
        ];
        $manufacturers = $this->Manufacturers->find()
            ->where(['Manufacturers.is_suspend' => 0])
            ->all();
        $suppliers = $this->Suppliers->find()
            ->where(['Suppliers.is_suspend' => 0])
            ->all();
        $data = [
            'locale' => $this->local,
            'data' => $this->paginate($this->SellerBrands),
            'paging' => $this->request->param('paging')['SellerBrands']['pageCount'],
            'manufacturers' => $manufacturers,
            'suppliers' => $suppliers,
        ];
        $this->set($data);
    }

    public function create()
    {
        $data = $this->SellerBrands->newEntity();
        $data1 = [
            'data' => $data,
            'locale' => $this->locale,
        ];
        $this->set($data1);
    }

    public function edit($id)
    {
        $data = $this->SellerBrands->findById($id)
            ->contain([
                'Sellers' => [
                    'Suppliers',
                    'Manufacturers',
                ],
                'ProductBrands',
            ])
            ->first();
        if (!$data) {
            throw new NotFoundException();
        }
        $this->loadModel('Manufacturers');
        $manufacturer = $this->Manufacturers->findById($data->product_brand->manufacturer_id)->first();
        $sellerBrands = $this->SellerBrands->find()
            ->where(['seller_id' => $data->seller_id])
            ->contain([
                'ProductBrands'
            ])
            ->all();
        $data1 = [
            'locale' => $this->locale,
            'data' => $data,
            'seller_brands' => $sellerBrands,
            'manufacturer' => $manufacturer,
        ];
        $this->set($data1);
    }

    public function view($id)
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->viewBuilder()->layout('ajax');
        $data = $this->SellerBrands->findById($id)
            ->contain([
                'ProductBrands',
                'Sellers' => ['Manufacturers', 'Suppliers'],
                'SellerProducts' => [
                    'joinType' => 'LEFT',
                    'Products',
                ],
            ])->first();
        $data1 = $this->SellerBrands->findBySellerId($data->seller_id)
            ->contain(['ProductBrands'])->toArray();
        $this->set(compact('data', 'data1'));
    }

    public function delete()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->viewBuilder()->layout('ajax');
        $this->autoRender = false;

        $data = $this->SellerBrands->get($this->request->data['id']);
        if ($this->SellerBrands->delete($data)) {
            echo json_encode([
                'status' => 1,
                'message' => 'success',
            ]);
        }
    }

    public function saveOrUpdate()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->viewBuilder()->layout('ajax');
        $this->autoRender = false;
        $this->response->type('json');
        // check exist record
        if ($this->request->data('id')) {
            $data = $this->SellerBrands->get($this->request->data['id']);
        } else {
            $data = $this->SellerBrands->newEntity();
        }
        $validator = $this->SellerBrands->patchEntity($data, $this->request->data);
        // check validation
        if ($validator->errors()) {
            $this->response->body(json_encode([
                'status' => 0,
                'message' => MSG_ERROR,
                'data' => $validator->errors(),
            ]));
            return $this->response;
        }
        if ($this->request->data('id')) {
            $this->SellerBrands->deleteAll([
                'seller_id' => $data->seller_id,
                'id <>' => $data->id,
            ]);
        }
        $entities = [];
        foreach ($this->request->data['product_brand_id'] as $key => $value) {
            $entities[] = [
                'seller_id' => $this->request->data['seller_id'],
                'product_brand_id' => $value,
            ];
        }
        $entities = $this->SellerBrands->newEntities($entities, [
            'validate' => false,
        ]);
        if ($this->SellerBrands->saveMany($entities)) {
            $this->response->body(json_encode([
                'status' => 1,
                'message' => MSG_SUCCESS,
            ]));
            return $this->response;
        }
    }

    public function updateSuspend()
    {
        if ($this->request->is('ajax')) {
            $this->autoRender = false;
            $this->response->disableCache();
            $this->response->type('json');
            $this->viewBuilder()->layout(false);

            $data = $this->SellerBrands->get($this->request->data['id']);
            $data->is_suspend = ($this->request->data['is_suspend'] == 0 ) ? 1 : 0;
            if ($this->SellerBrands->save($data)) {
                $this->response->body(json_encode([
                    'status' => 1,
                    'message' => 'success'
                ]));
                return $this->response;
            }

            $this->response->body(json_encode([
                'status' => 0,
                'message' => 'error'
            ]));
            return $this->response;
        }
    }

    public function getListOfSellerBrandsBySellerId()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->viewBuilder()->layout('ajax');
        $this->autoRender = false;
        $this->response->type('json');
        $keyword = trim($this->request->query('keyword'));
        $data = $this->SellerBrands->find()
            ->contain([
                'ProductBrands' => function ($q) use ($keyword) {
                    $options = [
                        'ProductBrands.is_suspend' => 0
                    ];
                    if ($keyword) {
                        $options['OR'] = [
                            'ProductBrands.name LIKE' => '%' . $keyword . '%',
                            'ProductBrands.name_en LIKE' => '%' . $keyword . '%',
                        ];
                    }
                    return $q->where($options);
                },
            ])
            ->where(['seller_id' => $this->request->query('seller_id')])
            ->limit(PAGE_LIMIT_NUMBER)
            ->all();
        $this->response->body(json_encode([
            'status' => '1',
            'message' => MSG_SUCCESS,
            'data' => $data,
        ]));
        return $this->response;
    }

    public function getProductBrandByManufacturerId()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->viewBuilder()->layout('ajax');
        $this->loadModel('ProductBrands');
        $product_brands = $this->ProductBrands->find()
            ->where(['is_suspend' => 0])
            ->where(['manufacturer_id' => $this->request->query('manufacturer_id')])
            ->limit(PAGE_LIMIT_NUMBER)
            ->all();
        $data = [
            'locale' => $this->locale,
            'data' => $product_brands,
        ];
        $this->set($data);
        $this->render('get_list_of_product_brands');
    }

    public function getListOfSellerBrands()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->viewBuilder()->layout('ajax');
        $this->autoRender = false;
        $this->response->type('json');
        $keyword = trim($this->request->query('keyword'));
        $data = $this->SellerBrands->find()
            ->contain([
                'Sellers' => [
                    'Manufacturers' => function ($q) use ($keyword) {
                        $options = [
                            'Manufacturers.is_suspend' => 0
                        ];
                        if ($keyword) {
                            $options['OR'] = [
                                'Manufacturers.name LIKE' => '%' . $keyword . '%',
                                'Manufacturers.name_en LIKE' => '%' . $keyword . '%',
                            ];
                        }
                        return $q->where($options);
                    },
                    'Suppliers' => function ($q) use ($keyword) {
                        $options = [
                            'Suppliers.is_suspend' => 0
                        ];
                        if ($keyword) {
                            $options['OR'] = [
                                'Suppliers.name LIKE' => '%' . $keyword . '%',
                                'Suppliers.name_en LIKE' => '%' . $keyword . '%',
                            ];
                        }
                        return $q->where($options);
                    },
                ],
            ])
            ->group('SellerBrands.seller_id')
            ->limit(PAGE_LIMIT_NUMBER)
            ->all();
        $this->response->body(json_encode([
            'status' => '1',
            'message' => MSG_SUCCESS,
            'data' => $data,
        ]));
        return $this->response;
    }
}
