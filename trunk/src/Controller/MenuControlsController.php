<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Network\Exception\NotFoundException;

class MenuControlsController extends AppController
{
    public function updateMenu()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->viewBuilder()->layout('ajax');
        $this->autoRender = false;
        $this->response->disableCache();
        $this->response->type('json');
        $data = $this->request->data;
        $this->set('_serialize', [$data]);
        // Checking data is empty
        if ($data['data']) {
            // Delete all records in menu_controls
            $this->MenuControls->deleteAll([
                'user_group_id' => $data['user_group_id'],
            ]);
            foreach ($data['data'] as $key => $value) {
                $menu = [
                    'menu_id' => $value['menuId'],
                    'user_group_id' => $data['user_group_id'],
                    'menu_order' => ($key + 1),
                ];
                $entity = $this->MenuControls->newEntity($menu);
                $result = $this->MenuControls->save($entity);
                // Check sub-menu
                if (isset($value['children'])) {
                    $this->saveSubMenu($result, $value['children']);
                }
            }
            $this->response->body(json_encode([
                'status' => 1,
                'message' => MSG_SUCCESS,
            ]));

            return $this->response;
        }
    }

    public function delete()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->viewBuilder()->layout('ajax');
        $this->autoRender = false;
        $this->response->disableCache();
        $menuControl = $this->MenuControls->findById($this->request->data['id'])->first();
        if ($this->MenuControls->delete($menuControl)) {
            $this->response->body(json_encode([
                'status' => 1,
                'message' => MSG_SUCCESS,
            ]));
            return $this->response;
        }
    }

    private function saveSubMenu($parent, $data)
    {
        if (!$data) {
            return false;
        }
        foreach ($data as $key => $value) {
            $menu = [
                'user_group_id' => $parent->user_group_id,
                'parent_id' => $parent->id,
                'menu_id' => $value['menuId'],
                'menu_order' => $parent->menu_order . ($key + 1),
            ];
            $entity = $this->MenuControls->newEntity($menu);
            $result = $this->MenuControls->save($entity);
            // Check sub-menu
            if (isset($value['children'])) {
//                $list = $value['children'];
                $this->saveSubMenu($result, $value['children']);
            }
        }
    }
}
