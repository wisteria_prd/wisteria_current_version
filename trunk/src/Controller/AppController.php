<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Controller\Controller;
use Cake\Event\Event;
use Cake\ORM\TableRegistry;
use Cake\I18n\I18n;
use Cake\View\Helper\SessionHelper;

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @link http://book.cakephp.org/3.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller
{
    protected $user_data;

    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('RequestHandler');
        $this->loadComponent('Flash');
        $this->loadComponent('Paginator');
        $this->loadComponent('Auth', [
            'loginAction' => [
                'controller' => 'users',
                'action' => 'login'
            ],
            'loginRedirect' => [
                'controller' => 'home',
                'action' => 'index'
            ],
            'logoutRedirect' => [
                'controller' => 'users',
                'action' => 'login'
            ],
            'authenticate' => [
                'Form' => [
                    'fields' => [
                        'username' => 'email',
                        'password' => 'password'
                    ],
                    'scope' => ['status' => STATUS_ACTIVE]
                ]
            ],
            'unauthorizedRedirect' => false,
        ]);

        // I18n::locale('en_US');

        $this->loadComponent('CustomConstant');
        $this->loadComponent('Common');
        $this->loadComponent('AccessClient');
        $this->Auth->allow(['activation']);
    }

    public function isAuthorized($user = null)
    {
        // Any registered user can access public functions
        if (empty($this->request->params['admin'])) {
            return true;
        }

        // Only admins can access admin functions
        if (isset($this->request->params['admin'])) {
            return (bool)($user['role'] === 'admin');
        }

        // Default deny
        return false;
    }

    public function beforeRender(Event $event)
    {
        if (!array_key_exists('_serialize', $this->viewVars) &&
            in_array($this->response->type(), ['application/json', 'application/xml'])
        ) {
            $this->set('_serialize', true);
        }
    }

    public function beforeFilter(Event $event)
    {
        if ($this->request->session()->read('Auth.User')) {
            $session = $this->request->session();
            $this->loadModel('UserGroups');
            $this->loadModel('MenuControls');
            $this->loadModel('Breadcrumbs');

            $user_group_id = $session->read('Auth.User.user_group_id');
            $breadcrumbs = $this->Breadcrumbs->getListByControllerAndAction($this->request->controller, $this->request->action, $this->request->session()->read('Auth.User.user_group_id'), $this->request);
            // pr($breadcrumbs);
            $user_group = $this->UserGroups->getUserGroupById($user_group_id);
            // Checking user_group
            if ($user_group) {
                $session->write('user_group', $user_group);
            }
            // Switch language base on affiliation_class of user
            if ($user_group->affiliation_class === USER_CLASS_MEDIPRO) {
                I18n::locale('en_US');
                $session->write('tb_field', '_en');
            } else {
                $session->write('tb_field', '');
            }
            $menuControls = $this->MenuControls->findByUserGroupId($user_group_id)
                    ->contain(['Menus'])
                    ->all();
            $data = [
                'menuControls' => $menuControls,
                'user_groups' => $user_group,
                'breadcrumbs' => $breadcrumbs,
                // 'en' => $this->request->session()->read('tb_field'),
                'locale' => $this->request->session()->read('tb_field'),
            ];

            // Checking user access
            // $moduleControl = $this->AccessClient->setModule($this->request->controller);
            // $moduleAction = $this->AccessClient->setAction($this->request->action);
            // if (!$this->AccessClient->checkAccessControl()) {
            //     $this->set($data);
            //     if ($this->request->is('ajax')) {
            //         $this->viewBuilder()->layout('ajax');
            //         $this->render('/pages/ajax_deny');
            //     } else {
            //         $this->render('/pages/deny');
            //     }
            // }

            $this->set($data);
        }
    }
}
