<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Network\Exception\NotFoundException;

class FocsController extends AppController
{
    public $locale;

    public function initialize()
    {
        parent::initialize();
        $this->locale = $this->request->session()->read('tb_field');
    }

    public function index()
    {
        $this->loadModel('Sellers');
        $options = [];
        if ($this->request->query('seller_id')) {
            $options['Focs.seller_id'] = $this->request->query('seller_id');
        }
        $display = PAGE_NUMBER;
        if (!empty($this->request->query('displays'))) {
            $display = $this->request->query('displays');
        }
        if ($this->request->query('inactive')) {
            $options[] = [
                'Focs.is_suspend' => 1,
            ];
        }
        //TODO: confirm with Nik san, no assciation with product_brand and product
        if ($this->request->query('keyword')) {
            /* $options[]['OR'] = [
                'ProductBrands.name LIKE ' => '%'.$keyword.'%',
                'ProductBrands.name_en LIKE ' => '%'.$keyword.'%',
                'Products.name LIKE ' => '%'.$keyword.'%',
                'Products.name_en LIKE ' => '%'.$keyword.'%',
            ]; */
        }
        $this->paginate = [
            'conditions' => $options,
            'contain' => [
                'Currencies',
                'FocRanges',
                'Sellers' => [
                    'joinType' => 'LEFT',
                    'Manufacturers' => function ($q) {
                        return $q->where(['Manufacturers.is_suspend' => 0]);
                    },
                    'Suppliers' => function ($q) {
                        return $q->where(['Suppliers.is_suspend' => 0]);
                    },
                ]
            ],
            'limit' => $display,
        ];
        $data = [
            'locale' => $this->locale,
            'data' => $this->paginate($this->Focs),
            'paging' => $this->request->param('paging')['Focs']['pageCount'],
            'sellers' => $this->Sellers->sellerDropdownAsSupplier(),
        ];
        $this->set($data);
    }

    public function focValue()
    {
        if ($this->request->is('ajax')) {
            $foc_for = $this->request->query('foc_for');
            $en = $this->request->session()->read('tb_field');
            $seller_id = $this->request->query('seller_id');

            $data = [];

            if ($foc_for == TYPE_BRAND) {
                $this->loadModel('ProductBrands');
                $data = $this->ProductBrands->find('all')
                    ->join([
                        'SellerBrands' => [
                            'table' => 'seller_brands',
                            'type' => 'INNER',
                            'conditions' => [
                                'ProductBrands.id = SellerBrands.product_brand_id',
                                'SellerBrands.is_suspend = 0'
                            ],
                        ],
                        'Sellers' => [
                            'table' => 'sellers',
                            'type' => 'INNER',
                            'conditions' => 'Sellers.id = SellerBrands.seller_id',
                        ]
                    ])
                    ->where([
                        'Sellers.id' => $seller_id,
                        'ProductBrands.is_suspend' => 0
                    ]);

            } else if ($foc_for == TYPE_PRODUCT) {
                $this->loadModel('SellerBrands');

                $data = $this->SellerBrands->find('all')
                    ->contain([
                        'SellerProducts' => [
                            'Products' => [
                                'ProductDetails' => [
                                    'PackSizes',
                                    'SingleUnits'
                                ]
                            ]
                        ]
                    ])
                    ->where([
                        'SellerBrands.seller_id' => $seller_id,
                        'SellerBrands.is_suspend' => 0
                    ])
                    ->group([
                        'SellerBrands.id'
                    ]);
            }

            $this->set(compact('data', 'en', 'foc_for'));
            $this->set('_serialize', ['data', 'en', 'foc_for']);
        }
        $this->viewBuilder()->layout('ajax');
    }

    public function focRange()
    {
        if ($this->request->is('ajax')) {
            $this->loadModel('FocRanges');
            $foc_id = $this->request->query('foc_id');
            $foc_for = $this->request->query('foc_for');
            $seller_name = $this->request->query('seller_name');
            $range = $this->FocRanges->find()
                ->contain([
                    'FocDiscounts' => function ($q) {
                        return $q->where([
                            'FocDiscounts.type' => TYPE_FOC_RANGE
                        ]);
                    }
                ])
                ->where([
                    'foc_id' => $foc_id
                ]);

            $this->set(compact('range', 'foc_id', 'seller_name', 'foc_for'));
            $this->set('_serialize', ['range', 'foc_id', 'seller_name', 'foc_for']);
        }

        $this->viewBuilder()->layout('ajax');
    }

    public function create()
    {
        $foc = $this->Focs->newEntity();
        if ($this->request->is('ajax')) {
            $this->autoRender = false;
            $this->response->disableCache();
            $this->response->type('json');
            $this->viewBuilder()->layout(false);

            $foc = $this->Focs->patchEntity($foc, $this->request->data);
            if ($this->Focs->save($foc)) {
                $this->response->body(json_encode(['status' => 1]));
                return $this->response;
            }

            $this->response->body(json_encode(['status' => 0, 'message' => $foc->errors()]));
            return $this->response;
        }
        $this->loadModel('Currencies');
        $currencies = $this->Currencies->getDropdown();
        $this->loadModel('Sellers');
        $sellers = $this->Sellers->sellerDropdownAsSupplier();

        $for_foc = $this->CustomConstant->focSelectBox();
        $foc_range = $this->CustomConstant->focRangeType();
        $en = $this->request->session()->read('tb_field');

        $txt_register = $this->Common->txtRegister();
        $this->set(compact('en', 'sellers', 'foc', 'currencies', 'for_foc', 'foc_range', 'txt_register'));
        $this->set('_serialize', ['en', 'sellers', 'foc', 'currencies', 'for_foc', 'foc_range']);
    }

    public function edit($id = null)
    {
        $foc = $this->Focs->get($id, ['conditions' => ['is_suspend' => 0]]);

        if ($this->request->is('ajax')) {
            $this->autoRender = false;
            $this->response->disableCache();
            $this->response->type('json');
            $this->viewBuilder()->layout(false);

            $foc = $this->Focs->patchEntity($foc, $this->request->data);
            if ($this->Focs->save($foc)) {
                $this->response->body(json_encode(['status' => 1]));
                return $this->response;
            }

            $this->response->body(json_encode(['status' => 0, 'message' => $foc->errors()]));
            return $this->response;
        }

        $this->loadModel('Currencies');
        $currencies = $this->Currencies->getDropdown();
        $this->loadModel('Sellers');
        $sellers = $this->Sellers->sellerDropdownAsSupplier();

        $for_foc = $this->CustomConstant->focSelectBox();
        $foc_range = $this->CustomConstant->focRangeType();
        $en = $this->request->session()->read('tb_field');

        $txt_edit = $this->Common->txtEdit();
        $this->set(compact('en', 'sellers', 'foc', 'currencies', 'for_foc', 'foc_range', 'txt_edit'));
        $this->set('_serialize', ['en', 'sellers', 'foc', 'currencies', 'for_foc', 'foc_range']);
    }

    public function delete()
    {
        if ($this->request->is('ajax')) {
            $this->autoRender = false;
            $this->response->disableCache();
            $this->response->type('json');
            $this->viewBuilder()->layout(false);

            $this->request->allowMethod(['post', 'delete']);
            $discount = $this->Focs->get($this->request->data['id']);
            if ($this->Focs->delete($discount)) {
                $this->response->body(json_encode(['status' => 1]));
                return $this->response;
            }
            $this->response->body(json_encode(['status' => 0, 'message' => __('TXT_DELETE_TROUBLE')]));
            return $this->response;
        }
    }

    public function updateSuspend()
    {
        if ($this->request->is('ajax')) {
            $this->autoRender = false;
            $this->response->disableCache();
            $this->response->type('json');
            $this->viewBuilder()->layout(false);

            $data = $this->Focs->get($this->request->data['id']);
            $data->is_suspend = ($this->request->data['is_suspend'] == 0 ) ? 1 : 0;
            if ($this->Focs->save($data)) {
                $this->response->body(json_encode([
                    'status' => 1,
                    'message' => 'success'
                ]));
                return $this->response;
            }

            $this->response->body(json_encode([
                'status' => 0,
                'message' => 'error'
            ]));
            return $this->response;
        }
    }
}
