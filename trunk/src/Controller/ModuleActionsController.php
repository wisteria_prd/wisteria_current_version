<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Network\Exception\NotFoundException;

class ModuleActionsController extends AppController
{
    /**
    * @var string for store local session field
    */
    private $local;

    public function initialize() {
        parent::initialize();
    }

    public function index()
    {
        
    }

    public function create()
    {
        
    }

    public function delete()
    {
        
    }

    public function update()
    {
        
    }
}