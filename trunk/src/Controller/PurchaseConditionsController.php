<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Network\Exception\NotFoundException;

class PurchaseConditionsController extends AppController
{
    private $locale;

    public function initialize()
    {
        parent::initialize();
        $this->locale = $this->request->session()->read('tb_field');
        $this->loadComponent('Common');
    }

    public function brand()
    {
        $keyword = $this->request->query('keyword');
        $supplier = $this->request->query('supplier');
        $conditions = [];
        $conditions[] = [
            'PurchaseConditions.type' => TYPE_SELLER_BRAND
        ];
        $displays = 10;
        if ($this->request->query('displays')) {
            $displays = $this->request->query('displays');
        }
        if ($this->request->query('inactive')) {
            $conditions[] = [
                'PurchaseConditions.is_suspend' => 1,
            ];
        }
        if ($this->request->query('keyword')) {
            $conditions[]['OR'] = [
                'Suppliers.name LIKE ' => '%'.$keyword.'%',
                'Suppliers.name_en LIKE ' => '%'.$keyword.'%',
                'Manufacturers.name LIKE ' => '%'.$keyword.'%',
                'Manufacturers.name_en LIKE ' => '%'.$keyword.'%',
                'ProductBrands.name LIKE ' => '%'.$keyword.'%',
                'ProductBrands.name_en LIKE ' => '%'.$keyword.'%',
            ];
        }
        $this->paginate = [
            'conditions' => $conditions,
            'limit' => $displays,
            'contain' => [
                'Currencies',
                'SellerBrands' => [
                    'joinType' => 'LEFT',
                    'ProductBrands',
                    'Sellers' => [
                        'joinType' => 'LEFT',
                        'Manufacturers' => function ($q) use($supplier) {
                            if ($supplier) {
                                $supplier = explode(' ', trim($supplier));
                                $con = [];
                                $conditions = [];
                                foreach ($supplier as $k => $v) {
                                    $con[]['Manufacturers.name LIKE '] = '%' . $v . '%';
                                    $con[]['Manufacturers.name_en LIKE '] = '%' . $v . '%';
                                }
                                $conditions['OR'] = $con;
                                $q->where($conditions);
                            }
                            return $q;
                        },
                        'Suppliers' => function ($q) use($supplier) {
                            if ($supplier) {
                                $supplier = explode(' ', trim($supplier));
                                $con = [];
                                $conditions = [];
                                foreach ($supplier as $k => $v) {
                                    $con[]['Suppliers.name LIKE '] = '%' . $v . '%';
                                    $con[]['Suppliers.name_en LIKE '] = '%' . $v . '%';
                                }
                                $conditions['OR'] = $con;
                                $q->where($conditions);
                            }
                            return $q;
                        },
                    ],
                    'SellerProducts' => [
                        'queryBuilder' => function($q) {
                            return $q->group(['seller_brand_id']);
                        },
                        'Products',
                    ],
                ],
            ],
            'sortWhitelist' => [
                'Manufacturers.name_en',
                'Manufacturers.name',
                'Suppliers.name',
                'Suppliers.name_en',
                'ProductBrands.name_en',
                'ProductBrands.name',
                'PurchaseConditions.type',
                'PurchaseConditions.external_id',
                'Products.code',
                'min_amount',
                'min_quantity',
            ],
        ];
        $data = [
            'data' => $this->paginate($this->PurchaseConditions),
            'paging' => $this->request->param('paging')['PurchaseConditions']['pageCount'],
            'locale' => $this->locale,
            'displays' => $displays,
        ];
        $this->set($data);
    }

    public function product()
    {
        $keyword = $this->request->query('keyword');
        $supplier = $this->request->query('supplier');
        $displays = 10;
        $conditions = [];
        $conditions[] = [
            'PurchaseConditions.type' => TYPE_SELLER_PRODUCT,
        ];
        if ($this->request->query('displays')) {
            $displays = $this->request->query('displays');
        }
        if ($this->request->query('inactive')) {
            $conditions[] = [
                'PurchaseConditions.is_suspend' => 1,
            ];
        }
        //TODO: confirm with Nik san, no assciation with products and manufacturer
        if ($this->request->query('keyword')) {
            $conditions[]['OR'] = [
                'Suppliers.name LIKE ' => '%'.$keyword.'%',
                'Suppliers.name_en LIKE ' => '%'.$keyword.'%',
                'Manufacturers.name LIKE ' => '%'.$keyword.'%',
                'Manufacturers.name_en LIKE ' => '%'.$keyword.'%',
                //'ProductBrands.name LIKE ' => '%'.$keyword.'%',
                //'ProductBrands.name_en LIKE ' => '%'.$keyword.'%',
                //'Products.name LIKE ' => '%'.$keyword.'%',
                //'Products.name_en LIKE ' => '%'.$keyword.'%',
            ];
        }
        $this->paginate = [
            'conditions' => $conditions,
            'limit' => $displays,
            'contain' => [
                'Currencies',
                'SellerProducts' => [
                    'joinType' => 'LEFT',
                    'SellerBrands' => [
                        'joinType' => 'LEFT',
                        'Sellers' => [
                            'joinType' => 'LEFT',
                            'Manufacturers' => function ($q) use($supplier) {
                                if ($supplier) {
                                    $supplier = explode(' ', trim($supplier));
                                    $con = [];
                                    $conditions = [];
                                    foreach ($supplier as $k => $v) {
                                        $con[]['Manufacturers.name LIKE '] = '%' . $v . '%';
                                        $con[]['Manufacturers.name_en LIKE '] = '%' . $v . '%';
                                    }
                                    $conditions['OR'] = $con;
                                    $q->where($conditions);
                                }
                                return $q;
                            },
                            'Suppliers' => function ($q) use($supplier) {
                                if ($supplier) {
                                    $supplier = explode(' ', trim($supplier));
                                    $con = [];
                                    $conditions = [];
                                    foreach ($supplier as $k => $v) {
                                        $con[]['Suppliers.name LIKE '] = '%' . $v . '%';
                                        $con[]['Suppliers.name_en LIKE '] = '%' . $v . '%';
                                    }
                                    $conditions['OR'] = $con;
                                    $q->where($conditions);
                                }
                                return $q;
                            },
                        ],
                    ],
                    'ProductDetails' => [
                        'joinType' => 'LEFT',
                        'Products' => [
                            'ProductBrands' => [
                                'MF',
                            ],
                        ],
                    ],
                ],
            ],
            'sortWhitelist' => [
                'MF.name_en',
                'MF.name',
                'Suppliers.name',
                'Suppliers.name_en',
                'PurchaseConditions.min_amount',
                'PurchaseConditions.min_quantity',
                'ProductDetails.type',
                'PurchaseConditions.external_id',
                'ProudctBrands.name',
                'ProductBrands.name_en',
                'Products.name',
                'Products.name_en',
            ],
        ];
        $data = [
            'data' => $this->paginate($this->PurchaseConditions),
            'locale' => $this->locale,
            'paging' => $this->request->param('paging')['PurchaseConditions']['pageCount'],
            'displays' => $displays,
        ];
        $this->set($data);
    }

    public function createConditionBrand()
    {
        $this->loadModel('Currencies');
        $data = $this->PurchaseConditions->newEntity();
        $currencies = $this->Currencies->getList($this->locale);
        $data1 = [
            'currencies' => $currencies,
            'locale' => $this->locale,
            'data' => $data,
        ];
        $this->set($data1);
    }

    public function editBrand($id = null)
    {
        $data = $this->PurchaseConditions->findById($id)
            ->contain([
                'Currencies',
                'SellerBrands' => [
                    'Sellers' => [
                        'Manufacturers',
                        'Suppliers',
                    ],
                    'ProductBrands',
                ],
            ])
            ->first();
        if (!$data) {
            throw new NotFoundException();
        }
        $this->loadModel('Currencies');
        $currencies = $this->Currencies->getList($this->locale);
        $data1 = [
            'data' => $data,
            'locale' => $this->locale,
            'currencies' => $currencies,
        ];
        $this->set($data1);
    }

    public function createConditionProduct()
    {
        $this->loadModel('Currencies');
        $data = $this->PurchaseConditions->newEntity();
        $currencies = $this->Currencies->getList($this->locale);
        $data1 = [
            'currencies' => $currencies,
            'locale' => $this->locale,
            'data' => $data,
        ];
        $this->set($data1);
    }

    public function editProduct($id = null)
    {
        $data = $this->PurchaseConditions->findById($id)
            ->contain([
                'Currencies',
                'SellerProducts' => [
                    'ProductDetails',
                    'SellerBrands' => [
                        'ProductBrands',
                        'Sellers' => [
                            'Manufacturers',
                            'Suppliers',
                        ]
                    ],
                ],
            ])
            ->first();
        if (!$data) {
            throw new NotFoundException();
        }
        $this->loadModel('Currencies');
        $currencies = $this->Currencies->getList($this->locale);
        $data1 = [
            'data' => $data,
            'locale' => $this->locale,
            'currencies' => $currencies,
        ];
        $this->set($data1);
    }

    public function saveOrUpdate()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->viewBuilder()->layout('ajax');
        $this->autoRender = false;
        $this->response->type('json');
        // check exist record
        if ($this->request->data('id')) {
            $data = $this->PurchaseConditions->get($this->request->data['id']);
        } else {
            $data = $this->PurchaseConditions->newEntity();
        }
        $validator = $this->validate($data, $this->request->data);
        // Check validation
        if ($validator->errors()) {
            $this->response->body(json_encode([
                'status' => 0,
                'message' => MSG_ERROR,
                'data' => $validator->errors(),
            ]));
            return $this->response;
        }
        $data->is_moq = $this->request->data('is_moq') ? 1 : 0;
        $data->is_moa = $this->request->data('is_moa') ? 1 : 0;
        $data->external_id = $this->getExternalIdByType($data);
        if ($this->PurchaseConditions->save($data)) {
            $this->response->body(json_encode([
                'status' => 1,
                'message' => MSG_SUCCESS,
            ]));
            return $this->response;
        }
    }

    public function getMfAndSupplierBySearch()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->viewBuilder()->layout('ajax');
        $this->autoRender = false;
        $this->response->type('json');
        $this->loadModel('SellerBrands');
        $keyword = $this->request->query('keyword');
        $sellerBrands = $this->SellerBrands->find()
            ->where(['SellerBrands.is_suspend' => 0])
            ->contain([
                'Sellers' => [
                    'Manufacturers' => function ($q) use ($keyword) {
                        if ($keyword) {
                            $q->where([
                                'AND' => [
                                    'Manufacturers.is_suspend' => 0,
                                    'OR' => [
                                        'Manufacturers.name LIKE' => '%' . trim($keyword) . '%',
                                        'Manufacturers.name_en LIKE' => '%' . trim($keyword) . '%',
                                    ]
                                ]
                            ]);
                        }
                        return $q;
                    },
                    'Suppliers' => function ($q) use ($keyword) {
                        if ($keyword) {
                            $q->where([
                                'AND' => [
                                    'Suppliers.is_suspend' => 0,
                                    'OR' => [
                                        'Suppliers.name LIKE' => '%' . trim($keyword) . '%',
                                        'Suppliers.name_en LIKE' => '%' . trim($keyword) . '%',
                                    ]
                                ]
                            ]);
                        }
                        return $q;
                    },
                ],
            ])
            ->limit(20)
            ->group('seller_id')
            ->all();
        $this->response->body(json_encode([
            'status' => '1',
            'message' => MSG_SUCCESS,
            'data' => $sellerBrands,
        ]));
        return $this->response;
    }

    public function getStatus()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->viewBuilder()->layout('ajax');
        $purchaseCondition = $this->PurchaseConditions->get($this->request->query('id'));
        $data = [
            'data' => $purchaseCondition,
            'status' => $this->request->query('status'),
        ];
        $this->set($data);
    }

    public function changeOfStatus()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->viewBuilder()->layout('ajax');
        $this->autoRender = false;
        $this->response->disableCache();
        $this->response->type('json');
        $data = $this->PurchaseConditions->get($this->request->data['id']);
        $data->is_suspend = $this->request->data['status'];
        if ($this->PurchaseConditions->save($data)) {
            $this->response->body(json_encode([
                'status' => 1,
                'message' => MSG_SUCCESS,
            ]));
            return $this->response;
        }
    }

    public function delete()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->viewBuilder()->layout('ajax');
        $this->autoRender = false;
        $this->response->type('json');
        $this->response->disableCache();
        $data = $this->PurchaseConditions->get($this->request->data['id']);
        if ($this->PurchaseConditions->delete($data)) {
            $this->response->body(json_encode([
                'status' => 1,
                'message' => MSG_SUCCESS,
            ]));
            return $this->response;
        }
    }

    public function getDelete()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->viewBuilder()->layout('ajax');
        $data1 = $this->PurchaseConditions->get($this->request->query('id'));
        $data = [
            'data' => $data1,
        ];
        $this->set($data);
    }

    private function validate($data, $request)
    {
        $validate = [];
        switch ($request['type']) {
            case TYPE_SELLER_BRAND:
                $validate = $this->PurchaseConditions->patchEntity($data, $request, [
                    'validate' => 'ProductBrand',
                ]);
                break;

            default:
                $validate = $this->PurchaseConditions->patchEntity($data, $request, [
                    'validate' => 'Product',
                ]);
        }
        return $validate;
    }

    private function getExternalIdByType($data)
    {
        $this->loadModel('SellerBrands');
        switch ($data->type) {
            case TYPE_SELLER_BRAND:
                $sellerBrand = $this->SellerBrands->find()
                    ->where(['seller_id' => $data->seller_id])
                    ->where(['product_brand_id' => $data->product_brand_id])
                    ->first();
                return $sellerBrand->id;
                break;

            default:
                $sellerBrand = $this->SellerBrands->find()
                    ->where(['seller_id' => $data->seller_id])
                    ->where(['product_brand_id' => $data->product_brand_id])
                    ->contain([
                        'SellerProducts' => [
                            'ProductDetails' => function ($q) use ($data) {
                                return $q->where([
                                    'ProductDetails.is_suspend' => 0,
                                    'ProductDetails.id' => $data->product_detail_id,
                                ]);
                            }
                        ],
                    ])
                    ->first();
                return $sellerBrand->seller_products[0]->id;
        }
    }
}
