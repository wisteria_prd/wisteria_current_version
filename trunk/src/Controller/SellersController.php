<?php
namespace App\Controller;

use App\Controller\AppController;

class SellersController extends AppController
{
    public $locale;

    public function initialize() {
        parent::initialize();
        $this->locale = $this->request->session()->read('tb_field');
    }

    public function supplierDropdown()
    {
        if ($this->request->is('ajax')) {
            $en = $this->request->session()->read('tb_field');
            $manufacturer_id = $this->request->query('manufacturer_id');
            $suppliers = $this->Sellers->find()
                ->contain([
                    'Suppliers' => function ($q) {
                        return $q->where([
                            'Suppliers.is_suspend' => 0
                        ]);
                    }
                ])
                ->where([
                    'Sellers.manufacturer_id' => $manufacturer_id
                ]);
            $this->set(compact('suppliers', 'en'));
            $this->set('_serialize', ['suppliers', 'en']);
        }
        $this->viewBuilder()->layout('ajax');
    }

    public function getListofSellersByAjax()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->viewBuilder()->layout('ajax');
        $this->autoRender = false;
        $this->response->type('json');
        $this->response->disableCache();
        $keyword = trim($this->request->query('keyword'));
        $data = $this->Sellers->find()
            ->contain([
                'Suppliers' => function ($q) use ($keyword) {
                    $options = [
                        'Suppliers.is_suspend' => 0
                    ];
                    if ($keyword) {
                        $options['OR'] = [
                            'Suppliers.name LIKE' => '%' . $keyword . '%',
                            'Suppliers.name_en LIKE' => '%' . $keyword . '%',
                        ];
                    }
                    return $q->where($options);
                },
                'Manufacturers' => function ($q) use ($keyword) {
                    $options = [
                        'Manufacturers.is_suspend' => 0
                    ];
                    if ($keyword) {
                        $options['OR'] = [
                            'Manufacturers.name LIKE' => '%' . $keyword . '%',
                            'Manufacturers.name_en LIKE' => '%' . $keyword . '%',
                        ];
                    }
                    return $q->where($options);
                },
            ]);
        if ($this->request->query('kind')) {
            $data = $data->where(['kind' => $this->request->query('kind')]);
        }
        $data = $data->limit(PAGE_LIMIT_NUMBER)
            ->all();
        $this->response->body(json_encode([
            'status' => 1,
            'message' => MSG_SUCCESS,
            'data' => $data,
        ]));
        return $this->response;
    }
}
