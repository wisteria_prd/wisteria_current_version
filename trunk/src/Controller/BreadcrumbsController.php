<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Network\Exception\NotFoundException;

class BreadcrumbsController extends AppController
{
    public $locale;
    public $user_group_id;

    public function initialize()
    {
        parent::initialize();
        $this->locale = 'name' . $this->request->session()->read('tb_field');
        $this->user_group_id = $this->request->session()->read('Auth.User.user_group_id');
    }

    public function index()
    {
        $data = $this->Breadcrumbs->find()->order([
            'id' => 'desc',
            ])->all();
        $this->set(['data' => $data]);
    }

    public function create()
    {
        $entity = $this->Breadcrumbs->newEntity();
        if ($this->request->is('post')) {
            $breadcrumb = $this->Breadcrumbs->patchEntity($entity, $this->request->data);
            // validate
            if ($this->Breadcrumbs->save($breadcrumb)) {
                return $this->redirect('/breadcrumbs');
            }
        }
        $this->set('breadcrumb', $entity);
    }

    public function edit($id = null)
    {
        $data = $this->Breadcrumbs->get($id);
        if (!$data) {
            throw new NotFoundException();
        }
        if ($this->request->is('put')) {
            $breadcrumb = $this->Breadcrumbs->patchEntity($data, $this->request->data);
            // ignore validation
            if ($this->Breadcrumbs->save($breadcrumb)) {
                return $this->redirect('/breadcrumbs');
            }
        }
        $this->set('breadcrumb', $data);
    }

    public function delete($id = null)
    {
        $breadcrumb = $this->Breadcrumbs->get($id);
        $this->Breadcrumbs->delete($breadcrumb);
        return $this->redirect('/breadcrumbs');
    }

    public function update()
    {
        $data = $this->Breadcrumbs->find()
            ->where(['user_group_id' => $this->user_group_id])
            // ->where(['controller <>' => ''])
            // ->order(['controller' => 'asc'])
            ->all();
        $data1 = [
            'data' => $data,
            'locale' => $this->locale,
            'user_group_id' => $this->user_group_id,
        ];
        $this->set($data1);
    }

    public function saveOrUpdate()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->autoRender = false;
        $this->viewBuilder()->layout('ajax');
        $this->response->type('json');
        // To Do
        $this->response->body(json_encode([]));
        return $this->response;
    }

    public function getAllActionByController()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->autoRender = false;
        $this->viewBuilder()->layout('ajax');
        $this->response->type('json');
        $data = $this->Breadcrumbs->getAllActionByControllerName($this->request->query('controller'), $this->user_group_id);
        $this->response->body(json_encode([
            'data' => $data,
            'status' => 1,
            'message' => MSG_SUCCESS,
        ]));
        return $this->response;
    }
}
