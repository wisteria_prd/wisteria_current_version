<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Network\Exception\NotFoundException;
use Cake\Collection\Collection;

class SaleWStocksController extends AppController
{
    private $local;

    public function initialize()
    {
        parent::initialize();
        $this->loadModel('Sales');
        $this->loadComponent('Image');
        $this->loadComponent('CustomConstant');
        $this->loadComponent('ReconstructData');
        $this->loadComponent('Function');
        $this->local = $this->request->session()->read('tb_field');
    }

    public function index()
    {
        $options = [];
        if ($this->request->query('keyword')) {
            $keyword = trim($this->request->query('keyword'));
            $options['AND']['OR'] = [
                'Sales.sale_number LIKE' => '%' . $keyword . '%',
                'Sales.rop_number LIKE' => '%' . $keyword . '%',
            ];
        }
        $options['AND']['Sales.type'] = TYPE_WISTERIA_STOCK;
        $displays = PAGE_NUMBER;
        if ($this->request->query('displays')) {
            $displays = $this->request->query('displays');
        }
        $this->paginate = [
            'conditions' => $options,
            'limit' => $displays,
            'contain' => [
                'Customers',
                'Currencies',
                'Sellers' => [
                    'Suppliers',
                    'Manufacturers',
                ],
                'SaleDetails' => function ($q) {
                    $q->select([
                        'id',
                        'sale_id',
                        'total_amount' => $q->func()->sum('unit_price * quantity'),
                    ])->group('sale_id');
                    return $q;
                },
            ],
            'sortWhitelist' => [
                'status',
                'rop_number',
                'sale_number',
                'Customers.name',
                'Customers.name_en',
                'Sellers.id',
            ],
        ];
        $sales = $this->paginate($this->Sales);
        $data = [
            'sales' => $sales,
            'local' => $this->local
        ];
        $this->set($data);
    }

    public function registerSale()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->viewBuilder()->layout('ajax');
        $this->response->type('json');
        $this->autoRender = false;
        $data = $this->Sales->newEntity();
        $request = [];
        parse_str($this->request->data['data'], $request);
        $validator = $this->Sales->patchEntity($data, $request);
        // check validation
        if ($validator->errors()) {
            $this->response->body(json_encode([
                'status' => 0,
                'message' => MSG_ERROR,
                'data' => $validator->errors(),
            ]));
            return $this->response;
        }
        $this->loadModel('Sellers');
        $this->loadModel('Customers');
        // get seller data
        list($name, $address) = $this->Sellers->getSeller($data->seller_id, $this->local);
        $data->seller_name = $name;
        $data->seller_address = $address;

        // get customer data
        list($c_name, $c_address) = $this->Customers->getCustomer($data->customer_id, $this->local);
        $data->customer_name = $c_name;
        $data->customer_address = $c_address;
        if (!$data->id) {
            $data->user_id = $this->Auth->user('id');
            $data->status = PO_STATUS_DRAFF;
            $data->affiliation_class = $this->request->session()->read('user_group.affiliation_class');
            $data->rop_number = $this->Sales->ropNumber($data->type);
        }
        if ($request['order_date']) {
            $order_date = date_create($request['order_date']);
            $data->order_date = date_format($order_date, 'Y-m-d');
        }
        if ($this->Sales->save($data)) {
            $this->response->body(json_encode([
                'status' => 1,
                'message' => MSG_SUCCESS,
                'data' => $data->id,
            ]));
            return $this->response;
        }
    }

    // CLEARN UP CODE

    /**
     * Using for update sale record with subsidiary
     * @return object return data to ajax
     * @throws NotFoundException request is not ajax
     */
    public function updateSaleWithSubsidiary()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->loadModel('InfoDetails');
        $this->viewBuilder()->layout('ajax');
        $this->autoRender = false;
        $this->response->disableCache();
        $this->response->type('json');
        $data = $this->request->data;
        // Get info_detail records by email and type and tel
        $infoDetails = $this->InfoDetails->find()
                ->where(['external_id' => $data['subsidiary_id']])
                ->where(['type' => TYPE_SUBSIDIARY])
                ->all();
        $infoDetail = new Collection($infoDetails);
        $infoDetail = $infoDetail->filter(function ($value, $key) use ($data) {
            return (($value->email === $data['customer_email']) || ($value->tel === $data['customer_tel']));
        })->toArray();
        $infoDetail = reset($infoDetail);
        // Check exist records
        if ($infoDetail) {
//            $infoDetail = $this->InfoDetails->get($infoDetail->id);
            $infoDetail->tel = $data['customer_tel'];
            $infoDetail->email = $data['customer_email'];
            $this->InfoDetails->save($infoDetail);
            pr($infoDetail);
        } else {
            $infoDetail = [];
            $infoDetail = $this->InfoDetails->newEntity();
            $infoDetail->external_id = $data['subsidiary_id'];
            $infoDetail->type = TYPE_SUBSIDIARY;
            $infoDetail->tel = $data['customer_tel'];
            $infoDetail->email = $data['customer_email'];
            // Save new recored to info_details
            $this->InfoDetails->save($infoDetail);
            if ($this->InfoDetails->save($infoDetail)) {
                $this->response->body(json_encode([
                    'status' => 1,
                    'message' => MSG_SUCCESS,
                ]));
                return $this->response;
            }
        }
        // Get sale record by id
        // Update record
        $sale = $this->Sales->findById($data['id'])->first();
        $sale->customer_name = $data['customer_name'];
        $sale->customer_address = $data['customer_address'];
        $sale->customer_tel = $infoDetail->tel;
        $sale->customer_email = $infoDetail->email;
        // Update sale record
        if ($this->Sales->save($sale)) {
            $this->response->body(json_encode([
                'status' => 1,
                'message' => MSG_SUCCESS,
            ]));
            return $this->response;
        }
    }

    /**
     * Using for create a new record or edit record
     * @return object return json_encode when it's successful
     */
    public function createAndUpdate()
    {
        $this->ajaxRequest(true);
        // Check target new or edit
        if ($this->request->data['target'] === TARGET_EDIT) {
            $data = $this->Sales->get($this->request->data['id']);
            if (isset($this->request->data['subsidiary_id'])) {
                $data->subsidiary_id = $this->request->data['subsidiary_id'];
            } else {
                $data->subsidiary_id =  null;
            }
        } else {
            $data = $this->Sales->newEntity();
            $data->type = TRADE_TYPE_SALE_W_STOCK;
        }
        $data = $this->Sales->patchEntity($data, $this->request->data);
        // Check Validation
        if ($data->errors()) {
            $this->response->body(json_encode([
                'status' => 0,
                'message' => MSG_ERROR,
                'data' => $this->Function->getError($data->errors()),
            ]));
            return $this->response;
        }
        $this->loadModel('Sellers');
        $this->loadModel('Customers');
        // Get seller data
        list($name, $address) = $this->Sellers->getSeller($data->seller_id, $this->local);
        $data->seller_name = $name;
        $data->seller_address = $address;
        // Get customer data
        list($c_name, $c_address) = $this->Customers->getCustomer($data->customer_id, $this->local);
        $data->customer_name = $c_name;
        $data->customer_address = $c_address;
        // Check exist
        if ($this->request->data['target'] === TARGET_NEW) {
            $data->user_id = $this->Auth->user('id');
            $data->status = PO_STATUS_DRAFF;
            $data->affiliation_class = $this->request->session()->read('user_group.affiliation_class');
            $data->rop_number = $this->Sales->ropNumber($data->type);
        }
        if ($this->request->data['order_date']) {
            $order_date = date_create($this->request->data['order_date']);
            $data->order_date = date_format($order_date, 'Y-m-d');
        }

        if ($this->Sales->save($data)) {
            $this->response->body(json_encode([
                'status' => 1,
                'message' => MSG_SUCCESS,
                'data' => [
                    'id' => $data->id,
                ],
            ]));
            return $this->response;
        }
    }

    public function po($id = null)
    {
        if ($id === null) {
            throw new NotFoundException();
        }
        $this->loadModel('SaleDetails');
        $person = [];
        $delivery = [];

        // get sales data
        $sale = $this->Sales->getById($id, null, [
            'Currencies',
            'Customers',
            'Sellers' => [
                'Manufacturers' => [
                    'InfoDetails',
                    'Countries'
                ],
                'Suppliers' => [
                    'InfoDetails',
                    'Countries'
                ]
            ],
            'SaleDeliveries' => ['Deliveries'],
        ]);
        // count child record where sale_id IS NOT null
        $countRecords = $this->SaleDetails->countRecords([
            'SaleDetails.parent_id IS NOT' => null,
            'SaleDetails.sale_id' => $id,
        ]);
        // get saleDetail with saleStockDetail
        $saleDetails = $this->SaleDetails->getAllDataList('threaded', null, ['SaleDetails.sale_id' => $id], [
            'SaleStockDetails' => function ($q) {
                $q->select([
                    'id',
                    'sale_detail_id',
                    'count' => $q->func()->count('*')
                ])->group('sale_detail_id');
                return $q;
            },
        ]);

        if ($sale) {
            $this->loadModel('PersonInCharges');
            $this->loadModel('Deliveries');
            $delivery = $this->Deliveries->findBySaleId($sale->id);
            $ext = [];
            switch ($sale->seller->type) {
                case TYPE_SUPPLIER :
                    $ext = [
                        'type' => TYPE_SUPPLIER,
                        'id' => $sale->seller->supplier->id,
                    ];
                    break;
                default :
                    $ext = [
                        'type' => TYPE_MANUFACTURER,
                        'id' => $sale->seller->manufacturer->id
                    ];
            }
            $person = ['' => __('TXT_SELECT_CONTACT')] + $this->PersonInCharges->getList($this->local, $ext['type'], $ext['id']);
        }

        $typePdf = $this->CustomConstant->typePdf();
        // set data to view layout
        $data = [
            'sale' => $sale,
            'person' => $person,
            'saleDetails' => $saleDetails,
            'typePdf' => $typePdf,
            'countRecords' => $countRecords,
            'local' => $this->local,
            'delivery' => $delivery,
        ];
        $this->set($data);
        // render view by customer type
        switch ($sale->customer->type) {
            case CUSTOMER_TYPE_NEW :
            case CUSTOMER_TYPE_BAD :
            case CUSTOMER_TYPE_NORMAL :
                $this->render('po');
                break;
            case CUSTOMER_TYPE_CONTRACT :
                $this->render('po_contract');
                break;
            default :
                $this->render('po_normal');
        }
    }

    /**
     * Using for retrieve sale data by id
     * @param int $id sale's id
     * @throws NotFoundException request doesn't have sale_id
     * @return object Sale, Currencies, Medias, SaleReceivePayment, Deliveries
     */
    public function detail($id = null)
    {
        if ($id === null) {
            throw new NotFoundException();
        }
        $this->loadModel('SaleDetails');
        $this->loadModel('Currencies');
        $this->loadModel('Medias');
        $this->loadModel('SaleReceivePayments');
        $this->loadModel('Deliveries');
        $person = [];
        $doctors = [];

        $currencies = $this->Currencies->getDataList('id', 'code', null, $this->local);
        // get sales data
        $sale = $this->Sales->getById($id, null, [
            'Currencies',
            'Customers' => [
                'Countries',
                'CustomerSubsidiaries' => ['Subsidiaries']
            ],
            'Sellers' => [
                'Manufacturers' => [
                    'InfoDetails',
                    'Countries'
                ],
                'Suppliers' => [
                    'InfoDetails',
                    'Countries'
                ]
            ]
        ]);
        // count child record where sale_id IS NOT null
        $countRecords = $this->SaleDetails->countRecords([
            'SaleDetails.parent_id IS NOT' => null,
            'SaleDetails.sale_id' => $id,
        ]);
        // get saleDetail with saleStockDetail
        $saleDetails = $this->SaleDetails->getAllDataList('threaded', null, ['SaleDetails.sale_id' => $id], [
            'SaleStockDetails' => function ($q) {
                $q->select([
                    'id',
                    'sale_detail_id',
                    'count' => $q->func()->count('*')
                ])
                ->group('sale_detail_id');
                return $q;
            },
        ]);
        if ($sale->customer) {
            $this->loadModel('ClinicDoctors');
            $doctors = $this->ClinicDoctors->getDoctors($sale->customer->id);
        }
        if ($sale) {
            $this->loadModel('PersonInCharges');
            $ext = [];
            switch ($sale->seller->type) {
                case TYPE_SUPPLIER :
                    $ext = [
                        'type' => TYPE_SUPPLIER,
                        'id' => $sale->seller->supplier->id,
                    ];
                    break;
                default :
                    $ext = [
                        'type' => TYPE_MANUFACTURER,
                        'id' => $sale->seller->manufacturer->id
                    ];
            }
            $person = ['' => __('TXT_SELECT_CONTACT')] + $this->PersonInCharges->getList($this->local, $ext['type'], $ext['id']);
        }
        // get payment by sale_id
        $payments = $this->SaleReceivePayments->getSalePaymentListBySaleId($id);
        // get sale_deliveries by sale_id
        $delivery = $this->Deliveries->findBySaleId($id)->contain(['DeliveryDetails'])->first();
        // get medis by type and external_id
        $medias = $this->Medias->getAllDataList('all', null, [
            'Medias.external_id' => $id,
            'Medias.type' => TYPE_SALE_W_STOCK,
        ]);
        $typePdf = $this->CustomConstant->typePdf();
        $saleStatus = $this->CustomConstant->saleStatus();
        // set data to view 
        $data = [
            'sale' => $sale,
            'person' => $person,
            'saleDetails' => $saleDetails,
            'countRecords' => $countRecords,
            'currencies' => $currencies,
            'payments' => $payments,
            'medias' => $medias,
            'typePdf' => $typePdf,
            'delivery' => $delivery,
            'doctors' => $doctors,
            'local' => $this->local,
            'saleStatus' => $saleStatus,
        ];
        $this->set($data);
    }

    public function createForm()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->viewBuilder()->layout('ajax');
        $this->loadModel('Countries');
        $data = $this->Sales->newEntity();
        $kind_of = $this->request->query('kind_of');
        $country_id = null;

        if ($kind_of == 'jp_only' || $kind_of == 'not_jp') {
            $country_id = $this->Countries->getJPCountryId();
        }
        $sellers = $this->getSeller($kind_of, $country_id, $this->local);
        $currencies = ['' => __('TXT_SELECT_CURRENCY')] + $this->getCurrency()->toArray();
        $data1 = [
            'data' => $data,
            'sellers' => $sellers,
            'currencies' => $currencies,
            'kind_of' => $kind_of,
        ];
        $this->set($data1);
    }

    public function getData()
    {
        $this->ajaxRequest(true);
        $this->loadModel('Countries');
        $this->loadModel('Sellers');
        $data = [];
        $subsidiary = [];
        $target = $this->request->query('target');
        // Check edit type
        if ($target === TARGET_EDIT) {
            $data = $this->Sales->findById($this->request->query('id'))
                    ->contain(['Customers'])
                    ->first();
        }
        // Retrieve subsidiaries
        if ($data && ($data->subsidiary_id !== null)) {
            $this->loadModel('Subsidiaries');
            $subsidiary = $this->Subsidiaries->findById($data->subsidiary_id)->first();
        }
        $sellers = $this->Sellers->find('all')
            ->where(['Sellers.kind' => USER_CLASS_WISTERIA])
            ->contain(['Suppliers', 'Manufacturers']);
        $currencies = ['' => __('TXT_SELECT_CURRENCY')] + $this->getCurrency()->toArray();
        $data1 = [
            'data' => $data,
            'subsidiary' => $subsidiary,
            'sellers' => $sellers,
            'currencies' => $currencies,
            'kind_of' => 'jp_only',
            'local' => $this->local,
            'target' => $target,
        ];
        $this->set($data1);
    }

    public function getFaxRop()
    {
        $this->ajaxRequest(true);
        $local = $this->local;
        $id = $this->request->query('id');
        $this->set(compact('local', 'id'));
    }

    public function changeDraffStatus()
    {
        $this->ajaxRequest(false);
        $this->response->type('json');
        $sale = $this->Sales->get($this->request->data['id']);
        $sale->status = STATUS_FAXED_W_INVOICE;
        if ($this->Sales->save($sale)) {
            $this->response->body(json_encode([
                'status' => 1,
                'message' => MSG_SUCCESS,
            ]));
        }
        return $this->response;
    }

    public function getCustomerList()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->viewBuilder()->layout('ajax');
        $this->autoRender = false;
        $this->response->disableCache();
        $this->response->type('json');
        $this->loadModel('Customers');
        $keyword = $this->request->query('keyword');

        $customers = $this->Customers->getAllDataList('all', null, [
            'AND' => [
                'Customers.is_suspend' => 0,
                'OR' => [
                    'Customers.name_en LIKE' => '%' . $keyword . '%',
                    'Customers.name LIKE' => '%' . $keyword . '%',
                ]
            ],
        ], null, 20);

        $this->response->body(json_encode([
            'status' => 1,
            'message' => MSG_SUCCESS,
            'data' => $customers,
        ]));
        return $this->response;
    }

    public function getAllSaleBySearch()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->viewBuilder()->layout('ajax');
        $this->autoRender = false;
        $this->response->type('json');
        $this->loadModel('Sellers');

        $seller_id = $this->Sellers->sellerIdAsMp();
        $keyword = $this->request->query('keyword');

        $sale = $this->Sales->getAllDataList('all', null, [
            'AND' => [
                'Sales.is_suspend' => 0,
                'Sales.seller_id' => $seller_id,
                'OR' => [
                    'Sales.rop_number LIKE' => '%' . $keyword . '%',
                    'Sales.sale_number LIKE' => '%' . $keyword . '%',
                ],
            ],
        ], null, 20);

        $this->response->body(json_encode([
            'status' => 1,
            'message' => MSG_SUCCESS,
            'data' => $sale,
        ]));
        return $this->response;
    }

    public function getBrowseFile()
    {
        $this->ajaxRequest(true);
        $media = [];
        $sale = $this->Sales->get($this->request->query('sale_id'));
        $type = $this->request->query('type');
        $typePdf = $this->CustomConstant->typePdf();
        if ($type === 'edit') {
            $this->loadModel('Medias');
            $media = $this->Medias->get($this->request->query('id'));
        }
        $data = [
            'typePdf' => $typePdf,
            'sale' => $sale,
            'media' => $media,
        ];
        $this->set($data);
    }

    public function uploadFile()
    {
        $this->ajaxRequest(false);
        $this->loadModel('Medias');
        $this->response->type('json');
        $path = WWW_ROOT . 'img' . DS . 'uploads' . DS . 'po';
        $data = $this->request->data;
        $this->set('_serialize', ['data']);
        // check media type new or edit
        if (!$data['media_id']) {
            $entity = $this->Medias->newEntity();
            // check file
            if (isset($data['file'])) {
                $temp = explode('.', $data['file']['name']);
                $newFile = $this->Image->upload(null, $data['file'], $path);
                $entity->external_id = $data['external_id'];
                $entity->type = TYPE_SALE_W_STOCK;
                $entity->document_type = $data['document_type'];
                $entity->file_name = $newFile;
                $entity->file_type = end($temp);
                $this->Medias->deleteMediaById($data['external_id'], USER_ROLE_SALE);
                if ($this->Medias->save($entity)) {
                    $this->response->body(json_encode([
                        'status' => 1,
                        'message' => MSG_SUCCESS,
                        'data' => [
                            'file' => $newFile,
                            'id' => $entity->id,
                        ],
                    ]));
                }
            }
        } else {
            $media = $this->Medias->get($data['media_id']);
            $media->document_type = $data['document_type'];
            if ($this->Medias->save($media)) {
                $this->response->body(json_encode([
                    'status' => 1,
                    'message' => MSG_SUCCESS,
                ]));
            }
        }
        return $this->response;
    }

    public function updateStatus()
    {
        $this->ajaxRequest(true);
        $this->loadModel('Sellers');
        $this->loadModel('SaleReceivePayments');
        $this->loadModel('SaleDeliveries');
        $this->loadModel('Medias');
        $data = [];

        $seller_id = $this->Sellers->sellerIdAsMp();
        $sale = $this->Sales->get($this->request->data['id'], [
            'contain' => ['SaleDeliveries'],
        ]);
        $sale->status = $this->request->data['status'];
        $saleDeliveries = $this->SaleDeliveries->getByField([
            'SaleDeliveries.sale_id' => $sale->id,
        ], null, ['Deliveries']);
        $medias = $this->Medias->getAllDataList('all', null, [
            'Medias.external_id' => $sale->id,
            'Medias.type' => TYPE_SALE_W_STOCK,
        ]);
        $typePdf = $this->CustomConstant->typePdf();
        if ($this->request->data['action'] === 'po') {
            $sale->sale_number = $this->Sales->saleNumber($sale->type, [
                'seller_id' => $seller_id
            ]);
            if (!$sale->issue_date) {
                $sale->issue_date = $this->request->data['issue_date'];
            }
        }
        if ($this->Sales->save($sale)) {
            $data = [
                'controller' => 'sale',
                'currentStatus' => $sale->status,
                'saleStatus' => $this->CustomConstant->saleStatus(),
                'payments' => $this->SaleReceivePayments->getSalePaymentListBySaleId($this->request->data['id']),
                'saleDeliveries' => $saleDeliveries,
                'sale' => $sale,
                'medias' => $medias,
                'typePdf' => $typePdf,
            ];
        }
        $this->set($data);
        $this->render('render_step');
    }

    public function updateCurrencyExchange() {
        $this->ajaxRequest(false);
        $request = $this->request->data;
        $sale = $this->Sales->get($request['id']);

        $sale->currency_date = str_replace('/', '-', $request['currency_date']);
        $sale->currency_exchange_rate = $request['currency_exchange_rate'];

        if ($this->Sales->save($sale, ['validate' => 'exchange'])) {
            $this->Function->ajaxResponse(1, MSG_SUCCESS);
        } else {
            $this->Function->ajaxResponse(0, MSG_ERROR, $sale->errors());
        }
    }

    private function deductStock($saleId, $response, $request)
    {
        $this->loadModel('SaleDetails');

        $saleDetails = $this->SaleDetails->getAllDataList('all', null, ['SaleDetails.sale_id' => $saleId], [
            'SaleStockDetails',
        ]);

        switch ($request['po_type']) {
            case CUSTOMER_TYPE_CONTRACT :
                if ($request['step'] == 3) {
                    // sale stock detail
                    $this->grebSaleStockDetail($saleDetails);
                }
                break;
            default :
                if ($request['step'] == 4) {
                    // sale stock detail
                    $this->grebSaleStockDetail($saleDetails);
                }
                break;
        }

        $this->response->body(json_encode([
            'message' => MSG_SUCCESS,
            'status' => 1,
            'data' => [
                'step' => $response['step'],
                'status' => $response['status']
            ]
        ]));
        return $this->response;
    }

    private function grebSaleStockDetail($data)
    {
        $saleStockDetail = [];

        if ($data) {
            foreach ($data as $key => $value) {
                if ($value->sale_stock_details) {
                    $saleStockDetail[] = $value->sale_stock_details;
                }
            }
        }

        if ($saleStockDetail) {
            $this->loadModel('Stocks');
            $data1 = [];

            foreach ($saleStockDetail as $key1 => $value1) {
                $data1[] = $value1;
            }
            $this->Stocks->deductStock($data1);
        }
    }

    /**
    * Function ajaxRequest
    * using for checking the request is ajax
    * @param boolean $auto_render assign value to $this->autoRender
    */
    private function ajaxRequest($auto_render = false)
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->viewBuilder()->layout('ajax');
        $this->autoRender = $auto_render;
        $this->response->disableCache();
    }

    /**
     * Function getCurrency
     * using for get currency list
     * @return object retrun array list
     */
    private function getCurrency()
    {
        $this->loadModel('Currencies');
        $data = $this->Currencies->getDropdown('list');

        return $data;
    }

    /**
     * Function getSeller
     * using for get seller list
     * @return object retrun array list
     */
    private function getSeller($kind_of = false, $country_id = null, $en = null)
    {
        $this->loadModel('Sellers');
        $field_name = 'name' . $this->local;
        $data = $this->Sellers->getSellerList($field_name, $kind_of, $country_id, $en);

        return $data;
    }
}
