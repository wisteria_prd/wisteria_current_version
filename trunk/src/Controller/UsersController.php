<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Utility\Text;
use Cake\Routing\Router;
use Cake\Network\Email\Email;
use Cake\I18n\Date;
use Cake\Network\Exception\NotFoundException;
use Cake\Auth\DefaultPasswordHasher;

class UsersController extends AppController
{
    public $locale;

    public function initialize()
    {
        parent::initialize();
        $this->Auth->allow([
            'activate',
            'activation',
            'tcAgreement',
            'ppAgreement',
            'mediProForm',
            'agentForm',
            'clinicForm',
            'checkEmail'
        ]);
        $this->loadComponent('Flash');
        $this->loadComponent('Function');
        if ($this->Auth->user('id')) {
            $data = $this->Users->get($this->Auth->user('id'));
            // TODO: check and remove this line to prevent access
            $this->Auth->setUser($data);
        }
        $this->locale = $this->request->session()->read('tb_field');
    }

    public function login()
    {
        if ($this->request->is('post')) {
            $user = $this->Auth->identify();
            if ($user) {
                $this->Auth->setUser($user);
                $this->redirect($this->Auth->redirectUrl());
            } else {
                $this->Flash->error(__('INVALID_USER_LOGIN'));
            }
        }
    }

    public function logout()
    {
        $this->redirect($this->Auth->logout());
    }

    public function index()
    {
        $this->loadModel('UserGroups');
        $session = $this->request->session();
        $options = [];
        // CHECK KEYWORD SEARCH
        if (!empty($this->request->query('keyword'))) {
            $keyword = trim($this->request->query('keyword'));
            $options['AND'][]['OR'] = [
                'Users.firstname LIKE ' => '%' . $keyword . '%',
                'Users.firstname_en LIKE ' => '%' . $keyword . '%',
                'Users.lastname LIKE ' => '%' . $keyword . '%',
                'Users.lastname_en LIKE ' => '%' . $keyword . '%',
            ];
        }
        // CHECK USER ROLE
        if (!empty($this->request->query('role'))) {
            $options['AND'][] = [
                'Users.user_group_id' => $this->request->query('role'),
            ];
        }
        // CHECK USER STATUS
        if (!empty($this->request->query('inactive'))) {
            $status = STATUS_ACTIVE;
            if ($this->request->query('inactive') == 1) {
                $status = STATUS_INACTIVE;
            }
            $options['AND'][] = [
                'Users.status' => $status,
            ];
        }
        $this->paginate = [
            'conditions' => $options,
            'contain' => [
                'UserGroups',
            ],
            'sortWhitelist' => [
                'Users.status',
                'Users.lastname',
                'Users.email',
                'Users.created',
                'Users.modified',
                'UserGroups.name',
                'UserGroups.name_en'
            ],
            'order' => ['Users.status' => 'asc'],
            'limit' => PAGE_NUMBER,
        ];
        $userGroups = $this->UserGroups->find()
                ->where(['affiliation_class' => $session->read('user_group.affiliation_class')])
                ->all();
        $data = [
            'userGroups' => $userGroups,
            'users' => $this->paginate($this->Users),
        ];
        $this->set($data);
    }

    public function view()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->viewBuilder()->layout('ajax');
        $user = $this->Users->get($this->request->query('id'), [
            'contain' => ['UserGroups'],
        ]);
        $data = [
            'user' => $user,
        ];
        $this->set($data);
    }

    public function register()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->viewBuilder()->layout('ajax');
        $this->autoRender = false;
        $this->response->type('json');
        $this->response->disableCache();
        $token = Text::uuid();
        $data = $this->request->data;
        //CHECK USER ID EXIST
        if (!empty($data['id'])) {
            $user = $this->Users->get($data['id']);
        } else {
            $user = $this->Users->newEntity();
            $user->status = STATUS_INACTIVE;
            $user->activation_token = $token;
        }
        $user = $this->Users->patchEntity($user, $data);
        //CHECK VALIDATION IS NOT EMPTY AND RESPONSE ERROR MESSAGE
        if ($user->errors()) {
            $this->response->body(json_encode([
                'status' => 0,
                'message' => MSG_ERROR,
                'data' => $this->Function->getError($user->errors()),
            ]));
            return $this->response;
        }
        //SAVE DATA
        if ($this->Users->save($user)) {
            if ($data['target'] === TARGET_NEW) {
                $variable = [
                    'user_email' => $user->email,
                    'token' => $token,
                    'service' => __('User Registration Confirmation')
                ];
                $this->sendMail(
                        $user->email,
                        'activation',
                        'html',
                        __('User Register Confirmation'),
                        $variable
                    );
            }
            $this->response->body(json_encode([
                'status' => 1,
                'message' => MSG_SUCCESS,
            ]));
            return $this->response;
        }
    }

    public function getRegisterForm()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->viewBuilder()->layout('ajax');
        $this->loadModel('UserGroups');
        $userGroups = $this->UserGroups->find()->all();
        $user = $this->Users->newEntity();
        // CHECK TARGET OF RECORD
        if ($this->request->query('target') === TARGET_EDIT) {
            $user = $this->Users->findById($this->request->query('id'))
                    ->contain(['UserGroups'])
                    ->first();
        }
        $data = [
            'userGroups' => $userGroups,
            'user' => $user,
            'target' => $this->request->query('target'),
            'locale' => $this->locale,
        ];
        $this->set($data);
    }

    public function resetPasswordForm()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->viewBuilder()->layout('ajax');
    }

    public function reactivateEmail($id)
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->autoRender = false;
        $this->response->disableCache();
        $this->response->type('json');
        $this->viewBuilder()->layout(false);
        $user = $this->Users->get($id);
        if (!$user) {
            $this->response->body(json_encode([
                'result' => 'error'
            ]));
            return $this->response;
        }
        $token = Text::uuid();
        if (!$this->Users->checkUserByToken($token)) {
            $token = Text::uuid();
        }
        $variable = [
            'user_email' => $user->email,
            'token' => $token,
            'service' => __('User Registration Confirmation')
        ];
        // UPDATED USER TOKEN
        $data['activation_token'] = $token;
        $user = $this->Users->patchEntity($user, $data);
        $this->Users->save($user);
        // COMFIRMATION EMAIL
        $this->sendMail($user->email,
                'activation',
                'html',
                __('User Register Confirmation'),
                $variable,
                __('Email Reactivation')
                );

        $this->response->body(json_encode([
            'result' => 'ok',
        ]));
        return $this->response;
    }

    public function activation($token = null)
    {
        if (empty($token) || ($token === null)) {
            $this->redirect([
                'controller' => 'Users',
                'action' => 'login',
            ]);
        }
        $this->viewBuilder()->layout('login');
        $user = $this->Users->find()
                ->where(['activation_token' => $token])
                ->first();
        if (!$user) {
            $this->redirect([
                'controller' => 'Home',
                'action' => 'page_404_not_found',
            ]);
        }
        $registered_date = date_create($user->modified);
        $current_date = date_create('today');
        $interval = date_diff($registered_date, $current_date);
        $hour = $interval->format('%h');
        //CHECK TOKEN EXPIRED 24 HOURS
        if ($hour > EXPIRE_HOUR) {
            $this->redirect([
                'controller' => 'Home',
                'action' => 'page_404_not_found',
            ]);
        }
        $this->userActivate($user);
        $data = [
            'token' => $token,
            'type' => $this->request->query('type'),
            'user' => $user,
        ];
        $this->set($data);
    }

    public function accountActivate()
    {
        $login = $this->Auth->user();
        if (!$login['id']) {
            $this->redirect(['action' => 'login']);
        }
        $this->loadModel('UserGroups');
        $group = $this->UserGroups->get($this->Auth->user('user_group_id'));
        if (($group->affiliation_class == USER_CLASS_WISTERIA) ||
                ($group->affiliation_class == USER_CLASS_MEDIPRO)) {
            $user = $this->Users->findById($this->Auth->user('id'))->first();
            $data = [
                'user' => $user,
                'group' => $group,
            ];
            $this->set($data);
        } else {
            $this->redirect(['action' => 'login']);
        }
    }

    public function tcAgreement()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->autoRender = false;
        $this->viewBuilder()->layout(false);
        $this->response->disableCache();
        $this->response->type('json');
        if (!$this->Auth->user()) {
            throw new UnauthorizedException(__('TXT_NO_PERMISSION'));
        }
        $user = $this->Users->findById($this->Auth->user('id'))->first();
        $user = $this->Users->patchEntity($user, $this->request->data, [
            'validate' => 'tcAgreement',
        ]);
        // CHECK VALIDATION
        if ($user->errors()) {
            $this->response->body(json_encode([
                'status' => 0,
                'message' => MSG_ERROR,
                'data' => $this->Function->getError($user->errors()),
            ]));
            return $this->response;
        }
        if ($this->Users->save($user)) {
            $this->response->body(json_encode([
                'status' => 1,
                'message' => MSG_SUCCESS,
                'data' => [
                    'pp_agreement' => $user->pp_agreement,
                    'activated' => $user->activated,
                    'class' => $user->affiliation_class,
                ],
            ]));
            return $this->response;
        }
    }

    public function ppAgreement()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->autoRender = false;
        $this->viewBuilder()->layout(false);
        $this->response->disableCache();
        $this->response->type('json');
        if (!$this->Auth->user()) {
            throw new UnauthorizedException(__('TXT_NO_PERMISSION'));
        }
        $user = $this->Users->findById($this->Auth->user('id'))->first();
        $user = $this->Users->patchEntity($user, $this->request->data, [
            'validate' => 'ppAgreement',
        ]);
        // CHECK VALIDATION
        if ($user->errors()) {
            $this->response->body(json_encode([
                'status' => 0,
                'message' => MSG_ERROR,
                'data' => $this->Function->getError($user->errors()),
            ]));
            return $this->response;
        }
        if ($this->Users->save($user)) {
            $this->response->body(json_encode([
                'status' => 1,
                'message' => MSG_SUCCESS,
                'data' => [
                    'pp_agreement' => $user->pp_agreement,
                    'activated' => $user->activated,
                    'class' => $user->affiliation_class,
                ],
            ]));
            return $this->response;
        }
    }

    public function checkEmail()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->autoRender = false;
        $this->viewBuilder()->layout(false);
        $this->response->disableCache();
        $this->response->type('json');
        $error = '';
        if (empty($this->request->data['email'])) {
            $error = __('*登録のメールアドレスを入力してください。');
        }
        if (!filter_var($this->request->data['email'], FILTER_VALIDATE_EMAIL)) {
            $error = __('*登録のメールアドレスを入力してください。');
        }
        if (!empty($error)) {
            $this->response->body(json_encode([
                'status' => 'ERROR',
                'msg' => $error
            ]));
            return $this->response;
        }
        $user = $this->Users->findByEmail($this->request->data['email'])->first();
        if ($user) {
            $token = Text::uuid();
            //update user token
            $data['activation_token'] = $token;
            $data['status'] = STATUS_INACTIVE;
            $user = $this->Users->patchEntity($user, $data);
            $this->Users->save($user);
            $variable = [
                'user_email' => $user->email,
                'token' => $token,
                'service' => __('User Password Reset.')
            ];
            // EMAIL CONFIRMATION
            $this->sendMail($user->email,
                    __('reset_password'),
                    'html',
                    __('User Password Reset.'),
                    $variable,
                    __('Reset Password'));
            $this->response->body(json_encode([
                'status' => 'OK',
                'msg' => __('TXT_MESSAGE_CREATE')
            ]));
            return $this->response;
        }
        $this->response->body(json_encode([
            'status' => 'ERROR',
            'msg' => __('*登録のメールアドレスを入力してください。')
        ]));
        return $this->response;
    }

    public function changePassword()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->autoRender = false;
        $this->viewBuilder()->layout(false);
        $this->response->disableCache();
        $this->response->type('json');
        $user =$this->Users->get($this->Auth->user('id'));
        $user = $this->Users->patchEntity($user, $this->request->data, [
            'validate' => 'password',
        ]);
        //CHECK VALIDATION IS NOT EMPTY AND RESPONSE ERROR MESSAGE
        if ($user->errors()) {
            $this->response->body(json_encode([
                'status' => 0,
                'message' => MSG_ERROR,
                'data' => $user->errors(),
            ]));
            return $this->response;
        }
        //SAVE DATA
        if ($this->Users->save($user)) {
            $this->response->body(json_encode([
                'status' => 1,
                'message' => MSG_SUCCESS,
            ]));
            return $this->response;
        }
    }

    public function getStatus()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->viewBuilder()->layout('ajax');
        $user = $this->Users->get($this->request->query('id'));
        $data = [
            'user' => $user,
            'status' => $this->request->query('status'),
        ];
        $this->set($data);
    }

    public function getDelete()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->viewBuilder()->layout('ajax');
        $user = $this->Users->get($this->request->query('id'));
        $data = [
            'user' => $user,
        ];
        $this->set($data);
    }

    public function changeOfStatus()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->viewBuilder()->layout('ajax');
        $this->autoRender = false;
        $this->response->disableCache();
        $this->response->type('json');
        $user = $this->Users->get($this->request->data['id']);
        $user->status = $this->request->data['status'];
        if ($this->Users->save($user)) {
            $this->response->body(json_encode([
                'status' => 1,
                'message' => MSG_SUCCESS,
            ]));
            return $this->response;
        }
    }

    public function delete()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->viewBuilder()->layout('ajax');
        $this->autoRender = false;
        $this->response->disableCache();
        $this->response->type('json');
        $user = $this->Users->get($this->request->data['id']);
        if ($this->Users->delete($user)) {
            $this->response->body(json_encode([
                'status' => 1,
                'message' => MSG_SUCCESS,
            ]));
            return $this->response;
        }
    }

    public function getAutocomplete()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->viewBuilder()->layout('ajax');
        $this->autoRender = false;
        $this->response->disableCache();
        $this->response->type('json');
        $users = $this->Users->find()
                ->where(['status' => STATUS_ACTIVE])
                ->where(['OR' => [
                    'Users.firstname LIKE' => '%' . $this->request->query('search') . '%',
                    'Users.firstname_en LIKE' => '%' . $this->request->query('search') . '%',
                    'Users.lastname LIKE' => '%' . $this->request->query('search') . '%',
                    'Users.lastname_en LIKE' => '%' . $this->request->query('search') . '%',
                ]])
                ->limit(PAGE_LIMIT_NUMBER)
                ->all();
        $this->response->body(json_encode([
            'status' => 1,
            'message' => MSG_SUCCESS,
            'data' => [
                'users' => $users,
            ],
        ]));
        return $this->response;
    }

    public function getUserRole()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->viewBuilder()->layout('ajax');
        $userGroups = $this->UserGroups->find()
            ->where(['affiliation_class' => $this->request->query('affiliation_class')])
            ->all();
        $this->set([
            'userGroups' => $userGroups,
            'affiliation_class' => $this->request->query('affiliation_class'),
        ]);
    }

    // Send mail
    private function sendMail(
        $to,
        $template,
        $format,
        $subject,
        $variable,
        $type = 'User Registration'
    ) {
        $email = new Email('default');
        $email->from([SYSTEM_EMAIL => $type])
            ->to($to)
            ->template($template)
            ->viewVars($variable)
            ->emailFormat($format)
            ->subject($subject);
        $email->send();
    }

    private function userActivate($user = null)
    {
        if ($user === null) {
            return false;
        }
        if ($this->request->is(['put', 'post'])) {
            $user->status = STATUS_ACTIVE;
            $user->login_attempt = 0;
            $user = $this->Users->patchEntity($user, $this->request->data, [
                'validate' => 'newPassword',
            ]);
            if ($this->Users->save($user)) {
                $this->redirect([
                    'controller' => 'Users',
                    'action' => 'login',
                ]);
            }
        }
    }

    private function checkUserByToken($token)
    {
        if ($this->Users->exists(['activation_token' => $token])) {
            return false ;
        }
        return true;
    }
}
