<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Network\Exception\NotFoundException;
use Cake\Collection\Collection;

class SaleDetailsController extends AppController
{
    /**
    * @var string for store local session field
    */
    private $local;

    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('Pluralize');
        $this->loadComponent('Function');
        $this->local = $this->request->session()->read('tb_field');
    }

    public function create()
    {
        $this->ajaxRequest(false);
        if ($this->request->data['id']) {
            $data = $this->SaleDetails->get($this->request->data['id']);
        } else {
            $data = $this->SaleDetails->newEntity();
        }
        $data = $this->SaleDetails->patchEntity($data, $this->request->data);
        if ($data->errors()) {
            $errors = $this->Function->getError($data->errors());
            $this->Function->ajaxResponse(0, MSG_ERROR, $errors);
        }
        if (isset($this->request->data['pack_size_name'])) {
            $data->pack_size_name = $this->Pluralize->pluralize_if($this->request->data['pack_size_value'], $this->request->data['pack_size_name']);
        }
        $this->SaleDetails->save($data);
        $this->Function->ajaxResponse(1, MSG_SUCCESS);
    }

    public function getFormCreate()
    {
        $this->ajaxRequest(true);
        $this->loadModel('Sellers');
        $this->loadModel('Sales');
        $this->loadModel('Stocks');
        $productIds = [];
        $products = [];
        $saleDetail = [];

        $sale = $this->Sales->get($this->request->query('saleId'));
        $stocks = $this->Stocks->find('all')->contain(['ImportDetails']);
        $seller = $this->Sellers->get($sale->seller_id, [
            'contain' => [
                'SellerBrands' => ['SellerProducts']
            ]
        ]);
        if ($this->request->query('type') === TYPE_EDIT) {
            $saleDetail = $this->SaleDetails->get($this->request->query('id'));
        }
        if (!$seller && !$seller->seller_brands) {
            return false;
        }
        foreach ($seller->seller_brands as $key1 => $value1) {
            if (!$value1->seller_products) {
                continue;
            }
            foreach ($value1->seller_products as $key2 => $value2) {
                $productIds[] = $value2->product_id;
            }
        }
        if ($productIds) {
            $this->loadModel('Products');
            $products = $this->Products->find()
                ->contain([
                    'ProductBrands',
                    'ProductDetails' => [
                        'PackSizes',
                        'SingleUnits',
                        'queryBuilder' => function($q) {
                            return $q->where(['ProductDetails.is_suspend' => 0]);
                        },
                    ]
                ])->where([
                    'Products.is_suspend' => 0,
                    'Products.id IN ' => $productIds
                ]);
        }
        $data = [
            'local' => $this->local,
            'products' => $products,
            'sale' => $sale,
            'stocks' => $stocks,
            'saleDetail' => $saleDetail,
        ];
       $this->set($data);
    }

    public function productFilter()
    {
        $this->ajaxRequest(false);
        $this->loadModel('ProductDetails');
        $keyword = '';
        $data = [];

        if ($this->request->query('keyword')) {
            $keyword = $this->request->query('keyword');
        }
        if (!empty($keyword)) {
            $data = $this->ProductDetails->find('all')
                ->contain([
                    'SingleUnits',
                    'PackSizes',
                    'Products' => [
                        'ProductBrands',
                        'queryBuilder' => function ($q) use ($keyword) {
                            return $q->where([
                                'OR' => [
                                    'Products.name LIKE' => '%' . $keyword . '%',
                                    'Products.name_en LIKE' => '%' . $keyword . '%',
                                ]
                            ]);
                        }
                    ]
                ])->limit(5)->toArray();
        }

        $this->Function->ajaxResponse(1, MSG_SUCCESS, $data);
    }

    public function getPricing()
    {
        $this->ajaxRequest(false);
        $this->loadModel('Pricing');
        $this->loadModel('ClinicPricing');
        $data = [];
        $pricing = [];

        $ext_id = $this->request->query('id');
        $qty = $this->request->query('quantity');
        $customer_id = $this->request->query('customer_id');

        // get ClinicPricing data
        $clinic_pricing = $this->ClinicPricing->getByCustomerId($customer_id);
        if ($clinic_pricing) {
            $ids = [];
            foreach ($clinic_pricing as $key => $value) {
                $ids[] = $value->pricing_id;
            }

            // get Pricing by id data
            $pricing = $this->Pricing->getByClinicPricing($ids, $ext_id, $qty, TYPE_PRODUCT_DETAILS);
        }

        if ($pricing) {
            $data = [
                'price' => $pricing[0]->unit_price,
                'from' => 'Pricing',
                'id' => $pricing[0]->id,
            ];
        } else {
            // get Pricing with between Quantity
            $pricing1 = $this->Pricing->getByTypeAndExternalId(TYPE_PRODUCT_DETAILS, $ext_id, $qty);
            if ($pricing1) {
                $data = [
                    'price' => $pricing1[0]->unit_price,
                    'from' => 'Pricing',
                    'id' => $pricing1[0]->id,
                ];
            } else {
                // get WStandardPrice data
                $this->loadModel('WStandardPrices');
                $w_stanadrd_prices = $this->WStandardPrices->getByProductDetailId($ext_id);
                $data = [
                    'price' => $w_stanadrd_prices ? $w_stanadrd_prices[0]->standard_price : 0,
                    'from' => 'WStandardPrices',
                ];
            }
        }

        $this->Function->ajaxResponse(1, MSG_SUCCESS, $data);
    }

    public function delete()
    {
        $this->ajaxRequest(false);
        $data = $this->SaleDetails->get($this->request->data['id']);
        if ($this->SaleDetails->delete($data)) {
            $this->Function->ajaxResponse(1, MSG_SUCCESS);
        }
    }

    /**
    * Function ajaxRequest
    * using for checking the request is ajax
    * @param boolean $auto_render assign value to $this->autoRender
    */
    private function ajaxRequest($auto_render = false)
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->viewBuilder()->layout('ajax');

        if ($auto_render === false) {
            $this->autoRender = false;
        }
    }
}
