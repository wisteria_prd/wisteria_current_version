<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Routing\Router;

class ImportReturnsController extends AppController
{
    public function initialize()
    {
        parent::initialize();
    }

    public function index()
    {
        if (empty($this->request->query('import_id'))) {
            throw new NotFoundException(__('TXT_ITEM_ID_NOT_FOUND'));
        }

        $return_id = $this->request->query('return_id');

        $this->loadModel('ImportReturnDetails');
        $query = $this->ImportReturnDetails->find()
            ->contain([
                'ImportDetails' => [
                    'ProductDetails' => [
                        'Products',
                        'PackSizes',
                        'SingleUnits',
                        'queryBuilder' => function($q) {
                            return $q->where(['ProductDetails.is_suspend' => 0]);
                        },
                    ]
                ],
                'ImportReturns' => function ($q) use($return_id) {
                    return $q->where([
                        'ImportReturns.id' => $return_id
                    ]);
                }
            ]);
        try {
            $imports = $this->paginate($query); //pr($imports); exit;
            $this->set('imports', $imports);
            $this->set('_serialize', ['imports']);
        } catch (NotFoundException $e) {
            throw new NotFoundException(__('TXT_PAGE_OUT_OF_RANGE'));
        }

        $return = $this->ImportReturns->newEntity();
        if ($this->request->query('action') == 'edit') {
            $return = $this->ImportReturns->get($return_id, [
                'contain' => ['Currencies']
            ]);
        }

        $this->loadModel('Imports');
        $products = $this->Imports->get($this->request->query('import_id'), [
            'contain' => [
                'ImportDetails' => [
                    'ProductDetails' => [
                        'Products'
                    ]
                ]
            ]
        ]);
        
        $product_details = [];
        $import_details = [];
        if ($products->import_details) {
            $product_id = [];
            foreach ($products->import_details as $key => $p) {
                $product_id[$key] = $p->product_detail->product_id;
                $import_details[$p->id] = $p->lot_number . ' + ' . date('Y-m-d', strtotime($p->expiry_date));
            }
            $this->loadModel('Products');
            $product_details = $this->Products->productDetailsDropdown(array_unique($product_id));
        }

        $this->loadModel('Currencies');
        $currencies = $this->Currencies->getDropdown();
        $types = $this->CustomConstant->importReturnType();
        $levels = $this->CustomConstant->importDamageLevel();
        $en = $this->request->session()->read('tb_field');
        $this->set(compact('en', 'types', 'currencies', 'return', 'levels', 'product_details', 'import_details'));
        $this->set('_serialize', ['currencies', 'return', 'product_details', 'import_details']);
    }

    public function create()
    {
        if ($this->request->is('ajax')) {
            $this->autoRender = false;
            $this->response->disableCache();
            $this->response->type('json');
            $this->viewBuilder()->layout(false);

            $import = $this->ImportReturns->newEntity();
            
            $request = $this->request->data;

            $import = $this->ImportReturns->patchEntity($import, $request);

            $import->user_id = $this->Auth->user('id');
            if ($this->ImportReturns->save($import)) {
                $this->response->body(json_encode([
                    'status' => 1,
                    'redirect_url' => Router::url([
                        'action' => 'index',
                        '?' => [
                            'import_id' => $request['import_id'],
                            'return_id' => $import->id,
                            'action' => 'edit'
                        ]
                    ])
                ]));
                return $this->response;
            }

            $this->response->body(json_encode(['status' => 0, 'message' => $import->errors()]));
            return $this->response;
        }
    }

    public function edit()
    {
        if ($this->request->is('ajax')) {
            $this->autoRender = false;
            $this->response->disableCache();
            $this->response->type('json');
            $this->viewBuilder()->layout(false);

            $request = $this->request->data;

            $import = $this->ImportReturns->get($request['id'], [
                'contain' => ['Currencies']
            ]);

            $import = $this->ImportReturns->patchEntity($import, $request);

            if ($this->ImportReturns->save($import)) {
                $this->response->body(json_encode(['status' => 1]));
                return $this->response;
            }

            $this->response->body(json_encode(['status' => 0, 'message' => $import->errors()]));
            return $this->response;
        }
    }

    public function delete()
    {
        if ($this->request->is('ajax')) {
            $this->autoRender = false;
            $this->response->disableCache();
            $this->response->type('json');
            $this->viewBuilder()->layout(false);

            $this->request->allowMethod(['post', 'delete']);
            $discount = $this->ImportReturns->get($this->request->data['id']);
            if ($this->ImportReturns->delete($discount)) {
                $this->response->body(json_encode(['status' => 1]));
                return $this->response;
            }
            $this->response->body(json_encode(['status' => 0, 'message' => __('TXT_DELETE_TROUBLE')]));
            return $this->response;
        }
    }
}