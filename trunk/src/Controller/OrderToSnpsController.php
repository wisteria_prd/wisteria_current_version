<?php

namespace App\Controller;

use App\Controller\PurchasesController;
use Cake\Network\Exception\NotFoundException;

class OrderToSnpsController extends PurchasesController
{
    public function initialize()
    {
        parent::initialize();
        $this->loadModel('Purchases');
    }

    public function index()
    {
        $this->loadModel('Sellers');
        $options = [];

        if ($this->request->query('keyword')) {
            $options = ['Purchases.purchase_number' => $this->request->query('keyword')];
        }

        // Transection type = TRADE_TYPE_SALE_BEHALF_S_OUTSIDE_JP
        $options['AND']['Purchases.type'] = TRADE_TYPE_SALE_BEHALF_S_OUTSIDE_JP;

        $this->paginate = [
            'conditions' => $options,
            'limit' => $this->request->query('displays') ? $this->request->query('displays') : 10,
            'contain' => [
                'Users',
                'Currencies',
                'Sellers' => ['Suppliers', 'Manufacturers'],
                'PurchaseDetails' => function ($q) {
                    $q->select([
                        'id',
                        'purchase_id',
                        'total_amount' => $q->func()->sum('unit_price * quantity'),
                    ])->group('purchase_id');
                    return $q;
                },
            ],
            'sortWhitelist' => [
                'issue_date',
                'purchase_number',
                'status',
                'Users.lastname' . $this->local,
                'Sellers.id',
            ],
            'order' => ['Purchases.status' => 'asc'],
        ];
        $data = $this->paginate($this->Purchases);
        $local = $this->local;

        $this->set(compact('data', 'local'));
    }

    public function createOrder()
    {
        $this->ajaxRequest(false);
        $data = $this->Purchases->newEntity();
        $this->response->type('json');
        $data = $this->Purchases->patchEntity($data, $this->request->data, [
            'validate' => 'Create',
        ]);

        if ($data->errors()) {
            $this->response->body(json_encode([
                'status' => 0,
                'message' => MSG_ERROR,
                'data' => $this->Function->getError($data->errors()),
            ]));
        } else {
            if (isset($this->request->data['id'])) {
                $data = $this->Purchases->get($this->request->data['id']);
                $data->issue_date = $this->request->data['issue_date'];
                $data->currency_id = $this->request->data['currency_id'];
                $data->seller_id = $this->request->data['seller_id'];
            } else {
                $total = $this->Purchases->countRecord();
                $data->user_id = $this->Auth->user('id');
                $data->status = PO_STATUS_DRAFF;
                $data->purchase_number = date('ymd') . '0F' . str_pad($total + 1, 3, '0', STR_PAD_LEFT);
            }

            if ($this->Purchases->save($data)) {
                $this->response->body(json_encode([
                    'status' => 1,
                    'message' => MSG_SUCCESS,
                ]));
            }
        }

        return $this->response;
    }

    public function rop($id = null)
    {
        if ($id === null) {
            throw new NotFoundException();
        }
        $this->loadModel('PurchaseDetails');
        $this->loadModel('Medias');
        $person = [];

        // get purchase by id with associate data
        $data = $this->Purchases->getById($id, null, [
            'Currencies',
            'Sellers' => ['Manufacturers', 'Suppliers'],
            'PurchaseImports' => ['Imports'],
            'PurchasePayments' => ['Payments'],
        ]);

        // get purchaseDetails
        $pdConditions = ['PurchaseDetails.purchase_id' => $id];
        $purchaseDetails = $this->PurchaseDetails->getAllDataList('threaded', null, $pdConditions);

        // count child record where parent_id IS NOT null
        $countRecords = $this->PurchaseDetails->countRecords([
            'PurchaseDetails.id' => $id,
            'PurchaseDetails.parent_id IS NOT' => null,
        ]);

        // get pdf file from media
        $medias = $this->Medias->getAllDataList('all', null, [
            'Medias.external_id' => $id,
            'Medias.type' => TYPE_ORDER_TO_S,
        ]);

        if ($data) {
            $this->loadModel('PersonInCharges');
            $ext_id = [];

            switch ($data->seller->type) {
                case TYPE_SUPPLIER :
                    $ext_id = [
                        'type' => TYPE_SUPPLIER,
                        'id' => $data->seller->supplier->id,
                    ];
                    break;
                default :
                    $ext_id = [
                        'type' => TYPE_MANUFACTURER,
                        'id' => $data->seller->manufacturer->id
                    ];
            }
            $person = $this->PersonInCharges->getList($this->local, $ext_id['type'], $ext_id['id'], 'id');
        }
        $typePdf = $this->CustomConstant->typePdf();

        $this->set(compact('data', 'person', 'purchaseDetails', 'countRecords', 'medias', 'typePdf'));
    }

    public function statusDownload()
    {
        $this->ajaxRequest(false);
        $this->loadModel('Medias');
        $medias = [];
        $response = [];
        $countNewFile = 0;
        $path = WWW_ROOT . 'img' . DS . 'uploads' . DS . 'po';

        $data = $this->request->data;
        $this->set('_serialize', ['data']);

        if (isset($data['files'])) {
            $countNewFile = count($data['files']);

            // check if new files
            if ($data['files']) {
                foreach ($data['files'] as $key => $value) {
                    $temp = explode('.', $value['name']);
                    $newFile = $this->Image->upload(null, $value, $path);
                    $medias[] = [
                        'external_id' => $data['id'],
                        'type' => TYPE_ORDER_TO_S,
                        'document_type' => $data['types'][$key],
                        'file_name' => $newFile,
                        'file_type' => end($temp),
                    ];
                }
            }

            // check is old files
            if (isset($data['old_files'])) {
                foreach ($data['old_files'] as $key => $value) {
                    $temp = explode('.', $value);
                    $medias[$countNewFile] = [
                        'external_id' => $data['id'],
                        'type' => TYPE_ORDER_TO_S,
                        'document_type' => $data['old_doc_types'][$key],
                        'file_name' => $value,
                        'file_type' => end($temp),
                    ];
                    $countNewFile++;
                }
            }
        }

        if ($medias) {
            $this->Medias->deleteMediaById($data['id'], TYPE_ORDER_TO_S);
            $data1 = $this->Medias->newEntities($medias);
            $this->Medias->saveMany($data1);
        }

        $purchase = $this->Purchases->get($data['id']);
        $purchase->status = $data['status'];

        if ($this->Purchases->save($purchase)) {
            $response = [
                'step' => $data['step'] + 1,
                'status' => $purchase->status,
            ];
        }

        $this->Function->ajaxResponse(1, MSG_SUCCESS, $response);
    }

    public function advanceSearch()
    {
        $this->ajaxRequest(true);
        $this->loadModel('Sellers');
        $id = $this->request->query('seller_id');
        $en = $this->local;

        $seller = $this->Sellers->findById($id)
            ->contain([
                'SellerBrands' => ['ProductBrands']
            ])->first();

        $this->set(compact('seller', 'en'));
    }

    public function getFaxRop()
    {
        $this->ajaxRequest(true);
        $local = $this->local;
        $id = $this->request->query('id');
        $this->set(compact('local', 'id'));
    }

    public function changeDraffStatus()
    {
        $this->ajaxRequest(false);
        $this->response->type('json');
        $purchase = $this->Purchases->get($this->request->data['id']);
        $purchase->status = STATUS_FAXED_ROP;
        if ($this->Purchases->save($purchase)) {
            $this->response->body(json_encode([
                'status' => 1,
                'message' => MSG_SUCCESS,
            ]));
        }
        return $this->response;
    }

    public function updateStatus()
    {
        $this->ajaxRequest(false);
        $this->loadModel('SaleReceivePayments');
        $data = $this->Purchases->get($this->request->data['id']);
        $data->status = $this->request->data['status'];

        if ($this->Purchases->save($data)) {
            $response = [
                'step' => $this->request->data['step'],
                'status' => $data->status,
            ];
            $this->Function->ajaxResponse(1, MSG_SUCCESS, $response);
        }
    }

    public function getProductByBrandId()
    {
        $this->ajaxRequest(false);
        $this->loadModel('Products');
        $brandId = $this->request->query('brandId');
        $products = $this->Products->findByProductBrandId($brandId)->toArray();
        $data = $this->ReconstructData->convertToSelect2($products, $this->local);

        $this->Function->ajaxResponse(1, MSG_SUCCESS, $data);
    }

    public function getProductDetailByProductId()
    {
        $this->ajaxRequest(true);
        $this->loadModel('ProductDetails');
        $local = $this->local;
        $productId = $this->request->query('productId');
        $data = $this->ProductDetails->findByProductId($productId)
            ->contain([
                'Products' => ['ProductBrands'],
                'SingleUnits',
                'PackSizes',
            ])
            ->toArray();

        $this->set(compact('data', 'local'));
        $this->render('get_product_detail');
    }

    public function getForm()
    {
        $this->ajaxRequest(true);
        $this->loadModel('Sellers');
        $this->loadModel('Currencies');
        $data = [];
        $request = $this->request->query;

        if ($request['type'] === 'edit') {
            $data = $this->Purchases->get($request['id']);
        }

        $sellers = ['' => __('TXT_SELECT_SELLER')] + $this->Sellers->getSellerList();
        $currencies = ['' => __('TXT_SELECT_CURRENCY')] + $this->Currencies->getDropdown('list')->toArray();

        $this->set(compact('data', 'sellers', 'currencies'));
    }

    /**
     * this function use when register sale from order managements form
     * @param type $id
     */
    public function registerSale($id = null)
    {
        $this->ajaxRequest(false);
        $request = $this->ReconstructData->convertToObjFormVue($this->request->data['data']);
        $data = $this->Purchases->newEntity();
        $this->response->type('json');
        $data = $this->Purchases->patchEntity($data, $request, [
            'validate' => 'Create',
        ]);

        if ($data->errors()) {
            $this->response->body(json_encode([
                'status' => 0,
                'message' => MSG_ERROR,
                'data' => $this->Function->getError($data->errors()),
            ]));
        } else {
            if (isset($request['id'])) {
                $data = $this->Purchases->get($request['id']);
                $data->issue_date = $request['issue_date'];
                $data->currency_id = $request['currency_id'];
                $data->seller_id = $request['seller_id'];
            } else {
                $total = $this->Purchases->countRecord();
                $data->user_id = $this->Auth->user('id');
                $data->status = PO_STATUS_DRAFF;
                $data->purchase_number = date('ymd') . '0F' . str_pad($total + 1, 3, '0', STR_PAD_LEFT);
            }

            if ($this->Purchases->save($data)) {
                $this->response->body(json_encode([
                    'status' => 1,
                    'message' => MSG_SUCCESS,
                ]));
            }
        }

        return $this->response;
    }

    public function createForm()
    {
        $this->ajaxRequest(true);
        $this->loadModel('Sellers');
        $this->loadModel('Currencies');
        $data = [];
//        $request = $this->request->query;
//
//        if ($request['type'] === 'edit') {
//            $data = $this->Purchases->get($request['id']);
//        }

        $sellers = ['' => __('TXT_SELECT_SELLER')] + $this->Sellers->getSellerList();
        $currencies = ['' => __('TXT_SELECT_CURRENCY')] + $this->Currencies->getDropdown('list')->toArray();

        $this->set(compact('data', 'sellers', 'currencies'));
    }
}
