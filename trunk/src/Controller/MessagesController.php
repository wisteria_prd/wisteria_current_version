<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Network\Exception\NotFoundException;
use Cake\Utility\Hash;

class MessagesController extends AppController
{
    private $locale;
    private $auth;

    public function initialize()
    {
        parent::initialize();
        $this->locale = $this->request->session()->read('tb_field');
        $this->auth = $this->Auth->user();
    }

    private function checkSaveAssociate()
    {
        $this->loadModel('Users');
        $users = $this->Users->find('list')->toArray();
        $message_receivers = [];
        if ($this->request->data('is_sent_to_all')) {
            $this->request->data['message_receivers'] = array_values($users);
        }
        if (empty($this->request->data['message_receivers'])) {
            return [];
        }
        foreach ($this->request->data['message_receivers'] as $key => $value) {
            $message_receivers[$key] = [
                'user_id' => $value,
            ];
        }
        $this->request->data['message_receivers'] = $message_receivers;
    }

    public function register()
    {
        $message = $this->Messages->newEntity();
        $selected_receivers = [];
        if ($this->request->is('post')) {
            $message->user_id = $this->Auth->user('id');
            $message->external_id = $this->request->query('external_id');
            $message->type = $this->request->query('type');
            $this->checkSaveAssociate();
            $message = $this->Messages->patchEntity($message, $this->request->data);
            // CHECK FILE TYPE
            $file_type = $this->checkFileType($this->request->data['file']);
            if ($file_type) {
                $message->file = $file_type['file'];
                $message->mime_type = $file_type['mime_type'];
            }
            if ($this->Messages->save($message, [
                'associate' => 'MessageReceivers',
                'validate' => false,
            ])) {
                $this->redirect('/'.$this->request->query('referer'));
            } else {
                $selected_receivers = $this->request->data('message_receivers');
            }
        }
        $this->loadModel('MessageCategories');
        $this->loadModel('Users');
        $categories = $this->MessageCategories->find('list', [
            'keyField' => 'id',
            'valueField' => 'name',
        ])->where(['MessageCategories.is_deleted <>' => 1]);
        $users = $this->Users->find('list', [
            'keyField' => 'id',
            'valueField' => function ($user) {
                return $user->get('FullName');
            },
            'conditions' => ['Users.id <>' => $this->Auth->user('id')],
        ]);
        $data = [
            'selected_receivers' => $selected_receivers,
            'message' => $message,
            'auth' => $this->auth,
            'categories' => $categories,
            'users' => $users,
        ];
        $this->set($data);
    }

    public function edit($id = null)
    {
        if ($id === null) {
            throw new NotFoundException();
        }
        $message = $this->Messages->findById($id)
                ->contain(['MessageReceivers'])
                ->first();
        $selected_receivers = [];
                // pr($message->message_receivers);
                // pr(Hash::extract($message->message_receivers, '{0}.id'));
        $file = $message->file;
        // CHECK REQUEST TYPE
        if ($this->request->is(['post', 'put'])) {
            $this->checkSaveAssociate();
            $message = $this->Messages->patchEntity($message, $this->request->data);
            // CHECK FILE TYPE
            $file_type = $this->checkFileType($this->request->data['file']);
            if ($file_type) {
                $message->file = $file_type['file'];
                $message->mime_type = $file_type['mime_type'];
            } else {
                $message->file = $file;
            }

            if ($message->errors()) {
                // Do nothing
                $selected_receivers = $this->request->data('message_receivers');
            } else {
                $this->loadModel('MessageReceivers');
                $this->MessageReceivers->deleteAll([
                    'message_id' => $message->id,
                ]);
            }
            if ($this->Messages->save($message)) {
                $this->redirect('/'.$this->request->query('referer'));
            }
        } else {
            // pr((array) $message);
            $this->loadModel('MessageReceivers');
            $receivers = $this->MessageReceivers->find('list', [
                'keyField' => 'user_id',
                'valueField' => 'user_id',
            ])
            ->where(['MessageReceivers.message_id' => $message->id])->toArray();
            $this->request->data['message_receivers'] = array_values($receivers);
            $this->request->data['is_sent_to_all'] = $message->is_sent_to_all;
            $this->checkSaveAssociate();
            $selected_receivers = $this->request->data['message_receivers'];
        }
        $this->loadModel('MessageCategories');
        $this->loadModel('Users');
        $categories = $this->MessageCategories->find('list', [
            'keyField' => 'id',
            'valueField' => 'name',
        ])->where(['MessageCategories.is_deleted <>' => 1]);
        $users = $this->Users->find('list', [
            'keyField' => 'id',
            'valueField' => function ($user) {
                return $user->get('FullName');
            },
            'conditions' => ['Users.id <>' => $this->Auth->user('id')],
        ]);
        $data = [
            'selected_receivers' => $selected_receivers,
            'message' => $message,
            'auth' => $this->auth,
            'categories' => $categories,
            'users' => $users,
        ];
        $this->set($data);
    }

    public function changeOfStatus()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->autoRender = false;
        $this->response->disableCache();
        $this->response->type('json');
        $this->viewBuilder()->layout(false);
        $message = $this->Messages->get($this->request->data['id']);
        $message->status = $this->request->data['status'];
        if ($this->Messages->save($message)) {
            $this->response->body(json_encode([
                'status' => 1,
                'message' => MSG_SUCCESS,
            ]));
            return $this->response;
        }
    }

    public function getStatus()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->viewBuilder()->layout(false);
        $data = [
            'id' => $this->request->query('id'),
        ];
        $this->set($data);
    }

    public function view()
    {
        $params = new \stdClass();
        $params->external_id = $this->request->query('external_id');
        $params->msg_id = $this->request->query('msg_id');
        $params->type = $this->request->query('type');
        // CHECK PARAMS IS EMPTY
        if (empty($params->external_id) || empty($params->msg_id) || empty($params->type)) {
            throw new NotFoundException();
        }
        $this->loadModel('Replies');
        $this->loadModel('MessageReceivers');
        $message = $this->Messages->find()
            ->contain([
                'Users',
                'MessageCategories',
                'MessageReceivers' => ['Users'],
            ])
            ->where([
                'Messages.id' => $params->msg_id,
                'Messages.external_id' => $params->external_id,
                'Messages.type' => $params->type,
            ])
            ->first();
        if (!$message) {
            throw new NotFoundException();
        }
        $replies = $this->Replies->find()
                ->contain(['Users'])
                ->where([
                    'Replies.message_id' => $message->id,
                    'Replies.is_deleted' => 0,
                ])
                ->all();
        $receiver = $this->MessageReceivers->find()
                ->where([
                    'user_id' => $this->auth['id'],
                    'message_id'=> $message->id,
                ])
                ->first();
        // CHECK RECEIVERS AND UPDATE IS_READ = 1
        if ($receiver) {
            $receiver->is_read = 1;
            $this->MessageReceivers->save($receiver);
        }
        $data = [
            'message' => $message,
            'receiver' => $receiver,
            'replies' => $replies,
            'auth' => $this->auth,
        ];
        $this->set($data);
    }

    public function filterMessage()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->viewBuilder()->layout('ajax');
        $data = [];
        $params = new \stdClass();
        $params->external_id = $this->request->query('id');
        $params->type = $this->request->query('type');
        // FILTER MESSAGE BASED ON TYPE OF TAB
        switch ($this->request->query('tab')) {
            case MESSAGE_TYPE:
                $condition1 = [
                    'Messages.external_id' => $params->external_id,
                    'Messages.type' => $params->type,
                    'Messages.user_id' => $this->auth['id'],
                ];
                if (!empty($this->request->query('keyword'))) {
                    $condition1['Messages.title LIKE'] = '%' . $this->request->query('keyword') . '%';
                }
                if ((int)$this->request->query('status') != -1) {
                    $condition1['Messages.status'] = (int)$this->request->query('status');
                }
                $data = $this->getAllMessages($params, $condition1);
                break;
            default:
                $condition2 = [];
                if (!empty($this->request->query('keyword'))) {
                    $condition2['Messages.title LIKE'] = '%' . $this->request->query('keyword') . '%';
                }
                if ((int)$this->request->query('status') != -1) {
                    $condition2['Messages.status'] = (int)$this->request->query('status');
                }
                $data = $this->getAllReceivers($condition2, $params);
        }
        $response = [
            'auth' => $this->auth,
            'data' => $data,
            'id' => $this->request->query('id'),
            'tab' => $this->request->query('tab'),
            'type' => $this->request->query('type'),
            'referer' => $this->request->query('referer'),
        ];
        $this->set($response);
        $this->render('get_tabs');
    }

    public function getMessageList()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->viewBuilder()->layout('ajax');
        $this->loadModel('MessageReceivers');
        $params = new \stdClass();
        $params->external_id = $this->request->query('id');
        $params->type = $this->request->query('type');
        $data = $this->getAllMessages($params);
        $receiverCount = $this->getAllReceivers([], $params, 'count');
        $response = [
            'id' => $this->request->query('id'),
            'type' => $this->request->query('type'),
            'referer' => $this->request->query('referer'),
            'data' => $data,
            'auth' => $this->auth,
            'receiverCount' => $receiverCount,
        ];
        $this->set($response);
    }

    // Get all messages sent to you
    public function getTabs()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->loadModel('MessageReceivers');
        $this->viewBuilder()->layout('ajax');
        $data = [];
        $params = new \stdClass();
        $params->external_id = $this->request->query('id');
        $params->type = $this->request->query('type');
        // CHECK TYPE
        switch ($this->request->query('tab')) {
            // MESSAGE
            case MESSAGE_TYPE:
                $data = $this->getAllMessages($params);
                break;
            // RECEIVER
            default:
                $data = $this->getAllReceivers([], $params);
        }
        $response = [
            'auth' => $this->auth,
            'data' => $data,
            'id' => $params->external_id,
            'tab' => $this->request->query('tab'),
            'type' => $params->type,
            'referer' => $this->request->query('referer'),
        ];
        $this->set($response);
    }

    // Get all message
    private function getAllMessages($params, $conditions = [])
    {
        $this->loadModel('MessageReceivers');
        $subquery = $this->MessageReceivers->find()
            ->select(['message_id'])
            ->where(['MessageReceivers.user_id' => $this->auth['id']]);
            // pr($subquery->all());exit;
        $messages = $this->Messages->find()
            ->contain([
                'MessageCategories',
                'MessageReceivers' => ['Users'],
                'Replies' => function($q) {
                    $q->select([
                        'Replies.message_id',
                        'total_reply' => $q->func()->count('Replies.id')
                    ])->where(['Replies.is_deleted' => 0]);
                    return $q;
                }
            ]);
        // For filter
        if ($conditions) {
            $messages = $messages->where($conditions)
            ->orWhere([
                'Messages.external_id' => $params->external_id,
                'Messages.type' => $params->type,
                'Messages.status <>' => 0,
                'Messages.id IN' => $subquery
            ]);
        } else {
            // Get message receivers
            // get all message created by him and receive message
            $message = $messages->where([
                'Messages.external_id' => $params->external_id,
                'Messages.type' => $params->type,
                'Messages.status <>' => 0,
                'Messages.user_id' => $this->auth['id'],
            ])->orWhere([
                'Messages.external_id' => $params->external_id,
                'Messages.type' => $params->type,
                'Messages.status <>' => 0,
                'Messages.id IN' => $subquery,
            ]);
        }
        $message = $messages->order(['Messages.status' => 'asc'])->all();
        return $messages;
    }

    private function getAllReceivers($conditions = [], $params = [], $mode = null)
    {
        $this->loadModel('MessageReceivers');
        $subquery = $this->MessageReceivers->find()
            ->select(['message_id'])
            ->where(['MessageReceivers.is_read <>' => 1])
            ->where(['MessageReceivers.user_id' => $this->auth['id']]);
        $messages = $this->Messages->find()
            ->contain(['MessageCategories'])
            ->contain([
                'Replies' => function($q) {
                    $q->select([
                        'Replies.message_id',
                        'total_reply' => $q->func()->count('Replies.id')
                    ])->where(['Replies.is_deleted' => 0]);
                    return $q;
                }
            ])
            ->contain([
                'MessageReceivers' => ['Users']
            ])
            ->where(['Messages.external_id' => $params->external_id])
            ->where(['Messages.type' => $params->type])
            ->where(['Messages.status <>' => 0])
            ->where(['Messages.id IN' => $subquery]);
        if ($conditions) {
            $messages->where($conditions);
        }
        switch ($mode) {
            case 'count':
                $messages = $messages->count();
                break;

            case 'all':
            default:
                $messages = $messages->all();
                break;
        }
        return $messages;
    }

    private function checkFileType($file = [])
    {
        if (!$file || ($file === 'undefined')) {
            return false;
        }
        if (!is_uploaded_file($file['tmp_name'])) {
            return false;
        }
        $tmp = explode('.', $file['name']);
        $fname = date('YmdHis') . '_'
                . md5($file['name']) . '.'
                . end($tmp);
        // UPLOAD FILE
        $path = WWW_ROOT . 'img/uploads' . DS . 'messages' . DS . $fname;
        if (!move_uploaded_file($file['tmp_name'], $path)) {
            return false;
        }
        $data = [
            'file' => $fname,
            'mime_type' => end($tmp),
        ];
        return $data;
    }
}
