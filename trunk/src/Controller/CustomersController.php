<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Network\Exception\NotFoundException;

class CustomersController extends AppController
{
    public $field;
    private $locale;

    public function initialize()
    {
        parent::initialize();
        $this->field = 'name' . $this->request->session()->read('tb_field');
        $this->loadModel('InfoDetails');
        $this->locale = $this->request->session()->read('tb_field');
    }

    public function index()
    {
        $options = [];
        if ($this->request->query('keyword')) {
            $keyword = trim($this->request->query('keyword'));
            $options[] = [
                'OR' => [
                    'Customers.name_en LIKE' => '%' .$keyword .'%',
                    'Customers.name LIKE' => '%' .$keyword .'%',
                    'MedicalCorporations.name LIKE' => '%' . $keyword . '%',
                    'MedicalCorporations.name_en LIKE' => '%' . $keyword . '%'
                ]
            ];
        }
        $displays = 10;
        if ($this->request->query('displays')) {
            $displays = $this->request->query('displays');
        }
        if ($this->request->query('inactive')) {
            $options[] = [
                'Customers.is_suspend' => 1,
            ];
        }
        $this->paginate = [
            'conditions' => $options,
            'limit' => $displays,
            'order' => [
                $this->field => 'asc',
            ],
            'contain' => [
                'MedicalCorporations' => ['joinType' => 'LEFT'],
                'PaymentCategories',
                'InvoiceDeliveryMethods',
                'PersonInCharges' => function($q) {
                    return $q->where(['PersonInCharges.type' => TYPE_CUSTOMER]);
                },
                'ClinicDoctors' => function($q) {
                    $q->select([
                        'ClinicDoctors.id',
                        'ClinicDoctors.doctor_id',
                        'ClinicDoctors.customer_id',
                        'count_doctor' => $q->func()->count('ClinicDoctors.doctor_id')
                    ])->group(['ClinicDoctors.doctor_id']);
                    return $q;
                },
            ],
            'sortWhitelist' => [
                'Customers.type',
                'Customers.code',
                'Customers.name',
                'Customers.name_en',
                'Customers.reciever_name',
                'Customers.payment_category_id',
                'Customers.invoice_delivery_id',
                'MedicalCorporations.name',
                'MedicalCorporations.name_en',
            ],
        ];
        $data = [
            'customer' => $this->paginate($this->Customers),
            'locale' => $this->locale,
            'paging' => $this->request->param('paging')['Customers']['pageCount'],
            'displays' => $displays,
        ];

        $this->set($data);
    }

    public function create()
    {
        $this->loadModel('Countries');
        $data = [
            'locale' => $this->locale,
            'txt_register' => $this->Common->txtRegister(),
            'payments' => $this->getPaymentCategoriesList(),
            'invoices' => $this->getInvoiceDeliveryList(),
            'types' => $this->CustomConstant->customerType(),
            'customer' => $this->Customers->newEntity(),
            'countries' => $this->Countries->getListOfCountries($this->locale),
        ];
        $this->set($data);
    }

    public function edit($id = null)
    {
        if ($id === null) {
            throw new NotFoundException();
        }
        $this->loadModel('Countries');
        $customer = $this->Customers->findById($id)
                ->contain([
                    'ClinicDoctors' => [
                        'Doctors',
                    ],
                    'CustomerSubsidiaries' => [
                        'Subsidiaries',
                    ],
                    'InfoDetails' => function ($q) use ($id) {
                        return $q->where([
                            'external_id' => $id,
                            'type' => TYPE_CUSTOMER,
                        ]);
                    }
                ])
                ->firstOrFail();
        $data = [
            'locale' => $this->locale,
            'txt_edit' => $this->Common->txtEdit(),
            'payments' => $this->getPaymentCategoriesList(),
            'invoices' => $this->getInvoiceDeliveryList(),
            'types' => $this->CustomConstant->customerType(),
            'customer' => $customer,
            'countries' => $this->Countries->getListOfCountries($this->locale),
        ];
        $this->set($data);
    }

    public function saveAndUpdate()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->viewBuilder()->layout('ajax');
        $this->autoRender = false;
        $this->response->type('json');
        $this->response->disableCache();
        if ($this->request->data('id')) {
            $customer = $this->Customers->get($this->request->data['id']);
        } else {
            $customer = $this->Customers->newEntity();
            $customer->user_id = $this->Auth->user('id');
        }
        $response = $this->Customers->patchEntity($customer, $this->request->data);
        if ($response->errors()) {
            $this->response->body(json_encode([
                'status' => 0,
                'message' => MSG_ERROR,
                'data' => $response->errors(),
            ]));
            return $this->response;
        }
        // Save and edit record
        if ($this->Customers->save($response)) {
            $this->updateInfoDetails($this->request->data, $response->id);
            $this->updateCustomerSubsidiaries($this->request->data, $response->id);
            $this->updateClinicDoctor($this->request->data, $response->id);
            $this->response->body(json_encode([
                'status' => 1,
                'message' => MSG_SUCCESS,
            ]));
            return $this->response;
        }
    }

    public function delete()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->viewBuilder()->layout('ajax');
        $this->autoRender = false;
        $data = $this->Customers->get($this->request->data['id']);
        if ($this->Customers->delete($data)) {
            echo json_encode([
                'status' => 1,
                'message' => 'success',
            ]);
        }
    }

    public function view()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->viewBuilder()->layout('ajax');
        $customer = $this->Customers->findById($this->request->query('id'))->contain([
            'Countries',
            'MedicalCorporations',
            'CustomerSubsidiaries' => ['Subsidiaries'],
            'InfoDetails' => function($q) {
                return $q->where([
                    'InfoDetails.type' => TYPE_CUSTOMER,
               ]);
            },
            'PersonInCharges' => function($q) {
               return $q->where([
                    'PersonInCharges.type' => TYPE_CUSTOMER,
               ]);
            },
            'ClinicDoctors' => function($q) {
                $q->select([
                    'ClinicDoctors.doctor_id',
                    'ClinicDoctors.customer_id',
                    'count_doctor' => $q->func()->count('ClinicDoctors.doctor_id')
                ])->group(['ClinicDoctors.doctor_id']);
                return $q;
            },
        ])->first();
        $payment_name = $this->getPaymentCategoriesList()->toArray();
        $invoice_name = $this->getInvoiceDeliveryList()->toArray();
        $data = [
            'locale' => $this->locale,
            'customer' => $customer,
            'payment_name' => $payment_name[$customer->payment_category_id],
            'invoice_name' => $invoice_name[$customer->invoice_delivery_method_id],
        ];

        $this->set($data);
    }

    public function getPaymentCategoriesList()
    {
        $this->loadModel('PaymentCategories');
        $payments = $this->PaymentCategories->find()
                ->order(['name' => 'asc'])
                ->all();
        return $payments;
    }

    public function getInvoiceDeliveryList()
    {
        $this->loadModel('InvoiceDeliveryMethods');
        $data = $this->InvoiceDeliveryMethods->find()
                ->order(['name' => 'asc'])
                ->all();
        return $data;
    }

    public function updateSuspend()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->autoRender = false;
        $this->response->disableCache();
        $this->response->type('json');
        $this->viewBuilder()->layout(false);
        $data = $this->Customers->get($this->request->data['id']);
        $data->is_suspend = ($this->request->data['is_suspend'] == 0 ) ? 1 : 0;
        if ($this->Customers->save($data)) {
            $this->response->body(json_encode([
                'status' => 1,
                'message' => 'success'
            ]));
            return $this->response;
        }
        $this->response->body(json_encode([
            'status' => 0,
            'message' => 'error'
        ]));
        return $this->response;
    }

    // GET CUSTOMERS
    public function getCustomerBySearch()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->viewBuilder()->layout('ajax');
        $this->autoRender = false;
        $this->response->type('json');
        $data = $this->Customers->find()
                ->where(['is_suspend' => 0])
                ->where(['OR' => [
                            'name_en LIKE' => '%' . $this->request->query('keyword') . '%',
                            'name LIKE' => '%' . $this->request->query('keyword') . '%',
                        ]
                    ])
                ->limit(PAGE_LIMIT_NUMBER)
                ->all();
        $this->response->body(json_encode([
            'status' => '1',
            'message' => MSG_SUCCESS,
            'data' => $data,
        ]));
        return $this->response;
    }

    public function getCustomerUpdateName()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->autoRender = false;
        $this->response->type('json');
        $data = '';
        $customer = $this->Customers->get($this->request->query('id'));
        if (!$customer) {
            $this->response->body(json_encode([
                'status' => 0,
                'data' => [],
                'message' => MSG_ERROR
            ]));
            return $this->response;
        }
        switch ($this->request->query('id')) {
            case 'address':
                list($address, $jp_addr2, $en_addr2, $phone, $fax) = $this->Common->addressFormat(
                    $this->request->query('en'),
                    $customer,
                    $this->request->query('is_mp')
                );
                if (mb_substr($address, 0, 1) == '〒') {
                    $data .= $address;
                    $data .= "<br />" . $jp_addr2;
                } else {
                    $data .= $address;
                    $data .= "<br />" . $en_addr2;
                }
                break;

            case 'name':
                $data = $this->Common->nameEnOrJp($customer->name, $customer->name_en, $this->request->query('en'));
                break;

            case 'fax':
                $data = $customer->fax;
                break;
        }
        $this->response->body(json_encode([
            'status' => 1,
            'data' => ['value' => $data],
            'message' => MSG_SUCCESS
        ]));
        return $this->response;
    }

    public function getCustomerById()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->viewBuilder()->layout('ajax');
        $this->response->type('json');
        $customer = $this->Customers->findById($this->request->query('customer_id'))->first();
        $this->response->body(json_encode([
            'status' => '1',
            'message' => MSG_SUCCESS,
            'keyword' => $this->request->query('keyword'),
            'data' => $customer,
        ]));
        return $this->response;
    }

    public function getAutocomplete()
    {
         if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->viewBuilder()->layout('ajax');
        $this->autoRender = false;
        $this->response->type('json');
        $this->response->disableCache();
        $customers = $this->Customers->find()
                ->where(['is_suspend' => 0])
                ->where(['OR' => [
                        'name_en LIKE' => '%' . $this->request->query('keyword') . '%',
                        'name LIKE' => '%' . $this->request->query('keyword') . '%',
                ]])
                ->limit(20)
                ->all();
        $this->response->body(json_encode([
            'status' => '1',
            'message' => MSG_SUCCESS,
            'data' => $customers,
        ]));
        return $this->response;
    }

    private function updateInfoDetails($data, $customerId = null)
    {
        if ($customerId === null) {
            return false;
        }
        $this->InfoDetails->deleteAll([
            'external_id' => $customerId,
            'type' => TYPE_CUSTOMER,
        ]);
        //pr($data['info_details']);exit;
        $info_details = [];
        if (isset($data['info_details'])) {
            foreach ($data['info_details'] as $key => $value) {
                //pr($value);exit;
                $info_details[] = [
                    'external_id' => $customerId,
                    'phone' => isset($value['phone']) ? $value['phone'] : null,
                    'tel' => isset($value['tel']) ? $value['tel'] : null,
                    'tel_extension' => isset($value['tel_extension']) ? $value['tel_extension'] : null,
                    'email' => isset($value['email']) ? $value['email'] : null,
                    'type' => TYPE_CUSTOMER,
                ];
            }
        }
        if (!$info_details) {
            return false;
        }
        $entities = $this->InfoDetails->newEntities($info_details);
        if ($this->InfoDetails->saveMany($entities)) {
            return true;
        }
        return true;
    }

    private function updateClinicDoctor($data, $customerId = null)
    {
        if ($customerId === null) {
            return false;
        }
        $this->loadModel('ClinicDoctors');
        $this->ClinicDoctors->deleteAll([
            'customer_id' => $customerId,
        ]);
        $clinic_doctors = [];
        if (isset($data['clinic_doctors'])) {
            foreach ($data['clinic_doctors'] as $key => $value) {
                $clinic_doctors[] = [
                    'customer_id' => $customerId,
                    'doctor_id' => $value['doctor_id'],
                    'position' => $value['position'],
                    'is_priority' => $value['is_priority'],
                ];
            }
        }
        if (!$clinic_doctors) {
            return false;
        }
        $entities = $this->ClinicDoctors->newEntities($clinic_doctors);
        if ($this->ClinicDoctors->saveMany($entities)) {
            return true;
        }
        return true;
    }

    private function updateCustomerSubsidiaries($data, $customerId = null)
    {
        if ($customerId === null) {
            return false;
        }
        $this->loadModel('CustomerSubsidiaries');
        $this->CustomerSubsidiaries->deleteAll([
            'customer_id' => $customerId,
        ]);
        $customer_subsidiaries = [];
        if (isset($data['customer_subsidiaries'])) {
            foreach ($data['customer_subsidiaries'] as $key => $value) {
                $customer_subsidiaries[] = [
                    'customer_id' => $customerId,
                    'subsidiary_id' => $value['subsidiary_id'],
                ];
            }
        }
        if (!$customer_subsidiaries) {
            return false;
        }
        $entities = $this->CustomerSubsidiaries->newEntities($customer_subsidiaries);
        if ($this->CustomerSubsidiaries->saveMany($entities)) {
            return true;
        }
        return true;
    }
}
