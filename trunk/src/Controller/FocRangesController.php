<?php
namespace App\Controller;

use App\Controller\AppController;

class FocRangesController extends AppController
{
    public function initialize()
    {
        parent::initialize();
    }

    public function create()
    {
        $foc_range = $this->FocRanges->newEntity();
        if ($this->request->is('ajax')) {
            $this->autoRender = false;
            $this->response->disableCache();
            $this->response->type('json');
            $this->viewBuilder()->layout(false);

            $foc_range = $this->FocRanges->patchEntity($foc_range, $this->request->data);
            if ($this->FocRanges->save($foc_range)) {
                $this->response->body(json_encode(['status' => 1]));
                return $this->response;
            }

            $this->response->body(json_encode(['status' => 0, 'message' => $foc_range->errors()]));
            return $this->response;
        }
        $this->set(compact('foc_range'));
        $this->set('_serialize', ['foc_range']);
    }

    public function edit()
    {
        if ($this->request->is('ajax')) {
            $this->autoRender = false;
            $this->response->disableCache();
            $this->response->type('json');
            $this->viewBuilder()->layout(false);

            $foc_range = $this->FocRanges->get($this->request->data['id']);
            $foc_range = $this->FocRanges->patchEntity($foc_range, $this->request->data);
            if ($this->FocRanges->save($foc_range)) {
                $this->response->body(json_encode(['status' => 1]));
                return $this->response;
            }

            $this->response->body(json_encode(['status' => 0, 'message' => $foc_range->errors()]));
            return $this->response;
        }
    }

    public function delete()
    {
        if ($this->request->is('ajax')) {
            $this->autoRender = false;
            $this->response->disableCache();
            $this->response->type('json');
            $this->viewBuilder()->layout(false);

            $this->request->allowMethod(['post', 'delete']);
            $foc_range = $this->FocRanges->get($this->request->data['id']);
            if ($this->FocRanges->delete($foc_range)) {
                $this->response->body(json_encode(['status' => 1]));
                return $this->response;
            }
            $this->response->body(json_encode(['status' => 0, 'message' => __('Cannot remove current item. Please try again.')]));
            return $this->response;
        }
    }
}