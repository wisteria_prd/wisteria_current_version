<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Routing\Router;

class AgentsController extends AppController
{
    public function initialize()
    {
        parent::initialize();
    }

    public function finalAgreement()
    {
        if ($this->request->is('ajax')) {
            $this->viewBuilder()->layout(false);
            $this->response->disableCache();
            $this->response->type('json');

            $user = $this->Auth->user();
            if (!$user['id']) {
                return $this->redirect(['controller' => 'users', 'action' => 'login']);
            } else {
                if ($user['affiliation_class'] == CLASS_AGENT) {
                    $this->loadModel('Users');
                    $user = $this->Users->get($user['id'], [
                        'contain' => ['Agents']
                    ]);
                    $this->request->data['activated'] = 1; //user rich final activation.
                    $save_agent = $this->Agents->patchEntity($user['agent'], $this->request->data['agents']);
                    $new_agent = $this->Agents->save($save_agent);
                    if ($new_agent) {
                        $save_user = $this->Users->patchEntity($user, $this->request->data);
                        $this->Users->save($save_user);
                        $this->Flash->alertSuccess(__('TXT_ACTIVATE_SUCCESS'));
                        $this->response->body(json_encode([
                            'status' => 'OK',
                            'msg' => __('TXT_MESSAGE_CREATE'),
                            'url' => Router::url(['controller' => 'users', 'action' => 'index'])
                        ]));
                        return $this->response;
                    }
                    $this->response->body(json_encode([
                        'status' => 'ERROR',
                        'msg' => __('TXT_MESSAGE_ERROR')
                    ]));
                    return $this->response;
                }
            }
        }
    }
}
