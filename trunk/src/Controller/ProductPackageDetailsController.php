<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Network\Exception\NotFoundException;

class ProductPackageDetailsController extends AppController
{
    public $locale;

    public function initialize()
    {
        parent::initialize();
        $this->loadModel('Countries');
        $this->loadModel('InfoDetails');
        $this->locale = $this->request->session()->read('tb_field');
    }

    public function index()
    {
        if (!$this->request->query('pkg_id')) {
            throw new NotFoundException(__('Product package id not found'));
        }
        $keyword = '';
        $options = [];
        $package_id = $this->request->query('pkg_id');
        $options = [
            'ProductPackageDetails.product_package_id' => $package_id,
        ];
        if ($this->request->query('keyword')) {
            $keyword = trim($this->request->query('keyword'));
        }
        if ($this->request->query('inactive')) {
            $options[] = [
                'ProductPackageDetails.is_suspend' => 1,
            ];
        }
        $this->paginate = [
            'contain' => [
                'ProductPackages' => ['Currencies'],
                'ProductDetails' => [
                    'Products' => [
                        'joinType' => 'LEFT',
                        'queryBuilder' => function($q) use ($keyword) {
                            $con = [
                                'Products.is_suspend' => 0,
                            ];
                            if ($keyword) {
                                $con = [
                                    'OR' => [
                                        'Products.name LIKE' => '%'.$keyword.'%',
                                        'Products.name_en LIKE' => '%'.$keyword.'%',
                                    ],
                                ];
                            }
                            return $q->where($con);
                        },
                        'ProductBrands',
                    ],
                    'PackSizes',
                    'SingleUnits',
                    'WStandardPrices' => [
                        'Currencies'
                    ]
                ]
            ],
            'conditions' => $options,
            'group' => ['ProductPackageDetails.id'],
            'order' => ['ProductPackageDetails.name' => 'asc']
        ];
        $paging = $this->request->param('paging')['ProductPackageDetails']['pageCount'];
        $data = [
            'data' => $this->paginate($this->ProductPackageDetails),
            'locale' => $this->locale,
            'paging' => $paging,
        ];
        $this->set($data);
    }

    public function create()
    {
        $product_package_details = $this->ProductPackageDetails->newEntity();
        if ($this->request->is('ajax')) {
            $this->autoRender = false;
            $this->response->disableCache();
            $this->response->type('json');
            $this->viewBuilder()->layout(false);

            $product_package_details = $this->ProductPackageDetails->patchEntity($product_package_details, $this->request->data);
            if ($this->ProductPackageDetails->save($product_package_details)) {
                $this->response->body(json_encode(['status' => 1]));
                return $this->response;
            }

            $this->response->body(json_encode(['status' => 0, 'message' => $product_package_details->errors()]));
            return $this->response;
        }

        $txt_register = $this->Common->txtRegister();
        $this->set(compact('product_package_details', 'txt_register'));
        $this->set('_serialize', ['product_package_details']);
    }

    public function edit($id = null)
    {
        $product_package_details = $this->ProductPackageDetails->get($id, [
            'contain' => [
                'WStandardPrices'
            ]
        ]);

        if ($this->request->is('ajax')) {
            $this->autoRender = false;
            $this->response->disableCache();
            $this->response->type('json');
            $this->viewBuilder()->layout(false);

            $product_package_details = $this->ProductPackageDetails->patchEntity($product_package_details, $this->request->data);
            if ($this->ProductPackageDetails->save($product_package_details)) {
                $this->response->body(json_encode(['status' => 1]));
                return $this->response;
            }

            $this->response->body(json_encode(['status' => 0, 'message' => $product_package_details->errors()]));
            return $this->response;
        }

        $txt_edit = $this->Common->txtEdit();
        $this->set(compact('product_package_details', 'txt_edit'));
        $this->set('_serialize', ['product_package_details']);
    }

    public function searchProductList()
    {
        if ($this->request->is('ajax')) {
            $this->loadModel('Manufacturers');
            $en = $this->request->session()->read('tb_field');
            //$menufacturers = $this->Manufacturers->manufacturerDropdown($en);
            $menufacturers = $this->Manufacturers->find()
                ->where([
                    'is_suspend' => 0
                ]);
            $product_id = $this->request->query('product_id');
            $product_detail_id = $this->request->query('product_detail_id');

            $this->loadModel('Products');
            //$products = $this->Products->productsDropdown(null, $en, 'list');
            $products = $this->Products->productsDropdown(null, null, 'all'); //pr($products->toArray()); exit;
            $this->set(compact('products', 'product_id'));

            if ($product_id > 0 && $product_detail_id > 0) {
                $this->loadModel('ProductDetails');
                //$product_details = $this->productDropdown($product_id);
                $product_details = $this->Products->getSingeProductWithDetails($product_id);
                $prices = $this->detailPrice($product_detail_id);
                $this->set(compact('product_details', 'product_detail_id', 'prices'));
            }
            $this->set(compact('menufacturers', 'en'));
            $this->set('_serialize', ['menufacturers', 'en']);
        }

        $this->viewBuilder()->layout('ajax');
    }

    /**
     * This function is temporary not use. It replaces by brandDropdown inside ProductBrandController
     */
    public function productBrandDropdown()
    {
        if ($this->request->is('ajax')) {
            $this->loadModel('ProductBrands');
            $man_id = $this->request->query('man_id');
            $en = $this->request->session()->read('tb_field');
            $brands = $this->ProductBrands->brandDropdown($man_id, null, 'all');
            $this->set(compact('brands', 'en'));
            $this->set('_serialize', ['brands', 'en']);
        }

        $this->viewBuilder()->layout('ajax');
    }

    public function productNameList()
    {
        if ($this->request->is('ajax')) {
            $this->autoRender = false;
            $this->response->type('json');
            $this->viewBuilder()->layout(false);

            $this->loadModel('Products');
            $brand_id = null;
            if ($this->request->query('brand_id')) {
                $brand_id = $this->request->query('brand_id');
            }
            $en = $this->request->session()->read('tb_field');
            //$products = $this->Products->productsDropdown($brand_id, $en);
            $products = $this->Products->productsDropdown($brand_id, null, 'all');
            if ($products) {
                $this->response->body(json_encode([
                    'status' => '1',
                    'message' => 'success',
                    'en' => $en,
                    'data' => $products->toArray()
                ]));
                return $this->response;
            }
            $this->response->body(json_encode([
                'status' => '1',
                'message' => 'success',
                'en' => $en,
                'data' => []
            ]));
            return $this->response;
        }
    }

    public function productDetailList()
    {
        if ($this->request->is('ajax')) {
            $this->loadModel('Products');

            $product_id = $this->request->query('product_id');
            //$products = $this->productDropdown($product_id);
            $products = [];
            if ($product_id) {
                $products = $this->Products->getSingeProductWithDetails($product_id);
            }

            $en = $this->request->session()->read('tb_field');

            $this->set(compact('products', 'en'));
            $this->set('_serialize', ['products', 'en']);
        }

        $this->viewBuilder()->layout('ajax');
    }

    private function productDropdown($product_id)
    {
        $this->loadModel('Products');
        $products = $this->Products->find()
            ->contain([
                'ProductBrands',
                'ProductDetails' => [
                    'PackSizes',
                    'SingleUnits',
                    'queryBuilder' => function($q) {
                        return $q->where(['ProductDetails.is_suspend' => 0]);
                    },
                ]
            ])
            ->where([
                'Products.is_suspend' => 0,
                'Products.id' => $product_id
            ])
            ->first();
        return $products;
    }

    private function detailPrice($product_detail_id)
    {
        $this->loadModel('WStandardPrices');
        $prices = $this->WStandardPrices->find('all')
            ->contain([
                'Currencies'
            ])
            ->where([
                'WStandardPrices.product_detail_id' => $product_detail_id,
                'WStandardPrices.is_suspend' => 0
            ]);
        return $prices;
    }

    public function productListPrice()
    {
        if ($this->request->is('ajax')) {
            $prices = $this->detailPrice($this->request->query('product_detail_id'));
            $this->set(compact('prices'));
            $this->set('_serialize', ['prices']);
        }

        $this->viewBuilder()->layout('ajax');
    }

    public function view()
    {
        if ($this->request->is('ajax')) {
            $this->loadModel('ProductDetails');

            $products = $this->ProductDetails->find()
                ->contain([
                    'Products',
                    'LogisticTemperatures',
                    'PackSizes',
                    'SingleUnits'
                ])
                ->where([
                    'ProductDetails.is_suspend' => 0,
                    'ProductDetails.id' => $this->request->query('product_detail_id')
                ])
                ->first();
            $en = $this->request->session()->read('tb_field');

            $this->set(compact('products', 'en'));
            $this->set('_serialize', ['products', 'en']);
        }

        $this->viewBuilder()->layout('ajax');
    }

    public function delete()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->viewBuilder()->layout('ajax');
        $this->autoRender = false;

        $data = $this->ProductPackageDetails->get($this->request->data['id']);
        if ($this->ProductPackageDetails->delete($data)) {
            echo json_encode([
                'status' => 1,
                'message' => 'success',
            ]);
        }
    }

    public function getList()
    {
        if ($this->request->is('ajax')) {
            $this->autoRender = false;
            $this->response->disableCache();
            $this->response->type('json');
            $this->viewBuilder()->layout(false);

            $this->loadModel('Products');
            $data = $this->Products->find('all')->contain([]);

            if ($data) {
                $this->response->body(json_encode([
                    'status' => 1,
                    'message' => 'success',
                    'data' => $data->toArray()
                ]));
                return $this->response;
            }

            $this->response->body(json_encode([
                'status' => 1,
                'message' => 'success',
                'data' => []
            ]));
            return $this->response;
        }
    }

    public function updateSuspend()
    {
        if ($this->request->is('ajax')) {
            $this->autoRender = false;
            $this->response->disableCache();
            $this->response->type('json');
            $this->viewBuilder()->layout(false);

            $data = $this->ProductPackageDetails->get($this->request->data['id']);
            $data->is_suspend = ($this->request->data['is_suspend'] == 0 ) ? 1 : 0;
            if ($this->ProductPackageDetails->save($data)) {
                $this->response->body(json_encode([
                    'status' => 1,
                    'message' => 'success'
                ]));
                return $this->response;
            }

            $this->response->body(json_encode([
                'status' => 0,
                'message' => 'error'
            ]));
            return $this->response;
        }
    }
}
