<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Routing\Router;
use Cake\Network\Exception\NotFoundException;

class MediasController extends AppController
{
    public $components = ['Image'];

    public function initialize() {
        parent::initialize();
    }

    public function upload()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->viewBuilder()->layout('ajax');
        $this->autoRender = false;
        $request = $this->request->data;

        $path = WWW_ROOT.UPLOAD_PATH;
        $file = isset($request['file']) ? $request['file'] : '';
        $profile = isset($request['profile']) ? $request['profile'] : '';
        $file_name = $this->Image->upload($profile, $file, $path);

        echo json_encode($file_name);
    }
}