<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Network\Exception\NotFoundException;

class DeliveriesController extends AppController
{
    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('Function');
    }

    public function create()
    {
        $this->ajaxRequest(false);
        $error = [];
        $data = $this->request->data;
        if ($data) {
            foreach ($data as $key => $value) {
                if (empty($value)) {
                    $error[$key] = __('TXT_MESSAGE_REQUIRED');
                }
            }
            $this->Function->ajaxResponse(0, MSG_ERROR, $error);
        }

        if (empty($error)) {
            $this->loadModel('SaleStockDetails');
            $saleStockDetails = $this->SaleStockDetails->getAllDataList('all', null, null, [
                'Stocks' => ['ImportDetails'],
                'SaleDetails' => function($q) use ($data) {
                    return $q->where(['SaleDetails.sale_id' => $data['sale_id']]);
                },
            ]);

            // deliveries object
            $data1 = [
                'id' => $data['id'],
                'sale_id' => $data['sale_id'],
                'user_id' => $this->Auth->user('id'),
                'affiliation_class' => $data['affiliation_class'],
                'delivery_date' => $data['delivery_date'],
                'tracking' => $data['tracking'],
            ];

            // save data
            if ($data['id'] == -1) {
                unset($data1['id']);

                // sale_deliveries object
                $data1['sale_deliveries'] = [
                    ['sale_id' => $data['sale_id']]
                ];

                // sale_stock_details object
                if ($saleStockDetails) {
                    foreach ($saleStockDetails as $key => $value) {
                        $data1['delivery_details'][] = [
                            'stock_id' => $value->id,
                            'lot' => $value->stock->import_detail->lot_number,
                            'expiry_date' => $value->stock->import_detail->expiry_date,
                            'quantity' => $value->quantity,
                        ];
                    }
                }
                $entities = $this->Deliveries->newEntity($data1, [
                    'associated' => [
                        'SaleDeliveries',
                        'DeliveryDetails',
                    ]
                ]);
                $this->Deliveries->save($entities);
            } else {
                // edit data
                $entity = $this->Deliveries->get($data['id']);
                $entity->tracking = $data['tracking'];
                $entity->delivery_date = $data['delivery_date'];

                $this->Deliveries->save($entity);
            }

            $this->Function->ajaxResponse(1, MSG_SUCCESS);
        }
    }

    public function nextAndPreviousStatus()
    {
        $this->ajaxRequest(false);
        $this->loadModel('Medias');
        $data = $this->request->data;
        $response = [];
        $medias = $this->Medias->getAllDataList('all', null, [
            'Medias.external_id' => $data['id'],
            'Medias.type' => TYPE_SALE_W_STOCK,
        ]);
        $delivery = $this->Deliveries->get($data['delivery_id']);
        $delivery->status = $data['status'];
        $step = $data['step'];
        if ($this->Deliveries->save($delivery)) {
            $response = [
                'step' => $step,
                'currentStatus' => $data['status'],
                'delivery' => $delivery,
                'saleStatus' => $this->CustomConstant->saleStatus(),
                'medias' => $medias,
            ];
        }
        $this->set($response);
        $this->render('render_step');
    }

    public function checkIsExist()
    {
        $this->ajaxRequest(true);
        $this->loadModel('Sales');
        $this->response->type('json');
        $data = [];

        $request = $this->request->data;
        $sale = $this->Sales->get($request['id'], [
            'contain' => 'Customers'
        ]);
        $delivery = $this->Deliveries->findBySaleId($request['id'])->first();
        if ($sale->status === PO_STATUS_PAID) {
            if ($delivery) {
                $delivery->status = $request['status'];
                if ($this->Deliveries->save($delivery)) {
                    $data = [
                        'currentStatus' => $request['status'],
                        'sale' => $sale,
                        'delivery' => $delivery,
                        'saleStatus' => $this->CustomConstant->saleStatus(),
                    ];
                }
            } else {
                $entity = $this->Deliveries->newEntity();
                $entity->sale_id = $sale->id;
                $entity->customer_id = $sale->customer_id;
                $entity->user_id = $sale->user_id;
                $entity->status = $request['status'];
                $entity->customer_name = $sale->customer->name;
                $entity->customer_address = $sale->customer->code . ',' .
                    $sale->customer->prefecture . ',' . $sale->customer->street . ',' .
                    $sale->customer->building;
                if ($this->Deliveries->save($entity)) {
                    $data = [
                        'currentStatus' => $request['status'],
                        'sale' => $sale,
                        'saleStatus' => $this->CustomConstant->saleStatus(),
                        'delivery' => $delivery,
                    ];
                }
            }
        }
        $this->set($data);
        $this->render('render_step');
    }

    public function updateTrackingAndDeliveryDate()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->viewBuilder()->layout('ajax');
        $this->autoRender = false;
        $this->response->type('json');
        $this->loadModel('Medias');
        $data = [];

        $request = $this->request->data;
        $delivery = $this->Deliveries->get($request['delivery_id']);
        $oldStatus = $delivery->status;
        $validator = $this->Deliveries->patchEntity($delivery, $request);
        // Check validate
        if ($delivery->errors()) {
            $response = [
                'status' => $request['status'],
                'message' => MSG_ERROR,
                'data' => $this->Function->getError($validator->errors()),
            ];
            $this->response->body(json_encode($response));
            return $this->response;
        }
        $date = date_create($request['delivery_date']);
        $delivery->status = $request['status'];
        $delivery->delivery_date = date_format($date, 'Y-m-d');
        $medias = $this->Medias->getAllDataList('all', null, [
            'Medias.external_id' => $request['id'],
            'Medias.type' => TYPE_SALE_W_STOCK,
        ]);
         // deduck stock in packed status
        if ($oldStatus === STATUS_PACKED) {
            $this->deduckStock($delivery->id);
        }
        if ($this->Deliveries->save($delivery)) {
            $this->autoRender = true;
            $data = [
                'currentStatus' => $request['status'],
                'message' => MSG_SUCCESS,
                'step' => $request['step'],
                'saleStatus' => $this->CustomConstant->saleStatus(),
                'medias' => $medias,
                'delivery' => $delivery,
            ];
             // deduck stock in packed status
            if ($oldStatus === STATUS_PACKED) {
                $this->deduckStock($delivery->id);
            }
        }
        $this->set($data);
        $this->render('render_step');
    }

    /**
    * Function ajaxRequest
    * using for checking the request is ajax
    * @param boolean $auto_render assign value to $this->autoRender
    */
    private function ajaxRequest($auto_render = false)
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->viewBuilder()->layout('ajax');
        $this->autoRender = $auto_render;
    }

    private function deduckStock($deliveryId)
    {
        if ($deliveryId === null) {
            return false;
        }
        $this->loadModel('DeliveryDetails');
        $deliveryDetails = $this->DeliveryDetails->getAllDataList('all', null, [
            'DeliveryDetails.delivery_id' => $deliveryId,
        ]);
        if (!$deliveryDetails) {
            return false;
        }
        $this->loadModel('Stocks');
        foreach ($deliveryDetails as $key => $value) {
            $stock = $this->Stocks->get($value->stock_id);
            if ($stock->quantity > $value->quantity) {
                $stock->quantity -= $value->quantity;
            }
            $this->Stocks->save($stock);
        }
    }
}
