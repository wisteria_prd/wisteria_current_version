<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Network\Exception\NotFoundException;

class ReceivePaymentsController extends AppController
{
    private $local;

    public function initialize()
    {
        parent::initialize();
        // $this->loadComponent('Csrf');
        $this->loadComponent('Function');
        $this->local = $this->request->session()->read('tb_field');
    }

    public function index()
    {
        $options = [];
        $params = new \stdClass();
        $params->display = PAGE_NUMBER;
        $params->status = 0;
        // Check status
        if ($this->request->query('status') === TYPE_PAID) {
            $params->status = 1;
        }
        // Checking GET request
        if ($this->request->query) {
            $params->display = $this->request->query('displays');
            $params->keyword = $this->request->query('keyword');
        }
        if ($this->request->query('inactive')) {
            $options[] = [
                'ReceivePayments.is_suspend' => 1,
            ];
        }
        $this->paginate = [
            'conditions' => $options,
            'contain' => [
                'Customers' => ['joinType' => 'LEFT'],
                'Subsidiaries'  => ['joinType' => 'LEFT'],
                'SaleReceivePayments' => [
                    'joinType' => 'LEFT',
                    'Sales' => [
                        'joinType' => 'LEFT',
                        'queryBuilder' => function($q) use ($params) {
                            if (!isset($params->keyword)) {
                                return $q;
                            }
                            $q->where(['Sales.is_receive_payment' => $params->status])
                                ->where(['OR' => [
                                'Sales.sale_number LIKE ' => '%' . trim($params->keyword) . '%',
                                'Sales.rop_number LIKE ' => '%' . trim($params->keyword) . '%',
                                'Sales.rop_issue_date LIKE ' => '%' . trim($params->keyword) . '%',
                                'Sales.amount LIKE ' => '%' . trim($params->keyword) . '%',
                            ]]);
                            return $q;
                        },
                        'Currencies',
                    ],
                ],
            ],
            'sortWhitelist' => [
                'Sales.is_receive_payment',
                'Customers.name_en',
                'Customers.name',
                'Sales.rop_number',
                'Sales.rop_issue_date',
                'Sales.is_suspend',
                'Sales.amount',
                'Sales.sale_number',
                'ReceivePayments.customer_name',
                'ReceivePayments.created',
            ],
            'limit' => $params->display,
        ];
        $receivePayment = $this->paginate($this->ReceivePayments);
        $data = [
            'local' => $this->local,
            'receivePayment' => $receivePayment,
            'payStatus' => $this->CustomConstant->paymentStatus(),
            'display' => $params->display,
        ];
        $this->set($data);
    }

    public function paymentForm()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->viewBuilder()->layout('ajax');
        $this->loadModel('Sales');
        $receivePayment = [];
        $sale = null;
        $sales = $this->Sales->find('all')->limit(20);
        if ($this->request->query('target') === TARGET_EDIT) {
            $this->loadModel('Sales');
            $receivePayment = $this->ReceivePayments->get($this->request->query('payment_id'));
            $sale = $this->Sales->findById($this->request->query('sale_id'))
                ->contain([
                    'Currencies',
                    'SaleDetails' => function ($q) {
                        $q->select([
                            'id',
                            'sale_id',
                            'total_amount' => $q->func()->sum('SaleDetails.unit_price * SaleDetails.quantity'),
                        ])->group('SaleDetails.sale_id');
                        return $q;
                    },
                ])->first();
        }
        $data = [
            'local' => $this->local,
            'sales' => $sales,
            'target' => $this->request->query('target'),
            'receivePayment' => $receivePayment,
            'sale' => $sale,
        ];
        $this->set($data);
    }

    public function createAndUpdate()
    {
        if (!$this->request->is('ajax')) {
            throw New NotFoundException();
        }
        $this->autoRender = false;
        $this->response->type('json');
        $this->response->disableCache();
        $this->viewBuilder()->layout(false);
        // check type edit or new
        if ($this->request->data['target'] === TARGET_EDIT) {
            $data = $this->ReceivePayments->get($this->request->data['id']);
        } else {
            $data = $this->ReceivePayments->newEntity();
        }
        $data = $this->ReceivePayments->patchEntity($data, $this->request->data);
        // Check validation
        if ($data->errors()) {
            $this->response->body(json_encode([
                'status' => 0,
                'message' => MSG_ERROR,
                'data' => $this->Function->getError($data->errors()),
            ]));
            return $this->response;
        }
        $associated = [
            'customer_id' => $data->customer_id,
            'receiver_id' => $data->receiver_id,
            'pay_date' => $data->pay_date,
            'customer_name' => $data->customer_name,
            'payee_name' => $data->payee_name,
            'amount' => intval(str_replace(',', '', $this->request->data['amount'])),
            'sale_receive_payments' => [
                ['sale_id' => $data->sale_id],
            ],
        ];
        $entity = $this->ReceivePayments->newEntity($associated, [
            'associated' => [
                'SaleReceivePayments',
            ],
            'validate' => false,
        ]);
        if ($this->request->data['target'] === TARGET_EDIT) {
            $entity = $data;
            $entity->amount = intval(str_replace(',', '', $this->request->data['amount']));
        }
        // save data
        if ($this->ReceivePayments->save($entity)) {
            $this->response->body(json_encode([
                'status' => 1,
                'message' => MSG_SUCCESS,
            ]));
            return $this->response;
        }
    }

    public function view($id = null)
    {
        if ($this->request->is('ajax')) {
            $payment = $this->ReceivePayments->getById($id, null, [
                'SaleReceivePayments' => [
                    'Sales' => ['Currencies'],
                ],
                'Users'
            ]);
            $data = [
                'payment' => $payment,
                'local' => $this->local,
            ];
            $this->set($data);
        }
        $this->viewBuilder()->layout('ajax');
    }

    /**
     * Using for show popup advance search form
     * @throws NotFoundException request is not ajax
     * @return object return data to view layout
     */
    public function getFormSearch()
    {
        if (!$this->request->is('ajax')) {
            throw New NotFoundException();
        }
        $this->viewBuilder()->layout('ajax');
        $data = [];
        $this->set($data);
    }

    private function saveSRP($sale_id, $receive_id) //sale receive payment
    {
        $this->loadModel('SaleReceivePayments');
        $request['receive_payment_id'] = $receive_id;
        $request['sale_id'] = $sale_id;
        $srp = $this->SaleReceivePayments->newEntity();
        $srp = $this->SaleReceivePayments->patchEntity($srp, $request, ['validate' => false]);
        return $this->SaleReceivePayments->save($srp);
    }

    private function updateSaleAsPaid($sale_id)
    {
        //update Sales is_receive_payment to 1
        $this->loadModel('Sales');
        $sale = $this->Sales->get($sale_id);
        $sale->is_receive_payment = 1;
        $this->Sales->save($sale);
    }

    private function checkExistingPayment($id)
    {
        $this->loadModel('SaleReceivePayments');
        $data = $this->SaleReceivePayments->getByField(['SaleReceivePayments.receive_payment_id' => $id]);

        return $data;
    }

    public function getStatus()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->viewBuilder()->layout('ajax');
        $data = $this->ReceivePayments->get($this->request->query('id'));
        $data1 = [
            'data' => $data,
            'status' => $this->request->query('status'),
        ];
        $this->set($data1);
    }

    public function changeOfStatus()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->viewBuilder()->layout('ajax');
        $this->autoRender = false;
        $this->response->disableCache();
        $this->response->type('json');
        $data = $this->ReceivePayments->get($this->request->data['id']);
        $data->is_suspend = $this->request->data['status'];
        if ($this->ReceivePayments->save($data)) {
            $this->response->body(json_encode([
                'status' => 1,
                'message' => MSG_SUCCESS,
            ]));
            return $this->response;
        }
    }

    public function getDelete()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->viewBuilder()->layout('ajax');
        $data = $this->ReceivePayments->get($this->request->query('id'));
        $data1 = [
            'data' => $data,
        ];
        $this->set($data1);
    }

    public function delete()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->viewBuilder()->layout('ajax');
        $this->autoRender = false;
        $this->response->type('json');
        $this->response->disableCache();
        $data = $this->ReceivePayments->get($this->request->data['id']);
        if ($this->ReceivePayments->delete($data)) {
            $this->response->body(json_encode([
                'status' => 1,
                'message' => MSG_SUCCESS,
            ]));
            return $this->response;
        }
    }
}
