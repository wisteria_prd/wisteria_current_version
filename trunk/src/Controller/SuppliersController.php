<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Network\Exception\NotFoundException;

class SuppliersController extends AppController
{
    private $locale;

    public function initialize()
    {
        parent::initialize();
        $this->loadModel('InfoDetails');
        $this->loadComponent('Function');
        $this->locale = $this->request->session()->read('tb_field');
    }

    public function index()
    {
        $keyword = '';
        $conditions = [];
        if ($this->request->query('keyword')) {
            $keyword = trim($this->request->query('keyword'));
        }
        // SET PAGE LIMIT ON PAGINATION
        $display = PAGE_NUMBER;
        if ($this->request->query('displays')) {
            $display = $this->request->query('displays');
        }
        if ($this->request->query('inactive')) {
            $conditions[] = [
                'Suppliers.is_suspend' => 1,
            ];
        }
        $conditions[] = [
            'or' => [
                'Suppliers.name LIKE' => '%' . $keyword .'%',
                'Suppliers.name_en LIKE' => '%' . $keyword .'%',
            ],
        ];
        $this->paginate = [
            'conditions' => $conditions,
            'contain' => [
                'PersonInCharges',
                'Sellers',
                'Countries'
            ],
            'limit' => $display,
            'order' => ['Suppliers.name' . $this->locale => 'asc'],
            'sortWhitelist' => [
                'Suppliers.code',
                'Suppliers.name',
                'Suppliers.name_en',
                'count_person',
                'Countries.name',
                'Countries.name_en',
            ]
        ];
        $data = [
            'locale' => $this->locale,
            'display' => $display,
            'suppliers' => $this->paginate($this->Suppliers),
        ];

        $this->set($data);
    }

    public function add()
    {
        $this->loadModel('Countries');
        $supplier = $this->Suppliers->newEntity();
        $data = [
            'locale' => $this->locale,
            'countries' => $this->Countries->getListOfCountries($this->locale),
            'supplier' => $supplier,
        ];

        $this->set($data);
    }

    public function edit($id = null)
    {
        $this->loadModel('Countries');
        $supplier = $this->Suppliers->findById($id)
                ->contain([
                    'InfoDetails' => function ($q) use ($id) {
                    return $q->where([
                        'external_id' => $id,
                        'type' => TYPE_SUPPLIER,
                    ]);
                    },
                ])
                ->first();
        $data = [
            'locale' => $this->locale,
            'countries' => $this->Countries->getListOfCountries($this->locale),
            'supplier' => $supplier,
        ];

        $this->set($data);
    }

    public function view()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->viewBuilder()->layout('ajax');
        $supplier = $this->Suppliers->findById($this->request->query('id'))
                ->contain([
                    'Countries',
                    'InfoDetails' => function($q) {
                    return $q->where(['InfoDetails.type' => TYPE_SUPPLIER]);
                    }
                ])->first();
        $data = [
            'locale' => $this->locale,
            'supplier' => $supplier,
        ];

        $this->set($data);
    }

    public function delete()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->viewBuilder()->layout('ajax');
        $this->autoRender = false;
        $this->response->type('json');
        $this->response->disableCache();
        $data = $this->Suppliers->get($this->request->data['id']);
        if ($this->Suppliers->delete($data)) {
            $this->response->body(json_encode([
                'status' => 1,
                'message' => MSG_SUCCESS,
            ]));

            return $this->response;
        }
    }

    public function saveOrUpdate()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->viewBuilder()->layout('ajax');
        $this->autoRender = false;
        $this->response->type('json');
        $this->response->disableCache();
        if ($this->request->data('id')) {
            $supplier = $this->Suppliers->get($this->request->data['id']);
        } else {
            $supplier = $this->Suppliers->newEntity();
            $supplier->user_id = $this->Auth->user('id');
        }
        $response = $this->Suppliers->patchEntity($supplier, $this->request->data);
        if ($response->errors()) {
            $this->response->body(json_encode([
                'status' => 0,
                'message' => MSG_ERROR,
                'data' => $response->errors(),
            ]));

            return $this->response;
        }
        $this->InfoDetails->deleteAll([
            'type' => TYPE_SUPPLIER,
        ]);
        $associated = [
            'associate' => ['InfoDetails']
        ];
        if ($this->Suppliers->save($supplier, $associated)) {
            $this->response->body(json_encode([
                'status' => 1,
                'message' => MSG_SUCCESS,
            ]));

            return $this->response;
        }
    }

    public function changeOfStatus()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->viewBuilder()->layout('ajax');
        $this->autoRender = false;
        $this->response->disableCache();
        $this->response->type('json');
        $data = $this->Suppliers->get($this->request->data['id']);
        $data->is_suspend = $this->request->data['status'];
        if ($this->Suppliers->save($data)) {
            $this->response->body(json_encode([
                'status' => 1,
                'message' => MSG_SUCCESS,
            ]));

            return $this->response;
        }
    }

    public function getStatus()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->viewBuilder()->layout('ajax');
        $supplier = $this->Suppliers->get($this->request->query('id'));
        $data = [
            'data' => $supplier,
            'status' => $this->request->query('status'),
        ];

        $this->set($data);
    }

    public function getDelete()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->viewBuilder()->layout('ajax');
        $manufacturer = $this->Suppliers->get($this->request->query('id'));
        $data = [
            'data' => $manufacturer,
        ];

        $this->set($data);
    }

    public function getListOfSuppliersByAjax()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->viewBuilder()->layout('ajax');
        $this->autoRender = false;
        $this->response->type('json');
        $this->response->disableCache();
        $keyword = trim($this->request->query('keyword'));
        $data = $this->Suppliers->find()
            ->where(['Suppliers.is_suspend' => 0])
            ->where(['or' => [
                'Suppliers.name LIKE' => '%' . $keyword . '%',
                'Suppliers.name_en LIKE' => '%' . $keyword . '%',
            ]])
            ->limit(PAGE_LIMIT_NUMBER)
            ->all();
        $this->response->body(json_encode([
            'status' => 1,
            'message' => MSG_SUCCESS,
            'data' => $data,
        ]));

        return $this->response;
    }
}
