<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Network\Exception\NotFoundException;

class BreadcrumbDetailsController extends AppController
{
    public $user_group_id;

    public function initialize()
    {
        parent::initialize();
        $this->user_group_id = $this->request->session()->read('Auth.User.user_group_id');
    }

    public function update()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->autoRender = false;
        $this->viewBuilder()->layout('ajax');
        $this->response->type('json');
        $data = $this->request->data;
        $this->set('_serialize', [$data]);
        if ($data['data']) {
            // Delete all records based on user_group_id
            $this->BreadcrumbDetails->deleteAll([
                'breadcrumb_id' => $data['action'],
            ]);
            foreach ($data['data'] as $key => $value) {
                $parent = [
                    'breadcrumb_id' => $data['action'],
                    'action' => $value['action'],
                    'controller' => $value['controller'],
                    'name' => $value['name'],
                    'name_en' => $value['nameEn'],
                    'param' => $value['param'],
                    'url' => $value['url'],
                    'order_number' => ($key + 1),
                ];
                $entity = $this->BreadcrumbDetails->newEntity($parent);
                $result = $this->BreadcrumbDetails->save($entity);
                // Check sub-breadcrumb
                if (isset($value['children'])) {
                    $this->saveSubBreadcrumb($result, $value['children']);
                }
            }
            $this->response->body(json_encode([
                'status' => 1,
                'message' => MSG_SUCCESS,
            ]));
            return $this->response;
        }
    }

    public function getAllByBreadcrumbId()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->autoRender = false;
        $this->viewBuilder()->layout('ajax');
        $this->response->type('json');
        $data = $this->BreadcrumbDetails->findByBreadcrumbId($this->request->query('breadcrumb_id'))
                                        ->order(['order_number' => 'asc'])
                                        ->all();
        $this->response->body(json_encode([
            'status' => 1,
            'message' => MSG_SUCCESS,
            'data' => $data,
        ]));
        return $this->response;
    }

    private function saveSubBreadcrumb($parent, $data)
    {
        if (!$data) {
            return false;
        }
        foreach ($data as $key => $value) {
            $child = [
                'parent_id' => $parent->id,
                'breadcrumb_id' => $parent->breadcrumb_id,
                'action' => $value['action'],
                'controller' => $value['controller'],
                'name' => $value['name'],
                'name_en' => $value['nameEn'],
                'param' => $value['param'],
                'url' => $value['url'],
                'order_number' => $parent->order_number . ($key + 1),
            ];
            $entity = $this->BreadcrumbDetails->newEntity($child);
            $result = $this->BreadcrumbDetails->save($entity);
            // Check sub-breadcrumb
            if (isset($value['children'])) {
                $this->saveSubBreadcrumb($result, $value['children']);
            }
        }
    }
}
