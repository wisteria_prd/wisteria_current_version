<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Network\Exception\NotFoundException;

class PaymentCategoriesController extends AppController
{
    public function initialize() {
        parent::initialize();
        $this->loadComponent('Function');
    }

    public function getPaymentForm()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->viewBuilder()->layout('ajax');
        $payments = $this->PaymentCategories->find()
                    ->order(['created' => 'desc'])
                    ->all();
        $data = [
            'payments' => $payments,
        ];
        $this->set($data);
    }

    public function saveOrUpdate()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->autoRender = false;
        $this->response->disableCache();
        $this->response->type('json');
        $this->viewBuilder()->layout('ajax');
        //CHECK EXIST ID
        if ($this->request->data('id')) {
            $payment = $this->PaymentCategories->get($this->request->data['id']);
        } else {
            $payment = $this->PaymentCategories->newEntity();
        }
        $validator = $this->PaymentCategories->patchEntity($payment, $this->request->data);
        //CHECK VALIDATION
        if ($validator->errors()) {
            $this->response->body(json_encode([
                'status' => 0,
                'message' => MSG_ERROR,
                'data' => $this->Function->getError($validator->errors()),
            ]));
        }
        //SAVE DATA
        if ($this->PaymentCategories->save($payment)) {
            $this->response->body(json_encode([
                'status' => 1,
                'message' => MSG_SUCCESS,
                'data' => [
                    'payments' => $this->getPaymenCategorytList(),
                ],
            ]));
        }
    }

    public function delete()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->viewBuilder()->layout('ajax');
        $this->autoRender = false;
        $this->response->disableCache();
        $this->response->type('json');
        $data =  $this->PaymentCategories->get($this->request->data['id']);
        //DELETE RECORD
        if ($this->PaymentCategories->delete($data)) {
            $this->response->body(json_encode([
                'status' => 1,
                'message' => MSG_SUCCESS,
                'data' => [
                    'payments' => $this->getPaymenCategorytList(),
                ],
            ]));
        }
    }

    private function getPaymenCategorytList()
    {
        $payments = $this->PaymentCategories->find()
                    ->order(['created' => 'desc'])
                    ->all();

        return $payments;
    }
}