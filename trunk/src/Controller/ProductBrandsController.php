<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Network\Exception\NotFoundException;

class ProductBrandsController extends AppController
{
    public $locale;

    public function initialize()
    {
        parent::initialize();
        $this->loadModel('Medias');
        $this->locale = $this->request->session()->read('tb_field');
    }

    public function index()
    {
        $options = [];
        if ($this->request->query('keyword')) {
            $keyword = trim($this->request->query('keyword'));
            $options[] = [
                'or' => [
                    'ProductBrands.name LIKE' => '%' . $keyword .'%',
                    'ProductBrands.name_en LIKE' => '%' . $keyword .'%',
                ],
            ];
        }
        $display = PAGE_NUMBER;
        if ($this->request->query('displays')) {
            $display = $this->request->query('displays');
        }
        if ($this->request->query('inactive')) {
            $options[] = [
                'ProductBrands.is_suspend' => 1
            ];
        }
        $this->paginate = [
            'contain' => [
                'Manufacturers',
            ],
            'conditions' => $options,
            'limit' => $display,
            'order' => ['name' . $this->locale => 'asc'],
        ];
        $data = [
            'locale' => $this->locale,
            'data' => $this->paginate($this->ProductBrands),
            'display' => $display,
            'paging' => $this->request->param('paging')['ProductBrands']['pageCount'],
        ];
        $this->set($data);
    }

    public function create()
    {
        $data = $this->ProductBrands->newEntity();
        $data1 = [
            'data' => $data,
            'locale' => $this->locale,
        ];
        $this->set($data1);
    }

    public function edit($id = null)
    {
        $data = $this->ProductBrands->findById($id)
            ->contain(['Manufacturers'])
            ->first();
        $medias = $this->Medias->getMediaList($id, TYPE_PRODUCT_BRAND);
        $data1 = [
            'data' => $data,
            'medias' => $medias,
            'locale' => $this->locale,
        ];
        $this->set($data1);
    }

    public function saveOrUpdate()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->viewBuilder()->layout('ajax');
        $this->autoRender = false;
        $this->response->type('json');
        // check exist record
        if ($this->request->data('id')) {
            $data = $this->ProductBrands->get($this->request->data['id']);
        } else {
            $data = $this->ProductBrands->newEntity();
        }
        $validator = $this->ProductBrands->patchEntity($data, $this->request->data);
        // check validation
        if ($validator->errors()) {
            $this->response->body(json_encode([
                'status' => 0,
                'message' => MSG_ERROR,
                'data' => $validator->errors(),
            ]));
            return $this->response;
        }
        // save or update media
        if ($this->request->data('id')) {
            $this->Medias->deleteMediaById($this->request->data['id'], TYPE_PRODUCT_BRAND);
        }
        if ($this->ProductBrands->save($data)) {
            $this->Medias->saveMedia($data->files, $data->id, TYPE_PRODUCT_BRAND, null, null);
            $this->response->body(json_encode([
                'status' => 1,
                'message' => MSG_SUCCESS,
            ]));
            return $this->response;
        }
    }

    public function view($id = null)
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->viewBuilder()->layout('ajax');
        $data = $this->ProductBrands->find('all')
            ->where(['ProductBrands.id' => $id])
            ->contain([
                'Manufacturers',
                'Products',
                'Medias' => function($q) {
                    return $q->select(['external_id', 'file_name'])
                        ->where(['Medias.type' => TYPE_PRODUCT_BRAND])->order(['file_order']);
                },
                'Products.Medias' => function ($q) {
                    return $q->select(['id', 'type', 'file_name', 'external_id'])
                        ->where(['Medias.type' => TYPE_PRODUCT]);
                }
            ])->first();
        $locale = $this->locale;
        $this->set(compact('data', 'locale'));
    }

    public function getList()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->viewBuilder()->layout('ajax');
        $this->autoRender = false;
        $data = $this->ProductBrands->find('all')
            ->select([
                'name',
                'name_en',
                'id',
                'manufacturer_id',
            ])->toArray();

        if ($data) {
            echo json_encode([
                'status' => 1,
                'message' => 'success',
                'data' => $data,
            ]);
        }
    }

    public function updateSuspend()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->autoRender = false;
        $this->response->disableCache();
        $this->response->type('json');
        $this->viewBuilder()->layout(false);
        $data = $this->ProductBrands->get($this->request->data['id']);
        $data->is_suspend = ($this->request->data['is_suspend'] == 0 ) ? 1 : 0;
        if ($this->ProductBrands->save($data)) {
            $this->response->body(json_encode([
                'status' => 1,
                'message' => 'success'
            ]));
            return $this->response;
        }
        $this->response->body(json_encode([
            'status' => 0,
            'message' => 'error'
        ]));
        return $this->response;
    }

    public function delete()
    {
        if ($this->request->is('ajax')) {
            $this->autoRender = false;
            $this->response->disableCache();
            $this->response->type('json');
            $this->viewBuilder()->layout(false);

            $this->request->allowMethod(['post', 'delete']);
            $discount = $this->ProductBrands->get($this->request->data['id']);
            if ($this->ProductBrands->delete($discount)) {
                $this->response->body(json_encode(['status' => 1]));
                return $this->response;
            }
            $this->response->body(json_encode(['status' => 0, 'message' => __('TXT_DELETE_TROUBLE')]));
            return $this->response;
        }
    }

    public function getManufacturerList()
    {
        $this->loadModel('Manufacturers');
        $data = $this->Manufacturers->getManufacturerList($this->field, 'id');

        return $data;
    }

    public function getBrandBySellerId()
    {
        if ($this->request->is('ajax')) {
            $this->autoRender = false;
            $this->response->disableCache();
            $this->response->type('json');
            $this->viewBuilder()->layout(false);

            $this->loadModel('Sellers');
            $seller = $this->Sellers->find('all')
                ->where([
                    'external_id' => $this->request->query('id'),
                    'type' => $this->request->query('seller_type'),
                ])->first();
            $data = [];
            if ($seller) {
                $this->loadModel('SellerBrands');
                $data = $this->ProductBrands->find('all')->matching('SellerBrands', function ($q) use ($seller) {
                    return $q->where(['SellerBrands.seller_id' => $seller->id]);
                })->toArray();
            }

            $this->response->body(json_encode([
                'status' => 1,
                'message' => 'success',
                'data' => $data,
            ]));
            return $this->response;
        }
    }

    public function brandDropdown()
    {
        if ($this->request->is('ajax')) {
            $manufacturer_id = $this->request->query('man_id');
            $en = $this->request->session()->read('tb_field');
            //$brands = $this->ProductBrands->brandDropdown($manufacturer_id, $en, 'list');
            $brands = $this->ProductBrands->find()
                ->where([
                    'ProductBrands.manufacturer_id' => $manufacturer_id,
                    'ProductBrands.is_suspend' => 0
                ]);
            $this->set(compact('brands', 'en'));
            $this->set('_serialize', ['brands', 'en']);
        }

        $this->viewBuilder()->layout('ajax');
    }

    public function getListOfProductBrandsByAjax()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->viewBuilder()->layout('ajax');
        $this->autoRender = false;
        $this->response->type('json');
        $keyword = trim($this->request->query('keyword'));
        $product_brands = $this->ProductBrands->find()
                ->where(['is_suspend' => 0])
                ->where(['or' => [
                    'name_en LIKE' => '%' . $keyword . '%',
                    'name LIKE' => '%' . $keyword . '%',
                ]]);
        if ($this->request->query('manufacturer_id')) {
            $product_brands = $product_brands->where([
                'manufacturer_id' => $this->request->query('manufacturer_id'),
            ]);
        }
        $product_brands = $product_brands->limit(PAGE_LIMIT_NUMBER)->all();
        $this->response->body(json_encode([
            'status' => '1',
            'message' => MSG_SUCCESS,
            'data' => $product_brands,
        ]));
    }
}
