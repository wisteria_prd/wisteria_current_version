<?php
namespace App\Controller;

use App\Controller\AppController;

class PaymentManagementsController extends AppController
{
    private $local;

    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('FileUpload');
        $this->local = $this->request->session()->read('tb_field');
    }

    public function index()
    {
        $en = $this->local;
        $this->set(compact('en'));
    }

}