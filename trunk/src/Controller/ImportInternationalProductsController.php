<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\Network\Exception\NotFoundException;

class ImportInternationalProductsController extends AppController
{
    public function initialize()
    {
        parent::initialize();
        $this->loadModel('Imports');
        $this->loadComponent('FileUpload');
    }

    public function index()
    {
        $this->loadModel('PurchaseImports');
        $conditions = [];
        if ($this->request->query('keyword')) {
            $keyword = trim($this->request->query('keyword'));
            $conditions['purchase_number LIKE '] = '%' . $keyword . '%';
        }

        $display = PAGE_NUMBER;
        if (!empty($this->request->query('displays'))) {
            $display = $this->request->query('displays');
        }
        //TODO: confirm is_suspend field
        if ($this->request->query('inactive')) {
            $conditions[] = [
                'Imports.is_suspend' => 1,
            ];
        }
        $this->paginate = [
            'contain' => [
                'ImportDetails',
                'PurchaseImports' => [
                    'Sales',
                    'Purchases' => [
                        'joinType' => 'LEFT',
                        'Sellers' => [
                            'joinType' => 'LEFT',
                            'Manufacturers',
                            'Suppliers',
                        ],
                    ],
                ],
            ],
            'conditions' => $conditions,
            'sortWhitelist' => [
                'Imports.type',
                'Imports.tax_payment_date',
                'Imports.arrival_date',
                'Imports.receiving_date',
                'Imports.custom_clearerance_date',
            ],
            'order' => [
                'Imports.created' => 'DESC'
            ],
            'limit' => $display
        ];

        try {
            $imports = $this->paginate($this->Imports);
            $paging = $this->request->param('paging')['Imports']['pageCount'];
        } catch (NotFoundException $e) {
            $paging = $this->request->param('paging')['Imports']['pageCount'];
            $imports = [];
        }

        $this->set(compact('imports', 'paging'));
        $this->set('_serialize', ['imports', 'paging']);
    }

    public function create()
    {
        $import = $this->Imports->newEntity();

        $group = USER_CLASS_WISTERIA;
        if ($this->request->session()->check('user_group')) {
            $group = $this->request->session()->read('user_group')['affiliation_class'];
        }

        if ($this->request->is('ajax')) {
            $this->autoRender = false;
            $this->response->disableCache();
            $this->response->type('json');
            $this->viewBuilder()->layout(false);

            $request = $this->request->data;
            if (isset($request['medias']) && !empty($request['medias'][0]['file_name'])) {
                for ($i = 0; $i < count($request['medias']); $i++) {
                    $request['medias'][$i]['type'] = TYPE_IMPORT;
                    $name = explode('.', $request['medias'][$i]['file_name']);
                    $request['medias'][$i]['file_type'] = $name[1];
                    $request['medias'][$i]['is_deleted'] = 0;
                    $request['medias'][$i]['file_priority'] = $i;
                    $request['medias'][$i]['file_order'] = $i;
                    $request['medias'][$i]['permission'] = 'all';
                    $request['medias'][$i]['status'] = 0;
                }
            }

            $result = $this->Imports->createInternationalImport($import, $request, $group);

            if (is_array($result)) {
                $this->response->body(json_encode(['status' => 0, 'message' => $result]));
                return $this->response;
            }
            $this->response->body(json_encode(['status' => 1]));
            return $this->response;
        }

        $this->loadModel('Purchases');
        $purchases = $this->Purchases->purchaseDropdown($group);
        $this->loadModel('Currencies');
        $currencies = $this->Currencies->getDropdown();
        $txt_register = $this->Common->txtRegister();
        $files = $this->CustomConstant->pdfDocumentType();
        $types = $this->CustomConstant->importType();
        $this->set(compact('import', 'txt_register', 'files', 'currencies', 'types', 'purchases'));
        $this->set('_serialize', ['import', 'purchases']);
    }

    public function edit($id = null)
    {
        $import = $this->Imports->get($id, [
            'contain' => [
                'Medias' => function($q) use($id) {
                    return $q->where(['Medias.type' => TYPE_IMPORT, 'Medias.external_id' => $id]);
                },
                'PurchaseImports' => [
                    'Purchases',
                    'Sales'
                ]
            ]
        ]);

        $group = USER_CLASS_WISTERIA;
        if ($this->request->session()->check('user_group')) {
            $group = $this->request->session()->read('user_group')['affiliation_class'];
        }

        $data = [];
        if ($import->purchase_imports[0]->sale) {
            $this->loadModel('Sales');
            $data = $this->Sales->saleImportDropdown($group, 'list', $import->type);
        } else {
            $this->loadModel('Purchases');
            $data = $this->Purchases->purchaseDropdown($group);
        }

        if ($this->request->is('ajax')) {
            $this->autoRender = false;
            $this->response->disableCache();
            $this->response->type('json');
            $this->viewBuilder()->layout(false);

            $request = $this->request->data;
            if (isset($request['medias']) && !empty($request['medias'][0]['file_name'])) {
                for ($i = 0; $i < count($request['medias']); $i++) {
                    $request['medias'][$i]['type'] = TYPE_IMPORT;
                    $name = explode('.', $request['medias'][$i]['file_name']);
                    $request['medias'][$i]['file_type'] = $name[1];
                    $request['medias'][$i]['is_deleted'] = 0;
                    $request['medias'][$i]['file_priority'] = $i;
                    $request['medias'][$i]['file_order'] = $i;
                    $request['medias'][$i]['permission'] = 'all';
                    $request['medias'][$i]['status'] = 0;
                }
            }

            $result = $this->Imports->updateInternationalImport($id, $import, $request);

            if (is_array($result)) {
                $this->response->body(json_encode(['status' => 0, 'message' => $result]));
                return $this->response;
            }
            $this->response->body(json_encode(['status' => 1]));
            return $this->response;
        }

        $this->loadModel('Currencies');
        $currencies = $this->Currencies->getDropdown();
        $txt_edit = $this->Common->txtEdit();
        $files = $this->CustomConstant->pdfDocumentType();
        $types = $this->CustomConstant->importType();

        $this->set(compact('import', 'txt_edit', 'files', 'currencies', 'types', 'data'));
        $this->set('_serialize', ['import']);
    }

    public function importType()
    {
        if ($this->request->is('ajax')) {
            $group = USER_CLASS_WISTERIA;
            if ($this->request->session()->check('user_group')) {
                $group = $this->request->session()->read('user_group')['affiliation_class'];
            }

            $type = $this->request->query('type');
            if ($type == CUSTOMER_TYPE_NORMAL) {
                $this->loadModel('Purchases');
                $data = $this->Purchases->purchaseDropdown($group);
            } else if ($type == TYPE_SAMPLE) {
                $this->loadModel('Sales');
                $data = $this->Sales->saleImportDropdown($group, 'list', $type);
            }
            $this->set(compact('data', 'type'));
            $this->set('_serialize', ['data', 'type']);
        }
        $this->viewBuilder()->layout('ajax');
    }

    public function delete()
    {
        if ($this->request->is('ajax')) {
            $this->autoRender = false;
            $this->response->disableCache();
            $this->response->type('json');
            $this->viewBuilder()->layout(false);

            $this->request->allowMethod(['post', 'delete']);
            $discount = $this->Imports->get($this->request->data['id']);
            if ($this->Imports->delete($discount)) {
                $this->response->body(json_encode(['status' => 1]));
                return $this->response;
            }
            $this->response->body(json_encode(['status' => 0, 'message' => __('TXT_DELETE_TROUBLE')]));
            return $this->response;
        }
    }

    public function updateSuspend()
    {
        if ($this->request->is('ajax')) {
            $this->autoRender = false;
            $this->response->disableCache();
            $this->response->type('json');
            $this->viewBuilder()->layout(false);

            $data = $this->Imports->get($this->request->data['id']);
            $data->is_suspend = ($this->request->data['is_suspend'] == 0 ) ? 1 : 0;
            if ($this->Imports->save($data)) {
                $this->response->body(json_encode([
                    'status' => 1,
                    'message' => 'success'
                ]));
                return $this->response;
            }

            $this->response->body(json_encode([
                'status' => 0,
                'message' => 'error'
            ]));
            return $this->response;
        }
    }

    public function upload()
    {
        if ($this->request->is('ajax')) {
            $this->autoRender = false;
            $this->response->disableCache();
            $this->response->type('json');
            $this->viewBuilder()->layout(false);

            $request = $this->request->data;

            $mimeType = ['application/pdf'];
            $this->FileUpload->setMimeType($mimeType);
            $extension = ['pdf'];
            $this->FileUpload->setFileExtension($extension);
            $destination = WWW_ROOT . 'img' . DS . 'uploads';
            if (!file_exists($destination)) {
                mkdir($destination, 0755, true);
            }
            $destination .= DS . 'imports';
            if (!file_exists($destination)) {
                mkdir($destination, 0755, true);
            }
            $this->FileUpload->setDestination($destination);

            $result = $this->FileUpload->upload($this->request->data);

            if ($result['error'] == 0) {
                $this->response->body(json_encode([
                    'status' => 1,
                    'message' => $result['msg'],
                    'file' => [
                        'original_name' => $request['file']['name'],
                        'new_name' => $result['new_name']
                    ]
                ]));
                return $this->response;
            }

            $this->response->body(json_encode([
                'status' => 0,
                'message' => ['file' => $result['msg']]
            ]));
            return $this->response;
        }
    }

    /**
    * get all purchase number by search auto complete
    * @return object
    */
    public function getPurchaseNumberBySearch()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->viewBuilder()->layout('ajax');
        $this->autoRender = false;
        $this->response->type('json');
        $keyword = $this->request->query('keyword');

        $purchaseNumbers = $this->Imports->find()
            ->where(['purchase_number LIKE' => '%' . $keyword . '%'])
            ->limit(20)
            ->all();

        $this->response->body(json_encode([
            'status' => '1',
            'message' => MSG_SUCCESS,
            'data' => $purchaseNumbers,
        ]));
        return $this->response;
    }

}
