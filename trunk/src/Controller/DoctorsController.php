<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Network\Exception\NotFoundException;

class DoctorsController extends AppController
{
    private $local;

    public function initialize()
    {
        parent::initialize();
        $this->loadModel('Medias');
        $this->loadModel('Countries');
        $this->loadModel('ClinicDoctors');
        $this->loadModel('InfoDetails');
        $this->loadModel('DoctorSocieties');
        $this->loadComponent('Function');
        $this->local = $this->request->session()->read('tb_field');
    }

    public function index()
    {
        $options = [];
        if ($this->request->query('keyword')) {
            $keyword = trim($this->request->query('keyword'));
            $options[] = [
                'OR' => [
                    'first_name LIKE' => '%' . $keyword . '%',
                    'last_name LIKE' => '%' . $keyword . '%',
                    'CONCAT(last_name, " ", first_name) LIKE' => '%' . str_replace(' ', '%', $keyword) . '%',
                    'last_name_en LIKE' => '%' . $keyword . '%',
                    'first_name_en LIKE' => '%' . $keyword . '%',
                    'CONCAT(first_name_en, " ", last_name_en) LIKE' => '%' . str_replace(' ', '%', $keyword) . '%',
                ]
            ];
        }
        if ($this->request->query('external_id') && $this->request->query('filter_by')) {
            $options[] = ['id' => $this->request->query('external_id')];
        }
        $displays = 10;
        if ($this->request->query('displays')) {
            $displays = $this->request->query('displays');
        }
        if ($this->request->query('inactive')) {
            $options[] = [
                'Doctors.is_suspend' => 1,
            ];
        }
        $this->paginate = [
            'conditions' => $options,
            'limit' => $displays,
            'order' => ['position' => 'asc'],
            'contain' => [
                'Medias' => function ($q) {
                    return $q->select(['file_name', 'external_id'])->where([
                        'Medias.type' => 'doctor',
                        'Medias.status' => 1,
                    ]);
                },
                'ClinicDoctors' => ['Customers']
            ]
        ];
        $data = [
            'data' => $this->paginate($this->Doctors),
            'paging' => $this->request->param('paging')['Doctors']['pageCount'],
            'displays' => $displays,
        ];

        $this->set($data);
    }

    public function create()
    {
        $data = [
            'local' => $this->local,
            'txt_register' => $this->Common->txtRegister(),
            'gender' => $this->CustomConstant->userGender(),
            'doctor' => $this->Doctors->newEntity(),
        ];

        $this->set($data);
    }

    public function edit($id = null)
    {
        if ($id === null) {
            throw new NotFoundException();
        }
        $doctor = $this->Doctors->findById($id)
                ->contain([
                    'DoctorSocieties' => ['Societies'],
                    'InfoDetails' => function ($q) use ($id) {
                        return $q->where(['external_id' => $id])
                                ->where(['type' => TYPE_DOCTOR]);
                    },
                    'Medias' => function ($q) use ($id) {
                        return $q->where(['external_id' => $id])
                                ->where(['type' => TYPE_DOCTOR]);
                    },
                ])
                ->first();
        $data = [
            'local' => $this->local,
            'txt_edit' => $this->Common->txtEdit(),
            'gender' => $this->CustomConstant->userGender(),
            'doctor' => $doctor,
        ];

        $this->set($data);
    }

    public function saveAndUpdate()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->viewBuilder()->layout('ajax');
        $this->autoRender = false;
        $this->response->type('json');
        $this->response->disableCache();
        $data = $this->request->data;
        parse_str($data['data'], $data);
        // Checking Record Id
        if ($data['id']) {
            $doctor = $this->Doctors->findById($data['id'])
                    ->first();
        } else {
            $doctor = $this->Doctors->newEntity();
        }
        $doctor = $this->Doctors->patchEntity($doctor, $data);
        // Checking Validations
        if ($doctor->errors()) {
            $this->response->body(json_encode([
                'status' => 0,
                'message' => MSG_ERROR,
                'data' => $doctor->errors(),
            ]));
            return $this->response;
        }
        // Save or update record
        if ($this->Doctors->save($doctor)) {
            // Save and Update InfoDetails
            $this->updateInfoDetails($data, $doctor->id);
            // Save and Update DoctorSocieties
            $this->updateDoctorSociety($this->request->data, $doctor->id);
            // Upate Profile
            $this->updateDoctorProfile($data, $doctor->id);
            // Update Media
            $this->updateMedia($data['files'], $doctor->id);
            $this->response->body(json_encode([
                'status' => 1,
                'message' => MSG_SUCCESS,
            ]));
            return $this->response;
        }
    }

    public function view($id = null)
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->viewBuilder()->layout('ajax');

        $data = $this->Doctors->get($id, [
            'contain' => [
                'Medias' => function($q) {
                    return $q->order(['file_order' => 'asc']);
                },
                'ClinicDoctors' => ['Customers'],
                'DoctorSocieties' => ['Societies'],
                'InfoDetails' => function($q) {
                    return $q->where(['InfoDetails.type' => TYPE_DOCTOR]);
                }
            ]
        ]);
        $this->set(compact('data'));
        $this->set('_serialize', ['data']);
    }

    public function updateSuspend()
    {
        if ($this->request->is('ajax')) {
            $this->autoRender = false;
            $this->response->disableCache();
            $this->response->type('json');
            $this->viewBuilder()->layout(false);

            $data = $this->Doctors->get($this->request->data['id']);
            $data->is_suspend = ($this->request->data['is_suspend'] == 0 ) ? 1 : 0;
            if ($this->Doctors->save($data)) {
                $this->response->body(json_encode([
                    'status' => 1,
                    'message' => 'success'
                ]));
                return $this->response;
            }

            $this->response->body(json_encode([
                'status' => 0,
                'message' => 'error'
            ]));
            return $this->response;
        }
    }

    public function delete()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->viewBuilder()->layout('ajax');
        $this->autoRender = false;
        $data = $this->Doctors->get($this->request->data['id']);
        if ($this->Doctors->delete($data)) {
            echo json_encode([
                'status' => 1,
                'message' => 'success',
            ]);
        }
    }

    public function getCustomerList()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->loadModel('Customers');
        $this->viewBuilder()->layout('ajax');
        $this->autoRender = false;
        $this->response->disableCache();
        $this->response->type('json');
        $keyword = $this->request->query('keyword');
        $customers = $this->Customers->find()
                ->where(['is_suspend' => 0])
                ->where(['OR' => [
                    'Customers.name_en LIKE' => '%' . $keyword . '%',
                    'Customers.name LIKE' => '%' . $keyword . '%',
                ]])
                ->limit(20)
                ->all();
        $this->response->body(json_encode([
            'status' => 1,
            'message' => MSG_SUCCESS,
            'data' => $customers,
        ]));
        return $this->response;
    }

    public function getCustomerListByAjax()
    {
        if (!$this->request->is('ajax'))
        {
            throw new NotFoundException();
        }
        $this->viewBuilder()->layout('ajax');
        $this->autoRender = false;
        $data = $this->ClinicDoctors->find('all')->select(['position'])->toArray();

        echo json_encode([
            'status' => 1,
            'message' => 'success',
            'data' => $data,
        ]);
    }

    public function getDoctorList()
    {
        if (!$this->request->is('ajax'))
        {
            throw new NotFoundException();
        }
        $this->viewBuilder()->layout('ajax');
        $this->autoRender = false;
        $this->response->type('json');
        $this->response->disableCache();
        $data = $this->Doctors->find()
                ->where(['is_suspend' => 0])
                ->where(['OR' => [
                    'first_name LIKE ' => '%' . $this->request->query('keyword') . '%',
                    'last_name LIKE ' => '%' . $this->request->query('keyword') . '%',
                    'first_name_en LIKE ' => '%' . $this->request->query('keyword') . '%',
                    'last_name_en LIKE ' => '%' . $this->request->query('keyword') . '%',
                    'maiden_name LIKE ' => '%' . $this->request->query('keyword') . '%',
                ]])
                ->limit(20)
                ->all();
        $this->response->body(json_encode([
            'status' => 1,
            'message' => 'success',
            'data' => $data,
        ]));
        return $this->response;
    }

    public function getDoctorById($ids)
    {
        if (!$this->request->is('ajax'))
        {
            throw new NotFoundException();
        }
        $this->viewBuilder()->layout('ajax');
        $this->autoRender = false;
    }

    /**
    * get all doctor first_name, first_name_en, last_name, last_name_en by search auto complete
    * @return object
    */
    public function getDoctorBySearch()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->viewBuilder()->layout('ajax');
        $this->autoRender = false;
        $this->response->type('json');
        $keyword = $this->request->query('keyword');

        $doctors = $this->Doctors->find()
            ->select([
                'first_name',
                'last_name',
                'first_name_en',
                'last_name_en',
            ])
            ->where([
                'AND' => [
                    'is_suspend' => 0,
                    'OR' => [
                        'first_name LIKE' => '%' . $keyword . '%',
                        'last_name LIKE' => '%' . $keyword . '%',
                        'first_name_en LIKE' => '%' . $keyword . '%',
                        'last_name_en LIKE' => '%' . $keyword . '%',
                    ]
                ]
            ])
            ->limit(20)
            ->all();

        $this->response->body(json_encode([
            'status' => '1',
            'message' => MSG_SUCCESS,
            'data' => $doctors,
        ]));
        return $this->response;
    }

    private function updateClinicDoctor($data, $doctorId = null)
    {
        if ($doctorId === null)
        {
            return false;
        }
        $this->ClinicDoctors->deleteAll(['doctor_id' => $doctorId]);
        $clinic_doctors = [];
        if (isset($data['clinic_doctors'])) {
            foreach ($data['clinic_doctors'] as $key => $value) {
                $clinic_doctors[] = [
                    'doctor_id' => $doctorId,
                    'customer_id' => $value['customer_id'],
                    'position' => $value['position'],
                ];
            }
        }
        if (!$clinic_doctors) {
            return false;
        }
        $entities = $this->ClinicDoctors->newEntities($clinic_doctors);
        if ($this->ClinicDoctors->saveMany($entities)) {
            return true;
        }
        return false;
    }

    private function updateInfoDetails($data, $doctorId = null)
    {
        if ($doctorId === null) {
            return false;
        }
        $this->InfoDetails->deleteAll([
            'external_id' => $doctorId,
            'type' => TYPE_DOCTOR,
        ]);
        $info_details = [];
        if (isset($data['info_details'])) {
            foreach ($data['info_details'] as $key => $value) {
                $info_details[] = [
                    'external_id' => $doctorId,
                    'phone' => isset($value['phone']) ? $value['phone'] : null,
                    'tel' => isset($value['tel']) ? $value['tel'] : null,
                    'type' => TYPE_DOCTOR,
                    'email' => isset($value['email']) ? $value['email'] : null,
                    'tel_extension' => isset($value['tel_extension']) ? $value['tel_extension'] : null,
                ];
            }
        }
        if (!$info_details) {
            return false;
        }
        $entities = $this->InfoDetails->newEntities($info_details);
        if ($this->InfoDetails->saveMany($entities)) {
            return true;
        }
        return true;
    }

    private function updateDoctorSociety($data, $doctorId = null)
    {
        if ($doctorId === null)
        {
            return false;
        }
        $this->DoctorSocieties->deleteAll(['doctor_id' => $doctorId]);
        $doctor_societies = [];
        if (isset($data['societies'])) {
            foreach ($data['societies'] as $key => $value) {
                $doctor_societies[] = [
                    'doctor_id' => $doctorId,
                    'society_id' => $value,
                ];
            }
        }
        if (!$doctor_societies) {
            return false;
        }
        $entities = $this->DoctorSocieties->newEntities($doctor_societies);
        if ($this->DoctorSocieties->saveMany($entities)) {
            return true;
        }
        return true;
    }

    private function updateDoctorProfile($data, $doctorId = null)
    {
        if ($doctorId === null)
        {
            return false;
        }
        $this->Medias->deleteAll([
            'type' => TYPE_DOCTOR,
            'external_id' => $doctorId,
        ]);
        if (!isset($data['media-profile'])) {
            return false;
        }
        $entity = $this->Medias->newEntity();
        $entity->type = TYPE_DOCTOR;
        $entity->file_name = $data['media-profile'];
        $entity->status = 1;
        $entity->external_id = $doctorId;
        if ($this->Medias->save($entity)) {
            return true;
        }
        return false;
    }

    private function updateMedia($data, $doctorId = null)
    {
        if ($doctorId === null)
        {
            return false;
        }
        $media = [];
        if ($data) {
            $data = explode(',', $data);
            foreach ($data as $key => $value) {
                $media[] = [
                    'external_id' => $doctorId,
                    'type' => TYPE_DOCTOR,
                    'file_name' => $value,
                    'file_order' => ($key + 1),
                    'is_status' => 1,
                ];
            }
        }
        if (!$media) {
            return false;
        }
        $entities = $this->Medias->newEntities($media);
        if ($this->Medias->saveMany($entities)) {
            return true;
        }
        return true;
    }
}
