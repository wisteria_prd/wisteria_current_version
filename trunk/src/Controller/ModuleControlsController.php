<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Network\Exception\NotFoundException;

class ModuleControlsController extends AppController
{
    /**
    * @var string for store local session field
    */
    private $local;

    public function initialize() {
        parent::initialize();
    }

    public function index()
    {
        
    }

    public function create()
    {
        
    }

    public function delete()
    {
        
    }

    public function update()
    {
        
    }
}