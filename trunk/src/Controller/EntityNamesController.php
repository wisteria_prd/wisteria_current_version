<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Network\Exception\NotFoundException;

class EntityNamesController extends AppController
{
    public function initialize()
    {
        parent::initialize();
        $this->loadModel('Customers');
    }

    public function create()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->viewBuilder()->layout('ajax');
        $this->autoRender = false;
        $data = $this->EntityNames->newEntity();

        if ($this->request->data) {
            $data->name = $this->request->data['name'];
            $data->name_en = $this->request->data['name_en'];
            if ($this->EntityNames->save($data)) {
                $this->response->type('json');
                $this->response->body(json_encode([
                    'status' => 1,
                    'message' => 'success',
                ]));
                return $this->response;
            }
        }
    }

    public function edit($id)
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->viewBuilder()->layout('ajax');
        $this->autoRender = false;

        if ($this->request->data) {
            $data = $this->EntityNames->get($id);
            $data->name = $this->request->data['name'];
            $data->name_en = $this->request->data['name_en'];
            if ($this->EntityNames->save($data)) {
                $this->response->type('json');
                $this->response->body(json_encode([
                    'status' => 1,
                    'message' => 'success',
                ]));
                return $this->response;
            }
        }
    }

    public function delete($id)
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->loadModel('ProductDetails');
        $this->viewBuilder()->layout('ajax');
        $this->autoRender = false;
        $message = [];

        $customers = $this->Customers->find('all')
                ->where(['Customers.entity_name_id' => $id])->toArray();
        if ($customers) {
            $message = [
                'status' => 0,
                'message' => 'Invalid',
            ];
        } else {
            $data = $this->EntityNames->get($id);
            if ($this->EntityNames->delete($data)) {
                $message = [
                    'status' => 1,
                    'message' => 'success',
                ];
            }
        }
        $this->response->type('json');
        $this->response->body(json_encode($message));
        return $this->response;
    }

    public function getList()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->viewBuilder()->layout('ajax');
        $this->autoRender = false;
        $data = $this->EntityNames->find('all')->order(['EntityNames.name_en' => 'asc'])->toArray();

        $this->response->type('json');
        $this->response->body(json_encode([
            'status' => 1,
            'message' => 'success',
            'data' => $data,
        ]));

        return $this->response;
    }
}