<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Datasource\ConnectionManager;
use Cake\Network\Exception\NotFoundException;

class SocietiesController extends AppController
{
    private $locale;

    public function initialize() {
        parent::initialize();
        $this->locale = $this->request->session()->read('tb_field');
    }

    public function saveOrUpdate()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->viewBuilder()->layout('ajax');
        $this->autoRender = false;
        $this->response->type('json');
        if ($this->request->data('id')) {
            $data = $this->Societies->get($this->request->data['id']);
        } else {
            $data = $this->Societies->newEntity();
        }
        $validator = $this->Societies->patchEntity($data, $this->request->data);
        if ($validator->errors()) {
            $this->response->body(json_encode([
                'status' => 0,
                'message' => MSG_ERROR,
                'data' => $validator->errors(),
            ]));
            return $this->response;
        }
        if ($this->Societies->save($data)) {
            $this->response->body(json_encode([
                'status' => 1,
                'message' => 'success',
                'data' => $this->getList(),
            ]));
            return $this->response;
        }
    }

    public function delete($id)
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->loadModel('DoctorSocieties');
        $this->viewBuilder()->layout('ajax');
        $this->autoRender = false;
        $message = [];

        $doctor_societies = $this->DoctorSocieties->find('all')
                ->where(['DoctorSocieties.society_id' => $id])->toArray();
        if ($doctor_societies) {
            $message = [
                'status' => 0,
                'message' => 'Invalid',
            ];
        } else {
            $data = $this->Societies->get($id);
            if ($this->Societies->delete($data)) {
                $message = [
                    'status' => 1,
                    'message' => 'success',
                ];
            }
        }
        $this->response->type('json');
        $this->response->body(json_encode($message));
        return $this->response;
    }

    public function getList()
    {
        $data = $this->Societies->find()
            ->order(['Societies.name' => 'asc'])
            ->all();
        return $data;
    }

    public function getSocietyById()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->viewBuilder()->layout('ajax');
        $this->autoRender = false;
        $data = '';
        if ($this->request->query('ids')) {
            $data = $this->Societies->find('all')
                ->where(['Societies.id IN' => $this->request->query('ids')])->order(['Societies.name_en' => 'asc'])->toArray();
        }

        echo json_encode([
            'status' => 1,
            'message' => 'success',
            'data' => $data,
        ]);
    }

    public function getForm()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->viewBuilder()->layout('ajax');
        $data = $this->Societies->newEntity();
        $ids = json_decode($this->request->query('ids'));
        $societies = $this->getList();
        $data1 = [
            'societies' => $societies,
            'ids' => $ids,
            'data' => $data,
            'locale' => $this->locale,
        ];
        $this->set($data1);
    }
}