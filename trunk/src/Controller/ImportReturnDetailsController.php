<?php
namespace App\Controller;

use App\Controller\AppController;

class ImportReturnDetailsController extends AppController
{
    public function initialize()
    {
        parent::initialize();
    }

    public function create()
    {
        if ($this->request->is('ajax')) {
            $this->autoRender = false;
            $this->response->disableCache();
            $this->response->type('json');
            $this->viewBuilder()->layout(false);

            $import = $this->ImportReturnDetails->newEntity();
            
            $request = $this->request->data;
            unset($request['id']);

            $import = $this->ImportReturnDetails->patchEntity($import, $request);

            if ($this->ImportReturnDetails->save($import)) {
                $this->response->body(json_encode([
                    'status' => 1
                ]));
                return $this->response;
            }

            $this->response->body(json_encode(['status' => 0, 'message' => $import->errors()]));
            return $this->response;
        }
    }

    public function edit()
    {
        if ($this->request->is('ajax')) {
            $this->autoRender = false;
            $this->response->disableCache();
            $this->response->type('json');
            $this->viewBuilder()->layout(false);

            $request = $this->request->data;

            $import = $this->ImportReturnDetails->get($request['id']);

            $import = $this->ImportReturnDetails->patchEntity($import, $request);

            if ($this->ImportReturnDetails->save($import)) {
                $this->response->body(json_encode(['status' => 1]));
                return $this->response;
            }

            $this->response->body(json_encode(['status' => 0, 'message' => $import->errors()]));
            return $this->response;
        }
    }

    public function delete()
    {
        if ($this->request->is('ajax')) {
            $this->autoRender = false;
            $this->response->disableCache();
            $this->response->type('json');
            $this->viewBuilder()->layout(false);

            $this->request->allowMethod(['post', 'delete']);
            $discount = $this->ImportReturnDetails->get($this->request->data['id']);
            if ($this->ImportReturnDetails->delete($discount)) {
                $this->response->body(json_encode(['status' => 1]));
                return $this->response;
            }
            $this->response->body(json_encode(['status' => 0, 'message' => __('TXT_DELETE_TROUBLE')]));
            return $this->response;
        }
    }
}