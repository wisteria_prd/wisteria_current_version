<?php
namespace App\Controller;

use App\Controller\SalesController;
use Cake\Network\Exception\NotFoundException;

class SaleReportsController extends SalesController
{
    public function taxInvoicePdf($id = null)
    {
        if (empty($id)) {
            throw new NotFoundException();
        }
        $this->loadModel('SaleDetails');
        $this->loadModel('Sales');

        $saleDetails = $this->SaleDetails->find('threaded', [
            'keyField' => 'id',
            'parentField' => 'parent_id'
        ])->where([
            'SaleDetails.sale_id' => $id,
        ])->contain([
            'Sales' => [
                'Customers' => [
                    'InfoDetails' => [
                        'conditions' => ['type' => TYPE_CUSTOMER],
                    ],
                ],
                'SaleTaxInvoices',
            ],
            'SaleStockDetails' => [
                'Stocks' => ['ImportDetails'],
            ],
        ])->toArray();

        $sale = $this->Sales->findById($id)
            ->contain(['Currencies'])->first();

        $this->set(compact('saleDetails', 'sale'));
        $this->viewBuilder()->layout(false);
        $this->response->type('pdf');
    }

    public function ropPdf($id = null)
    {
        if (empty($id)) {
            throw new NotFoundException();
        }
        $this->loadModel('SaleDetails');
        $this->loadModel('Sales');

        $saleDetails = $this->SaleDetails->find('threaded', [
            'keyField' => 'id',
            'parentField' => 'parent_id',
        ])->where([
            'SaleDetails.sale_id' => $id,
        ])->contain([
            'SaleStockDetails' => [
                'Stocks' => ['ImportDetails'],
            ],
        ])->toArray();

        $sale = $this->Sales->findById($id)
            ->contain([
                'Currencies',
                'Customers' => [
                    'InfoDetails' => [
                        'conditions' => ['type' => TYPE_CUSTOMER],
                    ],
                    'MedicalCorporations' => ['joinType' => 'LEFT'],
                    'ClinicDoctors' => [
                        'Doctors',
                    ],
                ],
                'SaleTaxInvoices',
            ])->first();

//        pr($saleDetails);
//        exit;

        $this->set(compact('saleDetails', 'sale'));
        $this->viewBuilder()->layout(false);
        $this->response->type('pdf');
    }

    public function ropHtml($id = null)
    {
        if (empty($id)) {
            throw new NotFoundException();
        }
        $this->loadModel('SaleDetails');
        $this->loadModel('Sales');

        $saleDetails = $this->SaleDetails->find('threaded', [
            'keyField' => 'id',
            'parentField' => 'parent_id',
        ])->where([
            'SaleDetails.sale_id' => $id,
        ])->contain([
            'SaleStockDetails' => [
                'Stocks' => ['ImportDetails'],
            ],
        ])->toArray();

        $sale = $this->Sales->findById($id)
            ->contain([
                'Currencies',
                'Customers' => [
                    'InfoDetails' => [
                        'conditions' => ['type' => TYPE_CUSTOMER],
                    ],
                    'MedicalCorporations' => ['joinType' => 'LEFT'],
                    'ClinicDoctors' => [
                        'Doctors',
                    ],
                ],
                'SaleTaxInvoices',
            ])->first();

        $this->set(compact('saleDetails', 'sale'));
        $this->viewBuilder()->layout('print');
    }

    public function customClearencePdf($sale_id = null)
    {
        if (empty($sale_id)) {
            throw new NotFoundException(__('TXT_ITEM_NOT_FOUND'));
        }

        $this->loadModel('Sales');
        $pdf_data = $this->Sales->find()
            ->contain([
                'Customers' => [
                    'InfoDetails' => function($q) {
                        return $q->where(['InfoDetails.type' => TYPE_CUSTOMER]);
                    },
                    'ClinicDoctors' => [
                        'Doctors'
                    ],
                    'MedicalCorporations' => [
                        'joinType' => 'LEFT'
                    ],
                    'Countries',
                ],
                'SaleCustomClearanceDocs',
                'SaleDetails' => [
                    'ProductDetails',
                    'sort' => ['ProductDetails.type' => 'asc'],
                ],
            ])
            ->where([
                //'Sales.is_suspend' => 0,
                'Sales.id' => $sale_id
            ])
            //->order(['ProductDetails.type' => 'asc'])
            ->first();

        if (!$pdf_data) {
            throw new NotFoundException(__('TXT_ITEM_NOT_FOUND'));
        }

        $detail_id = [];
        if (!empty($pdf_data->sale_details)) {
            $i = 0;
            foreach ($pdf_data->sale_details as $sale) {
                $detail_id[$i] = $sale->product_detail_id;
                $i++;
                if ($i == 6) {
                    break;
                }
            }
        }

        $manufacturer_info = '';

        if (!empty($detail_id)) {
            $this->loadModel('ProductDetails');
            $brands = $this->ProductDetails->find()
                ->contain([
                    'Products' => [
                        'ProductBrands' => [
                            'Manufacturers' => ['Countries']
                        ]
                    ]
                ])
                ->where([
                    'ProductDetails.id IN ' => $detail_id
                ])
                ->group(['Manufacturers.id']);

            if ($brands) {
                foreach ($brands as $brand) {
                    $country_jp = '';
                    $coutry_en = '';

                    if ($brand->product->product_brand->manufacturer->country) {
                        $country_jp = $brand->product->product_brand->manufacturer->country->name;
                        $coutry_en = $brand->product->product_brand->manufacturer->country->name_en;
                    }
                    $manufacturer_info .= $brand->product->product_brand->manufacturer->name_en;
                    $manufacturer_info .= '（ ';
                    $manufacturer_info .= $brand->product->product_brand->manufacturer->name.' ） ';
                    $manufacturer_info .= '/';
                    $manufacturer_info .= $coutry_en;
                    $manufacturer_info .= '（ ';
                    $manufacturer_info .= $country_jp;
                    $manufacturer_info .= ' ） ';
                    $manufacturer_info .= "\n";
                }
            }
        }
        $this->set('manufacturer', rtrim($manufacturer_info, '"\n"'));
        $name = 'name' . $this->local;
        $name = 'name' . $this->local;
        $this->set(compact('pdf_data', 'name'));

        $this->viewBuilder()->layout(false);
        $this->response->type('pdf');
    }

    public function customClearenceHtml($sale_id = null)
    {
        if (empty($sale_id)) {
            throw new NotFoundException(__('TXT_ITEM_NOT_FOUND'));
        }

        $this->loadModel('Sales');
        $pdf_data = $this->Sales->find()
            ->contain([
                'Customers' => [
                    'InfoDetails' => function($q) {
                        return $q->where(['InfoDetails.type' => TYPE_CUSTOMER]);
                    },
                    'ClinicDoctors' => [
                        'Doctors'
                    ],
                    'MedicalCorporations' => [
                        'joinType' => 'LEFT'
                    ],
                    'Countries',
                ],
                'SaleCustomClearanceDocs',
                'SaleDetails' => [
                    'ProductDetails',
                    'sort' => ['ProductDetails.type' => 'asc'],
                ],
            ])
            ->where([
                //'Sales.is_suspend' => 0,
                'Sales.id' => $sale_id
            ])
            //->order(['ProductDetails.type' => 'asc'])
            ->first();

        if (!$pdf_data) {
            throw new NotFoundException(__('TXT_ITEM_NOT_FOUND'));
        }

        $detail_id = [];
        if (!empty($pdf_data->sale_details)) {
            $i = 0;
            foreach ($pdf_data->sale_details as $sale) {
                $detail_id[$i] = $sale->product_detail_id;
                $i++;
                if ($i == 6) {
                    break;
                }
            }
        }

        $manufacturer_info = '';

        if (!empty($detail_id)) {
            $this->loadModel('ProductDetails');
            $brands = $this->ProductDetails->find()
                ->contain([
                    'Products' => [
                        'ProductBrands' => [
                            'Manufacturers' => ['Countries']
                        ]
                    ]
                ])
                ->where([
                    'ProductDetails.id IN ' => $detail_id
                ])
                ->group(['Manufacturers.id']);

            if ($brands) {
                foreach ($brands as $brand) {
                    $country_jp = '';
                    $coutry_en = '';

                    if ($brand->product->product_brand->manufacturer->country) {
                        $country_jp = $brand->product->product_brand->manufacturer->country->name;
                        $coutry_en = $brand->product->product_brand->manufacturer->country->name_en;
                    }
                    $manufacturer_info .= $brand->product->product_brand->manufacturer->name_en;
                    $manufacturer_info .= '（ ';
                    $manufacturer_info .= $brand->product->product_brand->manufacturer->name.' ） ';
                    $manufacturer_info .= '/';
                    $manufacturer_info .= $coutry_en;
                    $manufacturer_info .= '（ ';
                    $manufacturer_info .= $country_jp;
                    $manufacturer_info .= ' ） ';
                    $manufacturer_info .= "\n";
                }
            }
        }
        $this->set('manufacturer', rtrim($manufacturer_info, '"\n"'));
        $name = 'name' . $this->local;
        $name = 'name' . $this->local;
        $this->set(compact('pdf_data', 'name'));

        $this->viewBuilder()->layout('print');
    }

    public function customInvoicePdf($id = null)
    {
        if (empty($id)) {
            throw new NotFoundException();
        }
        $this->loadModel('SaleDetails');
        $this->loadModel('Sales');

        $saleDetails = $this->SaleDetails->find('threaded', [
            'keyField' => 'id',
            'parentField' => 'parent_id'
        ])->where([
            'SaleDetails.sale_id' => $id,
        ])->contain([
            'Sales' => [
                'Customers' => [
                    'InfoDetails' => [
                        'conditions' => ['type' => TYPE_CUSTOMER],
                    ],
                ],
                'SaleTaxInvoices',
            ],
            'SaleStockDetails' => [
                'Stocks' => ['ImportDetails'],
            ],
        ])->toArray();

        $sale = $this->Sales->findById($id)
            ->contain(['Currencies'])->first();

        $this->set(compact('saleDetails', 'sale'));
        $this->viewBuilder()->layout(false);
        $this->response->type('pdf');
    }

    public function customInvoiceHtml($id = null)
    {
        if (empty($id)) {
            throw new NotFoundException();
        }
        $this->loadModel('SaleDetails');
        $this->loadModel('Sales');

        $saleDetails = $this->SaleDetails->find('threaded', [
            'keyField' => 'id',
            'parentField' => 'parent_id'
        ])->where([
            'SaleDetails.sale_id' => $id,
        ])->contain([
            'Sales' => [
                'Customers' => [
                    'InfoDetails' => [
                        'conditions' => ['type' => TYPE_CUSTOMER],
                    ],
                ],
                'SaleTaxInvoices',
            ],
            'SaleStockDetails' => [
                'Stocks' => ['ImportDetails'],
            ],
        ])->toArray();

        $sale = $this->Sales->findById($id)
            ->contain(['Currencies'])->first();

        $this->set(compact('saleDetails', 'sale'));
        $this->viewBuilder()->layout('print');
    }

    public function wInvoicePdf($id = null)
    {
        if (empty($id)) {
            throw new NotFoundException();
        }
        $this->loadModel('SaleDetails');
        $this->loadModel('Sales');

        $saleDetails = $this->SaleDetails->find('threaded', [
            'keyField' => 'id',
            'parentField' => 'parent_id',
        ])->where([
            'SaleDetails.sale_id' => $id,
        ])->contain([
            'SaleStockDetails' => [
                'Stocks' => ['ImportDetails'],
            ],
        ])->toArray();

        $sale = $this->Sales->findById($id)
            ->contain([
                'Currencies',
                'Customers' => [
                    'InfoDetails' => [
                        'conditions' => ['type' => TYPE_CUSTOMER],
                    ],
                    'MedicalCorporations' => ['joinType' => 'LEFT'],
                    'ClinicDoctors' => [
                        'Doctors',
                    ],
                ],
                'SaleTaxInvoices',
            ])->first();

//        pr($saleDetails);
//        exit;

        $this->set(compact('saleDetails', 'sale'));
        $this->viewBuilder()->layout(false);
        $this->response->type('pdf');
    }

    public function wOfJpPdf($id = null)
    {
        if (empty($id)) {
            throw new NotFoundException();
        }
        $this->loadModel('SaleDetails');
        $this->loadModel('Sales');

        $saleDetails = $this->SaleDetails->find('threaded', [
            'keyField' => 'id',
            'parentField' => 'parent_id',
        ])->where([
            'SaleDetails.sale_id' => $id,
        ])->contain([
            'SaleStockDetails' => [
                'Stocks' => ['ImportDetails'],
            ],
        ])->toArray();

        $sale = $this->Sales->findById($id)
            ->contain([
                'Currencies',
                'Customers' => [
                    'InfoDetails' => [
                        'conditions' => ['type' => TYPE_CUSTOMER],
                    ],
                    'MedicalCorporations' => ['joinType' => 'LEFT'],
                    'ClinicDoctors' => [
                        'Doctors',
                    ],
                ],
                'SaleTaxInvoices',
            ])->first();

//        pr($saleDetails);
//        exit;

        $this->set(compact('saleDetails', 'sale'));
        $this->viewBuilder()->layout(false);
        $this->response->type('pdf');
    }

    public function wOfEnPdf($id = null)
    {
        if (empty($id)) {
            throw new NotFoundException();
        }
        $this->loadModel('SaleDetails');
        $this->loadModel('Sales');

        $saleDetails = $this->SaleDetails->find('threaded', [
            'keyField' => 'id',
            'parentField' => 'parent_id',
        ])->where([
            'SaleDetails.sale_id' => $id,
        ])->contain([
            'SaleStockDetails' => [
                'Stocks' => ['ImportDetails'],
            ],
        ])->toArray();

        $sale = $this->Sales->findById($id)
            ->contain([
                'Currencies',
                'Customers' => [
                    'InfoDetails' => [
                        'conditions' => ['type' => TYPE_CUSTOMER],
                    ],
                    'MedicalCorporations' => ['joinType' => 'LEFT'],
                    'ClinicDoctors' => [
                        'Doctors',
                    ],
                ],
                'SaleTaxInvoices',
            ])->first();

//        pr($saleDetails);
//        exit;

        $this->set(compact('saleDetails', 'sale'));
        $this->viewBuilder()->layout(false);
        $this->response->type('pdf');
    }

    public function wRopSampleStockPdf($id = null)
    {
        if (empty($id)) {
            throw new NotFoundException();
        }
        $this->loadModel('SaleDetails');
        $this->loadModel('Sales');

        $saleDetails = $this->SaleDetails->find('threaded', [
            'keyField' => 'id',
            'parentField' => 'parent_id',
        ])->where([
            'SaleDetails.sale_id' => $id,
        ])->contain([
            'SaleStockDetails' => [
                'Stocks' => ['ImportDetails'],
            ],
        ])->toArray();

        $sale = $this->Sales->findById($id)
            ->contain([
                'Currencies',
                'Customers' => [
                    'InfoDetails' => [
                        'conditions' => ['type' => TYPE_CUSTOMER],
                    ],
                    'MedicalCorporations' => ['joinType' => 'LEFT'],
                    'ClinicDoctors' => [
                        'Doctors',
                    ],
                ],
                'SaleTaxInvoices',
            ])->first();

//        pr($saleDetails);
//        exit;

        $this->set(compact('saleDetails', 'sale'));
        $this->viewBuilder()->layout(false);
        $this->response->type('pdf');
    }
}
