<?php
namespace App\Controller;

use App\Controller\AppController;

class SellerPricingController extends AppController
{
    public function initialize()
    {
        parent::initialize();
        $this->loadModel('Countries');
    }

    public function index()
    {
        $en = $this->request->session()->read('tb_field');
        $this->loadModel('Products');
        $products = $this->Products->find()
            ->contain([
                'ProductDetails' => [
                    'PackSizes',
                    'SingleUnits',
                    'queryBuilder' => function($q) {
                        return $q->where(['ProductDetails.is_suspend' => 0]);
                    },
                ]
            ])
            ->where([
                'Products.is_suspend' => 0
            ]);

        $this->loadModel('Sellers');
        $sellers = $this->Sellers->sellerDropdown();

        $this->set(compact('en', 'products', 'sellers'));
        $this->set('_serialize', ['en', 'products', 'sellers']);
    }

    public function getPrice()
    {
        if ($this->request->is('ajax')) {
            $data = $this->SellerPricing->find()
                ->where([
                    'SellerPricing.product_detail_id' => $this->request->query('detail_id')
                ]);
            $this->set(compact('data'));
            $this->set('_serialize', ['data']);
        }
        $this->viewBuilder()->layout('ajax');
    }

    public function getSinglePrice()
    {
        if ($this->request->is('ajax')) {
            $id = $this->request->query('inserted_id');
            $data = $this->SellerPricing->get($id);

            $this->set(compact('data'));
            $this->set('_serialize', ['data']);
        }
        $this->viewBuilder()->layout('ajax');
    }

    public function create()
    {
        $pricing = $this->SellerPricing->newEntity();
        if ($this->request->is('ajax')) {
            $this->autoRender = false;
            $this->response->disableCache();
            $this->response->type('json');
            $this->viewBuilder()->layout(false);

            $pricing = $this->SellerPricing->patchEntity($pricing, $this->request->data);
            if ($this->SellerPricing->save($pricing)) {
                $this->response->body(json_encode(['status' => 1, 'inserted_id' => $pricing->id]));
                return $this->response;
            }

            $this->response->body(json_encode(['status' => 0, 'message' => $pricing->errors()]));
            return $this->response;
        }

        $this->set(compact('pricing'));
        $this->set('_serialize', ['pricing']);
    }

    public function edit()
    {
        if ($this->request->is('ajax')) {
            $this->autoRender = false;
            $this->response->disableCache();
            $this->response->type('json');
            $this->viewBuilder()->layout(false);

            $pricing = $this->SellerPricing->get($this->request->data['id']);

            $pricing = $this->SellerPricing->patchEntity($pricing, $this->request->data);
            if ($this->SellerPricing->save($pricing)) {
                $this->response->body(json_encode(['status' => 1]));
                return $this->response;
            }

            $this->response->body(json_encode(['status' => 0, 'message' => $pricing->errors()]));
            return $this->response;
        }
    }

    public function getFoc()
    {
        if ($this->request->is('ajax')) {
            $pricing_id = $this->request->query('pricing_id');

            $this->loadModel('FocDiscounts');
            $discounts = $this->FocDiscounts->find()
                ->contain([
                    'ProductDetails' => [
                        'queryBuilder' => function ($q) {
                            return $q->where(['ProductDetails.is_suspend' => 0]);
                        },
                        'Products' => function ($q) {
                            return $q->where(['Products.is_suspend' => 0]);
                        },
                        'SingleUnits',
                        'PackSizes',
                    ],
                ])
                ->where([
                    'FocDiscounts.external_id' => $pricing_id,
                    'FocDiscounts.type' => TYPE_SELLER_PRICING
                ]);

                $this->loadModel('Products');
                $detail_list = $this->Products->productDetailsDropdown();

            $en = $this->request->session()->read('tb_field');
            $this->set(compact('discounts', 'detail_list', 'en'));
            $this->set('_serialize', ['discounts', 'detail_list', 'en']);
        }
        $this->viewBuilder()->layout('ajax');
    }

    public function view()
    {
        if ($this->request->is('ajax')) {
            $this->loadModel('ProductDetails');

            $products = $this->ProductDetails->find()
                ->contain([
                    'Products.ProductBrands.Manufacturers' => [
                        'queryBuilder' => function($q) {
                            return $q->where(['Manufacturers.is_suspend' => 0]);
                        }
                    ],
                    'LogisticTemperatures',
                    'PackSizes',
                    'SingleUnits'
                ])
                ->where([
                    'ProductDetails.is_suspend' => 0,
                    'ProductDetails.id' => $this->request->query('detail_id')
                ])
                ->first();

            $en = $this->request->session()->read('tb_field');

            $this->set(compact('products', 'en'));
            $this->set('_serialize', ['products', 'en']);
        }

        $this->viewBuilder()->layout('ajax');
    }

    public function delete()
    {
        if ($this->request->is('ajax')) {
            $this->autoRender = false;
            $this->response->disableCache();
            $this->viewBuilder()->layout(false);
            $this->response->type('json');

            $this->request->allowMethod(['post', 'delete']);
            $data = $this->SellerPricing->get($this->request->data['id']);
            if ($this->SellerPricing->delete($data)) {
                $this->response->body(json_encode(['status' => 1]));
                return $this->response;
            }
            $this->response->body(json_encode(['status' => 0]));
            return $this->response;
        }
    }

    public function productDetailList()
    {
        if ($this->request->is('ajax')) {
            $en = $this->request->session()->read('tb_field');
            $seller_id = $this->request->query('seller_id');
            $this->loadModel('SellerBrands');
            $seller_brands = $this->SellerBrands->find()
                ->contain([
                    'SellerProducts' => [
                        'SellerProductDetails',
                        'queryBuilder' => function ($q) {
                            return $q->where(['SellerProducts.is_suspend' => 0]);
                        }
                    ]
                ])
                ->where([
                    'SellerBrands.seller_id' => $seller_id,
                    'SellerBrands.is_suspend' => 0
                ]);

            $product_id = [];
            $detail_id = [];
            if ($seller_brands) {
                foreach ($seller_brands as $brand) {
                    if ($brand->seller_products) {
                        foreach ($brand->seller_products as $sp) {
                            $product_id[] = $sp->product_id;
                            if ($sp->seller_product_details) {
                                foreach ($sp->seller_product_details as $detail) {
                                    $detail_id[] = $detail->product_detail_id;
                                }
                            }
                        }
                    }
                }
            }
            $product_details = [];
            if ($detail_id) {
                $this->loadModel('ProductDetails');
                $product_details = $this->ProductDetails->find()
                ->contain([
                    'Products' => [
                        'SellerProducts' => function ($q) {
                            return $q->where([
                                'SellerProducts.is_suspend' => 0
                            ]);
                        },
                        'queryBuilder' => function ($q) {
                            return $q->where(['Products.is_suspend' => 0]);
                        }
                    ],
                    'PackSizes',
                    'SingleUnits'
                ])
                ->where([
                    'ProductDetails.is_suspend' => 0,
                    'ProductDetails.id IN ' => $detail_id
                ]);
                $this->set(compact('product_details', 'en'));
                $this->set('_serialize', ['product_details', 'en']);
            }            
        }

        $this->viewBuilder()->layout('ajax');
    }
}