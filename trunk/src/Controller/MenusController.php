<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Collection\Collection;
use Cake\Network\Exception\NotFoundException;

class MenusController extends AppController
{
    private $args = [];

    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('Function');
        $this->loadModel('MenuControls');
    }

    public function index()
    {
        $nestedMenus = [];
        $menus = $this->Menus->find()
                ->all();
        $userGroups = $this->getUserGroupList();
        $menuControls = $this->MenuControls->findByUserGroupId($this->request->query('user_group_id'))
                ->contain(['Menus'])
                ->all();
        if (iterator_count($menuControls)) {
            $nestedMenus = $menuControls->nest('id', 'parent_id')->toList();
        }
        $data = [
            'userGroups' => $userGroups,
            'nestedMenus' => $nestedMenus,
            'menus' => $menus,
        ];
        $this->set($data);
    }

    public function createAndUpdate()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException;
        }
        $this->viewBuilder()->layout('ajax');
        $this->autoRender = false;
        $this->response->type('json');
        $this->response->disableCache();
        // Checking id exist
        if (isset($this->request->data['id'])) {
            $menu = $this->Menus->findById($this->request->data['id'])
                    ->contain(['MenuControls'])
                    ->first();
        } else {
            // Save a new menu with associated menu_controls object
            $menu = [
                'name_en' => $this->request->data['name_en'],
                'name' => $this->request->data['name'],
                'url' => $this->request->data['url'],
                'menu_controls' => [[
                    'user_group_id' => $this->request->data['user_group_id'],
                    'parent_id' => $this->request->data['parent_id'],
                    'menu_order' => $this->countChildMenu(
                            $this->request->data['parent_id'],
                            $this->request->data['user_group_id']),
                ]],
            ];
            // Initialize asscoiated data
            $menu = $this->Menus->newEntity($menu, [
                'associated' => ['MenuControls'],
            ]);
        }
        $menu = $this->Menus->patchEntity($menu, $this->request->data);
        // Checking error
        if ($menu->errors()) {
            $this->response->body(json_encode([
                'status' => 0,
                'message' => MSG_ERROR,
                'data' => $this->Function->getError($menu->errors()),
            ]));
            return $this->response;
        }
        // Update MenuControl info
        if (isset($this->request->data['menu_control_id'])) {
            $menuControl = $this->MenuControls->get($this->request->data['menu_control_id']);
            $menuControl->parent_id = $this->request->data['parent_id'];
            $menuControl->menu_order = 0;
            $this->MenuControls->save($menuControl);
        }
        // Save data
        if ($this->Menus->save($menu)) {
            $this->response->body(json_encode([
                'status' => 1,
                'message' => MSG_SUCCESS,
            ]));
            return $this->response;
        }
    }

    public function getForm()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException;
        }
        $this->loadModel('UserGroups');
        $this->viewBuilder()->layout('ajax');
        $menu = [];
        $menus = $this->getMenuList();
        $userGroups = $this->getUserGroupList();
        // Checking target of record edit/create
        if ($this->request->query('target') === TARGET_EDIT) {
            $menu = $this->Menus->findById($this->request->query('menu_id'))
                    ->contain(['MenuControls'])
                    ->first();
        }
        $data = [
            'menus' => $menus,
            'userGroups' => $userGroups,
            'menu' => $menu,
            'userGroupId' => $this->request->query('user_group_id'),
            'menuControlId' => $this->request->query('menu_control_id'),
        ];
        $this->set($data);
    }

    private function countChildMenu($parentId = false, $userGroupId = null)
    {
        $order = 0;
        if (!$parentId) {
            $menus = $this->MenuControls->find()
                    ->where(['parent_id IS' => null])
                    ->where(['user_group_id' => $userGroupId])
                    ->all();
            $order = $menus->max(function ($max) {
                return $max->menu_order;
            });
            return (int)($order->menu_order + 1);
        } else {
            $mainOrder = $this->MenuControls->findById($parentId)->first();
            $menus = $this->MenuControls->findByParentId($parentId)->all();
            // Checking child menu
            if (!iterator_count($menus)) {
                $order = ($mainOrder->menu_order) . ($order + 1);
                return (int)$order;
            }
            $maxOrder = $menus->max(function ($max) {
                return $max->menu_order;
            });
            return (int)($maxOrder->menu_order + 1);
        }
    }

    private function getMenuList()
    {
        $data = [];
        $menus = $this->MenuControls->find()
                ->contain(['Menus'])
                ->all();
        if ($menus) {
            foreach ($menus as $key => $value) {
                $data[] = [
                    'value' => $value->id,
                    'text' => $value->menu->name_en . ' ' . $value->menu->name,
                ];
            }
        }
        return $data;
    }

    private function getUserGroupList()
    {
        $data = [];
        $userGroups = $this->UserGroups->find()->all();
        if ($userGroups) {
            foreach ($userGroups as $key => $value) {
                $data[] = [
                    'value' => $value->id,
                    'text' => $value->name_en . ' ' . $value->name,
                ];
            }
        }
        return $data;
    }
}
