<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Network\Exception\NotFoundException;

class InvoiceDeliveryMethodsController extends AppController
{
    public function initialize() {
        parent::initialize();
        $this->loadComponent('Function');
    }

    public function getList()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->viewBuilder()->layout('ajax');
        $invoices = $this->InvoiceDeliveryMethods->find()
                ->order(['created' => 'desc'])
                ->all();
        $data = [
            'invoices' => $invoices,
        ];

        $this->set($data);
    }

    public function saveOrUpdate()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->viewBuilder()->layout('ajax');
        $this->autoRender = false;
        $this->response->disableCache();
        $this->response->type('json');
        //CHECK EXIST ID
        if ($this->request->data('id')) {
            $invoice = $this->InvoiceDeliveryMethods->get($this->request->data['id']);
        } else {
            $invoice = $this->InvoiceDeliveryMethods->newEntity();
        }
        $validator = $this->InvoiceDeliveryMethods->patchEntity($invoice, $this->request->data);
        //CHECK VALIDATION
        if ($validator->errors()) {
            $this->response->body(json_encode([
                'status' => 0,
                'message' => MSG_ERROR,
                'data' => $this->Function->getError($validator->errors()),
            ]));
        }
        //SAVE DATA
        if ($this->InvoiceDeliveryMethods->save($invoice)) {
            $this->response->body(json_encode([
                'status' => 1,
                'message' => MSG_SUCCESS,
                'data' => [
                    'invoices' => $this->getInvoiceDeliveryList(),
                ],
            ]));
        }
    }

    public function delete()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->viewBuilder()->layout('ajax');
        $this->autoRender = false;
        $this->response->disableCache();
        $this->response->type('json');
        $data =  $this->InvoiceDeliveryMethods->get($this->request->data['id']);
        //DELETE RECORD
        if ($this->InvoiceDeliveryMethods->delete($data)) {
            $this->response->body(json_encode([
                'status' => 1,
                'message' => MSG_SUCCESS,
                'data' => [
                    'invoices' => $this->getInvoiceDeliveryList(),
                ],
            ]));
        }
    }

    private function getInvoiceDeliveryList()
    {
        $invoices = $this->InvoiceDeliveryMethods->find()
                    ->order(['created' => 'desc'])
                    ->all();

        return $invoices;
    }
}