<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\Network\Exception\NotFoundException;

class StocksController extends AppController
{
    public function index()
    {
        $en = $this->request->session()->read('tb_field');
        $conditions['ProductDetails.is_suspend'] =  0;
        if (!empty($this->request->query('product_detail_id')) || !empty($this->request->query('product_detail'))) {
            $conditions['ProductDetails.id'] = $this->request->query('product_detail_id');
        }

        $user_class = $this->request->query('affiliation_class');

        if (!empty($this->request->query('manufacturer'))) {
            $conditions['Manufacturers.id'] = $this->request->query('manufacturer');
        }

        if (!empty($this->request->query('brand'))) {
            $conditions['ProductBrands.id'] = $this->request->query('brand');
        }

        if (!empty($this->request->query('product'))) {
            $conditions['Products.id'] = $this->request->query('product');
        }

        if (!empty($this->request->query('date'))) {
            $conditions['DATE(Stocks.created)'] = $this->request->query('date');
        }

        $display = PAGE_NUMBER;
        if (!empty($this->request->query('displays'))) {
            $display = $this->request->query('displays');
        }

        $this->loadModel('ProductDetails');

        $this->paginate = [
            'contain' => [
                'Products' => [
                    'ProductBrands' => [
                        'Manufacturers'
                    ]
                ],
                'PackSizes',
                'SingleUnits',
                'ImportDetails' => [
                    'Stocks' => function ($q) use ($user_class) {
                        if (!empty($user_class)) {
                            return $q->where([
                                'Stocks.affiliation_class' => $user_class,
                                'Stocks.is_suspend' => 0
                            ]);
                        }
                        return $q->where(['Stocks.is_suspend' => 0]);
                    }
                ]
            ],
            'conditions' => $conditions,
            'sortWhitelist' => [
                'Products.name' . $en,
                ''
            ],
            'order' => ['ProductDetails.created' => 'DESC'],
            'limit' => $display
        ];

        try {
            $stocks = $this->paginate($this->ProductDetails);
            $paging = $this->request->param('paging')['ProductDetails']['pageCount'];
        } catch (NotFoundException $e) {
            $paging = $this->request->param('paging')['ProductDetails']['pageCount'];
            $stocks = [];
        }

        $affiliations = $this->CustomConstant->affiliationClass();
        $this->loadModel('Manufacturers');
        $manufacturers = $this->Manufacturers->manufacturerDropdown($en, 'all');
        $this->set(compact('affiliations', 'manufacturers', 'en', 'stocks', 'paging'));
        $this->set('_serialize', ['affiliations', 'manufacturers', 'en', 'stocks']);
    }


    public function stockDetail($product_detail_id = null)
    {
        $en = $this->request->session()->read('tb_field');
        $conditions['ProductDetails.is_suspend'] = 0;
        $conditions['ProductDetails.id'] = $product_detail_id;
        if (!empty($this->request->query('product_detail_id')) || !empty($this->request->query('product_detail'))) {
            $conditions['ProductDetails.id'] = $this->request->query('product_detail_id');
        }

        $user_class = $this->request->session()->read('user_group')['affiliation_class'];
        if (!empty($this->request->query('affiliation_class'))) {
            $user_class = $this->request->query('affiliation_class');
        }

        $conditions['Stocks.affiliation_class'] = $user_class;

        if (!empty($this->request->query('manufacturer'))) {
            $conditions['Manufacturers.id'] = $this->request->query('manufacturer');
        }

        if (!empty($this->request->query('brand'))) {
            $conditions['ProductBrands.id'] = $this->request->query('brand');
        }

        if (!empty($this->request->query('product'))) {
            $conditions['Products.id'] = $this->request->query('product');
        }

        $this->loadModel('ProductDetails');

        if ($this->request->query('inactive')) {
            $conditions[] = [
                'Stocks.is_suspend' => 1,
            ];
        }
        $this->paginate = [
            'contain' => [
                'ImportDetails' => [
                    'Imports',
                    'ProductDetails' => [
                        'PackSizes',
                        'SingleUnits',
                        'Products' => [
                            'ProductBrands' => [
                                'Manufacturers'
                            ]
                        ]
                    ]
                ]
            ],
            'conditions' => $conditions,
            'sortWhitelist' => [

            ],
            'order' => ['Stocks.created' => 'DESC'],
            'limit' => PAGE_NUMBER
        ];

        try {
            $stocks = $this->paginate($this->Stocks);
            $paging = $this->request->param('paging')['Stocks']['pageCount'];
        } catch (NotFoundException $e) {
            $paging = $this->request->param('paging')['Stocks']['pageCount'];
            $stocks = [];
        }

        $affiliations = $this->CustomConstant->affiliationClass();
        $this->loadModel('Manufacturers');
        $manufacturers = $this->Manufacturers->manufacturerDropdown($en, 'all');
        $this->set(compact('stocks', 'affiliations', 'manufacturers', 'en', 'paging'));
        $this->set('_serialize', ['stocks', 'affiliations', 'manufacturers', 'en']);
    }

    public function autoCompleteList()
    {
        if ($this->request->is('ajax')) {
            $this->autoRender = false;
            $this->response->disableCache();
            $this->response->type('json');
            $this->viewBuilder()->layout(false);

            $this->loadModel('Products');
            $data = $this->Products->productDetailsDropdown();
            $en = $this->request->session()->read('tb_field');
            if ($data) {
                $this->response->body(json_encode([
                    'status' => 1,
                    'message' => 'success',
                    'en' => $en,
                    'data' => $data->toArray()
                ]));
                return $this->response;
            }

            $this->response->body(json_encode([
                'status' => 1,
                'message' => 'success',
                'en' => $en,
                'data' => []
            ]));
            return $this->response;
        }
    }
}
