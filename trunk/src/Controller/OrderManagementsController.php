<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Network\Exception\NotFoundException;

class OrderManagementsController extends AppController
{
    /**
    * @var string for store local session field
    */
    private $local;

    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('Csrf');
        $this->loadComponent('ReconstructData');
        $this->local = $this->request->session()->read('tb_field');
    }

    public function index()
    {
        $this->loadModel('Sales');
        $this->loadModel('Purchases');
        $this->loadModel('Deliveries');
        $keyword = '';
        $sales = [];
        $limit = $this->request->query('displays') ? $this->request->query('displays') : 10;

        if ($this->request->query('keyword')) {
            $keyword = $this->request->query('keyword');
        }
        // get sales data

        $conditions = [];
        $conditions['Sales.is_deleted'] = 0;
        if ($this->request->query('inactive')) {
            $conditions['Sales.is_suspend'] = 1;
        }
        $saleList = $this->Sales->find()
            ->where($conditions)
            ->contain([
                'SaleReceivePayments' => ['ReceivePayments'],
                'Sellers' => [
                    'joinType' => 'LEFT',
                    'Suppliers',
                    'Manufacturers',
                ],
                'SaleDetails' => function($q) {
                    $q->select([
                        'id',
                        'sale_id',
                        'product_name',
                        'total_amount' => $q->func()->sum('SaleDetails.quantity * SaleDetails.unit_price'),
                    ])->group('sale_id');
                    return $q;
                },
                'Users',
                'Currencies',
                'Customers',
            ])
            ->limit($limit)
            ->all();

        if ($saleList) {
            foreach ($saleList as $key => $value) {
                unset($value->customer_address);
                $sales[$key] = $value;
                $sales[$key]['deliveries'] = $this->Deliveries->getByField([
                    'Deliveries.sale_id' => $value->id,
                ], null, ['Customers']);
            }
        }
        $saleRecordCounts = $this->Sales->countRecords();
        // get purchase data
        $purchases = $this->Purchases->find()
                ->limit(30)
                ->all();
        $data = [
            'sales' => $sales,
            'purchases' => $purchases,
            'locale' => $this->local,
            'saleRecordCounts' => $saleRecordCounts,
        ];
        $this->set($data);
    }

    public function view()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }

    }

    public function advanceSearch()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->viewBuilder()->layout('ajax');
        $data = $this->ReconstructData->convertToObjFormVue($this->request->data['data']);
    }

    public function getRegisterList()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->viewBuilder()->layout('ajax');
        $data = [
            'local' => $this->local,
        ];
        $this->set($data);
    }

    public function getDatabyPage()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->viewBuilder()->layout('ajax');
        $this->response->type('json');
        $this->response->disableCache();
        $this->loadModel('Sales');
        $this->loadModel('Deliveries');
        $sales = [];
        $page = $this->request->query('page');
        $contains = [
            'SaleReceivePayments' => function($q) {
                $q->select([
                    'id',
                    'sale_id',
                    'count' => $q->func()->count('*'),
                ])
                ->group('SaleReceivePayments.sale_id');
                return $q;
            },
            'Sellers' => [
                'joinType' => 'LEFT',
                'Suppliers',
                'Manufacturers',
            ],
            'SaleDetails' => function($q) {
                $q->select([
                    'id',
                    'sale_id',
                    'total_amount' => $q->func()->sum('SaleDetails.quantity * SaleDetails.unit_price'),
                ])
                ->group('sale_id');
                return $q;
            },
            'Users',
            'Currencies',
            'Customers',
        ];
        $saleList = $this->Sales->getAllDataList('all', null, ['Sales.is_deleted' => 0],
            $contains, 10, null, null, $page);
        if ($saleList) {
            foreach ($saleList as $key => $value) {
                unset($value->customer_address);
                $sales[$key] = $value;
                $sales[$key]['deliveries'] = $this->Deliveries->getByField([
                    'Deliveries.sale_id' => $value->id,
                ], null, ['Customers']);
            }
        }
        $this->response->body(json_encode([
            'status' => 1,
            'message' => MSG_SUCCESS,
            'data' => $sales,
        ]));
        return $this->response;
    }

    public function getAdvanceSearchForm()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->viewBuilder()->layout('ajax');
        $years = [];
        $months = [];
        $days = [];

        for ($i = 2000; $i <= date('Y'); $i++) {
            $years[] = $i . ' 年';
        }
        for ($m = 1; $m <= 12; $m++) {
            $months[] = $m . ' 月';
        }
        for ($d = 1; $d <= 31; $d++) {
            $days[] = $d . ' 日';
        }

        $data = [
            'years' => $years,
            'months' => $months,
            'days' => $days,
        ];
        $this->set($data);
    }
}
