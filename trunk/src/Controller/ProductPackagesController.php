<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Network\Exception\NotFoundException;

class ProductPackagesController extends AppController
{
    public $locale;

    public function initialize()
    {
        parent::initialize();
        $this->loadModel('Countries');
        $this->loadModel('InfoDetails');
        $this->locale = $this->request->session()->read('tb_field');
    }

    public function index()
    {
        $options = [];
        if ($this->request->query('keyword')) {
            $keyword = trim($this->request->query('keyword'));
            $options = ['ProductPackages.name LIKE ' => '%' . $keyword . '%'];
        }
        $display = PAGE_NUMBER;
        if (!empty($this->request->query('displays'))) {
            $display = $this->request->query('displays');
        }
        if ($this->request->query('inactive')) {
            $options[] = [
                'ProductPackages.is_suspend' => 1,
            ];
        }
        $this->paginate = [
            'contain' => [
                'Currencies',
                'ProductPackageDetails' => [
                    'ProductDetails' => [
                        'Products' => ['ProductBrands'],
                    ],
                ],
            ],
            'conditions' => $options,
            'sortWhitelist' => [
                'ProductPackages.name',
                'ProductPackages.unit_price',
                'ProductPackages.remark',
                'ProductPackages.code',
                'Currencies.code'
            ],
            'limit' => $display,
            'order' => ['ProductPackages.name' => 'asc']
        ];
        $data = [
            'locale' => $this->locale,
            'data' => $this->paginate($this->ProductPackages),
            'paging' => $this->request->param('paging')['ProductPackages']['pageCount'],
            'display' => $display,
        ];
        $this->set($data);
    }

    public function create()
    {
        $product_package = $this->ProductPackages->newEntity();
        if ($this->request->is('ajax')) {
            $this->autoRender = false;
            $this->response->disableCache();
            $this->response->type('json');
            $this->viewBuilder()->layout(false);

            $product_package = $this->ProductPackages->patchEntity($product_package, $this->request->data);
            if ($this->ProductPackages->save($product_package)) {
                $this->response->body(json_encode(['status' => 1]));
                return $this->response;
            }

            $this->response->body(json_encode(['status' => 0, 'message' => $product_package->errors()]));
            return $this->response;
        }

        $this->loadModel('Currencies');
        $currencies = $this->Currencies->getDropdown();
        $txt_register = $this->Common->txtRegister();
        $this->set(compact('product_package', 'txt_register', 'currencies'));
        $this->set('_serialize', ['product_package']);
    }

    public function edit($id = null)
    {
        $product_package = $this->ProductPackages->get($id, ['conditions' => ['is_suspend' => 0]]);

        if ($this->request->is('ajax')) {
            $this->autoRender = false;
            $this->response->disableCache();
            $this->response->type('json');
            $this->viewBuilder()->layout(false);

            $product_package = $this->ProductPackages->patchEntity($product_package, $this->request->data);
            if ($this->ProductPackages->save($product_package)) {
                $this->response->body(json_encode(['status' => 1]));
                return $this->response;
            }

            $this->response->body(json_encode(['status' => 0, 'message' => $product_package->errors()]));
            return $this->response;
        }

        $this->loadModel('Currencies');
        $currencies = $this->Currencies->getDropdown();
        $txt_edit = $this->Common->txtEdit();
        $this->set(compact('product_package', 'txt_edit', 'currencies'));
        $this->set('_serialize', ['product_package']);
    }

    public function view()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->viewBuilder()->layout('ajax');
        $data = $this->ProductPackages->findById($this->request->query('id'))
            ->contain([
                'Currencies',
                'ProductPackageDetails' => [
                    'ProductDetails' => [
                        'Products' => ['ProductBrands'],
                    ],
                ],
                ])
            ->first();
        $data1 = [
            'data' => $data,
            'locale' => $this->locale,
        ];
        $this->set($data1);
    }

    public function changeOfStatus()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->viewBuilder()->layout('ajax');
        $this->autoRender = false;
        $this->response->disableCache();
        $this->response->type('json');
        $data = $this->ProductPackages->get($this->request->data['id']);
        $data->is_suspend = $this->request->data['status'];
        if ($this->ProductPackages->save($data)) {
            $this->response->body(json_encode([
                'status' => 1,
                'message' => MSG_SUCCESS,
            ]));
            return $this->response;
        }
    }

    public function delete()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->viewBuilder()->layout('ajax');
        $this->autoRender = false;
        $this->response->type('json');
        $this->response->disableCache();
        $data = $this->ProductPackages->get($this->request->data['id']);
        if ($this->ProductPackages->delete($data)) {
            $this->response->body(json_encode([
                'status' => 1,
                'message' => MSG_SUCCESS,
            ]));
            return $this->response;
        }
    }

    public function getStatus()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->viewBuilder()->layout('ajax');
        $data1 = $this->ProductPackages->get($this->request->query('id'));
        $data = [
            'data' => $data1,
            'status' => $this->request->query('status'),
        ];
        $this->set($data);
    }

    public function getDelete()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->viewBuilder()->layout('ajax');
        $data1 = $this->ProductPackages->get($this->request->query('id'));
        $data = [
            'data' => $data1,
        ];
        $this->set($data);
    }
}
