<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Network\Exception\NotFoundException;

class RepliesController extends AppController
{
    public function initialize() {
        parent::initialize();
        $this->loadComponent('Function');
    }

    public function create()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->autoRender = false;
        $this->response->disableCache();
        $this->response->type('json');
        $this->viewBuilder()->layout(false);
        $entity = $this->Replies->newEntity();
        $reply = $this->Replies->patchEntity($entity, $this->request->data);
        if ($reply->errors()) {
            $this->response->body(json_encode([
                'status'=> 0,
                'message' => MSG_ERROR,
                'data' => $this->Function->getError($reply->errors()),
            ]));
            return $this->response;
        }
        // CHECK FILE TYPE
        $file_type = $this->checkFileType($this->request->data['file']);
        if ($file_type) {
            $reply->file = $file_type['file'];
            $reply->mime_type = $file_type['mime_type'];
        }
        $reply->message_id = $this->request->data['message_id'];
        $reply->user_id = $this->Auth->user('id');
        $reply->parent_id = null;
        if ($this->request->data['parent_id'] !== 'undefined') {
            $reply->parent_id = $this->request->data['parent_id'];
        }
        if ($this->Replies->save($reply)) {
            $this->response->body(json_encode([
                'status'=> 1,
                'message' => MSG_SUCCESS,
            ]));
            return $this->response;
        }
    }

    public function getReplyForm()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->viewBuilder()->layout('ajax');
        $data = [
            'parent_id' => $this->request->query('parent_id'),
            'message_id' => $this->request->query('message_id'),
        ];
        $this->set($data);
    }

    public function delete()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->autoRender = false;
        $this->response->disableCache();
        $this->response->type('json');
        $this->viewBuilder()->layout(false);
        $data = $this->Replies->get($this->request->data['id']);
        $data->is_deleted = 1;
        if ($this->Replies->save($data)) {
            $this->response->body(json_encode([
                'status' => 1,
                'message' => MSG_SUCCESS,
            ]));
            return $this->response;
        }
    }

    private function checkFileType($file = [])
    {
        if (!$file || ($file === 'undefined')) {
            return false;
        }
        if (!is_uploaded_file($file['tmp_name'])) {
            return false;
        }
        $tmp = explode('.', $file['name']);
        $fname = date('YmdHis') . '_'
                . md5($file['name']) . '.'
                . end($tmp);
        // UPLOAD FILE
        $path = WWW_ROOT . 'img/uploads' . DS . 'replies' . DS . $fname;
        if (!move_uploaded_file($file['tmp_name'], $path)) {
            return false;
        }
        $data = [
            'file' => $fname,
            'mime_type' => end($tmp),
        ];
        return $data;
    }
}
