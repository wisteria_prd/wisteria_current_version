<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\I18n\Date;

class HomeController extends AppController
{
    public function index()
    {
        if ($this->Auth->user('id')) {
            $this->loadModel('Users');
            $data = $this->Users->get($this->Auth->user('id'));
            $this->Auth->setUser($data);
        }

        $this->loadModel('Sale1s');

        $clinic_name_list = [];
        $current = date('Y-m-d');
        $time = new Date($current);
//        $previous_2_months = $time->modify('-2 month')->format('Y-m-d');
//        $last_2_month_date = (new Date($previous_2_months))->modify('last day of this month');
//        $last_2_month_date = $last_2_month_date->format('Y-m-d');
//        $first_day = date('Y-m-d', strtotime($this->Sale1s->minOrderDate()));
//        $up_to_month_date = date('Y-m-d', strtotime($last_2_month_date . ' +1 day'));
        $last_3_years = $time->modify('-36 month')->format('Y-m-d');
//        $data1 = $this->selectBetween($first_day, $last_2_month_date);
//        $data2 = $this->selectBetween($up_to_month_date, $current);
//
//        if ($data2) {
//            foreach ($data2 as $key1 => $value1) {
//                $clinic_name_list[] = $value1['clinic_name_jp'];
//            }
//        }
//        if ($data1) {
//            foreach ($data1 as $key2 => $value2) {
//                if (in_array($value2['clinic_name_jp'], $clinic_name_list)) {
//                    unset($data1[$key2]);
//                }
//            }
//        }


//echo 'DATE(Sale1s.order_date) BETWEEN ' . $last_2_month_date . ' AND ' . $up_to_month_date;exit;
//pr($up_to_month_date);exit;
//pr($last_2_month_date);exit;
        $this->paginate = [
            'fields' => [
                'Sale1s.clinic_name_jp',
                'Sale1s.prefecture',
                'Sale1s.order_date',
                'Sale1s.product_name',
                'Sale1s.agent_name',
                'Sale1s.product_brand',
                'Users.id',
                'Users.firstname',
                'Users.lastname',
                //'total_quantity' => $query->func()->sum('quantity'),
                //'total_amount' => $query->func()->sum('quantity * unit_price'),
            ],
            'conditions' => [
                'DATE(Sale1s.order_date) >= ' . $last_3_years,
                'Sale1s.invoice_number <>' => '0',
                'Sale1s.invoice_number <>' => '',
            ],
            'limit' => 50,
            'order' => [
                'Sale1s.order_date' => 'DESC',
                'Sale1s.product_brand' => 'ASC',
            ],
            'group' => [
                'Sale1s.invoice_number',
                'Sale1s.product_brand',
            ],
            'contain' => [
                'Users',
            ],
            'sortWhitelist' => [
                'Sale1s.prefecture',
                'Sale1s.product_brand',
                'Sale1s.clinic_name_jp',
                'Sale1s.order_date',
                'Sale1s.product_name',
                'Sale1s.agent_name',
                'total_quantity',
                'total_amount',
            ],
            'finder' => 'totalAmount',
        ];
        $data = $this->paginate($this->Sale1s);
//pr($data);exit;
        $this->set('data', $data);
    }

    public function selectBetween($first_2_month_date, $last_2_month_date)
    {
        $query = $this->Sale1s->find();
        $data = $query->select([
            'Sale1s.clinic_name_jp',
            'Sale1s.order_date',
            'Sale1s.product_name',
            'Sale1s.agent_name',
            'Users.id',
            'Users.firstname',
            'Users.lastname',
            'total_quantity' => $query->func()->sum('quantity'),
            'total_amount' => $query->func()->sum('quantity * unit_price'),
        ])
        ->join([
            'table' => 'users',
            'alias' => 'Users',
            'type' => 'LEFT',
            'conditions' => 'Sale1s.user_id = Users.id'
        ]);
        $query->where([
            'Sale1s.invoice_number <>' => '0',
            'Sale1s.invoice_number <>' => '',
        ]);
        $query->where(function ($exp, $q) use($first_2_month_date, $last_2_month_date) {
            return $exp->between(
                'DATE(Sale1s.order_date)', $first_2_month_date, $last_2_month_date);
        });

        $query->group(['Sale1s.clinic_name_jp'])
        ->order([
            'Sale1s.order_date' => 'DESC',
            'Sale1s.clinic_name_jp' => 'ASC'
        ]);

        return $data;
    }

    public function visitTargetDetails($clinic_name = null)
    {
        if (is_null($clinic_name)) {
            throw new \Cake\Network\Exception\NotFoundException('Clinic name not found.');
        }
        $this->loadModel('Sale1s');

        $sub_query = $this->Sale1s->find();
        $sub_query->select(['order_date'])
            ->where([
                'Sale1s.clinic_name_jp' => $clinic_name
            ])
            ->order(['Sale1s.order_date' => 'DESC'])
            ->limit(1);

        $query = $this->Sale1s->find();
        $data = $query->select([
            'clinic_name_jp',
            'order_date',
            'product_name',
            'unit_price',
            'quantity'
        ])
        ->where([
            'Sale1s.clinic_name_jp' => $clinic_name,
            'Sale1s.invoice_number <>' => '0',
            'Sale1s.invoice_number <>' => '',
        ]);
        $query->where([
            'DATE(Sale1s.order_date)' => $sub_query
        ]);

//        $user_role = $this->Auth->user('role');
//        if ($user_role == MANAGER_SALE) {
//            $query->where([
//                'Sale1s.agent_name IS NULL',
//                'Sale1s.user_id IS NOT NULL'
//            ]);
//        } else if ($user_role == ROLE_WISTERIA_SALE) {
//            $query->where([
//                'Sale1s.user_id' => $this->Auth->user('id')
//            ]);
//        }

        $query->order(['Sale1s.order_date' => 'DESC']);
        $this->set('data', $data->toArray());
    }
}