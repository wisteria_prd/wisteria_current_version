<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Datasource\ConnectionManager;
use Cake\Network\Exception\NotFoundException;

class LogisticTemperaturesController extends AppController
{
    public $locale;

    public function initialize() {
        parent::initialize();
        $this->locale = $this->request->session()->read('tb_field');
    }

    public function create()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->viewBuilder()->layout('ajax');
        $this->autoRender = false;
        $data = $this->LogisticTemperatures->newEntity();

        if ($this->request->data) {
            $data->name = $this->request->data['name'];
            $data->name_en = $this->request->data['name_en'];
            $data->temperature_range = $this->request->data['temperature_range'];
            if ($this->LogisticTemperatures->save($data)) {
                $this->response->type('json');
                $this->response->body(json_encode([
                    'status' => 1,
                    'message' => 'success',
                ]));
                return $this->response;
            }
        }
    }

    public function edit($id)
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->viewBuilder()->layout('ajax');
        $this->autoRender = false;

        if ($this->request->data) {
            $data = $this->LogisticTemperatures->get($id);
            $data->name = $this->request->data['name'];
            $data->name_en = $this->request->data['name_en'];
            $data->temperature_range = $this->request->data['temperature_range'];
            if ($this->LogisticTemperatures->save($data)) {
                $this->response->type('json');
                $this->response->body(json_encode([
                    'status' => 1,
                    'message' => 'success',
                ]));
                return $this->response;
            }
        }
    }

    public function delete($id)
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->loadModel('ProductDetails');
        $this->viewBuilder()->layout('ajax');
        $this->autoRender = false;
        $message = [];

        $product_detail = $this->ProductDetails->find('all')
                ->where(['ProductDetails.logistic_temperature_id' => $id])->toArray();
        if ($product_detail) {
            $message = [
                'status' => 0,
                'message' => 'Invalid',
            ];
        } else {
            $data = $this->LogisticTemperatures->get($id);
            if ($this->LogisticTemperatures->delete($data)) {
                $message = [
                    'status' => 1,
                    'message' => 'success',
                ];
            }
        }
        $this->response->type('json');
        $this->response->body(json_encode($message));
        return $this->response;
    }


    public function updateSuspend()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->autoRender = false;
        $this->response->disableCache();
        $this->response->type('json');
        $this->viewBuilder()->layout(false);
        $data = $this->LogisticTemperatures->get($this->request->data['id']);
        $data->is_suspend = ($this->request->data['is_suspend'] == 0 ) ? 1 : 0;
        if ($this->LogisticTemperatures->save($data)) {
            $this->response->body(json_encode([
                'status' => 1,
                'message' => 'success'
            ]));
            return $this->response;
        }
        $this->response->body(json_encode([
            'status' => 0,
            'message' => 'error'
        ]));
        return $this->response;
    }

    public function getList()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->viewBuilder()->layout('ajax');
        $this->autoRender = false;
        $data = $this->LogisticTemperatures->find('all')->order(['LogisticTemperatures.name_en' => 'asc'])->toArray();

        $this->response->type('json');
        $this->response->body(json_encode([
            'status' => 1,
            'message' => 'success',
            'data' => $data,
        ]));
        return $this->response;
    }
}
