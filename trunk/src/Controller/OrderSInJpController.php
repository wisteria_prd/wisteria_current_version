<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Network\Exception\NotFoundException;
use Cake\Collection\Collection;

class OrderSInJpController extends AppController
{
    private $local;

    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('Function');
        $this->loadComponent('Common');
        $this->loadComponent('Image');
        $this->loadComponent('ReconstructData');
        $this->loadModel('Sales');
        $this->local = $this->request->session()->read('tb_field');
    }

    public function index()
    {
        $this->loadModel('Countries');
        $options = [];
        $options['AND']['Sales.type'] = TRADE_TYPE_SALE_BEHALF_S_IN_JP;
        if ($this->request->query('keyword')) {
            $keyword = $this->request->query('keyword');
            $options['AND']['OR'] = [
                'Sales.rop_number LIKE' => '%' . $keyword . '%',
                'Sales.sale_number LIKE' => '%' . $keyword . '%',
            ];
        }
        $displays = PAGE_NUMBER;
        if ($this->request->query('displays')) {
            $displays = $this->request->query('displays');
        }
        $this->paginate = [
            'conditions' => $options,
            'limit' => $displays,
            'contain' => [
                'Customers',
                'Currencies',
                'Sellers' => ['Suppliers', 'Manufacturers'],
                'SaleDetails' => function ($q) {
                    $q->select([
                        'id',
                        'sale_id',
                        'total_amount' => $q->func()->sum('unit_price * quantity'),
                    ])->group(['sale_id']);
                    return $q;
                },
            ],
            'sortWhitelist' => [
                'status',
                'rop_number',
                'sale_number',
                'Customers.name' . $this->local,
                'Sellers.id',
            ],
            'order' => [],
        ];
        $sale = $this->paginate($this->Sales);
        $data = [
            'local' => $this->local,
            'sale' => $sale,
        ];
        $this->set($data);
    }

    public function createForm()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->viewBuilder()->layout('ajax');
        $this->loadModel('Countries');
        $data = $this->Sales->newEntity();
        $kind_of = $this->request->query('kind_of');
        $country_id = null;
        if ($kind_of == 'jp_only' || $kind_of == 'not_jp') {
            $country_id = $this->Countries->getJPCountryId();
        }
        $sellers = $this->getSeller($kind_of, $country_id, $this->local);
        $currencies = ['' => __('TXT_SELECT_CURRENCY')] + $this->getCurrency()->toArray();
        $data1 = [
            'data' => $data,
            'sellers' => $sellers,
            'kind_of' => $kind_of,
            'currencies' => $currencies,
        ];
        $this->set($data1);
    }

    public function registerSale()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->viewBuilder()->layout('ajax');
        $this->response->type('json');
        $this->autoRender = false;
        $data = $this->Sales->newEntity();
        $request = [];
        parse_str($this->request->data['data'], $request);
        $validator = $this->Sales->patchEntity($data, $request);
        // check validation
        if ($validator->errors()) {
            $this->response->body(json_encode([
                'status' => 0,
                'message' => MSG_ERROR,
                'data' => $validator->errors(),
            ]));
            return $this->response;
        }
        $this->loadModel('Sellers');
        $this->loadModel('Customers');
        // get seller data
        list($name, $address) = $this->Sellers->getSeller($data->seller_id, $this->local);
        $data->seller_name = $name;
        $data->seller_address = $address;

        // get customer data
        list($c_name, $c_address) = $this->Customers->getCustomer($data->customer_id, $this->local);
        $data->customer_name = $c_name;
        $data->customer_address = $c_address;

        if (!$data->id) {
            $data->user_id = $this->Auth->user('id');
            $data->status = PO_STATUS_DRAFF;
            $data->affiliation_class = $this->request->session()->read('user_group.affiliation_class');
            $data->rop_number = $this->Sales->ropNumber($data->type);
        }
        if ($request['order_date']) {
            $order_date = date_create($request['order_date']);
            $data->order_date = date_format($order_date, 'Y-m-d');
        }
        if ($this->Sales->save($data)) {
            $this->response->body(json_encode([
                'status' => 1,
                'message' => MSG_SUCCESS,
                'data' => $data->id,
            ]));
            return $this->response;
        }
    }

    public function getCustomerList()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->viewBuilder()->layout('ajax');
        $this->autoRender = false;
        $this->response->disableCache();
        $this->response->type('json');
        $this->loadModel('Customers');
        $keyword = $this->request->query('keyword');
        $customers = $this->Customers->getAllDataList('all', null, [
            'AND' => [
                'Customers.is_suspend' => 0,
                'OR' => [
                    'Customers.name_en LIKE' => '%' . $keyword . '%',
                    'Customers.name LIKE' => '%' . $keyword . '%',
                ]
            ],
        ], null, 20);
        $this->response->body(json_encode([
            'status' => 1,
            'message' => MSG_SUCCESS,
            'data' => $customers,
        ]));
        return $this->response;
    }

    // CLEARN UP CODE

    public function of($id)
    {
        if ($id === null) {
            throw new NotFoundException();
        }
        $this->loadModel('SaleDetails');
        $this->loadModel('Medias');
        $this->loadModel('SaleReceivePayments');
        $this->loadModel('SaleDeliveries');
        $person = [];

        // get sales data
        $sale = $this->Sales->getByField(['Sales.id' => $id], null, [
            'Currencies',
            'Customers',
            'Sellers' => [
                'Manufacturers' => ['InfoDetails', 'Countries'],
                'Suppliers' => ['InfoDetails', 'Countries'],
            ],
        ]);

        // count child record where sale_id IS NOT null
        $countRecords = $this->SaleDetails->countRecords([
            'SaleDetails.sale_id' => $id,
            'SaleDetails.parent_id IS NOT' => null,
        ]);

        // get saleDetail with saleStockDetail
        $saleDetails = $this->SaleDetails->getAllDataList('threaded', null, ['SaleDetails.sale_id' => $id], [
            'SaleStockDetails' => function ($q) {
                $q->select([
                    'id',
                    'sale_detail_id',
                    'count' => $q->func()->count('*')
                ])
                ->group('sale_detail_id');
                return $q;
            },
        ]);

        // get payments
        $payments = $this->SaleReceivePayments->getSalePaymentListBySaleId($id);
        // get sale_deliveries
        $saleDeliveries = $this->SaleDeliveries->getByField(['SaleDeliveries.sale_id' => $id], null, ['Deliveries']);
        // get medias
        $medias = $this->Medias->getAllDataList('all', null, [
            'Medias.external_id' => $id,
            'Medias.type' => USER_ROLE_SALE,
        ]);

        if ($sale) {
            $this->loadModel('PersonInCharges');
            $ext = [];

            switch ($sale->seller->type) {
                case TYPE_SUPPLIER :
                    $ext = [
                        'type' => TYPE_SUPPLIER,
                        'id' => $sale->seller->supplier->id,
                    ];
                    break;
                default :
                    $ext = [
                        'type' => TYPE_MANUFACTURER,
                        'id' => $sale->seller->manufacturer->id
                    ];
            }
            $person = ['' => __('TXT_SELECT_CONTACT')] + $this->PersonInCharges->getList($this->local, $ext['type'], $ext['id']);
        }
        $typePdf = $this->CustomConstant->typePdf();

        // set data to view layout
        $data = [
            'typePdf' => $typePdf,
            'person' => $person,
            'saleDetails' => $saleDetails,
            'payments' => $payments,
            'medias' => $medias,
            'countRecords' => $countRecords,
            'local' => $this->local,
            'sale' => $sale,
            'saleDeliveries' => $saleDeliveries,
        ];
        $this->set($data);

        // render view by customer type
        switch ($sale->customer->type) {
            case CUSTOMER_TYPE_NEW :
            case CUSTOMER_TYPE_BAD :
                $this->render('of_new');
                break;
            case CUSTOMER_TYPE_CONTRACT :
                $this->render('of_contract');
                break;
            default :
                $this->render('of_normal');
        }
    }

    public function rop($id)
    {
        if ($id === null) {
            throw new NotFoundException();
        }
        $this->loadModel('SaleDetails');
        $this->loadModel('Currencies');
        $person = [];
        $doctors = [];
        $sale = $this->Sales->findById($id)
            ->contain([
                'Currencies',
                'Customers' => [
                    'InfoDetails',
                    'Countries',
                    'ClinicDoctors',
                    'PersonInCharges',
                    'CustomerSubsidiaries'  => ['Subsidiaries'],
                ],
                'Sellers' => [
                    'Manufacturers' => [
                        'InfoDetails',
                        'Countries'
                    ],
                    'Suppliers' => [
                        'InfoDetails',
                        'Countries'
                    ]
                ],
                'SaleDetails' => function ($q) {
                    $q->select([
                        'id',
                        'sale_id',
                        'count' => $q->func()->count('*')
                    ])->group(['sale_id']);
                    return $q;
                },
            ])->first();
        $saleDetail = $this->SaleDetails->getAllBySaleId($id, 'threaded');
        if ($sale->customer) {
            $this->loadModel('ClinicDoctors');
            $doctors = $this->ClinicDoctors->getDoctors($sale->customer->id);
        }
        $currencies = $this->Currencies->getDataList('id', 'code', null, $this->local);
        if ($sale) {
            $this->loadModel('PersonInCharges');
            $ext = [];
            switch ($sale->seller->type) {
                case TYPE_SUPPLIER :
                    $ext = [
                        'type' => TYPE_SUPPLIER,
                        'id' => $sale->seller->supplier->id,
                    ];
                    break;
                default :
                    $ext = [
                        'type' => TYPE_MANUFACTURER,
                        'id' => $sale->seller->manufacturer->id
                    ];
            }
            $person = ['' => __('TXT_SELECT_CONTACT')] + $this->PersonInCharges->getList($this->local, $ext['type'], $ext['id']);
        }
        $data = [
            'sale' => $sale,
            'saleDetail' => $saleDetail,
            'doctors' => $doctors,
            'currencies' => $currencies,
            'local' => $this->local,
        ];
        $this->set($data);
    }

    /**
     * Using for update sale record with subsidiary
     * @return object return data to ajax
     * @throws NotFoundException request is not ajax
     */
    public function updateSaleWithSubsidiary()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->loadModel('InfoDetails');
        $this->viewBuilder()->layout('ajax');
        $this->autoRender = false;
        $this->response->disableCache();
        $this->response->type('json');
        $data = $this->request->data;
        // Get info_detail records by email and type and tel
        $infoDetails = $this->InfoDetails->find()
                ->where(['external_id' => $data['subsidiary_id']])
                ->where(['type' => TRADE_TYPE_SALE_BEHALF_S_IN_JP])
                ->all();
        $infoDetail = new Collection($infoDetails);
        $infoDetail = $infoDetail->filter(function ($value, $key) use ($data) {
            return (($value->email === $data['customer_email']) || ($value->tel === $data['customer_tel']));
        })->toArray();
        $infoDetail = reset($infoDetail);
        // Check exist records
        if ($infoDetail) {
//            $infoDetail = $this->InfoDetails->get($infoDetail->id);
            $infoDetail->tel = $data['customer_tel'];
            $infoDetail->email = $data['customer_email'];
            $this->InfoDetails->save($infoDetail);
            pr($infoDetail);
        } else {
            $infoDetail = [];
            $infoDetail = $this->InfoDetails->newEntity();
            $infoDetail->external_id = $data['subsidiary_id'];
            $infoDetail->type = TRADE_TYPE_SALE_BEHALF_S_IN_JP;
            $infoDetail->tel = $data['customer_tel'];
            $infoDetail->email = $data['customer_email'];
            // Save new recored to info_details
            $this->InfoDetails->save($infoDetail);
            if ($this->InfoDetails->save($infoDetail)) {
                $this->response->body(json_encode([
                    'status' => 1,
                    'message' => MSG_SUCCESS,
                ]));
                return $this->response;
            }
        }
        // Get sale record by id
        // Update record
        $sale = $this->Sales->findById($data['id'])->first();
        $sale->customer_name = $data['customer_name'];
        $sale->customer_address = $data['customer_address'];
        $sale->customer_tel = $infoDetail->tel;
        $sale->customer_email = $infoDetail->email;
        // Update sale record
        if ($this->Sales->save($sale)) {
            $this->response->body(json_encode([
                'status' => 1,
                'message' => MSG_SUCCESS,
            ]));
            return $this->response;
        }
    }

    /**
     * Using for create a new record or edit record
     * @return object return json_encode when it's successful
     */
    public function createAndUpdate()
    {
        $this->ajaxRequest(true);
        // Check target new or edit
        if ($this->request->data['target'] === TARGET_EDIT) {
            $data = $this->Sales->get($this->request->data['id']);
            if (isset($this->request->data['subsidiary_id'])) {
                $data->subsidiary_id = $this->request->data['subsidiary_id'];
            } else {
                $data->subsidiary_id =  null;
            }
        } else {
            $data = $this->Sales->newEntity();
            $data->type = TRADE_TYPE_SALE_BEHALF_S_IN_JP;
        }
        $data = $this->Sales->patchEntity($data, $this->request->data);
        // Check Validation
        if ($data->errors()) {
            $this->response->body(json_encode([
                'status' => 0,
                'message' => MSG_ERROR,
                'data' => $this->Function->getError($data->errors()),
            ]));
            return $this->response;
        }
        $this->loadModel('Sellers');
        $this->loadModel('Customers');
        // Get seller data
        list($name, $address) = $this->Sellers->getSeller($data->seller_id, $this->local);
        $data->seller_name = $name;
        $data->seller_address = $address;
        // Get customer data
        list($c_name, $c_address) = $this->Customers->getCustomer($data->customer_id, $this->local);
        $data->customer_name = $c_name;
        $data->customer_address = $c_address;
        // Check exist
        if ($this->request->data['target'] === TARGET_NEW) {
            $data->user_id = $this->Auth->user('id');
            $data->status = PO_STATUS_DRAFF;
            $data->affiliation_class = $this->request->session()->read('user_group.affiliation_class');
            $data->rop_number = $this->Sales->ropNumber($data->type);
        }
        if ($this->request->data['order_date']) {
            $order_date = date_create($this->request->data['order_date']);
            $data->order_date = date_format($order_date, 'Y-m-d');
        }

        if ($this->Sales->save($data)) {
            $this->response->body(json_encode([
                'status' => 1,
                'message' => MSG_SUCCESS,
                'data' => [
                    'id' => $data->id,
                ],
            ]));
            return $this->response;
        }
    }

    /**
     * get all supplier name and manufacturer name by search keyword
     * return object
     */
    public function getAllSaleBySearch()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->viewBuilder()->layout('ajax');
        $this->autoRender = false;
        $this->response->type('json');
        $this->loadModel('Countries');

        $keyword = $this->request->query('keyword');
        $country_id = $this->Countries->getJPCountryId();
        $seller_id = '';

        if (!is_null($country_id)) {
            $this->loadModel('Sellers');
            $seller_id = $this->Sellers->sellerIdByCountry($country_id, false);
        }

        $sale = $this->Sales->find()
            ->select([
                'id',
                'seller_id',
                'is_suspend',
                'rop_number',
                'sale_number',
            ])
            ->where([
                'AND' => [
                    'Sales.is_suspend' => 0,
                    'Sales.seller_id' => (int)$seller_id,
                    'OR' => [
                        'Sales.rop_number LIKE' => '%' . $keyword . '%',
                        'Sales.sale_number LIKE' => '%' . $keyword . '%',
                    ],
                ],
            ])
            ->limit(20)
            ->all();

        $this->response->body(json_encode([
            'status' => 1,
            'message' => MSG_SUCCESS,
            'data' => $sale,
        ]));
        return $this->response;
    }

    public function invoice($id)
    {
        if (empty($id)) {
            throw new NotFoundException();
        }
        $this->loadModel('SaleDetails');
        $person = [];

        $data = $this->Sales->findById($id)
            ->contain([
                'Currencies',
                'Sellers' => ['Manufacturers', 'Suppliers'],
                'SaleDetails' => function ($q) {
                    $q->select([
                        'id',
                        'sale_id',
                        'count' => $q->func()->count('*')
                    ])->group(['sale_id']);

                    return $q;
                },
            ])->first();
        $saleDetail = $this->SaleDetails->getAllBySaleId($id, 'threaded');

        if ($data) {
            $this->loadModel('PersonInCharges');
            $ext = [];

            switch ($data->seller->type) {
                case TYPE_SUPPLIER :
                    $ext = [
                        'type' => TYPE_SUPPLIER,
                        'id' => $data->seller->supplier->id,
                    ];
                    break;
                default :
                    $ext = [
                        'type' => TYPE_MANUFACTURER,
                        'id' => $data->seller->manufacturer->id
                    ];
            }
            $person = ['' => __('TXT_SELECT_CONTACT')] + $this->PersonInCharges->getList($this->local, $ext['type'], $ext['id']);
        }

        $this->set(compact('data', 'person', 'saleDetail'));
    }

    public function updateStatus()
    {
        $this->ajaxRequest(false);
        $data = $this->Sales->get($this->request->data['id']);
        $data->status = $this->request->data['status'];
        $this->loadModel('SaleReceivePayments');
        $this->loadModel('Medias');
        $this->loadModel('Countries');
        $seller_id = [];

        $country_id = $this->Countries->getJPCountryId();
        if (!is_null($country_id)) {
            $this->loadModel('Sellers');
            $seller_id = $this->Sellers->sellerIdByCountry($country_id, false);
        }

        $medias = $this->Medias->find('all')
            ->where([
                'Medias.external_id' => $data->id,
                'Medias.type' => USER_ROLE_SALE,
            ])->toArray();

        if ($this->request->data['action'] === 'of') {
            $data->sale_number = $this->Sales->saleNumber($data->type, ['Sales.seller_id IN ' => $seller_id]);
            if (!$data->issue_date) {
                $data->issue_date = $this->request->data['issue_date'];
            }
        }

        if ($this->Sales->save($data)) {
            $response = [
                'step' => $this->request->data['step'],
                'status' => $data->status,
                'payments' => $this->SaleReceivePayments->getSalePaymentListBySaleId($data->id),
                'medias' => $medias,
            ];

            $this->Function->ajaxResponse(1, MSG_SUCCESS, $response);
        }
    }

    public function statusDownload()
    {
        $this->ajaxRequest(false);
        $this->loadModel('Sales');
        $this->loadModel('Medias');
        $medias = [];
        $response = [];
        $countNewFile = 0;
        $path = WWW_ROOT . 'img' . DS . 'uploads' . DS . 'po';

        $data = $this->request->data;
        $this->set('_serialize', ['data']);

        if (isset($data['files'])) {
            $countNewFile = count($data['files']);

            // check if new files
            if ($data['files']) {
                foreach ($data['files'] as $key => $value) {
                    $temp = explode('.', $value['name']);
                    $newFile = $this->Image->upload(null, $value, $path);
                    $medias[] = [
                        'external_id' => $data['id'],
                        'type' => USER_ROLE_SALE,
                        'document_type' => $data['types'][$key],
                        'file_name' => $newFile,
                        'file_type' => end($temp),
                    ];
                }
            }

            // check is old files
            if (isset($data['old_files'])) {
                foreach ($data['old_files'] as $key => $value) {
                    $temp = explode('.', $value);
                    $medias[$countNewFile] = [
                        'external_id' => $data['id'],
                        'type' => USER_ROLE_SALE,
                        'document_type' => $data['old_doc_types'][$key],
                        'file_name' => $value,
                        'file_type' => end($temp),
                    ];
                    $countNewFile++;
                }
            }
        }

        if ($medias) {
            $this->Medias->deleteMediaById($data['id'], USER_ROLE_SALE);
            $data1 = $this->Medias->newEntities($medias);
            $this->Medias->saveMany($data1);
        }

        $sale = $this->Sales->get($data['id']);
        $sale->status = $data['status'];

        if ($this->Sales->save($sale)) {
            $response = [
                'step' => $data['step'] + 1,
                'status' => $sale->status,
            ];
        }

        $this->Function->ajaxResponse(1, MSG_SUCCESS, $response);
    }

    public function updateSubStatus()
    {
        $this->ajaxRequest(false);
        $data = $this->Sales->get($this->request->data['id']);
        $data->sub_status = $this->request->data['subStatus'];

        if ($this->Sales->save($data)) {
            $this->ajaxResponse(1, MSG_SUCCESS);
        }
    }

    public function updateCurrencyExchange() {
        $this->ajaxRequest(false);
        $request = $this->request->data;
        $sale = $this->Sales->get($request['id']);

        $sale->currency_date = str_replace('/', '-', $request['currency_date']);
        $sale->currency_exchange_rate = $request['currency_exchange_rate'];

        if ($this->Sales->save($sale, ['validate' => 'exchange'])) {
            $this->Function->ajaxResponse(1, MSG_SUCCESS);
        } else {
            $this->Function->ajaxResponse(0, MSG_ERROR, $sale->errors());
        }
    }

    public function getInfo()
    {
        $this->ajaxRequest(false);
        $this->loadModel('InfoDetails');
        $response = $this->request->query;
        $data = $this->InfoDetails->getListByExtAndId($response['external_id'], TYPE_PERSON_IN_CHARGE);

        $this->Function->ajaxResponse(1, MSG_SUCCESS, $data);
    }

    public function getData()
    {
        $this->ajaxRequest(true);
        $this->loadModel('Countries');
        $this->loadModel('Sellers');
        $data = [];
        $subsidiary = [];
        $target = $this->request->query('target');
        // Check edit type
        if ($target === TARGET_EDIT) {
            $data = $this->Sales->findById($this->request->query('id'))
                    ->contain(['Customers'])
                    ->first();
        }
        // Retrieve subsidiaries
        if ($data && ($data->subsidiary_id !== null)) {
            $this->loadModel('Subsidiaries');
            $subsidiary = $this->Subsidiaries->findById($data->subsidiary_id)->first();
        }
        $sellers = $this->Sellers->find('all')
            ->where(['Sellers.kind' => USER_CLASS_WISTERIA])
            ->contain(['Suppliers', 'Manufacturers']);
        $currencies = ['' => __('TXT_SELECT_CURRENCY')] + $this->getCurrency()->toArray();
        $data1 = [
            'data' => $data,
            'subsidiary' => $subsidiary,
            'sellers' => $sellers,
            'currencies' => $currencies,
            'kind_of' => 'jp_only',
            'local' => $this->local,
            'target' => $target,
        ];
        $this->set($data1);
    }

    public function getFaxRop()
    {
        $this->ajaxRequest(true);
        $local = $this->local;
        $id = $this->request->query('id');
        $this->set(compact('local', 'id'));
    }

    public function changeDraffStatus()
    {
        $this->ajaxRequest(false);
        $this->response->type('json');
        $sale = $this->Sales->get($this->request->data['id']);
        $sale->status = STATUS_FAXED_ROP;
        if ($this->Sales->save($sale)) {
            $this->response->body(json_encode([
                'status' => 1,
                'message' => MSG_SUCCESS,
            ]));
        }
        return $this->response;
    }

    /**
     * Function getCurrency
     * using for get currency list
     * @return object retrun array list
     */
    private function getCurrency()
    {
        $this->loadModel('Currencies');
        $data = $this->Currencies->getDropdown('list');

        return $data;
    }

    /**
     * Function getSeller
     * using for get seller list
     * @return object retrun array list
     */
    private function getSeller()
    {
        $this->loadModel('Sellers');
        $field_name = 'name' . $this->local;
        $data = $this->Sellers->getSellerList($field_name);

        return $data;
    }

    /**
    * Function ajaxRequest
    * using for checking the request is ajax
    * @param boolean $auto_render assign value to $this->autoRender
    */
    private function ajaxRequest($auto_render = false)
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->viewBuilder()->layout('ajax');
        $this->autoRender = $auto_render;
    }
}
