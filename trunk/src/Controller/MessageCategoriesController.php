<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Network\Exception\NotFoundException;

class MessageCategoriesController extends AppController
{
    public function initialize() {
        parent::initialize();
        $this->loadComponent('Function');
    }

    public function getList()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->viewBuilder()->layout('ajax');
        $this->autoRender = false;
        $data = $this->MessageCategories->find('all')
        ->where(['MessageCategories.is_deleted <>' => 1])
        ->order(['MessageCategories.name_en' => 'asc'])->toArray();

        $this->response->type('json');
        $this->response->body(json_encode([
            'status' => 1,
            'message' => 'success',
            'data' => $data,
        ]));
        return $this->response;
    }

    public function create()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->viewBuilder()->layout('ajax');
        $this->autoRender = false;
        $data = $this->MessageCategories->newEntity();

        if ($this->request->data) {
            $data->name = $this->request->data['name'];
            $data->name_en = $this->request->data['name_en'];
            if ($this->MessageCategories->save($data)) {
                $this->response->type('json');
                $this->response->body(json_encode([
                    'status' => 1,
                    'message' => 'success',
                ]));
                return $this->response;
            }
        }
    }

    public function edit($id)
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->viewBuilder()->layout('ajax');
        $this->autoRender = false;

        if ($this->request->data) {
            $data = $this->MessageCategories->get($id);
            $data->name = $this->request->data['name'];
            $data->name_en = $this->request->data['name_en'];
            if ($this->MessageCategories->save($data)) {
                $this->response->type('json');
                $this->response->body(json_encode([
                    'status' => 1,
                    'message' => 'success',
                ]));
                return $this->response;
            }
        }
    }

    public function delete($id)
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->loadModel('Messages');
        $this->viewBuilder()->layout('ajax');
        $this->autoRender = false;
        $response = [];
        $message = $this->Messages->find('all')
                ->where(['Messages.message_category_id' => $id])->toArray();
        if ($message) {
            $response = [
                'status' => 0,
                'message' => 'Invalid',
            ];
        } else {
            $data = $this->MessageCategories->get($id);
            $data->is_deleted = 1;
            if ($this->MessageCategories->save($data)) {
                $response = [
                    'status' => 1,
                    'message' => 'success',
                ];
            }
        }
        $this->response->type('json');
        $this->response->body(json_encode($response));
        return $this->response;
    }
}
