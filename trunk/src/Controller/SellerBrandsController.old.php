<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Network\Exception\NotFoundException;

class SellerBrandsController extends AppController
{
    private $locale;

    public function initialize() {
        parent::initialize();
        $this->loadComponent('Function');
        $this->locale = 'name' . $this->request->session()->read('tb_field');
    }

    public function index()
    {
        $this->loadModel('Manufacturers');
        $this->loadModel('Suppliers');
        $options = [];
        if ($this->request->query('brand_name')) {
            $options['OR']['ProductBrands.name LIKE '] = '%' . trim($this->request->query('brand_name')) . '%';
            $options['OR']['ProductBrands.name_en LIKE '] = '%' . trim($this->request->query('brand_name')) . '%';
        }
        if ($this->request->query('supplier_id')) {
            $options['Suppliers.id'] = $this->request->query('supplier_id');
        }
        if ($this->request->query('manufacturer_id')) {
            $options['Manufacturers.id'] = $this->request->query('manufacturer_id');
        }
        $display = PAGE_NUMBER;
        if (!empty($this->request->query('displays'))) {
            $display = $this->request->query('displays');
        }
        $this->paginate = [
            'conditions' => $options,
            'limit' => $display,
            'contain' => [
                'ProductBrands',
                'Sellers' => [
                    'Manufacturers',
                    'Suppliers',
                ],
                'SellerProducts' => [
                    'queryBuilder' => function($q) {
                        return $q->select([
                            'id',
                            'seller_brand_id',
                            'product_id',
                            'product_detail_id',
                            'count' => $q->func()->count('*')
                        ])->group(['seller_brand_id']);
                    },
                    'Products',
                ],
            ],
            'sortWhitelist' => [
                'Manufacturers.name',
                'Manufacturers.name_en',
                'Suppliers.name',
                'Suppliers.name_en',
                'ProductBrands.name',
                'ProductBrands.name_en',
            ],
        ];
        $manufacturers = $this->Manufacturers->find()
            ->where(['Manufacturers.is_suspend' => 0])
            ->all();
        $suppliers = $this->Suppliers->find()
            ->where(['Suppliers.is_suspend' => 0])
            ->all();
        $data = [
            'data' => $this->paginate($this->SellerBrands),
            'paging' => $this->request->param('paging')['SellerBrands']['pageCount'],
            'locale' => $this->locale,
            'manufacturers' => $manufacturers,
            'suppliers' => $suppliers,
            'display' => $display,
        ];
        pr($data['data']);
        $this->set($data);
    }

    public function add()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->viewBuilder()->layout('ajax');
        $this->autoRender = false;
        $this->response->type('json');
        $params = new \stdClass();
        $params->seller_id = $this->request->data['seller_id'];
        parse_str($this->request->data['data'], $params->data);
        // CLEAR RECORD IN SELLER_BRANDS
        $this->SellerBrands->deleteAll([
            'SellerBrands.seller_id' => $params->seller_id,
        ]);
        // INITIAL ASSOCIATED DATA
        $seller_brands = $this->initialSellerBrandObj($params->seller_id, $params->data);
        $entities = $this->SellerBrands->newEntities($seller_brands, [
            'validate' => 'productBrand',
        ]);

        // $result = $this->SellerBrands->saveMany($entities);
        if ($result) {
            $this->response->body(json_encode([
                'status' => 1,
                'message' => MSG_SUCCESS,
            ]));

            return $this->response;
        }
        $this->response->body(json_encode([
            'status' => 0,
            'message' => MSG_ERROR,
            'data' => $this->Function->getErrors($entities),
        ]));
    }

    public function getListOfSellerBrands()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->viewBuilder()->layout('ajax');
        $this->loadModel('Sellers');
        $seller = $this->Sellers->find()
                ->where(['Sellers.type' => $this->request->query('type')])
                ->where(['Sellers.external_id' => $this->request->query('external_id')])
                ->contain([
                    'SellerBrands' => ['ProductBrands']
                ])
                ->first();
        $data = [
            'external_id' => $this->request->query('external_id'),
            'type' => $this->request->query('type'),
            'seller' => $seller,
        ];

        $this->set($data);
    }

    private function initialSellerBrandObj($seller_id, $data = [])
    {
        if (!isset($data['product_brand_id']) && !$data['product_brand_id']) {
            return false;
        }
        $seller_brands = [];
        foreach ($data['product_brand_id'] as $key => $value) {
            $seller_brands[] = [
                'seller_id' => $seller_id,
                'product_brand_id' => $value,
            ];
        }
        if ($seller_brands) {
            return $seller_brands;
        }

        return false;
    }
}
