<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Network\Exception\NotFoundException;

class UserPermissionsController extends AppController
{
    /**
    * @var string for store local session field
    */
    private $local;

    public function initialize() {
        parent::initialize();
    }

    public function update()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->viewBuilder()->layout('ajax');
        $this->autoRender = false;
        $this->UserPermissions->deleteAll([
            'user_group_id' => $this->request->data['user_group_id'],
        ]);
        $data = [];
        $list = $this->request->data['list'];
        if ($list) {
            foreach ($list as $key => $value) {
                $data[] = [
                    'module_action_id' => $value,
                    'user_group_id' =>  $this->request->data['user_group_id'],
                ];
            }
        }
        $entities = $this->UserPermissions->newEntities($data);
        if ($this->UserPermissions->saveMany($entities)) {
            $this->response->body(json_encode([
                'status' => 1,
                'message' => MSG_SUCCESS,
            ]));
            return $this->response;
        }
    }

    public function setting()
    {
        // Checking params
        $userGroupId = $this->request->query('user_group_id');
        if (($userGroupId === null) || (empty($userGroupId))) {
            throw new NotFoundException();
        }
        $this->loadModel('ModuleControls');
        $moduleControls = $this->ModuleControls->find()
                ->contain([
                    'ModuleActions' => [
                        'UserPermissions' => function ($q) use ($userGroupId) {
                        $q->where(['user_group_id' => $userGroupId]);
                        return $q;
                        }
                    ],
                ])
                ->all();
        $auth = $this->Auth->user('user_group_id');
        $disabled = ($auth == $userGroupId) ? true : false;
        $data = [
            'moduleControls' => $moduleControls,
            'disabled' => $disabled,
        ];
        $this->set($data);
    }
}