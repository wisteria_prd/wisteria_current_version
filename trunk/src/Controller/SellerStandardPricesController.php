<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Network\Exception\NotFoundException;

class SellerStandardPricesController extends AppController
{
    public $field;

    public function initialize()
    {
        parent::initialize();
        $this->field = 'name' . $this->request->session()->read('tb_field');
        $this->loadModel('SellerStandardPrices');
        $this->loadComponent('Common');
    }

    public function index()
    {
        // TODO: remove this save instruction when create function will be fixed
        // $entity = $this->SellerStandardPrices->newEntity();
        // $entity->seller_product_detail_id = 4;
        // $entity->currency_id = 1;
        // $entity->is_suspend = 1;
        // $entity->purchase_standard_price = 200;
        // $this->SellerStandardPrices->save($entity);
        $options = [];
        if ($this->request->query('keyword')) {
            $keyword = $this->request->query('keyword');
            $options = [
                'OR' => [
                    'Products.name LIKE' => '%' . $keyword . '%',
                    'Products.name_en LIKE' => '%' . $keyword . '%',
                ],
            ];
        }
        $display = PAGE_NUMBER;
        if (!empty($this->request->query('displays'))) {
            $display = $this->request->query('displays');
        }
        //TODO: confirm is_suspend field
        if ($this->request->query('inactive')) {
            $options[] = [
                'SellerStandardPrices.is_suspend' => 1,
            ];
        }
        $this->paginate = [
            'conditions' => $options,
            'limit' => $display,
            'contain' => [
                'SellerProductDetails' => [
                    'joinType' => 'LEFT',
                    'SellerProducts' => [
                        'Products',
                        'SellerBrands' => [
                            'Sellers' => [
                                'Suppliers',
                                'Manufacturers',
                            ],
                        ],
                    ],
                ],
                'Currencies',
            ],
            'sortWhitelist' => [
                'Products.name_en',
                'Products.name',
                'SellerStandardPrices.purchase_standard_price',
            ],
        ];
        try {
            $data = $this->paginate($this->SellerStandardPrices);
            $paging = $this->request->param('paging')['SellerStandardPrices']['pageCount'];
        } catch (NotFoundException $e) {
            $paging = $this->request->param('paging')['SellerStandardPrices']['pageCount'];
            $data = [];
        }
        $this->set(compact('data', 'paging'));
    }

    public function create()
    {
        $data = $this->SellerStandardPrices->newEntity();
        $this->loadModel('Sellers');
        $sellers = $this->Sellers->sellerDropdownAsSupplier();
        $this->loadModel('Currencies');
        $currencies = $this->Currencies->find('list', [
            'keyField' => 'id',
            'valueField' => 'code',
        ])->toArray();
        if ($this->request->is('ajax')) {
            $this->autoRender = false;
            $this->response->disableCache();
            $this->response->type('json');
            $validate = $this->validate($this->request->data);
            if ($validate === true) {
                $this->loadModel('Sellers');
                $seller = $this->Sellers->find('all')
                    ->where([
                        'external_id' => $this->request->data['seller_id'],
                        'type' => $this->request->data['seller_type'],
                    ])->first();
                $this->loadModel('SellerBrands');
                $seller_brand = $this->SellerBrands->find('all')
                    ->where([
                        'seller_id' => $seller->id,
                        'product_brand_id' => $this->request->data['brand_id'],
                    ])->first();
                $this->loadModel('SellerProducts');
                $seller_product = $this->SellerProducts->find('all')
                    ->where([
                        'seller_brand_id' => $seller_brand->id,
                        'is_suspend' => IS_SUSPENDED,
                    ])->first();
                if ($seller_product) {
                    $this->loadModel('SellerProductDetails');
                    $seller_product_details = $this->SellerProductDetails->find('all')
                        ->where([
                            'seller_product_id' => $seller_product->id,
                            'product_detail_id' => $this->request->data['product_detail_id'],
                        ])->first();
                }

                if (isset($seller_product_details) && !empty($seller_product_details)) {
                    $data2 = [];
                    foreach ($this->request->data['price'] as $key => $value) {
                        if (!empty($value['price'])) {
                            $data[] = [
                                'seller_product_detail_id' => $seller_product_details->id,
                                'currency_id' => $value['currency_id'],
                                'purchase_standard_price' => $value['price'],
                            ];
                        }
                    }
                    if (!empty($data2)) {
                        $data = $this->SellerStandardPrices->patchEntity($data, $data2);
                        if ($this->SellerStandardPrices->saveMany($data)) {
                            $this->response->body(json_encode(['status' => 1, 'message' => '', 'data' => '']));
                            return $this->response;
                        }
                    }
                }
                $this->response->body(json_encode(['status' => 0, 'message' => 'no seller product detail', 'data' => '']));
                return $this->response;
            } else {
                $this->response->body(json_encode(['status' => 0, 'message' => '', 'data' => $validate]));
                return $this->response;
            }
        }

        $txt_register = $this->Common->txtRegister();
        $this->set(compact('data', 'sellers', 'currencies', 'txt_register'));
    }

    public function saveData($data)
    {
        $response = [];

        if ($data->errors()) {
            $response = json_encode([
                'status' => 0,
                'message' => MSG_ERROR,
                'data' => $this->errorField($data->errors(), $this->request->data)
            ]);
        } else {
            $conditions = [];
            if (isset($data->id)) {
                $conditions['AND'] = [
                    'SellerStandardPrices.id <>' => $data->id,
                    'SellerStandardPrices.product_detail_id' => $this->request->data['product_detail_id'],
                ];
            } else {
                $conditions['AND'] = [
                    'SellerStandardPrices.product_detail_id' => $this->request->data['product_detail_id'],
                    'SellerStandardPrices.seller_product_id' => $this->request->data['seller_product_id'],
                ];
            }
            $data1 = $this->SellerStandardPrices->find('all')
                ->where($conditions)
                ->toArray();
            if ($data1) {
                $response = json_encode([
                    'status' => 0,
                    'message' => MSG_ERROR,
                    'data' => [
                        'product_detail_id' => 'Data cannot be duplicate',
                    ]
                ]);
            } else {
                $this->SellerStandardPrices->save($data);
                $response = json_encode([
                    'status' => 1,
                    'message' => MSG_SUCCESS,
                ]);
            }
        }

        return $response;
    }

    public function delete()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->viewBuilder()->layout('ajax');
        $this->autoRender = false;
        $data = $this->SellerStandardPrices->get($this->request->data['id']);

        if ($this->SellerStandardPrices->delete($data)) {
            echo json_encode([
                'status' => 1,
                'message' => MSG_SUCCESS,
            ]);
        }
    }

    public function edit($id)
    {
        $txt_edit = $this->Common->txtEdit();
        $this->set(compact('txt_edit'));

        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->viewBuilder()->layout('ajax');
        $this->autoRender = false;
        $this->response->type('json');
        $this->response->disableCache();
        $response = [];
        $data = $this->SellerStandardPrices->get($id);
        $data = $this->SellerStandardPrices->patchEntity($data, $this->request->data);
        $response = $this->saveData($data);

        $this->response->body($response);
        return $this->response;
    }

    public function getBySellerProduct($id)
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->viewBuilder()->layout('ajax');
        $this->loadModel('ProductDetails');
        $product_details = [];
        $data = $this->SellerStandardPrices->findBySellerProductId($id)
            ->contain([
                'SellerProducts' => ['Currencies'],
            ])->toArray();

        if ($data) {
            $data1 = $this->ProductDetails->findByProductId($data[0]->seller_product->product_id)
                ->contain(['PackSizes', 'SingleUnits'])
                ->toArray();
            $product_details = $this->productDetailNameList($data1);
        }

        $this->set(compact('data', 'product_details'));
    }

    public function errorField($fields, $request)
    {
        $data = [];
        if ($fields) {
            foreach ($fields as $key => $value) {
                $data[$key] = reset($value);
            }
        }

        return $data;
    }

    public function productDetailNameList($list)
    {
        $data = [];
        if ($list) {
            foreach ($list as $item) {
                $data[$item->id] = $this->concatProductDetailName($item);
            }
        }

        return $data;
    }

    public function concatProductDetailName($item)
    {
        $su_name = ($this->field === 'name_en') ? $item->single_unit->name_en : $item->single_unit->name;
        $pz_name = ($this->field === 'name_en') ? $item->tb_pack_sizes->name_en : $item->tb_pack_sizes->name;
        $name = $item->pack_size . ' ' . $pz_name . ' X ' . $item->single_unit_size . ' ' . $su_name;

        return $name;
    }

    private function validate($data)
    {
        $errors = [];
        if (empty($data['seller_id'])) {
            $errors['seller_id'] = __('TXT_MESSAGE_REQUIRED');
        }
        if (empty($data['brand_id'])) {
            $errors['brand_id'] = __('TXT_MESSAGE_REQUIRED');
        }
        if (empty($data['product_detail_id'])) {
            $errors['product_detail_id'] = __('TXT_MESSAGE_REQUIRED');
        }
        foreach ($data['price'] as $key => $value) {
            if (empty($value['price'])) {
                $errors['price'][$key]['price'] =  __('TXT_MESSAGE_REQUIRED');
            }
            if (empty($value['currency_id'])) {
                $errors['price'][$key]['currency'] =  __('TXT_MESSAGE_REQUIRED');
            }
        }
        return empty($errors) ? true : $errors;
    }

    public function updateSuspend()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->autoRender = false;
        $this->response->disableCache();
        $this->response->type('json');
        $this->viewBuilder()->layout(false);
        $data = $this->SellerStandardPrices->get($this->request->data['id']);
        $data->is_suspend = ($this->request->data['is_suspend'] == 0 ) ? 1 : 0;
        if ($this->SellerStandardPrices->save($data)) {
            $this->response->body(json_encode([
                'status' => 1,
                'message' => 'success'
            ]));
            return $this->response;
        }
        $this->response->body(json_encode([
            'status' => 0,
            'message' => 'error'
        ]));
        return $this->response;
    }
}
