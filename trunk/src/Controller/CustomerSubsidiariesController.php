<?php
namespace App\Controller;
use App\Controller\AppController;
use Cake\Network\Exception\NotFoundException;

class CustomerSubsidiariesController extends AppController
{
    private $local;

    public function initialize()
    {
        parent::initialize();
        $this->local = $this->request->session()->read('tb_field');
    }

    public function getAutocompleteByCustomerId()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->viewBuilder()->layout('ajax');
        $this->autoRender = false;
        $this->response->disableCache();
        $this->response->type('json');
        $keyword = $this->request->query('keyword');
        $data = $this->CustomerSubsidiaries->find('all')
                ->where(['customer_id' => $this->request->query('customer_id')])
                ->contain([
                    'Subsidiaries' => function ($query) use ($keyword) {
                    $query->where([
                        'OR' => [
                            'name LIKE' => '%' . $keyword . '%',
                            'name_en LIKE' => '%' . $keyword . '%',
                        ],
                    ]);
                    return $query;
                    }
                ])
                ->limit(20);
        $this->response->body(json_encode([
            'message' => MSG_SUCCESS,
            'status' => 1,
            'data' => [
                'data' => $data,
                'local' => $this->local,
            ],
        ]));
        return $this->response;
    }
}
