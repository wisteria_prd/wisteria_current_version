<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Network\Exception\NotFoundException;

class PricingManagementsController extends AppController
{
    private $locale;

    public function initialize()
    {
        parent::initialize();
        $this->locale = $this->request->session()->read('tb_field');
    }

    public function index()
    {
        $mId = $this->request->query('filter_manufacturer');
        $bId = $this->request->query('filter_brand');
        $pId = $this->request->query('filter_product');
        $this->loadModel('ProductDetails');
        $this->loadModel('ProductPackages');
        $this->loadModel('Manufacturers');
        $this->loadModel('ProductBrands');
        $this->loadModel('Products');
        $manufacturers = $this->Manufacturers->getAvailableList();
        $brands = $this->ProductBrands->getAvailableListByManufacturerId($mId);
        $products = $this->Products->getAvailableListByProductBrandId($bId);
        $keyword = '';
        $options = [];
        if ($this->request->query('keyword')) {
            $keyword = trim($this->request->query('keyword'));
        }
        // SET PAGE LIMIT ON PAGINATION
        $display = PAGE_NUMBER;
        if (!empty($this->request->query('displays'))) {
            $display = $this->request->query('displays');
        }
        if ($this->request->query('inactive')) {
            $options['PricingManagements.is_suspend'] = 1;
        }
        $options[] = [
            'or' => [
                'Manufacturers.name LIKE' => '%' . $keyword .'%',
                'Manufacturers.name_en LIKE' => '%' . $keyword .'%',
                'ProductBrands.name LIKE' => '%' . $keyword .'%',
                'ProductBrands.name_en LIKE' => '%' . $keyword .'%',
                'Products.name LIKE' => '%' . $keyword .'%',
                'Products.name_en LIKE' => '%' . $keyword .'%',
            ],
        ];
        if ($this->request->query('filter_manufacturer')) {
            $options[] = [
                'Manufacturers.id' => $mId,
            ];
        }
        if ($this->request->query('filter_brand')) {
            $options[] = [
                'ProductBrands.id' => $bId,
            ];
        }
        if ($this->request->query('filter_product')) {
            $options[] = [
                'Products.id' => $pId,
            ];
        }
        $this->paginate = [
            'conditions' => $options,
            'limit' => $display,
            'order' => [
                'PricingManagements.id' => 'asc',
            ],
            'contain' => [
                'PricingRangeManagements' => [
                    'Currencies',
                ],
                'ProductDetails' => [
                    'Products' => [
                        'ProductBrands' => [
                            'Manufacturers',
                            'SellerBrands' => function($q) {
                                // get only supplier
                                return $q->group('SellerBrands.product_brand_id')
                                ->order('SellerBrands.id', 'desc')
                                ->contain([
                                    'Sellers' => [
                                        'Manufacturers' => function($q) {
                                            return $q->where([
                                                'Sellers.is_manufacture_seller' => 1
                                            ]);
                                        },
                                        'Suppliers',
                                    ]
                                ]);
                            }
                        ]
                    ]
                ],
            ],
            'sortWhitelist' => [
                'Manufacturers.name',
                'Suppliers.name',
                'Products.name',
                'PricingManagements.price_type',
            ],
        ];
        $query = $this->paginate($this->PricingManagements);
        // pr($query->toArray());exit;
        $data = [
            'data' => $query,
            'locale' => $this->locale,
            'display' => $display,
            'manufacturers' => $manufacturers,
            'paging' => $this->request->param('paging')['PricingManagements']['pageCount'],
            'brands' => $brands,
            'products' => $products,
        ];
       $this->set($data);
    }

    public function create()
    {
        // get currency
        $this->loadModel('Currencies');
        $currencies = $this->Currencies->getList($this->locale);
        $data = $this->PricingManagements->newEntity();
        $data1 = [
            'currencies' => $currencies,
            'data' => $data,
            'locale' => $this->locale,
            'customer' => [],
        ];
        $this->set($data1);
    }

    public function edit($id = null)
    {
        $data = $this->PricingManagements->find()
        ->where([
            'PricingManagements.id' => $id,
        ])
        ->contain([
            'Currencies',
            'PricingRangeManagements',
            'ProductDetails' => [
                'Products',
                'PackSizes',
                'SingleUnits',
            ],
            'ProductBrands',
            'Sellers' => [
                'Manufacturers',
                'Suppliers',
            ],
        ])->first();
        // get currency
        $this->loadModel('Currencies');
        $this->loadModel('Customers');
        $customer = [];
        $currencies = $this->Currencies->getList($this->locale);
        if (!$data) {
            throw new \Exception("Error Processing Request", 1);
        }
        if ($data->external_id_type === TYPE_CUSTOMER) {
            $customer = $this->Customers->findById($data->external_id)->first();
        }
        $data1 = [
            'currencies' => $currencies,
            'data' => $data,
            'locale' => $this->locale,
            'customer' => $customer,
        ];
        $this->set($data1);
    }

    public function saveOrUpdate()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->viewBuilder()->layout('ajax');
        $this->autoRender = false;
        $this->response->type('json');
        $this->loadModel('PricingRangeManagements');
        $pricingRanges = [];
        if ($this->request->data('id')) {
            $data = $this->PricingManagements->get($this->request->data['id']);
            $pricingRanges = $this->PricingRangeManagements->find()
                ->where(['pricing_management_id' => $this->request->data['id']])
                ->first();
        } else {
            $data = $this->PricingManagements->newEntity();
        }
        $validator = $this->PricingManagements->patchEntity($data, $this->request->data);
        // check validation
        if ($validator->errors()) {
            $this->response->body(json_encode([
                'status' => 0,
                'message' => MSG_ERROR,
                'data' => $validator->errors(),
            ]));
            return $this->response;
        }
        if ($pricingRanges) {
            $this->PricingRangeManagements->deleteAll([
                'pricing_management_id' => $this->request->data['id']
            ]);
        }
        switch ($this->request->data['external_id_type']) {
            case TYPE_MANUFACTURER:
            case TYPE_SUPPLIER:
                $data->external_id = $this->request->data['seller_id'];
                break;
            case TYPE_CUSTOMER:
                $data->external_id = $this->request->data['customer_id'];
                break;
            default:
                $data->external_id = null;
        }
        if ($this->PricingManagements->save($data)) {
            $this->response->body(json_encode([
                'status' => 0,
                'message' => MSG_SUCCESS,
            ]));
            return $this->response;
        }
    }

    public function delete()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->viewBuilder()->layout('ajax');
        $this->autoRender = false;
        $this->response->type('json');
        $this->response->disableCache();
        $data = $this->PricingManagements->get($this->request->data['id']);
        if ($this->PricingManagements->delete($data)) {
            $this->response->body(json_encode([
                'status' => 1,
                'message' => MSG_SUCCESS,
            ]));

            return $this->response;
        }
    }

    public function view()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->viewBuilder()->layout('ajax');
        $data = $this->PricingManagements->findById($this->request->query('id'))
            ->contain([
                'PricingRangeManagements' => [
                    'Currencies',
                ],
                'ProductDetails' => [
                    'Products' => [
                        'ProductBrands' => [
                            'Manufacturers',
                            'SellerBrands' => function($q) {
                                return $q->group('SellerBrands.product_brand_id')
                                ->order('SellerBrands.id', 'desc')
                                ->contain([
                                    'Sellers' => [
                                        'Manufacturers' => function($q) {
                                            return $q->where([
                                                'Sellers.is_manufacture_seller' => 1
                                            ]);
                                        },
                                        'Suppliers',
                                    ]
                                ]);
                            }
                        ]
                    ]
                ],
            ])
            ->first();
        $response = [
            'locale' => $this->locale,
            'data' => $data,
        ];
        $this->set($response);
    }

    public function changeOfStatus()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->viewBuilder()->layout('ajax');
        $this->autoRender = false;
        $this->response->disableCache();
        $this->response->type('json');
        $data = $this->PricingManagements->get($this->request->data['id']);
        $data->is_suspend = $this->request->data['status'];
        if ($this->PricingManagements->save($data)) {
            $this->response->body(json_encode([
                'status' => 1,
                'message' => MSG_SUCCESS,
            ]));

            return $this->response;
        }
    }

    public function getStatus()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->viewBuilder()->layout('ajax');
        $pricing = $this->PricingManagements->get($this->request->query('id'));
        $data = [
            'manufacturer' => $pricing,
            'status' => $this->request->query('status'),
        ];

        $this->set($data);
    }

    public function getDelete()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->viewBuilder()->layout('ajax');
        $pricing = $this->PricingManagements->get($this->request->query('id'));
        $data = [
            'data' => $pricing,
        ];

        $this->set($data);
    }

    public function getProductBrandName()
    {
        if (!$this->request->is('ajax')) {
            throw new \Exception("Error Processing Request", 1);
        }
        $this->viewBuilder()->layout('ajax');
        $this->loadModel('Sellers');
        $brands = $this->Sellers->find()
        ->where([
            'Sellers.id' => $this->request->data['seller_id']
        ])
        ->contain([
            'SellerBrands' => [
                'ProductBrands'
            ]
        ])->first();
        $data = [
            'data' => $brands,
        ];
        $this->set($data);
    }

    public function getProductDetailName()
    {
        if (!$this->request->is('ajax')) {
            throw new \Exception("Error Processing Request", 1);
        }
        $this->viewBuilder()->layout('ajax');
        $this->loadModel('ProductBrands');
        $brands = $this->ProductBrands->find()
        ->where([
            'ProductBrands.id' => $this->request->data['product_brand_id']
        ])
        ->contain([
            'Products' => [
                'ProductDetails' => function($q) {
                    return $q->group('ProductDetails.product_id');
                }
            ]
        ])->first();
        $data = [
            'data' => $brands,
        ];
        $this->set($data);
    }

    public function getSellerName()
    {
        if (!$this->request->is('ajax')) {
            throw new \Exception("Error Processing Request", 1);
        }
        $this->viewBuilder()->layout('ajax');
        $sellers = [];
        switch ($this->request->data['filter_type']) {
            case 'customer':
                $this->loadModel('Customers');
                $sellers = $this->Customers->find(
                    'list', [
                        'keyField' => 'id',
                        'valueField' => function ($data) {
                            return $data->get('label');
                    }]
                )
                ->where([
                    'Customers.is_suspend <>' => 1,
                ])
                ->toArray();
                break;

            case TYPE_SUPPLIER:
            case TYPE_MANUFACTURER:
            default:
                $this->loadModel('Sellers');
                $sellers = $this->Sellers->find(
                    'list', [
                        'keyField' => 'id',
                        'valueField' => function ($data) {
                            return $data->get('label');
                    }]
                )
                ->contain([
                    'Manufacturers',
                    'Suppliers',
                ])->toArray();
                break;
        }
        $data = [
            'data' => $sellers,
        ];
        $this->set($data);
    }
}
