<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Network\Exception\NotFoundException;

class ManufacturersController extends AppController
{
    public $locale;

    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('Function');
        $this->locale = $this->request->session()->read('tb_field');
        $this->loadModel('InfoDetails');
    }

    public function index()
    {
        $options = [];
        if ($this->request->query('keyword')) {
            $options['OR'] = [
                'Manufacturers.name LIKE' => '%' . $this->request->query('keyword') .'%',
                'Manufacturers.name_en LIKE' => '%' . $this->request->query('keyword') .'%',
            ];
        }
        // SET PAGE LIMIT ON PAGINATION
        $display = PAGE_NUMBER;
        if (!empty($this->request->query('displays'))) {
            $display = $this->request->query('displays');
        }
        // CHECK FIELD NAME BASED ON LOCALE
        $name = 'Manufacturers.name';
        if ($this->locale === '_en') {
            $name = 'Manufacturers.name_en';
        }
        if ($this->request->query('inactive')) {
            $options['Manufacturers.is_suspend'] = 1;
        }
        $this->paginate = [
            'conditions' => $options,
            'limit' => $display,
            'order' => [$name => 'asc'],
            'contain' => [
                'Countries',
                'ProductBrands' => ['Products'],
                'PersonInCharges' => function($q) {
                    $q->select([
                        'PersonInCharges.id',
                        'PersonInCharges.type',
                        'PersonInCharges.external_id',
                        'total' => $q->func()->count('PersonInCharges.external_id')
                   ])
                    ->where(['PersonInCharges.type' => TYPE_MANUFACTURER])
                    ->group(['PersonInCharges.external_id']);
                   return $q;
                },
            ],
        ];
        $data = [
            'manufacturers' => $this->paginate($this->Manufacturers),
            'locale' => $this->locale,
            'display' => $display,
            'paging' => $this->request->param('paging')['Manufacturers']['pageCount'],
        ];

        $this->set($data);
    }

    public function create()
    {
        $this->loadModel('Countries');
        $entity = $this->Manufacturers->newEntity();
        $countries = $this->Countries->getListOfCountries($this->locale);
        $data = [
            'manufacturer' => $entity,
            'auth' => $this->auth,
            'countries' => $countries,
        ];

        $this->set($data);
    }

    public function edit($id = null)
    {
        $this->loadModel('Countries');
        $countries = $this->Countries->getListOfCountries($this->locale);
        $manufacturer = $this->Manufacturers->findById($id)
                ->contain([
                    'Sellers',
                    'InfoDetails' => function($q) use($id) {
                        return $q->where([
                            'InfoDetails.type' => TYPE_MANUFACTURER,
                            'InfoDetails.external_id' => $id,
                        ]);
                    }
                ])
                ->first();
        $data = [
            'manufacturer' => $manufacturer,
            'auth' => $this->auth,
            'countries' => $countries,
        ];
        $this->set($data);
    }

    public function view()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->viewBuilder()->layout('ajax');
        $manufacturer = $this->Manufacturers->findById($this->request->query('id'))
                ->contain([
                    'Countries',
                    'InfoDetails' => function($q) {
                        return $q->where(['InfoDetails.type' => TYPE_MANUFACTURER]);
                    },
                    'PersonInCharges' => function($q) {
                        $q->select([
                            'PersonInCharges.id',
                            'PersonInCharges.type',
                            'PersonInCharges.external_id',
                            'total' => $q->func()->count('PersonInCharges.external_id')
                       ])
                        ->where(['PersonInCharges.type' => TYPE_MANUFACTURER])
                        ->group(['PersonInCharges.external_id']);
                       return $q;
                    },
                    'ProductBrands' => [
                        'Products' => function ($q) {
                            return $q->select([
                                'id',
                                'product_brand_id',
                                'total' => $q->func()->count('Products.product_brand_id')
                                ])->group(['product_brand_id']);
                        },
                    ],
                ])
                ->first();
        $data = [
            'locale' => $this->locale,
            'manufacturer' => $manufacturer,
        ];

        $this->set($data);
    }

    public function saveAndUpdate()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->viewBuilder()->layout('ajax');
        $this->autoRender = false;
        $this->response->disableCache();
        $this->response->type('json');
        // if exist record
        if ($this->request->data('id')) {
            $manufacturer = $this->Manufacturers->get($this->request->data['id']);
        } else {
            $manufacturer = $this->Manufacturers->newEntity();
        }
        $response = $this->Manufacturers->patchEntity($manufacturer, $this->request->data);
        // check validation
        if ($response->errors()) {
            $this->response->body(json_encode([
                'status' => 0,
                'message' => MSG_ERROR,
                'data' => $response->errors(),
            ]));
            return $this->response;
        }
        // delete all datas in info_details
        $this->InfoDetails->deleteAll([
            'external_id' => $this->request->data['id'],
            'type' => TYPE_MANUFACTURER,
        ]);
        $associated = [
            'associate' => [
                'InfoDetails',
            ],
        ];
        if ($this->Manufacturers->save($manufacturer, $associated)) {
            $this->checkSeller($manufacturer);
            $this->response->body(json_encode([
                'status' => 1,
                'message' => MSG_SUCCESS,
            ]));
            return $this->response;
        }
    }

    public function changeOfStatus()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->viewBuilder()->layout('ajax');
        $this->autoRender = false;
        $this->response->disableCache();
        $this->response->type('json');
        $data = $this->Manufacturers->get($this->request->data['id']);
        $data->is_suspend = $this->request->data['status'];
        if ($this->Manufacturers->save($data)) {
            $this->response->body(json_encode([
                'status' => 1,
                'message' => MSG_SUCCESS,
            ]));

            return $this->response;
        }
    }

    public function delete()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->viewBuilder()->layout('ajax');
        $this->autoRender = false;
        $this->response->type('json');
        $this->response->disableCache();
        $data = $this->Manufacturers->get($this->request->data['id']);
        if ($this->Manufacturers->delete($data)) {
            $this->response->body(json_encode([
                'status' => 1,
                'message' => MSG_SUCCESS,
            ]));

            return $this->response;
        }
    }

    public function getStatus()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->viewBuilder()->layout('ajax');
        $manufacturer = $this->Manufacturers->get($this->request->query('id'));
        $data = [
            'manufacturer' => $manufacturer,
            'status' => $this->request->query('status'),
        ];

        $this->set($data);
    }

    public function getDelete()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->viewBuilder()->layout('ajax');
        $manufacturer = $this->Manufacturers->get($this->request->query('id'));
        $data = [
            'data' => $manufacturer,
        ];

        $this->set($data);
    }

    public function getListOfManufacturersByAjax()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->viewBuilder()->layout('ajax');
        $this->autoRender = false;
        $this->response->type('json');
        $this->response->disableCache();
        $keyword = trim($this->request->query('keyword'));
        $data = $this->Manufacturers->find()
            ->where(['Manufacturers.is_suspend' => 0])
            ->where(['or' => [
                'Manufacturers.name_en LIKE' => '%' . $keyword . '%',
                'Manufacturers.name LIKE' => '%' . $keyword . '%',
            ]])
            ->limit(PAGE_LIMIT_NUMBER)
            ->all();
        $this->response->body(json_encode([
            'status' => 1,
            'message' => MSG_SUCCESS,
            'data' => $data,
        ]));

        return $this->response;
    }

    private function checkSeller($data)
    {
        $this->loadModel('Sellers');
        $seller = $this->Sellers->find()
            ->where([
                'type' => TYPE_MANUFACTURER,
                'external_id' => $data->id,
            ])
            ->first();
        if ($seller) {
            $seller->is_manufacture_seller = isset($data->is_manufacture_seller) ? $data->is_manufacture_seller : 0;
        } else {
            $seller = $this->Sellers->newEntity();
            $seller->external_id = $data->id;
            $seller->type = TYPE_MANUFACTURER;
            $seller->is_manufacture_seller = $data->is_manufacture_seller;
        }
        $this->Sellers->save($seller);
    }
}
