<?php

namespace App\Controller;

use App\Controller\SalesController;
use Cake\Network\Exception\NotFoundException;

class OrderToMpSampleController extends SalesController
{
    public function initialize()
    {
        parent::initialize();
        $this->loadModel('Sales');
        $this->loadComponent('Image');
    }

    public function po($id = null)
    {
        if ($id === null) {
            throw new NotFoundException();
        }
        $this->loadModel('SaleDetails');
        $this->loadModel('Medias');
        $this->loadModel('SaleReceivePayments');
        $this->loadModel('SaleDeliveries');
        $this->loadModel('PurchaseImports');
        $person = [];

        // get sales data
        $sale = $this->Sales->getById($id, null, [
            'Currencies',
            'Customers' => ['Countries'],
            'SaleDeliveries' => ['Deliveries'],
            'Sellers' => [
                'Manufacturers' => ['InfoDetails', 'Countries'],
                'Suppliers' => ['InfoDetails', 'Countries'],
            ],
        ]);

        // count child record where sale_id IS NOT null
        $countRecords = $this->SaleDetails->countRecords([
            'SaleDetails.parent_id IS NOT' => null,
            'SaleDetails.sale_id' => $id,
        ]);

        // get saleDetail with saleStockDetail
        $saleDetail = $this->SaleDetails->getAllDataList('threaded', null, [
            'SaleDetails.sale_id' => $id
        ], [
            'SaleStockDetails' => function ($q) {
                $q->select([
                    'id',
                    'sale_detail_id',
                    'count' => $q->func()->count('*'),
                ])
                ->group('sale_detail_id');

                return $q;
            },
        ]);

        // get payments by sale_id
        $payments = $this->SaleReceivePayments->getSalePaymentListBySaleId($id);

        // get sale_deliveries by sale_id
        $saleDeliveries = $this->SaleDeliveries->getAllDataList('all', null, [
            'SaleDeliveries.sale_id' => $id
        ], ['Deliveries']);

        // get medias by type
        $medias = $this->Medias->getAllDataList('all', null, [
            'Medias.external_id' => $id,
            'Medias.type' => TYPE_ORDER_TO_MP,
        ]);

        // get purchase imports
        $purchaseImports = $this->PurchaseImports->getAllDataList('all', ['id'], [
            'PurchaseImports.sale_id' => $id,
        ]);

        if ($sale) {
            $this->loadModel('PersonInCharges');
            $ext = [];

            switch ($sale->seller->type) {
                case TYPE_SUPPLIER :
                    $ext = [
                        'type' => TYPE_SUPPLIER,
                        'id' => $sale->seller->supplier->id,
                    ];
                    break;
                default :
                    $ext = [
                        'type' => TYPE_MANUFACTURER,
                        'id' => $sale->seller->manufacturer->id
                    ];
            }
            $person = ['' => __('TXT_SELECT_CONTACT')] + $this->PersonInCharges->getList($this->local, $ext['type'], $ext['id']);
        }

        $typePdf = $this->CustomConstant->typePdf();

        // set data to view layout
        $data = [
            'sale' => $sale,
            'person' => $person,
            'saleDetail' => $saleDetail,
            'payments' => $payments,
            'medias' => $medias,
            'typePdf' => $typePdf,
            'saleDeliveries' => $saleDeliveries,
            'countRecords' => $countRecords,
            'purchaseImports' => $purchaseImports,
        ];

        $this->set($data);
    }

    public function rop($id = null) {
        parent::rop($id);
        if ($id === null) {
            throw new NotFoundException();
        }
        $this->loadModel('SaleDetails');
        $this->loadModel('Currencies');
        $person = [];
        $doctors = [];

        // get currencies list
        $currencies = $this->Currencies->getDataList('id', 'code', null, $this->local);

        // get sale data by id
        $sale = $this->Sales->getById($id, null, [
            'Currencies',
            'Customers' => [
                'InfoDetails',
                'Countries',
                'ClinicDoctors',
                'PersonInCharges',
            ],
            'Sellers' => [
                'Manufacturers' => ['InfoDetails', 'Countries'],
                'Suppliers' => ['InfoDetails', 'Countries'],
            ],
            'SaleDetails' => function ($q) {
                $q->select([
                    'id',
                    'sale_id',
                    'count' => $q->func()->count('*')
                ])
                ->group(['sale_id']);

                return $q;
            },
        ]);

        // get sale_details by id
        $saleDetail = $this->SaleDetails->getAllDataList('threaded', null, [
            'SaleDetails.id' => $id,
        ]);

        if ($sale->customer) {
            $this->loadModel('ClinicDoctors');
            $doctors = $this->ClinicDoctors->getById($sale->customer->id);
        }

        if ($sale) {
            $this->loadModel('PersonInCharges');
            $ext = [];

            switch ($sale->seller->type) {
                case TYPE_SUPPLIER :
                    $ext = [
                        'type' => TYPE_SUPPLIER,
                        'id' => $sale->seller->supplier->id,
                    ];
                    break;
                default :
                    $ext = [
                        'type' => TYPE_MANUFACTURER,
                        'id' => $sale->seller->manufacturer->id
                    ];
            }
            $person = ['' => __('TXT_SELECT_CONTACT')] + $this->PersonInCharges->getList($this->local, $ext['type'], $ext['id']);
        }

        // set data to view layout
        $data = [
            'sale' => $sale,
            'person' => $person,
            'saleDetail' => $saleDetail,
            'doctors' => $doctors,
            'curencies' => $currencies,
        ];

        $this->set($data);
    }

    public function updateStatusMp()
    {
        $this->ajaxRequest(false);
        $this->loadModel('SaleReceivePayments');
        $this->loadModel('Sellers');

        $seller_id = $this->Sellers->sellerIdAsMp();
        $sale = $this->Sales->get($this->request->data['id']);
        $sale->status = $this->request->data['status'];

        if ($this->request->data['action'] === 'po') {
            $sale->sale_number = $this->Sales->saleNumber($sale->type, ['seller_id' => $seller_id]);

            if (!$sale->issue_date) {
                $sale->issue_date = $this->request->data['issue_date'];
            }
        }

        if ($this->Sales->save($sale)) {
            $response = [
                'step' => $this->request->data['step'],
                'status' => $sale->status,
                'payments' => $this->SaleReceivePayments->getSalePaymentListBySaleId($sale->id),
            ];

            // deduct stock
            $this->deductStockMp($sale->id, $response, $this->request->data, 3);
        }
    }

    public function statusDownload()
    {
        $this->ajaxRequest(false);
        $this->loadModel('Sales');
        $this->loadModel('Medias');
        $medias = [];
        $response = [];
        $countNewFile = 0;
        $path = WWW_ROOT . 'img' . DS . 'uploads' . DS . 'po';

        $data = $this->request->data;
        $this->set('_serialize', ['data']);

        if (isset($data['files'])) {
            $countNewFile = count($data['files']);

            // check if new files
            if ($data['files']) {
                foreach ($data['files'] as $key => $value) {
                    $temp = explode('.', $value['name']);
                    $newFile = $this->Image->upload(null, $value, $path);
                    $medias[] = [
                        'external_id' => $data['id'],
                        'type' => TYPE_ORDER_TO_MP,
                        'document_type' => $data['types'][$key],
                        'file_name' => $newFile,
                        'file_type' => end($temp),
                    ];
                }
            }

            // check is old files
            if (isset($data['old_files'])) {
                foreach ($data['old_files'] as $key => $value) {
                    $temp = explode('.', $value);
                    $medias[$countNewFile] = [
                        'external_id' => $data['id'],
                        'type' => USER_ROLE_SALE,
                        'document_type' => $data['old_doc_types'][$key],
                        'file_name' => $value,
                        'file_type' => end($temp),
                    ];
                    $countNewFile++;
                }
            }
        }

        if ($medias) {
            $this->Medias->deleteMediaById($data['id'], USER_ROLE_SALE);
            $data1 = $this->Medias->newEntities($medias);
            $this->Medias->saveMany($data1);
        }

        $sale = $this->Sales->get($data['id']);
        $sale->status = $data['status'];

        if ($this->Sales->save($sale)) {
            $response = [
                'step' => $data['step'] + 1,
                'status' => $sale->status,
            ];
        }

        $this->Function->ajaxResponse(1, MSG_SUCCESS, $response);
    }

    private function deductStockMp($saleId, $response, $request, $step)
    {
        $this->loadModel('SaleDetails');

        if ($request['step'] == $step) {
            $saleDetails = $this->SaleDetails->getAllDataList('all', null, ['SaleDetails.sale_id' => $saleId], [
                'SaleStockDetails',
            ]);
            // sale stock detail
            $this->grebSaleStockDetail($saleDetails);
        }

        $this->Function->ajaxResponse(1, MSG_SUCCESS, $response);
    }
}
