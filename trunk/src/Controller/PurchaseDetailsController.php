<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\Network\Exception\NotFoundException;

class PurchaseDetailsController extends AppController
{
    /**
    * @var string for store local session field
    */
    private $local;

    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('Function');
        $this->loadComponent('Pluralize');
        $this->local = $this->request->session()->read('tb_field');
    }

    public function delete()
    {
        $this->ajaxRequest(false);
        $data = $this->PurchaseDetails->get($this->request->data['id']);

        if ($this->PurchaseDetails->delete($data)) {
            $this->Function->ajaxResponse(1, MSG_SUCCESS);
        }
    }

    public function getFormCreate()
    {
        $this->ajaxRequest(true);
        $this->loadModel('SellerBrands');
        $this->loadModel('Sellers');
        $purchase_detail = [];
        $childs = [];
        $products = [];
        $product_id = [];

        $purchase_id = $this->request->query('purchase_id');
        $id = $this->request->query('id');
        $seller_id = $this->request->query('seller_id');

        $parents = $this->PurchaseDetails->getAllDataList('all', null, [
            'PurchaseDetails.purchase_id' => $purchase_id,
            'PurchaseDetails.parent_id IS ' => null,
        ]);
        $parents = ['' => __('TXT_PARENT')] + $this->Pluralize->poDetail($parents);

        if ($this->request->query('target') === TARGET_EDIT) {
            $purchase_detail = $this->PurchaseDetails->getById($id, null, [
                'ProductDetails' => ['SingleUnits', 'PackSizes', 'Products'],
            ]);
            $childs = $this->PurchaseDetails->countRecords(['PurchaseDetails.parent_id' => $id]);
        }
        $seller = $this->Sellers->getById($seller_id, null, ['SellerBrands' => ['SellerProducts']]);

        if ($seller) {
            if ($seller->seller_brands) {
                $i = 0;
                foreach ($seller->seller_brands as $brand) {
                    if ($brand->seller_products) {
                        foreach($brand->seller_products as $p) {
                            $product_id[$p->product_id] = $p->product_id;
                            $i++;
                        }
                    }
                }
            }
        }

        if ($product_id) {
            $this->loadModel('Products');
            $products = $this->Products->getAllDataList('all', null, [
                'Products.is_suspend' => 0,
                'Products.id IN ' => $product_id,
            ], [
                'ProductBrands',
                'ProductDetails' => [
                    'PackSizes',
                    'SingleUnits',
                    'queryBuilder' => function($q) {
                        return $q->where(['ProductDetails.is_suspend' => 0]);
                    },
                ],
            ]);
        }

        $this->set(compact('purchase_detail', 'parents', 'childs', 'products', 'purchase_id'));
    }

    public function create()
    {
        $this->ajaxRequest(false);
        $data = $this->PurchaseDetails->newEntity();
        $data = $this->PurchaseDetails->patchEntity($data, $this->request->data);

        if ($data->errors()) {
            $errors = $this->Function->getError($data->errors());
            $this->Function->ajaxResponse(0, MSG_ERROR, $errors);
        } else {
            if (isset($this->request->data['id'])) {
                $data2 = $this->PurchaseDetails->get($this->request->data['id']);
                $data = $this->PurchaseDetails->patchEntity($data2, $this->request->data, [
                    'validate' => false,
                ]);

                if ($data->parent_id && ($data->parent_id !== $this->request->data['parent_id'])) {
                    $data->unit_price = 0;
                }
                if (!empty($this->request->data['parent_id'])) {
                    $data->is_discount = 0;
                }
            }
            $data->pack_size_name = $this->Pluralize->pluralize_if($this->request->data['pack_size_value'], $this->request->data['pack_size_name']);
            $this->PurchaseDetails->save($data);
            $this->Function->ajaxResponse(1, MSG_SUCCESS);
        }
    }

    /**
    * Function ajaxRequest
    * using for checking the request is ajax
    * @param boolean $auto_render assign value to $this->autoRender
    */
    private function ajaxRequest($auto_render = false)
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->viewBuilder()->layout('ajax');

        if ($auto_render === false) {
            $this->autoRender = false;
        }
    }
}
