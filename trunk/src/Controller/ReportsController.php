<?php

namespace App\Controller;
//require ROOT . DS . 'src/Lib/tfpdf/tfpdf.php';
//require ROOT . DS . 'src/Lib/fpdfjp/japanese.php';
require ROOT . DS . 'src/Lib/tcpdf/tcpdf_config_alt.php';
require ROOT . DS . 'src/Lib/tcpdf/tcpdf.php';

use App\Controller\AppController;
//use tFPDF;
//use PDF_Japanese;
use TCPDF;

class ReportsController extends AppController
{
    public function product()
    {
        if ($this->request->is('get')) {
            if ($this->request->query('name')) {
                $this->loadModel('Sale1s');
                $query = $this->Sale1s->find();
                $product = $query->select([
                    'id',
                    'quantity',
                    'unit_price',
                    'invoice_number',
                    'clinic_name_jp',
                    'product_name',
                    'order_date'
                ])
                ->where(['product_name' => $this->request->query('name'), 'is_deleted = 0'])
                ->order(['clinic_name_jp']); // this sort will b change to clinic_name_en after this field is crated

                $report = $this->Sale1s->setProductValue($product);
                if ($report) {
                    $this->set('min_date', $this->Sale1s->minOrderDate());
                    $this->set('max_date', $this->Sale1s->maxOrderDate());
                    $this->set('data', $this->Sale1s->getProductValue());
                } else {
                    $this->set('min_date', '');
                    $this->set('max_date', '');
                    $this->set('data', '');
                }
            }
        }

        //Auto complete search.
        $this->loadModel('Product1s');
        $query = $this->Product1s->find()
            ->hydrate(false)
            ->join([
                'table' => 'sale1s',
                'alias' => 'Sale1s',
                'type' => 'LEFT',
                'conditions' => 'Sale1s.product_code = Product1s.code',
            ])
            ->select(['Product1s.name', 'Product1s.manufacturer'])
            ->where(['DATE(Sale1s.order_date) <>' => date('Y-m-d H:i:s', strtotime('0000-00-00 00:00:00'))])
            ->order(['Product1s.name']);
        $name = [];
        if (!empty($query)) {
            foreach ($query as $value) {
                $name[] = $value['name'];
                if (!empty($value['manufacturer'])) {
                    $name[] = $value['manufacturer'];
                }
            }
        }

        if (!empty($name)) {
            $this->set('name', json_encode(array_unique($name)));
        } else {
            $this->set('name', json_encode([]));
        }
    }

    public function loadClinicName()
    {
        if ($this->request->is('ajax')) {
            $this->autoRender = false;
            $this->response->disableCache();
            $this->response->type('json');
            $this->viewBuilder()->layout(false);

            $keyword = trim($this->request->query('keyword'));
            $query = $this->Sale1s->find()
                ->select(['clinic_name_jp', 'clinic_name_en'])
                ->where(['is_deleted = 0'])
                ->group(['clinic_name_jp'])
                ->order(['clinic_name_jp']);

            $results = $query->where(['clinic_name_jp LIKE ' => '%' . $keyword . '%'])
                ->orWhere(['clinic_name_en LIKE ' => '%' . $keyword . '%']);
            $this->response->body(json_encode($results->toArray()));
            return $this->response;
        }
    }

    public function productPdf()
    {
        $this->viewBuilder()->layout(false);
        $this->response->type('pdf');

        if ($this->request->is('get')) {
            if ($this->request->query('name')) {
                $this->loadModel('Sale1s');
                $query = $this->Sale1s->find();
                $product = $query->select([
                    'id',
                    'quantity',
                    'unit_price',
                    'invoice_number',
                    'clinic_name_jp',
                    'product_name',
                    'order_date'
                ])
                ->where([
                    'product_name' => trim($this->request->query('name')),
                    'is_deleted' => 0,
                    'DATE(Sale1s.order_date) <>' => date('Y-m-d H:i:s', strtotime('0000-00-00 00:00:00'))
                ])
                ->order(['clinic_name_jp']); // this sort will b change to clinic_name_en after this field is crated

                $this->Sale1s->setProductValue($product);
                $this->set('data', $this->Sale1s->getProductValue());
            }
        }
    }

    // TODO check and remove
    public function Oldclinic()
    {
        $this->loadModel('Sale1s');
        if ($this->request->is('get')) {
            $keyword = $this->request->query('name');
            if (!empty($keyword)) {
                $query = $this->Sale1s->find();
                $clinic_name = trim($keyword);
                $product = $query->select([
                    'id',
                    'quantity',
                    'unit_price',
                    'invoice_number',
                    'clinic_name_jp',
                    'clinic_name_en',
                    'product_name',
                    'order_date'
                ])
                ->where([
                    'clinic_name_jp' => $clinic_name,
                    'DATE(Sale1s.order_date) <>' => date('Y-m-d H:i:s', strtotime('0000-00-00 00:00:00'))
                ])
                ->orWhere(['clinic_name_en' => $clinic_name])
                ->andWhere(['is_deleted' => 0])
                ->order(['product_name']);
                //pr($product->toArray());exit;
                $report = $this->Sale1s->setClinicValue($product);
                if ($report) {
                    $this->set('clinic_name', $this->Sale1s->clinic_name_in_jp);
                    $this->set('data', $this->Sale1s->getClinicValue($product));
                    $this->set('min_date', $this->Sale1s->minOrderDate());
                    $this->set('max_date', $this->Sale1s->maxOrderDate());
                } else {
                    $this->set('clinic_name', $this->Sale1s->clinic_name_in_jp);
                    $this->set('data', '');
                    $this->set('min_date', '');
                    $this->set('max_date', '');
                }
            }
        }

        $this->loadModel('Sale1s');
        $query = $this->Sale1s->find()
            ->select(['clinic_name_jp', 'clinic_name_en'])
            ->where([
                'is_deleted' => 0,
                'DATE(Sale1s.order_date) <>' => date('Y-m-d H:i:s', strtotime('0000-00-00 00:00:00'))
            ])
            ->order(['clinic_name_jp']);

        $name = [];
        if (!empty($query)) {
            foreach ($query as $value) {
                $name[] = $value['clinic_name_jp'];
                if (!empty($value['clinic_name_en'])) {
                    $name[] = $value['clinic_name_en'];
                }
            }
        }

        if (!empty($name)) {
            $this->set('name', json_encode(array_unique($name)));
        } else {
            $this->set('name', json_encode([]));
        }
    }

    public function clinic()
    {
        // LOAD
        $this->loadModel('Customers');
        $this->loadModel('Sales');
        $name = $this->Customers->getAllAvailableJpAndEnName();
        $this->set('name', json_encode($name));
        $data = $this->Sales->getAllAvailableProductByClinicName(trim($this->request->query('name')));
        $this->loadComponent('Report');
        $this->Report->setClinicValue($data);
        $this->set('data', $this->Report->getClinicValue());
        $this->set('min_date', $this->Sales->getOrderDate('min'));
        $this->set('max_date', $this->Sales->getOrderDate('max'));
        // $this->Oldclinic();
    }

    public function clinicPdf()
    {
        $this->viewBuilder()->layout(false);
        $this->response->type('pdf');

        if ($this->request->query('name')) {
            $this->loadModel('Sale1s');
            $query = $this->Sale1s->find();
            $clinic_name = trim($this->request->query('name'));
            $product = $query->select([
                'id',
                'quantity',
                'unit_price',
                'invoice_number',
                'clinic_name_jp',
                'clinic_name_en',
                'product_name',
                'order_date'
            ])
            ->where([
                'clinic_name_jp' => $clinic_name,
                'Sale1s.invoice_number <>' => '0',
                'Sale1s.invoice_number <>' => '',
                'DATE(Sale1s.order_date) <>' => date('Y-m-d H:i:s', strtotime('0000-00-00 00:00:00'))
            ])
            ->orWhere(['clinic_name_en' => $clinic_name])
            ->andWhere(['is_deleted' => 0])
            ->order(['product_name']);

            $this->Sale1s->setClinicValue($product);
            $this->set('data', $this->Sale1s->getClinicValue($product));
            $this->set('clinic_name', $this->Sale1s->clinic_name_in_jp);
        }
    }
}