<?php
namespace App\Controller;
use App\Controller\AppController;
use Cake\Network\Exception\NotFoundException;

class CustomerGroupsController extends AppController
{
    protected $field;

    public function initialize()
    {
        parent::initialize();
        $this->field = 'name' . $this->request->session()->read('tb_field');
    }

    public function getByGroupId($id)
    {
        $this->loadModel('Groups');
        $group = $this->Groups->findById($id)->first();
        $keyword = $this->request->query('keyword');
        $this->paginate = [
            'conditions' => ['group_id' => $id],
            'limit' => $this->request->query('displays') ? $this->request->query('displays') : 10,
            'order' => [$this->field => 'asc'],
            'sortWhitelist' => [
                'Customers.code',
                'MedicalCorporations.code',
                'Customers.name',
                'MedicalCorporations.name',
                'InfoDetails.email',
                'Users.lastname_en',
                'PaymentCategories.name',
                'InvoiceDeliveryMethods.name',
            ],
        ];
        if ($group->type === TYPE_CUSTOMER) {
            $this->paginate['contain'] = [
                'Customers' => [
                    'PaymentCategories', 'InvoiceDeliveryMethods', 'Users',
                    'InfoDetails' => function ($q) {
                        return $q->where(['InfoDetails.type' => TYPE_CUSTOMER]);
                    },
                    'queryBuilder' => function ($q) use ($keyword) {
                        if ($keyword) {
                            $q->where([
                                'OR' => [
                                    'Customers.name LIKE' => '%' . $keyword . '%',
                                    'Customers.name_en LIKE' => '%' . $keyword . '%',
                                    'Customers.code LIKE' => '%' . $keyword . '%',
                                ]
                            ]);
                        }
                        return $q;
                    },
                ],
            ];
        } else {
            $this->paginate['contain']= [
                'MedicalCorporations' => [
                    'PaymentCategories', 'InvoiceDeliveryMethods', 'Users',
                    'InfoDetails' => function ($q) {
                        return $q->where(['InfoDetails.type' => TYPE_MEDICAL_CORP]);
                    },
                    'queryBuilder' => function ($q) use ($keyword) {
                        if ($keyword) {
                            $q->where([
                                'OR' => [
                                    'MedicalCorporations.name LIKE' => '%' . $keyword . '%',
                                    'MedicalCorporations.name_en LIKE' => '%' . $keyword . '%',
                                    'MedicalCorporations.code LIKE' => '%' . $keyword . '%',
                                ]
                            ]);
                        }
                        return $q;
                    },
                ],
            ];
        }
        $data = $this->paginate($this->CustomerGroups);
        $this->set(compact('data', 'group'));
    }

    public function getList()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->viewBuilder()->layout('ajax');
        $this->autoRender = false;
        $this->response->disableCache();
        $this->response->type('json');
        $data = [];
        $type = $this->request->query('type');
        $data2 = $this->getExternalIdByGroupId($this->request->query('group_id'), $type);
        $ids = $data2 ? $data2 : 0;

        switch ($type) {
            case TYPE_CUSTOMER :
                $this->loadModel('Customers');
                $data = $this->Customers->find('all')
                            ->where(['Customers.id NOT IN' => $ids])
                            ->contain(['InfoDetails'])->toArray();
                break;

            case TYPE_SUBSIDIARY :
                $this->loadModel('Subsidiaries');
                $data = $this->Subsidiaries->find('all')
                            ->where(['Subsidiaries.id NOT IN' => $ids])
                            ->contain(['InfoDetails'])->toArray();
                break;

            case TYPE_MEDICAL_CORP :
                $this->loadModel('MedicalCorporations');
                $data = $this->MedicalCorporations->find('all')
                            ->where(['MedicalCorporations.id NOT IN' => $ids])
                            ->contain(['InfoDetails'])->toArray();
                break;
        }

        $this->response->body(json_encode([
            'status' => 1,
            'message' => 'success',
            'data' => $data,
        ]));
        return $this->response;
    }

    public function saveData()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->viewBuilder()->layout('ajax');
        $this->autoRender = false;
        if ($this->request->data) {
            $entities = $this->CustomerGroups->newEntities($this->request->data['data']);
            $this->CustomerGroups->saveMany($entities);
            echo json_encode([
                'status' => 1,
                'message' => 'success',
            ]);
        }
    }

    public function delete()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->viewBuilder()->layout('ajax');
        $this->autoRender = false;
        $data = $this->CustomerGroups->get($this->request->data['id']);
        if ($this->CustomerGroups->delete($data)) {
            echo json_encode([
                'status' => 1,
                'message' => 'success',
            ]);
        }
    }

    public function updateIsMain()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->viewBuilder()->layout('ajax');
        $this->autoRender = false;
        $this->CustomerGroups->updateIsMain();
        $data = $this->CustomerGroups->get($this->request->data['id']);
        $data->is_main = true;
        if ($this->CustomerGroups->save($data)) {
            echo json_encode([
                'status' => 1,
                'message' => 'success',
            ]);
        }
    }

    public function getExternalIdByGroupId($group_id, $type)
    {
        $ids = [];
        $data = $this->CustomerGroups->find('all')
            ->where([
                'CustomerGroups.group_id' => $group_id,
                'CustomerGroups.external_type' => $type,
            ])->toArray();
        if ($data) {
            foreach ($data as $key => $value) {
                $ids[] = $value->external_id;
            }
        }
        return $ids;
    }
}
