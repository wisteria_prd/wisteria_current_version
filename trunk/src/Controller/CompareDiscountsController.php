<?php
namespace App\Controller;
use App\Controller\AppController;

class CompareDiscountsController extends AppController
{
    public $field;

    public function initialize()
    {
        parent::initialize();
        $this->field = 'name' . $this->request->session()->read('tb_field');
    }

    public function index()
    {
        // CHECK KEYWORD FOR SEARCH
        if ($this->request->query('keyword')) {
            $this->loadModel('SellerProducts');
            $this->paginate = [
                'conditions' => [
                    'or' => [
                        'Products.name LIKE' => '%' . $this->request->query('keyword') .'%',
                        'Products.name_en LIKE' => '%' . $this->request->query('keyword') .'%',
                    ],
                ],
                'order' => [
                    'PurchaseConditions' => 'desc',
                ],
                'limit' => $this->request->query('displays') ? $this->request->query('displays') : 10,
                'contain' => [
                    'SellerBrands' => [
                        'Sellers' => [
                            'Manufacturers',
                            'Suppliers',
                        ],
                    ],
                    'SellerStandardPrices',
                    'Products' => [
                        'ProductDetails',
                    ],
                    'Currencies',
                    'PurchaseConditions',
                ],
            ];
            $data = $this->paginate($this->SellerProducts);
            $this->set(compact('data'));
        }
    }
}
