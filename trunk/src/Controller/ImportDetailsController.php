<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Network\Exception\NotFoundException;
use Cake\Routing\Router;

class ImportDetailsController extends AppController
{
    public $locale;

    public function initialize()
    {
        parent::initialize();
        $this->locale = $this->request->session()->read('tb_field');
    }

    public function index()
    {
        if (empty($this->request->query('import_id'))) {
            throw new NotFoundException(__('TXT_ITEM_ID_NOT_FOUND'));
        }
        $conditions ['ImportDetails.import_id'] =  $this->request->query('import_id');
        if (!empty($this->request->query('keyword'))) {
            $keyword = trim($this->request->query('keyword'));
            $conditions['ImportDetails.lot_number LIKE '] = '%' . $keyword . '%';
        }
        $en = $this->request->session()->read('tb_field');
        $display = PAGE_NUMBER;
        if (!empty($this->request->query('displays'))) {
            $display = $this->request->query('displays');
        }
        if ($this->request->query('inactive')) {
            $conditions[] = [
                'ImportDetails.is_suspend' => 1,
            ];
        }
        $this->paginate = [
            'contain' => [
                'Imports',
                'ProductDetails' => [
                    'Products' => [
                        'ProductBrands' => ['Manufacturers'],
                    ],
                    'PackSizes',
                    'SingleUnits',
                    'queryBuilder' => function($q) {
                        return $q->where(['ProductDetails.is_suspend' => 0]);
                    },
                ]
            ],
            'conditions' => $conditions,
            'sortWhitelist' => [
                'Products.name' . $en,
                'ImportDetails.lot_number',
                'ImportDetails.expiry_date',
                'ImportDetails.quantity',
                'ImportDetails.is_foc',
            ],
            'order' => ['ImportDetails.created' => 'DESC'],
            'limit' => $display
        ];
        $data = [
            'locale' => $this->locale,
            'imports' => $this->paginate($this->ImportDetails),
            'paging' => $this->request->param('paging')['ImportDetails']['pageCount'],
        ];
        $this->set($data);
    }

    public function create()
    {
        if (empty($this->request->query('import_id'))) {
            throw new NotFoundException(__('TXT_ITEM_ID_NOT_FOUND'));
        }

        $import = $this->ImportDetails->newEntity();

        if ($this->request->is('ajax')) {
            $this->autoRender = false;
            $this->response->disableCache();
            $this->response->type('json');
            $this->viewBuilder()->layout(false);

            $request = $this->request->data;

            list($error, $msg) = $this->validateDamageLevel($request);

            $import = $this->ImportDetails->patchEntity($import, $request);

            if (empty($import->errors()) && $error == false) {
                $this->ImportDetails->save($import, ['validate' => false]);

                $group = $this->request->session()->read('user_group')['affiliation_class'];
                $this->loadModel('Stocks');
                $this->Stocks->createStock($request, $group, $import->id);

                $url = Router::url(['action' => 'index', '?' => [
                    'import_id' => $request['import_id'],
                    'pch_number' => $request['p_number']
                ]]);
                $this->response->body(json_encode(['status' => 1, 'redirect' => $url]));
                return $this->response;
            }

            $errors = array_merge($import->errors(), ['quantity' => ['qty' => $msg]]);

            $this->response->body(json_encode(['status' => 0, 'message' => $errors]));
            return $this->response;
        }

        $txt_register = $this->Common->txtRegister();
        $issue_types = $this->CustomConstant->importDetailType();

        $this->loadModel('Products');
        $products = $this->Products->productDetailsDropdown();
        $en = $this->request->session()->read('tb_field');

        $purchase_number = $this->importPurchaseNumber($this->request->query('import_id'));

        $this->set(compact('import', 'txt_register', 'issue_types', 'products', 'en', 'purchase_number'));
        $this->set('_serialize', ['import', 'products', 'purchase_number']);
    }

    public function edit($id = null)
    {
        $import_id = $this->request->query('import_id');
        if (empty($import_id)) {
            throw new NotFoundException(__('TXT_ITEM_ID_NOT_FOUND'));
        }

        $import = $this->ImportDetails->get($id, [
            'contain' => [
                'Stocks'
            ]
        ]);

        if ($this->request->is('ajax')) {
            $this->autoRender = false;
            $this->response->disableCache();
            $this->response->type('json');
            $this->viewBuilder()->layout(false);

            $request = $this->request->data;

            list($error, $msg) = $this->validateDamageLevel($request);

            $import = $this->ImportDetails->patchEntity($import, $request);

            if (empty($import->errors()) && $error == false) {
                $this->ImportDetails->save($import, ['validate' => false]);

                $this->loadModel('Stocks');
                $this->Stocks->deleteAll(['import_detail_id' => $id]);
                $group = $this->request->session()->read('user_group')['affiliation_class'];
                $this->Stocks->createStock($request, $group, $id);

                $url = Router::url(['action' => 'index', '?' => [
                    'import_id' => $import_id,
                    'pch_number' => $request['p_number']
                ]]);
                $this->response->body(json_encode(['status' => 1, 'redirect' => $url]));
                return $this->response;
            }

            $errors = array_merge($import->errors(), ['quantity' => ['qty' => $msg]]);

            $this->response->body(json_encode(['status' => 0, 'message' => $errors]));
            return $this->response;
        }

        $txt_edit = $this->Common->txtEdit();
        $issue_types = $this->CustomConstant->importDetailType();

        $this->loadModel('Products');
        $products = $this->Products->productDetailsDropdown();
        $en = $this->request->session()->read('tb_field');

        $purchase_number = $this->importPurchaseNumber($this->request->query('import_id'));

        $this->set(compact('import', 'txt_edit', 'issue_types', 'products', 'en', 'purchase_number'));
        $this->set('_serialize', ['import', 'products', 'purchase_number']);
    }

    public function delete()
    {
        if ($this->request->is('ajax')) {
            $this->autoRender = false;
            $this->response->disableCache();
            $this->response->type('json');
            $this->viewBuilder()->layout(false);

            $this->request->allowMethod(['post', 'delete']);
            $discount = $this->ImportDetails->get($this->request->data['id']);
            if ($this->ImportDetails->delete($discount)) {
                $this->response->body(json_encode(['status' => 1]));
                return $this->response;
            }
            $this->response->body(json_encode(['status' => 0, 'message' => __('TXT_DELETE_TROUBLE')]));
            return $this->response;
        }
    }

    private function validateDamageLevel($request)
    {
        $error = false;
        $msg = '';
        if (empty($request['damage_a_quanity']) &&
            empty($request['damage_b_quanity']) &&
            empty($request['damage_c_quanity']) &&
            empty($request['damage_f_quanity'])) {
            $error = true;
            $msg = __('At least one damage level is enter.');
        } else {
            $damage_number = 0;
            $quantity = $request['quantity'];

            if (!empty($request['damage_a_quanity'])) {
                $damage_number += $request['damage_a_quanity'];
            }

            if (!empty($request['damage_b_quanity'])) {
                $damage_number += $request['damage_b_quanity'];
            }

            if (!empty($request['damage_c_quanity'])) {
                $damage_number += $request['damage_c_quanity'];
            }

            if (!empty($request['damage_f_quanity'])) {
                $damage_number += $request['damage_f_quanity'];
            }

            if ($damage_number != $quantity) {
                $error = true;
                $msg = __('Quantity and number of damage level are not equal.');
            }
        }

        return [$error, $msg];
    }

    //temporary use. The spec is unclear.!
    private function importPurchaseNumber($import_id = null)
    {
        $this->loadModel('PurchaseImports');
        return $this->PurchaseImports->find()
            ->contain([
                'Purchases' => function ($q) {
                    return $q->where(['Purchases.is_suspend' => 0]);
                }
            ])
            ->where([
                'PurchaseImports.import_id' => $import_id
            ]);
    }

    public function updateSuspend()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->autoRender = false;
        $this->response->disableCache();
        $this->response->type('json');
        $this->viewBuilder()->layout(false);
        $data = $this->ImportDetails->get($this->request->data['id']);
        $data->is_suspend = ($this->request->data['is_suspend'] == 0 ) ? 1 : 0;
        if ($this->ImportDetails->save($data)) {
            $this->response->body(json_encode([
                'status' => 1,
                'message' => 'success'
            ]));
            return $this->response;
        }
        $this->response->body(json_encode([
            'status' => 0,
            'message' => 'error'
        ]));
        return $this->response;
    }
}
