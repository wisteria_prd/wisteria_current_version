<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Network\Exception\NotFoundException;
use Cake\Network\Email\Email;

class GreetingCardsController extends AppController
{

    public function index()
    {
        $entities = $this->CustomConstant->greatingSendTo();

        $this->set(compact('entities'));
    }

    public function filterNameByType()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->viewBuilder()->layout('ajax');
        $this->autoRender = false;
        $this->response->type('json');
        $this->response->disableCache();
        $type = $this->request->data['type'];
        $data = [];

        switch ($type) {
            case TYPE_CUSTOMER:
                $data = $this->getCustomerList();
                break;
            case TYPE_DOCTOR:
                $data = $this->getDoctorList();
                break;
            case TYPE_MANUFACTURER:
                $data = $this->getManufacturerList();
                break;
            case TYPE_PERSON_IN_CHARGE:
                $data = $this->getPersonInChargeList();
                break;
            case TYPE_SUPPLIER:
                $data = $this->getSupplierList();
                break;
        }

        $this->response->body(json_encode([
            'status' => 1,
            'message' => 'success',
            'field' => $this->request->session()->read('tb_field'),
            'data' => $data,
        ]));
        return $this->response;
    }

    public function getInfoList()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->viewBuilder()->layout('ajax');
        $this->loadModel('InfoDetails');
        $data = $this->getDataByTypeAndId($this->request->query('id'), $this->request->query('type'));

        $this->set(compact('data'));
    }

    public function getCustomerList()
    {
        $this->loadModel('Customers');
        $data = $this->Customers->find('all')
            ->select(['id', 'name', 'name_en'])
            ->toArray();

        return $data;
    }

    public function getDoctorList()
    {
        $this->loadModel('Doctors');
        $data = [];
        $data1 = $this->Doctors->find('all')
            ->select([
                'id',
                'last_name',
                'first_name',
                'last_name_en',
                'first_name_en',
            ])->toArray();
        if ($data1) {
            $data = $this->concatName($data1);
        }

        return $data;
    }

    public function getManufacturerList()
    {
        $this->loadModel('Manufacturers');
        $data = $this->Manufacturers->find('all')
            ->select(['id', 'name_en', 'name'])->toArray();

        return $data;
    }

    public function getPersonInChargeList()
    {
        $this->loadModel('PersonInCharges');
        $data = [];
        $data1 = $this->PersonInCharges->find('all')
            ->select([
                'id',
                'last_name',
                'first_name',
                'last_name_en',
                'first_name_en',
            ])->toArray();
        if ($data1) {
            $data = $this->concatName($data1);
        }

        return $data;
    }

    public function getSupplierList()
    {
        $this->loadModel('Suppliers');
        $data = $this->Suppliers->find('all')
            ->select(['id', 'name','name_en'])->toArray();

        return $data;
    }

    public function concatName($list)
    {
        $data = [];
        if ($list) {
            foreach ($list as $key => $value) {
                $data[] = [
                    'id' => $value->id,
                    'name' => $value->last_name . ' ' . $value->first_name,
                    'name_en' => $value->last_name_en . ' ' . $value->first_name_en,
                ];
            }
        }

        return $data;
    }

    public function getDataByTypeAndId($id, $type)
    {
        $data = [];
        switch ($type) {
            case TYPE_MANUFACTURER :
                $this->loadModel('Manufacturers');
                $data = $this->Manufacturers->findById($id)
                    ->select(['id', 'fax'])
                    ->contain([
                        'InfoDetails' => function($q) use ($id) {
                            return $q->where([
                                'InfoDetails.type' => TYPE_MANUFACTURER,
                                'InfoDetails.external_id' => $id,
                                'InfoDetails.email <>' => '',
                            ]);
                        },
                    ])->first();
                break;
            case TYPE_SUPPLIER :
                $this->loadModel('Suppliers');
                $data = $this->Suppliers->findById($id)
                    ->select(['id', 'fax'])
                    ->contain([
                        'InfoDetails' => function($q) use ($id) {
                            return $q->where([
                                'InfoDetails.type' => TYPE_SUPPLIER,
                                'InfoDetails.external_id' => $id,
                                'InfoDetails.email <>' => '',
                            ]);
                        },
                    ])->first();
                break;
            case TYPE_CUSTOMER :
                $this->loadModel('Customers');
                $data = $this->Customers->findById($id)
                    ->select(['id', 'fax'])
                    ->contain([
                        'InfoDetails' => function($q) use ($id) {
                            return $q->where([
                                'InfoDetails.type' => TYPE_CUSTOMER,
                                'InfoDetails.external_id' => $id,
                                'InfoDetails.email <>' => '',
                            ]);
                        },
                    ])->first();
                break;
            case TYPE_PERSON_IN_CHARGE :
                $this->loadModel('PersonInCharges');
                $data = $this->PersonInCharges->findById($id)
                    ->select(['id'])
                    ->contain([
                        'InfoDetails' => function($q) {
                            return $q->where([
                                'InfoDetails.type' => TYPE_PERSON_IN_CHARGE,
                                'InfoDetails.email <>' => '',
                            ]);
                        },
                    ])->first();
                break;
            default :
                $this->loadModel('Doctors');
                $data = $this->Doctors->findById($id)
                    ->contain([
                        'InfoDetails' => function($q) use ($id) {
                            return $q->where([
                                'InfoDetails.type' => TYPE_DOCTOR,
                                'InfoDetails.external_id' => $id,
                                'InfoDetails.email <>' => '',
                            ]);
                        },
                    ])->first();
                break;
        }

        return $data;
    }

    public function sendEmail()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->viewBuilder()->layout('ajax');
        $this->autoRender = false;
        $list = [];
        $emails = $this->request->data['emails'];
        if ($emails) {
            foreach ($emails as $key => $value) {
                $list[] = $value['communicate'];
            }
        }
        if ($list) {
            $this->sendMessage($list, 'Greeting Card', 'Greeting Card', $this->request->data);
            echo json_encode([
                'status' => 1,
                'message' => 'success',
            ]);
        }
    }

    private function sendMessage($to, $type, $subject, $message)
    {
        $email = new Email('default');
        $email->from([SYSEM_EMAIL => $type])
            ->to($to)
            ->template('greeting_card')
            ->viewVars($message)
            ->emailFormat('html')
            ->subject($subject);
        $email->send();
    }
}