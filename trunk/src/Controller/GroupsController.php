<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\Network\Exception\NotFoundException;

class GroupsController extends AppController
{
    public function index()
    {
        $options = [];
        if ($this->request->query('keyword')) {
            $options[] = [
                'name LIKE' => '%' . $this->request->query('keyword') . '%',
            ];
        }
        $this->paginate = [
            'conditions' => $options,
            'limit' => $this->request->query('displays') ? $this->request->query('displays') : 10,
            'order' => ['name' => 'asc'],
            'contain' => ['CustomerGroups'],
        ];
        try {
            $data = $this->paginate($this->Groups);
            $paging = $this->request->param('paging')['Groups']['pageCount'];
        } catch (NotFoundException $e) {
            $paging = $this->request->param('paging')['Groups']['pageCount'];
            $data = [];
        }

        $groups = $this->CustomConstant->groupType();

        $this->set(compact('data', 'groups', 'paging'));
    }

    public function create()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->viewBuilder()->layout('ajax');
        $this->autoRender = false;
        $response = [];
        $data = $this->Groups->newEntity();

        if ($this->request->data) {
            $data = $this->Groups->patchEntity($data, $this->request->data);
            if ($data->errors()) {
                $response = [
                    'status' => 0,
                    'message' => 'error',
                    'data' => $data->errors(),
                ];
            } else {
                $result = $this->Groups->save($data);
                $response = [
                    'status' => 1,
                    'message' => 'success',
                    'data' => ['id' => $result->id],
                ];
            }

            echo json_encode($response);
        }
    }

    public function edit($id)
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->viewBuilder()->layout('ajax');
        $this->autoRender = false;
        $response = [];
        $data = $this->Groups->get($id);
        $data = $this->Groups->patchEntity($data, $this->request->data);
        if ($data->errors()) {
            $response = [
                'status' => 0,
                'message' => 'error',
                'data' => $data->errors(),
            ];
        } else {
            $this->Groups->save($data);
            $response = [
                'status' => 1,
                'message' => 'success',
                'data' => ['id' => $id],
            ];
        }

        echo json_encode($response);
    }

    public function getById()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->viewBuilder()->layout('ajax');
        $this->autoRender = false;
        $data = $this->Groups->get($this->request->query('id'));

        echo json_encode([
            'status' => 1,
            'message' => 'success',
            'data' => $data,
        ]);
    }

    public function delete()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->viewBuilder()->layout('ajax');
        $this->autoRender = false;

        $data = $this->Groups->get($this->request->data['id']);
        if ($this->Groups->delete($data)) {
            echo json_encode([
                'status' => 1,
                'message' => 'success',
            ]);
        }
    }

    public function updateSuspend()
    {
        if ($this->request->is('ajax')) {
            $this->autoRender = false;
            $this->response->disableCache();
            $this->response->type('json');
            $this->viewBuilder()->layout(false);

            $data = $this->Groups->get($this->request->data['id']);
            $data->is_suspend = ($this->request->data['is_suspend'] == 0 ) ? 1 : 0;
            if ($this->Groups->save($data)) {
                $this->response->body(json_encode([
                    'status' => 1,
                    'message' => 'success'
                ]));
                return $this->response;
            }

            $this->response->body(json_encode([
                'status' => 0,
                'message' => 'error'
            ]));
            return $this->response;
        }
    }

    /**
    * get all group name by search auto complete
    * @return object
    */
    public function getGroupNameBySearch()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->viewBuilder()->layout('ajax');
        $this->autoRender = false;
        $this->response->type('json');
        $keyword = $this->request->query('keyword');

        $groups = $this->Groups->find()
            ->where([
                'AND' => [
                    'is_suspend' => 0,
                    'OR' => ['name LIKE' => '%' . $keyword . '%']
                ]
            ])
            ->limit(20)
            ->all();

        $this->response->body(json_encode([
            'status' => '1',
            'message' => MSG_SUCCESS,
            'data' => $groups,
        ]));
        return $this->response;
    }

}
