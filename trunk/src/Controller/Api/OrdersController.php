<?php
namespace App\Controller\Api;

use App\Controller\AppController;
use Cake\Network\Exception\NotFoundException;

class OrdersController extends AppController
{
    public function index()
    {
        $this->loadModel('Sales');
        $this->loadModel('Purchases');
        $this->loadModel('Deliveries');
        $this->response->type('json');
        $this->paginate = [
            'limit' => 10,
            'contain' => [
                'Currencies',
            ],
        ];
        $data = $this->paginate($this->Sales);
        $this->response->body(json_encode([
            'data' => $data,
            'pagination' => $this->request->param('paging')['Sales'],
        ]));
        return $this->response;
    }
}
