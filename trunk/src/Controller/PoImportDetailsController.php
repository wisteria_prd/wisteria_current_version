<?php
namespace App\Controller;

use App\Controller\AppController;

class PoImportDetailsController extends AppController
{
    public function initialize()
    {
        parent::initialize();
    }

    public function index()
    {
        $this->paginate = [];
        $data = $this->paginate($this->PoImportDetails);
        $this->set(compact('data'));
        $this->set('_serialize', ['data']);
    }

    public function create()
    {
        $po_details = $this->PoImportDetails->newEntity();
        if ($this->request->is('ajax')) {
            $this->autoRender = false;
            $this->response->disableCache();
            $this->response->type('json');
            $this->viewBuilder()->layout(false);

            $po_details = $this->PoImportDetails->patchEntity($po_details, $this->request->data);
            if ($this->PoImportDetails->save($po_details)) {
                $this->response->body(json_encode(['status' => 1]));
                return $this->response;
            }

            $this->response->body(json_encode(['status' => 0, 'message' => $po_details->errors()]));
            return $this->response;
        }

        $this->set(compact('po_details'));
        $this->set('_serialize', ['po_details']);
    }

    public function edit($id = null)
    {
        $po_details = $this->PoImportDetails->get($id);
        if ($this->request->is('ajax')) {
            $this->autoRender = false;
            $this->response->disableCache();
            $this->response->type('json');
            $this->viewBuilder()->layout(false);
            
            $po_details = $this->PoImportDetails->patchEntity($po_details, $this->request->data);
            if ($this->PoImportDetails->save($po_details)) {
                $this->response->body(json_encode(['status' => 1]));
                return $this->response;
            }

            $this->response->body(json_encode(['status' => 0, 'message' => $po_details->errors()]));
            return $this->response;
        }

        $this->set(compact('po_details'));
        $this->set('_serialize', ['po_details']);
    }
}