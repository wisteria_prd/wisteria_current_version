<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\Utility\Text;
use Cake\Network\Email\Email;
use Cake\Network\Exception\NotFoundException;

require ROOT . DS . 'src/Lib/tcpdf/tcpdf_config_alt.php';
require ROOT . DS . 'src/Lib/tcpdf/tcpdf.php';
use TCPDF;

class PurchasesController extends AppController
{
    /**
    * @var string for store local session field
    */
    protected $local;

    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('CustomConstant');
        $this->loadComponent('ReconstructData');
        $this->loadComponent('Function');
        $this->loadComponent('Common');
        $this->loadComponent('Image');
        $this->local = $this->request->session()->read('tb_field');
    }

    public function index()
    {
        $options = [];

        if ($this->request->query('keyword')) {
            $keyword = $this->request->query('keyword');
            $options = ['Purchases.number LIKE' => '%' . $keyword . '%'];
        }
        $this->paginate = [
            'conditions' => $options,
            'limit' => $this->request->query('displays') ? $this->request->query('displays') : 10,
            'contain' => [
                'Users',
                'Currencies',
                'Sellers' => ['Suppliers', 'Manufacturers'],
                'PurchaseDetails' => function ($q) {
                    $q->select([
                        'id',
                        'purchase_id',
                        'total_amount' => $q->func()->sum('unit_price * quantity'),
                    ])
                    ->group('purchase_id');

                    return $q;
                },
            ],
            'sortWhitelist' => [
                'issue_date',
                'status',
                'Users.lastname' . $this->local,
                'Seller.id',
            ],
            'order' => [],
        ];
        $data = $this->paginate($this->Purchases);
        $affiliation_class = $this->CustomConstant->affiliationClass();
        $sellers = $this->getSeller();
        $currencies = $this->getCurrency();
        $type = $this->CustomConstant->puchaseOrderTypes();

        $this->set([
            'data' => $data,
            'affiliation_class' => $affiliation_class,
            'sellers' => $sellers,
            'currencies' => $currencies,
            'type' => $type,
        ]);
    }

    public function create($id = null)
    {
        $this->ajaxRequest(false);
        $data = $this->Purchases->newEntity();
        $data = $this->Purchases->patchEntity($data, $this->request->data);

        if ($data->errors()) {
            $errors = $this->Function->getError($data->errors());
            $this->Function->ajaxResponse(0, MSG_ERROR, $errors);
        } else {
            $data1 = $this->Purchases->newEntity();
            $data1 = $this->Purchases->patchEntity($data1, $this->request->data, ['validate' => false]);

            if ($id === null) {
                $data1->user_id = $this->Auth->user('id');
                $data1->status = PO_STATUS_DRAFF;
                $data1->type = TYPE_STOCK;
            } else {
                $data1->id = $id;
            }
            $purchase = $this->Purchases->save($data1);
            if ($purchase) {
                $this->Function->ajaxResponse(1, MSG_SUCCESS, $purchase);
            }
        }

        $txt_register = $this->Common->txtRegister();
        $this->set(compact('txt_register'));
    }

    public function updateSuspend()
    {
        $this->ajaxRequest(false);
        $data = $this->Purchases->get($this->request->data['id']);
        $data->is_suspend = ($this->request->data['is_suspend'] == 0 ) ? 1 : 0;

        if ($this->Purchases->save($data)) {
            $this->Function->ajaxResponse(1, MSG_SUCCESS);
        }
    }

    public function delete()
    {
        $this->ajaxRequest(false);
        $data = $this->Purchases->get($this->request->data['id']);

        if ($this->Purchases->delete($data)) {
            $this->Function->ajaxResponse(1, MSG_SUCCESS);
        }
    }

    public function edit($id)
    {
        $this->ajaxRequest(false);
        $data = $this->Purchases->get($id);
        $this->Function->ajaxResponse(1, MSG_SUCCESS, $data);

        $txt_edit = $this->Common->txtEdit();
        $this->set(['txt_edit' => $txt_edit]);
    }

    public function poWToMp()
    {
        
    }

    public function poDetail($id = null)
    {
        if (!$id) {
            throw new NotFoundException();
        }
        $this->loadModel('PurchaseOrders');
        $person = [];
        $contact = [];

        $data = $this->Purchases->getById($id, null, [
            'Sellers' => ['Manufacturers', 'Suppliers']
        ]);

        if ($data) {
            $this->loadModel('PersonInCharges');
            $ext_id = [];

            switch ($data->seller->type) {
                case TYPE_SUPPLIER :
                    $ext_id = [
                        'type' => TYPE_SUPPLIER,
                        'id' => $data->seller->supplier->id,
                    ];
                    break;
                default :
                    $ext_id = [
                        'type' => TYPE_MANUFACTURER,
                        'id' => $data->seller->manufacturer->id
                    ];
            }
            $person = $this->PersonInCharges->getList($this->local, $ext_id['type'], $ext_id['id'], 'id');
        }

        $this->set([
            'data' => $data,
            'person' => $person,
        ]);
    }

    public function draft()
    {
        
    }

    public function poDetailsPdf()
    {
        $this->viewBuilder()->layout(false);
        $this->response->type('pdf');
    }

    public function customClearence($type)
    {
        $this->viewBuilder()->layout(false);
        $this->response->type('pdf');

        switch ($type) {
            case 1:
                $this->render('custom_clearence_type_1');
                break;
            case 2:
                $this->render('custom_clearence_type_2');
                break;
            case 3:
                $this->render('custom_clearence_type_3');
                break;
            case 4:
                $this->render('custom_clearence_type_4');
                break;
            case 5:
                $this->render('custom_clearence_type_5');
                break;
        }
    }

    public function sendApprove()
    {
        if ($this->request->is('ajax')) {
            $this->autoRender = false;
            $this->response->disableCache();
            $this->response->type('json');
            $this->viewBuilder()->layout(false);

            if ($this->request->data['action'] == 'approve') {
                // sending email confirmation
                $variable = [
                    'user_email' => 'daroath.ouk.bitex@gmail.com',
                    'token' => Text::uuid(),
                    'service' => __('PO approving confirmation')
                ];
                $this->sendMail('daroath.ouk.bitex@gmail.com', 'approve_request', 'html', __('PO approving Confirmation'), $variable);
                $this->response->body(json_encode(['status' => 1]));
                return $this->response;
            }
        }
    }

    public function approving()
    {
        
    }

    public function sendPaying()
    {
        if ($this->request->is('ajax')) {
            $this->autoRender = false;
            $this->response->disableCache();
            $this->response->type('json');
            $this->viewBuilder()->layout(false);

            if ($this->request->data['action'] == 'paying') {
                // sending email confirmation
                $variable = [
                    'user_email' => 'daroath.ouk.bitex@gmail.com',
                    'token' => Text::uuid(),
                    'service' => __('PO paying confirmation')
                ];
                $this->sendMail('daroath.ouk.bitex@gmail.com', 'transfer_request', 'html', __('PO paying Confirmation'), $variable);
                $this->response->body(json_encode(['status' => 1]));
                return $this->response;
            }
        }
    }

    public function paying()
    {
        $this->loadModel('Currencies');
        $currencies = $this->Currencies->getDropdown();

        $this->set(compact('currencies'));
        $this->set('_serialize', ['currencies']);
    }

    public function paid()
    {

    }

    public function clearance()
    {

    }

    public function inspecting()
    {

    }

    /**
    * get all purchase_name by search auto complete
    * @return object
    */
    public function getPurchaseNumberBySearch()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->viewBuilder()->layout('ajax');
        $this->autoRender = false;
        $this->response->type('json');
        $keyword = $this->request->query('keyword');

        $conditions['AND'] = [
            'Purchases.is_suspend' => 0,
            'OR' => ['Purchases.purchase_number LIKE ' => '%' . $keyword . '%']
        ];
        $purchases = $this->Purchases->getAllDataList('all', null, $conditions, null, 20);

        $this->response->body(json_encode([
            'status' => '1',
            'message' => MSG_SUCCESS,
            'data' => $purchases,
        ]));
        return $this->response;
    }

    /**
    * Function sendMail
    * using for sending email
    * @param string $to assign email for send to
    * @param string $template assign template name for send email
    * @param string $format assign email format
    * @param string $subject assign email subject
    * @param object $variable assign object to template for display data
    * @param string $type assign type to send from 
    */
    private function sendMail($to, $template, $format, $subject, $variable, $type = 'Purchase Order')
    {
        $email = new Email('default');
        $email->from([SYSEM_EMAIL => $type])
            ->to($to)
            ->template($template)
            ->viewVars($variable)
            ->emailFormat($format)
            ->subject($subject);
        $email->send();
    }

    /**
    * Function ajaxRequest
    * using for checking the request is ajax
    * @param boolean $auto_render assign value to $this->autoRender
    */
    public function ajaxRequest($auto_render = false)
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->viewBuilder()->layout('ajax');

        if ($auto_render === false) {
            $this->autoRender = false;
        }
    }

    /**
     * Function getSeller
     * using for get seller list
     * @return object retrun array list
     */
    private function getSeller()
    {
        $this->loadModel('Sellers');
        $field_name = 'name' . $this->local;
        $data = $this->Sellers->getSellerList($field_name);

        return $data;
    }

    /**
     * Function getCurrency
     * using for get currency list
     * @return object retrun array list
     */
    private function getCurrency()
    {
        $this->loadModel('Currencies');
        $data = $this->Currencies->getDropdown('list');

        return $data;
    }
}
