<?php
namespace App\Controller;
use App\Controller\AppController;
use Cake\Network\Exception\NotFoundException;

class CurrenciesController extends AppController
{
    public function getCurrencyList()
    {
        // CHECK REQUEST IS AJAX
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->viewBuilder()->layout('ajax');
        $currencies = $this->Currencies->find()
            ->order(['Currencies.code' => 'asc']);
        $this->set('currencies', $currencies);
    }

    public function create()
    {
        $currency = $this->Currencies->newEntity();
        // CHECK REQUEST IS AJAX
        if ($this->request->is('ajax')) {
            $this->autoRender = false;
            $this->response->disableCache();
            $this->response->type('json');
            $this->viewBuilder()->layout(false);
            $currency = $this->Currencies->patchEntity($currency, $this->request->data);
            // SAVE DATA
            if ($this->Currencies->save($currency)) {
                $this->response->body(json_encode([
                    'status' => 1,
                    'message' => MSG_SUCCESS,
                ]));
                return $this->response;
            }
            $this->response->body(json_encode([
                'status' => 0,
                'message' => $currency->errors(),
            ]));
            return $this->response;
        }

        $this->set(compact('currencies'));
    }

    public function edit()
    {
        // CHECK REQUEST IS AJAX
        if ($this->request->is('ajax')) {
            $this->autoRender = false;
            $this->response->disableCache();
            $this->response->type('json');
            $this->viewBuilder()->layout('ajax');
            $currency = $this->Currencies->get($this->request->data['id']);
            $currency = $this->Currencies->patchEntity($currency, $this->request->data);
            // SAVE DATA
            if ($this->Currencies->save($currency)) {
                $this->response->body(json_encode([
                    'status' => 1,
                    'message' => MSG_SUCCESS,
                ]));
                return $this->response;
            }
            $this->response->body(json_encode([
                'status' => 0,
                'message' => $currency->errors(),
            ]));
            return $this->response;
        }
        $this->set(compact('currencies'));
    }

    public function delete()
    {
        // CHECK REQUEST IS AJAX
        if ($this->request->is('ajax')) {
            $this->autoRender = false;
            $this->response->disableCache();
            $this->viewBuilder()->layout('ajax');
            $this->response->type('json');
            $this->request->allowMethod(['post', 'delete']);
            $data = $this->Currencies->get($this->request->data['id']);
            // DELETE DATA
            if ($this->Currencies->delete($data)) {
                $this->response->body(json_encode([
                    'status' => 1,
                    'message' => MSG_SUCCESS,
                ]));
                return $this->response;
            }
            $this->response->body(json_encode([
                'status' => 0,
                'message' => MSG_ERROR,
            ]));
            return $this->response;
        }
    }
}
