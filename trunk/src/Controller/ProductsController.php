<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Datasource\ConnectionManager;
use Cake\Network\Exception\NotFoundException;

class ProductsController extends AppController
{
    public $field;

    public function initialize()
    {
        parent::initialize();
        $this->field = $this->request->session()->read('tb_field');
        $this->loadModel('Medias');
        $this->loadModel('Image');
    }

    public function index()
    {
        $options = [];
        $manufacturer = [];
        $productBrand = [];
        $params = new \stdClass();
        if ($this->request->query('filter_manufacturer')) {
            $this->loadModel('Manufacturers');
            $params->manufacturer_id = $this->request->query('filter_manufacturer');
            $manufacturer = $this->Manufacturers->findById($params->manufacturer_id)->first();
        }
        if ($this->request->query('filter_brand')) {
            $this->loadModel('ProductBrands');
            $params->brand_id = $this->request->query('filter_brand');
            $productBrand = $this->ProductBrands->findById($params->brand_id)->first();
        }
        if ($this->request->query('keyword')) {
            $keyword = trim($this->request->query('keyword'));
            $options[] = [
                'OR' => [
                    'Products.name_en LIKE' => '%' . $keyword .'%',
                    'Products.name LIKE' => '%' . $keyword .'%',
                ]
            ];
        }
        if ($this->request->query('inactive')) {
            $options['Products.is_suspend'] = 1;
        }
        $displays = PAGE_NUMBER;
        if ($this->request->query('displays')) {
            $displays = $this->request->query('displays');
        }
        $this->paginate = [
            'conditions' => $options,
            'limit' => $displays,
            'order' => ['Products.' . $this->field => 'asc'],
            'contain' => [
                'ProductDetails' => function ($q) {
                    return $q->group('product_id');
                },
                'ProductBrands' => [
                    'Manufacturers' => function($q) use ($params) {
                        if (isset($params->manufacturer_id)) {
                            $q->where(['Manufacturers.id' => $params->manufacturer_id]);
                        }
                        return $q;
                    },
                    'queryBuilder' => function($q) use ($params) {
                        if (isset($params->brand_id)) {
                            $q->where(['ProductBrands.id' => $params->brand_id]);
                        }
                        return $q;
                    },
                ],
            ],
            'sortWhitelist' => [
                'Proudcts.code',
                'Products.name_en',
                'Products.name',
                'Manufacturers.name',
                'ProductBrands.name',
                'ProductBrands.name_en',
                'Manufacturers.name_en',
                'Products.purchase_flag',
                'Products.sell_flag',
            ],
        ];
        $data1 = [
            'locale' => $this->field,
            'data' => $this->paginate($this->Products),
            'paging' => $this->request->param('paging')['Products']['pageCount'],
            'affiliations' => $this->CustomConstant->affiliationClass(),
            'displays' => $displays,
            'manufacturer' => $manufacturer,
            'productBrand' => $productBrand,
        ];
        $this->set($data1);
    }

    public function create()
    {
        $data = $this->Products->newEntity();
        if ($this->request->is('ajax')) {
            $this->autoRender = false;
            $this->response->disableCache();
            $this->response->type('json');
            $this->viewBuilder()->layout(false);

            $request = $this->request->data;
            $data = $this->Products->patchEntity($data, $request);
            if ($this->Products->save($data)) {
                $this->Medias->saveMedia($data->files, $data->id, TYPE_PRODUCT, null, null);
                $this->response->body(json_encode(['status' => 1]));
                return $this->response;
            }
            $this->response->body(json_encode(['status' => 0, 'message' => $data->errors()]));
            return $this->response;
        }
        $this->loadModel('Manufacturers');
        $manufacturer_list = $this->Manufacturers->find()
            ->where(['Manufacturers.is_suspend' => 0]);
        $txt_register = $this->Common->txtRegister();

        $en = $this->field;
        $types = $this->CustomConstant->productType();
        $this->set(compact('data', 'manufacturer_list', 'txt_register', 'types', 'en'));
        $this->set('_serialize', ['data', 'manufacturer_list']);
    }

    public function edit($id = null)
    {
        $this->loadModel('ProductBrands');
        $data = $this->Products->get($id);
        if ($this->request->is('ajax')) {
            $this->autoRender = false;
            $this->response->disableCache();
            $this->response->type('json');
            $this->viewBuilder()->layout(false);

            $request = $this->request->data;
            $data = $this->Products->patchEntity($data, $request);
            if ($this->Products->save($data)) {
                $this->Medias->deleteMediaById($id, TYPE_PRODUCT);
                $this->Medias->saveMedia($data->files, $id, TYPE_PRODUCT, null, null);
                $this->response->body(json_encode(['status' => 1]));
                return $this->response;
            }
            $this->response->body(json_encode(['status' => 0, 'message' => $data->errors()]));
            return $this->response;
        }
        $this->loadModel('Manufacturers');
        $manufacturer_list = $this->Manufacturers->find()
            ->where(['Manufacturers.is_suspend' => 0]);

        $medias = $this->Medias->getMediaList($id, TYPE_PRODUCT);
        $txt_edit = $this->Common->txtEdit();

        $en = $this->field;
        $types = $this->CustomConstant->productType();
        $this->set(compact('data', 'manufacturer_list', TYPE_PRODUCT_BRAND, 'medias', 'txt_edit', 'types', 'en'));
        $this->set('_serialize', ['data', 'manufacturer_list']);
    }

    public function view($id = null)
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->viewBuilder()->layout('ajax');
        $data = $this->Products->find('all')
            ->where(['Products.id' => $id])
            ->contain([
                'ProductDetails' => function ($q) {
                    return $q->group(['product_id']);
                },
                'ProductBrands',
                'ProductBrands' => ['Manufacturers'],
                'Medias' => function($q) {
                    return $q->select(['external_id', 'file_name'])
                        ->where(['Medias.type' => TYPE_PRODUCT])->order(['file_order']);
                },
                'ProductBrands.Medias' => function ($q) {
                    return $q->select(['external_id', 'file_name'])
                        ->where(['Medias.type' => TYPE_PRODUCT_BRAND])->order(['file_order']);
                }
            ])->first();

        $this->set(compact('data'));
    }

    public function delete()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->viewBuilder()->layout('ajax');
        $this->autoRender = false;
        $data = $this->Products->get($this->request->data['id']);
        if ($this->Products->delete($data)) {
            echo json_encode([
                'status' => 1,
                'message' => 'success',
            ]);
        }
    }

    public function updateSuspend()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->viewBuilder()->layout('ajax');
        $this->autoRender = false;

        $data = $this->Products->get($this->request->data['id']);
        $data->is_suspend = ($this->request->data['is_suspend'] == 0 ) ? 1 : 0;
        if ($this->Products->save($data)) {
            echo json_encode([
                'status' => 1,
                'message' => 'success',
            ]);
        }
    }

    public function loadName()
    {
        if ($this->request->is('ajax')) {
            $this->autoRender = false;
            $this->response->disableCache();
            $this->response->type('json');
            $this->viewBuilder()->layout(false);
            $keyword = trim($this->request->query('keyword'));
            $query = $this->Products->find()
                ->hydrate(false)
                ->join([
                    'Sales' => [
                        'table' => 'sales',
                        'type' => 'INNER',
                        'conditions' => 'Sales.product_code = Products.code',
                    ]
                ])
                ->select(['Products.name', 'Products.manufacturer'])
                ->group(['Products.name'])
                ->order(['Products.name']);

            $results = $query->where(['Products.name LIKE ' => '%' . $keyword . '%'])
                ->orWhere(['Products.manufacturer LIKE ' => '%' . $keyword . '%'])
                ->orWhere(['Sales.product_name LIKE ' => '%' . $keyword . '%']);

            $this->response->body(json_encode($results->toArray()));
            return $this->response;
        }
    }

    public function upload()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->autoRander = false;
        if (isset($this->request->data['file'])) {
            $this->loadComponent('ExtractCsv');
            $data = $this->ExtractCsv->executeExtract($this->request->data['file']);
            if($data) {
                $conn = ConnectionManager::get('default');
                $conn->execute('TRUNCATE TABLE products');
                $entities = $this->Products->newEntities($data);
                $result = $this->Products->saveMany($entities);
                if ($result) {
                    // save log
                    $this->loadModel('Logs');
                    $log_entity = $this->Logs->newEntity(['type' => ucfirst(TYPE_PRODUCT)]);
                    $this->Logs->save($log_entity);

                    echo json_encode(['success' => 1, 'message' => __('TXT_MESSAGE_CREATE')]);
                    exit();
                }
            }
        }
        echo json_encode(['success' => 0, 'message' => __('TXT_MESSAGE_ERROR')]);
    }

    public function test()
    {
        $this->autoRender = false;
        // $this->Products->deleteAll();
        $conn = ConnectionManager::get('default');
        $conn->execute('TRUNCATE TABLE products');
    }

    public function getManufacturerList()
    {
        $this->loadModel('Manufacturers');
        $data = $this->Manufacturers->find('list', [
            'keyField' => 'id',
            'valueField' => 'name' . $this->request->session()->read('tb_field'),
        ])->toArray();

        return $data;
    }

    public function getProductBrandsList()
    {
        $this->loadModel('ProductBrands');
        $data = $this->ProductBrands->find('list', [
            'keyField' => 'id',
            'valueField' => 'name' . $this->request->session()->read('tb_field'),
        ])->toArray();

        return $data;
    }

    public function getProductBrand()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->viewBuilder()->layout('ajax');
        $this->autoRender = false;
        $this->loadModel('ProductBrands');

        $data = $this->ProductBrands->find('all')
            ->select(['id', 'name', 'name_en', 'manufacturer_id'])
            ->where([
            'ProductBrands.manufacturer_id' => $this->request->query('id'),
        ])->toArray();

        echo json_encode([
            'status' => 1,
            'message' => 'success',
            'data' => $data,
        ]);
    }

    public function filterByBrandId()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->viewBuilder()->layout('ajax');
        $this->autoRender = false;
        $this->loadModel('ProductBrands');
        $data = '';

        if ($this->request->query('des') === TYPE_PRODUCT_BRAND) {
            $data = $this->ProductBrands->find('all')
                ->select([
                    'id',
                    'manufacturer_id',
                    'name_en',
                    'name'
                ])->where(['ProductBrands.id' => $this->request->query('id')])
                ->contain(['Manufacturers'])->toArray();
        }

        echo json_encode([
            'status' => 1,
            'message' => 'success',
            'data' => $data,
        ]);
    }

    public function getProductByBrandId()
    {
        if ($this->request->is('ajax')) {
            $this->autoRender = false;
            $this->response->disableCache();
            $this->response->type('json');
            $this->viewBuilder()->layout(false);

            //pr($this->request->query('id'));exit;
            $this->loadModel('Products');
            $data = $this->Products->find('all')
                ->where([
                    'Products.product_brand_id' => $this->request->query('id'),
                ])->toArray();

            if ($data) {
                $this->response->body(json_encode([
                    'status' => 1,
                    'message' => 'success',
                    'data' => $data,
                ]));
                return $this->response;
            } else {
                $this->response->body(json_encode([
                    'status' => 0,
                    'message' => 'success',
                    'data' => '',
                ]));
                return $this->response;
            }
        }
    }

    /**
     * list all products base on brand selected.
     */
    public function productDropdown()
    {
        if ($this->request->is('ajax')) {
            $brand_id = $this->request->query('brand_id');
            $en = $this->request->session()->read('tb_field');
            $products = $this->Products->productsDropdown($brand_id, $en, 'list');
            $this->set(compact('products', 'en'));
            $this->set('_serialize', ['products', 'en']);
        }

        $this->viewBuilder()->layout('ajax');
    }

    public function productAndDetailsDropdown()
    {
        if ($this->request->is('ajax')) {
            $en = $this->request->session()->read('tb_field');
            $products = $this->Products->productDetailsDropdown();
            $this->set(compact('products', 'en'));
            $this->set('_serialize', ['products', 'en']);
        }

        $this->viewBuilder()->layout('ajax');
    }

    public function getProductNameBySearch()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->viewBuilder()->layout('ajax');
        $this->autoRender = false;
        $this->response->type('json');
        $keyword = trim($this->request->query('keyword'));
        $products = $this->Products->find()
            ->where(['is_suspend' => 0])
            ->where([
                'OR' => [
                    'name_en LIKE' => '%' . $keyword . '%',
                    'name LIKE' => '%' . $keyword . '%',
                ]
            ])
            ->limit(PAGE_LIMIT_NUMBER)
            ->all();
        $this->response->body(json_encode([
            'status' => '1',
            'message' => MSG_SUCCESS,
            'data' => $products,
        ]));
        return $this->response;
    }

    public function getListOfProductsByProductBrandId()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->viewBuilder()->layout('ajax');
        $this->autoRender = false;
        $this->response->type('json');
        $keyword = trim($this->request->query('keyword'));
        $products = $this->Products->find()
            ->where(['is_suspend' => 0])
            ->where(['product_brand_id' => $this->request->query('product_brand_id')])
            ->where([
                'OR' => [
                    'name_en LIKE' => '%' . $keyword . '%',
                    'name LIKE' => '%' . $keyword . '%',
                ]
            ])
            ->contain([
                'ProductDetails' => [
                    'queryBuilder' => function ($q) {
                        return $q->where(['ProductDetails.is_suspend' => 0]);
                    },
                    'SingleUnits',
                    'PackSizes',
                ],
            ])
            ->limit(PAGE_LIMIT_NUMBER)
            ->all();
        $this->response->body(json_encode([
            'status' => '1',
            'message' => MSG_SUCCESS,
            'data' => $products,
        ]));
        return $this->response;
    }
}
