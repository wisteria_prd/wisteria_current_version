<?php
namespace App\Controller;
use App\Controller\AppController;
use Cake\Network\Exception\NotFoundException;

class CountriesController extends AppController
{
    public function getList()
    {
        // CHECK REQUEST IS AJAX
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->viewBuilder()->layout('ajax');
        $this->autoRender = false;
        $this->response->type('json');
        $data = $this->Countries->find('all')->toArray();
        $this->response->body(json_encode([
            'status' => 1,
            'message' => MSG_SUCCESS,
            'data' => $data,
        ]));
        return $this->response;
    }
}
