<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\Datasource\ConnectionManager;
use Cake\Database\Schema\TableSchema;
use Cake\Database\Schema\Collection;
class Sale1sController extends AppController
{
    public function initialize() {
        parent::initialize();
    }

    public function loadClinicName()
    {
        if ($this->request->is('ajax')) {
            $this->autoRender = false;
            $this->response->disableCache();
            $this->response->type('json');
            $this->viewBuilder()->layout(false);

            $keyword = trim($this->request->query('keyword'));
            $query = $this->Sale1s->find()
                ->select(['clinic_name_jp', 'clinic_name_en'])
                ->where(['is_deleted = 0'])
                ->group(['clinic_name_jp'])
                ->order(['clinic_name_jp']);

            $results = $query->where(['clinic_name_jp LIKE ' => '%' . $keyword . '%'])
                ->orWhere(['clinic_name_en LIKE ' => '%' . $keyword . '%']);
            $this->response->body(json_encode($results->toArray()));
            return $this->response;
        }
    }

    public function upload()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->autoRander = false;

        if (isset($this->request->data['file'])) {
            $this->loadComponent('ExtractCsv');

            $query = $this->current_db();
            if ($query) {
                $fields = $query->toArray();
                $fields_name = array_keys($fields);
                $data = $this->ExtractCsv->executeExtract($this->request->data['file'], $fields_name);
//pr($data);exit;
                if (!empty($data)) {
                    $conn = ConnectionManager::get('default');
                    $conn->execute('TRUNCATE TABLE sale1s');
                    $entities = $this->Sale1s->newEntities($data);
                    $result = $this->Sale1s->saveMany($entities);
                    if ($result) {
                        // save log
                        $this->loadModel('Logs');
                        $log_entity = $this->Logs->newEntity(['type' => ucfirst(USER_ROLE_SALE)]);
                        $this->Logs->save($log_entity);

                        echo json_encode(['success' => 1, 'message' => __('TXT_MESSAGE_CREATE')]);
                        exit;
                    }
                } else {
                    $response = $this->Sale1s->responseErrorMessage($this->ExtractCsv->msg);
                    echo json_encode(['success' => 0, 'message' => $response]);
                    exit;
                }
            } else {
                //echo json_encode(['success' => 0, 'message' => __('An unexpected SYSTEM error happened. Please retry CSV uploading or contact a system administrator.')]);
                exit;
            }
        }
        echo json_encode(['success' => 0, 'message' => __('TXT_MESSAGE_ERROR')]);
    }

    //do this way to get column name
    protected function current_db()
    {
        $query = $this->Sale1s->find();
        $fields = $query
            ->select()
            ->first();
        if (!empty($fields)) {
            return $fields;
        }
        $fake_data = [
            'clinic_name_jp' => 'fake',
            'clinic_name_en' => 'fake',
            'invoice_number' => '1205',
            'order_date' => date('Y-m-d'),
            'product_code' => 'abc123',
            'product_name' => 'fake',
            'quantity' => 12345,
            'unit_price' => 12345,
            'agent_name' => 'fake'
        ];
        $entity = $this->Sale1s->newEntity($fake_data);
        $entity = $this->Sale1s->patchEntity($entity, $fake_data, ['validate' => false]);
        if ($this->Sale1s->save($entity)) {
            $query = $this->Sale1s->find();
            $fields = $query
                ->select()
                ->first();
            return $fields;
        }
        return false;
    }
}