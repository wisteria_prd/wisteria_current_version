<?php
namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\Network\Exception\NotFoundException;

class FunctionComponent extends Component
{
    public function ajaxResponse($status, $message, $data = null)
    {
        echo json_encode([
            'status' => $status,
            'message' => $message,
            'data' => $data,
        ]);
    }

    public function getError($request = [])
    {
        $data = [];
        if ($request) {
            foreach ($request as $key => $value) {
                $data[$key] = reset($value);
            }
        }

        return $data;
    }

    public function getErrors($data = [])
    {
        if (!$data) {
            return false;
        }
        $errors = [];
        foreach ($data as $key => $value) {
            $errors[$key] = reset($value);
        }

        return $errors;
    }
}
