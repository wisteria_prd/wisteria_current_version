<?php

namespace App\Controller\Component;

use Cake\Controller\Component;

class ReconstructDataComponent extends Component
{
    /**
     * Function for convert data from select all of cake-ROM and convert to Select2 array format
     * @return array
     */
    public function convertToSelect2($data = array(), $local = null)
    {
        $list = [];

        if ($data) {
            $fieldName = 'name' . $local;
            foreach ($data as $key => $value) {
                $name = $value[$fieldName];
                if (empty($name)) {
                    switch ($local) {
                        case '_en' :
                            $name = $value->name;
                            break;
                        default :
                            $name = $value->name_en;
                    }
                }
                $list[$value->id] = $name;
            }
        }

        return $list;
    }

    /**
     * get request from ajax of vue js response
     * @param type $data string
     */
    public function convertToObjFormVue($data)
    {
        $response = [];
        $data = explode('&', $data);

        if ($data) {
            foreach ($data as $key => $value) {
                $temp = explode('=', $value);
                $response[str_replace('"', '', current($temp))] = str_replace('"', '', end($temp));
            }
        }

        return $response;
    }
}
