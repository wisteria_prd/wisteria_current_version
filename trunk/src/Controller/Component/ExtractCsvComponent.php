<?php
namespace App\Controller\Component;

use Cake\Controller\Component;

class ExtractCsvComponent extends Component
{
    public $msg = null;

    public function executeExtract($file, $field_name = array())
    {
        if (isset($file['tmp_name']) && !empty($file['tmp_name'])) {
            $data_file = fopen($file['tmp_name'], 'r');
        } else {
            $data_file = false;
        }

        $data_extract = array();
        if ($data_file) {
            $fields = array();
            $count  = 0;
            $data = array();
            while (($csv = fgetcsv($data_file)) !== false) {
                $count++;
                // find field name
                if ($count === 1) {
                    $fields = array_flip($csv);
                    //pr($csv);
                    $csv_fields = array_keys($fields);
                    $db_fields = count($field_name);

                    if (empty(array_intersect($field_name, $csv_fields))) {
                        $this->msg = 'not contain field name';
                        $data_extract = [];
                        break;
                    } else if (count($csv_fields) > $db_fields) {
                        $this->msg = 'contain too much field';
                        $data_extract = [];
                        break;
                    } else if (count($csv_fields) < $db_fields) {
                        $this->msg = 'cotain less field';
                        $data_extract = [];
                        break;
                    } else {
                        foreach ($csv_fields as $f) {
                            if (!in_array($f, $field_name)) {
                                $this->msg = $f;
                                $data_extract = [];
                                break 2;
                            }
                        }
                    }
                    continue;
                }
                foreach ($fields as $key => $value) {
                    if (in_array($key, array('code', 'name', 'clinic_name_jp', 'product_code', 'product_name', 'product_brand', 'prefecture'))) {
                        $data[$key] = mb_convert_encoding(trim($csv[$value]), 'UTF-8', 'Shift-JIS, EUC-JP, JIS, SJIS, JIS-ms, eucJP-win, SJIS-win, ISO-2022-JP, ISO-2022-JP-MS, SJIS-mac, SJIS-Mobile#DOCOMO, SJIS-Mobile#KDDI, SJIS-Mobile#SOFTBANK, UTF-8-Mobile#DOCOMO, UTF-8-Mobile#KDDI-A, UTF-8-Mobile#KDDI-B, UTF-8-Mobile#SOFTBANK, ISO-2022-JP-MOBILE#KDDI');
                        //$data[$key] = mb_convert_encoding(trim($csv[$value]), 'UTF-8');
                    } else {
                        $data[$key] = trim($csv[$value]);
                    }
                    unset($data['id']);
                    unset($data['created']);
                    if (isset($data['not_display']) && empty($data['not_display'])) {
                        $data['not_display'] = 0;
                    }
                }
                //pr(mb_convert_encoding(trim($csv[2]), 'UTF-8'));
                //pr($fields);exit;
                $data_extract[] = $data;
            }
        }
        return $data_extract;
    }
}
