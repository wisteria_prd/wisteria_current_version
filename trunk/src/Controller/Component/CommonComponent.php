<?php
namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\ORM\TableRegistry;

// TODO REMOVE txt.. method
class CommonComponent extends Component
{
    private $local;

    public function initialize(array $config) {
        // INITIALIZE LOCAL
        parent::initialize($config);
        $this->local = $this->request->session()->read('tb_field');
    }

    public function txtConfig()
    {
        return __('TXT_CONFIGURATION');
    }

    public function txtSuppliers()
    {
        return __('TXT_SUPPLIERS');
    }

    public function txtImports()
    {
        return __('TXT_IMPORT');
    }

    public function txtInternationalImports()
    {
        return __('TXT_INTERNATIONAL_IMPORT');
    }

    public function txtImportDetails()
    {
        return __('TXT_IMPORT_DETAIL');
    }

    public function txtPurchaseConditions()
    {
        return __('TXT_PURCHASE_CONDITIONS');
    }

    public function txtBrand()
    {
        return __('TXT_BRANDS');
    }

    public function txtProduct()
    {
        return __('TXT_PRODUCT_MENU');
    }

    public function txtProductMain()
    {
        return __('TXT_PRODUCT_MENU_MAIN');
    }

    public function txtSuppliersMgt()
    {
        return __('TXT_SUPPLIERS_MANAGEMENT');
    }

    public function txtSellerBrand()
    {
        return __('TXT_SELLER_BRAND');
    }

    public function txtProductPackage()
    {
        return __('TXT_PRODUCT_PACKAGES_MENU');
    }

    public function txtManufacturers()
    {
        return __('TXT_MANUFACTURERS');
    }

    public function txtClinicMgt()
    {
        return __('TXT_CLINIC_MANAGEMENT');
    }

    public function txtClinics()
    {
        return __('TXT_CLINICS');
    }

    public function txtDoctor()
    {
        return __('TXT_DOCTOR');
    }

    public function txtSubsidiary()
    {
        return __('TXT_SUBSIDIARIES');
    }

    public function txtGroup()
    {
        return __('TXT_GROUPS');
    }

    public function txtGreetingCard()
    {
        return __('TXT_GREATING_CARD');
    }

    public function txtPrice()
    {
        return __('TXT_PRICE_MENU');
    }

    public function txtSalePrice()
    {
        return __('TXT_PRICE_MENU_SALE');
    }

    public function txtPurchaseMgtMenu()
    {
        return __('TXT_PURCHASE_MGT_MENU');
    }

    public function txtSalePayment()
    {
        return __('TXT_SALE_PAYMENT_MENU');
    }

    public function txtPurchasePayment()
    {
        return __('TXT_PURCHASE_PAYMENT_MENU');
    }

    public function txtReceivePayment()
    {
        return __('TXT_RECEIVE_PAYMENT');
    }

    public function txtPurchasePrice()
    {
        return __('TXT_PRICE_MENU_PURCHASE');
    }

    public function txtPurchasePriceStandard()
    {
        return __('TXT_PRICE_MENU_SELLER_STANDARD');
    }

    public function txtPurchaseDiscount()
    {
        return __('TXT_PRICE_MENU_PURCHASE_DISCOUNT');
    }

    public function txtPurchaseFoc()
    {
        return __('TXT_PRICE_MENU_FOC');
    }

    public function txtStockSummary()
    {
        return __('TXT_STOCK_SUMMARY');
    }

    public function txtMenuAbroad()
    {
        return __('TXT_ABROAD_TRADE');
    }

    public function txtMenuDomestic()
    {
        return __('TXT_DOMESTIC_TRADE');
    }

    public function txtToMp()
    {
        return __('TXT_TO_MP');
    }

    public function txtToSnp()
    {
        return __('TXT_TO_SNP');
    }

    public function txtSupplierToJp()
    {
        return __('TXT_SUPPLIER_TO_JP');
    }

    public function txtOrderToS()
    {
        return __('TXT_ORDER_TO_S');
    }

    public function txtToSJP()
    {
        return __('TXT_TO_SJP');
    }

    public function txtMenuPayment()
    {
        return __('STR_PAYMENT_CAT');
    }

    public function txtMenuPurchase()
    {
        return __('TXT_MENU_PURCHASE');
    }

    public function txtMenuPoList()
    {
        return __('TXT_MENU_PO_LIST');
    }

    public function txtRegister()
    {
        return __('TXT_REGISTER');
    }

    public function txtEdit()
    {
        return __('TXT_EDIT');
    }

    public function txtList()
    {
        return __('TXT_LIST');
    }

    public function nameEnOrJp($name_jp, $name_en, $en = '')
    {
        $new_name = '';
        switch ($en) {
            case '_en':
                $new_name = $name_en;
                if ($new_name == '') {
                    $new_name = $name_jp;
                }
                break;

            case '_jp':
            default:
                $new_name = $name_jp;
                if ($new_name == '') {
                    $new_name = $name_en;
                }
                break;
        }
        return h($new_name);
    }

    public function addressFormat($en, $data, $isSingapore = '')
    {
        $address = '';
        $jp_addr2 = '';
        $en_addr2 = '';
        $tel = '';
        $fax = $data->fax;
        switch ($en) {
            case '_en':
                if ($data->building_en || $data->street_en || $data->city_en) {
                    $address .= $this->addressEnFormat($data, $isSingapore);
                    $en_addr2 .= $this->addressEnFormat2($data, $isSingapore);
                } else {
                    $address .= $this->addressJpFormat($data);
                    $jp_addr2 .= $this->addressJpFormat2($data);
                }
                break;

            case '_jp':
            default:
                if ($data->building || $data->street || $data->city) {
                    $address .= $this->addressJpFormat($data);
                    $jp_addr2 .= $this->addressJpFormat2($data);
                } else {
                    $address .= $this->addressEnFormat($data, $isSingapore);
                    $en_addr2 .= $this->addressEnFormat2($data, $isSingapore);
                }
                break;
        }
        if ($data->info_details) {
            if ($data->info_details[0]->phone) {
                $tel .= $data->info_details[0]->phone;
            } else {
                $tel .= $data->info_details[0]->tel;
            }
        }
        return [$address, $jp_addr2, $en_addr2, $tel, $fax];
    }

    // JAPAN ADDRESS FORMAT FIRST LINE
    private function addressJpFormat($data)
    {
        $address = '〒';
        $address .= $data->postal_code . '　';
        if ($data->prefecture) {
            $address .= $data->prefecture;
        }
        if ($data->city) {
            $address .= $data->city;
        }
        return $address;
    }
}
