<?php
namespace App\Controller\Component;

use Cake\Controller\Component;

class CustomConstantComponent extends Component
{
    /**
     * @var int VALUE_ZERO Inactive, No, disabled...
     */
    const VALUE_ZERO = 0;

    /**
     * @var int VALUE_ONE Enable, Yes, Active...
     */
    const VALUE_ONE = 1;

    /**
     * @var string MANUFACTURER store type manufacturer
     */
    const MANUFACTURER = 'manufacturer';

    /**
     * @var string MEDICAL_CORP store type medical_corporations
     */
    const MEDICAL_CORP = 'medical_corporations';

    /**
     * @var string SUPPLIER store type supplier
     */
    const SUPPLIER = 'supplier';

    /**
     * @var string CUSTOMER store type customer
     */
    const CUSTOMER = 'customer';

    /**
     * @var string SUBSIDIARY store type subsidiary
     */
    const SUBSIDIARY = 'subsidiary';

    /**
     * @var string GROUP_MEDICAL_CORP store type group_medical_corporations
     */
    const GROUP_MEDICAL_CORP = 'group_medical_corporations';

    /**
     * @var string GROUP_CUSTOMER store type group_customer
     */
    const GROUP_CUSTOMER = 'group_customer';

    /**
     * @var string DOCTOR store type doctor
     */
    const DOCTOR = 'doctor';

    /**
     * @var string PERSON_IN_CHARGE store type person_in_charge
     */
    const PERSON_IN_CHARGE = 'person_in_charge';

    /**
     * @var string FOR_BRAND store type brand
     */
    const FOR_BRAND = 'brand';

    /**
     * @var string FOR_PRODUCT store type product
     */
    const FOR_PRODUCT = 'product';

    /**
     * @var string FOR_PO store type po
     */
    const FOR_PO = 'po';

    /**
     * @var string TYPE_AMOUNT store amount
     */
    const TYPE_AMOUNT = 'amount';

    /**
     * @var string TYPE_QTY store quantity
     */
    const TYPE_QTY = 'quantity';

    /**
     * @var string PRICING_PACKAGE store product_package
     */
    const PRICING_PACKAGE = 'product_package';

    /**
     * @var string PRICING_PRODUCT_DETAILS store product_detail
     */
    const PRICING_PRODUCT_DETAILS = 'product_detail';

    /**
     * @var int GENDER_MALE 1
     */
    const GENDER_MALE = 1;

    /**
     * @var int GENDER_FEMALE 0
     */
    const GENDER_FEMALE = 0;

    /**
     * use for change sale status
     * @return array status list
     */
    public function saleStatus()
    {
        return [
            PO_STATUS_DRAFF => __('TXT_DRAFT'),
            STATUS_FAXED_ROP => __('TXT_BUTTON_FAX_ROP'),
            PO_STATUS_PAID => __('TXT_PAID'),
            PO_STATUS_REQUESTED_SHIPPING => __('TXT_REQUESTED_SHIPMENT'),
            PO_STATUS_CUSTOM_CLEARANCE => __('TXT_CUSTOM_CLEARANCE'),
            PO_STATUS_REQUESTED_CC => __('TXT_REQUESTED_CC'),
            PO_STATUS_DELIVERED => __('TXT_DELIVERED'),
            PO_STATUS_COMPLETED => __('TXT_PO_STATUS_COMPLETED'),
            STATUS_PACKED => __('TXT_PACKED'),
            STATUS_SHIPPED => __('TXT_SHIPPED'),
            STATUS_APPLIED_CC => __('STATUS_APPLIED_CC'),
            PO_STATUS_ISSUE_ROP_OR_WINV => __('TXT_BUTTON_FAX_ROP'),
            STATUS_CANCEL => __('BTN_CANCEL'),
            STATUS_FAXED_W_INVOICE => __('STATUS_FAXED_W_INVOICE'),
            STATUS_PAID => __('TXT_PAID'),
            PO_STATUS_DELIVERING => __('TXT_DELIVERING'),
        ];
    }

    /**
     * Function suspendStatus
     * use for dropdown list suspend and recover
     * @return array
     */
    public function suspendStatus()
    {
        return [
            self::VALUE_ZERO => __('STR_USER_STATUS_RECOVER'),
            self::VALUE_ONE => __('STR_USER_STATUS_SUSPEND')
        ];
    }

    /**
     * Function userGender
     * use for dropdown list gender
     * @return array
     */
    public function userGender()
    {
        return [
            self::GENDER_MALE => __('TXT_GENDER_MALE'),
            self::GENDER_FEMALE => __('TXT_GENDER_FEMALE')
        ];
    }

    /**
     * Function pricingType
     * use for dropdown list pricing type
     * @return array
     */
    public function pricingType()
    {
        return [
            self::PRICING_PACKAGE => __('TXT_PRODUCT_PACKAGES'),
            self::PRICING_PRODUCT_DETAILS => __('TXT_PRODUCT_DETAIL')
        ];
    }

    /**
     * Function subsidiaryTypes
     * use for dropdown list subsidiary type
     * @return array
     */
    public function subsidiaryTypes()
    {
        return [
            self::CUSTOMER => __('TXT_CUSTOMERS'),
            self::MEDICAL_CORP => __('TXT_MEDICAL_CORPORATIONS'),
            self::GROUP_MEDICAL_CORP => __('TXT_GROUP_MEDICAL_CORPORATIONS'),
            self::GROUP_CUSTOMER => __('TXT_GROUP_CUSTOMERS')
        ];
    }

    /**
     * Function subsidiaryTypes
     * use for dropdown list puchaseOrder types
     * @return array
     */
    public function puchaseOrderTypes()
    {
        return [
            PO_TYPE_MP_TO_S => __('PO_TYPE_MP_TO_S'),
            PO_TYPE_W_TO_S => __('PO_TYPE_W_TO_S'),
            PO_TYPE_W_TO_MP => __('PO_TYPE_W_TO_MP'),
        ];
    }

    /**
     * Function messageStatus
     * use for dropdown list message status
     * @return array
     */
    public function messageStatus()
    {
        return [
            1 => __('TXT_URGENT'),
            2 => __('TXT_COMMENTS_COMPLETE'),
            3 => __('TXT_COMMENTS_NORMAL')
        ];
    }

    /**
     * Function groupType
     * use for dropdown list group type
     * @return array
     */
    public function groupType()
    {
        return [
            self::CUSTOMER => __('TXT_CUSTOMERS'),
            self::MEDICAL_CORP => __('TXT_MEDICAL_CORPORATIONS')
        ];
    }

    /**
     * Function greatingSendTo
     * use for dropdown list greating send to
     * @return array
     */
    public function greatingSendTo()
    {
        return [
            self::SUPPLIER => __('TXT_SUPPLIERS'),
            self::CUSTOMER => __('TXT_CUSTOMERS'),
            self::MANUFACTURER => __('TXT_MANUFACTURERS'),
            self::PERSON_IN_CHARGE => __('TXT_PERSON_IN_CHARGE'),
            self::DOCTOR => __('TXT_DOCTORS')
        ];
    }

    /**
     * Function focSelectBox
     * use for dropdown list foc select box
     * @return array
     */
    public function focSelectBox()
    {
        return [
            self::FOR_BRAND => __('TXT_BRANDS'),
            self::FOR_PRODUCT => __('STR_PRODUCT'),
            self::FOR_PO => __('PO')
        ];
    }

    /**
     * Function focRangeType
     * use for dropdown list foc range type
     * @return array
     */
    public function focRangeType()
    {
        return [
            self::TYPE_AMOUNT => __('TXT_AMOUNT'),
            self::TYPE_QTY => __('TXT_QUANTITY')
        ];
    }

    /**
     * Function affiliationClass
     * Get Affiliation Class Type List
    * @return array return array list of affiliation type
    */
    public function affiliationClass()
    {
        return [
            '' => __('TXT_SELECT_AFFILIATION_CLASS'),
            USER_CLASS_WISTERIA => 'wisteria',
            USER_CLASS_MEDIPRO => 'medipro',
        ];
    }

    /**
     * Function customerType
     * use for dropdown list customer type
     * @return array
     */
    public function customerType()
    {
        return [
            CUSTOMER_TYPE_NEW => __('TXT_NEW'),
            CUSTOMER_TYPE_NORMAL => __('TXT_NORMAL'),
            CUSTOMER_TYPE_CONTRACT => __('TXT_CONTRACT'),
            CUSTOMER_TYPE_BAD => __('TXT_BAD')
        ];
    }

    /**
     * Function productType
     * use for dropdown list product type
     * @return array
     */
    public function productType()
    {
        return [
            NORMAL_TYPE => __('TXT_NORMAL'),
            TYPE_DEVICE => __('TXT_DIVICE')
            /* future changed to these values
            TYPE_DEVICE => __('PRODUCT_TXT_SELECT_PRODUCT_CATEGORY_MEDICAL_DEVICE'),
            NORMAL_TYPE => __('PRODUCT_TXT_SELECT_PRODUCT_CATEGORY_NORMAL'),
            */
        ];
    }

    /**
     * Function pdfDocumentType
     * use for dropdown pdf file upload selection
     * @return array
     */
    public function pdfDocumentType()
    {
        return [
            PDF_IMPORT_DOC => __('TXT_IMPORT_DOCS'),
            PDF_ACC_EVIDENCE => __('TXT_ACCOUNTING_EVIDENCES'),
            PDF_OTHERS => __('TXT_OTHERS')
        ];
    }

    /**
     * Function importType
     * use for dropdown import type selection
     * @return array
     */
    public function importType()
    {
        return [
            TYPE_NORMAL => __('TXT_IMPORT_NORMAL'),
            TYPE_SAMPLE => __('TXT_IMPORT_SAMPLE')
        ];
    }

    /**
     * Function importDetailType
     * use for dropdown import detail type selection
     * @return array
     */
    public function importDetailType()
    {
        return [
            ISSUE_TYPE_WRONG => __('TXT_WRONG_PRODUCT'),
            ISSUE_TYPE_DAMAGE => __('TXT_DAMAGE_PRODUCT'),
            ISSUE_TYPE_LESS_QTY => __('TXT_DAMAGE_LESS_QTY')
        ];
    }

    /**
     * Function importReturnType
     * use for dropdown import return type selection
     * @return array
     */
    public function importReturnType()
    {
        return [
            STATUS_RETURNING => __('TXT_RETURNING'),
            STATUS_RETURNED => __('TXT_RETURNED'),
            STATUS_COMPLETED => __('TXT_COMPLETE')
        ];
    }

    /**
     * Function importDamageLevel
     * use for dropdown import return detail level selection
     * @return array
     */
    public function importDamageLevel()
    {
        return [
            LEVEL_A => LEVEL_A,
            LEVEL_B => LEVEL_B,
            LEVEL_C => LEVEL_C,
            LEVEL_F => LEVEL_F
        ];
    }

    public function typePdf()
    {
        return [
            '' => __('TXT_SELECT_TYPE'),
            PDF_IMPORT_DOC => __('TXT_IMPORT_DOCS'),
            PDF_ACC_EVIDENCE => __('TXT_ACCOUNTING_EVIDENCES'),
            PDF_OTHERS => __('TXT_OTHERS'),
        ];
    }

    public function paymentStatus()
    {
        return [
            'paid' => __('RECEIVE_PAYMENT_TXT_SELECT_RECEIVED'),
            'unpaid' => __('RECEIVE_PAYMENT_TXT_SELECT_NOT_RECEIVED')
        ];
    }
}
