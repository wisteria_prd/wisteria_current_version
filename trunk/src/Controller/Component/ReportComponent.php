<?php
namespace App\Controller\Component;

use Cake\Controller\Component;

class ReportComponent extends Component
{
    
    public $dataHeader = array();
    public $dataBody = array();
    public $data = array();

    public function setClinicValue($data)
    {
        if (!$data->count()) {
            return false;
        }
        foreach ($data->all() as $key => $value) {
            $this->setProductDetail($value);
            $this->dataHeader['order_date'][] = strtotime(date('Y-m-d', strtotime($value->order_date)));
            $this->dataHeader['total_order'] = ++$key;
        }
        $this->dataHeader['Report']['max_date'] = date('Y-m-d', max($this->dataHeader['order_date']));
        $this->dataHeader['Report']['min_date'] = date('Y-m-d', min($this->dataHeader['order_date']));
        $this->setBetweenDate('header');
        $this->setAveragePurchaseNumber('header');
        $this->setCalcuateAndUpdateEachProduct();
    }

    private function setProductDetail($value)
    {
        foreach ($value->sale_details as $key2 => $value2) {
            // HEADER
            // GROUP ARRAY ITEM
            $this->dataHeader['group_item'][$value->id]['product_name'][$key2] =$value2->product_name;
            $this->dataHeader['group_item'][$value->id]['sub_total'][$key2] = $value2->unit_price * $value2->quantity;
            $this->dataHeader['group_item'][$value->id]['qty'][$key2] = $value2->quantity;
            $this->dataHeader['group_item'][$value->id]['order_date'][$key2] = strtotime(date('Y-m-d', strtotime($value->order_date)));
            // CLINIC
            $this->dataBody[$value2->product_name]['clinic'][] = array(
                'id' => $value2->id,
                'quantity' => $value2->quantity,
                'unit_price' => $value2->unit_price,
                'invoice_number' => $value->sale_number,
                'product_name' => $value2->product_name,
                'order_date' => strtotime(date('Y-m-d', strtotime($value->order_date))),
            );
            $this->dataBody[$value2->product_name]['unit_price'][] = $value2->unit_price;
            // TOTAL AMOUNT
            $this->dataHeader['total_amount'][] = $value2->quantity * $value2->unit_price;
        }
    }

    private function setBetweenDate($mode = null, $key = null, $value = array())
    {
        if ($mode === 'header') {
            $order_dates = array_unique($this->dataHeader['order_date']);
            asort($order_dates);
            foreach ($order_dates as $key => $value) {
                if ($key === 0) {
                    $date = date_create(date('Y-m-d', $value));
                    $this->dataHeader['diff_order_date'] = array();
                } else {
                    $date2 = date_create(date('Y-m-d', $value));
                    $this->dataHeader['diff_order_date'][] = date_diff($date, $date2)->format('%a');
                    // SET AFTER DATE TO ...
                    $date = $date2;
                }
            }
        }
        if ($mode === 'body') {
            $order_dates = array_unique(\Cake\Utility\Hash::extract($value, 'clinic.{n}.order_date'));
            asort($order_dates);
            foreach ($order_dates as $key2 => $value2) {
                if ($key2 === 0) {
                    $date = date_create(date('Y-m-d', $value2));
                    $this->dataBody[$key]['average_order'] = array();
                } else {
                    $date2 = date_create(date('Y-m-d', $value2));
                    $this->dataBody[$key]['average_order'][] = date_diff($date, $date2)->format('%a');
                    // SET AFTER DATE TO ...
                    $date = $date2;
                }
            }
            $this->dataBody[$key]['date'] = $order_dates;
        }
    }

    private function setAveragePurchaseNumber($mode = null, $key = null)
    {
        if ($mode === 'header') {
            if (isset($this->dataHeader['diff_order_date']) && count($this->dataHeader['diff_order_date']) > 0) {
                $this->dataHeader['average_total_order_date'] = array_sum($this->dataHeader['diff_order_date']) / count($this->dataHeader['diff_order_date']);
            } else {
                $this->dataHeader['average_total_order_date'] = 0;
            }
        }
        if ($mode === 'body') {
            if ($this->dataBody[$key]['average_order']) {
                $sum_to_month = array_sum($this->dataBody[$key]['average_order']) / 30;
                $this->dataBody[$key]['average_order'] = $sum_to_month / $this->dataBody[$key]['total_order'];
            } else {
                $this->dataBody[$key]['average_order'] = 0;
            }
        }
    }

    private function setPurchaseOrderNumberForThreeYears($value = array(), $key = null)
    {
        $lastestY = date('Y', max($this->dataHeader['order_date']));
        $previousY = $lastestY - 1;
        $afterPreviousY = $previousY - 1;
        $total_unit_price = 0;
        foreach ($value['clinic'] as $key3 => $value3) {
            $month = (int)date('m', $value3['order_date']);
            if ($lastestY == date('Y', $value3['order_date'])) {
                $this->dataBody[$key]['year'][$month][$lastestY]['qty'][] = $value3['quantity'];
                $this->dataBody[$key]['year'][$month][$lastestY]['unit_price'][] = $value3['unit_price'];
            }
            if ($previousY == date('Y', $value3['order_date'])) {
                $this->dataBody[$key]['year'][$month][$previousY]['qty'][] = $value3['quantity'];
                $this->dataBody[$key]['year'][$month][$previousY]['unit_price'][] = $value3['unit_price'];
            }
            if ($afterPreviousY == date('Y', $value3['order_date'])) {
                $this->dataBody[$key]['year'][$month][$afterPreviousY]['qty'][] = $value3['quantity'];
                $this->dataBody[$key]['year'][$month][$afterPreviousY]['unit_price'][] = $value3['unit_price'];
            }
            $subtotal = $value3['quantity'] * $value3['unit_price'];
            $total_unit_price += $subtotal;
        }
        $this->dataBody[$key]['total_unit_price'] = $total_unit_price;
        // CALCULATE AND SET VALUE
        for ($i = 1; $i <= 12; $i++) {
            // LAST YEAR
            $this->dataBody[$key]['year'][$i][$lastestY]['qty'] = array();
            $this->dataBody[$key]['year'][$i][$lastestY]['unit_price'] = array();
            // PREVIOUS YEAR
            $this->dataBody[$key]['year'][$i][$previousY]['qty'] = array();
            $this->dataBody[$key]['year'][$i][$previousY]['unit_price'] = array();
            // AFTER PREVIOUS YEAR
            $this->dataBody[$key]['year'][$i][$afterPreviousY]['qty'] = array();
            $this->dataBody[$key]['year'][$i][$afterPreviousY]['unit_price'] = array();
        }
    }

    private function setCalcuateAndUpdateEachProduct()
    {
        foreach ($this->dataBody as $key => $value) {
            $this->dataBody[$key]['product_name'] = $key;
            $this->dataBody[$key]['total_order'] = count($value['clinic']);
            $this->dataBody[$key]['total_unit_price'] = array_sum($value['unit_price']);
            $this->dataBody[$key]['average_amount'] = array_sum($value['unit_price']) / count($value['clinic']);
            $this->setBetweenDate('body', $key, $value);
            $this->setAveragePurchaseNumber('body', $key);
            $this->setPurchaseOrderNumberForThreeYears($value, $key);
            // REMOVE UNUSED ARRAY
            unset($this->dataBody[$key]['clinic']);
            unset($this->dataBody[$key]['unit_price']);
        }
    }

    public function getClinicValue()
    {
        if ($this->dataHeader['total_order']) {
            $this->dataHeader['Report']['total_order'] = $this->dataHeader['total_order'];
        } else {
            $this->dataHeader['Report']['total_order'] = 0;
        }
        if (isset($this->dataHeader['total_amount'])) {
            $this->dataHeader['Report']['total_amount'] =  array_sum($this->dataHeader['total_amount']);
        } else {
            $this->dataHeader['Report']['total_amount'] = 0;
        }
        if (isset($this->dataHeader['total_amount'])) {
            $this->dataHeader['Report']['total_average_amount'] =  array_sum($this->dataHeader['total_amount']) / $this->dataHeader['Report']['total_order'];
        } else {
            $this->dataHeader['Report']['total_average_amount'] = 0;
        }
        if (isset($this->dataHeader['average_total_order_date']) && $this->dataHeader['average_total_order_date']) {
            $this->dataHeader['Report']['average_total_order_date'] =  $this->dataHeader['average_total_order_date'];
        } else {
            $this->dataHeader['Report']['average_total_order_date'] = 0;
        }
        if (isset($this->dataHeader['diff_order_date']) && $this->dataHeader['diff_order_date']) {
            $this->dataHeader['Report']['diff_order_date'] =  $this->dataHeader['diff_order_date'];
        } else {
            $this->dataHeader['Report']['diff_order_date'] = 0;
        }
        // list
        if ($this->dataBody) {
            $this->dataBody['Report'] = $this->dataBody;
        } else {
            $this->dataBody['Report'] = array();
        }
        $this->data['Header'] = $this->dataHeader['Report'];
        $this->data['Body'] = $this->dataBody['Report'];
        return $this->data;
    }
}
