<?php
namespace App\Controller\Component;

use Cake\Controller\Component;

class FileUploadComponent extends Component
{

    private $_fileMimeType;
    private $_allowExtension;
    private $_uploadedDestination;

    public $error_info = [];
    public $maxSize = 2097152;

    public function setMimeType($mimeType)
    {
        $this->_fileMimeType = $mimeType;
    }

    public function getMimeType()
    {
        //$mime = ['application/pdf', 'image/jpeg'];
        return $this->_fileMimeType;
    }

    public function setFileExtension($allowExtension)
    {
        $this->_allowExtension = $allowExtension;
    }

    public function getFileExtension()
    {
        //$extension = ['jpg', 'jpeg', 'gif', 'png', 'pdf'];
        return $this->_allowExtension;
    }

    public function setDestination($destination)
    {
        $this->_uploadedDestination = $destination;
    }

    public function getDestination()
    {
        return $this->_uploadedDestination;
    }

    public function upload($request = [])
    {
        if (empty($request['file']['tmp_name'])) {
            $this->error_info = ['error' => 1, 'origin_name' => '', 'new_name' => '', 'msg' => __('TXT_NO_FILE_UPLOADED')];
            return $this->error_info;
        }

        $support_types = $this->getMimeType();
        $is_valid_file = mime_content_type($request['file']['tmp_name']);

        if (in_array($is_valid_file, $support_types)) {
            $extension = substr(strrchr($request['file']['name'], '.'), 1);
            $array_extensions = $this->getFileExtension();

            if ($request['file']['size'] >= $this->maxSize) {
                $this->error_info = ['error' => 2, 'origin_name' => '', 'new_name' => '', 'msg' => __('TXT_UPLOAD_EXCEED_FILE_SIZE')];
                return $this->error_info;
            }

            if (in_array(strtolower($extension), $array_extensions)) {
                $file = explode('.', $request['file']['name']);
                $file = preg_replace('/\s+/', '', $file[0]);
                $file = str_replace('-', '_', $file);
                $fname = date('YmdHis') . '_' . $file . '.' . $extension;
                if (!move_uploaded_file($request['file']['tmp_name'], $this->getDestination() . DS . $fname)) {
                    $this->error_info = ['error' => 3, 'origin_name' => '', 'new_name' => '', 'msg' => __('TXT_ERROR_ON_UPLOAD')];
                    return $this->error_info;
                }
                $this->error_info = [
                    'error' => 0,
                    'origin_name' => $request['file']['name'],
                    'new_name' => $fname,
                    'msg' => __('TXT_FILE_UPLOAD_COMPLETED')
                ];
                return $this->error_info;
            } else {
                $this->error_info = ['error' => 4, 'origin_name' => '', 'new_name' => '', 'msg' => __('TXT_FILE_UPLOADED_NOT_SUPPORT')];
                return $this->error_info;
            }
        }
        $this->error_info = ['error' => 4, 'origin_name' => '', 'new_name' => '', 'msg' => __('TXT_FILE_UPLOADED_NOT_SUPPORT')];
        return $this->error_info;
    }
}