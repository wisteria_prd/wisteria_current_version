<?php
namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\ORM\TableRegistry;

class AccessClientComponent extends Component
{
    public function setModule($module = null)
    {
        $this->module = $module;
        return $this;
    }

    public function getModule()
    {
        return $this->module;
    }

    public function setAction($action = null)
    {
        $this->action = $action;
        return $this;
    }

    public function getAction()
    {
        return $this->action;
    }

    public function outPutArray()
    {
        $moduleControl = TableRegistry::get('ModuleControls');
        $module = $moduleControl->getByName($this->getModule());
        if (!$module) {
            return false;
        }
        $data = [
            'module_name' => $this->getModule(),
            'module_control_id' => $module,
            'action' => $this->getAction(),
            'user_group_id' => $this->request->session()->read('Auth.User.user_group_id'),
        ];
        return $data;
    }

    public function checkAccessControl()
    {
        $userPermission = TableRegistry::get('UserPermissions');
        return $this->isAuthorized($this->outPutArray(), $userPermission);
    }

    private function isAuthorized($data = null, $table = null)
    {
        $userPermission = $table->find()
            ->where(['user_group_id' => $data['user_group_id']])
            ->contain([
                'ModuleActions' => function ($q) use ($data) {
                if ($data) {
                    $q->where(['module_control_id' => $data['module_control_id']]);
                }
                return $q;
                },
            ])
            ->all();
        if (!iterator_count($userPermission)) {
            return false;
        }
        // MAKE ARRAY LIST OF ACTION
        $actions = [];
        foreach ($userPermission as $key => $value) {
            if ($value->module_action) {
                $actions[] = $value->module_action->name;
            }
        }
        // CHECK IF THIS ACTION HAS A RIGHT TO ACCESS
        if (!in_array($data['action'], $actions)) {
            return false;
        }
        return true;
    }
}
