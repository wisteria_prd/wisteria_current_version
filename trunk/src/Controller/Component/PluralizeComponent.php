<?php

namespace App\Controller\Component;

use Cake\Controller\Component;

class PluralizeComponent extends Component
{
    static $plural = [
        '/(quiz)$/i' => '$1zes',
        '/^(ox)$/i' => '$1en',
        '/([m|l])ouse$/i' => '$1ice',
        '/(matr|vert|ind)ix|ex$/i' => '$1ices',
        '/(x|ch|ss|sh)$/i' => '$1es',
        '/([^aeiouy]|qu)y$/i' => '$1ies',
        '/(hive)$/i' => '$1s',
        '/(?:([^f])fe|([lr])f)$/i' => '$1$2ves',
        '/(shea|lea|loa|thie)f$/i' => '$1ves',
        '/sis$/i' => 'ses',
        '/([ti])um$/i' => '$1a',
        '/(tomat|potat|ech|her|vet)o$/i' => '$1oes',
        '/(bu)s$/i' => '$1ses',
        '/(alias)$/i' => '$1es',
        '/(octop)us$/i' => '$1i',
        '/(ax|test)is$/i' => '$1es',
        '/(us)$/i' => '$1es',
        '/s$/i' => 's',
        '/$/' => 's'
    ];

    static $singular = [
        '/(quiz)zes$/i' => '$1',
        '/(matr)ices$/i' => '$1ix',
        '/(vert|ind)ices$/i' => '$1ex',
        '/^(ox)en$/i' => '$1',
        '/(alias)es$/i' => '$1',
        '/(octop|vir)i$/i' => '$1us',
        '/(cris|ax|test)es$/i' => '$1is',
        '/(shoe)s$/i' => '$1',
        '/(o)es$/i' => '$1',
        '/(bus)es$/i' => '$1',
        '/([m|l])ice$/i' => '$1ouse',
        '/(x|ch|ss|sh)es$/i' => '$1',
        '/(m)ovies$/i' => '$1ovie',
        '/(s)eries$/i' => '$1eries',
        '/([^aeiouy]|qu)ies$/i' => '$1y',
        '/([lr])ves$/i' => '$1f',
        '/(tive)s$/i' => '$1',
        '/(hive)s$/i' => '$1',
        '/(li|wi|kni)ves$/i' => '$1fe',
        '/(shea|loa|lea|thie)ves$/i' => '$1f',
        '/(^analy)ses$/i' => '$1sis',
        '/((a)naly|(b)a|(d)iagno|(p)arenthe|(p)rogno|(s)ynop|(t)he)ses$/i' => '$1$2sis',
        '/([ti])a$/i' => '$1um',
        '/(n)ews$/i' => '$1ews',
        '/(h|bl)ouses$/i' => '$1ouse',
        '/(corpse)s$/i' => '$1',
        '/(us)es$/i' => '$1',
        '/s$/i' => ''
    ];

    static $irregular = [
        'move' => 'moves',
        'foot' => 'feet',
        'goose' => 'geese',
        'sex' => 'sexes',
        'child' => 'children',
        'man' => 'men',
        'tooth' => 'teeth',
        'person' => 'people',
        'valve' => 'valves'
    ];

    static $uncountable = [
        'sheep',
        'fish',
        'deer',
        'series',
        'species',
        'money',
        'rice',
        'information',
        'equipment'
    ];

    public function pluralize($string)
    {
        // save some time in the case that singular and plural are the same
        if (in_array(strtolower($string), self::$uncountable)) return $string;


        // check for irregular singular forms
        foreach (self::$irregular as $pattern => $result) {
            $pattern = '/'.$pattern.'$/i';

            if (preg_match($pattern, $string))
                    return preg_replace($pattern, $result, $string);
        }

        // check for matches using regular expressions
        foreach (self::$plural as $pattern => $result) {
            if (preg_match($pattern, $string))
                    return preg_replace($pattern, $result, $string);
        }

        return $string;
    }

    public function singularize($string)
    {
        // save some time in the case that singular and plural are the same
        if (in_array(strtolower($string), self::$uncountable)) return $string;

        // check for irregular plural forms
        foreach (self::$irregular as $result => $pattern) {
            $pattern = '/'.$pattern.'$/i';

            if (preg_match($pattern, $string))
                    return preg_replace($pattern, $result, $string);
        }

        // check for matches using regular expressions
        foreach (self::$singular as $pattern => $result) {
            if (preg_match($pattern, $string))
                    return preg_replace($pattern, $result, $string);
        }

        return $string;
    }

    public function pluralize_if($count, $string)
    {
        if ($count < 2 ) return $string;
        else return self::pluralize($string);
    }

    /**
    * follow pattern:
    * * if have both single_unit_size, pack_size, n description:
        * P1. Brand Product (1ml x 1Syringe) (description)
        * Other patterns r:
        * P2. Brand Product (1Syringe) (description)
        * P3. Brand Product (description)
        * b. No product_detail record is also ok
        * Then, display is
        * P4. Brand Product
     * @param object $data
    * @returns string
    */
    public function poDetail($data)
    {
        $list = [];
        $str = '';

        if ($data) {
            foreach ($data as $key => $value) {
                // brand name
                if (!empty($value->brand_name)) {
                    $str = $value->brand_name . ' ';
                }
                // product name
                if (!empty($value->product_name)) {
                    $str .= $value->product_name . ' ';
                }
                // detail
                if (!empty($value->single_unit_value) && !empty($value->pack_size_value)) {
                    $sgun = $this->pluralize_if($value->single_unit_value, $value->single_unit_name);
                    $psn = $this->pluralize_if($value->pack_size_value, $value->pack_size_name);
                    $str .= '(' .
                        $value->single_unit_value .
                        $sgun . ' x ' .
                        $value->pack_size_value .
                        $psn . ') ';
                } else if (empty($value->single_unit_value) && !empty($value->pack_size_value)) {
                    $psn = $this->pluralize_if($value->pack_size_value, $value->pack_size_name);
                    $str .= '(' .
                        $value->pack_size_value .
                        $psn . ') ';
                } else if (empty($value->pack_size_value) && !empty($value->single_unit_value)) {
                    $sgun = $this->pluralize_if($value->single_unit_value, $value->single_unit_name);
                    $str .= '(' .
                        $value->single_unit_value .
                        $sgun . ') ';
                }
                // description
                if (!empty($value->description)) {
                    $str .= '(' . $value->description . ')';
                }

                $list[$value->id] = $str;
            }
        }

        return $list;
    }
}
