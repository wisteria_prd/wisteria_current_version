<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\Network\Exception\NotFoundException;

class SaleCustomClearanceDocsController extends AppController
{
    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('Function');
    }

    public function createAndUpdate()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->autoRender = false;
        $this->response->disableCache();
        $this->response->type('json');
        $this->viewBuilder()->layout(false);
        $this->loadModel('Doctors');
        $data = $this->request->data;
        $doctor = null;

        if (isset($data['id'])) {
            $docs = $this->SaleCustomClearanceDocs->get($data['id']);
        } else {
            $docs = $this->SaleCustomClearanceDocs->newEntity();
        }
        $docs = $this->SaleCustomClearanceDocs->patchEntity($docs, $data);

        // get doctors by id
        if (!empty($data['doctor_id'])) {
            $doctor = $this->Doctors->get($docs->doctor_id);
            $docs->doctor_name = $doctor->last_name . ' ' . $doctor->first_name;
        }

        if ($docs->errors()) {
            $this->response->body(json_encode([
                'status' => 0,
                'message' => MSG_ERROR,
                'data' => $this->Function->getError($docs->errors()),
            ]));
        } else {
            if ($this->SaleCustomClearanceDocs->save($docs)) {
                $this->response->body(json_encode([
                    'status' => 1,
                    'message' => MSG_SUCCESS,
                ]));
            }
        }
        return $this->response;
    }
}