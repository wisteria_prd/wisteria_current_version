<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Network\Exception\NotFoundException;

class ProductDetailsController extends AppController
{
    private $en;

    public function initialize() {
        parent::initialize();
        $this->loadModel('Products');
        $this->en = $this->request->session()->read('tb_field');
    }

    public function index()
    {
        $options = [];
        $keyword = '';
        $product = '';
        if ($this->request->query('product_id')) {
            $product_id = $this->request->query('product_id');
            $options[] = [
                'product_id' => $product_id,
            ];
            $product = $this->Products->get($product_id);
        }
        if ($this->request->query('keyword')) {
            $keyword = $this->request->query('keyword');
            $options[] = [
                'OR' => [
                    'ProductDetails.description LIKE' => '%'.trim($keyword).'%',
                    'ProductDetails.remark LIKE' => '%'.trim($keyword).'%',
                ],
            ];
        }
        if ($this->request->query('inactive')) {
            $options[] = [
                'is_suspend' => 1,
            ];
        }
        $this->paginate = [
            'conditions' => $options,
            'limit' => $this->request->query('displays') ? $this->request->query('displays') : '10',
            'contain' => [
                'Products' => [
                    'ProductBrands' => ['Manufacturers'],
                ],
                'Packings' => [
                    'sort' => ['Packings.name_en' => 'asc'],
                ],
                'SingleUnits',
                'PackSizes',
                'LogisticTemperatures',
            ],
            'sortWhitelist' => [
                'Packings.name_en',
                'pack_size',
                'SingleUnits.name_en',
                'single_unit_size',
                'sale_flag',
                'purchase_flag',
                'Products.code',
                'ProductDetails.type',
                'Products.name',
                'Products.name_en',
                'Manufacturers.name',
            ],
        ];
        if (!empty($keyword)) {
            $this->paginate['contain']['SingleUnits'] = function($q) use($keyword) {
                return $q->where([
                    'or' => [
                        'SingleUnits.name LIKE' => '%' . $keyword . '%',
                        'SingleUnits.name_en LIKE' => '%' . $keyword . '%',
                    ],
                ]);
            };
            $this->paginate['contain']['LogisticTemperatures'] = function($q) use($keyword) {
                return $q->where([
                    'or' => [
                        'SingleUnits.name LIKE' => '%' . $keyword . '%',
                        'SingleUnits.name_en LIKE' => '%' . $keyword . '%',
                    ],
                ]);
            };
            $this->paginate['contain']['PackSizes'] = function($q) use($keyword) {
                return $q->where([
                    'or' => [
                        'SingleUnits.name LIKE' => '%' . $keyword . '%',
                        'SingleUnits.name_en LIKE' => '%' . $keyword . '%',
                    ],
                ]);
            };
        }
        $data = [
            'data' => $this->paginate($this->ProductDetails),
            'paging' =>  $this->request->param('paging')['ProductDetails']['pageCount'],
            'product' => $product,
            'locale' => $this->en,
        ];
        $this->set($data);
    }

    public function create()
    {
        $data = $this->ProductDetails->newEntity();
        if ($this->request->is('post')) {
            $data = $this->ProductDetails->patchEntity($data, $this->request->data);
            if ($this->ProductDetails->save($data)) {
                $this->redirect([
                    'action' => 'index',
                    '?' => [
                        'product_id' => $data->product_id,
                    ],
                ]);
            }
        }
        $pack_sizes = $this->getPackSizeList();
        $single_units = $this->getSingleUnitList();
        $packings = $this->getPackingList();
        $temperaturers = $this->getTemperaturerList();

        $txt_register = $this->Common->txtRegister();
        $this->set(compact('data', 'pack_sizes', 'single_units', 'packings', 'temperaturers', 'txt_register'));
    }

    public function edit($id)
    {
        $data = $this->ProductDetails->get($id);
        if ($this->request->is(['post', 'put'])) {
            $data = $this->ProductDetails->patchEntity($data, $this->request->data);
            if ($this->ProductDetails->save($data)) {
                $this->redirect([
                    'action' => 'index',
                    '?' => [
                        'product_id' => $data->product_id,
                    ],
                ]);
            }
        }
        $pack_sizes = $this->getPackSizeList();
        $single_units = $this->getSingleUnitList();
        $packings = $this->getPackingList();
        $temperaturers = $this->getTemperaturerList();

        $txt_edit = $this->Common->txtEdit();
        $this->set(compact('data', 'pack_sizes', 'single_units', 'packings', 'temperaturers', 'txt_edit'));
    }

    public function delete()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->viewBuilder()->layout('ajax');
        $this->autoRender = false;

        $data = $this->ProductDetails->get($this->request->data['id']);
        if ($this->ProductDetails->delete($data)) {
            echo json_encode([
                'status' => 1,
                'message' => 'success',
            ]);
        }
    }

    public function view($id)
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->viewBuilder()->layout('ajax');
        $data = $this->ProductDetails->find()
            ->where(['ProductDetails.id' => $id])
            ->contain([
                'Products' => [
                    'ProductBrands' => ['Manufacturers'],
                ],
                'SingleUnits',
                'Packings',
                'PackSizes',
                'LogisticTemperatures',
            ])->first();
        $data1 = [
            'data' => $data,
            'locale' => $this->en,
        ];
        $this->set($data1);
    }

    public function updateSuspend()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->viewBuilder()->layout('ajax');
        $this->autoRender = false;
        $data = $this->ProductDetails->get($this->request->data['id']);
        $data->is_suspend = ($this->request->data['is_suspend'] == 0 ) ? 1 : 0;

        if ($this->ProductDetails->save($data)) {
            echo json_encode([
                'status' => 1,
                'message' => 'success',
            ]);
        }
    }

    public function getPackSizeList()
    {
        $this->loadModel('PackSizes');
        $data = $this->PackSizes->find('list', [
            'keyField' => 'id',
            'valueField' => 'name' . $this->en,
        ])->order(['PackSizes.name' . $this->en => 'asc'])->toArray();

        return $data;
    }

    public function getSingleUnitList()
    {
        $this->loadModel('SingleUnits');
        $data = $this->SingleUnits->find('list', [
            'keyField' => 'id',
            'valueField' => 'name' . $this->en,
        ])->order(['SingleUnits.name' . $this->en => 'asc'])->toArray();

        return $data;
    }

    public function getPackingList()
    {
        $this->loadModel('Packings');
        $data = $this->Packings->find('list', [
            'keyField' => 'id',
            'valueField' => 'name' . $this->en,
        ])->order(['Packings.name' . $this->en => 'asc'])->toArray();

        return $data;
    }

    public function getTemperaturerList()
    {
        $this->loadModel('LogisticTemperatures');
        $data = $this->LogisticTemperatures->find('list', [
            'keyField' => 'id',
            'valueField' => 'name' . $this->en,
        ])->order(['LogisticTemperatures.name' . $this->en => 'asc'])->toArray();

        return $data;
    }

    public function getList()
    {
        if ($this->request->is('ajax')) {
            $this->autoRender = false;
            $this->response->disableCache();
            $this->response->type('json');
            $this->viewBuilder()->layout(false);

            $data = $this->ProductDetails->find('all')
                ->contain([
                    'SingleUnits',
                    'LogisticTemperatures',
                    'PackSizes',
                ])->toArray();

            $this->response->body(json_encode([
                'status' => 1,
                'message' => 'success',
                'data' => $data,
            ]));
            return $this->response;
        }
    }

    public function productList()
    {
        if ($this->request->is('ajax')) {
            $en = $this->request->session()->read('tb_field');
            $products = $this->ProductDetails->find('all')
                ->contain([
                    'Products' => ['ProductBrands'],
                    'PackSizes',
                    'SingleUnits'
                ])
                ->where(['ProductDetails.is_suspend' => 0]);
            $this->set(compact('products', 'en'));
            $this->set('_serialize', ['products']);
        }
        $this->viewBuilder()->layout('ajax');
    }

    /**
     * list all products details base on product selected.
     */
    public function productDetailsDropdown()
    {
        if ($this->request->is('ajax')) {
            $product_id = $this->request->query('product_id');
            $product_name = $this->request->query('product_name');
            $products = $this->ProductDetails->productDetailsDropdown($product_id);
            //pr($products->toArray()); exit;
            $en = $this->request->session()->read('tb_field');
            $this->set(compact('products', 'en', 'product_name'));
            $this->set('_serialize', ['products', 'en', 'product_name']);
        }

        $this->viewBuilder()->layout('ajax');
    }

    public function getListOfProductDetailsByProductId()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->viewBuilder()->layout('ajax');
        $this->autoRender = false;
        $this->response->type('json');
        $keyword = trim($this->request->query('keyword'));
        $product_details = $this->ProductDetails->find()
            ->where(['ProductDetails.is_suspend' => 0])
            ->where(['ProductDetails.product_id' => $this->request->query('product_id')])
            ->where([
                'OR' => [
                    'SingleUnits.name_en LIKE' => '%' . $keyword . '%',
                    'SingleUnits.name LIKE' => '%' . $keyword . '%',
                    'PackSizes.name_en LIKE' => '%' . $keyword . '%',
                    'PackSizes.name LIKE' => '%' . $keyword . '%',
                ]
            ])
            ->contain([
                'SingleUnits',
                'PackSizes',
            ])
            ->limit(PAGE_LIMIT_NUMBER)
            ->all();
        $this->response->body(json_encode([
            'status' => '1',
            'message' => MSG_SUCCESS,
            'data' => $product_details,
        ]));
        return $this->response;
    }
}
