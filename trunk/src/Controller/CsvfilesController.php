<?php
namespace App\Controller;
use App\Controller\AppController;
use Cake\Event\Event;

class CsvfilesController extends AppController
{
    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
    }

    // TODO check and remove, Log model has not found
    public function upload()
    {
        $this->loadModel('Logs');
        //upload log of products csv
        $last_product_log = $this->Logs->find()->where([
            'type' => ucfirst(TYPE_PRODUCT),
            ])
            ->order(['created' => 'desc'])
            ->first();

        //upload log of sales csv
        $last_sale_log = $this->Logs->find()->where([
            'type' => ucfirst(USER_ROLE_SALE),
            ])
            ->order(['created' => 'desc'])
            ->first();

        if ($last_product_log) {
            $this->set('last_upload_product_csv', $last_product_log->created);
        }
        if ($last_sale_log) {
            $this->set('last_upload_sale_csv', $last_sale_log->created);
        }
    }

}