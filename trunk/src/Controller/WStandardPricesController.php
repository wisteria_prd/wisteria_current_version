<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\Network\Exception\NotFoundException;

class WStandardPricesController extends AppController
{
    private $local;

    public function initialize() {
        parent::initialize();
        $this->local = $this->request->session()->read('tb_field');
    }

    public function index()
    {
        $options = [];
        if ($this->request->query('keyword')) {
            $keyword = $this->request->query('keyword');
            $options = [
                'OR' => [
                    'Products.name LIKE' => '%' . $keyword . '%',
                    'Products.name_en LIKE' => '%' . $keyword . '%',
                ]
            ];
        }
        $display = PAGE_NUMBER;
        if (!empty($this->request->query('displays'))) {
            $display = $this->request->query('displays');
        }
        if ($this->request->query('inactive')) {
            $options[] = [
                'WStandardPrices.is_suspend' => 1,
            ];
        }
        $this->paginate = [
            'conditions' => $options,
            'limit' => $display,
            'sortWhitelist' => [
                'Products.name',
                'Products.name_en',
                'standard_price',
            ],
            'contain' => [
                'Currencies',
                'ProductDetails' => [
                    'Products' => [
                        'ProductBrands' => ['Manufacturers'],
                    ],
                    'PackSizes',
                    'SingleUnits'
                ],
            ],
            'order' => ['WStandardPrices.created' => 'desc']
        ];

        try {
            $data = $this->paginate($this->WStandardPrices);
            $paging = $this->request->param('paging')['WStandardPrices']['pageCount'];
        } catch (NotFoundException $e) {
            $paging = $this->request->param('paging')['WStandardPrices']['pageCount'];
            $data = [];
        }

        $this->set(compact('data', 'paging'));
    }

    public function create()
    {
        $data = $this->WStandardPrices->newEntity();

        if ($this->request->is('ajax')) {
            $this->viewBuilder()->layout('ajax');
            $this->autoRender = false;
            $data = $this->WStandardPrices->patchEntity($data, $this->request->data);

            if ($data->errors()) {
                $errors = $this->getErrors($data->errors(), $this->request->data);
                $this->ajaxResponse(0, MSG_ERROR, $errors);
            } else {
                $errors = $this->getErrors(null, $this->request->data);
                if ($errors) {
                    $this->ajaxResponse(0, MSG_ERROR, $errors);
                } else {
                    $data1 = $this->getData($this->request->data);
                    $data1 = $this->WStandardPrices->newEntities($data1, ['validate' => false]);
                    $this->WStandardPrices->saveMany($data1);
                    $this->ajaxResponse(1, MSG_SUCCESS, $errors);
                }
            }
        }
        $this->loadModel('Manufacturers');
        $manufacturers_list = $this->Manufacturers->find()
            ->where(['Manufacturers.is_suspend' => 0]);
        $currencies = $this->getCurrenciesList();

        $txt_register = $this->Common->txtRegister();
        $en = $this->local;
        $this->set(compact('data', 'manufacturers_list', 'currencies', 'txt_register', 'en'));
    }

    public function edit($id)
    {
        $data = $this->WStandardPrices->get($id, [
            'contain' => [
                'ProductDetails' => [
                    'PackSizes',
                    'SingleUnits',
                    'Products' => [
                        'ProductBrands' => ['Manufacturers']
                    ]
                ],
            ]
        ]);

        $data1 = $this->WStandardPrices->getByProductDetailId($data->product_detail_id);

        if ($this->request->is('ajax')) {
            $this->viewBuilder()->layout('ajax');
            $this->autoRender = false;
            $errors = $this->getErrors(null, $this->request->data);

            if ($errors) {
                $this->ajaxResponse(0, MSG_ERROR, $errors);
            } else {
                $data2 = $this->getData($this->request->data, $data1);
                $data2 = $this->WStandardPrices->newEntities($data2, ['validate' => false]);
                $this->WStandardPrices->saveMany($data2);
                $this->ajaxResponse(1, MSG_SUCCESS, null);
            }
        }

        $product_detail = $data->product_detail;
        $product = $product_detail->product;
        $brand = $product->product_brand;
        $brands = [$brand->id => $this->nameEnOrJp($brand->name, $brand->name_en, $this->local)];
        $products = [$product->id => $this->nameEnOrJp($product->name, $product->name_en, $this->local)];
        $currencies = $this->getCurrenciesList();

        $txt_edit = $this->Common->txtEdit();
        $this->loadModel('Manufacturers');
        $manufacturers_list = $this->Manufacturers->find()
            ->where(['Manufacturers.is_suspend' => 0]);
        $en = $this->local;
        $this->set(compact('data', 'manufacturers_list', 'manufacturers', 'brands', 'products', 'product_detail', 'currencies', 'data1', 'txt_edit', 'en'));
    }

    public function view($id)
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->viewBuilder()->layout('ajax');
        $data = $this->WStandardPrices->findById($id)
            ->contain([
                'Currencies',
                'ProductDetails' => ['SingleUnits', 'PackSizes', 'Products']
            ])->first();

        $this->set(compact('data'));
    }

    public function delete()
    {
        if ($this->request->is('ajax')) {
            $this->autoRender = false;
            $this->response->disableCache();
            $this->response->type('json');
            $this->viewBuilder()->layout(false);

            $this->request->allowMethod(['post', 'delete']);
            $data = $this->WStandardPrices->get($this->request->data['id']);
            if ($this->WStandardPrices->delete($data)) {
                $this->response->body(json_encode(['status' => 1]));
                return $this->response;
            }
            $this->response->body(json_encode(['status' => 0, 'message' => __('TXT_DELETE_TROUBLE')]));
            return $this->response;
        }
    }

    public function updateSuspend()
    {
        if ($this->request->is('ajax')) {
            $this->autoRender = false;
            $this->response->disableCache();
            $this->response->type('json');
            $this->viewBuilder()->layout(false);

            $data = $this->WStandardPrices->get($this->request->data['id']);
            $data->is_suspend = ($this->request->data['is_suspend'] == 0 ) ? 1 : 0;
            if ($this->WStandardPrices->save($data)) {
                $this->response->body(json_encode([
                    'status' => 1,
                    'message' => 'success'
                ]));
                return $this->response;
            }

            $this->response->body(json_encode([
                'status' => 0,
                'message' => 'error'
            ]));
            return $this->response;
        }
    }

    public function getBrandByManufacturerId($id)
    {
        if ($this->request->is('ajax')) {
            $this->autoRender = false;
            $this->response->disableCache();
            $this->response->type('json');
            $this->viewBuilder()->layout(false);

            $this->loadModel('ProductBrands');
            $data = $this->ProductBrands->brandDropdown($id);
            $this->response->body(json_encode([
                'status' => 1,
                'data' => $data,
                'en' => $this->local,
                'message' => 'success'
            ]));
            return $this->response;
        }
    }

    public function getProductByBrandId($id)
    {
        if ($this->request->is('ajax')) {
            $this->autoRender = false;
            $this->response->disableCache();
            $this->response->type('json');
            $this->viewBuilder()->layout(false);

            $this->loadModel('Products');
            $data = $this->Products->find('all')
                ->where([
                    'Products.product_brand_id' => $id
                ]);
            $this->response->body(json_encode([
                'status' => 1,
                'data' => $data,
                'en' => $this->local,
                'message' => 'success'
            ]));
            return $this->response;
        }
    }

    public function getProductDetailByProductId($id)
    {
        if ($this->request->is('ajax')) {
            $this->autoRender = false;
            $this->response->disableCache();
            $this->response->type('json');
            $this->viewBuilder()->layout(false);

            $this->loadModel('ProductDetails');
            $data = $this->ProductDetails->productDetailsDropdown($id);
            $this->response->body(json_encode([
                'status' => 1,
                'data' => $data,
                'en' => $this->local,
                'message' => 'success'
            ]));
            return $this->response;
        }
    }

    private function getManufacturerList($key_field)
    {
        $this->loadModel('Manufacturers');
        $field_name = 'name' . $this->local;
        $data = $this->Manufacturers->getManufacturerList($field_name, $key_field);

        return $data;
    }

    private function getCurrenciesList()
    {
        $this->loadModel('Currencies');
        $data = $this->Currencies->getDropdown('list');

        return $data->toArray();
    }

    private function ajaxRequest()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->viewBuilder()->layout('ajax');
        $this->autoRender = false;
    }

    private function ajaxResponse($status, $message, $data)
    {
        echo json_encode([
            'status' => $status,
            'message' => $message,
            'data' => $data,
        ]);
    }

    private function getErrors($error = null, $request)
    {
        $data = [];
        if ($error) {
            foreach ($error as $key => $value) {
                $data[$key] = reset($value);
            }
        }
        if ($request['standard_price']) {
            foreach ($request['standard_price'] as $key => $value) {
                if ($value == '') {
                    $data['standard_price'][$key] = __('TXT_MESSAGE_REQUIRED');
                } else if ($value <= 0) {
                    $data['standard_price'][$key] = __('TXT_MESSAGE_VALUE_GREATER_THAN_ZERO');
                }
            }
        }
        if ($request['currency_id']) {
            $unique = array_unique($request['currency_id']);
            $duplicates = array_diff_assoc($request['currency_id'], $unique);

            foreach ($request['currency_id'] as $key => $value) {
                if ($value == '') {
                    $data['currency_id'][$key] = __('TXT_MESSAGE_REQUIRED');
                } else {
                    if ($duplicates) {
                        foreach ($duplicates as $key2 => $value2) {
                            $data['currency_id'][$key2] = __('TXT_MESSAGE_INVALID_DUPLICATE_VALUE');
                        }
                    }
                }
            }
        }

        return $data;
    }

    private function getData($request, $list = null)
    {
        $data = [];
        $new_data1 = [];
        $new_data2 = [];
        $old_data1 = [];
        $old_data2 = [];

        if ($list) {
            foreach ($list as $key1 => $value1) {
                $old_data1[$key1] = $value1->standard_price;
                $old_data2[$key1] = $value1->currency_id;
            }
            $new_data1 = array_diff_key($request['standard_price'], $old_data1);
            $new_data2 = array_diff_key($request['currency_id'], $old_data2);

            foreach ($new_data1 as $key2 => $value2) {
                $data[] = [
                    'product_detail_id' => $request['product_detail_id'],
                    'standard_price' => $value2,
                    'currency_id' => $new_data2[$key2],
                ];
            }
        } else {
            if ($request['standard_price']) {
                foreach ($request['standard_price'] as $key => $value) {
                    $data[] = [
                        'product_detail_id' => $request['product_detail_id'],
                        'standard_price' => $value,
                        'currency_id' => $request['currency_id'][$key],
                    ];
                }
            }
        }

        return $data;
    }

    private function nameEnOrJp($name_jp, $name_en, $en = '')
    {
        $new_name = '';
        if ($en == '_en') {
            $new_name = $name_en;
            if ($new_name == '') {
                $new_name = $name_jp;
            }
        } else {
            $new_name = $name_jp;
            if ($new_name == '') {
                $new_name = $name_en;
            }
        }

        return h($new_name);
    }
}
