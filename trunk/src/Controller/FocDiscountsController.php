<?php
namespace App\Controller;

use App\Controller\AppController;

class FocDiscountsController extends AppController
{
    public function initialize()
    {
        parent::initialize();
    }

    public function create()
    {
        $discount = $this->FocDiscounts->newEntity();
        if ($this->request->is('ajax')) {
            $this->autoRender = false;
            $this->response->disableCache();
            $this->response->type('json');
            $this->viewBuilder()->layout(false);

            $discount = $this->FocDiscounts->patchEntity($discount, $this->request->data);
            if ($this->FocDiscounts->save($discount)) {
                $this->response->body(json_encode(['status' => 1]));
                return $this->response;
            }

            $this->response->body(json_encode(['status' => 0, 'message' => $discount->errors()]));
            return $this->response;
        }

        $this->set(compact('discount'));
        $this->set('_serialize', ['discount']);
    }

    public function edit()
    {
        if ($this->request->is('ajax')) {
            $this->autoRender = false;
            $this->response->disableCache();
            $this->response->type('json');
            $this->viewBuilder()->layout(false);

            $discount = $this->FocDiscounts->get($this->request->data['id']);
            $discount = $this->FocDiscounts->patchEntity($discount, $this->request->data);
            if ($this->FocDiscounts->save($discount)) {
                $this->response->body(json_encode(['status' => 1]));
                return $this->response;
            }

            $this->response->body(json_encode(['status' => 0, 'message' => $discount->errors()]));
            return $this->response;
        }
    }

    public function delete()
    {
        if ($this->request->is('ajax')) {
            $this->autoRender = false;
            $this->response->disableCache();
            $this->response->type('json');
            $this->viewBuilder()->layout(false);

            $this->request->allowMethod(['post', 'delete']);
            $discount = $this->FocDiscounts->get($this->request->data['id']);
            if ($this->FocDiscounts->delete($discount)) {
                $this->response->body(json_encode(['status' => 1]));
                return $this->response;
            }
            $this->response->body(json_encode(['status' => 0, 'message' => __('TXT_DELETE_TROUBLE')]));
            return $this->response;
        }
    }

    public function discountList()
    {
        if ($this->request->is('ajax')) {
            $external_id = $this->request->query('external_id');
            $type = $this->request->query('type');

            $this->loadModel('FocDiscounts');
            $discounts = $this->FocDiscounts->find()
                ->contain([
                    'ProductDetails' => [
                        'queryBuilder' => function ($q) {
                            return $q->where(['ProductDetails.is_suspend' => 0]);
                        },
                        'Products' => function ($q) {
                            return $q->where(['Products.is_suspend' => 0]);
                        },
                        'SingleUnits',
                        'PackSizes',
                    ],
                ])
                ->where([
                    'FocDiscounts.external_id' => $external_id,
                    'FocDiscounts.type' => $type
                ]);
//pr($discounts->toArray()); exit;
                $this->loadModel('Products');
                $detail_list = $this->Products->productDetailsDropdown();
            $en = $this->request->session()->read('tb_field');
            $this->set(compact('discounts', 'detail_list', 'en', 'type', 'external_id'));
            $this->set('_serialize', ['discounts', 'detail_list', 'en', 'type', 'external_id']);
        }
        $this->viewBuilder()->layout('ajax');
    }
}