<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\Network\Exception\NotFoundException;

class MedicalCorporationsController extends AppController
{
    public function initialize()
    {
        parent::initialize();
        $this->loadModel('Countries');
        $this->loadModel('EntityNames');
        $this->loadModel('InfoDetails');
    }

    public function index()
    {
        $options = [];
        if ($this->request->query('keyword')) {
            $keyword = trim($this->request->query('keyword'));
            $options[] = [
                'or' => [
                    'name_en LIKE' => '%' .$keyword .'%',
                    'name LIKE' => '%' .$keyword .'%'
                ]
            ];
        }
        $displays = 10;
        if ($this->request->query('displays')) {
            $displays = $this->request->query('displays');
        }
        $this->paginate = [
            'conditions' => $options,
            'limit' => $displays,
            'order' => ['name_en' => 'asc'],
            'contain' => [
                'PersonInCharges' => function($q) {
                    return $q->where(['PersonInCharges.type' => TYPE_MEDICAL_CORP]);
                }
            ]
        ];
        $data = [
            'displays' => $displays,
            'data' => $this->paginate($this->MedicalCorporations),
            'paging' => $this->request->param('paging')['MedicalCorporations']['pageCount'],
            'invoice_list' => $this->getInvoiceDeliveryList()->toArray(),
            'payment_list' => $this->getPaymentCategoriesList()->toArray(),
        ];

        $this->set($data);
    }

    public function create()
    {
        $this->loadModel('InvoiceDeliveryMethods');
        $this->loadModel('PaymentCategories');

        $medical_corp = $this->MedicalCorporations->newEntity();
        if ($this->request->is('ajax')) {
            $this->autoRender = false;
            $this->response->disableCache();
            $this->response->type('json');
            $this->viewBuilder()->layout('ajax');

            $medical_corp = $this->MedicalCorporations->patchEntity($medical_corp, $this->request->data);
            $result = $this->MedicalCorporations->save($medical_corp, ['associated' => ['InfoDetails']]);
            if ($result) {
                $this->response->body(json_encode(['status' => 1]));
                return $this->response;
            }

            $this->response->body(json_encode(['status' => 0, 'message' => $medical_corp->errors()]));
            return $this->response;
        }
        $en = $this->request->session()->read('tb_field');

        $payment_list = $this->PaymentCategories->payDropdown($en);//$this->getPaymentCategoriesList();
        $invoice_list = $this->InvoiceDeliveryMethods->invoiceDropdown($en); //$this->getInvoiceDeliveryList();

        $countries = $this->Countries->getListOfCountries($this->locale);
        $breadcrumbs = $this->breadCrumb();
        $txt_register = $this->Common->txtRegister();

        $this->set(compact('medical_corp', 'payment_list', 'invoice_list', 'breadcrumbs', 'txt_register', 'countries'));
        $this->set('_serialize', ['medical_corp']);
    }

    public function edit($id = null)
    {
        $medical_corp = $this->MedicalCorporations->find()
            ->where(['MedicalCorporations.id' => $id])
            ->contain([
                'InfoDetails' => function($q) use ($id) {
                    return $q->where([
                        'InfoDetails.type' => TYPE_MEDICAL_CORP,
                        'InfoDetails.external_id' => $id,
                    ]);
                }
            ])
            ->all();

        if ($this->request->is('ajax')) {
            $this->autoRender = false;
            $this->response->disableCache();
            $this->response->type('json');
            $this->viewBuilder()->layout(false);

            $medical_corp = $this->MedicalCorporations->patchEntity($medical_corp, $this->request->data);
            if (empty($medical_corp->errors())) {
                $this->loadModel('InfoDetails');
                $this->InfoDetails->deleteAll(['external_id' => $id, 'type' => TYPE_MEDICAL_CORP]);
                if ($this->MedicalCorporations->save($medical_corp, ['associated' => ['InfoDetails']])) {
                    $this->response->body(json_encode(['status' => 1]));
                    return $this->response;
                }
            }

            $this->response->body(json_encode(['status' => 0, 'message' => $medical_corp->errors()]));
            return $this->response;
        }
        $en = $this->request->session()->read('tb_field');

        $this->loadModel('InvoiceDeliveryMethods');
        $this->loadModel('PaymentCategories');
        $payment_list = $this->PaymentCategories->payDropdown($en);//$this->getPaymentCategoriesList();
        $invoice_list = $this->InvoiceDeliveryMethods->invoiceDropdown($en); //$this->getInvoiceDeliveryList();

        $countries = $this->Countries->getListOfCountries($this->locale);
        $txt_edit = $this->Common->txtEdit();
        $breadcrumbs = $this->breadCrumb();

        $this->set(compact('medical_corp', 'payment_list', 'invoice_list', 'breadcrumbs', 'txt_edit', 'countries'));
        $this->set('_serialize', ['medical_corp']);
    }

    public function view($id = null)
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->viewBuilder()->layout('ajax');
        $data = $this->MedicalCorporations->get($id, [
            'contain' => [
                'Countries',
                'InfoDetails' => function($q) {
                    return $q->where([
                        'InfoDetails.type' => TYPE_MEDICAL_CORP,
                   ]);
                },
                'PersonInCharges' => function($q) {
                   return $q->where([
                        'PersonInCharges.type' => TYPE_MEDICAL_CORP,
                   ]);
                }
            ]
        ]);

        $payment_name = $this->getPaymentCategoriesList()->toArray();
        $invoice_name = $this->getInvoiceDeliveryList()->toArray();
        $data['payment_name'] = $payment_name[$data->payment_category_id];
        $data['invoice_name'] = $invoice_name[$data->invoice_delivery_method_id];

        $en = $this->request->session()->read('tb_field');
        $name = 'name' . $en;
        $this->set('data', $data);
        $this->set('name', $name);
    }

    public function getPaymentCategoriesList()
    {
        $this->loadModel('PaymentCategories');
        $data = $this->PaymentCategories->find('list', [
            'keyField' => 'id',
            'valueField' => 'name',
        ]);

        return $data;
    }

    public function getInvoiceDeliveryList()
    {
        $this->loadModel('InvoiceDeliveryMethods');
        $data = $this->InvoiceDeliveryMethods->find('list', [
            'keyField' => 'id',
            'valuefield' => 'name',
        ]);

        return $data;
    }

    public function getCountryList()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->viewBuilder()->layout('ajax');
        $this->autoRender = false;
        $data = $this->Countries->getList();

        if ($data) {
            $this->response->type('json');
            $this->response->body(json_encode([
                'status' => 1,
                'message' => 'success',
                'data' => $data,
            ]));
            return $this->response;
        }
    }

    public function delete()
    {
        if ($this->request->is('ajax')) {
            $this->autoRender = false;
            $this->response->disableCache();
            $this->response->type('json');
            $this->viewBuilder()->layout(false);

            $this->request->allowMethod(['post', 'delete']);
            $data = $this->MedicalCorporations->get($this->request->data['id']);
            if ($this->MedicalCorporations->delete($data)) {
                $this->response->body(json_encode(['status' => 1]));
                return $this->response;
            }
            $this->response->body(json_encode(['status' => 0, 'message' => __('TXT_DELETE_TROUBLE')]));
            return $this->response;
        }
    }

    public function updateSuspend()
    {
        if ($this->request->is('ajax')) {
            $this->autoRender = false;
            $this->response->disableCache();
            $this->response->type('json');
            $this->viewBuilder()->layout(false);

            $data = $this->MedicalCorporations->get($this->request->data['id']);
            $data = $this->MedicalCorporations->patchEntity($data, $this->request->data, ['validate' => false]);
            if ($this->MedicalCorporations->save($data)) {
                $this->response->body(json_encode([
                    'status' => 1,
                    'message' => 'success'
                ]));
                return $this->response;
            }

            $this->response->body(json_encode([
                'status' => 0,
                'message' => 'error'
            ]));
            return $this->response;
        }
    }

    /**
    * get all medical_corporation name and name_en by search auto complete
    * @return object
    */
    public function getMedialCorBySearch()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->viewBuilder()->layout('ajax');
        $this->autoRender = false;
        $this->response->type('json');
        $keyword = $this->request->query('keyword');

        $medicalCorations = $this->MedicalCorporations->find()
            ->select(['name', 'name_en'])
            ->where([
                'AND' => [
                    'is_suspend' => 0,
                    'OR' => [
                        'name_en LIKE' => '%' . $keyword . '%',
                        'name LIKE' => '%' . $keyword . '%',
                    ]
                ]
            ])
            ->limit(20)
            ->all();

        $this->response->body(json_encode([
            'status' => '1',
            'message' => MSG_SUCCESS,
            'data' => $medicalCorations,
        ]));
        return $this->response;
    }

    /**
     * @return typeinitialize breadcrumb inside initialize() not translate
     * @return string
     */
    private function breadCrumb() {
        return $this->Common->txtConfig() .
            '&nbsp;' . $this->Common->txtSuppliers() .
            '&nbsp;' . $this->Common->txtClinicMgt() .
            '&nbsp;' . __('TXT_MEDICAL_CORPORATIONS');
    }
}
