<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Network\Exception\NotFoundException;

class PaymentsController extends AppController
{
    private $local;

    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('FileUpload');
        $this->local = $this->request->session()->read('tb_field');
    }

    public function index()
    {
        $this->loadModel('Sales');
        $conditions = [];
        $keyword = '';
        $supplier = $this->request->query('supplier');
        $affiliation_class = $this->request->session()->read('user_group.affiliation_class');

        if (!empty($this->request->query('keyword'))) {
            $keyword = trim($this->request->query('keyword'));
            $conditions['Payments.payment_code LIKE '] = '%' . $keyword . '%';
        }

        if (!empty($this->request->query('currency'))) {
            $conditions['Currencies.id'] = $this->request->query('currency');
        }

        if (!empty($this->request->query('date'))) {
            $date = date_create($this->request->query('date'));
            $conditions['YEAR(Payments.payment_date)'] = date_format($date, 'Y');
            $conditions['MONTH(Payments.payment_date)'] = date_format($date, 'm');
        }

        $displays = PAGE_NUMBER;
        if (!empty($this->request->query('displays'))) {
            $displays = $this->request->query('displays');
        }

        if ($this->request->query('inactive')) {
            $conditions['Payments.is_suspend'] = 1;
        }

        $this->paginate = [
            'conditions' => $conditions,
            'contain' => [
                'Currencies',
                'SalePayments' => [
                    'queryBuilder' => function ($q) {
                        return $q->group('SalePayments.payment_id');
                    },
                    'Sales' => [
                        'queryBuilder' => function ($q) use($keyword) {
                            $q->select([
                                'id',
                                'seller_id',
                                'amount',
                                'sale_number',
                                'total_amount' => $q->func()->sum('amount'),
                                'count_record' => $q->func()->count('*'),
                            ])
                            ->group('Sales.seller_id');

                            if (!empty($keyword)) {
                                $q->where([
                                    'Sales.sale_number LIKE ' => '%' . $keyword . '%',
                                ]);
                            }
                            return $q;
                        },
                        'Sellers' => [
                            'queryBuilder' => function ($q) use($supplier) {
                                if (!empty($supplier)) {
                                    $q->where([
                                        'Sellers.id' => $supplier,
                                    ]);
                                }
                                return $q;
                            },
                            'Suppliers' => function ($q) use($keyword) {
                                if (!empty($keyword)) {
                                    $q->where([
                                        'Suppliers.name LIKE ' => '%' . $keyword . '%',
                                    ]);
                                }
                                return $q;
                            },
                            'Manufacturers' => function ($q) use($keyword) {
                                if (!empty($keyword)) {
                                    $q->where([
                                        'Manufacturers.name LIKE ' => '%' . $keyword . '%',
                                    ]);
                                }
                                return $q;
                            }
                        ]
                    ],
                ]
            ],
            'order' => ['Payments.created' => 'DESC'],
            'limit' => $displays,
        ];
        $payments = $this->paginate($this->Payments);

        $months = $this->Sales->getSaleIssueDate($affiliation_class, $this->local);
        $suppliers = $this->Sales->sellerDropdown();
        $data = [
            'payments' => $payments,
            'suppliers' => $suppliers,
            'months' => $months,
            'local' => $this->local,
            'displays' => $displays,
        ];
        $this->set($data);
    }

    public function create()
    {
        $this->loadModel('Purchases');
        $this->loadModel('Currencies');
        $this->loadModel('Sales');
        $payment = $this->Payments->newEntity();
        $affiliation_class = $this->request->session()->read('user_group.affiliation_class');

        if ($this->request->is('ajax')) {
            $this->autoRender = false;
            $this->response->disableCache();
            $this->response->type('json');
            $this->viewBuilder()->layout(false);
            $request = $this->request->data;

            $assoc[] = 'SalePayments';
            $assoc[] = 'Currencies';
            $associated['associated'] = $assoc;

            if (
                !empty($request['additional_payments'][0]['description']) ||
                !empty($request['additional_payments'][0]['amount'])
            ) {
                $assoc[] = 'AdditionalPayments';
            }

            $additional_error = $this->validateAdditionalAmount($request);

            if (isset($request['medias']) && !empty($request['medias'])) {
                for ($i = 0; $i < count($request['medias']); $i++) {
                    if (!empty($request['medias'][$i]['document_type']) && !empty($request['medias'][$i]['file_name'])) {
                        $request['medias'][$i]['type'] = TYPE_PAYMENT;
                        $name = explode('.', $request['medias'][$i]['file_name']);
                        $request['medias'][$i]['file_type'] = $name[1];
                        $request['medias'][$i]['is_deleted'] = 0;
                        $request['medias'][$i]['file_priority'] = $i;
                        $request['medias'][$i]['file_order'] = $i;
                        $request['medias'][$i]['permission'] = 'all';
                        $request['medias'][$i]['status'] = 0;
                    }
                }

                // prevent error in case just add file but not upload
                if (!empty($request['medias'][0]['file_name'])) {
                    $assoc[] = 'Medias';
                }
            }

            $payment = $this->Payments->patchEntity($payment, $request, $associated);
            if (empty($payment->errors()) && empty($additional_error)) {
                $this->Payments->save($payment, ['validate' => false]);
                $this->updateSalePaid($request);
                $this->response->body(json_encode(['status' => 1]));
                return $this->response;
            }

            $all_errors = array_merge(['additional' => $additional_error], $payment->errors());

            $this->response->body(json_encode(['status' => 0, 'message' => $all_errors]));
            return $this->response;
        }

        $purchases = $this->Purchases->purchaseDropdown($affiliation_class);
        $currencies = $this->Currencies->getDataList('id', 'code', null, $this->local);
        $suppliers = $this->Sales->sellerDropdown();
        $months = $this->Sales->getSaleIssueDate($affiliation_class, $this->local);

        $txt_register = $this->Common->txtRegister();
        $files = $this->CustomConstant->pdfDocumentType();

        // set data to view layout
        $data = [
            'payment' => $payment,
            'txt_register' => $txt_register,
            'currencies' => $currencies,
            'purchases' => $purchases,
            'files' => $files,
            'suppliers' => $suppliers,
            'months' => $months,
            'local' => $this->local,
        ];

        $this->set($data);
    }

    //Temporary not use.
    public function edit($id = null)
    {
        $this->loadModel('Purchases');
        $this->loadModel('Currencies');
        $this->loadModel('Sales');
        $affiliation_class = $this->request->session()->read('user_group.affiliation_class');

        $payment = $this->Payments->getByField($id, null, [
            'AdditionalPayments',
            'Medias',
            'SalePayments' => [
                'Sales' => ['Currencies']
            ]
        ]);

        if ($this->request->is('ajax')) {
            $this->autoRender = false;
            $this->response->disableCache();
            $this->response->type('json');
            $this->viewBuilder()->layout(false);
            $request = $this->request->data;

            $assoc[] = 'SalePayments';
            $assoc[] = 'Currencies';
            $associated['associated'] = $assoc;

            if (
                !empty($request['additional_payments'][0]['description']) ||
                !empty($request['additional_payments'][0]['amount'])
            ) {
                $assoc[] = 'AdditionalPayments';
            }
            $additional_error = $this->validateAdditionalAmount($request);

            if (isset($request['medias']) && !empty($request['medias'])) {
                for ($i = 0; $i < count($request['medias']); $i++) {
                    if (!empty($request['medias'][$i]['document_type']) && !empty($request['medias'][$i]['file_name'])) {
                        $request['medias'][$i]['type'] = TYPE_PAYMENT;
                        $name = explode('.', $request['medias'][$i]['file_name']);
                        $request['medias'][$i]['file_type'] = $name[1];
                        $request['medias'][$i]['is_deleted'] = 0;
                        $request['medias'][$i]['file_priority'] = $i;
                        $request['medias'][$i]['file_order'] = $i;
                        $request['medias'][$i]['permission'] = 'all';
                        $request['medias'][$i]['status'] = 0;
                    }
                }

                // prevent error in case just add file but not upload
                if (!empty($request['medias'][0]['file_name'])) {
                    $assoc[] = 'Medias';
                }
            }

            $payment = $this->Payments->patchEntity($payment, $request, $associated);
            if (empty($payment->errors()) && !empty($request['medias'][0]['file_name'])) {
                $this->loadModel('Medias');
                $this->loadModel('AdditionalPayments');
                $this->loadModel('SalePayments');
                $this->resetSalePaid($id);

                $this->Medias->deleteAll(['external_id' => $id, 'type' => TYPE_PAYMENT]);
                $this->AdditionalPayments->deleteAll(['payment_id' => $id]);
                $this->SalePayments->deleteAll(['payment_id' => $id]);
                $this->updateSalePaid($request);
                $this->Payments->save($payment, ['validate' => false]);
                $this->response->body(json_encode(['status' => 1]));
                return $this->response;
            }

            $all_errors = array_merge(['additional' => $additional_error], $payment->errors());
            $this->response->body(json_encode(['status' => 0, 'message' => $all_errors]));
            return $this->response;
        }

        $purchases = $this->Purchases->purchaseDropdown($affiliation_class);
        $currencies = $this->Currencies->getDataList('id', 'code', null, $this->local);
        $suppliers = $this->Sales->sellerDropdown();
        $months = $this->Sales->getSaleIssueDate($affiliation_class, $this->local);

        $txt_edit = $this->Common->txtEdit();
        $files = $this->CustomConstant->pdfDocumentType();

        // set data t0 view layout
        $data = [
            'payment' => $payment,
            'txt_edit' => $txt_edit,
            'currencies' => $currencies,
            'purchases' => $purchases,
            'files' => $files,
            'suppliers' => $suppliers,
            'months' => $months,
            'local' => $this->local,
        ];
        $this->set($data);
    }

    public function view($id = null)
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->viewBuilder()->layout('ajax');
        $payment = $this->Payments->getById($id, null, [
            'Medias',
            'AdditionalPayments',
            'Currencies',
            'SalePayments' => [
                'Sales' => [
                    'Sellers' => ['Suppliers', 'Manufacturers'],
                ]
            ],
        ]);

        $data = [
            'payment' => $payment,
            'local' => $this->local,
        ];
        $this->set($data);
    }

    public function indexPurchase()
    {
        $this->loadModel('PurchasePayments');
        $this->loadModel('Purchases');
        $conditions = [];
        $affiliation_class = $this->request->session()->read('user_group.affiliation_class');

        if (!empty($this->request->query('keyword'))) {
            $keyword = trim($this->request->query('keyword'));
            $conditions['OR'] = [
                'Purchases.purchase_number LIKE ' => '%' . $keyword . '%',
                'Payments.payment_code LIKE ' => '%' . $keyword . '%',
            ];
        }

        if (!empty($this->request->query('currency'))) {
            $conditions['Currencies.id'] = $this->request->query('currency');
        }

        if (!empty($this->request->query('supplier'))) {
            $conditions['Sellers.id'] = $this->request->query('supplier');
        }

        if (!empty($this->request->query('date'))) {
            $date = date_create($this->request->query('date'));
            $conditions['YEAR(Payments.payment_date)'] = date_format($date, 'Y');
            $conditions['MONTH(Payments.payment_date)'] = date_format($date, 'm');
        }

        if ($this->request->query('inactive')) {
            $conditions['Payments.is_suspend'] = 1;
        }
        $this->paginate = [
            'conditions' => $conditions,
            'contain' => [
                'Payments' => ['Currencies'],
                'Purchases' => [
                    'Sellers' => [
                        'Suppliers',
                        'Manufacturers'
                    ]
                ]
            ],
            'sortWhitelist' => [
                'Payments.payment_code',
                'Payments.payment_date',
                'Purchases.purchase_number',
                'Purchases.amount',
                'Payments.paid_amount',
                'Currencies.code'
            ],
            'order' => ['Payments.created' => 'DESC'],
            'limit' => PAGE_NUMBER
        ];
        $payments = $this->paginate($this->PurchasePayments);

        $months = $this->Purchases->getPurchaseIssueDate($affiliation_class, $this->local);
        $suppliers = $this->Purchases->sellerDropdown();

        // set data to view layout
        $data = [
            'payments' => $payments,
            'suppliers' => $suppliers,
            'months' => $months,
            'local' => $this->local,
        ];
        $this->set($data);
    }

    public function createPurchase()
    {
        $this->loadModel('Purchases');
        $this->loadModel('Currencies');

        $affiliation_class = $this->request->session()->read('user_group.affiliation_class');
        $payment = $this->Payments->newEntity();

        if ($this->request->is('ajax')) {
            $this->autoRender = false;
            $this->response->disableCache();
            $this->response->type('json');
            $this->viewBuilder()->layout(false);

            $request = $this->request->data;

            $assoc[] = 'PurchasePayments';
            $assoc[] = 'Currencies';
            $associated['associated'] = $assoc;

            if (!empty($request['additional_payments'][0]['description'])) {
                $assoc[] = 'AdditionalPayments';
            }

            if (isset($request['medias']) && !empty($request['medias'][0]['file_name'])) {
                $assoc[] = 'Medias';
                for ($i = 0; $i < count($request['medias']); $i++) {
                    $request['medias'][$i]['type'] = TYPE_PAYMENT;
                    $name = explode('.', $request['medias'][$i]['file_name']);
                    $request['medias'][$i]['file_type'] = $name[1];
                    $request['medias'][$i]['document_type'] = PDF_OTHERS;
                    $request['medias'][$i]['is_deleted'] = 0;
                    $request['medias'][$i]['file_priority'] = $i;
                    $request['medias'][$i]['file_order'] = $i;
                    $request['medias'][$i]['permission'] = 'all';
                    $request['medias'][$i]['status'] = 0;
                }
            }

            $payment = $this->Payments->patchEntity($payment, $request, $associated);
            if (empty($payment->errors())) {
                $this->updatePurchasePaid($request);
                $this->Payments->save($payment, ['validate' => false]);
                $this->response->body(json_encode(['status' => 1]));
                return $this->response;
            }

            $this->response->body(json_encode(['status' => 0, 'message' => $payment->errors()]));
            return $this->response;
        }

        $purchases = $this->Purchases->purchaseDropdown($affiliation_class);
        $currencies = $this->Currencies->getDataList('id', 'code', null, $this->local);
        $months = $this->Purchases->getPurchaseIssueDate($affiliation_class, $this->local);
        $suppliers = $this->Purchases->sellerDropdown();

        $txt_register = $this->Common->txtRegister();
        $files = $this->CustomConstant->pdfDocumentType();

        // set data to view layout
        $data = [
            'payment' => $payment,
            'txt_register' => $txt_register,
            'currencies' => $currencies,
            'purchases' => $purchases,
            'files' => $files,
            'suppliers' => $suppliers,
            'months' => $months,
            'local' => $this->local,
        ];
        $this->set($data);
    }

    public function editPurchase($id = null)
    {
        $this->loadModel('Purchases');
        $this->loadModel('Currencies');
        $payment = $this->Payments->getById($id, null, [
            'AdditionalPayments',
            'Medias',
            'PurchasePayments' => ['Purchases']
        ]);
        $affiliation_class = $this->request->session()->read('user_group.affiliation_class');

        if ($this->request->is('ajax')) {
            $this->autoRender = false;
            $this->response->disableCache();
            $this->response->type('json');
            $this->viewBuilder()->layout(false);
            $request = $this->request->data;

            $assoc[] = 'PurchasePayments';
            $assoc[] = 'Currencies';
            $associated['associated'] = $assoc;

            if (!empty($request['additional_payments'][0]['description'])) {
                $assoc[] = 'AdditionalPayments';
            }

            if (isset($request['medias']) && !empty($request['medias'][0]['file_name'])) {
                $assoc[] = 'Medias';
                for ($i = 0; $i < count($request['medias']); $i++) {
                    $request['medias'][$i]['type'] = TYPE_PAYMENT;
                    $name = explode('.', $request['medias'][$i]['file_name']);
                    $request['medias'][$i]['file_type'] = $name[1];
                    $request['medias'][$i]['document_type'] = PDF_OTHERS;
                    $request['medias'][$i]['is_deleted'] = 0;
                    $request['medias'][$i]['file_priority'] = $i;
                    $request['medias'][$i]['file_order'] = $i;
                    $request['medias'][$i]['permission'] = 'all';
                    $request['medias'][$i]['status'] = 0;
                }
            }

            $payment = $this->Payments->patchEntity($payment, $request, $associated);
            if (empty($payment->errors())) {
                $this->resetPurchasePaid($id);
                $this->loadModel('Medias');
                $this->loadModel('AdditionalPayments');
                $this->loadModel('PurchasePayments');

                $this->Medias->deleteAll(['external_id' => $id, 'type' => TYPE_PAYMENT]);
                $this->AdditionalPayments->deleteAll(['payment_id' => $id]);
                $this->PurchasePayments->deleteAll(['payment_id' => $id]);
                $this->updatePurchasePaid($request);
                $this->Payments->save($payment, ['validate' => false]);
                $this->response->body(json_encode(['status' => 1]));
                return $this->response;
            }

            $this->response->body(json_encode(['status' => 0, 'message' => $payment->errors()]));
            return $this->response;
        }
        $purchases = $this->Purchases->purchaseDropdown($affiliation_class);
        $currencies = $this->Currencies->getDataList('id', 'code', null, $this->local);
        $months = $this->Purchases->getPurchaseIssueDate($affiliation_class, $this->local);
        $suppliers = $this->Purchases->sellerDropdown();

        $txt_edit = $this->Common->txtEdit();
        $files = $this->CustomConstant->pdfDocumentType();

        // set data to view layout
        $data = [
            'payment' => $payment,
            'currencies' => $currencies,
            'purchases' => $purchases,
            'files' => $files,
            'suppliers' => $suppliers,
            'months' => $months,
            'local' => $this->local,
            'txt_edit' => $txt_edit,
        ];
        $this->set($data);
    }

    /**
     * Sale list for payment register search and filter
     */
    public function saleList()
    {
        if ($this->request->is('ajax')) {
            $this->loadModel('Sales');

            $name = 'name' . $this->local;
            $conditions['Sales.is_suspend'] = 0;
            $conditions['Sales.is_paid_supplier'] = 0;
            $keyword = $this->request->query('keyword');

            if (!empty($keyword)) {
                $conditions['OR'] = [
                    ['Sales.sale_number LIKE ' => '%' . trim($keyword) . '%'],
                    ['Customers.' . $name . ' LIKE ' => '%' . trim($keyword) . '%']
                ];
            }

            $contains = [
                'Customers' => function ($q) {
                    return $q->where([
                        'Customers.is_suspend' => 0
                    ]);
                },
                'Currencies',
                'Sellers' => [
                    'Suppliers',
                    'Manufacturers'
                ]
            ];

            if (!empty($this->request->query('supplier'))) {
                $conditions['Sellers.id'] = $this->request->query('supplier');
            }

            if (!empty($this->request->query('currency'))) {
                $conditions['Sales.currency_id'] = $this->request->query('currency');
            }

            if (!empty($this->request->query('date'))) {
                $date = date_create($this->request->query('date'));
                $conditions['YEAR(Sales.issue_date)'] = date_format($date, 'Y');
                $conditions['MONTH(Sales.issue_date)'] = date_format($date, 'm');
            }
            $query = $this->Sales->getAllDataList('all', null, $conditions, $contains);
            $sales = $this->paginate($query);
            $this->set(compact('sales', 'name'));
        }
        $this->viewBuilder()->layout('ajax');
    }

    /**
     * sale index auto complete data
     * @return json
     */
    public function getList()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->autoRender = false;
        $this->response->disableCache();
        $this->response->type('json');
        $this->viewBuilder()->layout(false);
        $this->loadModel('SalePayments');

        $data = $this->SalePayments->find('all', null, null, ['Payments', 'Sales'], null, 'Payments.id');

        if ($data) {
            $this->response->body(json_encode([
                'status' => 1,
                'message' => 'success',
                'data' => $data
            ]));
            return $this->response;
        }

        $this->response->body(json_encode([
            'status' => 1,
            'message' => 'success',
            'data' => []
        ]));
        return $this->response;
    }

    /**
     * Auto complete search and filter for sale payment register.
     * @return json
     */
    public function getSaleList()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->autoRender = false;
        $this->response->disableCache();
        $this->response->type('json');
        $this->viewBuilder()->layout(false);
        $this->loadModel('Sales');

        $data = $this->Sales->find('all')
            ->contain([
                'Customers' => function ($q) {
                    return $q->where([
                        'Customers.is_suspend' => 0
                    ]);
                }
            ])
            ->where(['Sales.is_paid_supplier' => 0]);
        if ($data) {
            $this->response->body(json_encode([
                'status' => 1,
                'message' => 'success',
                'data' => $data->toArray()
            ]));
            return $this->response;
        }

        $this->response->body(json_encode([
            'status' => 1,
            'message' => 'success',
            'data' => []
        ]));
        return $this->response;
    }

    /**
     * Temporary not use.
     * get update amount of sale products
     * @return json
     */
    public function getUpdateSaleAmount()
    {
        if ($this->request->is('ajax')) {
            $this->autoRender = false;
            $this->response->disableCache();
            $this->response->type('json');
            $this->viewBuilder()->layout(false);
            $sale_ids = $this->request->query('sale_ids');

            if ($sale_ids) {
                $this->loadModel('Sales');

                $ids = explode(',', rtrim($sale_ids, ','));
                $query = $this->Sales->find();
                $query->select(['product_amount' => $query->func()->sum('Sales.amount')]);
                $result = $query->where(['Sales.id IN ' => $ids])->toArray();

                if ($result) {
                    $this->response->body(json_encode([
                        'status' => 1,
                        'amount' => $result[0]->product_amount
                    ]));
                    return $this->response;
                }
                $this->response->body(json_encode(['status' => 1, 'amount' => 0]));
                return $this->response;
            }
            $this->response->body(json_encode(['status' => 0]));
            return $this->response;
        }
    }

    /**
     * puchase list filter for register purchase payment
     * @throws NotFoundException
     */
    public function purchaseList()
    {
        if ($this->request->is('ajax')) {
            $this->loadModel('Purchases');

            $name = 'name' . $this->local;
            $conditions['Purchases.is_suspend'] = 0;
            //$conditions['Purchases.is_paid_supplier'] = 0;

            if (!empty($this->request->query('supplier'))) {
                $conditions['Sellers.id'] = $this->request->query('supplier');
            }

            if (!empty($this->request->query('currency'))) {
                $conditions['Purchases.currency_id'] = $this->request->query('currency');
            }

            if (!empty($this->request->query('date'))) {
                $date = date_create($this->request->query('date'));
                $conditions['YEAR(Purchases.issue_date)'] = date_format($date, 'Y');
                $conditions['MONTH(Purchases.issue_date)'] = date_format($date, 'm');
            }

            $query = $this->Purchases->find()
                ->contain([
                    'Sellers' => [
                        'Suppliers',
                        'Manufacturers'
                    ],
                    'Currencies'
                ])
                ->where($conditions);
            $purchases = $this->paginate($query);
            $this->set(compact('purchases', 'name'));
        }
        $this->viewBuilder()->layout('ajax');
    }

    public function getPurchaseList()
    {
        if ($this->request->is('ajax')) {
            if ($this->request->is('ajax')) {
                $this->autoRender = false;
                $this->response->disableCache();
                $this->response->type('json');
                $this->viewBuilder()->layout(false);
                $this->loadModel('PurchasePayments');

                $data = $this->PurchasePayments->find()
                    ->contain([
                        'Payments',
                        'Purchases'
                    ])
                    ->group('Payments.id');

                if ($data) {
                    $this->response->body(json_encode([
                        'status' => 1,
                        'message' => 'success',
                        'data' => $data->toArray()
                    ]));
                    return $this->response;
                }

                $this->response->body(json_encode([
                    'status' => 1,
                    'message' => 'success',
                    'data' => []
                ]));
                return $this->response;
            }
        }
    }

    /**
     * get update amount of purchase products
     * @return json
     */
    public function getUpdatePurchaseAmount()
    {
        if ($this->request->is('ajax')) {
            $this->autoRender = false;
            $this->response->disableCache();
            $this->response->type('json');
            $this->viewBuilder()->layout(false);
            $purchase_ids = $this->request->query('purchase_ids');

            if ($purchase_ids) {
                $this->loadModel('Purchases');

                $ids = explode(',', rtrim($purchase_ids, ','));
                $query = $this->Purchases->find();
                $query->select(['product_amount' => $query->func()->sum('Purchases.amount')]);
                $result = $query->where(['Purchases.id IN ' => $ids])->toArray();

                if ($result) {
                    $this->response->body(json_encode([
                        'status' => 1,
                        'amount' => $result[0]->product_amount,
                    ]));
                    return $this->response;
                }
                $this->response->body(json_encode(['status' => 1, 'amount' => 0]));
                return $this->response;
            }
            $this->response->body(json_encode(['status' => 0]));
            return $this->response;
        }
    }

    public function delete()
    {
        if ($this->request->is('ajax')) {
            $this->autoRender = false;
            $this->response->disableCache();
            $this->response->type('json');
            $this->viewBuilder()->layout(false);

            $this->request->allowMethod(['post', 'delete']);
            $this->loadModel('Medias');

            $pay_id = $this->request->data['id'];
            $payment = $this->Payments->get($pay_id);
            $medias = $this->Medias->getPaymentFile($pay_id);
            $files = [];

            if ($medias) {
                $t = 0;
                // check for existing pdf file before it is deleted
                foreach ($medias as $media) {
                    $files[$t] = $media->file_name;
                    $t++;
                }
            }

            // check for existing payment before it is deleted
            $data = $this->checkExistingPayment($pay_id);
            $sale_ids = [];
            if ($data) {
                $i = 0;
                //LOOP after delete. You get empty because the cascade delete completely delete object
                foreach ($data as $dt) {
                    $sale_ids[$i] = $dt->sale_id;
                    $i++;
                }
            }
            $payment = $this->Payments->get($pay_id);

            if ($this->Payments->delete($payment)) {
                //reset Sales is_paid_supplier to 0
                if ($sale_ids) {
                    $this->loadModel('Sales');
                    $this->Sales->resetSalePayment($sale_ids);
                }

                //remove pdf file from server
                if ($files) {
                    for ($i = 0; $i < count($files); $i++) {
                        $file = WWW_ROOT . 'img' . DS . 'uploads' . DS . 'payments' . DS . $files[$i];
                        if (file_exists($file)) {
                            unlink($file);
                        }
                    }
                }

                $this->response->body(json_encode(['status' => 1, 'message' => 'success']));
                return $this->response;
            }
            $this->response->body(json_encode(['status' => 0, 'message' => __('TXT_DELETE_TROUBLE')]));
            return $this->response;
        }
    }

    public function updateSuspend()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->autoRender = false;
        $this->response->disableCache();
        $this->response->type('json');
        $this->viewBuilder()->layout(false);
        $data = $this->Payments->get($this->request->data['id']);
        $data->is_suspend = ($this->request->data['is_suspend'] == 0 ) ? 1 : 0;
        if ($this->Payments->save($data)) {
            $this->response->body(json_encode([
                'status' => 1,
                'message' => 'success'
            ]));
            return $this->response;
        }
        $this->response->body(json_encode([
            'status' => 0,
            'message' => 'error'
        ]));
        return $this->response;
    }

    public function upload()
    {
        if ($this->request->is('ajax')) {
            $this->autoRender = false;
            $this->response->disableCache();
            $this->response->type('json');
            $this->viewBuilder()->layout(false);

            $request = $this->request->data;

            $mimeType = ['application/pdf'];
            $this->FileUpload->setMimeType($mimeType);
            $extension = ['pdf'];
            $this->FileUpload->setFileExtension($extension);
            $destination = WWW_ROOT . 'img' . DS . 'uploads';
            if (!file_exists($destination)) {
                mkdir($destination, 0755, true);
            }
            $destination .= DS . 'payments';
            if (!file_exists($destination)) {
                mkdir($destination, 0755, true);
            }
            $this->FileUpload->setDestination($destination);

            $result = $this->FileUpload->upload($this->request->data);

            if ($result['error'] == 0) {
                if (!empty($this->request->data['old_file'])) {
                    $file = WWW_ROOT . 'img' . DS . 'uploads' . DS . 'payments' . DS . $this->request->data['old_file'];
                    if (file_exists($file)) {
                        unlink($file);
                    }
                }
                $this->response->body(json_encode([
                    'status' => 1,
                    'message' => $result['msg'],
                    'file' => [
                        'original_name' => $request['file']['name'],
                        'new_name' => $result['new_name']
                    ]
                ]));
                return $this->response;
            }

            $this->response->body(json_encode([
                'status' => 0,
                'message' => ['file' => $result['msg']]
            ]));
            return $this->response;
        }
    }

    public function customerToWisteria()
    {
        $this->loadModel('Doctors');
        $doctors = $this->Doctors->getList($this->local);
        $this->paginate = [
            'limit' => PAGE_NUMBER,
            'conditions' => '',
        ];
        $data = $this->paginate($this->Payments);

        $this->set(compact('data', 'doctors'));
    }

    /** View details of payment from customer to wisteria
     *
     * @param integer $id id of payment record
    */
    public function viewCustomerToWisteria($id)
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        if ($id) {
            $this->viewBuilder()->layout('ajax');
            $data = $this->Payments->findById($id)->toArray();
            $this->set('data', $data);
        } else {
            $this->autoRender = false;
            echo json_encode([
                'status' => 0,
                'message' => '',
                'data' => '',
            ]);
        }
    }

    /**
     * update is_paid_supplier=1 after the payment is done.
     * @param type $request
     */
    private function updateSalePaid($request)
    {
        $ids = [];
        for ($i = 0; $i < count($request['sale_payments']); $i++) {
            $ids[$i] = $request['sale_payments'][$i]['sale_id'];
        }
        $this->loadModel('Sales');
        $this->Sales->updateAll(
            ['is_paid_supplier' => 1],
            ['id IN ' => $ids]
        );
    }

    /**
     * reset the is_paid_supplier to 0 on payment update changes sale id
     * @param type $request
     */
    private function resetSalePaid($payment_id)
    {
        $this->loadModel('SalePayments');
        $sale = $this->SalePayments->find()
            ->where([
                'payment_id' => $payment_id
            ]);
        $ids = [];
        if ($sale) {
            $i = 0;
            foreach ($sale as $s) {
                $ids[$i] = $s->sale_id;
                $i++;
            }
        }
        if ($ids) {
            $this->loadModel('Sales');
            $this->Sales->updateAll(
                ['is_paid_supplier' => 0],
                ['id IN ' => $ids]
            );
        }
    }

    private function updatePurchasePaid($request)
    {
        $this->loadModel('Purchases');
        $ids = [];
        for ($i = 0; $i < count($request['purchase_payments']); $i++) {
            $ids[$i] = $request['purchase_payments'][$i]['purchase_id'];
        }
        // TODO: confirm with Nik san, is_paid_supplier is not exists
        // $this->Purchases->updateAll(
        //     // ['is_paid_supplier' => 1],
        //     ['id IN ' => $ids]
        // );
    }

    /**
     * reset the is_paid_supplier to 0 on payment update changes purchase id
     * @param type $request
     */
    private function resetPurchasePaid($payment_id)
    {
        $this->loadModel('PurchasePayments');
        $purchase = $this->PurchasePayments->find()
            ->where([
                'payment_id' => $payment_id
            ]);
        $ids = [];
        if ($purchase) {
            $i = 0;
            foreach ($purchase as $p) {
                $ids[$i] = $p->purchase_id;
                $i++;
            }
        }
        if ($ids) {
            $this->loadModel('Purchases');
            $this->Purchases->updateAll(
                // ['is_paid_supplier' => 0],
                ['id IN ' => $ids]
            );
        }
    }

    private function validateAdditionalAmount($request)
    {
        $count = count($request['additional_payments']);
        $error = [];
        if ($count > 0) {
            for ($i = 0; $i < $count; $i++) {
                if (!empty($request['additional_payments'][$i]['description'])) {
                    if (empty($request['additional_payments'][$i]['amount'])) {
                        $error['amount'][$i] = __('TXT_MESSAGE_REQUIRED');
                    } else {
                        if (!filter_var($request['additional_payments'][$i]['amount'], FILTER_VALIDATE_FLOAT) ||
                            !filter_var($request['additional_payments'][$i]['amount'], FILTER_VALIDATE_INT))
                        {
                            $error['amount'][$i] = __('TXT_MESSAGE_VALUE_NUMBER_ONLY');
                        }
                    }
                } else {
                    if (!empty($request['additional_payments'][$i]['amount'])) {
                        if (!filter_var($request['additional_payments'][$i]['amount'], FILTER_VALIDATE_FLOAT) ||
                            !filter_var($request['additional_payments'][$i]['amount'], FILTER_VALIDATE_INT)) {
                            $error['amount'][$i] = __('TXT_MESSAGE_VALUE_NUMBER_ONLY');
                        }
                    }
                }
            }
        }
        return $error;
    }

    private function checkExistingPayment($id)
    {
        $this->loadModel('SalePayments');
        $data = $this->SalePayments->find()
            ->contain([])
            ->where(['SalePayments.payment_id' => $id]);
        return $data;
    }

}
