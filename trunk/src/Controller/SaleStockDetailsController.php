<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Network\Exception\NotFoundException;

class SaleStockDetailsController extends AppController
{
    public function getData()
    {
        $this->ajaxRequest(true);
        $this->loadModel('SaleDetails');
        $saleDetailId = $this->request->query('id');
        $saleDetail= $this->SaleDetails->get($saleDetailId, [
            'contain' => ['Sales'],
        ]);
        $stocks = ['' => __('TXT_SELECT_STOCK')] + $this->getLotNumber();
        $data = $this->SaleStockDetails->getBySaleDetailId($saleDetailId);

        $this->set(compact('data', 'saleDetail', 'stocks'));
    }

    public function create()
    {
        $this->ajaxRequest(false);
        $errors = $this->getError($this->request->data);

        if ($errors) {
            $this->ajaxResponse(0, MSG_ERROR, $errors);
        } else {
            $sum_quantity = $this->sumQuantity($this->request->data);

            if (intval($this->request->data['sale_detail_quantity']) < $sum_quantity) {
                $this->ajaxResponse(3, MSG_WARNING);
            } else {
                $data = $this->getEntities($this->request->data);
                $data = $this->SaleStockDetails->newEntities($data);
                $this->SaleStockDetails->deleteBySaleDetailId($this->request->data['sale_detail_id']);

                if ($this->SaleStockDetails->saveMany($data)) {
                    $this->ajaxResponse(1, MSG_SUCCESS);
                }
            }
        }
    }

    /**
     * Function getLotNumber
     * use for get lot_number from Stocks Table
     * @return object
     */
    private function getLotNumber()
    {
        $this->loadModel('Stocks');
        $data = $this->Stocks->getListLotNumber();

        return $data;
    }

    /**
    * Function ajaxRequest
    * using for checking the request is ajax
    * @param boolean $auto_render assign value to $this->autoRender
    */
    private function ajaxRequest($auto_render = false)
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->viewBuilder()->layout('ajax');

        if ($auto_render === false) {
            $this->autoRender = false;
        }
    }

    /**
    * Function ajaxResponse
    * using for response json data
    * @param int $status assign value to status
    * @param string $message assign value to message
    * @param object $data optional
    * @return object response json data
    */
    private function ajaxResponse($status, $message, $data = null)
    {
        echo json_encode([
            'status' => $status,
            'message' => $message,
            'data' => $data,
        ]);
    }

    /**
     * Function getError
     * using for get error message
     * @param object $request get data from server request
     * @return object get error message and field name
     */
    private function getError($request)
    {
        $data = [];
        $stock_ids = $request['stock_id'];
        $quantities = $request['quantity'];
        $duplicates = array_unique(array_diff_assoc($stock_ids, array_unique($stock_ids)));

        if ($stock_ids) {
            foreach ($stock_ids as $key => $value) {
                if (empty($value)) {
                    $data['stock_id'][$key] = __('TXT_MESSAGE_REQUIRED');
                }
            }

            if ($duplicates) {
                foreach ($duplicates as $key => $value) {
                    if (!isset($data['stock_id'][$key])) {
                        $data['stock_id'][$key] = __('TXT_MESSAGE_INVALID_DUPLICATE_VALUE');
                    }
                }
            }
        }

        if ($quantities) {
            foreach ($quantities as $key => $value) {
                if ($value === '') {
                    $data['quantity'][$key] = __('TXT_MESSAGE_REQUIRED');
                } elseif ($value <= 0) {
                    $data['quantity'][$key] = __('TXT_MESSAGE_QTY_GREATER_THAN_ZERO');
                }
            }
        }

        return $data;
    }

    /**
     * Function getEntities
     * use for re-order array
     * @param object $request
     * @return array
     */
    private function getEntities($request)
    {
        $data = [];

        if ($request) {
            $stock_ids = $request['stock_id'];
            $quantities = $request['quantity'];

            foreach ($stock_ids as $key => $value) {
                $data[] = [
                    'stock_id' => $value,
                    'quantity' => $quantities[$key],
                    'sale_detail_id' => $request['sale_detail_id']
                ];
            }
        }

        return $data;
    }

    /**
     * Function sumQuantity
     * use for sum quantity of stock quantity
     * @param object $request
     * @return int
     */
    private function sumQuantity($request)
    {
        $data = 0;

        if ($request['quantity']) {
            foreach ($request['quantity'] as $key => $value) {
                $data += $value;
            }
        }

        return $data;
    }
}