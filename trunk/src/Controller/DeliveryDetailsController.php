<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Network\Exception\NotFoundException;

class DeliveryDetailsController extends AppController
{
    /**
    * @var string for store local session field
    */
    private $local;

    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('Function');
        $this->local = $this->request->session()->read('tb_field');
    }

    public function getFormCreate()
    {
        $this->ajaxRequest(true);
        $this->loadModel('SaleDetails');
        $this->loadModel('Deliveries');
        $this->loadModel('DeliveryDetails');
        $this->loadModel('Stocks');
        $deliveryDetails = [];
        $saleDetail= $this->SaleDetails->get($this->request->query('sale_detail_id'), [
            'contain' => ['Sales'],
        ]);
        $delivery = $this->Deliveries->findBySaleId($saleDetail->sale->id)->first();
        if ($delivery) {
            $deliveryDetails = $this->DeliveryDetails->findByProductDetailId($this->request->query('product_detail_id'));
        }
        $stocks = $this->Stocks->find('all')->contain(['ImportDetails']);
        $data = [
            'saleDetail' => $saleDetail,
            'stocks' => $stocks,
            'local' => $this->local,
            'delivery' => $delivery,
            'deliveryDetails' => $deliveryDetails,
        ];
        $this->set($data);
    }

    public function create()
    {
        $this->ajaxRequest(false);
        $this->response->type('json');
        $quantity = 0;
        $data = json_decode($this->request->data['data']);
        // check record for delete
        if ($this->request->data['delete']) {
            $this->deleteData($this->request->data['delete']);
        }
        // if data is not empty
        if ($data) {
            foreach ($data as $key => $value) {
                $quantity += (int)$value->quantity;
            }
        }
        // check is sale_details.quantity > quantity
        if ($quantity > $this->request->data['quantity']) {
            $this->response->body(json_encode([
                'status' => 0,
                'message' => MSG_ERROR,
                'data' => __('TXT_MESSAGE_QTY_GREATER_THAN_ZERO'),
            ]));
            return $this->response;
        }
        // if data is not emtpty, insert data to DB
        if ($data) {
            foreach ($data as $key => $value) {
                $this->saveAndUpdate($value);
            }
        }
        $this->response->body(json_encode([
            'status' => 1,
            'message' => MSG_SUCCESS,
        ]));
        return $this->response;
    }

    public function saveAndUpdate($repsonse)
    {
        // if have id is edit else save data
        if ($repsonse->id) {
            $data = $this->DeliveryDetails->get($repsonse->id);
        } else {
            $data = $this->DeliveryDetails->newEntity();
        }
        $data->stock_id = $repsonse->stock_id;
        $data->delivery_id = $repsonse->delivery_id;
        $data->quantity = $repsonse->quantity ? (int)$repsonse->quantity : 0;
        $data->lot = $repsonse->lot;
        $data->expiry_date = $repsonse->expiry_date;
        $data->product_detail_id = $repsonse->product_detail_id;
        $this->DeliveryDetails->save($data);
    }

    public function deleteData($response)
    {
        foreach ($response as $key => $value) {
            $data = $this->DeliveryDetails->get($value);
            $this->DeliveryDetails->delete($data);
        }
    }
    /**
    * Function ajaxRequest
    * using for checking the request is ajax
    * @param boolean $auto_render assign value to $this->autoRender
    */
    private function ajaxRequest($auto_render = false)
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->viewBuilder()->layout('ajax');
        $this->autoRender = $auto_render;
    }
}
