<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\Network\Exception\NotFoundException;

class ImportsController extends AppController
{
    public function initialize() {
        parent::initialize();
        $this->loadComponent('Function');
    }

    public function create()
    {
        $this->ajaxRequest(false);
        $data = $this->Imports->newEntity();
        $data = $this->Imports->patchEntity($data, $this->request->data, [
            'validate' => 'UpdateStatus',
        ]);

        if ($data->errors()) {
            $errors = $this->Function->getError($data->errors());
            $this->Function->ajaxResponse(0, MSG_ERROR, $errors);
        } else {
            // imports object
            $data1 = [
                'id' => $data->import_id,
                'affiliation_class' => $data->affiliation_class,
                'delivery_date' => $data->delivery_date,
                'tracking' => $data->tracking,
            ];

            // save data
            if ($data1['id'] == -1) {
                unset($data1['id']);
                $data1['purchase_imports'][0] = ['purchase_id' => $data->purchase_id];
                $data1 = $this->Imports->newEntity($data1, [
                    'associated' => ['PurchaseImports'],
                    'validate' => false,
                ]);
                $this->Imports->save($data1);
            } else {
                // edit data
                $entity = $this->Imports->get($data->import_id);
                $entity->tracking = $data['tracking'];
                $entity->delivery_date = $data['delivery_date'];

                $this->Imports->save($entity);
            }

            $this->Function->ajaxResponse(1, MSG_SUCCESS);
        }
    }

    /**
    * Function ajaxRequest
    * using for checking the request is ajax
    * @param boolean $auto_render assign value to $this->autoRender
    */
    private function ajaxRequest($auto_render = false)
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->viewBuilder()->layout('ajax');
        $this->autoRender = $auto_render;
    }
}
