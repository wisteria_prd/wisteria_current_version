<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Network\Exception\NotFoundException;

class UserGroupsController extends AppController
{
    /**
    * @var string for store local session field
    */
    private $local;

    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('Function');
        $this->local = $this->request->session()->read('tb_field');
    }

    public function index()
    {
        $limit = PAGE_NUMBER;
        // Checking limit record
        if ($this->request->query('displays')) {
            $limit = $this->request->query('displays');
        }
        $this->paginate = [
            'limit' => $limit,
        ];
        $userGroups = $this->Paginate($this->UserGroups);
        $data = [
            'local' => $this->local,
            'userGroups' => $userGroups,
        ];
        $this->set($data);
    }

    public function delete()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->viewBuilder()->layout('ajax');
        $this->autoRender = false;
        $this->response->type('json');
        $this->response->disableCache();
        $userGroup = $this->UserGroups->get($this->request->data['id']);
        // Save data
        if ($this->UserGroups->delete($userGroup)) {
            $this->response->body(json_encode([
                'status' => 1,
                'message' => MSG_SUCCESS,
            ]));
            return $this->response;
        }
    }

    /**
     * Using for create a new record/edit record
     * @return object
     */
    public function createAndUpdate()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->viewBuilder()->layout('ajax');
        $this->autoRender = false;
        $this->response->type('json');
        $this->response->disableCache();
        // Checking target of record
        $target = $this->request->data['target'];
        if ($target === TARGET_NEW) {
            $userGroup = $this->UserGroups->newEntity();
        } else {
            $userGroup = $this->UserGroups->get($this->request->data['id']);
        }
        $userGroup = $this->UserGroups->patchEntity($userGroup, $this->request->data);
        // Checking validations
        if ($userGroup->errors()) {
            $this->response->body(json_encode([
                'status' => 0,
                'message' => MSG_ERROR,
                'data' => $this->Function->getError($userGroup->errors()),
            ]));
            return $this->response;
        }
        // Save data
        if ($this->UserGroups->save($userGroup)) {
            $this->response->body(json_encode([
                'status' => 1,
                'message' => MSG_SUCCESS,
            ]));
            return $this->response;
        }
    }

    /**
     * Using get delete modal popup
     * @throws NotFoundException request  is not ajax
     */
    public function getDeleteForm()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->viewBuilder()->layout('ajax');
        $data = [
            'id' => $this->request->query('id'),
        ];
        $this->set($data);
    }

    /**
     * Using for get create form and edit
     * @return object
     */
    public function getForm()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->viewBuilder()->layout('ajax');
        $userGroup = [];
        if ($this->request->query('target') === TARGET_EDIT) {
            $userGroup = $this->UserGroups->get($this->request->query('id'));
        }
        $data = [
            'local' => $this->local,
            'target' => $this->request->query('target'),
            'userGroup' => $userGroup,
        ];
        $this->set($data);
    }

    public function getUserGroupByAffiliationClass()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->viewBuilder()->layout('ajax');
        $this->response->type('json');
        $this->response->disableCache();
        $this->autRender = false;
        $userGroups = $this->UserGroups->find()
                ->where(['affiliation_class' => $this->request->query('affiliation_class')])
                ->all();
        $this->response->body(json_encode([
            'status' => 1,
            'message' => MSG_SUCCESS,
            'data' => [
                'userGroups' => $userGroups,
            ],
        ]));
        return $this->response;
    }
}