<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\Datasource\ConnectionManager;
use Cake\Network\Exception\NotFoundException;

class SingleUnitsController extends AppController
{
    public function initialize() {
        parent::initialize();
    }

    public function create()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->viewBuilder()->layout('ajax');
        $this->autoRender = false;
        $data = $this->SingleUnits->newEntity();

        if ($this->request->data) {
            $data->name = $this->request->data['name'];
            $data->name_en = $this->request->data['name_en'];
            if ($this->SingleUnits->save($data)) {
                $this->response->type('json');
                $this->response->body(json_encode([
                    'status' => 1,
                    'message' => 'success',
                ]));
                return $this->response;
            }
        }
    }

    public function edit($id)
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->viewBuilder()->layout('ajax');
        $this->autoRender = false;

        if ($this->request->data) {
            $data = $this->SingleUnits->get($id);
            $data->name = $this->request->data['name'];
            $data->name_en = $this->request->data['name_en'];
            if ($this->SingleUnits->save($data)) {
                $this->response->type('json');
                $this->response->body(json_encode([
                    'status' => 1,
                    'message' => 'success',
                ]));
                return $this->response;
            }
        }
    }

    public function delete($id)
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->loadModel('ProductDetails');
        $this->viewBuilder()->layout('ajax');
        $this->autoRender = false;
        $message = [];

        $product_detail = $this->ProductDetails->find('all')
                ->where(['ProductDetails.single_unit_id' => $id])->toArray();
        if ($product_detail) {
            $message = [
                'status' => 0,
                'message' => 'Invalid',
            ];
        } else {
            $data = $this->SingleUnits->get($id);
            if ($this->SingleUnits->delete($data)) {
                $message = [
                    'status' => 1,
                    'message' => 'success',
                ];
            }
        }
        $this->response->type('json');
        $this->response->body(json_encode($message));
        return $this->response;
    }

    public function updateSuspend()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->autoRender = false;
        $this->response->disableCache();
        $this->response->type('json');
        $this->viewBuilder()->layout(false);
        $data = $this->SingleUnits->get($this->request->data['id']);
        $data->is_suspend = ($this->request->data['is_suspend'] == 0 ) ? 1 : 0;
        if ($this->SingleUnits->save($data)) {
            $this->response->body(json_encode([
                'status' => 1,
                'message' => 'success'
            ]));
            return $this->response;
        }
        $this->response->body(json_encode([
            'status' => 0,
            'message' => 'error'
        ]));
        return $this->response;
    }

    public function getList()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->viewBuilder()->layout('ajax');
        $this->autoRender = false;
        $data = $this->SingleUnits->find('all')->order(['SingleUnits.name_en' => 'asc'])->toArray();

        $this->response->type('json');
        $this->response->body(json_encode([
            'status' => 1,
            'message' => 'success',
            'data' => $data,
        ]));
        return $this->response;
    }
}
