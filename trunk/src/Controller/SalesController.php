<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Network\Exception\NotFoundException;

require ROOT . DS . 'src/Lib/tcpdf/tcpdf_config_alt.php';
require ROOT . DS . 'src/Lib/tcpdf/tcpdf.php';

class SalesController extends AppController
{
    private $local;

    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('CustomConstant');
        $this->loadComponent('ReconstructData');
        $this->loadComponent('Function');
        $this->local = $this->request->session()->read('tb_field');
    }

    public function index()
    {
        $this->loadModel('Sellers');
        $options = [];
        $options['AND']['Sales.type'] = TRADE_TYPE_SALE_BEHALF_MP;
        $seller_id = $this->Sellers->sellerIdAsMp();
        if (!is_null($seller_id)) {
            $options['AND']['Sales.seller_id'] = $seller_id;
        }
        if ($this->request->query('keyword')) {
            $keyword = trim($this->request->query('keyword'));
            $options['AND']['OR'] = [
                'Sales.rop_number LIKE' => '%' . $keyword . '%',
                'Sales.sale_number LIKE' => '%' . $keyword . '%',
            ];
        }
        $displays = PAGE_NUMBER;
        if ($this->request->query('displays')) {
            $displays = $this->request->query('displays');
        }
        $this->paginate = [
            'conditions' => $options,
            'limit' => $displays,
            'contain' => [
                'Customers',
                'Currencies',
                'Sellers' => ['Suppliers', 'Manufacturers'],
                'SaleDetails' => function ($q) {
                    $q->select([
                        'id',
                        'sale_id',
                        'total_amount' => $q->func()->sum('unit_price * quantity'),
                    ])->group(['sale_id']);
                    return $q;
                },
            ],
            'sortWhitelist' => [
                'status',
                'order_date',
                'rop_number',
                'sale_number',
                'Customers.name' . $this->local,
                'Sellers.id',
            ],
            'order' => [],
        ];
        $data = $this->paginate($this->Sales);
        $local = $this->local;
        $this->set(compact('data', 'local'));
    }

    public function createForm()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->viewBuilder()->layout('ajax');
        $this->loadModel('Countries');
        $data = $this->Sales->newEntity();
        $kind_of = $this->request->query('kind_of');
        $country_id = null;
        if ($kind_of == 'jp_only' || $kind_of == 'not_jp') {
            $country_id = $this->Countries->getJPCountryId();
        }
        $sellers = $this->getSeller($kind_of, $country_id, $this->local);
        $currencies = ['' => __('TXT_SELECT_CURRENCY')] + $this->getCurrency()->toArray();
        $data1 = [
            'data' => $data,
            'sellers' => $sellers,
            'kind_of' => $kind_of,
            'currencies' => $currencies,
        ];
        $this->set($data1);
    }

    public function getCustomerList()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->viewBuilder()->layout('ajax');
        $this->autoRender = false;
        $this->response->disableCache();
        $this->response->type('json');
        $this->loadModel('Customers');
        $keyword = $this->request->query('keyword');
        $customers = $this->Customers->getAllDataList('all', null, [
            'AND' => [
                'Customers.is_suspend' => 0,
                'OR' => [
                    'Customers.name_en LIKE' => '%' . $keyword . '%',
                    'Customers.name LIKE' => '%' . $keyword . '%',
                ]
            ],
        ], null, 20);
        $this->response->body(json_encode([
            'status' => 1,
            'message' => MSG_SUCCESS,
            'data' => $customers,
        ]));
        return $this->response;
    }

    public function registerSale($id = null)
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->viewBuilder()->layout('ajax');
        $this->response->type('json');
        $this->autoRender = false;
        $data = $this->Sales->newEntity();
        $request = [];
        parse_str($this->request->data['data'], $request);
        $validator = $this->Sales->patchEntity($data, $request);
        // check validation
        if ($validator->errors()) {
            $this->response->body(json_encode([
                'status' => 0,
                'message' => MSG_ERROR,
                'data' => $validator->errors(),
            ]));
            return $this->response;
        }
        $this->loadModel('Sellers');
        $this->loadModel('Customers');
        // get seller data
        list($name, $address) = $this->Sellers->getSeller($data->seller_id, $this->local);
        $data->seller_name = $name;
        $data->seller_address = $address;

        // get customer data
        list($c_name, $c_address) = $this->Customers->getCustomer($data->customer_id, $this->local);
        $data->customer_name = $c_name;
        $data->customer_address = $c_address;

        if (!$data->id) {
            $data->user_id = $this->Auth->user('id');
            $data->status = PO_STATUS_DRAFF;
            $data->affiliation_class = $this->request->session()->read('user_group.affiliation_class');
            $data->rop_number = $this->Sales->ropNumber($data->type);
        }
        if ($request['order_date']) {
            $order_date = date_create($request['order_date']);
            $data->order_date = date_format($order_date, 'Y-m-d');
        }
        if ($this->Sales->save($data)) {
            $this->response->body(json_encode([
                'status' => 1,
                'message' => MSG_SUCCESS,
                'data' => $data->id,
            ]));
            return $this->response;
        }
    }

    // CLEARN UP CODE

    public function create()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->viewBuilder()->layout('ajax');
        $this->autoRender = false;
        $this->response->type('json');
        //CHECK EXIST ID
        if ($this->request->data('id')) {
            $sale = $this->Sales->get($this->request->data['id']);
        } else {
            $sale = $this->Sales->newEntity();
            $sale->user_id = $this->Auth->user('id');
            $sale->status = PO_STATUS_DRAFF;
            $sale->affiliation_class = $this->request->session()->read('user_group.affiliation_class');
            $sale->rop_number = $this->Sales->ropNumber($this->request->data['type']);
        }
        $validator = $this->Sales->patchEntity($sale, $this->request->data);
        //CHECK VALIDATION
        if ($validator->errors()) {
            $this->response->body(json_encode([
                'status' => 0,
                'message' => MSG_ERROR,
                'data' => $this->Function->getErrors($validator->errors()),
            ]));
            return $this->response;
        }
        $this->loadModel('Sellers');
        $this->loadModel('Customers');
        //GET SELLER DATA
        list($name, $address) = $this->Sellers->getSeller($sale->seller_id, $this->local);
        $sale->seller_name = $name;
        $sale->seller_address = $address;
        //GET CUSTOMER DATA
        list($c_name, $c_address) = $this->Customers->getCustomer($sale->customer_id, $this->local);
        $sale->customer_name = $c_name;
        $sale->customer_address = $c_address;
        $order_date = date_create($this->request->data['order_date']);
        $sale->order_date = date_format($order_date, 'Y-m-d');
        //SAVE DATA
        if ($this->Sales->save($sale)) {
            $this->response->body(json_encode([
                'status' => 1,
                'message' => MSG_SUCCESS,
                'data' => $sale->id,
            ]));
        }
    }

    public function edit($id)
    {
        $this->ajaxRequest(true);
        $this->loadModel('Countries');
        $kind_of = $this->request->query('kind_of');
        $country_id = null;

        $data = $this->Sales->get($id);
        $currencies = ['' => __('TXT_SELECT_CURRENCY')] + $this->getCurrency()->toArray();

        if ($kind_of == 'jp_only' || $kind_of == 'not_jp') {
            $country_id = $this->Countries->getJPCountryId();
        }
        $sellers = $this->getSeller($kind_of, $country_id, $this->local);

        $this->set(compact('data', 'sellers', 'currencies', 'kind_of'));
        $this->render('get_data');
    }

    public function updateSuspend()
    {
        $this->ajaxRequest(false);
        $data = $this->Sales->get($this->request->data['id']);
        $data->is_suspend = ($this->request->data['is_suspend'] == 0 ) ? 1 : 0;

        if ($this->Sales->save($data)) {
            $this->Function->ajaxResponse(1, MSG_SUCCESS);
        }
    }

    public function vueUpdateSuspend()
    {
        $this->ajaxRequest(false);
        $data = $this->Sales->get($this->request->data['id']);
        $data->is_suspend = ($this->request->data['is_suspend'] === 'true') ? false : true;

        if ($this->Sales->save($data)) {
            $this->Function->ajaxResponse(1, MSG_SUCCESS);
        }
    }

    public function isDelete()
    {
        $this->ajaxRequest(false);
        $data = $this->Sales->get($this->request->data['id']);
        $data->is_deleted = 1;

        if ($this->Sales->save($data)) {
            $this->Function->ajaxResponse(1, MSG_SUCCESS);
        }
    }

    public function delete()
    {
        $this->ajaxRequest(false);
        $data = $this->Sales->get($this->request->data['id']);

        if ($this->Sales->delete($data)) {
            $this->Function->ajaxResponse(1, MSG_SUCCESS);
        }
    }

    public function getData()
    {
        $this->ajaxRequest(true);
        $this->loadModel('Countries');
        $kind_of = $this->request->query('kind_of');
        $country_id = null;

        if ($kind_of == 'jp_only' || $kind_of == 'not_jp') {
            $country_id = $this->Countries->getJPCountryId();
        }
        $sellers = $this->getSeller($kind_of, $country_id, $this->local);
        $currencies = ['' => __('TXT_SELECT_CURRENCY')] + $this->getCurrency()->toArray();

        $this->set(compact('sellers', 'currencies', 'kind_of'));
    }

    public function po($id = null)
    {
        if ($id === null) {
            throw new NotFoundException();
        }
        $this->loadModel('SaleDetails');
        $this->loadModel('Medias');
        $this->loadModel('SaleReceivePayments');
        $this->loadModel('SaleDeliveries');
        $person = [];

        // get sales data
        $sale = $this->Sales->getByField(['Sales.id' => $id], null, [
            'Currencies',
            'Customers',
            'SaleDeliveries' => ['Deliveries'],
            'Sellers' => [
                'Manufacturers' => ['InfoDetails', 'Countries'],
                'Suppliers' => ['InfoDetails', 'Countries'],
            ],
        ]);
        // get saleDetail with saleStockDetail
        $saleDetail = $this->SaleDetails->getAllDataList('threaded', null, ['SaleDetails.sale_id' => $id], [
            'SaleStockDetails' => function ($q) {
                $q->select([
                    'id',
                    'sale_detail_id',
                    'count' => $q->func()->count('*')
                ])
                ->group('sale_detail_id');
                return $q;
            },
        ]);
        // count child record where sale_id IS NOT null
        $countRecords = $this->SaleDetails->countRecords(['SaleDetails.sale_id' => $id]);
        // get payments
        $payments = $this->SaleReceivePayments->getSalePaymentListBySaleId($id);
        // get sale_deliveries
        $saleDeliveries = $this->SaleDeliveries->getByField(['SaleDeliveries.sale_id' => $id], null, ['Deliveries']);
        // get medias
        $medias = $this->Medias->getAllDataList('all', null, [
            'Medias.external_id' => $id,
            'Medias.type' => USER_ROLE_SALE,
        ]);

        if ($sale) {
            $this->loadModel('PersonInCharges');
            $ext = [];

            switch ($sale->seller->type) {
                case TYPE_SUPPLIER :
                    $ext = [
                        'type' => TYPE_SUPPLIER,
                        'id' => $sale->seller->supplier->id,
                    ];
                    break;
                default :
                    $ext = [
                        'type' => TYPE_MANUFACTURER,
                        'id' => $sale->seller->manufacturer->id
                    ];
            }
            $person = ['' => __('TXT_SELECT_CONTACT')] + $this->PersonInCharges->getList($this->local, $ext['type'], $ext['id']);
        }
        $typePdf = $this->CustomConstant->typePdf();

        // set data to view layout
        $data = [
            'typePdf' => $typePdf,
            'person' => $person,
            'saleDetail' => $saleDetail,
            'payments' => $payments,
            'medias' => $medias,
            'saleDeliveries' => $saleDeliveries,
            'countRecords' => $countRecords,
            'local' => $this->local,
            'sale' => $sale,
            'statusList' => $this->CustomConstant->saleStatus(),
        ];
        $this->set($data);

        // render view by customer type
        switch ($sale->customer->type) {
            case CUSTOMER_TYPE_NEW :
            case CUSTOMER_TYPE_BAD :
                $this->render('po_new');
                break;
            case CUSTOMER_TYPE_CONTRACT :
                $this->render('po_contract');
                break;
            default :
                $this->render('po_normal');
                break;
        }
    }

    public function statusDownload()
    {
        $this->ajaxRequest(false);
        $this->loadModel('Sales');
        $this->loadModel('Medias');

        $medias = [];
        $response = [];
        $countNewFile = 0;
        $path = WWW_ROOT . 'img' . DS . 'uploads' . DS . 'po';

        $data = $this->request->data;
        $this->set('_serialize', ['data']);

        if (isset($data['files'])) {
            $countNewFile = count($data['files']);

            // check if new files
            if ($data['files']) {
                foreach ($data['files'] as $key => $value) {
                    $temp = explode('.', $value['name']);
                    $newFile = $this->Image->upload(null, $value, $path);
                    $medias[] = [
                        'external_id' => $data['id'],
                        'type' => USER_ROLE_SALE,
                        'document_type' => $data['types'][$key],
                        'file_name' => $newFile,
                        'file_type' => end($temp),
                    ];
                }
            }

            // check is old files
            if (isset($data['old_files'])) {
                foreach ($data['old_files'] as $key => $value) {
                    $temp = explode('.', $value);
                    $medias[$countNewFile] = [
                        'external_id' => $data['id'],
                        'type' => USER_ROLE_SALE,
                        'document_type' => $data['old_doc_types'][$key],
                        'file_name' => $value,
                        'file_type' => end($temp),
                    ];
                    $countNewFile++;
                }
            }
        }

        if ($medias) {
            $this->Medias->deleteMediaById($data['id'], USER_ROLE_SALE);
            $data1 = $this->Medias->newEntities($medias);
            $this->Medias->saveMany($data1);
        }

        $sale = $this->Sales->get($data['id']);
        $sale->status = $data['status'];

        if ($this->Sales->save($sale)) {
            $response = [
                'step' => $data['step'] + 1,
                'status' => $sale->status,
            ];
        }

        $this->Function->ajaxResponse(1, MSG_SUCCESS, $response);
    }

    public function poPdf($id = null)
    {
        if (empty($id)) {
            throw new NotFoundException();
        }

        $sale = $this->Sales->find()
            ->contain([
                'SaleDetails',
                'Sellers' => [
                    'Suppliers' => [
                        'InfoDetails' => function ($q) {
                            return $q->where(['InfoDetails.type' => TYPE_SUPPLIER]);
                        }
                    ],
                    'Manufacturers' => [
                        'InfoDetails' => function ($q) {
                            return $q->where(['InfoDetails.type' => TYPE_MANUFACTURER]);
                        }
                    ]
                ],
                'Customers' => [
                    'InfoDetails' => function ($q) {
                        return $q->where(['InfoDetails.type' => TYPE_CUSTOMER]);
                    },
                    'ClinicDoctors' => [
                        'Doctors',
                        'sort' => ['ClinicDoctors.is_priority' => 'desc'],
                    ],
                ],
            ])
            ->where([
                'Sales.id' => $id
            ])
            ->first();

        $en = $this->request->session()->read('tb_field');
        $this->set(compact('sale', 'en'));
        $this->set('_serialize', ['sale']);
        $this->viewBuilder()->layout(false);
        $this->response->type('pdf');
    }

    public function poHtml($id = null)
    {
        if (empty($id)) {
            throw new NotFoundException();
        }

        $sale = $this->Sales->find()
            ->contain([
                'SaleDetails',
                'Sellers' => [
                    'Suppliers' => [
                        'InfoDetails' => function ($q) {
                            return $q->where(['InfoDetails.type' => TYPE_SUPPLIER]);
                        }
                    ],
                    'Manufacturers' => [
                        'InfoDetails' => function ($q) {
                            return $q->where(['InfoDetails.type' => TYPE_MANUFACTURER]);
                        }
                    ]
                ],
                'Customers' => [
                    'InfoDetails' => function ($q) {
                        return $q->where(['InfoDetails.type' => TYPE_CUSTOMER]);
                    },
                    'ClinicDoctors' => [
                        'Doctors',
                        'sort' => ['ClinicDoctors.is_priority' => 'desc'],
                    ],
                ],
            ])
            ->where([
                'Sales.id' => $id
            ])
            ->first();

        $en = $this->request->session()->read('tb_field');
        $this->set(compact('sale', 'en'));
        $this->set('_serialize', ['sale']);
        $this->viewBuilder()->layout('print');
    }

    public function rop($id = null)
    {
        if (empty($id)) {
            throw new NotFoundException();
        }
        $this->loadModel('SaleDetails');
        $this->loadModel('Currencies');
        $person = [];
        $data = $this->Sales->findById($id)
            ->contain([
                'Currencies',
                'Customers' => [
                    'InfoDetails',
                    'Countries',
                    'ClinicDoctors',
                    'PersonInCharges',
                ],
                'Sellers' => [
                    'Manufacturers' => [
                        'InfoDetails',
                        'Countries'
                    ],
                    'Suppliers' => [
                        'InfoDetails',
                        'Countries',
                    ],
                ],
                'SaleDetails' => function ($q) {
                    $q->select([
                        'id',
                        'sale_id',
                        'count' => $q->func()->count('*')
                    ])->group(['sale_id']);

                    return $q;
                },
            ])->first();
        $saleDetail = $this->SaleDetails->getAllBySaleId($id, 'threaded');

        $doctors = [];
        if ($data->customer) {
            $this->loadModel('ClinicDoctors');
            $doctors = $this->ClinicDoctors->getDoctors($data->customer->id);
        }

        $currencies = $this->Currencies->getDataList('id', 'code', null, $this->local);
        if ($data) {
            $this->loadModel('PersonInCharges');
            $ext = [];

            switch ($data->seller->type) {
                case TYPE_SUPPLIER :
                    $ext = [
                        'type' => TYPE_SUPPLIER,
                        'id' => $data->seller->supplier->id,
                    ];
                    break;
                default :
                    $ext = [
                        'type' => TYPE_MANUFACTURER,
                        'id' => $data->seller->manufacturer->id
                    ];
            }
            $person = ['' => __('TXT_SELECT_CONTACT')] + $this->PersonInCharges->getList($this->local, $ext['type'], $ext['id']);
        }

        $this->set(compact('data', 'person', 'saleDetail', 'doctors', 'currencies'));
    }

    public function getInfo()
    {
        $this->ajaxRequest(false);
        $this->loadModel('InfoDetails');
        $response = $this->request->query;
        $data = $this->InfoDetails->getListByExtAndId($response['external_id'], TYPE_PERSON_IN_CHARGE);

        $this->Function->ajaxResponse(1, MSG_SUCCESS, $data);
    }

    public function advanceSearch()
    {
        $this->ajaxRequest(true);
        $this->loadModel('Sellers');
        $id = $this->request->query('seller_id');
        $en = $this->local;

        $seller = $this->Sellers->findById($id)
            ->contain([
                'SellerBrands' => [
                    'ProductBrands'
                ]
            ])->first();

        $this->set(compact('seller', 'en'));
    }

    //function temporary not use.
    public function getBrandByMfId()
    {
        $this->ajaxRequest(false);
        $this->loadModel('ProductBrands');
        $manufacturerId = $this->request->query('manufacturerId');
        $data = $this->ProductBrands->getBrandByMfIdList($manufacturerId, 'id', $this->local);

        $this->Function->ajaxResponse(1, MSG_SUCCESS, $data);
    }

    public function getProductByBrandId()
    {
        $this->ajaxRequest(false);
        $this->loadModel('Products');
        $brandId = $this->request->query('brandId');
        $products = $this->Products->findByProductBrandId($brandId)->toArray();
        $data = $this->ReconstructData->convertToSelect2($products, $this->local);

        $this->Function->ajaxResponse(1, MSG_SUCCESS, $data);
    }

    public function getProductDetailByProductId()
    {
        $this->ajaxRequest(true);
        $this->loadModel('ProductDetails');
        $local = $this->local;
        $productId = $this->request->query('productId');
        $data = $this->ProductDetails->findByProductId($productId)
            ->contain([
                'Products' => ['ProductBrands'],
                'SingleUnits',
                'PackSizes',
            ])
            ->toArray();

        $this->set(compact('data', 'local'));
        $this->render('get_product_detail');
    }

    public function getFaxRop()
    {
        $this->ajaxRequest(true);
        $local = $this->local;
        $id = $this->request->query('id');
        $this->set(compact('local', 'id'));
    }

    public function changeDraffStatus()
    {
        $this->ajaxRequest(false);
        $this->response->type('json');
        $sale = $this->Sales->get($this->request->data['id']);
        $sale->status = STATUS_FAXED_ROP;
        if ($this->Sales->save($sale)) {
            $this->response->body(json_encode([
                'status' => 1,
                'message' => MSG_SUCCESS,
            ]));
        }
        return $this->response;
    }

    public function updateStatus()
    {
        $this->ajaxRequest(false);
        $this->loadModel('SaleReceivePayments');
        $this->loadModel('Sellers');
        $seller_id = $this->Sellers->sellerIdAsMp();
        $data = $this->Sales->get($this->request->data['id']);
        $data->status = $this->request->data['status'];
        // CHECK ACTION
        if ($this->request->data['action'] === 'po') {
            $data->sale_number = $this->Sales->saleNumber($data->type, ['seller_id' => $seller_id]);
            // CEHCK ISSUE_DATE IS EMPTY
            if (!$data->issue_date) {
                $data->issue_date = $this->request->data['issue_date'];
            }
        }
        // SAVE DATA
        if ($this->Sales->save($data)) {
            $response = [
                'step' => $this->request->data['step'],
                'status' => $data->status,
                'subStatus' => $data->sub_status,
                'payments' => $this->SaleReceivePayments->getSalePaymentListBySaleId($data->id),
            ];
            // DEDUCK STOCK
            $this->deductStock($data->id, $response, $this->request->data);
        }
    }

    public function updateSubStatus()
    {
        $this->ajaxRequest(false);
        $data = $this->Sales->get($this->request->data['id']);
        $data->sub_status = $this->request->data['subStatus'];

        if ($this->Sales->save($data)) {
            $this->Function->ajaxResponse(1, MSG_SUCCESS);
        }
    }

    public function invoice($id)
    {
        if ($id === null) {
            throw new NotFoundException();
        }
        $this->loadModel('SaleDetails');
        $person = [];

        $data = $this->Sales->findById($id)
            ->contain([
                'Currencies',
                'Sellers' => ['Manufacturers', 'Suppliers'],
                'SaleDetails' => function ($q) {
                    $q->select([
                        'id',
                        'sale_id',
                        'count' => $q->func()->count('*')
                    ])->group(['sale_id']);

                    return $q;
                },
            ])->first();
        $saleDetail = $this->SaleDetails->getAllBySaleId($id, 'threaded');

        if ($data) {
            $this->loadModel('PersonInCharges');
            $ext = [];

            switch ($data->seller->type) {
                case TYPE_SUPPLIER :
                    $ext = [
                        'type' => TYPE_SUPPLIER,
                        'id' => $data->seller->supplier->id,
                    ];
                    break;
                default :
                    $ext = [
                        'type' => TYPE_MANUFACTURER,
                        'id' => $data->seller->manufacturer->id
                    ];
            }
            $person = ['' => __('TXT_SELECT_CONTACT')] + $this->PersonInCharges->getList($this->local, $ext['type'], $ext['id']);
        }

        $this->set(compact('data', 'person', 'saleDetail'));
    }

    public function updateSellInfo()
    {
        $this->ajaxRequest(false);
        $data = $this->Sales->get($this->request->data['id']);
        $data->seller_fax = $this->request->data['fax'];
        $data->person_in_charge_tel = $this->request->data['tel'];
        $data->person_in_charge_email = $this->request->data['email'];
        $data->person_in_charge_name = $this->request->data['personInChargeName'];
        $data->person_in_charge_id = $this->request->data['personInChargeId'];

        if ($this->Sales->save($data)) {
            $this->Function->ajaxResponse(1, MSG_SUCCESS);
        }
    }

    public function updateSaleInfo()
    {
        $this->ajaxRequest(false);
        $request = $this->request->data;
        $sale = $this->Sales->get($request['id']);
        $sale = $this->Sales->patchEntity($sale, $request, [
            'validate' => false,
        ]);

        if ($this->Sales->save($sale)) {
            $this->Function->ajaxResponse(1, MSG_SUCCESS);
        } else {
            $this->Function->ajaxResponse(0, MSG_ERROR);
        }
    }

    public function updateSaleDoctorInfo()
    {
        $this->ajaxRequest(false);
        $request = $this->request->data;
        $sale = $this->Sales->get($request['id']);
        $sale->doctor_name = $request['doctor_name'];
        if ($this->Sales->save($sale)) {
            $this->Function->ajaxResponse(1, MSG_SUCCESS);
        } else {
            $this->Function->ajaxResponse(0, MSG_ERROR);
        }
    }

    public function updateRopIssueDate()
    {
        $this->ajaxRequest(false);
        $request = $this->request->data;
        $sale = $this->Sales->get($request['id']);
        if ($request['type'] == 'rop') {
            unset($request['order_date']);
            $sale->rop_issue_date = $this->convertJpDate($request['rop_issue_date'], $request['en']);
        } else {
            unset($request['rop_issue_date']);
            $sale->order_date = $this->convertJpDate($request['order_date'], $request['en']);
        }

        if ($this->Sales->save($sale)) {
            $this->Function->ajaxResponse(1, MSG_SUCCESS);
        } else {
            $this->Function->ajaxResponse(0, MSG_ERROR);
        }
    }

    public function updateCurrency()
    {
        $this->ajaxRequest(false);
        $request = $this->request->data;
        $sale = $this->Sales->get($request['id']);
        $sale->currency_id = $request['currency_id'];
        if ($this->Sales->save($sale)) {
            $this->Function->ajaxResponse(1, MSG_SUCCESS);
        } else {
            $this->Function->ajaxResponse(0, MSG_ERROR);
        }
    }

    public function updateCurrencyExchange()
    {
        $this->ajaxRequest(false);
        $request = $this->request->data;
        $sale = $this->Sales->get($request['id']);
        //validate currency exchange as number only.
        $request['currency_exchange_rate'] = str_replace([' ', ','], ['', ''], $request['currency_exchange_rate']);

        $sale->currency_date = str_replace('/', '-', $request['currency_date']);
        $sale->currency_exchange_rate = $request['currency_exchange_rate'];

        $sale = $this->Sales->patchEntity($sale, $request, ['validate' => 'exchange']);

        if ($this->Sales->save($sale)) {
            $this->Function->ajaxResponse(1, MSG_SUCCESS);
        } else {
            $this->Function->ajaxResponse(0, MSG_ERROR, $sale->errors());
        }
    }

    public function getDomestic()
    {
        $this->ajaxRequest(true);
        $this->loadModel('Currencies');
        $currency = $this->Currencies->get(1);
        $data = $this->Sales->get($this->request->query('id'));

        $this->set(compact('data', 'currency'));
    }

    public function getHandlingFee()
    {
        $this->ajaxRequest(true);
        $this->loadModel('Currencies');
        $this->loadModel('Sellers');
        $fees = [];
        $is_contract = false;

        $currency = $this->Currencies->get(1);
        $data = $this->Sales->get($this->request->query('id'));
        $seller_id = $this->request->query('seller_id');
        $seller = $this->Sellers->get($seller_id);

        if ($seller) {
            if ($seller->kind == CUSTOMER_TYPE_CONTRACT) {
                $this->loadModel('ManagementFees');
                $fees = $this->ManagementFees->getFeeDropdown($seller_id);
                $is_contract = true;
            }
        }
        $this->set(compact('data', 'currency', 'fees', 'seller', 'is_contract'));
    }

    public function getSpecialDiscount()
    {
        $this->ajaxRequest(true);
        $this->loadModel('Currencies');
        $currency = $this->Currencies->get(1);
        $data = $this->Sales->get($this->request->query('id'));

        $this->set(compact('data', 'currency'));
    }

    public function updateSpecialDiscount()
    {
        $this->ajaxRequest(false);
        if ($this->request->data['digits'] > 0) {
            $temp = explode('.', $this->request->data['special_discount']);
            $str = current($temp);
            $value = str_replace(',', '', $str);
        } else {
            $value = str_replace(',', '', $this->request->data['special_discount']);
        }

        if ($value <= 0) {
            $this->Function->ajaxResponse(0, MSG_ERROR, __('TXT_MESSAGE_VALUE_GREATER_THAN_ZERO'));
        } else {
            $data = $this->Sales->get($this->request->data['id']);
            $data->special_discount = $value;

            if ($this->Sales->save($data)) {
                $this->Function->ajaxResponse(1, MSG_SUCCESS);
            }
        }
    }

    public function updateDemestic()
    {
        $this->ajaxRequest(false);
        if ($this->request->data['digits'] > 0) {
            $temp = explode('.', $this->request->data['domestic_courier_service_charge']);
            $str = current($temp);
            $value = str_replace(',', '', $str);
        } else {
            $value = str_replace(',', '', $this->request->data['domestic_courier_service_charge']);
        }

        if ($value <= 0) {
            $this->Function->ajaxResponse(0, MSG_ERROR, __('TXT_MESSAGE_VALUE_GREATER_THAN_ZERO'));
        } else {
            $data = $this->Sales->get($this->request->data['id']);
            $data->domestic_courier_service_charge = $value;

            if ($this->Sales->save($data)) {
                $this->Function->ajaxResponse(1, MSG_SUCCESS);
            }
        }
    }

    public function updateHandlingFee()
    {
        $this->ajaxRequest(false);
        if ($this->request->data['digits'] > 0) {
            $temp = explode('.', $this->request->data['handling_fee']);
            $str = current($temp);
            $value = str_replace(',', '', $str);
        } else {
            $value = str_replace(',', '', $this->request->data['handling_fee']);
        }

        if ($value <= 0) {
            $this->Function->ajaxResponse(0, MSG_ERROR, __('TXT_MESSAGE_VALUE_GREATER_THAN_ZERO'));
        } else {
            $data = $this->Sales->get($this->request->data['id']);
            $data->handling_fee = $value;

            if ($this->Sales->save($data)) {
                $this->Function->ajaxResponse(1, MSG_SUCCESS);
            }
        }
    }

    /**
    * Action Edit Document for PDF
    *
    * @param $id is sale id
    */
    public function editDoc()
    {
        $this->ajaxRequest(true);
        $this->loadModel('SaleCustomClearanceDocs');
        $this->loadModel('PersonInCharges');
        $this->loadModel('ClinicDoctors');
        $customerId = $this->request->query('customer_id');
        $saleId = $this->request->query('sale_id');

        // list clinic_doctors base on customer_id
        $clinicDoctors = $this->ClinicDoctors->getAllDataList('all', null, [
            'ClinicDoctors.customer_id' => $customerId,
        ], ['Doctors'], null, null, 'ClinicDoctors.is_priority');

        // list sale_custom_clearance base on customer_id
        $saleCustomClearance = $this->SaleCustomClearanceDocs->getByField(['SaleCustomClearanceDocs.sale_id' => $saleId], null);

        // list person_in_charge base on customer_id
        $person = $this->PersonInCharges->getPersonByCustomer($customerId);

        // set data to view layout
        $data = [
            'saleCustomClearance' => $saleCustomClearance,
            'local' => $this->local,
            'person' => $person,
            'clinicDoctors' => $clinicDoctors,
            'saleId' => $saleId,
        ];
        $this->set($data);
    }

    /**
     * get all supplier name and manufacturer name by search keyword
     * return object
     */
    public function getAllSaleBySearch()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->viewBuilder()->layout('ajax');
        $this->autoRender = false;
        $this->response->type('json');
        $this->loadModel('Sellers');
        $seller_id = $this->Sellers->sellerIdAsMp();
        $keyword = $this->request->query('keyword');
        $sale = $this->Sales->find()
            ->select([
                'id',
                'seller_id',
                'is_suspend',
                'rop_number',
                'sale_number',
            ])
            ->where([
                'AND' => [
                    'Sales.is_suspend' => 0,
                    'Sales.seller_id' => $seller_id,
                    'OR' => [
                        'Sales.rop_number LIKE' => '%' . $keyword . '%',
                        'Sales.sale_number LIKE' => '%' . $keyword . '%',
                    ],
                ],
            ])
            ->limit(20)
            ->all();

        $this->response->body(json_encode([
            'status' => 1,
            'message' => MSG_SUCCESS,
            'data' => $sale,
        ]));
        return $this->response;
    }

    public function getAllSales()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->viewBuilder()->layout('ajax');
        $this->autoRender = false;
        $this->response->type('json');
        $sales = $this->Sales->find()
            ->where([
                'AND' => [
                    'Sales.is_suspend' => 0,
                    'OR' => [
                        'Sales.rop_number LIKE' => '%' . $this->request->query('keyword') . '%',
                        'Sales.sale_number LIKE' => '%' . $this->request->query('keyword') . '%',
                    ],
                ],
            ])->limit(20);

        $this->response->body(json_encode([
            'status' => 1,
            'message' => MSG_SUCCESS,
            'data' => $sales,
        ]));
        return $this->response;
    }

    //savePersonInChargeToSale
    public function savePersonInChargeToSale()
    {
        $this->ajaxRequest();
        //echo json_encode($this->request->data);
        $sale_id = $this->request->data['sale_id'];
        $person_in_charge_id = $this->request->data['person_in_charge_id'];
        if ($sale_id && $person_in_charge_id) {
            // get person_in_charge by $person_in_charge_id
            $this->loadModel('PersonInCharges');
            $person = $this->PersonInCharges->findById($person_in_charge_id)->first();
            // save person_in_charge to sale table for custome clearence
            $person_data = [
                'person_in_charge_id' => $person->id,
                'person_in_charge_name' => $person->first_name . ' ' . $person->last_name,
            ];
            $sale = $this->Sales->get($sale_id);
            $data = $this->Sales->patchEntity($sale, $person_data, ['validate' => false]);
            if ($this->Sales->save($data)) {
                $this->response->body(json_encode(['status' => 1]));
                return $this->response;
            }
            $this->response->body(json_encode(['status' => 0, 'message' => $data->errors()]));
            return $this->response;
        }
    }

    private function deductStock($saleId, $response, $request)
    {
        $this->loadModel('SaleDetails');
        $data = [];

        if (isset($request['po_type'])) {
            $data = $this->SaleDetails->getByField(['SaleDetails.sale_id' => $saleId], null, [
                'SaleStockDetails',
            ]);

            switch ($request['po_type']) {
                case CUSTOMER_TYPE_NORMAL :
                    if ($request['step'] == 5) {
                        // sale stock detail
                        $this->grebSaleStockDetail($data);
                    }
                    break;
                case CUSTOMER_TYPE_CONTRACT :
                    break;
                default :

            }
        }

        $this->Function->ajaxResponse(1, MSG_SUCCESS, $response);
    }

    public function grebSaleStockDetail($data)
    {
        $saleStockDetail = [];

        if ($data) {
            foreach ($data as $key => $value) {
                if ($value->sale_stock_details) {
                    $saleStockDetail[] = $value->sale_stock_details;
                }
            }
        }
        if ($saleStockDetail) {
            $this->loadModel('Stocks');
            $data1 = [];

            foreach ($saleStockDetail as $key1 => $value1) {
                $data1[] = $value1;
            }
            $this->Stocks->deductStock($data1);
        }
    }

    public function getBySaleId()
    {
        $this->ajaxRequest(false);
        $this->response->type('json');
        $sale = $this->Sales->find()
            ->where(['Sales.id' => $this->request->query('sale_id')])
            ->contain([
                'Currencies',
                'SaleDetails' => function ($q) {
                    $q->select([
                        'id',
                        'sale_id',
                        'total_amount' => $q->func()->sum('SaleDetails.unit_price * SaleDetails.quantity'),
                    ])->group('SaleDetails.sale_id');
                    return $q;
                },
            ])->first();
        $this->response->body(json_encode([
            'status' => 1,
            'message' => MSG_SUCCESS,
            'data' => [
                'sale' => $sale,
            ],
        ]));
        return $this->response;
    }

    /**
    * Function ajaxRequest
    * using for checking the request is ajax
    * @param boolean $auto_render assign value to $this->autoRender
    */
    public function ajaxRequest($auto_render = false)
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->viewBuilder()->layout('ajax');
        $this->autoRender = $auto_render;
    }

    /**
     * Function getCurrency
     * using for get currency list
     * @return object retrun array list
     */
    private function getCurrency()
    {
        $this->loadModel('Currencies');
        $data = $this->Currencies->getDropdown('list');

        return $data;
    }

    /**
     * Function getSeller
     * using for get seller list
     * @return object retrun array list
     */
    private function getSeller($kind_of = false, $country_id = null, $en = null)
    {
        $this->loadModel('Sellers');
        $field_name = 'name' . $this->local;
        $data = $this->Sellers->getSellerList($field_name, $kind_of, $country_id, $en);

        return $data;
    }

    private function convertJpDate($date, $en)
    {
        if ($en == '') {
            $jp = ['年','月','日'];
            $dash = ['-','-','-'];

            return rtrim(str_replace($jp, $dash, $date), '-');
        }
        return $date;
    }

    /**
     * Date start by month first. Format m/d/Y
     */
    private function orderDateByMonth($order_date)
    {
        $date = explode('/', $order_date);
        $month = $date[0];
        $day = $date[1];
        $year = $date[2];
        $format = $year . '-' . $month . '-' . $day;
        return date('Y-m-d', strtotime($format));
    }
}
