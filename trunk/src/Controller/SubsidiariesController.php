<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Network\Exception\NotFoundException;

class SubsidiariesController extends AppController
{
    private $locale;
    public $field;

    public function initialize()
    {
        parent::initialize();
        $this->field = 'name' . $this->request->session()->read('tb_field');
        $this->locale = $this->request->session()->read('tb_field');
        $this->loadComponent('Function');
        $this->loadModel('EntityNames');
        $this->loadModel('InfoDetails');
    }

    public function index()
    {
        $options = [];
        if ($this->request->query('keyword')) {
            $keyword = trim($this->request->query('keyword'));
            $options[] = [
                'OR' => [
                    'Subsidiaries.name_en LIKE' => '%' .$keyword .'%',
                    'Subsidiaries.name LIKE' => '%' .$keyword .'%',
                    'MedicalCorporations.name LIKE' => '%' . $keyword . '%',
                    'MedicalCorporations.name_en LIKE' => '%' . $keyword . '%'
                ]
            ];
        }
        $displays = 10;
        if ($this->request->query('displays')) {
            $displays = $this->request->query('displays');
        }
        if ($this->request->query('inactive')) {
            $options[] = [
                'Subsidiaries.is_suspend' => 1
            ];
        }
        $this->paginate = [
            'conditions' => $options,
            'limit' => $displays,
            'order' => [
                $this->field => 'asc',
            ],
            'contain' => [
                'Users',
                'MedicalCorporations',
                'PersonInCharges' => function($q) {
                    return $q->where(['PersonInCharges.type' => TYPE_SUBSIDIARY]);
                }
            ]
        ];
        $data = [
            'locale' => $this->locale,
            'subsidiaries' => $this->paginate($this->Subsidiaries),
            'payment_list' => $this->getPaymentCategoriesList()->toArray(),
            'invoice_list' => $this->getInvoiceDeliveryList()->toArray(),
            'displays' => $displays,
            'paging' => $this->request->param('paging')['Subsidiaries']['pageCount'],
        ];

        $this->set($data);
    }

    public function create()
    {
        $this->loadModel('Countries');
        $this->loadModel('InvoiceDeliveryMethods');
        $this->loadModel('PaymentCategories');
        $data = $this->Subsidiaries->newEntity();
        $payment_list = $this->PaymentCategories->payDropdown($this->locale);
        $invoice_list = $this->InvoiceDeliveryMethods->invoiceDropdown($this->locale);
        $response = [
            'data' => $data,
            'locale' => $this->locale,
            'txt_register' => $this->Common->txtRegister(),
            'types' => $this->CustomConstant->subsidiaryTypes(),
            'countries' => $this->Countries->getListOfCountries($this->locale),
            'payment_list' => $payment_list,
            'invoice_list' => $invoice_list,
        ];
        $this->set($response);
    }

    public function edit($id = null)
    {
        $this->loadModel('Countries');
        $this->loadModel('InvoiceDeliveryMethods');
        $this->loadModel('PaymentCategories');
        $data = $this->Subsidiaries->findById($id)
                ->contain([
                    'CustomerSubsidiaries' => ['Customers'],
                    'InfoDetails' => function($q) use ($id) {
                    return $q->where([
                        'InfoDetails.type' => TYPE_SUBSIDIARY,
                        'InfoDetails.external_id' => $id,
                    ]);
                    }
                ])
                ->first();
        $payment_list = $this->PaymentCategories->payDropdown($this->locale);
        $invoice_list = $this->InvoiceDeliveryMethods->invoiceDropdown($this->locale);
        $response = [
            'data' => $data,
            'locale' => $this->locale,
            'txt_edit' => $this->Common->txtEdit(),
            'types' => $this->CustomConstant->subsidiaryTypes(),
            'countries' => $this->Countries->getListOfCountries($this->locale),
            'payment_list' => $payment_list,
            'invoice_list' => $invoice_list,
        ];
        $this->set($response);
    }

    public function saveOrUpdate()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->response->type('json');
        $this->response->disableCache();
        $this->autoRender = false;
        $this->viewBuilder()->layout('ajax');
        // check exist record
        if ($this->request->data('id')) {
            $data = $this->Subsidiaries->get($this->request->data['id']);
            $this->InfoDetails->deleteAll([
                'type' => TYPE_SUBSIDIARY,
                'external_id' => $this->request->data['id'],
            ]);
            // TODO: confirm with Nik san what to do with this condition
            if ($this->request->data['type'] === TYPE_CUSTOMER) {
                $this->loadModel('CustomerSubsidiaries');
                $this->CustomerSubsidiaries->deleteAll([
                    'subsidiary_id' => $this->request->data['id'],
                ]);
            }
        } else {
            $data = $this->Subsidiaries->newEntity();
        }
        // check validation
        $validator = $this->Subsidiaries->patchEntity($data, $this->request->data);
        if ($validator->errors()) {
            $this->response->body(json_encode([
                'status' => 0,
                'message' => MSG_ERROR,
                'data' => $validator->errors(),
            ]));
            return $this->response;
        }
        if ($this->Subsidiaries->save($data)) {
            $this->updateInfoDetails($this->request->data, $data->id);
            $this->response->body(json_encode([
                'status' => 1,
                'message' => MSG_SUCCESS,
            ]));
            return $this->response;
        }
    }

    private function updateInfoDetails($data, $customerId = null)
    {
        if ($customerId === null) {
            return false;
        }
        $this->InfoDetails->deleteAll([
            'external_id' => $customerId,
            'type' => TYPE_SUBSIDIARY,
        ]);
        //pr($data['info_details']);exit;
        $info_details = [];
        if (isset($data['info_details'])) {
            foreach ($data['info_details'] as $key => $value) {
                //pr($value);exit;
                $info_details[] = [
                    'external_id' => $customerId,
                    'phone' =>  null,
                    'tel' => isset($value['tel']) ? $value['tel'] : null,
                    'tel_extension' => isset($value['tel_extension']) ? $value['tel_extension'] : null,
                    'email' => isset($value['email']) ? $value['email'] : null,
                    'type' => TYPE_SUBSIDIARY,
                ];
            }
        }
        if (!$info_details) {
            return false;
        }
        $entities = $this->InfoDetails->newEntities($info_details);
        if ($this->InfoDetails->saveMany($entities)) {
            return true;
        }
        return true;
    }

    public function getSubType()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->viewBuilder()->layout('ajax');
        $this->autoRender = false;
        $this->response->disableCache();
        $data = [];
        $type = $this->request->query('type');
        $keyword = trim($this->request->query('keyword'));
        //CHECK TYPE
        switch ($type) {
            case TYPE_CUSTOMER:
                $this->loadModel('Customers');
                $data = $this->Customers->find()
                        ->where(['is_suspend' => 0])
                        ->where(['OR' => [
                            'Customers.name_en' => $keyword,
                            'Customers.name' => $keyword,
                        ]])
                        ->limit(PAGE_LIMIT_NUMBER)
                        ->all();
                break;

            case TYPE_HQ_CUSTOMER:
            case TYPE_HQMC:
                $this->loadModel('Groups');
                $data = $this->Groups->find()
                        ->where(['is_suspend' => 0])
                        ->where(['type' => $type])
                        ->limit(PAGE_LIMIT_NUMBER)
                        ->all();
                break;

            case TYPE_MEDICAL_CORP:
                $this->loadModel('MedicalCorporations');
                $data = $this->MedicalCorporations->find()
                        ->where(['is_suspend' => 0])
                        ->where(['entity_type' => $type])
                        ->limit(PAGE_LIMIT_NUMBER)
                        ->all();
                break;
        }
        $this->response->body(json_encode([
            'status' => 1,
            'message' => MSG_SUCCESS,
            'data' => $data,
        ]));

        return $this->response;
    }

    //TODO CLEANUP CODE

    public function view($id = null)
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->viewBuilder()->layout('ajax');
        $data = $this->Subsidiaries->findById($id)->contain([
                'Countries',
                'InfoDetails' => function($q) {
                    return $q->where([
                        'InfoDetails.type' => TYPE_SUBSIDIARY,
                   ]);
                },
                'PersonInCharges' => function($q) {
                   return $q->where([
                        'PersonInCharges.type' => TYPE_SUBSIDIARY,
                   ]);
                },
                'CustomerSubsidiaries' => [
                    'joinType' => 'LEFT',
                    'Customers',
                ],
            ])->first();
        $payment_name = $this->getPaymentCategoriesList()->toArray();
        $invoice_name = $this->getInvoiceDeliveryList()->toArray();
        $data['payment_name'] = $payment_name[$data->payment_category_id];
        $data['invoice_name'] = $invoice_name[$data->invoice_delivery_method_id];
        $en = $this->request->session()->read('tb_field');
        $name = 'name' . $en;
        $locale = $this->locale;
        $this->set(compact('data', 'name', 'locale'));
    }

    public function getPaymentCategoriesList()
    {
        $this->loadModel('PaymentCategories');
        $data = $this->PaymentCategories->find('list', [
            'keyField' => 'id',
            'valueField' => 'name',
        ]);

        return $data;
    }

    public function getInvoiceDeliveryList()
    {
        $this->loadModel('InvoiceDeliveryMethods');
        $data = $this->InvoiceDeliveryMethods->find('list', [
            'keyField' => 'id',
            'valuefield' => 'name',
        ]);

        return $data;
    }

    public function delete()
    {
        if ($this->request->is('ajax')) {
            $this->autoRender = false;
            $this->response->disableCache();
            $this->response->type('json');
            $this->viewBuilder()->layout(false);

            $this->request->allowMethod(['post', 'delete']);
            $data = $this->Subsidiaries->get($this->request->data['id']);
            if ($this->Subsidiaries->delete($data)) {
                $this->response->body(json_encode(['status' => 1]));
                return $this->response;
            }
            $this->response->body(json_encode(['status' => 0, 'message' => __('TXT_DELETE_TROUBLE')]));
            return $this->response;
        }
    }

    public function updateSuspend()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->autoRender = false;
        $this->response->disableCache();
        $this->response->type('json');
        $this->viewBuilder()->layout(false);
        $data = $this->Subsidiaries->get($this->request->data['id']);
        $data->is_suspend = ($this->request->data['is_suspend'] == 0 ) ? 1 : 0;
        if ($this->Subsidiaries->save($data)) {
            $this->response->body(json_encode([
                'status' => 1,
                'message' => 'success'
            ]));
            return $this->response;
        }
        $this->response->body(json_encode([
            'status' => 0,
            'message' => 'error'
        ]));
        return $this->response;
    }

    /**
     * Using for retrieve data while search on select dropdown
     * @return object response to ajax
     * @throws NotFoundException request is not ajax
     */
    public function getAutocompleteList()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->viewBuilder()->layout('ajax');
        $this->autoRender = false;
        $this->response->type('json');
        $this->response->disableCache();
        $data = $this->Subsidiaries->getAutocompleteList($this->request->query('keyword'));
        $this->response->body(json_encode([
            'status' => '1',
            'message' => MSG_SUCCESS,
            'data' => $data,
        ]));
        return $this->response;
    }

    /**
     * Using for retrieve subsidiary's infomation base on:
     * - subsidiary_id
     * @param int subsidiary_id
     * @return object object response to ajax
     * @throws NotFoundException request is not ajax
     */
    public function getSubsidiaryInfo()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->viewBuilder()->layout('ajax');
        $this->autoRender = false;
        $this->response->type('json');
        $this->response->disableCache();
        $data = $this->Subsidiaries->findById($this->request->query('subsidiary_id'))
                ->contain(['InfoDetails'])
                ->first();
        $this->response->body(json_encode([
            'status' => '1',
            'message' => MSG_SUCCESS,
            'data' => $data,
        ]));
        return $this->response;
    }

    /**
    * get all person_in_charge name by search auto complete
    * @return object
    */
    public function getSubsidiariesBySearch()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->viewBuilder()->layout('ajax');
        $this->autoRender = false;
        $this->response->type('json');
        $this->response->disableCache();
        $keyword = $this->request->query('keyword');

        $subsidiaries = $this->Subsidiaries->find()
            ->select([
                'Subsidiaries.name',
                'Subsidiaries.name_en',
                'MedicalCorporations.name',
                'MedicalCorporations.name_en',
            ])
            ->where([
                'AND' => [
                    'Subsidiaries.is_suspend' => 0,
                    'OR' => [
                        'Subsidiaries.name_en LIKE' => '%' . $keyword . '%',
                        'Subsidiaries.name LIKE' => '%' . $keyword . '%',
                    ]
                ]
            ])
            ->contain([
                'MedicalCorporations' => function ($q) use ($keyword) {
                    if (!empty($keyword)) {
                        $q->where([
                            'AND' => [
                                'MedicalCorporations.is_suspend' => 0,
                                'OR' => [
                                    'MedicalCorporations.name_en LIKE' => '%' . $keyword . '%',
                                    'MedicalCorporations.name LIKE' => '%' . $keyword . '%',
                                ]
                            ]
                        ]);
                    }

                    return $q;
                },
            ])
            ->limit(20)
            ->all();

        $this->response->body(json_encode([
            'status' => '1',
            'message' => MSG_SUCCESS,
            'data' => $subsidiaries,
        ]));
        return $this->response;
    }
}
