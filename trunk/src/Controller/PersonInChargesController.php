<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Network\Exception\NotFoundException;

class PersonInChargesController extends AppController
{
    private $locale;

    public function initialize()
    {
        parent::initialize();
        $this->loadModel('InfoDetails');
        $this->loadComponent('Function');
        $this->locale = $this->request->session()->read('tb_field');
    }

    public function index()
    {
        $conditions = [];
        if ($this->request->query('filter_by')) {
            $conditions['AND']['PersonInCharges.type'] = $this->request->query('filter_by');
        }
        if ($this->request->query('external_id')) {
            $conditions['AND']['PersonInCharges.external_id'] = $this->request->query('external_id');
        }
        if ($this->request->query('keyword')) {
            $keyword = trim($this->request->query('keyword'));
            $conditions['AND']['OR'] = [
                'PersonInCharges.department LIKE' => '%' . $keyword . '%',
                'PersonInCharges.first_name LIKE' => '%' . $keyword . '%',
                'PersonInCharges.last_name LIKE' => '%' . $keyword . '%',
            ];
        }
        // SET PAGE LIMIT ON PAGINATION
        $display = PAGE_NUMBER;
        if ($this->request->query('displays')) {
            $display = $this->request->query('displays');
        }
        if ($this->request->query('inactive')) {
            $conditions[] = [
                'PersonInCharges.is_suspend' => 1
            ];
        }
        $this->paginate = [
            'conditions' => $conditions,
            'contain' => [
                'Manufacturers',
                'Customers',
                'Suppliers',
                'Customers',
                'Subsidiaries',
            ],
            'limit' => $display,
            'order' => ['type' => 'asc']
        ];
        $data = [
            'data' => $this->paginate($this->PersonInCharges),
            'locale' => $this->locale,
            'display' => $display,
        ];
        // pr($data['data']);exit;

        $this->set($data);
    }

    public function add()
    {
        $personInCharge = $this->PersonInCharges->newEntity();
        $info_details = [];
        if ($this->request->query('des') === TYPE_SUBSIDIARY) {
            $info_details = $this->InfoDetails->find()
                    ->select(['tel'])
                    ->where(['type' => $this->request->query('des')])
                    ->where(['tel IS NOT' => null])
                    ->first();
        }
        $data = [
            'locale' => $this->locale,
            'personInCharge' => $personInCharge,
            'info_details' => $info_details,
        ];
        $this->set($data);
    }

    public function edit($id = null)
    {
        $personInCharge = $this->PersonInCharges->findById($id)
                ->contain([
                    'InfoDetails' => function ($q) {
                    return $q->where([
                        'InfoDetails.type' => TYPE_PERSON_IN_CHARGE,
                    ]);
                    }
                ])
                ->first();
        $ext_data = [];
        // CHECK TYPE
        switch ($personInCharge->type) {
            case TYPE_MANUFACTURER:
                $this->loadModel('Manufacturers');
                $ext_data = $this->Manufacturers->find('list', [
                    'conditions' => [
                        'Manufacturers.id' => $personInCharge->external_id,
                    ],
                    'keyField' => 'id',
                    'valueField' => function ($table) {
                    return $table->get('full_name');
                    }
                ]);
                break;

            case TYPE_CUSTOMER:
                $this->loadModel('Customers');
                $ext_data = $this->Customers->find('list', [
                    'conditions' => [
                        'Customers.id' => $personInCharge->external_id,
                    ],
                    'keyField' => 'id',
                    'valueField' => function ($table) {
                    return $table->get('full_name');
                    }
                ]);
                break;

            case TYPE_SUPPLIER:
                $this->loadModel('Suppliers');
                $ext_data = $this->Suppliers->find('list', [
                    'conditions' => [
                        'Suppliers.id' => $personInCharge->external_id,
                    ],
                    'keyField' => 'id',
                    'valueField' => function ($table) {
                    return $table->get('full_name');
                    }
                ]);
                break;

            case TYPE_MEDICAL_CORP:
                $this->loadModel('MedicalCorporations');
                $ext_data = $this->MedicalCorporations->find('list', [
                    'conditions' => [
                        'MedicalCorporations.id' => $personInCharge->external_id,
                    ],
                    'keyField' => 'id',
                    'valueField' => function ($table) {
                    return $table->get('full_name');
                    }
                ]);
                break;

            case TYPE_SUBSIDIARY:
                $this->loadModel('Subsidiaries');
                $ext_data = $this->Subsidiaries->find('list', [
                    'conditions' => [
                        'Subsidiaries.id' => $personInCharge->external_id,
                    ],
                    'keyField' => 'id',
                    'valueField' => function ($table) {
                    return $table->get('full_name');
                    }
                ]);
                break;
        }
        $data = [
            'locale' => $this->locale,
            'personInCharge' => $personInCharge,
            'ext_data' => $ext_data,
        ];
        $this->set($data);
    }

    public function view()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->viewBuilder()->layout('ajax');
        $personInCharge = $this->PersonInCharges->find('all')
            ->where(['PersonInCharges.id' => $this->request->query('id')])
            ->contain([
                'InfoDetails' => function($q) {
                    return $q->where(['InfoDetails.type' => TYPE_PERSON_IN_CHARGE]);
                },
                'Manufacturers',
                'Customers',
                'Suppliers',
                'Customers',
                'Subsidiaries',
            ])->first();
        $data = [
            'data' => $personInCharge,
            'locale' => $this->locale,
        ];
        $this->set($data);
    }

    public function delete()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->viewBuilder()->layout('ajax');
        $this->autoRender = false;
        $this->response->type('json');
        $this->response->disableCache();
        $data = $this->PersonInCharges->get($this->request->data['id']);
        if ($this->PersonInCharges->delete($data)) {
            $this->response->body(json_encode([
                'status' => 1,
                'message' => MSG_SUCCESS,
            ]));
            return $this->response;
        }
    }

    public function saveOrUpdate()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->viewBuilder()->layout('ajax');
        $this->autoRender = false;
        $this->response->disableCache();
        $this->response->type('json');
        $infoDetails = [];
        // check exist record
        if ($this->request->data('id')) {
            $data = $this->PersonInCharges->get($this->request->data['id']);
            $infoDetails = $this->InfoDetails->find()
                ->where(['external_id' => $this->request->data['id']])
                ->where(['type' => TYPE_PERSON_IN_CHARGE])
                ->first();
        } else {
            $data = $this->PersonInCharges->newEntity();
        }
        $validator = $this->PersonInCharges->patchEntity($data, $this->request->data);
        // check validation
        if ($validator->errors()) {
            $this->response->body(json_encode([
                'status' => 0,
                'message' => MSG_ERROR,
                'data' => $validator->errors(),
            ]));
            return $this->response;
        }
        if ($infoDetails) {
            $this->InfoDetails->deleteAll([
                'type' => TYPE_PERSON_IN_CHARGE,
            ]);
        }
        if ($this->PersonInCharges->save($data)) {
            $this->response->body(json_encode([
                'status' => 1,
                'message' => MSG_SUCCESS,
            ]));
            return $this->response;
        }
    }

    public function changeOfStatus()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->viewBuilder()->layout('ajax');
        $this->autoRender = false;
        $this->response->disableCache();
        $this->response->type('json');
        $data = $this->PersonInCharges->get($this->request->data['id']);
        $data->is_suspend = $this->request->data['status'];
        if ($this->PersonInCharges->save($data)) {
            $this->response->body(json_encode([
                'status' => 1,
                'message' => MSG_SUCCESS,
            ]));
            return $this->response;
        }
    }

    public function getStatus()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->viewBuilder()->layout('ajax');
        $personInCharge = $this->PersonInCharges->get($this->request->query('id'));
        $data = [
            'data' => $personInCharge,
            'status' => $this->request->query('status'),
        ];
        $this->set($data);
    }

    public function getDelete()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->viewBuilder()->layout('ajax');
        $manufacturer = $this->PersonInCharges->get($this->request->query('id'));
        $data = [
            'data' => $manufacturer,
        ];
        $this->set($data);
    }

    public function getPersonInChargeByType()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->viewBuilder()->layout('ajax');
        $this->autoRender = false;
        $this->response->type('json');
        $data = $this->getContainByType($this->request->query('type'));
        $this->response->body(json_encode([
            'status' => 1,
            'message' => MSG_SUCCESS,
            'data' => $data,
        ]));
        return $this->response;
    }

    private function getContainByType($type = null)
    {
        if ($type === null) {
            return false;
        }
        $data = [];
        // RETRIEVE DTA BY TYPE
        switch ($this->request->query('type')) {
            case TYPE_MANUFACTURER:
                $this->loadModel('Manufacturers');
                $data = $this->Manufacturers->find()
                        ->where(['is_suspend' => 0])
                        ->limit(PAGE_LIMIT_NUMBER)
                        ->all();
                break;

            case TYPE_CUSTOMER:
                $this->loadModel('Customers');
                $data = $this->Customers->find()
                        ->where(['is_suspend' => 0])
                        ->limit(PAGE_LIMIT_NUMBER)
                        ->all();
                break;

            case TYPE_SUPPLIER:
                $this->loadModel('Suppliers');
                $data = $this->Suppliers->find()
                        ->where(['is_suspend' => 0])
                        ->limit(PAGE_LIMIT_NUMBER)
                        ->all();
                break;

            case TYPE_MEDICAL_CORP:
                $this->loadModel('MedicalCorporations');
                $data = $this->MedicalCorporations->find()
                        ->where(['is_suspend' => 0])
                        ->limit(PAGE_LIMIT_NUMBER)
                        ->all();
                break;

            case TYPE_SUBSIDIARY:
                $this->loadModel('Subsidiaries');
                $data = $this->Subsidiaries->find()
                        ->where(['is_suspend' => 0])
                        ->limit(PAGE_LIMIT_NUMBER)
                        ->all();
                break;
        }

        return $data;
    }
}
