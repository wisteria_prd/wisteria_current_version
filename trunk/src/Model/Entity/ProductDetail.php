<?php

namespace App\Model\Entity;

use Cake\ORM\Entity;

class ProductDetail extends Entity
{
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];

    protected function _getIsSellFlag()
    {
        return ($this->sell_flag) ? __('TXT_YES') : __('TXT_NO');
    }

    protected function _getIsPurchaseFlag()
    {
        return ($this->purchase_flag) ? __('TXT_YES') : __('TXT_NO');
    }
}