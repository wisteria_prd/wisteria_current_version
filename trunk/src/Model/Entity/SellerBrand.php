<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

class SellerBrand extends Entity
{
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];
}
