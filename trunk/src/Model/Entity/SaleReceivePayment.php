<?php

namespace App\Model\Entity;

use Cake\ORM\Entity;

class SaleReceivePayment extends Entity
{
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];

}