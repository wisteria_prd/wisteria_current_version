<?php

namespace App\Model\Entity;

use Cake\ORM\Entity;

class Import extends Entity
{
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];
}
