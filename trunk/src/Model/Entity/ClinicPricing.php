<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

class ClinicPricing extends Entity
{
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];
}