<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

class Currency extends Entity
{
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];
}