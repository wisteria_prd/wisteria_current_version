<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

class Country extends Entity
{
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];

    protected function getName()
    {
        return $this->_properties['name'];
    }

    protected function getNameEn()
    {
        return $this->_properties['name_en'];
    }
}
