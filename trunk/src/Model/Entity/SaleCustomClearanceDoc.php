<?php

namespace App\Model\Entity;

use Cake\ORM\Entity;

class SaleCustomClearanceDoc extends Entity
{
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];

}