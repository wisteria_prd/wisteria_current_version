<?php

namespace App\Model\Entity;

use Cake\ORM\Entity;
use Cake\ORM\TableRegistry;

class Product extends Entity
{
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];

    protected function _getIsSellFlag()
    {
        return ($this->sell_flag) ? __('TXT_YES') : __('TXT_NO');
    }

    protected function _getIsPurchaseFlag()
    {
        return ($this->purchase_flag) ? __('TXT_YES') : __('TXT_NO');
    }

    protected function _getLabel()
    {
        return $this->_properties['name_en'] ? $this->_properties['name_en'] : $this->_properties['name'];
    }
}
