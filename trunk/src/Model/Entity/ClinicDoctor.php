<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

class ClinicDoctor extends Entity
{
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];

}