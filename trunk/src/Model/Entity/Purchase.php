<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

class Purchase extends Entity
{
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];
}