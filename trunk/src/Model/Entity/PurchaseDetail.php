<?php

namespace App\Model\Entity;

use Cake\ORM\Entity;

class PurchaseDetail extends Entity
{
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];
}