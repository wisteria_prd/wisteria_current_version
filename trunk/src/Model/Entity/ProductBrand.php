<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;
use Cake\ORM\TableRegistry;

class ProductBrand extends Entity
{
    protected $_virtual = [
        'manufacturers',
        'count_product',
        'full_name',
    ];

    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];

    protected function _getManufacturers()
    {
        $table = TableRegistry::get('Manufacturers');
        $manufacturer_id = $this->_properties['manufacturer_id'];
        $result = $table->find()
            ->select(['name_en', 'name', 'id'])
            ->where(['Manufacturers.id' => $manufacturer_id])
            ->first();

        return $result;
    }

    protected function _getCountProduct()
    {
        $table = TableRegistry::get('Products');
        $id = $this->_properties['id'];
        $result = $table->find('all')
            ->select(['id'])
            ->where(['Products.product_brand_id' => $id])
            ->count();

        return $result;
    }

    protected function _getForPurchase()
    {
        return ($this->is_purchase) ? __('TXT_YES') : __('TXT_NO');
    }

    protected function _getForSale()
    {
        return ($this->is_sale) ? __('TXT_YES') : __('TXT_NO');
    }

    protected function _getFullName()
    {
        return $this->_properties['name'] . ' ' . $this->_properties['name_en'];
    }

    // In your Authors Entity create a virtual field to be used as the displayField:
    protected function _getLabel()
    {
        return $this->_properties['name_en'] ? $this->_properties['name_en'] : $this->_properties['name'];
    }
}
