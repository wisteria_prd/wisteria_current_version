<?php

namespace App\Model\Entity;

use Cake\ORM\Entity;

class WStandardPrice extends Entity
{
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];

}