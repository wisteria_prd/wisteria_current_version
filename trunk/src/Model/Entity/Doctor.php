<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

class Doctor extends Entity
{
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];

    /**
     * Japanese
     * Lastname　Firstname
     * double byte space
     * @return string
     */
    protected function _getFullName()
    {
        return $this->last_name . '　' . $this->first_name;
    }

    /**
     * English
     * Firstname Lastname
     * single byte space
     * @return string
     */
    protected function _getFullNameEn()
    {
        return $this->first_name_en . ' ' . $this->last_name_en;
    }
}