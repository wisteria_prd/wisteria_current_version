<?php

namespace App\Model\Entity;

use Cake\ORM\Entity;

class SingleUnit extends Entity
{
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];
}