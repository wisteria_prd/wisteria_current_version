<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

class SellerPricing extends Entity
{
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];
}