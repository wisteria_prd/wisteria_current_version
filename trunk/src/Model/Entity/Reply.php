<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

class Reply extends Entity
{
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];

}