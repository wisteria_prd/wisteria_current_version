<?php

namespace App\Model\Entity;

use Cake\ORM\Entity;

class SalePayment extends Entity
{
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];

}