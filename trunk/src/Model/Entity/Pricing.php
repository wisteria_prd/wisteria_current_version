<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;
use Cake\ORM\TableRegistry;

class Pricing extends Entity
{
    protected $_virtual = ['get_result'];

    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];
}
