<?php

namespace App\Model\Entity;

use Cake\ORM\Entity;

class ModuleAction extends Entity
{
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];
}
