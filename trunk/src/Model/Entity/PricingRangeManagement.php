<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

class PricingRangeManagement extends Entity
{

    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];
}
