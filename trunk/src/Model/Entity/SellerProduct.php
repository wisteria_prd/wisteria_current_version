<?php

namespace App\Model\Entity;

use Cake\ORM\Entity;

class SellerProduct extends Entity
{
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];

}