<?php

namespace App\Model\Entity;

use Cake\ORM\Entity;

class InvoiceDeliveryMethod extends Entity
{
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];
}