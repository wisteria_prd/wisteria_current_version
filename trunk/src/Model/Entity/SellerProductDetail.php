<?php

namespace App\Model\Entity;

use Cake\ORM\Entity;

class SellerProductDetail extends Entity
{
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];

}