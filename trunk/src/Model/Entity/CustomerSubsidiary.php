<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

class CustomerSubsidiary extends Entity
{
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];
}