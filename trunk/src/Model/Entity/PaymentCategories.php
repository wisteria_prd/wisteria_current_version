<?php

namespace App\Model\Entity;

use Cake\ORM\Entity;

class PaymentCategories extends Entity
{
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];
}