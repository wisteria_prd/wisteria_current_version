<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

class PricingManagement extends Entity
{
    protected $_virtual = ['manufacturer_name', 'supplier_name'];

    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];

    protected function _getManufacturerName()
    {
        // $data = isset($this->_properties['product_detail']['product']['product_brand']['seller_brands']) ? $this->_properties['product_detail']['product']['product_brand']['seller_brands'] : [];
        // $result = '';
        // if ($this->_properties['external_id_type'] === 'manufacturer') {
        //     foreach ($data as $key => $value) {
        //         if ($this->_properties['external_id'] === $value->seller->external_id && $value->seller->type === 'manufacturer') {
        //             $result = $value->seller->external_id;
        //             break;
        //         }
        //     }
        // }
        // return $result;
    }

    protected function _getSupplierName()
    {

    }
}
