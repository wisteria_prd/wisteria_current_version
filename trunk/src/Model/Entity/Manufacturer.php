<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;
use Cake\ORM\TableRegistry;
use Cake\ORM\Query;

class Manufacturer extends Entity
{
    protected $_virtual = ['full_name'];

    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];

    protected function _getFullName()
    {
        return $this->_properties['name'] . ' ' . $this->_properties['name_en'];
    }

    // In your Authors Entity create a virtual field to be used as the displayField:
    protected function _getLabel()
    {
        return $this->_properties['name_en'] ? $this->_properties['name_en'] : $this->_properties['name'];
    }
}
