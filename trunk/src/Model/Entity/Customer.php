<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

class Customer extends Entity
{
    protected $_virtual = ['full_name'];

    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];

    protected function _getFullName()
    {
        return $this->_properties['name'] . ' ' . $this->_properties['name_en'];
    }

    protected function _getLabel()
    {
         return $this->_properties['name'] ? $this->_properties['name'] : $this->_properties['name_en'];
    }
}
