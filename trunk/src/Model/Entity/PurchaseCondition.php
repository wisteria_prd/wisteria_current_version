<?php

namespace App\Model\Entity;

use Cake\ORM\Entity;

class PurchaseCondition extends Entity
{
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];

}