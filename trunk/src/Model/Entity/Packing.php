<?php

namespace App\Model\Entity;

use Cake\ORM\Entity;

class Packing extends Entity
{
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];
}