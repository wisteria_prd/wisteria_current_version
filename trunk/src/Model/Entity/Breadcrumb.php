<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

class Breadcrumb extends Entity
{
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];
}
