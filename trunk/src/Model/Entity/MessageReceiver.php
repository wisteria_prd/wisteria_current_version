<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

class MessageReceiver extends Entity
{
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];

}
