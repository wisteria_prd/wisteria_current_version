<?php

namespace App\Model\Entity;

use Cake\ORM\Entity;

class ModuleControl extends Entity
{
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];
}
