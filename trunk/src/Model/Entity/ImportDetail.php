<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

class ImportDetail extends Entity
{
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];

    protected function _getIsFocs()
    {
        return ($this->is_foc) ? __('TXT_YES') : __('TXT_NO');
    }
}