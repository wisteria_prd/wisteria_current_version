<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

class PersonInCharge extends Entity
{
    protected $_virtual = [
        'full_name',
        'full_name_en',
    ];

    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];

    protected function _getFullName()
    {
        return $this->_properties['name'] . ' ' . $this->_properties['name_en'];
    }

    protected function _getFullNameEn()
    {
        return $this->first_name_en . ' ' . $this->last_name_en;
    }
}
