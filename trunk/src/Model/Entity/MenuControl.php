<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

class MenuControl extends Entity
{
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];
}