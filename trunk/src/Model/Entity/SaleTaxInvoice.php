<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

class SaleTaxInvoice extends Entity
{
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];
}
