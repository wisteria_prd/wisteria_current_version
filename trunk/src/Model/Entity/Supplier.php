<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;
use Cake\ORM\TableRegistry;

class Supplier extends Entity
{
    protected $_virtual = [
        'person_in_charges',
        'full_name',
    ];

    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];

    protected function _getPersonInCharges()
    {
        $table = TableRegistry::get('PersonInCharges');
        $id = isset($this->_properties['id']) ? $this->_properties['id'] : 0;
        $total = $table->find('all')
            ->where([
                'PersonInCharges.external_id' => $id,
                'PersonInCharges.type' => TYPE_SUPPLIER,
            ])->count();

        return $total;
    }

    protected function _getFullName()
    {
        return $this->_properties['name'] . ' ' . $this->_properties['name_en'];
    }
}
