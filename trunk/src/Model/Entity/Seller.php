<?php

namespace App\Model\Entity;

use Cake\ORM\Entity;

class Seller extends Entity
{
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];

    // get label
    protected function _getLabel()
    {
        $name = '';
        switch ($this->_properties['type']) {
            case TYPE_MANUFACTURER:
                $name = $this->_properties['manufacturer']['name_en'] ? $this->_properties['manufacturer']['name_en'] : $this->_properties['manufacturer']['name'];
                break;

            case TYPE_SUPPLIER:
                $name = $this->_properties['supplier']['name_en'] ? $this->_properties['supplier']['name_en'] : $this->_properties['supplier']['name'];
                break;
        }
        return $name;
    }

}
