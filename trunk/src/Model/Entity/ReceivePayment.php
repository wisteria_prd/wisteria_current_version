<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

class ReceivePayment extends Entity
{
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];

    protected function _getFormatDate()
    {
        return date('Y年m月d日', strtotime($this->pay_date));
    }

    protected function _getFormatDateEn()
    {
        return date('Y-m-d', strtotime($this->pay_date));
    }
}