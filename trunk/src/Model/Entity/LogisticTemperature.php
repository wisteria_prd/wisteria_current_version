<?php

namespace App\Model\Entity;

use Cake\ORM\Entity;

class LogisticTemperature extends Entity
{
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];
}