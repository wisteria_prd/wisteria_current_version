<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

class AffiliationClass extends Entity
{
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];
}