<?php

namespace App\Model\Entity;

use Cake\ORM\Entity;
use Cake\Network\Session;

class Sale extends Entity
{

    protected $_virtual = ['currency_formate'];

    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];

    protected function _getCurrencyFormate()
    {
        $code_en  = $this->_properties['currency']['code_en'];
        $code_jp  = $this->_properties['currency']['code'];

        $ammount = isset($this->_properties['sale_details'][0]) ? $this->_properties['sale_details'][0]['total_amount'] : 0;
        return $this->format($code_en, $code_jp, $ammount);
    }

    public function format($code_en = null, $code_jp = null, $amount = null)
    {
        $format = '';
        switch ($code_en) {
            case 'USD':
            case 'EUR':
            case 'SGD':
            case 'GBP':
                $format = number_format($amount, 2);
                break;

            case 'KRW':
            case 'JPY':
                $format = number_format($amount);
                break;
        }
        return $this->checkLanguageAndGetPrice($code_en, $code_jp, $format);
    }

    private function checkLanguageAndGetPrice($code_en, $code_jp, $format)
    {
        $result = '';
        $session = new Session();
        $lang_session = $session->read('tb_field');
        if ($lang_session === '_en') {
            $result = $code_en. ' '.$format;
        } else {
            $result = $format. ' '.$code_jp;
        }
        return $result;
    }
}
