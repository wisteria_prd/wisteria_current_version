<?php

namespace App\Model\Entity;

use Cake\ORM\Entity;

class Payment extends Entity
{
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];

    protected function _getFormatDate()
    {
        return date('Y年m月d日', strtotime($this->payment_date));
    }

    protected function _getFormatDateEn()
    {
        return date('Y-m-d', strtotime($this->payment_date));
    }

    protected function _getBankChargeFormatDate()
    {
        return date('Y年m月d日', strtotime($this->bank_charge_date));
    }

    protected function _getBankChargeFormatDateEn()
    {
        return date('Y-m-d', strtotime($this->bank_charge_date));
    }
}