<?php
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Auth\DefaultPasswordHasher;
use Cake\Validation\Validator;

class UsersTable extends Table
{
    public function initialize(array $config)
    {
        parent::initialize($config);
        $this->primaryKey('id');
        $this->addBehavior('Timestamp');

        $this->belongsTo('Agents', [
            'foreignKey' => 'external_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('UserGroups', [
            'foreignKey' => 'user_group_id',
            'joinType' => 'INNER'
        ]);
        $this->hasMany('Messages', [
            'foreignKey' => 'user_id',
            'dependent' => false,
        ]);
        $this->hasMany('Purchases', [
            'foreignKey' => 'user_id',
            'dependent' => false,
        ]);
        $this->hasMany('Replies', [
            'foreignKey' => 'user_id',
            'dependent' => false,
        ]);
        $this->hasMany('MessageReceivers', [
            'foreignKey' => 'user_id',
            'dependent' => false,
        ]);
        $this->hasMany('Customers', [
            'foreignKey' => 'user_id',
            'dependent' => false,
        ]);
        $this->hasMany('MedicalCorporations', [
            'foreignKey' => 'user_id',
            'dependent' => false,
        ]);
        $this->hasMany('Sale1s', [
            'foreignKey' => 'user_id',
            'dependent' => false,
        ]);
        $this->hasMany('Sales', [
            'foreignKey' => 'user_id',
            'dependent' => false,
        ]);
        $this->hasMany('ReceivePayments', [
            'foreignKey' => 'receiver_id',
            'dependent' => false,
        ]);
    }

    public function validationDefault(Validator $validator)
    {
        $validator
            ->requirePresence('email', 'create')
            ->notEmpty('email', __d('validate', 'TXT_MESSAGE_REQUIRED'))
            ->add('email', 'unique', [
                'rule' => 'validateUnique',
                'provider' => 'table',
                'message' => __d('validate', 'TXT_MESSAGE_USERS_EMAIL_DUPLICATE'),
                'on' => 'create',
            ]);
        $validator
            ->allowEmpty('password', __d('validate', 'TXT_MESSAGE_REQUIRED'), function($context) {
                if (empty($context['data']['password'])) {
                    return false;
                }
                return true;
            })
            ->add('password', 'validFormat', [
                'rule' => [
                    'custom',
                    '/^(?=(?:.*[A-Z]))(?=(?:.*[a-z]))(?=(?:.*\d))(?=(?:.*[?\'\"!@#$%^&*()\-_=+{};:,<.>]))(.{6,})$/',
                ],
                'message' => __d('validate', 'TXT_MESSAGE_USERS_PASSWORD_INVALID_LENGTH_FORMAT')
            ]);
        $validator
            ->allowEmpty('password_confirm', __d('validate', 'TXT_MESSAGE_REQUIRED'), function($context) {
                if (empty($context['data']['password'])) {
                    return false;
                }
                return true;
            })
            ->add('password_confirm', [
                'match' => [
                    'rule' => ['compareWith', 'password'],
                    'message' => __d('validate', 'TXT_MESSAGE_USERS_PASSWORD_NOT_MATCH')
                ]
            ]);
        $validator
            ->requirePresence('user_group_id', 'create')
            ->notEmpty('user_group_id', __d('validate', 'TXT_MESSAGE_REQUIRED'));
        $validator
            ->requirePresence('affiliation_class', 'create')
            ->notEmpty('affiliation_class', __d('validate', 'TXT_MESSAGE_REQUIRED'));
        $validator
            ->requirePresence('lastname', 'create')
            ->notEmpty('lastname', __d('validate', 'TXT_MESSAGE_REQUIRED'));
        $validator
            ->requirePresence('firstname', 'create')
            ->notEmpty('firstname', __d('validate', 'TXT_MESSAGE_REQUIRED'));
        $validator
            ->requirePresence('lastname_en', 'create')
            ->notEmpty('lastname_en', __d('validate', 'TXT_MESSAGE_REQUIRED'));
        $validator
            ->requirePresence('firstname_en', 'create')
            ->notEmpty('firstname_en', __d('validate', 'TXT_MESSAGE_REQUIRED'));
        $validator
            ->notEmpty('department', __d('validate', 'TXT_MESSAGE_REQUIRED'));
        return $validator;
    }

    public function validationDepartment(Validator $validator)
    {
        $validator
            ->requirePresence('department', 'update', __d('validate', 'TXT_MESSAGE_REQUIRED'))
            ->notEmpty('department', __d('validate', 'TXT_MESSAGE_REQUIRED'));
        return $validator;
    }

    public function validationPassword(Validator $validator )
    {
        $validator
                ->notEmpty('old_passowrd')
                ->add('old_passowrd', [
                    'length' => [
                        'rule' => ['minLength', 6],
                        'message' => __d('validate', 'TXT_MESSAGE_USERS_PASSWORD_INVALID_LENGTH'),
                    ],
                    'valid' => [
                        'rule' => function ($value, $context) {
                            $user = $this->get($context['data']['id']);
                            if (!$user) {
                                return false;
                            }
                            if ((new DefaultPasswordHasher)->check($value, $user->password)) {
                                return true;
                            }
                            return false;
                        },
                        'message' => __d('validate', 'TXT_MESSAGE_USERS_PASSWORD_NOT_MATCH'),
                    ],
                ]);
        $validator
                ->notEmpty('password')
                ->add('password', [
                    'length' => [
                        'rule' => ['minLength', 6],
                        'message' => __d('validate', 'TXT_MESSAGE_USERS_PASSWORD_INVALID_LENGTH'),
                    ],
                    'match' => [
                        'rule' => ['compareWith', 'confirm_passowrd'],
                        'message' => __d('validate', 'TXT_MESSAGE_USERS_PASSWORD_NOT_MATCH'),
                    ]
                ]);
        $validator
                ->notEmpty('confirm_passowrd')
                ->add('confirm_passowrd', [
                    'length' => ['rule' => ['minLength', 6],
                        'message' => __d('validate', 'TXT_MESSAGE_USERS_PASSWORD_INVALID_LENGTH'),
                    ],
                    'match'=>[
                        'rule' => ['compareWith', 'password'],
                        'message' => __d('validate', 'TXT_MESSAGE_USERS_PASSWORD_NOT_MATCH'),
                    ]
                ]);
        return $validator;
    }

    public function validationNewPassword(Validator $validator )
    {
        $validator
                ->notEmpty('password')
                ->add('password', [
                    'length' => [
                        'rule' => ['minLength', 6],
                        'message' => __d('validate', 'TXT_MESSAGE_USERS_PASSWORD_INVALID_LENGTH'),
                    ],
                    'match' => [
                        'rule' => ['compareWith', 'password_confirm'],
                        'message' => __d('validate', 'TXT_MESSAGE_USERS_PASSWORD_NOT_MATCH'),
                    ]
                ]);
        $validator
                ->notEmpty('password_confirm')
                ->add('password_confirm', [
                    'length' => ['rule' => ['minLength', 6],
                        'message' => __d('validate', 'TXT_MESSAGE_USERS_PASSWORD_INVALID_LENGTH'),
                    ],
                    'match'=>[
                        'rule' => ['compareWith', 'password'],
                        'message' => __d('validate', 'TXT_MESSAGE_USERS_PASSWORD_NOT_MATCH'),
                    ]
                ]);
        return $validator;
    }

    public function validationTcAgreement(Validator $validator )
    {
        $validator
                ->requirePresence('tc_agreement')
                ->notEmpty('tc_agreement', __d('validate', 'TXT_MESSAGE_REQUIRED'));
        return $validator;
    }

    public function validationPpAgreement(Validator $validator )
    {
        $validator
                ->requirePresence('pp_agreement')
                ->notEmpty('pp_agreement', __d('validate', 'TXT_MESSAGE_REQUIRED'));
        return $validator;
    }
}
