<?php
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

class ProductDetailsTable extends Table
{
    public function initialize(array $config)
    {
        $this->primaryKey('id');
        $this->addBehavior('Timestamp');

        $this->belongsTo('Products', [
            'foreignKey' => false,
            'conditions' => 'Products.id = ProductDetails.product_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('LogisticTemperatures', [
            'foreignKey' => false,
            'conditions' => 'LogisticTemperatures.id = ProductDetails.logistic_temperature_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('PackSizes', [
            'foreignKey' => false,
            'conditions' => 'PackSizes.id = ProductDetails.pack_size_id',
            'propertyName' => 'tb_pack_sizes',
            'joinType' => 'LEFT',
        ]);
        $this->belongsTo('SingleUnits', [
            'foreignKey' => false,
            'conditions' => 'SingleUnits.id = ProductDetails.single_unit_id',
            'joinType' => 'LEFT',
        ]);
        $this->belongsTo('Packings', [
            'foreignKey' => false,
            'conditions' => 'Packings.id = ProductDetails.packing_id',
            'joinType' => 'INNER',
        ]);
        $this->hasMany('Pricing', [
            'foreignKey' => 'external_id',
            'dependent' => false,
        ]);
        $this->hasMany('ProductPackageDetails', [
            'foreignKey' => 'product_detail_id',
            'dependent' => false,
        ]);
        $this->hasOne('FocDiscounts', [
            'dependent' => true,
        ]);
        $this->hasOne('SellerProductDetails', [
            'dependent' => true,
        ]);
        $this->hasOne('WStandardPrices', [
            'dependent' => true,
        ]);
        $this->hasOne('PurchaseDetails', [
            'dependent' => false,
        ]);
        $this->hasOne('SaleDetails', [
            'dependent' => false,
        ]);
        $this->hasOne('SellerProducts', [
            'foreignKey' => 'product_detail_id',
            'dependent' => false,
        ]);
        $this->hasMany('ImportDetails', [
            'foreignKey' => 'product_detail_id',
            'dependent' => false,
        ]);
        $this->hasMany('Stocks', [
            'foreignKey' => 'product_detail_id',
            'dependent' => false,
        ]);
        $this->hasMany('PricingManagements', [
            'foreignKey' => 'product_detail_id',
            'dependent' => false,
        ]);
    }

    public function validationDefault(Validator $validator)
    {
        $validator
            ->requirePresence('pack_size')
            ->notEmpty('pack_size', __d('validate', 'TXT_MESSAGE_REQUIRED'))
            ->add('pack_size', [
                'numeric' => [
                    'rule' => 'numeric',
                    'message' => 'Invalid Number',
                ],
            ]);
        $validator
            ->requirePresence('single_unit_size')
            ->notEmpty('single_unit_size', __d('validate', 'TXT_MESSAGE_REQUIRED'))
            ->add('single_unit_size', [
                'numeric' => [
                    'rule' => 'numeric',
                    'message' => 'Invalid Number',
                ],
            ]);
        $validator
            ->requirePresence('pack_size_id')
            ->notEmpty('pack_size_id', __d('validate', 'TXT_MESSAGE_REQUIRED'));
        $validator
            ->requirePresence('single_unit_id')
            ->notEmpty('single_unit_id', __d('validate', 'TXT_MESSAGE_REQUIRED'));
        $validator
            ->requirePresence('logistic_temperature_id')
            ->notEmpty('logistic_tempearaturer_id', __d('validate', 'TXT_MESSAGE_REQUIRED'));
        return $validator;
    }

    public function detailsDropdown()
    {
        $products = $this->find()
            ->contain([
                'Products' => [
                    'ProductBrands',
                    'queryBuilder' => function($q) {
                        return $q->where(['Products.is_suspend' => 0]);
                    },
                ],
                'PackSizes',
                'SingleUnits'
            ])
            ->where([
                'ProductDetails.is_suspend' => 0
            ]);
        return $products;
    }

    /**
     *  product details drop down filtered by product id.
     * @param integer $product_id
     * @return object
     */
    public function productDetailsDropdown($product_id = null)
    {
        $products = $this->find()
            ->contain([
                'PackSizes',
                'SingleUnits'
            ])
            ->where([
                'ProductDetails.is_suspend' => 0,
                'ProductDetails.product_id' => $product_id
            ]);
        return $products;
    }

    public function getAllByProductId($id)
    {
        $data = $this->find('all', [
            'conditions' => [
                'ProductDetails.product_id' => $id
            ],
            'join' => [
                'PackSizes' => [
                    'table' => 'pack_sizes',
                    'type' => 'LEFT',
                    'conditions' => 'PackSizes.id = ProductDetails.pack_size_id',
                ],
                'SingleUnits' => [
                    'table' => 'single_units',
                    'type' => 'LEFT',
                    'conditions' => 'SingleUnits.id = ProductDetails.single_unit_id',
                ]
            ]
        ])->toArray();

        return $data;
    }

    public function getListByProductId($id, $en)
    {
        $data = [];
        $list = $this->findByProductId($id)
            ->contain(['PackSizes', 'SingleUnits'])->toArray();

        if ($list) {
            foreach ($list as $key => $value) {
                $data[$value->id] = $this->concatName($value, $en);
            }
        }

        return $data;
    }

    /**
     * Function getListByBrandId
     * use for get data list by brand_id
     * @param int $brandId
     * @param string $local
     * @return array
     */
    public function getListByBrandId($brandId, $local)
    {
        $data = [];

        $list = $this->find('all')
            ->contain([
                'PackSizes',
                'SingleUnits',
                'Products' => [
                    'ProductBrands' => function ($q) use ($brandId) {
                        return $q->where(['ProductBrands.id' => $brandId]);
                    }
                ]
            ])->toArray();

        if ($list) {
            foreach ($list as $key => $value) {
                $data[$value->id] = $value->product->name . $local . ' ( ' . $this->concatName($value, $local) . ' ) ';
            }
        }

        return $data;
    }

    /**
     * Function concatName
     * use for concat name of product_detail
     * @param object $data data with associate
     * @param string $en local store session
     * @param string $product_name name of product
     * @return string
     */
    private function concatName($data, $en, $product_name = null)
    {
        $name = '';

        if ($product_name !== null) {
            $name .= $product_name . ' ';
        }

        $pack_size = $data->tb_pack_sizes->name . $en;
        $single_unit = $data->single_unit->name . $en;
        $name .= $data->pack_size . ' ' . $pack_size . ' X ' . $data->single_unit_size . ' ' . $single_unit;

        return $name;
    }
}
