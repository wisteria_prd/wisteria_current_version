<?php

namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

class SaleDetailsTable extends Table
{
    public function initialize(array $config)
    {
        $this->primaryKey('id');
        $this->addBehavior('Timestamp');
        $this->addBehavior('Orm');

        $this->belongsTo('Sales', [
            'foreignKey' => 'sale_id',
            'joinType' => 'INNER',
        ]);

        $this->belongsTo('ProductDetails');

        $this->hasMany('SaleStockDetails', [
            'foreinKey' => 'sale_detail_id',
            'dependent' => true,
        ]);
    }

    public function validationDefault(Validator $validator) {
        parent::validationDefault($validator);
        $validator
            ->requirePresence('product_detail_id')
            ->notEmpty('product_detail_id', __('TXT_MESSAGE_REQUIRED'));

        $validator
            ->requirePresence('quantity')
            ->notEmpty('quantity', __('TXT_MESSAGE_REQUIRED'))
            ->add('quantity', 'valid', [
                'rule' => function ($value) {
                    return $value > 0;
                },
                'message' => __('TXT_MESSAGE_QTY_GREATER_THAN_ZERO'),
            ]);

        $validator
            ->requirePresence('unit_price')
            ->allowEmpty('unit_price', __('TXT_MESSAGE_REQUIRED'))
            ->add('unit_price', 'valid', [
                'rule' => function ($value) {
                    return $value >= 0;
                },
                'message' => __('TXT_MESSAGE_VALUE_GREATER_THAN_ZERO'),
            ]);

        return $validator;
    }

    /**
     * Function getListBySellerId
     * use for get sale detail by seller_id
     * @param int $seller_id
     * @return object
     */
    public function getListBySaleId($sale_id)
    {
        $data = [];
        $data1 = $this->find('all')
            ->where(['SaleDetails.sale_id' => $sale_id])->toArray();

        if ($data1) {
            foreach ($data1 as $key => $value) {
                $data[] = [
                    'id' => $value->id,
                    'sale_id' => $value->sale_id,
                    'product_detail_id' => $value->product_detail_id,
                    'parent_id' => $value->parent_id,
                    'brand_name' => $value->brand_name,
                    'product_name' => $value->product_name,
                    'single_unit_value' => $value->single_unit_value,
                    'single_unit_name' => $value->single_unit_name,
                    'pack_size_value' => $value->pack_size_value,
                    'pack_size_name' => $value->pack_size_name,
                    'quantity' => $value->quantity,
                    'packing_id' => $value->packing_id,
                    'original_unit_price' => $value->original_unit_price,
                    'discount_on_unit_price' => $value->discount_on_unit_price,
                    'unit_price' => $value->unit_price,
                    'full_name' => $this->concatName($value),
                    'created' => $value->created,
                    'modified' => $value->modified,
                ];
            }
        }

        return $data;
    }

    /**
     * Function getAllBySaleId
     * use for retrieve data from SaleDetails
     * @param int $saleId
     * @param type $type finding type such as: List, All, Threaded
     * @return object
     */
    public function getAllBySaleId($saleId, $type)
    {
        $data = $this->find($type)
            ->where(['SaleDetails.sale_id' => $saleId])->toArray();

        return $data;
    }

    /**
     * Fucntion concatName
     * use for concat name of product details
     * @param object $data
     * @return string name of product detail
     */
    private function concatName($data)
    {
        $name = $data->product_name . ' ' . $data->single_unit_value . ' ' .$data->single_unit_name . ' ';
        $name .= $data->pack_size_value . ' ' . $data->pack_size_name;

        return $name;
    }
}