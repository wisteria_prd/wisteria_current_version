<?php
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

class CustomerSubsidiariesTable extends Table
{
    public function initialize(array $config)
    {
        parent::initialize($config);
        $this->primaryKey('id');

        $this->belongsTo('Customers', [
            'foreignKey' => 'customer_id',
            'joinType' => 'LEFT',
        ]);
        $this->belongsTo('Subsidiaries', [
            'foreignKey' => 'subsidiary_id',
            'joinType' => 'LEFT',
        ]);
    }

    public function validationCustomer(Validator $validator)
    {
        $validator
            ->requirePresence('customer_id')
            ->notEmpty('customer_id', __d('validate', 'TXT_MESSAGE_REQUIRED'));
        $validator
            ->requirePresence('subsidiary_id')
            ->notEmpty('subsidiary_id', __d('validate', 'TXT_MESSAGE_REQUIRED'));
        return $validator;
    }
}
