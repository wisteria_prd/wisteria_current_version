<?php
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

class SocietiesTable extends Table
{
    public function initialize(array $config) {
        parent::initialize($config);
        $this->primaryKey('id');

        $this->hasMany('DoctorSocieties', [
            'foreignKey' => 'society_id',
            'dependent' => false,
        ]);
    }

    public function validationDefault(Validator $validator)
    {
        $validator
            ->requirePresence('name')
            ->notEmpty('name', __d('validate', 'TXT_MESSAGE_REQUIRED'));
        $validator
            ->requirePresence('name_en')
            ->notEmpty('name_en', __d('validate', 'TXT_MESSAGE_REQUIRED'));
        $validator
            ->requirePresence('url')
            ->notEmpty('url', __d('validate', 'TXT_MESSAGE_REQUIRED'))
            ->add('url', 'valid', [
                'rule' => 'url',
                'message' => __d('validate', 'TXT_MESSAGE_INVALID_URL'),
            ]);
        return $validator;
    }
}