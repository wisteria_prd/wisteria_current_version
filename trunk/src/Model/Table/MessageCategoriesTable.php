<?php
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

class MessageCategoriesTable extends Table
{
    public function initialize(array $config)
    {
        parent::initialize($config);
        $this->primaryKey('id');
        $this->addBehavior('Timestamp');

        $this->hasMany('Messages', [
            'foreignKey' => 'message_category_id',
            'dependent' => true,
        ]);
    }

    public function validationDefault(Validator $validator)
    {
        parent::validationDefault($validator);
        $validator
                ->requirePresence('name_en', 'create')
                ->notEmpty('name_en',  __('TXT_MESSAGE_REQUIRED'));
        $validator
                ->requirePresence('name', 'create')
                ->notEmpty('name',  __('TXT_MESSAGE_REQUIRED'));
        return $validator;
    }
}