<?php
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

class SaleStockDetailsTable extends Table
{
    public function initialize(array $config) {
        parent::initialize($config);
        $this->primaryKey('id');
        $this->addBehavior('Orm');
        $this->addBehavior('Timestamp');

        $this->belongsTo('SaleDetails', [
            'foreignKey' => 'sale_detail_id',
            'joinType' => 'INNER',
        ]);

        $this->belongsTo('Stocks', [
            'foreignKey' => 'stock_id',
            'joinType' => 'INNER',
        ]);
    }

    /**
     * Function getBySaleDetailId
     * use for retrieve all data by sale_detail_id
     * @param int $saleDetailId
     * @return object
     */
    public function getBySaleDetailId($saleDetailId)
    {
        $data = $this->findBySaleDetailId($saleDetailId);

        return $data->toArray();
    }

    /**
     * Function deleteBySaleDetailId
     * use for delete multiple records by sale_detail_id
     * @param int $saleDetailId
     * @return object
     */
    public function deleteBySaleDetailId($saleDetailId)
    {
        $this->deleteAll(['SaleStockDetails.sale_detail_id IN' => $saleDetailId]);
    }
}