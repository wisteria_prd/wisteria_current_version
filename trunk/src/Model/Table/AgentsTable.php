<?php
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

class AgentsTable extends Table
{
    public function initialize(array $config)
    {
        parent::initialize($config);
        $this->table('agents');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->hasMany('User', [
            'foreignKey' => 'external_id',
            'joinType' => 'INNER'
        ]);
    }

    public function validationDefault(Validator $validator)
    {
        $validator
            ->requirePresence('name')
            ->notEmpty('lastname', __('TXT_MESSAGE_REQUIRED'));

        return $validator;
    }

}