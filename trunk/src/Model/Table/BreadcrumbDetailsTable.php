<?php
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

class BreadcrumbDetailsTable extends Table
{
    public function initialize(array $config)
    {
        parent::initialize($config);
        $this->primaryKey('id');

        $this->belongsTo('Breadcrumbs', [
            'foreignKey' => 'breadcrumb_id',
            'joinType' => 'INNER',
        ]);
    }
}
