<?php
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

class SubsidiariesTable extends Table
{
    public function initialize(array $config)
    {
        parent::initialize($config);
        $this->primaryKey('id');
        $this->addBehavior('Timestamp');

        $this->hasMany('PersonInCharges', [
            'foreignKey' => 'external_id',
            'joinType' => 'INNER'
        ]);

        $this->hasMany('InfoDetails', [
            'foreignKey' => 'external_id',
            'dependent' => false,
        ]);

        $this->hasMany('CustomerGroups', [
            'foreignKey' => 'external_id',
            'dependent' => false,
        ]);

        $this->hasMany('CustomerSubsidiaries', [
            'foreignKey' => 'subsidiary_id',
            'dependent' => false,
        ]);

        $this->hasMany('ReceivePayments', [
            'foreignKey' => 'subsidiary_id',
            'dependent' => false,
        ]);

        $this->belongsTo('PaymentCategories', [
            'foreignKey' => 'payment_category_id',
            'joinType' => 'INNER'
        ]);

        $this->belongsTo('InvoiceDeliveryMethods', [
            'foreignKey' => 'invoice_delivery_method_id',
            'joinType' => 'INNER'
        ]);

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'LEFT'
        ]);

        $this->belongsTo('MedicalCorporations', [
            'foreignKey' => 'external_id',
            'joinType' => 'LEFT',
            'conditions' => ['Subsidiaries.type' => TYPE_MEDICAL_CORP]
        ]);

        $this->belongsTo('Countries', [
            'foreignKey' => 'country_id',
            'joinType' => 'LEFT',
        ]);
    }

    public function validationDefault(Validator $validator)
    {
        $validator
            ->requirePresence('name_en')
            ->notEmpty('name_en', __d('validate', 'TXT_MESSAGE_REQUIRED'));
        $validator
            ->requirePresence('url')
            ->allowEmpty('url')
            ->add('url', 'valid', [
                'rule' => 'url',
                'message' => __d('validate', 'TXT_MESSAGE_INVALID_URL'),
            ]);
        $validator
            ->requirePresence('si_email')
            ->allowEmpty('si_email')
            ->add('si_email', 'valid', [
                'rule' => 'email',
                'message' => __d('validate', 'TXT_MESSAGE_INVALID_EMAIL'),
            ]);
        return $validator;
    }

    public function getAutocompleteList($keyword = null)
    {
        $data = $this->find('all')
                ->where(['is_suspend' => 0])
                ->where([
                    'OR' => [
                        'name LIKE' => '%' . $keyword . '%',
                        'name_en LIKE' => '%' . $keyword . '%',
                    ]
                ])
                ->limit(20);
        return $data;
    }
}
