<?php
namespace App\Model\Table;

use Cake\ORM\Table;

class SaleReceivePaymentsTable extends Table
{
    public function initialize(array $config)
    {
        parent::initialize($config);
        $this->primaryKey('id');
        $this->addBehavior('Timestamp');
    }
}