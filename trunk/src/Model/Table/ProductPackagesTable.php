<?php
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

class ProductPackagesTable extends Table
{
    public function initialize(array $config)
    {
        parent::initialize($config);
        $this->primaryKey('id');
        $this->addBehavior('Timestamp');

        $this->hasMany('ProductPackageDetails', [
            'foreignKey' => 'product_package_id',
            'dependent' => false,
        ]);
        $this->hasMany('Pricing', [
            'foreignKey' => 'external_id',
            'dependent' => false,
        ]);
        $this->belongsTo('Currencies', [
            'foreignKey' => 'currency_id',
            'joinType' => 'INNER'
        ]);
    }

    public function validationDefault(Validator $validator)
    {
        $validator
            ->requirePresence('name')
            ->notEmpty('name', __d('validate', 'TXT_MESSAGE_REQUIRED'));
        $validator
            ->requirePresence('unit_price')
            ->notEmpty('unit_price', __d('validate', 'TXT_MESSAGE_REQUIRED'));
        $validator
            ->requirePresence('currency_id')
            ->notEmpty('currency_id', __d('validate', 'TXT_MESSAGE_REQUIRED'));
        return $validator;
    }

    public function packagesDropdown($en = null, $type = 'all')
    {
        $data = $this->find($type, [
            'keyField' => 'id',
            'valueField' => 'name' . $en,
            'conditions' => ['is_suspend' => 0]
        ]);
        return $data;
    }
}
