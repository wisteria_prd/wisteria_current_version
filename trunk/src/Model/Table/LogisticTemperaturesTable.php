<?php

namespace App\Model\Table;

use Cake\ORM\Table;

class LogisticTemperaturesTable extends Table
{
    public function initialize(array $config)
    {
        $this->primaryKey('id');

        $this->hasMany('ProductDetails', [
            'foreignKey' => 'logistic_temperature_id',
            'dependent' => false,
        ]);
    }
}