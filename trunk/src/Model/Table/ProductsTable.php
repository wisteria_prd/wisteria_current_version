<?php
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

class ProductsTable extends Table
{

    public function initialize(array $config)
    {
        $this->primaryKey('id');
        $this->addBehavior('Timestamp');
        $this->addBehavior('Orm');

        $this->belongsTo('ProductBrands', [
            'foreignKey' => 'product_brand_id',
            'joinType' => 'INNER',
        ]);
        $this->hasMany('ProductDetails', [
            'foreignKey' => 'product_id',
            'dependent' => false,
        ]);
        $this->hasMany('Medias', [
            'foreignKey' => 'external_id',
            'dependent' => false,
        ]);
        $this->hasMany('SellerProducts', [
            'foreignKey' => 'product_id',
            'dependent' => false,
        ]);
    }

    public function validationDefault(Validator $validator)
    {
        $validator
            ->requirePresence('name_en', 'create')
            ->notEmpty('name_en', __('TXT_MESSAGE_REQUIRED'));

        $validator
            ->requirePresence('code')
            ->notEmpty('code', __('TXT_MESSAGE_REQUIRED'));

        $validator
            ->requirePresence('type')
            ->notEmpty('type', __('TXT_MESSAGE_REQUIRED'));

        $validator
            ->requirePresence('url')
            ->allowEmpty('url')
            ->add('url', [
                'urlWithProtocol' => [
                    'rule' => ['url', 'http://', 'https://'],
                    'message' => __('TXT_MESSAGE_INVALID_URL_FORMAT'),
                ]
            ]);

        return $validator;
    }

    /**
     * products dropdown base on or not brand_id
     * @param integer $brand_id brand id
     * @param string $en default null|_en
     * @param string $type default all|list
     * @return array|object
     */
    public function productsDropdown($brand_id = null, $en = null, $type = 'all')
    {
        $conditions['is_suspend'] = 0;
        if (!is_null($brand_id)) {
            $conditions['product_brand_id'] = $brand_id;
        }

        $data = $this->find($type, [
            'keyField' => 'id',
            'valueField' => 'name' . $en,
            'conditions' => $conditions
        ]);

        if ($type == 'list') {
            return $data->toArray();
        }

        return $data;
    }

    /**
     *  product details drop down filtered by product id.
     * @param integer $product_id
     * @return object
     */
    public function productDetailsDropdown($product_id = [])
    {
        $conditions['Products.is_suspend'] = 0;
        if (!empty($product_id)) {
            $conditions['Products.id IN '] = $product_id;
        }

        $products = $this->find()
            ->contain([
                'ProductBrands',
                'ProductDetails' => [
                    'PackSizes',
                    'SingleUnits',
                    'queryBuilder' => function($q) {
                        return $q->where(['ProductDetails.is_suspend' => 0]);
                    },
                ]
            ])
            ->where($conditions);
        return $products;
    }

    /**
     * Function getAllProductByBrandId
     * use for get all product by product_brand_id
     * @param int $id
     * @return object
     */
    public function getAllProductByBrandId($id)
    {
        $data = $this->findByProductBrandId($id)->toArray();
        return $data;
    }

    /**
     * Function getProductListByBrandId
     * use get product list by product_brand_id
     * @param int $id
     * @param string $key_field
     * @param string $en
     * @return array
     */
    public function getProductListByBrandId($id, $key_field, $en)
    {
        $data = $this->find('list', [
            'keyField' => $key_field,
            'valueField' => 'name',
            'conditions' => [
                'name' . $en . ' <>' => '',
                'product_brand_id' => $id,
            ]
        ]);

        return $data;
    }

    public function getSingeProductWithDetails($product_id)
    {
        $product = $this->get($product_id, [
            'contain' => [
                'ProductBrands',
                'ProductDetails' => [
                    'PackSizes',
                    'SingleUnits',
                    'queryBuilder' => function($q) {
                        return $q->where(['ProductDetails.is_suspend' => 0]);
                    },
                ],
            ],
            'conditions' => [
                'Products.is_suspend' => 0
            ]
        ]);
        return $product;
    }

    public function getAvailableListByProductBrandId($bId = null)
    {
        $data = $this->find('list', [
            'keyField' => 'id',
            'keyValue' => function($data) {
                return $this->get('label');
            }
        ])->where([
            'Products.product_brand_id' => $bId,
            'Products.is_suspend' => 0,
        ]);
        return $data->toArray();
    }
}
