<?php
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

class StocksTable extends Table
{
    public function initialize(array $config) {
        parent::initialize($config);
        $this->primaryKey('id');
        $this->addBehavior('Timestamp');

        $this->hasMany('SaleStockDetails', [
            'foreignKey' => 'stock_id',
            'dependent' => false,
        ]);

        $this->belongsTo('ImportDetails', [
            'foreignKey' => 'import_detail_id',
            'joinType' => 'INNER',
        ]);

        $this->belongsTo('PurchaseDetails', [
            'foreignKey' => 'purchase_detail_id',
            'joinType' => 'LEFT',
        ]);
    }

    public function createStock ($request, $affiliation, $import_detail_id)
    {
        $data = [];
        $level = [];
        if (!empty($request['damage_a_quanity'])) {
            $level[LEVEL_A] = $request['damage_a_quanity'];
        }

        if (!empty($request['damage_b_quanity'])) {
            $level[LEVEL_B] = $request['damage_b_quanity'];
        }

        if (!empty($request['damage_c_quanity'])) {
            $level[LEVEL_C] = $request['damage_c_quanity'];
        }

        if (!empty($request['damage_f_quanity'])) {
            $level[LEVEL_F] = $request['damage_f_quanity'];
        }

        $i = 0;
        foreach ($level as $key => $value) {
            $data[$i] = [
                'import_detail_id' => $import_detail_id,
                'product_detail_id' => $request['product_detail_id'],
                //'purchase_id' => $request['purchase_id'],
                'damage_level' => $key,
                'quantity' => $value,
                'affiliation_class' => $affiliation,
                'lot_number' => $request['lot_number'],
                'expiry_date' => $request['expiry_date']
            ];
            $i++;
        }

        $stocks = $this->newEntities($data);
        return $this->saveMany($stocks, ['validate' => false]);
    }

    /**
     * Function getLotNumbet
     * use get list of lot_number from ImportDetails
     * @return array array list
     */
    public function getListLotNumber()
    {
        $data = [];

        $stocks = $this->find('all')
            ->contain(['ImportDetails'])->toArray();

        if ($stocks) {
            foreach ($stocks as $key => $value) {
                $data[$value->id] = $value->lot_number . ', Exp: ' . date('M-Y', strtotime($value->import_detail->expiry_date));
            }
        }

        return $data;
    }

    public function deductStock($data)
    {
        $stock = [];

        if ($data) {
            foreach ($data as $key => $value) {
                if ($value) {
                    foreach ($value as $key1 => $value1) {
                        $stock = $this->get($value1->stock_id);
                        $stock->quantity = ($stock->quantity - $value1->quantity);
                        $this->save($stock);
                    }
                }
            }
        }
    }
}