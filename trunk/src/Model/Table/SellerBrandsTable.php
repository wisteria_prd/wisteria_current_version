<?php
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\ORM\RulesChecker;

class SellerBrandsTable extends Table
{
    public function initialize(array $config)
    {
        $this->primaryKey('id');
        $this->addBehavior('Timestamp');
        $this->addBehavior('Orm');

        $this->belongsTo('Sellers', [
            'foreingKey' => 'seller_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('ProductBrands', [
            'foreingKey' => 'product_brand_id',
            'joinType' => 'INNER',
        ]);
        $this->hasMany('SellerProducts', [
            'foreignKey' => 'seller_brand_id',
            'dependent' => true,
        ]);
    }

    public function validationDefault(Validator $validator)
    {
        $validator
            ->requirePresence('seller_id')
            ->notEmpty('seller_id', __d('validate', 'TXT_MESSAGE_REQUIRED'));
        $validator
            ->requirePresence('manufacturer_id')
            ->notEmpty('manufacturer_id', __d('validate', 'TXT_MESSAGE_REQUIRED'));
        $validator
            ->requirePresence('product_brand_id')
            ->notEmpty('product_brand_id', __d('validate', 'TXT_MESSAGE_REQUIRED'));
        return $validator;
    }

    public function validationProductBrand(Validator $validator)
    {
        $validator
                ->requirePresence('product_brand_id')
                ->notEmpty('product_brand_id', __d('validate', 'TXT_MESSAGE_REQUIRED'));
        return $validator;
    }

    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['product_brand_id']), [
            'message' => __('TXT_MESSAGE_INVALID_DUPLICATE_VALUE'),
        ]);
        return $rules;
    }
}
