<?php
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

class InfoDetailsTable extends Table
{
    public function initialize(array $config) {
        parent::initialize($config);
        $this->primaryKey('id');

        $this->belongsTo('Manufacturers', array(
            'foreignKey' => false,
            'conditions' => 'Manufacturers.id = InfoDetails.external_id',
            'joinType' => 'INNER',
        ));
        $this->belongsTo('Suppliers', array(
            'foreignKey' => false,
            'conditions' => 'Suppliers.id = InfoDetails.external_id',
            'joinType' => 'INNER',
        ));
        $this->belongsTo('Customers', array(
            'foreignKey' => false,
            'conditions' => 'Customers.id = InfoDetails.external_id',
            'joinType' => 'INNER',
        ));
        $this->belongsTo('Doctors', array(
            'foreignKey' => false,
            'conditions' => 'Doctors.id = InfoDetails.external_id',
            'joinType' => 'INNER',
        ));
        $this->belongsTo('PersonInCharges', array(
            'foreignKey' => false,
            'conditions' => 'PersonInCharges.id = InfoDetails.external_id',
            'joinType' => 'INNER',
        ));
        $this->belongsTo('MedicalCorporations', [
            'foreignKey' => 'external_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Subsidiaries', [
            'foreignKey' => false,
            'conditions' => 'Subsidiaries.id = InfoDetails.external_id',
            'joinType' => 'INNER',
        ]);
    }

    public function validationDefault(Validator $validator)
    {
        $validator
            ->requirePresence('email')
            ->allowEmpty('email')
            ->add('email', 'valid', [
                'rule' => 'email',
                'message' => __d('validate', 'TXT_MESSAGE_INVALID_EMAIL'),
            ]);
        return $validator;
    }

    public function saveData($id, $type, $param)
    {
        $data = [];
        if (isset($param['tel'])) {
            foreach ($param['tel'] as $key => $value) {
                if ($value) {
                    $data[$key]['external_id'] = $id;
                    $data[$key]['type'] = $type;
                    $data[$key]['tel'] = $value;
                }
            }
        }
        if (isset($param['email'])) {
            foreach ($param['email'] as $key => $value) {
                if ($value) {
                    $data[$key]['external_id'] = $id;
                    $data[$key]['type'] = $type;
                    $data[$key]['email'] = $value;
                }
            }
        }
        if (isset($param['phone'])) {
            foreach ($param['phone'] as $key => $value) {
                if ($value) {
                    $data[$key]['external_id'] = $id;
                    $data[$key]['type'] = $type;
                    $data[$key]['phone'] = $value;
                }
            }
        }
        if ($data) {
            $entities = $this->newEntities($data);
            $this->saveMany($entities);
        }
    }

    public function deleteById($id)
    {
        $arr = [];
        $data = $this->find('all')
            ->select(['id'])
            ->where(['InfoDetails.external_id' => $id])->toArray();
        if ($data) {
            foreach ($data as $value) {
                $arr[] = $value->id;
            }
            $this->deleteAll(['InfoDetails.id IN' => $arr]);
        }
    }

    public function getListByExtAndId($external_id, $type)
    {
        $data = $this->find('all')
            ->where([
                'InfoDetails.type' => $type,
                'InfoDetails.external_id' => $external_id,
            ])->toArray();

        return $data;
    }
}