<?php
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

class PurchaseImportsTable extends Table
{
    public function initialize(array $config)
    {
        parent::initialize($config);
        $this->primaryKey('id');
        $this->addBehavior('Timestamp');
        $this->addBehavior('Orm');

        $this->belongsTo('Imports', [
            'foreignKey' => 'import_id',
            'joinType' => 'LEFT',
        ]);

        $this->belongsTo('Purchases', [
            'foreignKey' => 'purchase_id',
            'joinType' => 'LEFT',
        ]);

        $this->belongsTo('Sales', [
            'foreignKey' => 'sale_id',
            'joinType' => 'LEFT',
        ]);
    }

    public function validationDefault(Validator $validator)
    {
        return $validator;
    }

    public function isRequired($value, array $context)
    {
        //pr($context); exit;
        foreach ($context['data'] as $v) {
            if (!empty($v['purchase_id'])) {
                return true;
            }
            return false;
        }
    }
}