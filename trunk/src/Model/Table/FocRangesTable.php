<?php
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

class FocRangesTable extends Table
{
    public function initialize(array $config) {
        parent::initialize($config);
        $this->primaryKey('id');
        $this->addBehavior('Timestamp');

        $this->belongsTo('Focs', [
            'foreignKey' => 'foc_id',
            'joinType' => 'INNER',
        ]);

        $this->hasMany('FocDiscounts', [
            'foreignKey' => 'external_id',
            'dependent' => true,
        ]);
    }

    public function validationDefault(Validator $validator)
    {
        $validator
            ->requirePresence('range_from')
            ->notEmpty('range_from', __('TXT_MESSAGE_REQUIRED'))
            ->add('range_from', 'custom', [
                'rule' => function ($value, $context) {
                    return (int)$value > 0 ? true : false;
                },
                'message' => __('TXT_MESSAGE_VALUE_GREATER_THAN_ZERO')
            ]);

        $validator
            ->requirePresence('range_to')
            ->notEmpty('range_to', __('TXT_MESSAGE_REQUIRED'))
            ->add('range_to', 'custom', [
                'rule' => function ($value, $context) {
                    $from = $context['data']['range_from'];
                    $to = $context['data']['range_to'];
                    return ($to >= $from) ? true : false;
                },
                'message' => __('TXT_MESSAGE_MAX_RANGE_GREATER_THAN_MIN')
            ]);

        return $validator;
    }
}