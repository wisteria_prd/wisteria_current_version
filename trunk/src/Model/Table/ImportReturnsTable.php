<?php
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

class ImportReturnsTable extends Table
{
    public function initialize(array $config)
    {
        parent::initialize($config);
        $this->primaryKey('id');
        $this->addBehavior('Timestamp');

        $this->hasMany('ImportReturnDetails', [
            'foreignKey' => 'import_return_id',
            'dependent' => false,
        ]);

        $this->belongsTo('Imports', [
            'foreignKey' => 'import_id',
            'joinType' => 'INNER',
        ]);

        $this->belongsTo('Currencies', [
            'foreignKey' => 'currency_id',
            'joinType' => 'INNER',
        ]);
    }

    public function validationDefault(Validator $validator)
    {

        $validator
            ->requirePresence('currency_id')
            ->notEmpty('currency_id', __('TXT_MESSAGE_REQUIRED'));

        $validator
            ->requirePresence('expense')
            ->notEmpty('expense', __('TXT_MESSAGE_REQUIRED'));

        $validator
            ->requirePresence('report_date')
            ->notEmpty('report_date', __('TXT_MESSAGE_REQUIRED'))
            ->add('report_date', 'custom', [
                'rule' => ['date', 'ymd'],
                'message' => __('TXT_MESSAGE_INVALID_DATE_FORMAT')
            ]);

        $validator
            ->requirePresence('status')
            ->notEmpty('status', __('TXT_MESSAGE_REQUIRED'))
            ->add('status', [
                'list' => [
                    'rule' => ['inList', [STATUS_RETURNING, STATUS_RETURNED, STATUS_COMPLETED]],
                    'message' => __('TXT_MESSAGE_TYPE_SELECTION_INVALID'),
                ]
            ]);

        return $validator;
    }

}