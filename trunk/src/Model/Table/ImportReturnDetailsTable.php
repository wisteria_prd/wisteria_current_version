<?php
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

class ImportReturnDetailsTable extends Table
{
    public function initialize(array $config)
    {
        parent::initialize($config);
        $this->primaryKey('id');
        $this->addBehavior('Timestamp');

        $this->belongsTo('ImportReturns', [
            'foreignKey' => 'import_return_id',
            'joinType' => 'INNER',
        ]);

        $this->belongsTo('ImportDetails', [
            'foreignKey' => 'import_detail_id',
            'joinType' => 'INNER',
        ]);
    }

    public function validationDefault(Validator $validator)
    {
        $validator
            ->requirePresence('product_detail_id')
            ->notEmpty('product_detail_id', __('TXT_MESSAGE_REQUIRED'));

        $validator
            ->requirePresence('import_detail_id')
            ->notEmpty('import_detail_id', __('TXT_MESSAGE_REQUIRED'));

        $validator
            ->requirePresence('quantity')
            ->notEmpty('quantity', __('TXT_MESSAGE_REQUIRED'));

        $validator
            ->requirePresence('damage_level')
            ->notEmpty('damage_level', __('TXT_MESSAGE_REQUIRED'))
            ->add('status', [
                'list' => [
                    'rule' => ['inList', [LEVEL_A, LEVEL_B, LEVEL_C, LEVEL_F]],
                    'message' => __('TXT_MESSAGE_TYPE_SELECTION_INVALID'),
                ]
            ]);

        return $validator;
    }

}