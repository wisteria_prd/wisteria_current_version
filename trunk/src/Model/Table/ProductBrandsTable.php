<?php
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

class ProductBrandsTable extends Table
{
    public function initialize(array $config)
    {
        $this->primaryKey('id');
        $this->addBehavior('Timestamp');
        $this->addBehavior('Orm');

        $this->belongsTo('Manufacturers', array(
            'foreignKey' => false,
            'conditions' => 'ProductBrands.manufacturer_id = Manufacturers.id',
            'joinType' => 'INNER',
        ));
        $this->hasMany('Products', array(
            'foreignKey' => 'product_brand_id',
            'dependent' => true,
        ));
        $this->hasMany('Medias', [
            'foreignKey' => 'external_id',
            'dependent' => false,
        ]);
        $this->hasMany('SellerBrands', [
            'foreignKey' => 'product_brand_id',
            'dependent' => false,
        ]);
        $this->hasMany('PricingManagements', [
            'foreignKey' => 'product_brand_id',
            'dependent' => false,
        ]);
        $this->belongsTo('MF', array(
            'foreignKey' => 'manufacturer_id',
            'className' => 'Manufacturers',
            'joinType' => 'LEFT',
        ));
    }

    public function validationDefault(Validator $validator)
    {
        $validator
            ->requirePresence('name_en', 'create')
            ->notEmpty('name_en', __d('validate', 'TXT_MESSAGE_REQUIRED'));
        $validator
            ->requirePresence('manufacturer_id', 'create')
            ->notEmpty('manufacturer_id', __d('validate', 'TXT_MESSAGE_REQUIRED'));
        $validator
            ->requirePresence('url')
            ->allowEmpty('url')
            ->add('url', 'valid', [
                'rule' => 'url',
                'message' =>  __d('validate', 'TXT_MESSAGE_INVALID_URL'),
            ]);
        return $validator;
    }

    public function brandDropdown($man_id, $en = null, $type = 'all')
    {
        $data = $this->find($type, [
            'keyField' => 'id',
            'valueField' => 'name' . $en,
            'conditions' => [
                'manufacturer_id' => $man_id,
                'is_suspend' => 0,
            ]
        ]);

        if ($type == 'list') {
            return $data->toArray();
        }

        return $data;
    }

    public function getBrandByMfIdList($id, $key_field, $value_field)
    {
        $data = $this->find('list', [
            'keyField' => $key_field,
            'valueField' => 'name' . $value_field,
            'conditions' => [
                'name' . $value_field . ' <>' => '',
                'manufacturer_id' => $id
            ]
        ]);

        return $data->toArray();
    }

    public function getBrandList($key_field, $_en)
    {
        $data = $this->find('list', [
            'keyField' => $key_field,
            'valueField' => 'name' . $_en,
            'conditions' => [
                'name' . $_en . ' <>' => '',
            ]
        ]);

        return $data->toArray();
    }

    public function getListBrandBySellerId($seller_id, $local)
    {
        $data = $this->find('list', [
            'keyField' => 'id',
            'valueField' => 'name' . $local,
        ])->matching('SellerBrands', function ($q) use ($seller_id) {
            return $q->where(['SellerBrands.seller_id' => $seller_id]);
        })->toArray();

        return $data;
    }

    public function getListofBrandsBySellerId($seller_id = null)
    {
        $data = $this->find()
            ->where(['is_suspend' => 0])
            ->contain(['SellerBrands' => function ($q) use ($seller_id) {
                return $q->where(['SellerBrands.seller_id' => $seller_id]);
            }])
            ->all();
        return $data;
    }

    public function getAvailableListByManufacturerId($mId = null)
    {
        $data = $this->find('list', [
            'keyField' => 'id',
            'valueField' => function ($data) {
                return $data->get('label');
            }
        ])->where([
            'ProductBrands.manufacturer_id' => $mId,
            'ProductBrands.is_suspend' => 0,
        ]);
        return $data->toArray();
    }
}
