<?php
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

class ReceivePaymentsTable extends Table
{
    public function initialize(array $config)
    {
        parent::initialize($config);
        $this->primaryKey('id');
        $this->addBehavior('Timestamp');
        $this->addBehavior('Orm');

        $this->hasMany('SaleReceivePayments', [
            'foreignKey' => 'receive_payment_id',
            'dependent' => true,
            'cascadeCallbacks' => true,
        ]);

        $this->belongsTo('Users', [
            'foreignKey' => 'receiver_id',
            'joinType' => 'INNER'
        ]);

        $this->belongsTo('Subsidiaries', [
            'foreignKey' => 'subsidiary_id',
            'joinType' => 'INNER'
        ]);

        $this->belongsTo('Customers', [
            'foreignKey' => 'customer_id',
            'joinType' => 'INNER'
        ]);
    }

    public function validationDefault(Validator $validator)
    {
        $validator
            ->requirePresence('sale_id')
            ->notEmpty('sale_id', __('TXT_MESSAGE_REQUIRED'));
        $validator
            ->requirePresence('amount')
            ->notEmpty('amount', __('TXT_MESSAGE_REQUIRED'));
        $validator
            ->requirePresence('pay_date')
            ->notEmpty('pay_date', __('TXT_MESSAGE_REQUIRED'));
        $validator
            ->requirePresence('payee_name')
            ->notEmpty('payee_name', __('TXT_MESSAGE_REQUIRED'));
        return $validator;
    }

    public function checkSaleAlreadyPaid($pay_id)
    {
        $paid = $this->find()
            ->contain([
                'SaleReceivePayments' => [
                    'Sales'
                ]
            ])
            ->where(['ReceivePayments.id' => $pay_id]);

        $already_paid = 0;
        $amount_paid = 0;
        if ($paid) {
            foreach ($paid as $p) {
                $amount_paid = $p->amount;
                if ($p->sale_receive_payments) {
                    foreach ($p->sale_receive_payments as $amt) {
                        $already_paid += $amt->sale->amount;
                    }
                }
            }
        }
        return [$amount_paid, $already_paid];
    }
}
