<?php
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

class BreadcrumbsTable extends Table
{
    public function initialize(array $config)
    {
        parent::initialize($config);
        $this->primaryKey('id');

        $this->hasMany('BreadcrumbDetails', [
            'foreignKey' => 'breadcrumb_id',
            'dependent' => true,
        ]);
    }

    public function getListByControllerAndAction($controller = null, $action = null, $user_group_id, $request = null)
    {
        // pr($request);exit;
        $optional = '';
        if ($request->controller === 'PersonInCharges') {
            $optional = 'filter_by='.$request->query('filter_by');
        }
        if ($request->controller === 'Products') {
            if ($request->query('des')) {
                $optional = 'des='.$request->query('des');
            } else {
                $optional = '';
            }
        }
        if ($request->controller === 'Doctors') {
            $optional = 'des='.$request->query('des');
        }
        if ($request->controller === 'ProductDetails') {
            if ($request->query('des')) {
                $optional = 'des='.$request->query('des');
            } else {
                $optional = '';
            }
        }

        $data = $this->find()
            ->where(['controller' => $controller])
            ->where(['action' => $action])
            ->where(['user_group_id' => $user_group_id])
            ->where(['param' => $optional])
            ->contain([
                'BreadcrumbDetails' => [
                    'sort' => ['BreadcrumbDetails.order_number' => 'asc'],
                ],
            ])
            ->first();
        // pr($data);
        return $data;
    }

    public function getAllControllerByUserGroupId($user_group_id = null)
    {
        $data = $this->findByUserGroupId($user_group_id)
            ->all();
        return $data;
    }

    public function getAllActionByControllerName($controller = null, $user_group_id = null)
    {
        $data = $this->find()
            ->where(['controller' => trim($controller)]);
        if ($user_group_id !== null) {
            $data = $data->where(['user_group_id' => $user_group_id]);
        }
        $data = $data->all();
        return $data;
    }
}
