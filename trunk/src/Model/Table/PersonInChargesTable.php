<?php
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

class PersonInChargesTable extends Table
{
    public function initialize(array $config)
    {
        $this->primaryKey('id');
        $this->addBehavior('Timestamp');

        $this->belongsTo('Manufacturers', array(
            'foreignKey' => 'external_id',
            // 'conditions' => [
            //     'Manufacturers.id' => 'PersonInCharges.external_id',
            //     // 'PersonInCharges.type' => TYPE_MANUFACTURER,
            // ],
            'conditions' => 'Manufacturers.id = PersonInCharges.external_id AND PersonInCharges.type = "' . TYPE_MANUFACTURER . '"',
            'joinType' => 'LEFT',
        ));
        $this->belongsTo('Suppliers', array(
            'foreignKey' => false,
            // 'conditions' => [
            //     'Suppliers.id' => 'PersonInCharges.external_id',
            //     // 'PersonInCharges.type' => TYPE_SUPPLIER,
            // ],
            'conditions' => 'Suppliers.id = PersonInCharges.external_id AND PersonInCharges.type = "' . TYPE_SUPPLIER . '"',
            'joinType' => 'LEFT',
        ));
        $this->belongsTo('Customers', array(
            'foreignKey' => false,
            'conditions' => 'Customers.id = PersonInCharges.external_id AND PersonInCharges.type = "' . TYPE_CUSTOMER . '"',
            'joinType' => 'LEFT',
        ));
        $this->belongsTo('Subsidiaries', array(
            'foreignKey' => false,
            // 'conditions' => [
            //     'Manufacturers.id' => 'PersonInCharges.external_id',
            //     // 'PersonInCharges.type' => TYPE_MANUFACTURER,
            // ],
            'conditions' => 'Subsidiaries.id = PersonInCharges.external_id AND PersonInCharges.type = "' . TYPE_SUBSIDIARY . '"',
            'joinType' => 'LEFT',
        ));
        $this->hasMany('InfoDetails', [
            'foreignKey' => 'external_id',
            // 'conditions' => [
            //     'InfoDetails.id' => 'PersonInCharges.external_id',
            //     'InfoDetails.type' => TYPE_PERSON_IN_CHARGE,
            // ],
            // 'conditions' => 'PersonInCharges.id = InfoDetails.external_id AND InfoDetails.type = "' . TYPE_PERSON_IN_CHARGE . '"',
            'dependent' => false,
        ]);
        $this->hasMany('PersonInCharges', [
            'foreignKey' => 'person_in_charge_id',
            'dependent' => false,
        ]);
    }

    public function validationDefault(Validator $validator)
    {
        $validator
            ->requirePresence('type')
            ->notEmpty('type', __d('validate', 'TXT_MESSAGE_REQUIRED'));
        $validator
            ->requirePresence('external_id')
            ->notEmpty('external_id', __d('validate', 'TXT_MESSAGE_REQUIRED'));
        $validator
            ->requirePresence('gender')
            ->notEmpty('gender', __d('validate', 'TXT_MESSAGE_REQUIRED'));
        return $validator;
    }

    public function inChargeDropdown($en = null)
    {
        $data = $this->find('list')
            ->select([
                'first_name' . $en,
                'last_name' . $en
            ])
            ->where(['is_suspend' => 0])
            ->toArray();
        return $data;
    }

    /**
     * Function getList
     * using for get array list
     * @param string $local
     * @param string $type
     * @param int $external_id
     * @param string $key key of array
     * @return object array list
     */
    public function getList($local, $type, $external_id)
    {
        $data = [];

        $data1 = $this->find('all')->where([
                'PersonInCharges.type' => $type,
                'PersonInCharges.external_id' => $external_id,
            ])->toArray();

        if ($data1) {
            foreach ($data1 as $key => $value) {
                $data[$value->id] = $value->last_name . $local . ' ' . $value->first_name . $local;
            }
        }

        return $data;
    }

    public function getPersonByCustomer($customer_id = null)
    {
        return $this->find('all')
            ->where([
                'PersonInCharges.external_id' => $customer_id,
                'PersonInCharges.type' => TYPE_CUSTOMER,
            ]);
    }
}
