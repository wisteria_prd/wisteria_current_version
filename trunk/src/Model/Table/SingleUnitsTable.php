<?php

namespace App\Model\Table;

use Cake\ORM\Table;

class SingleUnitsTable extends Table
{
    public function initialize(array $config)
    {
        $this->primaryKey('id');

        $this->hasMany('ProductDetails', [
            'foreignKey' => 'single_unit_id',
            'dependent' => false,
        ]);
    }
}