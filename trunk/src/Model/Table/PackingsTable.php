<?php

namespace App\Model\Table;

use Cake\ORM\Table;

class PackingsTable extends Table
{
    public function initialize(array $config)
    {
        $this->primaryKey('id');

        $this->hasMany('ProductDetails', [
            'foreignKey' => 'packing_id',
            'dependent' => false,
        ]);

        $this->hasOne('PurchaseDetails', [
            'dependent' => false,
        ]);
    }
}