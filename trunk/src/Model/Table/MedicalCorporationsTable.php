<?php
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

class MedicalCorporationsTable extends Table
{
    public function initialize(array $config)
    {
        parent::initialize($config);
        $this->primaryKey('id');
        $this->addBehavior('Timestamp');

        $this->hasMany('PersonInCharges', [
            'foreignKey' => 'external_id',
            'joinType' => 'INNER'
        ]);

        $this->hasMany('CustomerGroups', [
            'foreignKey' => 'external_id',
            'dependent' => false,
        ]);

        $this->hasMany('Customers', [
            'foreignKey' => 'medical_corporation_id',
            'dependent' => false,
        ]);

        $this->hasMany('Subsidiaries', [
            'foreignKey' => 'external_id',
            'joinType' => 'LEFT',
            'conditions' => ['Subsidiaries.type' => TYPE_MEDICAL_CORP]
        ]);

        $this->hasMany('InfoDetails', [
            'foreignKey' => 'external_id',
            'joinType' => 'INNER'
        ]);

        $this->belongsTo('PaymentCategories', [
            'foreignKey' => 'payment_category_id',
            'joinType' => 'LEFT'
        ]);

        $this->belongsTo('InvoiceDeliveryMethods', [
            'foreignKey' => 'invoice_delivery_method_id',
            'joinType' => 'LEFT'
        ]);

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER'
        ]);

        $this->belongsTo('Countries', [
            'foreignKey' => 'country_id',
            'joinType' => 'LEFT',
        ]);
    }

    public function validationDefault(Validator $validator)
    {
        $validator
            ->requirePresence('name_en')
            ->notEmpty('name_en', __('TXT_MESSAGE_REQUIRED'));
//          TEMPORARY NOT USE.
//        $validator
//            ->requirePresence('building_en')
//            ->notEmpty('building_en', __('TXT_MESSAGE_REQUIRED'));
//
//        $validator
//            ->requirePresence('postal_code')
//            ->notEmpty('postal_code', __('TXT_MESSAGE_REQUIRED'));
//
//        $validator
//            ->requirePresence('street_en')
//            ->notEmpty('street_en', __('TXT_MESSAGE_REQUIRED'));
//
//        $validator
//            ->requirePresence('city_en')
//            ->notEmpty('city_en', __('TXT_MESSAGE_REQUIRED'));
//
//        $validator
//            ->requirePresence('prefecture_en')
//            ->notEmpty('prefecture_en', __('TXT_MESSAGE_REQUIRED'));

        $validator
            ->requirePresence('url')
            ->allowEmpty('url')
            ->add('url', 'urlWithProtocol', [
                'rule' => 'isHttp',
                'message' => __('TXT_MESSAGE_INVALID_URL_FORMAT'),
                'provider' => 'table'
            ]);

        return $validator;
    }

    public function isHttp($value, array $context)
    {
        if (strpos($value, 'http://') !== false || strpos($value, 'https://') !== false) {
            return true;
        }
        return false;
    }

    public function medicalDropdown($en = null)
    {
        $data = $this->find('list')
            ->select([
                'id',
                'name' . $en
            ])
            ->where(['is_suspend' => 0])
            ->toArray();
        return $data;
    }
}