<?php
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

class CustomersTable extends Table
{
    public function initialize(array $config)
    {
        parent::initialize($config);
        $this->primaryKey('id');
        $this->addBehavior('Orm');
        $this->addBehavior('Timestamp');

        $this->hasMany('PersonInCharges', [
            'foreignKey' => 'external_id',
            'dependent' => false,
        ]);

        $this->hasMany('CustomerSubsidiaries', [
            'foreignKey' => 'customer_id',
            'dependent' => false,
        ]);

        $this->hasMany('ClinicDoctors', [
            'foreignKey' => 'customer_id',
            'dependent' => false,
        ]);

        $this->hasMany('Deliveries', [
            'foreignKey' => 'customer_id',
            'dependent' => false,
        ]);

        $this->hasMany('InfoDetails', [
            'foreignKey' => 'external_id',
            'dependent' => false,
        ]);

        $this->hasMany('CustomerGroups', [
            'foreignKey' => 'external_id',
            'dependent' => false,
        ]);

        $this->hasMany('Sales', [
            'foreignKey' => 'customer_id',
            'dependent' => false,
        ]);

        $this->hasMany('ReceivePayments', [
            'foreignKey' => 'customer_id',
            'dependent' => false,
        ]);

        $this->belongsTo('PaymentCategories', [
            'foreignKey' => 'payment_category_id',
            'joinType' => 'INNER'
        ]);

        $this->belongsTo('InvoiceDeliveryMethods', [
            'foreignKey' => 'invoice_delivery_method_id',
            'joinType' => 'INNER'
        ]);

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER'
        ]);

        $this->belongsTo('MedicalCorporations', [
            'foreignKey' => 'medical_corporation_id',
            'joinType' => 'LEFT'
        ]);

        $this->hasMany('ClinicPricing', [
            'foreignKey' => 'customer_id',
            'dependent' => false,
        ]);

        $this->belongsTo('Countries', [
            'foreignKey' => 'country_id',
            'joinType' => 'LEFT',
        ]);
    }

    public function validationDefault(Validator $validator)
    {
        $validator
            ->requirePresence('country_id')
            ->notEmpty('country_id', __d('validate', 'TXT_MESSAGE_REQUIRED'));
        $validator
            ->requirePresence('name_en')
            ->notEmpty('name_en', __d('validate', 'TXT_MESSAGE_REQUIRED'));
        $validator
            ->requirePresence('type')
            ->notEmpty('type', __d('validate', 'TXT_MESSAGE_REQUIRED'))
            ->add('type', [
                'list' => [
                    'rule' => ['inList', [CUSTOMER_TYPE_NORMAL, CUSTOMER_TYPE_NEW, CUSTOMER_TYPE_CONTRACT]],
                    'message' => __d('validate', 'TXT_MESSAGE_REQUIRED'),
                ]
            ]);
        $validator
            ->requirePresence('payment_category_id')
            ->notEmpty('payment_category_id', __d('validate', 'TXT_MESSAGE_REQUIRED'));
        $validator
            ->requirePresence('invoice_delivery_method_id')
            ->notEmpty('invoice_delivery_method_id', __d('validate', 'TXT_MESSAGE_REQUIRED'));
        $validator
            ->requirePresence('url')
            ->allowEmpty('url')
            ->add('url', 'urlWithProtocol', [
                'rule' => 'isHttp',
                'message' => __d('validate', 'TXT_MESSAGE_INVALID_URL'),
                'provider' => 'table'
            ]);
        return $validator;
    }

    public function isHttp($value, array $context)
    {
        if (strpos($value, 'http://') !== false || strpos($value, 'https://') !== false) {
            return true;
        }
        return false;
    }

    public function customerDropdown($en = null, $default = 'list')
    {
        $name = 'name' . $en;
        $data = $this->find($default, [
            'keyField' => 'id',
            'valueField' => $name,
            'conditions' => [
                'Customers.is_suspend' => 0,
                'Customers.' . $name . ' <> ' => ''
            ]
        ]);
        return $data;
    }

    /**
     * use get customer list
     * @param string $local
     * @return array
     */
    public function getList($local)
    {
        $data = [];

        $customers = $this->find('all')
            ->where(['Customers.name' . $local . ' <>' => ''])->toArray();

        if ($customers) {
            foreach ($customers as $key => $value) {
                $data[$value->id] = $value->name . $local;
            }
        }

        return $data;
    }

    public function getCustomer($id = null, $en = '')
    {
        $customer = $this->get($id);
        $name = '';
        $address = '';
        //$c_name = 'name' . $en;
        $c_name = '';
        $prefecture = 'prefecture' . $en;
        $city = 'city' . $en;
        $street = 'street' . $en;
        $building = 'building' . $en;

        if ($customer) {
            $name = $customer->name . ' ' . $customer->name_en;
            $address .= $customer->postal_code . ',';
            $address .= $customer->$prefecture . ',';
            $address .= $customer->$city . ',';
            $address .= $customer->$street . ',';
            $address .= $customer->$building;
        }

        return [$name, $address];
    }

    /**
     * get name + name_en from customer
     * @param string $local local storage
     * @return array
     */
    public function getNameEnAndNameJp()
    {
        $data = [];

        $customers = $this->find()
            ->where(['Customers.is_suspend' => 0])
            ->all();

        if ($customers) {
            foreach ($customers as $key => $value) {
                $data[$value->id] = $value->name . ' ' . $value->name_en;
            }
        }

        return $data;
    }

    public function getAllAvailableJpAndEnName()
    {
        $data = $this->find()
            ->select([
                'name',
                'name_en',
            ])
            ->where([
                'Customers.is_suspend' => 0,
                'Customers.name <>' => '',
            ])
            ->order(['name' => 'desc']);
        $data2 = [];
        foreach ($data->all() as $key => $value) {
            $data2[] = $value->name;
            $data2[] = $value->name_en;
        }
        $name = array_filter(array_unique($data2));
        return $name;
    }

    public function getIdByName($clinic = null)
    {
        $data =  $this->find()
            ->where(['name' => $clinic])
            ->orWhere(['name_en' => $clinic])->first();
        return $data ? $data->id : null;
    }
}