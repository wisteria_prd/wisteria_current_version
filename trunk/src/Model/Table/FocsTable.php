<?php
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

class FocsTable extends Table
{
    public function initialize(array $config) {
        parent::initialize($config);
        $this->primaryKey('id');
        $this->addBehavior('Timestamp');

        $this->hasMany('FocRanges', [
            'foreignKey' => 'foc_id',
            'dependent' => true,
        ]);

        $this->belongsTo('Currencies', [
            'foreignKey' => 'currency_id',
            'joinType' => 'INNER',
        ]);

        $this->belongsTo('Sellers', [
            'foreignKey' => 'seller_id',
            'joinType' => 'INNER',
        ]);
    }

    public function validationDefault(Validator $validator)
    {
        $validator
            ->requirePresence('seller_id')
            ->notEmpty('seller_id', __('TXT_MESSAGE_REQUIRED'));

        $validator
            ->requirePresence('foc_for')
            ->notEmpty('foc_for', __('TXT_MESSAGE_REQUIRED'))
            ->add('foc_for', [
                'list' => [
                    'rule' => ['inList', [TYPE_BRAND, TYPE_PRODUCT, TYPE_PO]],
                    'message' => __('TXT_MESSAGE_FOC_SELECTION_INVALID'),
                ]
            ]);

        $validator
            ->requirePresence('range_type')
            ->notEmpty('range_type', __('TXT_MESSAGE_REQUIRED'))
            ->add('range_type', [
                'list' => [
                    'rule' => ['inList', [TYPE_AMOUNT, TYPE_QUANTITY]],
                    'message' => __('TXT_MESSAGE_FOC_INVALID_RANGE'),
                ]
            ]);

        $validator
            ->add('for_value', 'isValueSelect', [
                'rule' => 'isValueSelect',
                'message' => __('TXT_MESSAGE_REQUIRED'),
                'provider' => 'table'
            ]);

        $validator
            ->add('currency_id', 'custom', [
                'rule' => function ($value, $context) {
                    if ($context['data']['range_type'] == TYPE_AMOUNT) {
                        return !empty($value);
                    }
                },
                'message' => __('TXT_MESSAGE_REQUIRED'),
                'provider' => 'table'
            ]);

        return $validator;
    }

    public function isValueSelect($value, array $context)
    {
        if ($context['data']['foc_for'] == TYPE_BRAND || $context['data']['foc_for'] == TYPE_PRODUCT) {
            return !empty($value);
        }
        return true;
    }
}