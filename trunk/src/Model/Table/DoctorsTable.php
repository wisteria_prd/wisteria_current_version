<?php
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

class DoctorsTable extends Table
{
    public function initialize(array $config) {
        parent::initialize($config);
        $this->primaryKey('id');
        $this->addBehavior('Timestamp');
        $this->addBehavior('Orm');

        $this->hasMany('ClinicDoctors', [
            'foreignKey' => 'doctor_id',
            'dependent' => false,
        ]);

        $this->hasMany('Medias', [
            'foreignKey' => 'external_id',
            'dependent' => false,
        ]);

        $this->hasMany('DoctorSocieties', [
            'foreignKey' => 'doctor_id',
            'dependent' => false,
        ]);

        $this->hasMany('InfoDetails', [
            'foreignKey' => 'external_id',
            'dependent' => true,
        ]);
    }

    public function validationDefault(Validator $validator)
    {
        $validator
            ->requirePresence('first_name_en')
            ->notEmpty('first_name_en', __d('validate', 'TXT_MESSAGE_REQUIRED'));

        $validator
            ->requirePresence('last_name_en')
            ->notEmpty('last_name_en', __d('validate', 'TXT_MESSAGE_REQUIRED'));

        $validator
            ->requirePresence('gender')
            ->notEmpty('gender', __d('validate', 'TXT_MESSAGE_REQUIRED'));

        return $validator;
    }

    public function getDoctorDropdown()
    {
        return $this->find('all')
            ->where(['Doctors.is_suspend' => 0]);
    }

    public function getList($local)
    {
        return $this->find('list', [
            'keyField' => 'id',
            'valueField' => 'last_name' . $local,
        ])->toArray();
    }
}