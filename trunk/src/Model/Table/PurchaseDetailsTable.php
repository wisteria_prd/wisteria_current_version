<?php

namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\I18n\I18n;

class PurchaseDetailsTable extends Table
{
    public function initialize(array $config)
    {
        parent::initialize($config);
        $this->primaryKey('id');
        $this->addBehavior('Timestamp');
        $this->addBehavior('Orm');

        $this->belongsTo('Purchases', [
            'foreignKey' => 'purchase_id',
            'joinType' => 'INNER',
        ]);

        $this->belongsTo('ProductDetails');
        $this->belongsTo('Packings');
        $this->belongsTo('PackSizes');
    }

    public function validationDefault(Validator $validator)
    {
        parent::validationDefault($validator);
        $validator
            ->requirePresence('product_detail_id')
            ->notEmpty('product_detail_id', __('TXT_MESSAGE_REQUIRED'));

        $validator
            ->requirePresence('quantity')
            ->notEmpty('quantity', __('TXT_MESSAGE_REQUIRED'))
            ->add('quantity', 'valid', [
                'rule' => function ($value) {
                    return $value > 0;
                },
                'message' => __('TXT_MESSAGE_QTY_GREATER_THAN_ZERO'),
            ]);

        $validator
            ->requirePresence('unit_price')
            ->allowEmpty('unit_price', __('TXT_MESSAGE_REQUIRED'))
            ->add('unit_price', 'valid', [
                'rule' => function ($value) {
                    return $value >= 0;
                },
                'message' => __('TXT_MESSAGE_VALUE_GREATER_THAN_ZERO'),
            ]);

        return $validator;
    }
}
