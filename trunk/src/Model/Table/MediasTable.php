<?php
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Filesystem\File;

class MediasTable extends Table
{
    public function initialize(array $config) {
        parent::initialize($config);
        $this->primaryKey('id');
        $this->addBehavior('Timestamp');
        $this->addBehavior('Orm');

        $this->belongsTo('Doctors', [
            'foreignKey' => 'id',
            'joinType' => 'INNER',
        ]);

        $this->belongsTo('ProductBrands', [
            'foreignKey' => 'id',
            'joinType' => 'INNER',
        ]);

        $this->belongsTo('Products', [
            'foreignKey' => 'id',
            'joinType' => 'INNER',
        ]);

        $this->belongsTo('Messages', [
            'className' => 'Messages',
            'foreignKey' => 'external_id',
            'conditions' => ['type' => TYPE_MESSAGE]
        ]);

        $this->belongsTo('Replies', [
            'foreignKey' => false,
            'conditions' => 'Replies.id = Medias.external_id',
            'joinType' => 'INNER',
        ]);

        $this->belongsTo('Imports', [
            'className' => 'Imports',
            'foreignKey' => 'external_id',
            'conditions' => ['type' => TYPE_IMPORT],
            'joinType' => 'LEFT'
        ]);

        $this->belongsTo('Payments', [
            'className' => 'Payments',
            'foreignKey' => 'external_id',
            'conditions' => ['type' => TYPE_PAYMENT],
            'joinType' => 'LEFT'
        ]);
    }

    public function deleteFile($files)
    {
        foreach ($files as $item)
        {
            $file = new File(WWW_ROOT . 'img/uploads/' . $item, true, 0755);
            $file->delete();
            $file->close();
        }
    }

    public function deleteMediaById($id, $type)
    {
        $media_list = $this->find('all')
            ->where([
                'Medias.type' => $type,
                'Medias.external_id' => $id,
            ])->toArray();

        if ($media_list) {
            $id_list = [];
            foreach ($media_list as $value) {
                $id_list[] = $value->id;
            }
            $this->deleteAll(['Medias.id IN' => $id_list]);
        }
    }

    public function saveMedia($list, $id, $type = null, $priority_list = null, $profile = null)
    {
        $data = [];
        $file_list = $list ? explode(',', $list) : '';
        $set_priority = explode(',', $priority_list);

        if ($file_list) {
            foreach ($file_list as $key => $file) {
                $temp = [];
                $temp = explode('.', $file);
                $data[] = [
                    'external_id' => $id,
                    'type' => $type,
                    'file_type' => end($temp),
                    'file_name' => $file,
                    'file_order' => $key + 1,
                    'file_priority' => in_array($file, $set_priority) ? 1 : 0,
                ];
            }
        }
        if ($profile) {
            $temp = [];
            $temp = explode('.', $profile);
            $arr = [
                'external_id' => $id,
                'type' => $type,
                'file_type' => end($temp),
                'file_name' => $profile,
                'status' => 1
            ];
            array_push($data, $arr);
        }
        if ($data) {
            $medias = $this->newEntities($data);
            $this->saveMany($medias);
        }
    }

    public function getMediaList($id, $type)
    {
        $medias = $this->find('all')
            ->where([
                'Medias.type' => $type,
                'Medias.external_id' => $id,
            ])
            ->order(['file_order' => 'asc'])->toArray();

        return $medias;
    }

    public function getPaymentFile($id = null)
    {
        return $this->find()
            ->contain([])
            ->where([
                'Medias.external_id' => $id,
                'Medias.type' => TYPE_PAYMENT
            ]);
    }
}
