<?php
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

class PricingRangeManagementsTable extends Table
{
    public function initialize(array $config)
    {
        parent::initialize($config);
        $this->primaryKey('id');
        $this->addBehavior('Timestamp');

        $this->belongsTo('PricingManagements', [
            'foreignKey' => 'pricing_management_id',
            'joinType' => 'LEFT',
        ]);

        $this->belongsTo('Currencies', [
            'foreignKey' => 'currency_id',
            'joinType' => 'LEFT',
        ]);
    }

    public function validationDefault(Validator $validator)
    {
        $validator
            ->requirePresence('price_type')
            ->notEmpty('price_type', __d('validate', 'TXT_MESSAGE_REQUIRED'));

        $validator
            ->requirePresence('range_type')
            ->notEmpty('range_type', __d('validate', 'TXT_MESSAGE_REQUIRED'));

        $validator
            ->requirePresence('value_type')
            ->notEmpty('value_type', __d('validate', 'TXT_MESSAGE_REQUIRED'));
        $validator
            ->requirePresence('range_from')
            ->notEmpty('range_from', __d('validate', 'TXT_MESSAGE_REQUIRED'), function ($context) {
                $type = [
                    DISCOUNT_PRICE_TYPE,
                    CLINIC_PRICE_TYPE,
                ];
                if (in_array($context['data']['price_type'], $type) && !$context['data']['range_from']) {
                    return true;
                }
                return false;
            })
            ->add('range_from', 'valid', [
                'rule' => function($value) {
                    if ($value > 0) {
                        return true;
                    }
                    return false;
                },
                'message' => __d('validate', 'TXT_MESSAGE_VALUE_SMALLER_THAN_0'),
            ])
            ->add('range_from', 'custom', [
                'rule' => function($value, $context) {
                    $type = [
                        DISCOUNT_PRICE_TYPE,
                        CLINIC_PRICE_TYPE,
                    ];
                    if (in_array($context['data']['price_type'], $type) && ($value < $context['data']['range_to'])) {
                        return true;
                    }
                    return false;
                },
                'message' => __d('validate', 'TXT_MESSAGE_VALUE1_MUST_BE_SMALLER_THAN_VALUE2', ['Range To']),
            ]);
        $validator
            ->requirePresence('range_to')
            ->notEmpty('range_to', __d('validate', 'TXT_MESSAGE_REQUIRED'), function ($context) {
                $type = [
                    DISCOUNT_PRICE_TYPE,
                    CLINIC_PRICE_TYPE,
                ];
                if (in_array($context['data']['price_type'], $type) && !$context['data']['range_to']) {
                    return true;
                }
                return false;
            })
            ->add('range_to', 'valid', [
                'rule' => function($value) {
                    if ($value > 0) {
                        return true;
                    }
                    return false;
                },
                'message' => __d('validate', 'TXT_MESSAGE_VALUE_SMALLER_THAN_0'),
            ])
            ->add('range_to', 'custom', [
                'rule' => function($value, $context) {
                    $type = [
                        DISCOUNT_PRICE_TYPE,
                        CLINIC_PRICE_TYPE,
                    ];
                    if (in_array($context['data']['price_type'], $type) &&  ($value > $context['data']['range_from'])) {
                        return true;
                    }
                    return false;
                },
                'message' => __d('validate', 'TXT_MESSAGE_VALUE2_MUST_BE_BIGGER_THAN_VALUE1', ['Range From']),
            ]);
        $validator
            ->requirePresence('value')
            ->notEmpty('value', __d('validate', 'TXT_MESSAGE_REQUIRED'))
            ->add('value', 'valid', [
                'rule' => function($value) {
                    if ($value > 0) {
                        return true;
                    }
                    return false;
                },
                'message' => __d('validate', 'TXT_MESSAGE_VALUE_SMALLER_THAN_0'),
            ]);
        return $validator;
    }
}
