<?php
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

class PaymentCategoriesTable extends Table
{
    public function initialize(array $config) {
        parent::initialize($config);
        $this->primaryKey('id');
        $this->addBehavior('Timestamp');

        $this->hasOne('Customers', [
            'foreingKey' => 'payment_categories_id',
            'dependent' => false,
        ]);
    }

    public function validationDefault(Validator $validator)
    {
        $validator
            ->requirePresence('name_en')
            ->notEmpty('name_en', __('TXT_MESSAGE_REQUIRED'));

        $validator
            ->requirePresence('name')
            ->notEmpty('name', __('TXT_MESSAGE_REQUIRED'));

        return $validator;
    }

    public function payDropdown($en = null, $type = 'list')
    {
        $name = 'name' . $en;
        $data = $this->find($type, [
            'keyField' => 'id',
            'valueField' => $name,
            'order' => [
                $name => 'ASC'
            ]
        ]);
        return $data;
    }
}