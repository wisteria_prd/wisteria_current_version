<?php
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

class DeliveriesTable extends Table
{
    public function initialize(array $config) {
        parent::initialize($config);
        $this->primaryKey('id');
        $this->addBehavior('Orm');
        $this->addBehavior('Timestamp');

        $this->hasMany('SaleDeliveries', [
            'foreignKey' => 'delivery_id',
            'dependent' => true,
        ]);
        $this->hasMany('DeliveryDetails', [
            'foreignKey' => 'delivery_id',
            'dependent' => true,
        ]);
        $this->belongsTo('Customers', [
            'foreignKey' => 'customer_id',
            'joinType' => 'INNER',
        ]);
    }

    public function validationDefault(Validator $validator) {
        parent::validationDefault($validator);
        $validator
            ->requirePresence('tracking')
            ->notEmpty('tracking',  __('TXT_MESSAGE_REQUIRED'));
        $validator
            ->requirePresence('delivery_date')
            ->notEmpty('delivery_date',  __('TXT_MESSAGE_REQUIRED'));
        return $validator;
    }
}
