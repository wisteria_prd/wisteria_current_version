<?php
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\ORM\Query;

class SuppliersTable extends Table
{
    public function initialize(array $config)
    {
        $this->primaryKey('id');
        $this->addBehavior('Timestamp');

        $this->hasMany('PersonInCharges', array(
            'foreignKey' => 'external_id',
            'dependent' => true,
        ));
        $this->hasOne('Sellers', array(
            'foreignKey' => 'external_id',
            'conditions' => ['type' => TYPE_SUPPLIER],
            'dependent' => true,
        ));
        $this->hasMany('InfoDetails', array(
            'foreignKey' => 'external_id',
            'dependent' => true,
        ));
        $this->hasMany('PurchaseConditions', [
            'foreignKey' => 'external_id',
            'dependent' => true,
        ]);
        $this->belongsTo('Countries', [
            'foreignKey' => 'country_id',
            'joinType' => 'LEFT',
        ]);
    }

    public function validationDefault(Validator $validator)
    {
        $validator
            ->requirePresence('name_en')
            ->notEmpty('name_en', __d('validate', 'TXT_MESSAGE_REQUIRED'));
        $validator
            ->requirePresence('country_id')
            ->notEmpty('country_id', __d('validate', 'TXT_MESSAGE_REQUIRED'));
        $validator
            ->requirePresence('url')
            ->allowEmpty('url')
            ->add('url', 'urlWithProtocol', [
                'rule' => 'isHttp',
                'message' => __d('validate', 'TXT_MESSAGE_INVALID_URL'),
                'provider' => 'table'
            ]);
        $validator
            ->requirePresence('online_shop_url')
            ->allowEmpty('online_shop_url')
            ->add('online_shop_url', 'urlWithProtocol', [
                'rule' => 'isHttp',
                'message' => __d('validate', 'TXT_MESSAGE_INVALID_URL'),
                'provider' => 'table'
            ]);
        return $validator;
    }

    public function isHttp($value, array $context)
    {
        if (strpos($value, 'http://') !== false || strpos($value, 'https://') !== false) {
            return true;
        }
        return false;
    }

    public function findPersonInCharges(Query $query, array $options)
    {
        $sub_query = '';
        $id_list = $this->find('list', [
            'keyField' => 'id',
            'valueField' => 'id',
        ])->toArray();
        if ($id_list) {
            $sub_query = $this->query()->matching('PersonInCharges', function($q) use ($id_list) {
                return $q->where([
                    'PersonInCharges.type' => 'supplier',
                    'PersonInCharges.external_id IN' => $id_list,
                ]);
            })->count();
        }

        $query->select([
            'id',
            'name',
            'name_en',
            'postal_code',
            'country_id',
            'prefecture',
            'city',
            'street',
            'building',
            'prefecture_en',
            'city_en',
            'street_en',
            'building_en',
            'fax',
            'url',
            'online_shop_url',
            'remarks',
            'is_suspend',
            'created',
            'count_person' => $id_list ? $sub_query : 0,
        ]);
        return $query;
    }

    public function supplierDropdown($en = null, $type = 'list')
    {
        $name = 'name' . $en;
        $data = $this->find($type, [
            'keyField' => 'id',
            'valueField' => $name,
            'conditions' => [
                'Suppliers.is_suspend' => 0,
                'Suppliers.' . $name . ' <> ' => ''
            ]
        ]);
        return $data;
    }
}
