<?php
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

class MessagesTable extends Table
{

    public function initialize(array $config)
    {
        parent::initialize($config);
        $this->primaryKey('id');
        $this->addBehavior('Timestamp');

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Manufacturers', [
            'className' => 'Manufacturers',
            'foreignKey' => 'external_id',
            'conditions' => ['type' => TYPE_MANUFACTURER]
        ]);
        $this->belongsTo('MessageCategories', [
            'foreignKey' => 'message_category_id',
            'joinType' => 'INNER',
        ]);
        $this->hasMany('Replies', [
            'foreignKey' => 'message_id',
            'dependent' => false,
        ]);
        $this->hasMany('MessageReceivers', [
            'foreignKey' => 'message_id',
            'dependent' => false,
        ]);
        $this->belongsTo('Groups', [
            'foreignKey' => 'external_id',
            'conditions' => ['type' => TYPE_GROUP],
        ]);
        $this->hasMany('Medias', [
            'className' => 'Medias',
            'foreignKey' => 'external_id',
            'dependent' => false,
            'conditions' => ['type' => TYPE_MESSAGE]
        ]);
    }

    public function validationDefault(Validator $validator)
    {
        $validator
                ->requirePresence('status')
                ->notEmpty('status', __d('validate', 'TXT_MESSAGE_REQUIRED'));
        $validator
            ->requirePresence('message_category_id')
            ->notEmpty('message_category_id', __d('validate', 'TXT_MESSAGE_REQUIRED'));
        $validator
            ->requirePresence('title')
            ->notEmpty('title', __d('validate', 'TXT_MESSAGE_REQUIRED'));
        $validator
            ->requirePresence('description')
            ->notEmpty('description', __d('validate', 'TXT_MESSAGE_REQUIRED'));
        return $validator;
    }
}
