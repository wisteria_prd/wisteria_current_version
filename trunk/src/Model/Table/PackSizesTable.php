<?php

namespace App\Model\Table;

use Cake\ORM\Table;

class PackSizesTable extends Table
{
    public function initialize(array $config)
    {
        $this->primaryKey('id');

        $this->hasMany('ProductDetails', [
            'foreignKey' => 'pack_size_id',
            'dependent' => false,
        ]);

        $this->hasOne('PurchaseDetails', [
            'dependent' => false,
        ]);
    }
}