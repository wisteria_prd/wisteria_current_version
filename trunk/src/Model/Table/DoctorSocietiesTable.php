<?php
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

class DoctorSocietiesTable extends Table
{
    public function initialize(array $config) {
        parent::initialize($config);
        $this->primaryKey('id');

        $this->belongsTo('Doctors', [
            'foreignKey' => false,
            'conditions' => 'Doctors.id = DoctorSocieties.doctor_id',
            'joinType' => 'INNER',
        ]);

        $this->belongsTo('Societies', [
            'foreignKey' => false,
            'conditions' => 'DoctorSocieties.society_id = Societies.id',
            'joinType' => 'INNER',
        ]);
    }

    public function saveData($doctor_id, $list)
    {
        $data = [];
        $id = [];
        if (!empty($list)) {
            $id = explode(',', $list);
        }
        if ($id) {
            foreach ($id as $item) {
                if ($item) {
                    $data[] = [
                        'doctor_id' => $doctor_id,
                        'society_id' => $item,
                    ];
                }
            }
        }
        if ($data) {
            $entities = $this->newEntities($data);
            $this->saveMany($entities);
        }
    }

    public function deleteById($id)
    {
        $arr = [];
        $data = $this->find('all')
            ->select(['id'])
            ->where(['DoctorSocieties.doctor_id' => $id])->toArray();
        if ($data) {
            foreach ($data as $value) {
                $arr[] = $value->id;
            }
            $this->deleteAll(['DoctorSocieties.id IN' => $arr]);
        }
    }
}