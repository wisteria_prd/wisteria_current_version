<?php
namespace App\Model\Table;

use Cake\ORM\Table;

class SellerProductsTable extends Table
{
    public function initialize(array $config)
    {
        $this->primaryKey('id');
        $this->addBehavior('TimeStamp');
        $this->addBehavior('Orm');

        $this->belongsTo('Products', [
            'foreignKey' => 'product_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('SellerBrands', [
            'foreignKey' => 'seller_brand_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('ProductDetails', [
            'foreignKey' => 'product_detail_id',
            'joinType' => 'INNER',
        ]);
        $this->hasMany('SellerProductDetails', [
            'foreignKey' => 'seller_product_id',
            'dependent' => true,
        ]);
        $this->hasMany('PurchaseConditions', [
            'foreignKey' => 'external_id',
            'conditions' => ['type' => TYPE_SELLER_PRODUCT],
            'dependent' => true,
        ]);
    }

    public function getBySellerBrandId($id)
    {
        $data = $this->findBySellerBrandId($id)
            ->contain(['SellerProductDetails'])
            ->toArray();

        return $data;
    }
}
