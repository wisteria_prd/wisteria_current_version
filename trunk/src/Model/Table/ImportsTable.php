<?php

namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\ORM\TableRegistry;

class ImportsTable extends Table
{
    public function initialize(array $config)
    {
        parent::initialize($config);
        $this->primaryKey('id');
        $this->addBehavior('Timestamp');

        $this->hasMany('ImportDetails', [
            'foreignKey' => 'import_id',
            'dependent' => false,
        ]);

        $this->hasMany('ImportReturns', [
            'foreignKey' => 'import_id',
            'dependent' => false,
        ]);

        $this->hasMany('PurchaseImports', [
            'foreignKey' => 'import_id',
            'dependent' => false,
        ]);

        $this->hasMany('Medias', [
            'foreignKey' => 'external_id',
            'conditions' => ['type' => TYPE_IMPORT],
            'dependent' => false,
        ]);

        $this->belongsTo('Currencies', [
            'foreignKey' => 'currency_id',
            'joinType' => 'INNER',
        ]);
    }

    public function validationUpdateStatus(Validator $validator)
    {
        $validator
            ->requirePresence('tracking')
            ->notEmpty('tracking', __('TXT_MESSAGE_REQUIRED'));

        $validator
            ->requirePresence('delivery_date')
            ->notEmpty('delivery_date', __('TXT_MESSAGE_REQUIRED'));

        return $validator;
    }

    public function validationDefault(Validator $validator)
    {
        $validator
            ->requirePresence('currency_id')
            ->notEmpty('currency_id', __('TXT_MESSAGE_REQUIRED'));

        $validator
            ->requirePresence('type')
            ->notEmpty('type', __('TXT_MESSAGE_REQUIRED'));

        $validator
            ->requirePresence('currency_id')
            ->notEmpty('currency_id', __('TXT_MESSAGE_REQUIRED'));

        $validator
            ->requirePresence('courier_name')
            ->notEmpty('courier_name', __('TXT_MESSAGE_REQUIRED'));

        $validator
            ->requirePresence('import_tax')
            ->notEmpty('import_tax', __('TXT_MESSAGE_REQUIRED'));

        return $validator;
    }

    public function createInternationalImport($entity, $request, $group)
    {
        $associated = ['PurchaseImports' => ['validate' => false]];

        if (isset($request['medias']) && !empty($request['medias'][0]['file_name'])) {
            $associated = ['Medias' => ['validate' => false]];
        }
        $err = $this->validateByImportType($request);
        $import = $this->patchEntity($entity, $request, ['associated' => $associated]);

        if (empty($import->errors()) && empty($err)) {
            $import->affiliation_class = $group;
            return $this->save($import, ['validate' => false]);
        }

        $errors = [];
        if (!empty($err)) {
            $errors = array_merge($import->errors(), ['id' => $err]);
        } else {
            $errors = $import->errors();
        }
        
        return $errors;
    }

    public function updateInternationalImport($id, $entity, $request)
    {
        $associated = ['PurchaseImports' => ['validate' => false]];

        if (isset($request['medias']) && !empty($request['medias'][0]['file_name'])) {
            $associated = ['Medias' => ['validate' => false]];
        }

        $err = $this->validateByImportType($request);
        $import = $this->patchEntity($entity, $request, ['associated' => $associated]);

        if (empty($import->errors()) && empty($err)) {
            $this->Medias = TableRegistry::get('Medias');
            $this->Medias->deleteAll(['external_id' => $id, 'type' => TYPE_IMPORT]);

            $this->PurchaseImports = TableRegistry::get('PurchaseImports');
            $this->PurchaseImports->deleteAll(['import_id' => $id]);
            
            return $this->save($import, ['validate' => false]);
        }
        
        $errors = [];
        if (!empty($err)) {
            $errors = array_merge($import->errors(), ['id' => $err]);
        } else {
            $errors = $import->errors();
        }

        return $errors;
    }

    private function validateByImportType($request = [])
    {
        $error = [];
        if ($request['type'] == CUSTOMER_TYPE_NORMAL) {
            foreach ($request['purchase_imports'] as $key => $value) {
                if (empty($value['purchase_id'])) {
                    $error['purchase_id-' . $key] = __('TXT_MESSAGE_REQUIRED');
                }
            }
        } else if ($request['type'] == TYPE_SAMPLE) {
            foreach ($request['purchase_imports'] as $key => $value) {
                if (empty($value['sale_id'])) {
                    $error['sale_id-' . $key] = __('TXT_MESSAGE_REQUIRED');
                }
            }
        }
        return $error;
    }
}
