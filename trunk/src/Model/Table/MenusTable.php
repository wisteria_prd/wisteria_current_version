<?php
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Routing\Router;
use Cake\Validation\Validator;

class MenusTable extends Table
{
    public function initialize(array $config)
    {
        $this->primaryKey('id');

        $this->hasMany('MenuControls', [
            'foreignKey' => 'menu_id',
            'dependent' => true,
        ]);
    }

    public function validationDefault(Validator $validator) {
        parent::validationDefault($validator);
        $validator
            ->requirePresence('name')
            ->notEmpty('name',  __('TXT_MESSAGE_REQUIRED'));
        $validator
            ->requirePresence('name_en')
            ->notEmpty('name_en',  __('TXT_MESSAGE_REQUIRED'));
        $validator
            ->requirePresence('url')
            ->notEmpty('url',  __('TXT_MESSAGE_REQUIRED'));
        $validator
            ->requirePresence('user_group_id', 'create')
            ->notEmpty('user_group_id',  __('TXT_MESSAGE_REQUIRED'));
        return $validator;
    }
}
