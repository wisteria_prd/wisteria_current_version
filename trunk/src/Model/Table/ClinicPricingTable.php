<?php

namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

class ClinicPricingTable extends Table
{
    public function initialize(array $config)
    {
        parent::initialize($config);
        $this->primaryKey('id');
        $this->addBehavior('Timestamp');

        $this->belongsTo('Customers', [
            'foreignKey' => 'customer_id',
            'joinType' => 'LEFT',
        ]);

        $this->belongsTo('Pricing', [
            'foreignKey' => 'pricing_id',
            'joinType' => 'LEFT',
        ]);
    }

    public function validationDefault(Validator $validator)
    {
        return $validator;
    }

    /**
     * get data list by customer id
     * @param int $customer_id
     * @return object
     */
    public function getByCustomerId($customer_id)
    {
        $data = $this->findByCustomerId($customer_id)
            ->toArray();

        return $data;
    }
}