<?php

namespace App\Model\Table;

use Cake\ORM\Table;

class CurrenciesTable extends Table
{
    public function initialize(array $config) {
        parent::initialize($config);
        $this->primaryKey('id');
        $this->addBehavior('Timestamp');
        $this->addBehavior('Orm');

        $this->hasMany('SellerStandardPrices', [
            'foreignKey' => 'currency_id',
            'dependent' => false,
        ]);

        $this->hasMany('WStandardPrices', [
            'foreignKey' => 'currency_id',
            'dependent' => false,
        ]);

        $this->hasMany('Focs', [
            'foreignKey' => 'currency_id',
            'dependent' => false,
        ]);

        $this->hasMany('Purchases', [
            'foreignKey' => 'currency_id',
            'dependent' => false,
        ]);

        $this->hasMany('Sales', [
            'foreignKey' => 'currency_id',
            'dependent' => false,
        ]);

        $this->hasMany('ImportReturns', [
            'foreignKey' => 'currency_id',
            'dependent' => false,
        ]);

        $this->hasMany('Payments', [
            'foreignKey' => 'currency_id',
            'dependent' => false,
        ]);

        $this->hasMany('ProductPackages', [
            'foreignKey' => 'currency_id',
            'dependent' => false,
        ]);

        $this->hasMany('Imports', [
            'foreignKey' => 'currency_id',
            'dependent' => false,
        ]);

        $this->hasMany('Pricing', [
            'foreignKey' => 'currency_id',
            'dependent' => false,
        ]);

        $this->hasMany('PricingManagements', [
            'foreignKey' => 'currency_id',
            'dependent' => false,
        ]);
    }

    /**
     * currency drop down
     * @param string $type Default = "list". Use "list" or "all"
     * @return object list
     */
    public function getDropdown($type = 'list')
    {
        return $this->find($type, [
            'keyField' => 'id',
            'valueField' => 'code'
        ])
        ->order(['Currencies.sort' => 'ASC']);
    }

    public function getList($local)
    {
        $data = [];
        $field = '';
        switch ($local) {
            case '_en' :
                $field = 'code_en';
                break;
            default :
                $field = 'code';
        }
        $data = $this->find('list', [
            'keyField' => 'id',
            'valueField' => $field,
        ]);
        return $data;
    }
}
