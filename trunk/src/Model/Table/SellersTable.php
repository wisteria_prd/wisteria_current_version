<?php
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

class SellersTable extends Table
{
    public function initialize(array $config)
    {
        parent::initialize($config);
        $this->primaryKey('id');
        $this->addBehavior('Timestamp');
        $this->addBehavior('Orm');

        $this->hasOne('PricingManagements', [
            'foreignKey' => 'seller_id',
            'dependent' => false,
        ]);
        $this->hasMany('SellerBrands', [
            'foreignKey' => 'seller_id',
            'dependent' => false,
        ]);
        $this->hasMany('Purchases', [
            'foreignKey' => 'seller_id',
            'dependent' => false,
        ]);
        $this->hasMany('Focs', [
            'foreignKey' => 'seller_id',
            'dependent' => true,
        ]);
        $this->belongsTo('Suppliers', [
            'foreignKey' => 'external_id',
            // 'conditions' => ['Sellers.type' => TYPE_SUPPLIER],
            'joinType' => 'LEFT',
        ]);
        $this->belongsTo('Manufacturers', [
            'foreignKey' => 'external_id',
            // 'conditions' => ['Sellers.type' => TYPE_MANUFACTURER],
            'joinType' => 'LEFT',
        ]);
        $this->hasMany('ManagementFees', [
            'foreignKey' => 'seller_id',
            'dependent' => false,
        ]);
    }

    public function sellerDropdown()
    {
        $seller = $this->find()
            ->contain([
                'Manufacturers' => function ($q) {
                    return $q->where(['Manufacturers.is_suspend' => 0]);
                },
                'Suppliers' => function ($q) {
                    return $q->where(['Suppliers.is_suspend' => 0]);
                }
            ]);
        return $seller;
    }

    /**
     * Get all suppliers and manufacturers (as supplier)
     * @return array object
     */
    public function sellerDropdownAsSupplier()
    {
        $seller = $this->find()
            ->contain([
                'Manufacturers' => function ($q) {
                    return $q->where([
                        'Manufacturers.is_suspend' => 0
                    ]);
                },
                'Suppliers' => function ($q) {
                    return $q->where([
                        'Suppliers.is_suspend' => 0
                    ]);
                }
            ]);
        return $seller;
    }

    public function getSellerName()
    {
        $data = [];

        $sellers = $this->sellerDropdown();
        if ($sellers) {
            foreach ($sellers as $key => $value) {
                $item = [];
                if ($value->manufacturer) {
                    $item[$key] = [
                        'id' => $value->id,
                        'name' => $value->manufacturer->name,
                        'name_en' => $value->manufacturer->name_en,
                    ];
                } else {
                    $item[] = [
                        'id' => $value->id,
                        'name' => $value->supplier->name,
                        'name_en' => $value->supplier->name_en,
                    ];
                }
                $data[] = $item;
            }
        }

        return $data;
    }

    /**
     * Function getSellerList
     * using for get seller list
     * @param string $field_name get value from local session
     * @return object retrun array object
     */
    public function getSellerList($field_name = null, $kind_of = false, $country_id = null, $en = null)
    {
        $data = [];
        $conditions = [];

        if ($kind_of === TYPE_MP) {
            $conditions['Sellers.kind'] = TYPE_MP;
        } else {
            if ($kind_of === 'not_jp' && !is_null($country_id)) {
                $conditions['Sellers.kind <> '] = TYPE_MP;
            }
        }

        $sellers = $this->find('all')
            ->contain([
                'Suppliers' => function ($q) use ($kind_of, $country_id) {
                    if ($kind_of == 'jp_only' && !is_null($country_id)) {
                        return $q->where([
                            'Suppliers.country_id' => $country_id
                        ]);
                    } else if ($kind_of == 'not_jp' && !is_null($country_id)) {
                        return $q->where([
                            'Suppliers.country_id <> ' => $country_id
                        ]);
                    }

                    return $q;
                },
                'Manufacturers' => function ($q) use ($kind_of, $country_id) {
                    if ($kind_of == 'jp_only' && !is_null($country_id)) {
                        return $q->where([
                            'Manufacturers.country_id' => $country_id
                        ]);
                    } else if ($kind_of == 'not_jp' && !is_null($country_id)) {
                        return $q->where([
                            'Manufacturers.country_id <> ' => $country_id
                        ]);
                    }

                    return $q;
                }
            ])
            ->where($conditions)
            ->toArray();

        if ($sellers) {
            foreach ($sellers as $key => $value) {
                if ($value->manufacturer) {
                    $data[$value->id] = $this->nameEnOrJp($value->manufacturer->name, $value->manufacturer->name_en, $en);
                } else if ($value->supplier) {
                    $data[$value->id] = $this->nameEnOrJp($value->supplier->name, $value->supplier->name_en, $en);
                }
            }
        }

        return $data;
    }

    /**
     * get seller info for Sale [seller info] register.
     * @param integer $id
     * @return array object
     */
    public function getSeller($id = null, $en = '')
    {
        $seller = $this->find()
            ->contain([
                'Suppliers',
                'Manufacturers'
            ])
            ->where(['Sellers.id' => $id])
            ->first();

        $name = '';
        $address = '';

        if ($seller) {
            $prefecture = 'prefecture' . $en;
            $city = 'city' . $en;
            $street = 'street' . $en;
            $building = 'building' . $en;
            $n = 'name' . $en;
            if ($seller->type == TYPE_MANUFACTURER) {
                $name = $seller->manufacturer->$n;
                $address .= $seller->manufacturer->postal_code . ',';
                $address .= $seller->manufacturer->$prefecture . ',';
                $address .= $seller->manufacturer->$city . ',';
                $address .= $seller->manufacturer->$street . ',';
                $address .= $seller->manufacturer->$building;
            } else if ($seller->type == TYPE_SUPPLIER) {
                $name = $seller->supplier->$n;
                $address .= $seller->supplier->postal_code . ',';
                $address .= $seller->supplier->$prefecture . ',';
                $address .= $seller->supplier->$city . ',';
                $address .= $seller->supplier->$street . ',';
                $address .= $seller->supplier->$building;
            }
        }

        return [$name, $address];
    }

    public function nameEnOrJp($name_jp, $name_en, $en = '')
    {
        $new_name = '';
        if ($en == '_en') {
            $new_name = $name_en;
            if ($new_name == '') {
                $new_name = $name_jp;
            }
        } else {
            $new_name = $name_jp;
            if ($new_name == '') {
                $new_name = $name_en;
            }
        }

        return h($new_name);
    }

    /**
     * find seller type MP by seller_id
     * $id
     * @return integer|null
     */
    public function sellerIdAsMp()
    {
        $seller = $this->find()
            ->select(['id'])
            ->where(['Sellers.kind' => TYPE_MP])
            ->first();
        if ($seller) {
            return $seller->id;
        }
        return null;
    }

    /**
     * find seller id as by Country ID
     * @return integer|null
     */
    public function sellerIdByCountry($country_id, $is_jp = true)
    {
        $sellers = $this->find()
            ->contain([
                'Suppliers' => function ($q) use ($country_id, $is_jp) {
                    if ($is_jp) {
                        return $q->where([
                            'Suppliers.country_id' => $country_id
                        ]);
                    }
                    return $q->where([
                        'Suppliers.country_id <> ' => $country_id
                    ]);
                },
                'Manufacturers' => function ($q) use ($country_id, $is_jp) {
                    if ($is_jp) {
                        return $q->where([
                            'Manufacturers.country_id' => $country_id
                        ]);
                    }
                    return $q->where([
                        'Manufacturers.country_id <> ' => $country_id
                    ]);
                }
            ])
            ->where([
                'Sellers.kind <> ' => TYPE_MP //exclude seller as MP. It's not by JP and other countries.
            ]);
        $ids = [];
        if ($sellers) {
            $i = 0;
            foreach ($sellers as $seller) {
                if ($seller->manufacturer || $seller->supplier) {
                    $ids[$i] = $seller->id;
                    $i++;
                }
            }
        }

        return $ids;
    }

}
