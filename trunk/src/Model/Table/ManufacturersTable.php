<?php
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

class ManufacturersTable extends Table
{
    public function initialize(array $config)
    {
        $this->primaryKey('id');
        $this->addBehavior('Timestamp');

        $this->hasMany('PersonInCharges', array(
            'foreignKey' => 'external_id',
            // 'conditions' => ['PersonInCharges.type' => TYPE_MANUFACTURER],
            'dependent' => true,
        ));
        $this->hasMany('ProductBrands', array(
            'foreignKey' => 'manufacturer_id',
            'dependent' => true,
        ));
        $this->hasOne('Sellers', array(
            'foreignKey' => 'external_id',
            'conditions' => ['type' => TYPE_MANUFACTURER],
            'dependent' => true,
        ));
        $this->hasMany('Messages', [
            'className' => 'Messages',
            'foreignKey' => 'external_id',
            'conditions' => ['type' => TYPE_MANUFACTURER]
        ]);
         $this->hasMany('InfoDetails', [
             'foreignKey' => 'external_id',
             'dependent' => false,
         ]);
        $this->hasMany('PurchaseConditions', [
            'foreignKey' => 'external_id',
            'dependent' => true,
        ]);
        $this->belongsTo('Countries', [
            'foreignKey' => 'country_id',
            'joinType' => 'INNER',
        ]);
    }

    public function validationDefault(Validator $validator)
    {
        $validator
            ->requirePresence('name_en')
            ->notEmpty('name_en', __d('validate', 'TXT_MESSAGE_REQUIRED'));
        $validator
            ->requirePresence('country_id')
            ->notEmpty('country_id', __d('validate', 'TXT_MESSAGE_REQUIRED'));
        $validator
            ->requirePresence('url')
            ->allowEmpty('url')
            ->add('url', 'urlWithProtocol', [
                'rule' => 'isHttp',
                'message' => __d('validate', 'TXT_MESSAGE_INVALID_URL'),
                'provider' => 'table',
            ]);
        $validator
            ->requirePresence('online_shop_url')
            ->allowEmpty('online_shop_url')
            ->add('online_shop_url', 'urlWithProtocol', [
                'rule' => 'isHttp',
                'message' => __d('validate', 'TXT_MESSAGE_INVALID_URL'),
                'provider' => 'table',
            ]);
        return $validator;
    }

    public function manufacturerDropdown($en = null, $default = 'list')
    {
        $name = 'name' . $en;
        $data = $this->find($default, [
            'keyField' => 'id',
            'valueField' => $name,
            'conditions' => [
                'Manufacturers.is_suspend' => 0
            ]
        ]);
        return $data;
    }

    public function isHttp($value, $context = [])
    {
        if (strpos($value, 'http://') !== false || strpos($value, 'https://') !== false) {
            return true;
        }
        return false;
    }

    public function getAvailableList()
    {
        $data = $this->find('list', [
            'keyField' => 'id',
            'valueField' => function ($data) {
                return $data->get('label');
            }
        ])->where(['Manufacturers.is_suspend' => 0]);
        return $data;
    }
}
