<?php
namespace App\Model\Table;

use Cake\ORM\Table;

class AffiliationClassesTable extends Table
{
    public function initialize(array $config)
    {
        parent::initialize($config);
        $this->primaryKey('id');
        $this->addBehavior('Timestamp');
    }

    /**
     * Function getList
     * using for get affiliation class
     * @param string $key array key
     * @param string $local local session storage
     * @return array
     */
    public function getList($key, $local)
    {
        $value = 'name' . $local;

        $data = $this->find('list', [
            'keyField' => $key,
            'valueField' => $value,
        ])->where([$value . ' <>' => '']);

        return $data->toArray();
    }
}