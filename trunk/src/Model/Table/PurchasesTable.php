<?php

namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

class PurchasesTable extends Table
{
    public function initialize(array $config)
    {
        parent::initialize($config);
        $this->primaryKey('id');
        $this->addBehavior('Timestamp');
        $this->addBehavior('Orm');

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER',
        ]);

        $this->belongsTo('Currencies', [
            'foreignKey' => 'currency_id',
            'joinType' => 'INNER',
        ]);

        $this->belongsTo('Sellers', [
            'foreignKey' => 'seller_id',
            'joinType' => 'INNER',
        ]);

        $this->hasMany('PurchaseDetails', [
            'foreignKey' => 'purchase_id',
            'dependent' => false,
        ]);

        $this->hasMany('PurchaseImports', [
            'foreignKey' => 'purchase_id',
            'dependent' => false,
        ]);

        $this->hasMany('PurchasePayments', [
            'foreignKey' => 'purchase_id',
            'dependent' => true,
        ]);
    }

    public function validationDefault(Validator $validator) {
        $validator
            ->requirePresence('seller_id')
            ->notEmpty('seller_id', __('TXT_MESSAGE_REQUIRED'));

        $validator
            ->requirePresence('currency_id')
            ->notEmpty('currency_id', __('TXT_MESSAGE_REQUIRED'));

        $validator
            ->requirePresence('type')
            ->notEmpty('type', __('TXT_MESSAGE_REQUIRED'));

        $validator
            ->requirePresence('affiliation_class')
            ->notEmpty('affiliation_class', __('TXT_MESSAGE_REQUIRED'));

        $validator
            ->requirePresence('issue_date')
            ->notEmpty('issue_date', __('TXT_MESSAGE_REQUIRED'));

        return $validator;
    }

    public function validationCreate(Validator $validator)
    {
        $validator
            ->requirePresence('seller_id')
            ->notEmpty('seller_id', __('TXT_MESSAGE_REQUIRED'));

        $validator
            ->requirePresence('currency_id')
            ->notEmpty('currency_id', __('TXT_MESSAGE_REQUIRED'));

        $validator
            ->requirePresence('issue_date')
            ->notEmpty('issue_date', __('TXT_MESSAGE_REQUIRED'));

        return $validator;
    }

    public function purchaseDropdown($type = USER_CLASS_WISTERIA, $default = 'list')
    {
        $data = $this->find($default, [
            'keyField' => 'id',
            'valueField' => 'purchase_number'
        ])
        ->where([
            'is_suspend' => 0,
            'affiliation_class' => $type,
            'status <> ' => STATUS_COMPLETED
        ]);
        return $data;
    }

    /**
     * purchase issue date as dropdown for purchase search filter.
     * @param string $group value=wisteria|medipro
     * @param string $en default=_en
     * @return array object
     */
    public function getPurchaseIssueDate($group = USER_CLASS_WISTERIA, $en = '_en')
    {
        $query = $this->find();
        if (empty($en)) {
            $query->select([
                'issue_date',
                'purchase_date' => $query->func()->date_format([
                    'issue_date' => 'identifier',
                    "'%Y年%m月'" => 'literal'
                ])
            ]);
        } else {
            $query->select([
                'issue_date',
                'purchase_date' => $query->func()->date_format([
                    'issue_date' => 'identifier',
                    "'%Y-%m'" => 'literal'
                ])
            ]);
        }
        $query->where(['affiliation_class' => $group]);
        $query->group('purchase_date');
        $query->order(['purchase_date' => 'DESC']);
        return $query;
    }

    /**
     * sellers (contain both manufacturers and suppliers) dropdown base on purchase
     * @return array object
     */
    public function sellerDropdown() {
        return $this->find()
            ->contain([
                'Sellers' => [
                    'Suppliers',
                    'Manufacturers'
                ]
            ])
            ->group(['Purchases.seller_id']);
    }

    /**
     * use for count records if purchase.status is not equal cancel
     * @return int
     */
    public function countRecord()
    {
        $total = $this->find('all')
            ->where(['Purchases.status <>' => STATUS_CANCEL])->count();

        return $total;
    }
}
