<?php
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

class ManagementFeesTable extends Table
{
    public function initialize(array $config) {
        parent::initialize($config);
        $this->primaryKey('id');
        $this->addBehavior('Timestamp');

        $this->belongsTo('Sellers', [
            'foreignKey' => 'seller_id',
            'joinType' => 'INNER',
        ]);
    }

    public function validationDefault(Validator $validator)
    {
        return $validator;
    }

    public function getFeeDropdown($seller_id = null, $key_field = 'fee')
    {
        $conditions = [];
        if (!is_null($seller_id)) {
            $conditions['seller_id'] = $seller_id;
        }
        return $this->find('list', [
            'keyField' => $key_field,
            'valueField' => 'fee'
        ])->where($conditions);
    }
}