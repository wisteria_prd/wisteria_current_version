<?php

namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

class PricingTable extends Table
{
    public function initialize(array $config)
    {
        parent::initialize($config);
        $this->primaryKey('id');
        $this->addBehavior('Timestamp');

        $this->belongsTo('ProductPackages', [
            'foreignKey' => 'external_id',
            'joinType' => 'INNER',
        ]);

        $this->belongsTo('ProductDetails', [
            'foreignKey' => 'external_id',
            'joinType' => 'INNER',
        ]);

        $this->hasMany('FocDiscount', [
            'foreingKey' => 'external_id',
            'dependent' => false,
        ]);

        $this->hasMany('ClinicPricing', [
            'foreignKey' => 'pricing_id',
            'dependent' => false,
        ]);

        $this->belongsTo('Currencies', [
            'foreignKey' => 'external_id',
            'joinType' => 'INNER',
        ]);
    }

    public function validationDefault(Validator $validator)
    {
        $validator
            ->notEmpty('type', __('TXT_MESSAGE_REQUIRED'), 'create');

        $validator
            ->notEmpty('external_id', __('TXT_MESSAGE_REQUIRED'), 'create');

        $validator
            ->requirePresence('from_date')
            ->notEmpty('from_date', __('TXT_MESSAGE_REQUIRED'))
            ->add('from_date', 'custom', [
                'rule' => ['date', 'ymd'],
                'message' => __('TXT_MESSAGE_INVALID_FROM_DATE')
            ]);

        $validator
            ->requirePresence('to_date')
            ->notEmpty('to_date', __('TXT_MESSAGE_REQUIRED'))
            ->add('to_date', 'custom', [
                'rule' => ['date', 'ymd'],
                'message' => __('TXT_MESSAGE_INVALID_TO_DATE')
            ])
            ->add('to_date', 'custom', [
                'rule' => function ($value, $context) {
                    $from = strtotime($context['data']['from_date']);
                    $to = strtotime($context['data']['to_date']);
                    return ($to >= $from) ? true : false;
                },
                'message' => __('TXT_MESSAGE_INVALID_TO_DATE_GREATER')
            ]);

        $validator
            ->requirePresence('currency_id')
            ->notEmpty('currency_id', __('TXT_MESSAGE_REQUIRED'));

        $validator
            ->requirePresence('unit_price')
            ->notEmpty('unit_price', __('TXT_MESSAGE_REQUIRED'));

        $validator
            ->requirePresence('from_qty')
            ->notEmpty('from_qty', __('TXT_MESSAGE_REQUIRED'))
            ->add('from_qty', 'custom', [
                'rule' => function ($value, $context) {
                    return (int)$value > 0 ? true : false;
                },
                'message' => __('TXT_MESSAGE_QTY_GREATER_THAN_ZERO')
            ]);

        $validator
            ->requirePresence('to_qty')
            ->notEmpty('to_qty', __('TXT_MESSAGE_REQUIRED'))
            ->add('to_qty', 'custom', [
                'rule' => function ($value, $context) {
                    return (int)$value > 0 ? true : false;
                },
                'message' => __('TXT_MESSAGE_QTY_GREATER_THAN_ZERO')
            ])
            ->add('to_qty', 'custom', [
                'rule' => function ($value, $context) {
                    $from = $context['data']['from_qty'];
                    $to = $context['data']['to_qty'];
                    return ($to >= $from) ? true : false;
                },
                'message' => __('TXT_MESSAGE_TO_QTY_GREATER_THAN_FROM')
            ]);

        return $validator;
    }

    /**
     * use retrieve data where type and external_id
     * @param string $type
     * @param string $external_id
     * @param int $quantity
     * @return object
     */
    public function getByTypeAndExternalId($type, $external_id, $quantity)
    {
        $data = $this->find('all')
            ->where([
                'Pricing.external_id' => $external_id,
                'Pricing.type' => $type,
                'Pricing.from_qty <=' => $quantity,
                'Pricing.to_qty >=' => $quantity,
                'date(from_date) <=' => date('Y-m-d'),
                'date(to_date) >=' => date('Y-m-d'),
            ])
            ->toArray();

        return $data;
    }

    /**
     * Function getListQtyInBetween
     * use for get quantity list in between from_qty to to_qty
     * @param int $quantity
     * @return object
     */
    public function getListQtyInBetween($quantity)
    {
        $data = $this->find('all')
            ->where([
                'Pricing.from_qty <=' => $quantity,
                'Pricing.to_qty >=' => $quantity,
            ])->toArray();

        return $data;
    }

    /**
     * get data by id list
     * @param array $list
     * @return object
     */
    public function getByClinicPricing($list, $external_id, $quantity, $type)
    {
        $data = $this->find('all')
            ->where([
                'Pricing.id IN' => $list,
                'Pricing.type' => $type,
                'Pricing.external_id' => $external_id,
                'Pricing.from_qty <=' => $quantity,
                'Pricing.to_qty >=' => $quantity,
                'date(from_date) <=' => date('Y-m-d'),
                'date(to_date) >=' => date('Y-m-d'),
            ])
            ->toArray();

        return $data;
    }
}
