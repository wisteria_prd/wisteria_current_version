<?php
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

class ProductPackageDetailsTable extends Table
{
    public function initialize(array $config)
    {
        parent::initialize($config);
        $this->primaryKey('id');
        $this->addBehavior('Timestamp');

        $this->belongsTo('ProductPackages', [
            'foreignKey' => 'product_package_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('ProductDetails', [
            'foreignKey' => 'product_detail_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('WStandardPrices', [
            'foreignKey' => 'w_standard_price_id',
            'joinType' => 'INNER',
        ]);
    }

    public function validationDefault(Validator $validator)
    {
        $validator
            ->requirePresence('quantity')
            ->notEmpty('quantity', __d('validate', 'TXT_MESSAGE_REQUIRED'))
            ->add('quantity', 'custom', [
                'rule' => function ($value, $context) {
                    return (int)$value > 0 ? true : false;
                },
                'message' =>__d('validate', 'TXT_MESSAGE_VALUE_SMALLER_THAN_0')
            ]);
        return $validator;
    }
}