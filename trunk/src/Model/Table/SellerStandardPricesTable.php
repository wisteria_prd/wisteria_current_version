<?php

namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

class SellerStandardPricesTable extends Table
{
    public function initialize(array $config)
    {
        $this->primaryKey('id');
        $this->addBehavior('Timestamp');

        $this->hasMany('ProductPackageDetails', [
            'foreignKey' => 'seller_standard_price_id',
            'dependent' => false,
        ]);

        $this->belongsTo('SellerProductDetails', [
            'foreignKey' => 'seller_product_detail_id',
            'joinType' => 'INNER',
        ]);

        $this->belongsTo('Currencies', [
            'foreignKey' => 'currency_id',
            'joinType' => 'INNER',
        ]);
    }

    public function validationDefault(Validator $validator)
    {
        $validator
            ->notEmpty('purchase_standard_price', __('TXT_MESSAGE_REQUIRED'))
            ->add('purchase_standard_price', [
                'valid' => [
                    'rule' => function ($value) {
                        return $value >= 0;
                    },
                    'message' => __('TXT_MESSAGE_VALUE_GREATER_THAN_ZERO')
                ]
            ]);

        return $validator;
    }
}