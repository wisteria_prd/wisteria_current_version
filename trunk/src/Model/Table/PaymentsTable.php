<?php
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

class PaymentsTable extends Table
{
    public function initialize(array $config) {
        parent::initialize($config);
        $this->primaryKey('id');
        $this->addBehavior('Timestamp');
        $this->addBehavior('Orm');

        $this->hasMany('AdditionalPayments', [
            'foreignKey' => 'payment_id',
            'dependent' => true,
        ]);

        $this->belongsTo('Currencies', [
            'foreignKey' => 'currency_id',
            'joinType' => 'INNER'
        ]);

        $this->hasMany('Medias', [
            'foreignKey' => 'external_id',
            'conditions' => ['type' => TYPE_PAYMENT],
            'dependent' => true,
        ]);

        $this->hasMany('SalePayments', [
            'foreignKey' => 'payment_id',
            'dependent' => true,
        ]);

        $this->hasMany('PurchasePayments', [
            'foreignKey' => 'payment_id',
            'dependent' => true,
        ]);
    }

    public function validationDefault(Validator $validator)
    {
        $validator
            ->requirePresence('bank_charge')
            ->notEmpty('bank_charge', __('TXT_MESSAGE_REQUIRED'))
            ->add('bank_charge', 'custom', [
                'rule' => function ($value, $context) {
                    return (int)$value > 0 ? true : false;
                },
                'message' => __('TXT_MESSAGE_VALUE_NUMBER_ONLY')
            ]);

        // $validator
        //     ->requirePresence('bank_charge_date')
        //     ->notEmpty('bank_charge_date', __('TXT_MESSAGE_REQUIRED'))
        //     ->add('bank_charge_date', 'date', [
        //         'rule' => ['date', 'ymd'],
        //         'message' => __('TXT_MESSAGE_INVALID_DATE_FORMAT')
        //     ]);

        $validator
            ->requirePresence('product_amount')
            ->notEmpty('product_amount', __('TXT_MESSAGE_REQUIRED'))
            ->add('product_amount', 'custom', [
                'rule' => function ($value, $context) {
                    return (int)$value > 0 ? true : false;
                },
                'message' => __('TXT_MESSAGE_VALUE_NUMBER_ONLY')
            ]);

        $validator
            ->requirePresence('paid_amount')
            ->notEmpty('paid_amount', __('TXT_MESSAGE_REQUIRED'))
            ->add('paid_amount', 'custom', [
                'rule' => function ($value, $context) {
                    return (int)$value > 0 ? true : false;
                },
                'message' => __('TXT_MESSAGE_VALUE_NUMBER_ONLY')
            ]);

        $validator
            ->requirePresence('currency_id')
            ->notEmpty('currency_id', __('TXT_MESSAGE_REQUIRED'));

        $validator
            ->requirePresence('payment_date')
            ->notEmpty('payment_date', __('TXT_MESSAGE_REQUIRED'))
            ->add('payment_date', 'date', [
                'rule' => ['date', 'ymd'],
                'message' => __('TXT_MESSAGE_INVALID_DATE_FORMAT')
            ]);

        return $validator;
    }

}
