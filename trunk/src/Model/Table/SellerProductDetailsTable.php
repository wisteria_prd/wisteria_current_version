<?php

namespace App\Model\Table;

use Cake\ORM\Table;

class SellerProductDetailsTable extends Table
{
    public function initialize(array $config)
    {
        $this->primaryKey('id');
        $this->addBehavior('TimeStamp');

        $this->hasMany('SellerStandardPrices', [
            'foriengKey' => 'seller_product_details_id',
            'dependent' => true,
        ]);

        $this->belongsTo('SellerProducts', [
            'foreignKey' => 'seller_product_id',
            'joinType' => 'INNER',
        ]);

        $this->belongsTo('ProductDetails');
    }
}