<?php
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

class ImportDetailsTable extends Table
{
    public function initialize(array $config)
    {
        parent::initialize($config);
        $this->primaryKey('id');
        $this->addBehavior('Timestamp');

        $this->belongsTo('ProductDetails', [
            'foreignKey' => 'product_detail_id',
            'joinType' => 'INNER',
        ]);

        $this->belongsTo('Imports', [
            'foreignKey' => 'import_id',
            'joinType' => 'INNER',
        ]);

        $this->hasMany('ImportReturnDetails', [
            'foreignKey' => 'import_detail_id',
            'dependent' => false,
        ]);

        $this->hasMany('Stocks', [
            'foreignKey' => 'import_detail_id',
            'dependent' => true,
            'cascadeCallbacks' => true,
        ]);
    }

    public function validationDefault(Validator $validator)
    {

//        $validator
//            ->requirePresence('purchase_id')
//            ->notEmpty('purchase_id', __('TXT_MESSAGE_REQUIRED'));

        $validator
            ->requirePresence('purchase_number')
            ->notEmpty('purchase_number', __('TXT_MESSAGE_REQUIRED'));

        $validator
            ->requirePresence('product_detail_id')
            ->notEmpty('product_detail_id', __('TXT_MESSAGE_REQUIRED'))
            ->add('product_detail_id', 'custom', [
                'rule' => function ($value, $context) {
                    return (int)$value > 0 ? true : false;
                },
                'message' => __('TXT_MESSAGE_VALUE_NUMBER_ONLY')
            ]);

        $validator
            ->requirePresence('lot_number')
            ->notEmpty('lot_number', __('TXT_MESSAGE_REQUIRED'));

        $validator
            ->requirePresence('quantity')
            ->notEmpty('quantity', __('TXT_MESSAGE_REQUIRED'))
            ->add('quantity', 'custom', [
                'rule' => function ($value, $context) {
                    return (int)$value > 0 ? true : false;
                },
                'message' => __('TXT_MESSAGE_VALUE_NUMBER_ONLY')
            ]);

        $validator
            ->requirePresence('expiry_date')
            ->notEmpty('expiry_date', __('TXT_MESSAGE_REQUIRED'))
            ->add('expiry_date', 'custom', [
                'rule' => ['date', 'ymd'],
                'message' => __('TXT_MESSAGE_INVALID_DATE_FORMAT')
            ]);

        $validator
            ->allowEmpty('issue_type', function ($context) {
                return empty($context['data']['is_issue']);
            }, __('TXT_MESSAGE_REQUIRED'));

        $validator
            ->allowEmpty('less_quantity', function ($context) {
                if (!empty($context['data'])) {
                    if ($context['data']['issue_type'] == ISSUE_TYPE_LESS_QTY) {
                        return is_numeric($context['data']['less_quantity']);                        
                    }
                    return true;
                }
                return true;
            }, __('TXT_MESSAGE_VALUE_NUMBER_ONLY'));

        return $validator;
    }
}