<?php
namespace App\Model\Table;

use Cake\ORM\Table;

class CountriesTable extends Table
{
    public function initialize(array $config)
    {
        $this->primaryKey('id');

        $this->hasMany('Manufacturers', array(
            'foreignKey' => 'country_id',
            'dependent' => false,
        ));
        $this->hasMany('Suppliers', array(
            'foreignKey' => 'country_id',
            'dependent' => false,
        ));
        $this->hasMany('Subsidiaries', array(
            'foreignKey' => 'country_id',
            'dependent' => false,
        ));
        $this->hasMany('MedicalCorporations', array(
            'foreignKey' => 'country_id',
            'dependent' => false,
        ));
        $this->hasMany('Customers', array(
            'foreignKey' => 'country_id',
            'dependent' => false,
        ));
    }

    public function getListOfCountries($locale = null)
    {
        $countries = $this->find('list', [
            'keyField' => 'id',
            'valueField' => function ($q) use ($locale) {
            switch ($locale) {
                case LOCAL_EN:
                    return $q->get('name_en');
                default:
                    return $q->get('name');
            }
            }
        ]);
        return $countries;
    }
}
