<?php
namespace App\Model\Table;

use Cake\ORM\Table;

class ModuleControlsTable extends Table
{
    public function initialize(array $config)
    {
        $this->primaryKey('id');

        $this->hasMany('ModuleActions', [
            'foreignKey' => 'module_control_id',
            'dependent' => true,
        ]);
    }

    public function getByName($name = null)
    {
        if ($name === MUDULE_ACTION_DASHBOARD) {
            return true;
        }
        if ($name === null) {
            return false;
        }
        $moduleControl = $this->findByName($name)->first();
        return $moduleControl->id;
    }
}
