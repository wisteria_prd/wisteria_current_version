<?php
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Collection\Collection;
use Cake\Routing\Router;
use Cake\Validation\Validator;

class UserGroupsTable extends Table
{
    public function initialize(array $config)
    {
        parent::initialize($config);
        $this->table('user_groups');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->hasMany('Users', [
            'foreignKey' => 'user_group_id',
            'dependent' => false,
        ]);

        $this->hasMany('MenuControls', [
            'foreignKey' => 'user_group_id',
            'dependent' => false,
        ]);

        $this->hasMany('UserPermissions', [
            'foreignKey' => 'user_group_id',
            'dependent' => false,
        ]);
    }

    public function validationDefault(Validator $validator)
    {
        $validator
            ->requirePresence('affiliation_class')
            ->notEmpty('affiliation_class', __('TXT_MESSAGE_REQUIRED'));
        $validator
            ->requirePresence('name_en')
            ->notEmpty('name_en', __('TXT_MESSAGE_REQUIRED'));
        $validator
            ->requirePresence('name')
            ->notEmpty('name', __('TXT_MESSAGE_REQUIRED'));
        $validator
            ->requirePresence('role')
            ->notEmpty('role', __('TXT_MESSAGE_REQUIRED'));
        return $validator;
    }

    /**
     * drop down menu filter by affiliation class
     * @param string $class
     * @return object
     */
    public function userRoleByClassName($class)
    {
        if ($class == USER_CLASS_WISTERIA) {
            $query = $this->find('list', ['keyField' => 'id', 'valueField' => 'name']);
            $query->where(['UserGroups.affiliation_class' => USER_CLASS_WISTERIA]);
            return $query;
        } else if ($class == USER_CLASS_MEDIPRO) {
            $query = $this->find('list', ['keyField' => 'id', 'valueField' => 'name_en']);
            $query->where(['UserGroups.affiliation_class' => USER_CLASS_MEDIPRO]);
            return $query;
        }
    }

    /**
     * drop down menu filter by main affiliation class
     * @param string $class
     * @return object
     */
    public function userMainRoleByClassName($class)
    {
        if ($class == USER_CLASS_WISTERIA) {
            $query = $this->find('list', ['keyField' => 'affiliation_class', 'valueField' => 'name']);
            $query
                ->where(['UserGroups.affiliation_class' => USER_CLASS_WISTERIA])
                ->group(['UserGroups.affiliation_class']);
            return $query;
        } else if ($class == USER_CLASS_MEDIPRO || $class == USER_SYSTEM_ADMIN) {
            $query = $this->find('list', ['keyField' => 'affiliation_class', 'valueField' => 'name_en']);
            $query
                ->where(['UserGroups.affiliation_class' => USER_CLASS_MEDIPRO])
                ->group(['UserGroups.affiliation_class']);
            return $query;
        }
    }

    public function userSubRoleByClassName($class_name, $action = 'new')
    {
        if ($class_name == USER_CLASS_WISTERIA) {
            $query = $this->find('list', ['keyField' => 'id', 'valueField' => 'name']);
            if ($action == 'update') {
                $query->where([
                    'affiliation_class' => $class_name
                ]);
            } else {
                $query->where([
                    'affiliation_class' => $class_name,
                    'role <> ' => 'admin'
                ]);
            }
            return $query;
        } else if ($class_name == USER_CLASS_MEDIPRO || $class_name == USER_SYSTEM_ADMIN) {
            $query = $this->find('list', ['keyField' => 'id', 'valueField' => 'name_en']);
            if ($action == 'update') {
                $query->where([
                    'affiliation_class' => $class_name
                ]);
            } else {
                $query->where([
                    'affiliation_class' => $class_name,
                    'role <> ' => 'admin'
                ]);
            }
            return $query;
        }
    }

    /**
     * get user group by id
     * @param integer $id
     * @return object
     */
    public function getUserGroupById($id)
    {
        $data = $this->find()
            ->where(['UserGroups.id' => $id])
            ->contain(['Users'])->first();

        return $data;
    }
}