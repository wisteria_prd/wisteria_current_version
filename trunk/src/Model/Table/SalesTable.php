<?php
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

class SalesTable extends Table
{
    public function initialize(array $config)
    {
        $this->primaryKey('id');
        $this->addBehavior('Orm');
        $this->addBehavior('Timestamp');

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER',
        ]);

        $this->belongsTo('Sellers', [
            'foreignKey' => 'seller_id',
            'joinType' => 'INNER',
        ]);

        $this->belongsTo('Customers', [
            'foreignKey' => 'customer_id',
            'joinType' => 'INNER',
        ]);

        $this->belongsTo('Currencies', [
            'foreignKey' => 'currency_id',
            'joinType' => 'INNER',
        ]);

        $this->hasMany('SaleDetails', [
            'foreignKey' => 'sale_id',
            'dependent' => true,
        ]);

        $this->hasMany('SaleReceivePayments', [
            'foreignKey' => 'sale_id',
            'dependent' => true,
        ]);

        $this->hasMany('SalePayments', [
            'foreignKey' => 'sale_id',
            'dependent' => true,
        ]);

        $this->hasMany('SaleTaxInvoices', [
            'foreignKey' => 'sale_id',
            'dependent' => true,
        ]);

        $this->hasMany('SaleDeliveries', [
            'foreignKey' => 'sale_id',
            'dependent' => true,
            'joinType' => 'LEFT',
        ]);

        $this->hasMany('PurchaseImports', [
            'foreignKey' => 'sale_id',
            'dependent' => false,
        ]);

        $this->hasOne('SaleCustomClearanceDocs', [
            'foreignKey' => 'sale_id',
            'dependent' => true,
        ]);
    }

    public function validationDefault(Validator $validator) {
        $validator
            ->requirePresence('seller_id')
            ->notEmpty('seller_id', __d('validate', 'TXT_MESSAGE_REQUIRED'));

        $validator
            ->requirePresence('currency_id')
            ->notEmpty('currency_id', __d('validate', 'TXT_MESSAGE_REQUIRED'));

        $validator
            ->requirePresence('order_date', 'create')
            ->notEmpty('order_date', __d('validate', 'TXT_MESSAGE_REQUIRED'));

        $validator
            ->requirePresence('customer_id')
            ->notEmpty('customer_id', __d('validate', 'TXT_MESSAGE_REQUIRED'));

        $validator
            ->requirePresence('rop_issue_date', 'create')
            ->notEmpty('rop_issue_date', __d('validate', 'TXT_MESSAGE_REQUIRED'));
        
        return $validator;
    }

    public function validationExchange(Validator $validator) {
        $validator
            ->requirePresence('currency_date')
            ->notEmpty('currency_date',__d('validate', 'TXT_MESSAGE_REQUIRED'))
            ->add('currency_date', 'date', [
                'rule' => ['date', 'ymd'],
                'message' => __('TXT_MESSAGE_INVALID_DATE_FORMAT')
            ]);

        $validator
            ->requirePresence('currency_exchange_rate')
            ->notEmpty('currency_exchange_rate', __d('validate', 'TXT_MESSAGE_REQUIRED'))
            ->integer('currency_exchange_rate', __('TXT_MESSAGE_VALUE_NUMBER_ONLY'));

        return $validator;
    }

    /**
     * sale issue date as dropdown for sale search filter.
     * @param string $group value=wisteria|medipro
     * @param string $en default=_en
     * @return array object
     */
    public function getSaleIssueDate($group = USER_CLASS_WISTERIA, $en = '_en', $field = 'issue_date')
    {
        //only receive payments use this field name "rop_issue_date". Other use issue_date
        $query = $this->find();
        if (empty($en)) {
            $query->select([
                $field,
                'sale_date' => $query->func()->date_format([
                    $field => 'identifier',
                    "'%Y年%m月'" => 'literal'
                ])
            ]);
        } else {
            $query->select([
                $field,
                'sale_date' => $query->func()->date_format([
                    $field => 'identifier',
                    "'%Y-%m'" => 'literal'
                ])
            ]);
        }
        $query->where([
            'affiliation_class' => $group,
            $field . ' IS NOT ' => null,
            'DATE('.$field.') > ' => '1970-01-01' //just prevent wrong date.
        ]);
        $query->group('sale_date');
        $query->order(['sale_date' => 'DESC']);

        return $query;
    }

    /**
     * sellers (contain both manufacturers and suppliers) dropdown base on sales
     * @return array object
     */
    public function sellerDropdown() {
        return $this->find()
            ->contain([
                'Sellers' => [
                    'Suppliers',
                    'Manufacturers'
                ]
            ])
            ->group(['Sales.seller_id']);
    }

    /**
     * Generate ROP Number
     * @return string
     */
    public function ropNumber($type)
    {
        if ($type === TYPE_BEHALF_PURCHASE) {
            $number = 'WJA';
            $data = $this->find()
                ->where(['type' => TYPE_BEHALF_PURCHASE])
                ->count();
            $number .= str_pad($data + 1, 4, '0', STR_PAD_LEFT);
        } else {
            $number = 'WIM';
            $data = $this->find()
                ->where(['type <>' => TYPE_BEHALF_PURCHASE])
                ->count();
            $number .= str_pad($data + 1, 4, '0', STR_PAD_LEFT);
        }
        return $number;
    }

    /**
     * Generate SALE Number
     * @param string $type sale type
     * @param array $conditions sale conditions
     * @params array $contains contain with sale
     * @param int $countPurchase count purchase with purchase_number is not null
     * @return string
     */
    public function saleNumber($type, $conditions = [], $contains = [], $countPurchase = 0)
    {
        if ($type === TYPE_BEHALF_PURCHASE) {
            $number = 'WOF';
            $data = $this->find()
                ->where(['type' => TYPE_BEHALF_PURCHASE])
                ->count();
        } else {
            $number = 'WPO';
            $data = $this->find()
                ->andWhere([
                    $conditions,
                    'type <>' => TYPE_BEHALF_PURCHASE,
                    'sale_number <>' => '',
                ])
                ->contain($contains)
                ->count();
        }
        $number .= str_pad($data + 1 + $countPurchase, 4, '0', STR_PAD_LEFT);

        $number .= '0000';
        return $number;
    }

    /**
     * reset all sale with is_paid to zero in case the receive payment was deleted.
     * @param none
     */
    public function resetSalePayment($sale_ids, $field = 'is_paid_supplier')
    {
        if ($sale_ids) {
            $this->query()
                ->update()
                ->set([$field => 0])
                ->where(['id IN' => $sale_ids])
                ->execute();
        }
    }

    public function saleImportDropdown($group = USER_CLASS_WISTERIA, $default = 'list', $type = CUSTOMER_TYPE_NORMAL)
    {
        $conditions = [
            'is_suspend' => 0,
            'affiliation_class' => $group,
            'status <> ' => STATUS_COMPLETED
        ];

        if ($type == TYPE_SAMPLE) {
            $conditions['OR'] = [['type' => TYPE_S_SAMPLE], ['type' => TYPE_MP_SAMPLE]];
        }

        $data = $this->find($default, [
            'keyField' => 'id',
            'valueField' => 'sale_number'
        ])
        ->where($conditions);
        return $data;
    }
 
    public function getAllAvailableProductByClinicName($clinic_name = null)
    {
        $this->Customers = \Cake\ORM\TableRegistry::get('Customers');
        $this->Sales = \Cake\ORM\TableRegistry::get('Sales');
        $clinic = $this->Customers->getIdByName($clinic_name);
        $data = $this->Sales->find()
            ->where([
                'Sales.customer_id' => $clinic,
                'Sales.sale_number <>' => '',
                'Sales.is_deleted' => 0,
                'DATE(Sales.order_date) <>' => date('Y-m-d H:i:s', strtotime('0000-00-00 00:00:00'))
            ])
            ->contain([
                'SaleDetails'
            ]);
        return $data ? $data : null;
    }

    public function getOrderDate($mode = null)
    {
        $order = $mode === 'min' ? 'asc' : 'desc';
        $data = $this->find()
            ->select(['order_date'])
            ->where([
                'DATE(Sales.order_date) <>' => '0000-00-00 00:00:00'
            ])
            ->order(['Sales.order_date' => $order])
            ->first();
        return $data ? date('Y/m/01', strtotime($data->order_date)) : null;
    }
}