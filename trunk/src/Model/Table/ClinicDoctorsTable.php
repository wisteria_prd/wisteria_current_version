<?php
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

class ClinicDoctorsTable extends Table
{
    public function initialize(array $config) {
        parent::initialize($config);
        $this->primaryKey('id');
        $this->addBehavior('Orm');

        $this->belongsTo('Customers', [
            'foreignKey' => 'customer_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Doctors', [
            'foreignKey' => 'doctor_id',
            'joinType' => 'INNER',
        ]);
    }

    public function validationDefault(Validator $validator)
    {
        $validator
            ->requirePresence('doctor_id')
            ->notEmpty('doctor_id', __d('validate', 'TXT_MESSAGE_REQUIRED'));
        $validator
            ->requirePresence('position')
            ->notEmpty('position', __d('validate', 'TXT_MESSAGE_REQUIRED'))
            ->add('position', 'isRequired', [
                'rule' => 'isRequired',
                'message' => __d('validate', 'TXT_MESSAGE_REQUIRED'),
                'provider' => 'table'
            ]);
        return $validator;
    }

    public function isRequired($value, array $context)
    {
        if (!empty($context['clinic_doctors'])) {
            foreach ($context['clinic_doctors'] as $v) {
                if (!empty($v['position'])) {
                    return true;
                }
                return false;
            }
        }
        return true;
    }

    public function saveData($params)
    {
        $customers = [];
        $positions = [];
        $data = [];
        if ($params) {
            $customers = $params->customer;
            $positions = $params->position;
        }
        if ($positions) {
            foreach ($positions as $key => $value) {
                $data[] = [
                    'doctor_id' => $params->id,
                    'customer_id' => $customers[$key],
                    'position' => $positions[$key],
                ];
            }
            $entities = $this->newEntities($data);
            $this->saveMany($entities);
        }
    }

    public function deleteById($id)
    {
        $data = $this->find('all')
            ->where(['ClinicDoctors.doctor_id' => $id])->toArray();
        if ($data) {
            $this->deleteAll($data);
        }
    }

    public function getDoctors($customer_id)
    {
        return $this->find('all')
            ->contain([
                'Doctors' => function ($q) {
                    return $q->where(['Doctors.is_suspend' => 0]);
                }
            ])
            ->where([
                'ClinicDoctors.customer_id' => $customer_id
            ])
            ->order(['ClinicDoctors.is_priority' => 'DESC']);
    }
}
