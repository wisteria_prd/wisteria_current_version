<?php
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

class FocDiscountsTable extends Table
{
    public function initialize(array $config) {
        parent::initialize($config);
        $this->primaryKey('id');
        $this->addBehavior('Timestamp');

        $this->belongsTo('FocRanges', [
            'foreignKey' => 'external_id',
            'joinType' => 'INNER',
        ]);

        $this->belongsTo('Pricing', [
            'foreignKey' => 'external_id',
            'joinType' => 'INNER',
        ]);

        $this->belongsTo('ProductDetails');
    }

    public function validationDefault(Validator $validator)
    {
        $validator
            ->requirePresence('foc_discount_type')
            ->notEmpty('foc_discount_type', __('TXT_MESSAGE_REQUIRED'));

        $validator
            ->requirePresence('foc_discount_value')
            ->notEmpty('foc_discount_value', __('TXT_MESSAGE_REQUIRED'))
            ->add('foc_discount_value', 'custom', [
                'rule' => function ($value, $context) {
                    return (int)$value > 0 ? true : false;
                },
                'message' => __('TXT_MESSAGE_VALUE_NUMBER_ONLY')
            ]);

        $validator
            ->requirePresence('product_detail_id')
            ->notEmpty('product_detail_id', __('TXT_MESSAGE_REQUIRED'));

        return $validator;
    }

    /**
     * Function getByTypeAndExternalId
     * use for get by external_id and type
     * @param string $type 
     * @param int $externalId
     * @return object
     */
    public function getByTypeAndExternalId($type, $externalId)
    {
        $data = $this->find('all')
            ->where([
                'FocDiscounts.type' => $type,
                'FocDiscounts.external_id' => $externalId,
            ])->contain([
                'Pricing',
                'ProductDetails' => [
                    'Products' => ['ProductBrands'],
                    'SingleUnits',
                    'PackSizes',
                ],
            ]);

        return $data->toArray();
    }
}