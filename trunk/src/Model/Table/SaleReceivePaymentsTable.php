<?php
namespace App\Model\Table;

use Cake\ORM\Table;

class SaleReceivePaymentsTable extends Table
{
    public function initialize(array $config)
    {
        parent::initialize($config);
        $this->primaryKey('id');
        $this->addBehavior('Orm');
        $this->addBehavior('Timestamp');

        $this->belongsTo('Sales', [
            'foreignKey' => 'sale_id',
            'joinType' => 'LEFT',
        ]);

        $this->belongsTo('ReceivePayments', [
            'foreignKey' => 'receive_payment_id',
            'joinType' => 'LEFT',
        ]);
    }

    /**
     * use for get payment list from sale receive payments
     * @param int $saleId
     * @return array
     */
    public function getSalePaymentListBySaleId($saleId)
    {
        $data = [];

        $list = $this->find('all')->where(['sale_id' => $saleId]);
        if ($list) {
            foreach ($list as $key => $value) {
                $data[] = $value->receive_payment_id;
            }
            return $data;
        }
        return false;
    }
}