<?php
namespace App\Model\Table;

use Cake\ORM\Table;

class MenuControlsTable extends Table
{
    public function initialize(array $config)
    {
        $this->primaryKey('id');

        $this->belongsTo('Menus', [
            'foreignKey' => 'menu_id',
            'joinType' => 'INNER',
        ]);

        $this->belongsTo('UserGroups', [
            'foreignKey' => 'user_group_id',
            'joinType' => 'INNER',
        ]);
    }
}
