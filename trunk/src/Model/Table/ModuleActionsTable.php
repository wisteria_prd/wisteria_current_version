<?php
namespace App\Model\Table;

use Cake\ORM\Table;

class ModuleActionsTable extends Table
{
    public function initialize(array $config)
    {
        $this->primaryKey('id');

        $this->hasMany('UserPermissions', [
            'foreignKey' => 'module_action_id',
            'dependent' => false,
        ]);

        $this->belongsTo('ModuleControls', [
            'foreignKey' => 'module_control_id',
            'joinType' => 'LEFT',
        ]);
    }
}
