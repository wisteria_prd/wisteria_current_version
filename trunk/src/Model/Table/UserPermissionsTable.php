<?php
namespace App\Model\Table;

use Cake\ORM\Table;

class UserPermissionsTable extends Table
{
    public function initialize(array $config)
    {
        $this->primaryKey('id');

        $this->belongsTo('UserGroups', [
            'foreignKey' => 'user_group_id',
            'joinType' => 'LEFT',
        ]);

        $this->belongsTo('ModuleActions', [
            'foreignKey' => 'module_action_id',
            'joinType' => 'LEFT',
        ]);
    }
}
