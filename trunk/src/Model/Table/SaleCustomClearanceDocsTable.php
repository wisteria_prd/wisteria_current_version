<?php
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

class SaleCustomClearanceDocsTable extends Table
{
    public function initialize(array $config)
    {
        $this->primaryKey('id');
        $this->addBehavior('Timestamp');
        $this->addBehavior('Orm');

        $this->belongsTo('Sales', [
            'foreignKey' => 'sale_id',
            'joinType' => 'INNER',
        ]);

        $this->belongsTo('PersonInCharges', [
            'foreignKey' => 'person_in_charge_id',
            'joinType' => 'INNER',
        ]);
    }

    public function validationDefault(Validator $validator)
    {
//        $validator
//            ->requirePresence('person_in_charge_id')
//            ->notEmpty('person_in_charge_id', __('TXT_MESSAGE_REQUIRED'));

        $validator
            ->requirePresence('import_date')
            ->notEmpty('import_date', __('TXT_MESSAGE_REQUIRED'))
            ->add('import_date', 'date', [
                'rule' => ['date', 'ymd'],
                'message' => __('TXT_MESSAGE_INVALID_DATE_FORMAT')
            ]);

        $validator
            ->requirePresence('storage_location')
            ->notEmpty('storage_location', __('TXT_MESSAGE_REQUIRED'));

        $validator
            ->requirePresence('minister')
            ->notEmpty('minister', __('TXT_MESSAGE_REQUIRED'));

        return $validator;
    }
}