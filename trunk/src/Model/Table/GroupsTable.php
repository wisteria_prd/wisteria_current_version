<?php

namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

class GroupsTable extends Table
{
    public function initialize(array $config)
    {
        $this->primaryKey('id');
        $this->addBehavior('TimeStamp');

        $this->hasMany('CustomerGroups', [
            'foreignKey' => 'group_id',
            'dependent' => true,
        ]);

        $this->hasMany('Messages', [
            'foreignKey' => 'external_id',
            'dependent' => true,
        ]);
    }

    public function validationDefault(Validator $validator) {
        $validator
            ->requirePresence('name')
            ->notEmpty('name', __('TXT_MESSAGE_REQUIRED'));

        $validator
            ->requirePresence('type')
            ->notEmpty('type', __('TXT_MESSAGE_REQUIRED'));

        return $validator;
    }
}