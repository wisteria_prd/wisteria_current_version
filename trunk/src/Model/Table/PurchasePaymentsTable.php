<?php
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

class PurchasePaymentsTable extends Table
{
    public function initialize(array $config)
    {
        parent::initialize($config);
        $this->primaryKey('id');
        $this->addBehavior('Timestamp');

        $this->belongsTo('Purchases', [
            'foreignKey' => 'purchase_id',
            'joinType' => 'LEFT',
        ]);

        $this->belongsTo('Payments', [
            'foreignKey' => 'payment_id',
            'joinType' => 'LEFT',
        ]);
    }

    public function validationDefault(Validator $validator)
    {
        $validator
            ->requirePresence('purchase_id')
            ->add('purchase_id', 'isRequired', [
                'rule' => 'isRequired',
                'message' => __('TXT_MESSAGE_REQUIRED'),
                'provider' => 'table'
            ]);

        return $validator;
    }

    public function isRequired($value, array $context)
    {
        if (!empty($context['sale_payments'])) {
            foreach ($context['sale_payments'] as $v) {
                if (!empty($v['purchase_id'])) {
                    return true;
                }
                return false;
            }
        }
        return true;
    }
}
