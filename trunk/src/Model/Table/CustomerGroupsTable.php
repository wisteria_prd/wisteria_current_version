<?php

namespace App\Model\Table;

use Cake\ORM\Table;

class CustomerGroupsTable extends Table
{
    public function initialize(array $config)
    {
        $this->primaryKey('id');

        $this->belongsTo('Groups', [
            'foreignKey' => 'group_id',
            'joinType' => 'INNER',
        ]);

        $this->belongsTo('Customers', [
            'foreignKey' => false,
            'conditions' => 'CustomerGroups.external_id = Customers.id',
            'joinType' => 'INNER',
        ]);

        $this->belongsTo('MedicalCorporations', [
            'foreignKey' => false,
            'conditions' => 'CustomerGroups.external_id = MedicalCorporations.id',
            'joinType' => 'INNER',
        ]);
    }

    public function updateIsMain()
    {
        $this->updateAll(['is_main' => false], ['is_main' => true]);
    }
}