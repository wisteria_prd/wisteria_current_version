<?php

namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

class PurchaseConditionsTable extends Table
{
    public function initialize(array $config) {
        parent::initialize($config);
        $this->primaryKey('id');
        $this->addBehavior('TimeStamp');
        $this->addBehavior('Orm');

        $this->belongsTo('Currencies', [
            'foreignKey' => 'currency_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('SellerProducts', [
            'foreignKey' => 'external_id',
            'conditions' => ['PurchaseConditions.type' => TYPE_SELLER_PRODUCT],
        ]);
        $this->belongsTo('SellerBrands', [
            'foreignKey' => 'external_id',
            'conditions' => ['PurchaseConditions.type' => TYPE_SELLER_BRAND],
        ]);
        $this->belongsTo('Manufacturers', [
            'foreignKey' => 'external_id',
            'conditions' => ['PurchaseConditions.type' => TYPE_MANUFACTURER],
            //'joinType' => 'INNER',
        ]);
        $this->belongsTo('Suppliers', [
            'foreignKey' => 'external_id',
            'conditions' => ['PurchaseConditions.type' => TYPE_SUPPLIER],
            //'joinType' => 'INNER',
        ]);
    }

    public function validationProductBrand(Validator $validator)
    {
        $validator
            ->requirePresence('seller_id')
            ->notEmpty('seller_id', __d('validate', 'TXT_MESSAGE_REQUIRED'));
        $validator
            ->requirePresence('product_brand_id')
            ->notEmpty('product_brand_id', __d('validate', 'TXT_MESSAGE_REQUIRED'));
        $validator
            ->requirePresence('currency_id')
            ->notEmpty('currency_id', __d('validate', 'TXT_MESSAGE_REQUIRED'));
        return $validator;
    }

    public function validationProduct(Validator $validator)
    {
        $validator
            ->requirePresence('seller_id')
            ->notEmpty('seller_id', __d('validate', 'TXT_MESSAGE_REQUIRED'));
        $validator
            ->requirePresence('product_brand_id')
            ->notEmpty('product_brand_id', __d('validate', 'TXT_MESSAGE_REQUIRED'));
        $validator
            ->requirePresence('product_detail_id')
            ->notEmpty('product_detail_id', __d('validate', 'TXT_MESSAGE_REQUIRED'));
        $validator
            ->requirePresence('currency_id')
            ->notEmpty('currency_id', __d('validate', 'TXT_MESSAGE_REQUIRED'));
        return $validator;
    }
}