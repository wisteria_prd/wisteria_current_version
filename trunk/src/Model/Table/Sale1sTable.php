<?php

namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\ORM\Query;

class Sale1sTable extends Table
{
    public $reportHeader = array();
    public $reportList = array();
    public $data = array();

    public $clinic_name_in_jp = '';

    public function initialize(array $config)
    {
        $this->table('sale1s');
        $this->primaryKey('id');
        $this->addBehavior('Timestamp');

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'LEFT'
        ]);
    }

    public function findTotalAmount(Query $query, array $options)
    {
        $query->select([
            'total_amount' => $query->func()->sum('quantity * unit_price'),
        ]);
        return $query;
    }

    public function findTotalQuantity(Query $query, array $options)
    {
        $query->select([
            'total_quantity' => $query->func()->sum('quantity * unit_price'),
        ]);
        return $query;
    }

    public function setProductValue($data)
    {
        if (!empty($data->toArray())) {
            // header
            $reportHeaderResult = array();
            $this->reportHeader['clinic_name_jp'] = array();
            $this->reportHeader['total_quantity'] = array();
            $this->reportHeader['total_amount'] = array();
            $this->reportHeader['unit_price'] = array();
            $this->reportHeader['group_item'] = array();
            $this->reportHeader['average_sub_total'] = array();

            // list
            $this->reportList = array();

            foreach ($data->toArray() as $key => $value) {
                // header
                // clinic name
                if (!in_array($value->clinic_name_jp, array_values($this->reportHeader['clinic_name_jp']))) {
                    $this->reportHeader['clinic_name_jp'][] = $value->clinic_name_jp;
                }
                // group array item
                if (isset($this->reportHeader['group_item'][$value->invoice_number])) {
                    $this->reportHeader['group_item'][$value->invoice_number]['sub_total'][] = $value->unit_price * $value->quantity;
                    $this->reportHeader['group_item'][$value->invoice_number]['qty'][] = $value->quantity;
                } else {
                    $this->reportHeader['group_item'][$value->invoice_number]['sub_total'] = array($value->unit_price * $value->quantity);
                    $this->reportHeader['group_item'][$value->invoice_number]['qty'] = array($value->quantity);
                }
                // quantity
                $this->reportHeader['total_quantity'][] = $value->quantity;
                // total amount
                $this->reportHeader['total_amount'][] = $value->quantity * $value->unit_price;
                // unit price
                $this->reportHeader['unit_price'][] = $value->unit_price;
                // array of order date
                $this->reportHeader['order_date'][] = strtotime(date('Y-m-d', strtotime($value->order_date)));
                // end header

                // list
                // group item
                if (isset($this->reportList[$value->clinic_name_jp])) {
                    $this->reportList[$value->clinic_name_jp]['clinic'][] = array(
                        'id' => $value->id,
                        'quantity' => $value->quantity,
                        'unit_price' => $value->unit_price,
                        'invoice_number' => $value->invoice_number,
                        'clinic_name_jp' => $value->clinic_name_jp,
                        'product_name' => $value->product_name,
                        'order_date' => $value->order_date,
                    );
                    $this->reportList[$value->clinic_name_jp]['unit_price'][] = $value->unit_price;
                    $this->reportList[$value->clinic_name_jp]['average_order'] = '';
                } else {
                    $this->reportList[$value->clinic_name_jp]['clinic'][] = array(
                        'id' => $value->id,
                        'quantity' => $value->quantity,
                        'unit_price' => $value->unit_price,
                        'invoice_number' => $value->invoice_number,
                        'clinic_name_jp' => $value->clinic_name_jp,
                        'product_name' => $value->product_name,
                        'order_date' => $value->order_date,
                    );
                    $this->reportList[$value->clinic_name_jp]['unit_price'] = array($value->unit_price);
                    $this->reportList[$value->clinic_name_jp]['average_order'] = array();
                }
                // group invoice
                if (isset($this->reportList[$value->clinic_name_jp]['invoice'])) {
                    if (!in_array($value->invoice_number, $this->reportList[$value->clinic_name_jp]['invoice'])) {
                        $this->reportList[$value->clinic_name_jp]['invoice'][] = $value->invoice_number;
                        $this->reportList[$value->clinic_name_jp]['date'][] = strtotime(date('Y-m-d', strtotime($value->order_date)));
                    }
                } else {
                    $this->reportList[$value->clinic_name_jp]['invoice'] = array($value->invoice_number);
                    $this->reportList[$value->clinic_name_jp]['date'] = array(strtotime(date('Y-m-d', strtotime($value->order_date))));
                }
                // end list
            }

            // find lastest and previous date from all record
            if (isset($this->reportHeader['order_date'])) {
                $this->reportHeader['Report']['max_date'] = date('Y-m-d', max(array_values($this->reportHeader['order_date'])));
                $this->reportHeader['Report']['min_date'] = date('Y-m-d', min(array_values($this->reportHeader['order_date'])));
            } else {
                $this->reportHeader['Report']['max_date'] = '';
                $this->reportHeader['Report']['min_date'] = '';
            }

            // calculate sub total and average sub total
            $total_qty = 0;
            $total_amount = 0;
            $cc = 0;
            foreach ($this->reportHeader['group_item'] as $key1 => $value1) {
                // sub total in one group
                $reportHeaderResult[$key1]['total_qty'] = array_sum(array_values($value1['qty']));
                $reportHeaderResult[$key1]['total_amount'] = array_sum(array_values($value1['sub_total']));
                if ($cc < 1) {
                    $total_qty = $reportHeaderResult[$key1]['total_qty'];
                    $total_amount = $reportHeaderResult[$key1]['total_amount'];
                } else {
                    $total_qty += $reportHeaderResult[$key1]['total_qty'];
                    $total_amount += $reportHeaderResult[$key1]['total_amount'];
                }
                $cc++;
            }
            // set sub total and average into global array
            $this->reportHeader['group_item'] = array();
            $this->reportHeader['group_item'][] = $reportHeaderResult;

            $this->reportHeader['average_total_unit_price'] = $total_amount/$total_qty;

            $distin_date = array_reduce(array_column($this->reportList, 'date'), 'array_merge', array());
            arsort($distin_date);
            // find latest year from date
            $lastestY = date('Y', max(array_values($distin_date)));
            // find previous year from date
            $previousY = $lastestY - 1;
            // find after previous year from date
            $afterPreviousY = $previousY - 1;

            foreach ($this->reportList as $key5 => $value5) {
                // sort date desc
                arsort($value5['date']);
                // set clinic name
                $this->reportList[$key5]['clinic_name_jp'] = $key5;
                // total order
                $this->reportList[$key5]['total_order'] = count($value5['clinic']);
                // total unit price
                $this->reportList[$key5]['total_unit_price'] = array_sum(array_values($value5['unit_price']));
                // average amount
                //$this->reportList[$key5]['average_amount'] = $this->reportList[$key5]['total_unit_price']/$this->reportList[$key5]['total_order'];
                // find average order
                $cc = 0;
                $d1 = '';
                foreach ($value5['date'] as $i => $val) {
                    if ($cc < 1) {
                        $d1 = date_create(date('Y-m-d', $val));
                        $this->reportList[$key5]['average_order'] = array();
                    } else {
                        $d2 = date_create(date('Y-m-d', $val));
                        //$this->reportList[$key5]['d_date'][] = array('d1' => $d1, 'd2' => $d2);
                        $this->reportList[$key5]['average_order'][] = date_diff($d1, $d2)->format('%a');
                        $d1 = $d2;
                    }
                    $cc++;
                }

                // total average order
                if ($this->reportList[$key5]['average_order']) {
                    $this->reportList[$key5]['average_order'] = array_sum(array_values($this->reportList[$key5]['average_order'])) / count($this->reportList[$key5]['average_order']);
                } else {
                    $this->reportList[$key5]['average_order'] = 0;
                }
                // set 12 months to array
                for ($i = 1; $i <= 12; $i++) {
                    // define lastest year
                    $this->reportList[$key5]['years'][$i][$lastestY]['qty'] = array();
                    $this->reportList[$key5]['years'][$i][$lastestY]['unit_price'] = array();
                    $this->reportList[$key5]['years'][$i][$lastestY]['amount'] = array();
                    // define previous year
                    $this->reportList[$key5]['years'][$i][$previousY]['qty'] = array();
                    $this->reportList[$key5]['years'][$i][$previousY]['unit_price'] = array();
                    $this->reportList[$key5]['years'][$i][$previousY]['amount'] = array();
                    // define after previous year
                    $this->reportList[$key5]['years'][$i][$afterPreviousY]['qty'] = array();
                    $this->reportList[$key5]['years'][$i][$afterPreviousY]['unit_price'] = array();
                    $this->reportList[$key5]['years'][$i][$afterPreviousY]['amount'] = array();
                }
                // make array of qty, unit price for lastest and previous year
                $total_unit_price = 0;

                foreach ($value5['clinic'] as $key6 => $value6) {
                    $month = (int)date('m', strtotime($value6['order_date']));
                    if ($lastestY == date('Y', strtotime($value6['order_date']))) {
                        $this->reportList[$key5]['years'][$month][$lastestY]['qty'][] = $value6['quantity'];
                        $this->reportList[$key5]['years'][$month][$lastestY]['unit_price'][] = $value6['unit_price'];
                        $this->reportList[$key5]['years'][$month][$lastestY]['amount'][] = $value6['quantity'] * $value6['unit_price'];
                        $this->reportList[$key5]['year']['last']['qty'][] = $value6['quantity'];
                        $this->reportList[$key5]['year']['last']['unit_price'][] = $value6['unit_price'];
                        $this->reportList[$key5]['year']['last']['amount'][] = $value6['quantity'] * $value6['unit_price'];
                        //$this->reportList[$key5]['years'][$month][$lastestY]['date'] = $value6['order_date'];
                    } else {
                        $this->reportList[$key5]['year']['last']['qty'][] = array();
                        $this->reportList[$key5]['year']['last']['unit_price'][] = array();
                        $this->reportList[$key5]['year']['last']['amount'][] = array();
                    }
                    if ($previousY == date('Y', strtotime($value6['order_date']))) {
                        $this->reportList[$key5]['years'][$month][$previousY]['qty'][] = $value6['quantity'];
                        $this->reportList[$key5]['years'][$month][$previousY]['unit_price'][] = $value6['unit_price'];
                        $this->reportList[$key5]['years'][$month][$previousY]['amount'][] = $value6['quantity'] * $value6['unit_price'];
                        $this->reportList[$key5]['year']['previous']['qty'][] = $value6['quantity'];
                        $this->reportList[$key5]['year']['previous']['unit_price'][] = $value6['unit_price'];
                        $this->reportList[$key5]['year']['previous']['amount'][] = $value6['quantity'] * $value6['unit_price'];
                    } else {
                        $this->reportList[$key5]['year']['previous']['qty'][] = array();
                        $this->reportList[$key5]['year']['previous']['unit_price'][] = array();
                        $this->reportList[$key5]['year']['previous']['amount'][] = array();
                    }
                    if ($afterPreviousY == date('Y', strtotime($value6['order_date']))) {
                        $this->reportList[$key5]['years'][$month][$afterPreviousY]['qty'][] = $value6['quantity'];
                        $this->reportList[$key5]['years'][$month][$afterPreviousY]['unit_price'][] = $value6['unit_price'];
                        $this->reportList[$key5]['years'][$month][$afterPreviousY]['amount'][] = $value6['quantity'] * $value6['unit_price'];
                        $this->reportList[$key5]['year']['after_previous']['qty'][] = $value6['quantity'];
                        $this->reportList[$key5]['year']['after_previous']['unit_price'][] = $value6['unit_price'];
                        $this->reportList[$key5]['year']['after_previous']['amount'][] = $value6['quantity'] * $value6['unit_price'];
                    } else {
                        $this->reportList[$key5]['year']['after_previous']['qty'][] = array();
                        $this->reportList[$key5]['year']['after_previous']['unit_price'][] = array();
                        $this->reportList[$key5]['year']['after_previous']['amount'][] = array();
                    }
                // total_unit_price
                    $subtotal = $value6['quantity'] * $value6['unit_price'];
                    $total_unit_price += $subtotal;
                }
                $this->reportList[$key5]['total_unit_price'] = $total_unit_price;

                asort($this->reportList[$key5]['date']);
                // remove array of date
                //unset($this->reportList[$key5]['date']);
                // remove array of unit price
                unset($this->reportList[$key5]['unit_price']);
                // remove array of clinic
                unset($this->reportList[$key5]['clinic']);
            }
            return true;
        }
        return false;
    }

    public function getProductValue()
    {
        if ($this->reportHeader['clinic_name_jp']) {
            $this->reportHeader['Report']['clinic_name_jp'] = count($this->reportHeader['clinic_name_jp']);
        } else {
            $this->reportHeader['Report']['clinic_name_jp'] = 0;
        }
        if ($this->reportHeader['total_quantity']) {
            $this->reportHeader['Report']['total_quantity'] = array_sum(array_values($this->reportHeader['total_quantity']));
        } else {
            $this->reportHeader['Report']['total_quantity'] = 0;
        }
        if ($this->reportHeader['total_amount']) {
            $this->reportHeader['Report']['total_amount'] =  array_sum(array_values($this->reportHeader['total_amount']));
        } else {
            $this->reportHeader['Report']['total_amount'] = 0;
        }
        if ($this->reportHeader['unit_price']) {
            $this->reportHeader['Report']['min_unitprice'] =  min($this->reportHeader['unit_price']);
            $this->reportHeader['Report']['max_unitprice'] =  max($this->reportHeader['unit_price']);
        } else {
            $this->reportHeader['Report']['min_unitprice'] = 0;
            $this->reportHeader['Report']['max_unitprice'] = 0;
        }
        $this->reportHeader['Report']['avg_unitprice'] = $this->reportHeader['average_total_unit_price'];

        // list
        if ($this->reportList) {
            $this->reportList['Report'] = $this->reportList;
        } else {
            $this->reportList['Report'] = array();
        }

        $this->data['Header'] = $this->reportHeader['Report'];
        $this->data['Body'] = $this->reportList['Report'];

        return $this->data;
    }

    public function setClinicValue($data)
    {
        if (!empty($data->toArray())) {
            $this->reportHeader = array();
            $this->reportList = array();

            foreach ($data->toArray() as $key => $value) {
                // header
                // group array item
                $this->clinic_name_in_jp = $value->clinic_name_jp;
                if (isset($this->reportHeader['group_item'][$value->invoice_number])) {
                    $this->reportHeader['group_item'][$value->invoice_number]['product_name'][] = $value->product_name;
                    $this->reportHeader['group_item'][$value->invoice_number]['sub_total'][] = $value->unit_price * $value->quantity;
                    $this->reportHeader['group_item'][$value->invoice_number]['qty'][] = $value->quantity;
                    //$this->reportHeader['group_item'][$value->invoice_number]['order_date'][] = strtotime(date('Y-m-d', strtotime($value->order_date)));
                } else {
                    $this->reportHeader['group_item'][$value->invoice_number]['product_name'] = array($value->product_name);
                    $this->reportHeader['group_item'][$value->invoice_number]['sub_total'] = array($value->unit_price * $value->quantity);
                    $this->reportHeader['group_item'][$value->invoice_number]['qty'] = array($value->quantity);
                    $this->reportHeader['group_item'][$value->invoice_number]['order_date'] = array(strtotime(date('Y-m-d', strtotime($value->order_date))));
                }
                // total amount
                $this->reportHeader['total_amount'][] = $value->quantity * $value->unit_price;
                // array of order date
                $this->reportHeader['order_date'][] = strtotime(date('Y-m-d', strtotime($value->order_date)));

                // list
                // group item
                if (isset($this->reportList[$value->product_name])) {
                    $this->reportList[$value->product_name]['clinic'][] = array(
                        'id' => $value->id,
                        'quantity' => $value->quantity,
                        'unit_price' => $value->unit_price,
                        'invoice_number' => $value->invoice_number,
                        'product_name' => $value->product_name,
                        'order_date' => $value->order_date,
                    );
                    $this->reportList[$value->product_name]['unit_price'][] = $value->unit_price;
                    $this->reportList[$value->product_name]['average_order'] = '';
                } else {
                    $this->reportList[$value->product_name]['clinic'][] = array(
                        'id' => $value->id,
                        'quantity' => $value->quantity,
                        'unit_price' => $value->unit_price,
                        'invoice_number' => $value->invoice_number,
                        'product_name' => $value->product_name,
                        'order_date' => $value->order_date,
                    );
                    $this->reportList[$value->product_name]['unit_price'] = array($value->unit_price);
                    $this->reportList[$value->product_name]['average_order'] = array();
                }
                // group invoice
                if (isset($this->reportList[$value->product_name]['invoice'])) {
                    if (!in_array($value->invoice_number, $this->reportList[$value->product_name]['invoice'])) {
                        $this->reportList[$value->product_name]['invoice'][] = $value->invoice_number;
                        $this->reportList[$value->product_name]['date'][] = strtotime(date('Y-m-d', strtotime($value->order_date)));
                    }
                } else {
                    $this->reportList[$value->product_name]['invoice'] = array($value->invoice_number);
                    $this->reportList[$value->product_name]['date'] = array(strtotime(date('Y-m-d', strtotime($value->order_date))));
                }
                // end list
            }
    // pr($this->reportList);
            $arr_data = $data->toArray();
            $distin_arr = array_map(function($element){
                return $element['invoice_number'];
            }, $arr_data);

            // count purchase number
            $this->reportHeader['total_order'] = count(array_count_values($distin_arr));
            // find number of month between two date
            if (isset($this->reportHeader['order_date'])) {
                // sort date desc
                arsort($this->reportHeader['order_date']);
                $this->reportHeader['Report']['max_date'] = date('Y-m-d', max(array_values($this->reportHeader['order_date'])));
                $this->reportHeader['Report']['min_date'] = date('Y-m-d', min(array_values($this->reportHeader['order_date'])));
            } else {
                $this->reportHeader['Report']['max_date'] = '';
                $this->reportHeader['Report']['min_date'] = '';
            }

            // Average order date
            $this->reportHeader['all_order_date'] = array_reduce(array_column($this->reportHeader['group_item'], 'order_date'), 'array_merge', array());
            arsort($this->reportHeader['all_order_date']);

            // find average order
            $aa = 0;
            $a1 = '';
            foreach ($this->reportHeader['all_order_date'] as $i => $val) {
                if ($aa < 1) {
                    $a1 = date_create(date('Y-m-d', $val));
                    $this->reportHeader['diff_order_date'] = array();
                } else {
                    $a2 = date_create(date('Y-m-d', $val));
                    $this->reportHeader['d_date'][] = array('d1' => $a1, 'd2' => $a2);
                    $this->reportHeader['diff_order_date'][] = date_diff($a1, $a2)->format('%a');
                    $a1 = $a2;
                }
                $aa++;
            }

            // find average number from all purchase
            if (isset($this->reportHeader['diff_order_date']) && count($this->reportHeader['diff_order_date']) > 0) {
                $this->reportHeader['average_total_order_date'] = array_sum(array_values($this->reportHeader['diff_order_date'])) / count($this->reportHeader['diff_order_date']);
            } else {
                $this->reportHeader['average_total_order_date'] = 0;
            }

            $distin_date = array_reduce(array_column($this->reportList, 'date'), 'array_merge', array());
            arsort($distin_date);

            // group item by lastest and previous date
            foreach ($this->reportList as $key5 => $value5) {
                // find latest year from date
                $lastestY = date('Y', max(array_values($distin_date)));
                // find previous year from date
                $previousY = $lastestY - 1;
                // find after previous year from date
                $afterPreviousY = $previousY - 1;
                // set clinic name
                $this->reportList[$key5]['product_name'] = $key5;
                // total order
                $this->reportList[$key5]['total_order'] = count(array_count_values($value5['invoice']));
                // total unit price
                $this->reportList[$key5]['total_unit_price'] = array_sum(array_values($value5['unit_price']));
                // average amount
                $this->reportList[$key5]['average_amount'] = $this->reportList[$key5]['total_unit_price']/$this->reportList[$key5]['total_order'];
                // sort date desc
                arsort($value5['date']);

                // find average order
                $cc = 0;
                $d1 = '';
                foreach ($value5['date'] as $i => $val) {
                    if ($cc < 1) {
                        $d1 = date_create(date('Y-m-d', $val));
                        $this->reportList[$key5]['average_order'] = array();
                    } else {
                        $d2 = date_create(date('Y-m-d', $val));
                        //$this->reportList[$key5]['d_date'][] = array('d1' => $d1, 'd2' => $d2);
                        $this->reportList[$key5]['average_order'][] = date_diff($d1, $d2)->format('%a');
                        $d1 = $d2;
                    }
                    $cc++;
                }

                // find each average order
                if ($this->reportList[$key5]['average_order']) {
                    $sum_to_month = array_sum(array_values($this->reportList[$key5]['average_order'])) / 30;
                    $this->reportList[$key5]['average_order'] = $sum_to_month / $this->reportList[$key5]['total_order'];
                } else {
                    $this->reportList[$key5]['average_order'] = 0;
                }
                // set 12 months to array
                for ($i = 1; $i <= 12; $i++) {
                    // define lastest year
                    $this->reportList[$key5]['year'][$i][$lastestY]['qty'] = array();
                    $this->reportList[$key5]['year'][$i][$lastestY]['unit_price'] = array();
                    // define previous year
                    $this->reportList[$key5]['year'][$i][$previousY]['qty'] = array();
                    $this->reportList[$key5]['year'][$i][$previousY]['unit_price'] = array();
                    // define after previous year
                    $this->reportList[$key5]['year'][$i][$afterPreviousY]['qty'] = array();
                    $this->reportList[$key5]['year'][$i][$afterPreviousY]['unit_price'] = array();
                }

                // make array of qty, unit price for lastest and previous year
                // and calculate total_unit_price
                $total_unit_price = 0;
                foreach ($value5['clinic'] as $key6 => $value6) {
                    $month = (int)date('m', strtotime($value6['order_date']));
                    if ($lastestY == date('Y', strtotime($value6['order_date']))) {
                        $this->reportList[$key5]['year'][$month][$lastestY]['qty'][] = $value6['quantity'];
                        $this->reportList[$key5]['year'][$month][$lastestY]['unit_price'][] = $value6['unit_price'];
                    }
                    if ($previousY == date('Y', strtotime($value6['order_date']))) {
                        $this->reportList[$key5]['year'][$month][$previousY]['qty'][] = $value6['quantity'];
                        $this->reportList[$key5]['year'][$month][$previousY]['unit_price'][] = $value6['unit_price'];
                    }
                    if ($afterPreviousY == date('Y', strtotime($value6['order_date']))) {
                        $this->reportList[$key5]['year'][$month][$afterPreviousY]['qty'][] = $value6['quantity'];
                        $this->reportList[$key5]['year'][$month][$afterPreviousY]['unit_price'][] = $value6['unit_price'];
                    }

                    // total_unit_price
                    $subtotal = $value6['quantity'] * $value6['unit_price'];
                    $total_unit_price += $subtotal;
                }
                $this->reportList[$key5]['total_unit_price'] = $total_unit_price;

                // remove array of date
                //unset($this->reportList[$key5]['date']);
                asort($this->reportList[$key5]['date']);
                // remove array of unit price
                unset($this->reportList[$key5]['unit_price']);
                // remove array of clinic
                unset($this->reportList[$key5]['clinic']);
                // remove array of invoice
                unset($this->reportList[$key5]['invoice']);
            }
            return true;
        }
        return false;
    }

    public function getClinicValue()
    {
        if ($this->reportHeader['total_order']) {
            $this->reportHeader['Report']['total_order'] = $this->reportHeader['total_order'];
        } else {
            $this->reportHeader['Report']['total_order'] = 0;
        }
        if (isset($this->reportHeader['total_amount'])) {
            $this->reportHeader['Report']['total_amount'] =  array_sum(array_values($this->reportHeader['total_amount']));
        } else {
            $this->reportHeader['Report']['total_amount'] = 0;
        }
        if (isset($this->reportHeader['total_amount'])) {
            $this->reportHeader['Report']['total_average_amount'] =  array_sum(array_values($this->reportHeader['total_amount'])) / $this->reportHeader['Report']['total_order'];
        } else {
            $this->reportHeader['Report']['total_average_amount'] = 0;
        }
        if ($this->reportHeader['average_total_order_date']) {
            $this->reportHeader['Report']['average_total_order_date'] =  $this->reportHeader['average_total_order_date'];
        } else {
            $this->reportHeader['Report']['average_total_order_date'] = 0;
        }
        if ($this->reportHeader['diff_order_date']) {
            $this->reportHeader['Report']['diff_order_date'] =  $this->reportHeader['diff_order_date'];
        } else {
            $this->reportHeader['Report']['diff_order_date'] = 0;
        }

        // list
        if ($this->reportList) {
            $this->reportList['Report'] = $this->reportList;
        } else {
            $this->reportList['Report'] = array();
        }

        $this->data['Header'] = $this->reportHeader['Report'];
        $this->data['Body'] = $this->reportList['Report'];

        return $this->data;
    }

    public function minOrderDate()
    {
        $m_date = '';
        $min_date = $this->find();
        $min_date->select(['order_date'])
            ->where([
                'DATE(Sale1s.order_date) <>' => '0000-00-00 00:00:00'
            ])
            ->order(['Sale1s.order_date' => 'ASC'])
            ->limit(1);
        if ($min_date->toArray()) {
            foreach ($min_date as $date) {
                $m_date = date('Y/m/01', strtotime($date->order_date));
            }
        }
        return $m_date;
    }

    public function maxOrderDate()
    {
        $mx_date = '';
        $max_date = $this->find();
        $max_date->select(['order_date'])
            ->where([
                'DATE(Sale1s.order_date) <>' => '0000-00-00 00:00:00'
            ])
            ->order(['Sale1s.order_date' => 'DESC'])
            ->limit(1);
        if ($max_date->toArray()) {
            foreach ($max_date as $date) {
                $mx_date = date('Y/m/t', strtotime($date->order_date));
            }
        }
        return $mx_date;
    }

    public function responseErrorMessage($message = null)
    {
        $response = '';
        if ($message === 'not contain field name') {
            $response = __('not contain field name');
        } elseif ($message === 'contain too much field') {
            $response = __('contain too much field');
        } elseif ($message === 'cotain less field') {
            $response = __('cotain less field');
        } else {
            $response = __('invalid field');
        }

        return $response;
    }
}