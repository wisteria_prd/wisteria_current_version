<?php

namespace App\Model\Table;

use Cake\ORM\Table;

class Product1sTable extends Table
{
    public function initialize(array $config)
    {
        $this->table('product1s');
        $this->primaryKey('id');
        $this->addBehavior('Timestamp');

        $this->belongsTo('ProductBrands', array(
            'foreignKey' => 'product_brand_id',
            'joinType' => 'INNER',
        ));
    }

}