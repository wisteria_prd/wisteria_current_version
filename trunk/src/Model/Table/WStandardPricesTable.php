<?php

namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

class WStandardPricesTable extends Table
{
    public function initialize(array $config)
    {
        $this->primaryKey('id');
        $this->addBehavior('Timestamp');

        $this->belongsTo('Currencies', [
            'foreignKey' => 'currency_id',
            'joinType' => 'INNER',
        ]);

        $this->hasMany('ProductPackageDetails', [
            'foreignKey' => 'w_standard_price_id',
            'dependent' => false,
        ]);

        $this->belongsTo('ProductDetails');
    }

    public function validationDefault(Validator $validator)
    {
        $validator
            ->requirePresence('manufacturer_id')
            ->notEmpty('manufacturer_id', __('TXT_MESSAGE_REQUIRED'));

        $validator
            ->requirePresence('product_brand_id')
            ->notEmpty('product_brand_id', __('TXT_MESSAGE_REQUIRED'));

        $validator
            ->requirePresence('product_id')
            ->notEmpty('product_id', __('TXT_MESSAGE_REQUIRED'));

        $validator
            ->requirePresence('product_detail_id')
            ->notEmpty('product_detail_id', __('TXT_MESSAGE_REQUIRED'));

        return $validator;
    }

    public function getList($key)
    {
        $data = $this->find('list', [
            'keyField' => $key,
            'valueField' => 'standard_price',
        ]);

        return $data->toArray();
    }

    /**
     * Function getByProductDetailId
     * use for get data by product_detail_id
     * @param int $id
     * @return object
     */
    public function getByProductDetailId($id)
    {
        $data = $this->findByProductDetailId($id)->toArray();

        return $data;
    }
}