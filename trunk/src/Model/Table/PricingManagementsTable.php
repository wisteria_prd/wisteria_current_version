<?php
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

class PricingManagementsTable extends Table
{
    public function initialize(array $config)
    {
        parent::initialize($config);
        $this->primaryKey('id');
        $this->addBehavior('Timestamp');

        $this->hasMany('PricingRangeManagements', [
            'foreignKey' => 'pricing_management_id',
            'dependent' => false,
        ]);

        $this->belongsTo('Currencies', [
            'foreignKey' => 'currency_id',
            'joinType' => 'INNER',
        ]);

        // product
        $this->belongsTo('ProductDetails', [
            'foreignKey' => 'product_detail_id',
            'joinType' => 'INNER',
        ]);

        $this->belongsTo('Sellers', [
            'foreignKey' => 'seller_id',
            'joinType' => 'INNER',
        ]);

        $this->belongsTo('ProductBrands', [
            'foreignKey' => 'product_brand_id',
            'joinType' => 'INNER',
        ]);

        $this->belongsTo('Suppliers', [
            'foreignKey' => 'external_id',
            'conditions' => ['PricingManagements.external_id_type' => TYPE_SUPPLIER],
            'joinType' => 'LEFT',
        ]);

        $this->belongsTo('Manufacturers', [
            'foreignKey' => 'external_id',
            'conditions' => ['PricingManagements.external_id_type' => TYPE_MANUFACTURER],
            'joinType' => 'LEFT',
        ]);
    }

    public function validationDefault(Validator $validator)
    {
        $validator
            ->requirePresence('currency_id')
            ->notEmpty('currency_id', __d('validate', 'TXT_MESSAGE_REQUIRED'));

        $validator
            ->requirePresence('seller_id')
            ->notEmpty('seller_id', __d('validate', 'TXT_MESSAGE_REQUIRED'));

        $validator
            ->requirePresence('product_brand_id')
            ->notEmpty('product_brand_id', __d('validate', 'TXT_MESSAGE_REQUIRED'));

        $validator
            ->requirePresence('product_id')
            ->notEmpty('product_id', __d('validate', 'TXT_MESSAGE_REQUIRED'));

        $validator
            ->requirePresence('product_detail_id')
            ->notEmpty('product_detail_id', __d('validate', 'TXT_MESSAGE_REQUIRED'));

        $validator
            ->requirePresence('external_id_type')
            ->notEmpty('external_id_type', __d('validate', 'TXT_MESSAGE_REQUIRED'));

        $validator
            ->requirePresence('standard_price')
            ->notEmpty('standard_price', __d('validate', 'TXT_MESSAGE_REQUIRED'))
            ->add('standard_price', 'valid', [
                'rule' => function($value) {
                    return $value >= 0;
                },
                'message' => __d('validate', 'TXT_MESSAGE_VALUE_SMALLER_THAN_0')
            ]);
        
        $validator
            ->requirePresence('min_qty_for_standard_price')
            ->notEmpty('min_qty_for_standard_price', __d('validate', 'TXT_MESSAGE_REQUIRED'))
            ->add('min_qty_for_standard_price', 'valid', [
                'rule' => function($value) {
                    return $value >= 0;
                },
                'message' => __d('validate', 'TXT_MESSAGE_VALUE_SMALLER_THAN_0')
            ]);

        return $validator;
    }
}
