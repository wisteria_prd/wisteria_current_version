<?php
namespace App\Model\Behavior;

use Cake\ORM\Behavior;

class OrmBehavior extends Behavior
{
    /**
     * use for get all data from DB
     * @param string $find find type such as all, threaded
     * @param array $select select field
     * @param array $where condition
     * @param array $contain associate data
     * @param init $limit number of records
     * @param string $group field name
     * @param array $order order field
     * @return object
     */
    public function getAllDataList($find = 'all', $select = null, $where = null, $contain = null, $limit = null, $group = null, $order = null, $page = null)
    {
        $data = $this->_table->find($find);

        if ($select !== null) {
            $data = $data->select($select);
        }
        if ($where !== null) {
            $data = $data->where($where);
        }
        if ($contain !== null) {
            $data = $data->contain($contain);
        }
        if ($limit !== null) {
            $data = $data->limit($limit);
        }
        if ($group !== null) {
            $data = $data->group($group);
        }
        if ($order !== null) {
            $data = $data->order($order);
        }
        if ($page !== null) {
            $data = $data->page($page);
        }
        $data = $data->toArray();

        return $data;
    }

    /**
     * use for get data by id
     * @param int $id
     * @param array $select select field
     * @param array $contain associate data
     * @return object
     */
    public function getById($id, $select = null, $contain = null)
    {
        $data = $this->_table->findById($id);

        if ($select !== null) {
            $data = $data->select($select);
        }
        if ($contain !== null) {
            $data = $data->contain($contain);
        }
        $data = $data->first();

        return $data;
    }

    /**
     * use for get array list
     * @param string $key key of array
     * @param string $value value of array
     * @param array $where coditions
     * @param array $local local storage
     * @return array
     */
    public function getDataList($key_field, $value_field, $where = null, $local = null)
    {
        $list = [];
        $data = $this->_table->find('all');

        if ($where !== null) {
            $data = $data->where($where);
        }

        if ($data) {
            foreach ($data as $key => $value) {
                if ($local !== null) {
                    $field = $value_field . $local;
                    $name = $value[$field];

                    if (empty($name)) {
                        switch ($local) {
                            case '_en' :
                                $_field = $value_field;
                                $name = $value[$_field];
                                break;
                            default :
                                $_field = $value_field . '_en';
                                $name = $value[$_field];
                        }
                    }
                    $list[$value[$key_field]] = $name;
                } else {
                    $list[$value[$key_field]] = $value_field;
                }
            }
        }

        return $list;
    }

    /**
     * use for count records
     * @param array $where conditions
     * @param array $contain associate data
     * @return int total of records
     */
    public function countRecords($where = null, $contain = null)
    {
        $data = $this->_table->find('all');

        if ($where !== null) {
            $data = $data->where($where);
        }
        if ($contain !== null) {
            $data = $data->contain($contain);
        }
        $data = $data->count();

        return $data;
    }

    /**
     * use for get by field
     * @param array $where
     * @param array $select
     * @param array $contain
     * @return object
     */
    public function getByField($where, $select = null, $contain = null)
    {
        $data = $this->_table->find()
            ->where($where);

        if ($select !== null) {
            $data = $data->select($select);
        }
        if ($contain !== null)
        {
            $data = $data->contain($contain);
        }
        $data = $data->first();

        return $data;
    }
}
