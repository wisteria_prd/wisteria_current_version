<?php
//*****************************************************************************************************
//**  ＜＜ VTable TCPDF継承クラスライブラリ ＞＞
//**  仮想テーブル（VTable)上のセル位置を指定しながら帳票を作成できるクラスライブラリです。
//**
//**  Version:1.03 (2011/08/21)
//**
//**  Creation company : AGE Studio
//**  produce by E.Katayama
//**
//**  Copyright (C) 2011 All Rights Reserved by AGE Studio
//**  ※許可無くこのスクリプトの再配布などを禁止します.
//**  バグ報告は support@imadakeha.sakura.ne.jp 宛にお願いします。
//**
//**  TCPDF対応バージョン：
//**  Version: 5.9.***
//*****************************************************************************************************
require_once('tcpdf.php');

/**
 * VTable
 * @version 1.0
 * @copyright Copyright (c) 2011, E.Katayama
 */
class VTABLE_TCPDF extends TCPDF {
	/**
	 * 描画する枠線を描く位置を指定
	 * L:左側,T:上側,R:右側,B:下側
	 * S:斜め線（左上から右下）,/:斜め線（右上から左下）
	 * V:横線,H：縦線
	 *
	 * <code>
	 * $pdf->defParams[VTABLE_TCPDF::PRM_BORDER] = "L-R-B-T"; //上下右左に罫線を描く
	 * $pdf->defParams[VTABLE_TCPDF::PRM_BORDER] = "2L-2R-2B-2T"; //線の種別（２）を使用し罫線を描く（太線）
	 * </code>
	 */
	const PRM_BORDER = "border";

	/**
	 * 左右の描画位置を指定
	 * C:中心,L:左寄せ,R:右寄せ
	 */
	const PRM_ALIGN = "align";

	/**
	 * 上下の描画位置を指定
	 * M:上下中心,T:上寄せ,B:下寄せ
	 */
	const PRM_VALIGN = "valign";

	/** セル結合数（行）を指定 */
	const PRM_ROW_SPAN = "row_span";
	/** セル結合数（列）を指定 */
	const PRM_COL_SPAN = "col_span";

	/** フォントサイズを指定 */
	const PRM_FONT_SIZE = "font-size";
	/** フォントの色を指定 */
	const PRM_FONT_COLOR = "font-color";

	/**
	 * 枠線の種類を指定
	 * ※標準の線のスタイルから変更される項目だけ設定する。
	 *
	 * <code>
	 * //線の幅を変更し、色を変更する
	 * $pdf->defParams[VTABLE_TCPDF::PRM_BORDER_STYLE] = array('width' => 0.5, 'color' => array(255,200,255));
	 * </code>
	 */
	const PRM_BORDER_STYLE = "border-style";
	/** 線の種類（左側）を指定 */
	const PRM_BORDER_STYLE_L = "border-style-l";
	/** 線の種類（右側）を指定 */
	const PRM_BORDER_STYLE_R = "border-style-r";
	/** 線の種類（上側）を指定 */
	const PRM_BORDER_STYLE_T = "border-style-t";
	/** 線の種類（下側）を指定 */
	const PRM_BORDER_STYLE_B = "border-style-b";

	/** 丸角罫線 **/
	const PRM_BORDER_ROUND_RADIUS = "border-round-radius";	//コーナーの半径
	const PRM_BORDER_ROUND_CORNER = "border-round-corner";	//丸める角を指定
	const PRM_BORDER_ROUND_STYLE  = "border-round-style";	//D:罫線　F:塗りつぶし

	/**
	 * セル幅を指定（変更）する
	 * ※defParamに指定した場合、列幅の初期値となります。
	 * */
	const PRM_WIDTH = "width";
	/**
	 * セルの高さを指定（変更）する
	 * ※defParamに指定した場合、行高の初期値となります。
	 * */
	const PRM_HEIGHT = "height";

    /**
     * フォントの種類を指定
     *
     * <標準フォント>
     *  arialunicid0:Arial Uni CID0
     *  kozgopromedium:小塚ゴシックPro M
     *  kozminproregular:小塚明朝Pro M
     *  hysmyeongjostdmedium:HYSMyeongJoStd-Medium
     *  msungstdlight:MSungStd-Light
     *  stsongstdlight:STSongStd-Light
     */
	const PRM_FONT_FAMILY = "font-family";
	/** フォントスタイルを指定
	 * B:太字,I:斜め文字
	 */
	const PRM_FONT_STYLE = "font-style";
	/** フォントの縦書きを指定	 */
	const PRM_FONT_ORIENTATION = "font-orientation";
	/** 背景色を指定	 */
	const PRM_BACKGROUND_COLOR = "background-color";
	/** 背景画像を指定	 */
	const PRM_BACKGROUND_IMAGE = "background_image";
	/** 背景画像のパラメータを指定	 */
	const PRM_BACKGROUND_PARAM = "background_param";

	/** 文字の表示位置（Ｙ座標）を調整 */
	const PRM_MOVE_POS_Y = "move_pos_y";

	/**
	 * HTMLモード
	 * true:HTMLモードで出力 (1)
	 * false:標準モードで出力
	 *
	 * (1)ＨＴＭＬモードではセル表記（セルサイズ、罫線など）以外のパラメータ指定は無効となります
	 * **/
	const PRM_HTML_FLG = "html_flg";

   /**
    * 列の幅の配列
    * @access public
    */
	public $columnWidths = array();
   /**
    * 行の高さの配列
    * @access public
    */
	public $rowHeights = array();
   /**
    * 標準パラメータ
    * @access public
    */
	public $defParams = array();
   /**
    * 基本パラメータ
    * @access public
    */
	public $baseParams = array();

	/**
	 * 線の種別を設定
	 *
	 * 0: 0.125
	 * 2: 0.5 (太字)
	 *
	 * <code>
	 * $pdf->lineStyles[0] = array('width' => 0.125, 'cap' => 'square', 'join' => 'miter', 'dash' => 0, 'color' => $colors["line-color"]);
	 * </code>
	 */
	public $lineStyles = array();

    /**
    * 作業用パラメータ
    * @access private
    */
	private $_workParams = array(); //テーブル内標準（BeginTable～EndTableまで)
	private $_curPosX = 0;
	private $_curPosY = 0;
	private $_curPageNum = 0;

	/*
	 * ページの幅を取得する（オーバーライド）
	 */
	public function getPageWidth()
	{
		return $this->w -$this->lMargin - $this->rMargin;
	}
	public function getPageHeight()
	{
		return $this->h - $this->bMargin - $this->tMargin ;
	}

	/*
	 * コンストラクタ
	 */
	public function __construct($orientation='P', $unit='mm', $format='A4', $unicode=true, $encoding='UTF-8', $diskcache=false) {
		parent::__construct($orientation, $unit, $format, $unicode, $encoding, $diskcache);

		//デフォルトではヘッダーは表示しない
		$this->setPrintHeader(false);
		$this->setPrintFooter(false);
		$this->SetAutoPageBreak(0,true);

		//'cap' => 'square'：角をきれいに表示
		$this->lineStyles[0] = array('width' => 0.125, 'cap' => 'square', 'join' => 'miter', 'dash' => 0);
		$this->lineStyles[1] = array('width' => 0.5, 'cap' => 'square', 'join' => 'miter', 'dash' => 0);
		$this->lineStyles[2] = array('width' => 0.125, 'cap' => 'square', 'join' => 'miter', 'dash' => 0,'double' => 1);
	}

	/**
	 * テーブル作成　－開始
	 * @param array $rowHeights 横幅の配列
	 * @param array $columnWidths 列幅の配列
	 * @param array $params 標準パラメータ
	 */
	public function BeginTable($rowHeights = NULL,$columnWidths = NULL,$params = array())
	{
		if(isset($params["table-pos-x"]))
		{
			$this->x = $params["table-pos-x"];
		}
		if(isset($params["table-pos-y"]))
		{
			$this->y = $params["table-pos-y"];
		}
		if(!isset($rowHeights))
		{
			$rowHeights = array();
		}
		if(!isset($columnWidths))
		{
			$columnWidths = array();
		}

		$this->_curPosX = $this->x;
		$this->_curPosY = $this->y;

		$this->columnWidths = $columnWidths;
		$this->rowHeights = $rowHeights;
		$this->_workParams = $params;

		$backgroundParam = array(
			"type" => '',
			"link" => '',
			"align" => '',
			"resize" => true,
			"width" => 0,
			"height" => 0,
			"dpi" => 600,
			"palign" => '',
			"ismask" => false,
			"imgmask" => false,
			"border" => 0,
			"fitbox" =>false,
			"hidden" => false,
			"fitonpage" => false
		);

		$this->baseParams = array(
			"align" => 'J',"valign" => 'M',"row_span" => 1,"col_span" => 1,
			"font-size" => $this->FontSizePt,"font-family" => $this->FontFamily,
			"font-style" => $this->FontStyle,"move_pos_y" => 0,
			"font-color" => array(0,0,0), "background_param" => $backgroundParam,
			"border-round-radius" => 0,
			"border-round-corner" => "",
			"border-round-style" => "");

		$this->_curPageNum = $this->page;
	}

	/**
	 * テーブル作成　－終了
	 */
	public function EndTable()
	{
		$pos = $this->getTableRect();

		$this->x = $this->_curPosX;
		$this->y = $pos["y2"];
	}

	/**
	 *　テーブル矩形情報を取得する
	 */
	public function getTableRect()
	{
		$r = count($this->rowHeights);
		$c = count($this->columnWidths);

		$arrPos = $this->GetPosWriteCell($r,$c,$r,$c);

		foreach($arrPos as $pos)
		{
			$maxPosX = $pos["x2"];
			$maxPosY = $pos["y2"];
		}

		return array("x1" => $this->_curPosX,"y1" => $this->_curPosY,"x2" => $maxPosX,"y2" => $maxPosY);
	}

	/**
	 * テーブル開始座標のロード
	 */
	public function LoadCurPos()
	{
		$this->x = $this->_curPosX;
		$this->y = $this->_curPosY;
	}

	/**
	 * 仮想セルを描画する。
	 *
	 * ※sellのMarginにはまだ対応していません。
	 *
	 * @param int $row1 行番号
	 * @param int $col1 列番号
	 * @param string $txt セルに書き込む文字列
	 * @param string $border 出力する線の種類を指定
	 * @param array $params セルパラメータを指定
	 */
	public function WriteCell($row1,$col1, $txt,$border = "",$params = array())
	{
		//基本パラメータ　＜ 標準パラメータ　＜ 作業パラメータ
		$params = array_merge($this->baseParams,$this->defParams,$this->_workParams, $params);
		$params["background_param"] =
			array_merge(
				(isset($this->baseParams["background_param"])?$this->baseParams["background_param"]:array()),
				(isset($this->defParams["background_param"])?$this->defParams["background_param"]:array()),
				(isset($this->_workParams["background_param"])?$this->_workParams["background_param"]:array()),
				(isset($params["background_param"])?$params["background_param"]:array()));

		if($row1 != 0)
		{
			$row2 = $row1 + $params["row_span"] - 1;
		}
		else
		{
			$row1 = 1;
			$row2 = count($this->rowHeights);
		}

		if($col1 != 0)
		{
			$col2 = $col1 + $params["col_span"] - 1;
		}
		else
		{
			$col1 = 1;
			$col2 = count($this->columnWidths);
		}

		$this->SetFont($params["font-family"],$params["font-style"],$params["font-size"]);
		$this->SetTextColor($params["font-color"][0],$params["font-color"][1],$params["font-color"][2]);

		//縦書き
		if($params["font-orientation"] == "vertical")
		{
			$aRes = array();
			$sStr = $txt;
			while ($iLen = mb_strlen($sStr, 'UTF-8'))
			{
				array_push($aRes, mb_substr($sStr, 0, 1, 'UTF-8'));
				$sStr = mb_substr($sStr, 1, $iLen, 'UTF-8');
			}
			$txt = join( "\n", $aRes);
		}

		//行幅・列幅の指定は列結合に対応していません。
		if(isset($params["width"]))
		{
			for($c = $col1;$c <= $col2;$c++)
			{
				if(!isset($this->columnWidths[$c-1]))
				{
					$this->columnWidths[$c-1] = $params["width"];
				}
			}
		}

		//行指定・行高さの自動
		if($row1 == $row2 && $params["height"] == "auto")
		{
			$x1 = $this->_curPosX + array_sum(array_slice($this->columnWidths,0,$col1));
			$x2 = $this->_curPosX + array_sum(array_slice($this->columnWidths,0,$col2+1));
			$w = $x2 - $x1;

			//表示文字の行数を取得し、文字の高さを求める。
			$this->rowHeights[$row1-1] = $this->getStringHeight($w, $txt, true, false, '', 0);
		}

		for($r = $row1;$r <= $row2;$r++)
		{
			if(!isset($this->rowHeights[$r-1]))
			{
				$this->rowHeights[$r-1] = $params["height"];
			}
		}

		//ＸＹ座標を取得
		$arrPos = $this->GetPosWriteCell($row1,$col1,$row2,$col2);

		$idx = 0;
		foreach($arrPos as $pos)
		{
			$pageNum = (int)$pos["page_num"];
			if($pageNum <= $this->numpages)
			{
				$this->setPage($pageNum);
			}
			else
			{
				$this->AddPage();
			}

			$x1 = $pos["x1"] + $params["padding"]["L"];
			$x2 = $pos["x2"] - $params["padding"]["R"];

			$w = $x2 - $x1;
			$h = $pos["y2"] - $pos["y1"];

			if((boolean)$params["html_flg"] === false)
			{
				$marginTop = 0;

				//半角\マークはバックスラッシュとして表示されるため、全角に変換
				$txt = str_replace("\\","￥",$txt);

				//表示文字の行数を取得し、文字の高さを求める。
				$fontHeight = $this->getStringHeight($w, $txt, 0, true, '', 0);

				//背景のイメージ
				if(isset($params["background_image"]))
				{
					$backgroundImage = $params["background_image"];
					$backgroundParam = $params["background_param"];

					if($backgroundParam["resize"] === true)
					{
						$imgWidth = $pos["x2"] - $pos["x1"];
						$imgHeight = $pos["y2"] - $pos["y1"];
					}
					else
					{
						$imgWidth = $backgroundParam["width"];
						$imgHeight = $backgroundParam["height"];
					}

					switch($params[VTABLE_TCPDF::PRM_ALIGN])
					{
						case "C":
							$this->x = $pos["x1"] + ($w / 2) - ($imgWidth / 2);
							break;
						case "L":
							$this->x = $pos["x1"];
							break;
						case "R":
							$this->x = $pos["x2"] - $imgWidth;
							break;
					}

					switch($params[VTABLE_TCPDF::PRM_VALIGN])
					{
						case "M":
							$this->y = $pos["y1"] + ($h / 2) - ($imgHeight / 2);
							break;
						case "T":
							$this->y = $pos["y1"];
							break;
						case "B":
							$this->y = $pos["y2"] - $imgHeight;
							break;
					}

					$this->Image($backgroundImage, $this->x, $this->y,
							$imgWidth,
							$imgHeight,
							$backgroundParam["type"],
							$backgroundParam["link"],
							$backgroundParam["align"],
							$backgroundParam["resize"],
							$backgroundParam["dpi"],
							$backgroundParam["palign"],
							$backgroundParam["ismask"],
							$backgroundParam["imgmask"],
							$backgroundParam["border"],
							$backgroundParam["fitbox"],
							$backgroundParam["hidden"],
							$backgroundParam["fitonpage"]);
				}
				//背景の書き込み
				else if(isset($params["background-color"]))
				{
					if((int)$params[VTABLE_TCPDF::PRM_BORDER_ROUND_RADIUS] == 0)
					{
						$backgroundColor = $params["background-color"];

						$this->x = $pos["x1"];
						$this->y = $pos["y1"];

						$this->FillColor = sprintf('%.3F %.3F %.3F rg', ($backgroundColor[0] / 255), ($backgroundColor[1] / 255), ($backgroundColor[2] / 255));
						$this->SetFontSize(0);
						$this->MultiCell($pos["x2"] - $pos["x1"], $pos["y2"] - $pos["y1"],"" , 0,'', true, 0, '', '', true, 0, false,false, 0);
					}
				}


				$y1 = $pos["y1"] + $params["padding"]["T"];


				if($params["valign"] == 'M')
				{
					$marginTop = $h /2 - $fontHeight / 2 ;
				}
				elseif($params["valign"] == 'B')
				{
					$marginTop = $h - $fontHeight;
				}
				elseif($params["valign"] == 'T')
				{
					$marginTop = 0;
				}

				$this->x = $x1;
				$this->y = $y1 + $marginTop + $params["move_pos_y"];
				$this->SetFontSize($params["font-size"]);

				//               $w,$h,$txt ,$border=0,$align='J'      , $fill=false, $ln=1 ,$x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=0, $valign='T', $fitcell=false
				$this->MultiCell($w,$fontHeight,$txt ,"",$params["align"], false      , 0     ,''   , ''   , true       , 0         , false        , true );
			}
			else
			{
				$fillFlg = false;
				//ＨＴＭＬ出力時はいくつかのパラメータが無効になります。

				//背景の書き込み
				if(isset($params["background-color"]))
				{
					if((int)$params[VTABLE_TCPDF::PRM_BORDER_ROUND_RADIUS] == 0)
					{
						$fillFlg = true;
						$backgroundColor = $params["background-color"];

						$this->FillColor = sprintf('%.3F %.3F %.3F rg', ($backgroundColor[0] / 255), ($backgroundColor[1] / 255), ($backgroundColor[2] / 255));
					}
				}

				//$w, $h, $txt,
				//$border=0, $align='J', $fill=false, $ln=1,
				//$x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=0, $valign='T', $fitcell=false
				$h = $pos["y2"] - $pos["y1"];
				$this->y = $pos["y1"];
				$this->MultiCell($w,$h,$txt , 0,$params["align"], $fillFlg, 1,'', '', true, 0, true,true);

				if($pos["y2"] <= $this->y)
				{
					$pos["y2"] = $this->y;
					$this->rowHeights[$row1-1] = $pos["y2"] - $pos["y1"];
				}
			}

			//罫線を引く
			$borderTmp = $border;
			if(count($arrPos) > 1)
			{
				if($idx == 0)
				{
					$borderTmp = str_replace(array("b","B"),"",$borderTmp);
				}
				elseif($idx == (count($arrPos) - 1))
				{
					//最後
					$borderTmp = str_replace(array("t","T"),"",$borderTmp);
				}
				else
				{
					$borderTmp = str_replace(array("b","B"),"",$borderTmp);
					$borderTmp = str_replace(array("t","T"),"",$borderTmp);
				}
			}
			$this->WriteCellLine($borderTmp,$pos["x1"],$pos["y1"],$pos["x2"],$pos["y2"],$params);

			//角丸の罫線を引く
			if((int)$params[VTABLE_TCPDF::PRM_BORDER_ROUND_RADIUS] > 0)
			{
				$radius = $params[VTABLE_TCPDF::PRM_BORDER_ROUND_RADIUS];
				$corner = $params[VTABLE_TCPDF::PRM_BORDER_ROUND_CORNER];
				$style = $params[VTABLE_TCPDF::PRM_BORDER_ROUND_STYLE];
				if(isset($params[VTABLE_TCPDF::PRM_BACKGROUND_COLOR]))
					$color = $params[VTABLE_TCPDF::PRM_BACKGROUND_COLOR];
				else
					$color = null;

				$this->SetLineStyle($params["border-style"]);
				$this->RoundedRect($pos["x1"], $pos["y1"], $pos["x2"]-$pos["x1"], $pos["y2"]-$pos["y1"], $radius, $corner , $style, null, $color);
			}

			$idx++;
		}
	}

	/*
	 * 罫線を引く
	 */
	public function WriteCellLine($border,$x1, $y1, $x2, $y2,$params)
	{
		$chgPage = false;

		if($border == "")
		{
			$border = $params[VTABLE_TCPDF::PRM_BORDER];
			if($border == "")
			{
				return;
			}
		}
		$border = strtolower($border);

		$x = $x1 + ($x2 - $x1) / 2;
		$y = $y1 + ($y2 - $y1) / 2;

		$arrItems =
			array(
				's' => array($x1, $y1, $x2, $y2),
				'/' => array($x1, $y2, $x2, $y1),
				'l' => array($x1, $y1, $x1, $y2),
				'r' => array($x2, $y1, $x2, $y2),
				't' => array($x1, $y1, $x2, $y1),
				'b' => array($x1, $y2, $x2, $y2),
				'v' => array($x,  $y1, $x,  $y2),
				'h' => array($x1, $y,  $x2, $y)
			);

		//罫線を引く
		foreach($arrItems as $key => $range )
		{
			$pos = stripos($border,$key,0);
			if($pos !== false)
			{
				if($pos > 0)
					$widthIdx = (int)substr($border,$pos-1,1);
				else
					$widthIdx = 0;

				if(isset($params["border-style-" . $key]))
				{
					$arrLineStyle = array_merge($this->lineStyles[$widthIdx], $params["border-style-" . $key]);
				}
				else if(isset($params["border-style"]))
				{
					$arrLineStyle = array_merge($this->lineStyles[$widthIdx], $params["border-style"]);
				}
				else
				{
					$arrLineStyle = $this->lineStyles[$widthIdx];
				}

				$dbl = (int)$arrLineStyle["double"];

				$bw = array();
				$bw["t"] = (stripos($border,"t",0) === false?$arrLineStyle["width"] / 2:0);
				$bw["b"] = (stripos($border,"b",0) === false?$arrLineStyle["width"] / 2:0);
				$bw["l"] = (stripos($border,"l",0) === false?$arrLineStyle["width"] / 2:0);
				$bw["r"] = (stripos($border,"r",0) === false?$arrLineStyle["width"] / 2:0);

				switch($key)
				{
					case "l":
						$this->Line($range[0], $range[1] + $bw["t"],$range[2],$range[3] - $bw["b"],$arrLineStyle);
						if($dbl > 0)
							$this->Line($range[0] + $dbl, $range[1] + $bw["t"],$range[2] + $bw["b"] + $dbl,$range[3],$arrLineStyle);
						break;
					case "r":
						$this->Line($range[0], $range[1] + $bw["t"],$range[2],$range[3] - $bw["b"],$arrLineStyle);
						if($dbl > 0) $this->Line($range[0] - $dbl, $range[1] + $bw["t"],$range[2] - $dbl,$range[3] - $bw["b"],$arrLineStyle);
						break;
					case "t":
						$this->Line($range[0] + $bw["l"], $range[1] ,$range[2] - $bw["r"],$range[3],$arrLineStyle);
						if($dbl > 0) $this->Line($range[0] + $bw["l"], $range[1] + $dbl,$range[2] - $bw["r"],$range[3] + $dbl,$arrLineStyle);
						break;
					case "b":
						$this->Line($range[0] + $bw["l"], $range[1],$range[2] - $bw["r"],$range[3],$arrLineStyle);
						if($dbl > 0) $this->Line($range[0] + $bw["l"], $range[1]- $dbl,$range[2] - $bw["r"],$range[3] - $dbl,$arrLineStyle);
						break;
					case "s":
						$arrLineStyle['cap'] = 'butt';
						$this->Line($range[0], $range[1], $range[2],$range[3],$arrLineStyle);
						break;
					case "/":
						$arrLineStyle['cap'] = 'butt';
						$this->Line($range[0], $range[1], $range[2],$range[3],$arrLineStyle);
						break;
					case "v":
						$this->Line($range[0], $range[1] + $bw["t"],$range[2],$range[3] - $bw["b"],$arrLineStyle);
						break;
					case "h":
						$this->Line($range[0] + $bw["l"], $range[1],$range[2] - $bw["r"],$range[3],$arrLineStyle);
						break;
				}
			}
		}
	}

	/**
	 * 仮想セルに出力する座標を取得する。
	 * @param unknown_type $row1
	 * @param unknown_type $col1
	 * @param unknown_type $row2
	 * @param unknown_type $col2
	 * @param unknown_type $border
	 * @param unknown_type $align
	 */
	public function GetPosWriteCell($row1,$col1,$row2,$col2)
	{
		global $cmn,$log;

		$arrPos = array();

		if($col2===0)
			$col2 = $col1;
		if($row2===0)
			$row2 = $row1;

		$col1--;$row1--;$col2--;$row2--;

		$this->LoadCurPos();

		$y = $this->_curPosY - $this->tMargin;
		$pageNum = $this->_curPageNum;
		$arrRows = array();
		for($row = 0;$row <= $row2;$row++)
		{
			if( ($y + $this->rowHeights[$row]) >= $this->getPageHeight() )
			{
				$y = 0;
				$pageNum++;
			}

			$arrRows[$row] = array("y1" => $y,"y2" => $y + $this->rowHeights[$row],"page_num" => $pageNum);

			$y += $this->rowHeights[$row];
		}

		$stRow = $row1;

		$pageNum = $arrRows[$row1]["page_num"];
		for($r = $row1;$r <= $row2;$r++)
		{
			$p = $arrRows[$r]["page_num"];

			//ページがまだ作成されていなければ、先頭行の情報をセット
			if(!isset($arrPos[$p]))
			{
				$arrPos[$p]["page_num"] = $p;
				$arrPos[$p]["x1"] = $this->_curPosX + array_sum(array_slice($this->columnWidths,0,$col1));
				$arrPos[$p]["x2"] = $this->_curPosX + array_sum(array_slice($this->columnWidths,0,$col2+1));
				$arrPos[$p]["y1"] = $arrRows[$r]["y1"] + $this->tMargin;
				$arrPos[$p]["y2"] = $arrRows[$r]["y2"] + $this->tMargin;
			}
			else
			{
				$arrPos[$p]["y2"] = $arrRows[$r]["y2"] + $this->tMargin;
			}
		}

		return $arrPos;
	}
}

