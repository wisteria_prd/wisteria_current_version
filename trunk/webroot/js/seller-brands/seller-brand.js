$(function (e) {
    getAutocomplete('.select-brand', BASE + 'ProductBrands/getListOfProductBrandsByAjax');

    $('.btn-prd-brand').click(function(e) {
        var index = $('body').find('.form-group-wrap .form-group').length + 1;
        var content = '<div class="form-group">' +
                '<label class="col-sm-3 control-label"></label>' +
                '<div class="col-sm-5 custom-select">' +
                    '<select class="form-control select-brand" name="product_brand_id[' + index + ']">' +
                        '<option value="">' + TXT_SELECT_PRODUCT_BRAND + '</option>' +
                    '</select>' +
                '</div>' +
                '<div class="col-sm-2">' +
                    '<button type="button" class="btn btn-delete btn-sm btn-remove-field">' +
                        '<i class="fa fa-trash" aria-hidden="true"></i>' +
                    '</button>' +
                '</div>' +
            '</div>';
        $('body').find('.form-group-wrap').append(content);
        getAutocomplete('.select-brand', BASE + 'ProductBrands/getListOfProductBrandsByAjax');
    });

    $('.btn-prd-brand-register').click(function (e) {
        let form = $('form[name="product_brand_form"]');
        let options = {
            url: BASE + 'SellerBrands/add',
            type: 'POST',
            data: {
                data: $(form).serialize(),
                seller_id: $('meta[name="seller_id"]').attr('content')
            },
            beforeSend: function () {
                $(form).find('.error-message').remove();
                $.LoadingOverlay('show');
            }
        };
        ajaxRequest(options, SESSION_TIMEOUT, function (data) {
            if (data.message === MSG_ERROR) {
                $.each (data.data, function (index, value) {
                    if (value.length == 0) {
                        return true;
                    }
                    let message = '<label class="error-message">' + value.product_brand_id + '</label>';
                    let content = $(form).find('select').eq(index);
                    $(content).closest('div').append(message);
                });
                return false;
            }
            $('.modal-close').click();
        });
    });

    function getAutocomplete(element = null, target_url = null)
    {
        $(element).select2({
            width: '100%',
            minLength: 0,
            ajax: {
                url: target_url,
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    let query = {
                        keyword: params.term
                    };
                    return query;
                },
                processResults: function (data) {
                    let dataSource = [];
                    if (data.data === null && data.data === 'undefined') {
                        return false;
                    }
                    $.each(data.data, function(i, v) {
                        dataSource.push({
                            id: v.id,
                            text: v.full_name
                        });
                    });
                    return { results: dataSource };
                }
            }
        });
    }
});