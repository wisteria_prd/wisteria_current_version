$(function(e) {
    $('#select-type').change(function(e) {
        let disabled = {
            'disabled': true
        };
        if ($(this).val() !== CUSTOMER) {
            disabled.disabled = true;
            $('.customer-wrap').empty();
        } else {
            disabled.disabled = false;
        }
        $('.btn-add-c').attr(disabled);
    });

    $('#person-name').select2({
        width: '100%',
        minLength: 0,
        ajax: getAutocomplete()
    });

    $('.btn-add-c').click(function(e) {
        let content = $('.customer-wrap');
        let inx = $(content).find('.form-group').length;
        let element = '<div class="form-group custom-select">' +
                    '<label class="col-sm-2 control-label"></label>' +
                    '<div class="col-lg-4 col-md-4 col-sm-4">' +
                        '<select class="form-control" name="customer_subsidiaries[' + inx + '][customer_id]">' +
                            '<option value="">' + TXT_SELECT_TYPE + '</option>' +
                        '</select>' +
                    '</div>' +
                    '<button type="button" class=" btn btn-delete btn-sm form-btn-add btn-rm-position">' +
                        '<i class="fa fa-trash" aria-hidden="true"></i>' +
                    '</button>' +
                '</div>';
        $(content).append(element);
        $(content).find('select').select2({
            width: '100%',
            minLength: 0,
            ajax: getAutocomplete()
        });
    });

    $('body').on('click', '#btn-register', function(e) {
        let form = $('body').find('form[name="subsidiary_form"]');
        let options = {
            type: 'POST',
            url: BASE + 'Subsidiaries/saveOrUpdate',
            data: $(form).serialize(),
            beforeSend: function () {
                $(form).find('.error-message').remove();
                $.LoadingOverlay('show');
            }
        };
        ajaxRequest(options, SESSION_TIMEOUT, function (data) {
            if (data.message === MSG_ERROR) {
                showMessageError(form, data.data);
                if ('info_details' in data.data) {
                    showAssociateMessageError(form, data.data.info_details, 'info_details');
                }
                // if (data.data.customer_subsidiaries !== null) {
                //     CustomerSubsidiariesMsg(data.data.customer_subsidiaries);
                // }
                return false;
            }
            location.href = BASE + 'Subsidiaries';
        });
    });

    // ADD MULTIPLE TELS
    $('body').on('click', '.btn-tel-add', function(e) {
        var index = $('body').find('.tel-box-wrap .form-group').length + 1;
        var content = '<div class="form-group">' +
                '<label class="col-sm-2 control-label"></label>' +
                '<div class="col-md-4 col-lg-4 col-sm-4">' +
                    '<input type="tel" placeholder="' + TXT_ENTER_TEL + '" class="form-control" name="info_details[' + index + '][tel]"/>' +
                    '<input type="hidden" name="info_details[' + index + '][type]" value="' + SUBSIDIARY + '"/>' +
                '</div>' +
                '<div class="col-md-4 col-lg-4 col-sm-4">' +
                    '<input type="text" placeholder="' + STR_TEL_EXT + '" class="form-control" name="info_details[' + index + '][tel_extension]"/>' +
                '</div>' +
                '<div class="col-sm-1">' +
                    '<button type="button" class=" btn btn-delete btn-sm form-btn-add btn-remove-field">' +
                        '<i class="fa fa-trash" aria-hidden="true"></i>' +
                    '</button>' +
                '</div>' +
            '</div>';
        $('body').find('.tel-box-wrap').append(content);
    });

    $('body').on('click', '.btn-email-add', function(e) {
        let index = $('body').find('.email-box-wrap .form-group').length + 1;
        let content = '<div class="form-group">' +
                    '<label class="col-sm-2 control-label"></label>' +
                    '<div class="col-lg-4 col-md-4 col-sm-4">' +
                        '<input type="email" placeholder="' + TXT_ENTER_EMAIL + '" class="form-control" name="info_details[' + index + '][email]"/>' +
                        '<input type="hidden" name="info_details[' + index + '][type]" value="' + SUBSIDIARY + '"/>' +
                    '</div>' +
                    '<button type="button" class=" btn btn-delete btn-sm form-btn-add btn-rm-position"><i class="fa fa-trash" aria-hidden="true"></i></button>' +
                '</div>';
        $('body').find('.email-box-wrap').append(content);
    });

    $('body').on('click', '.btn-rm-position', function(e) {
        $(this).closest('.form-group').remove();
    });

    $('body').on('click', '.btn-payment-cats', function(e) {
        let options = {
            url: BASE + 'PaymentCategories/getPaymentForm',
            type: 'GET',
            beforeSend: function () {
                $.LoadingOverlay('show');
            }
        };
        ajaxRequest(options, SESSION_TIMEOUT, function (data) {
            $('body').prepend(data);
            $('body').find('.modal-payment-cat').modal('show');
        });
    });

    $('body').on('click', '.btn-invoice-methods', function(e) {
        let options = {
            url: BASE + 'InvoiceDeliveryMethods/getList',
            type: 'GET',
            beforeSend: function () {
                $.LoadingOverlay('show');
            }
        };
        ajaxRequest(options, SESSION_TIMEOUT, function (data) {
            $('body').prepend(data);
            $('body').find('.modal-invoice-method').modal('show');
        });
    });

    function getAutocomplete()
    {
        let data = {
            url: BASE + 'Subsidiaries/getSubType',
            dataType: 'json',
            delay: 250,
            data: function (params) {
                let query = {
                    keyword: params.term,
                    type: $('#select-type').val()
                };
                return query;
            },
            processResults: function (data) {
                let dataSource = [];
                if (data.data === null && data.data === 'undefined') {
                    return false;
                }
                $.each(data.data, function(i, v) {
                    dataSource.push({
                        id: v.id,
                        text: v.full_name
                    });
                });
                return { results: dataSource };
            }
        };

        return data;
    }

    function CustomerSubsidiariesMsg(data)
    {
        if (data !== null && data !== 'undefined') {
            $.each(data, function (i, v) {
                let content = $('.customer-wrap');
                let message = '<label class="error-message">' + v.customer_id._empty + '</label>';
                $(content).find('select[name="customer_subsidiaries[' + i + '][customer_id]"]')
                        .closest('.col-sm-7')
                        .append(message);
            });
        }
    }
});
