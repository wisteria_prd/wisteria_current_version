$(function(e) {
    $('.data-tb-list .table > tbody > tr').click(function (e) {
        let options = {
            type: 'GET',
            url: BASE + 'Customers/view',
            data: {
                id: $(this).closest('tr').attr('data-id')
            },
            beforeSend: function() {
                $.LoadingOverlay('show');
            }
        };
        ajaxRequest(options, SESSION_TIMEOUT, function(data) {
            $('body').prepend(data);
            $('body').find('#modal_detail').modal('show');
        });
    });

    $('.btn_comment').click(function (e) {
        e.stopPropagation();
        let params = {
            id: $(this).closest('tr').attr('data-id'),
            type: CUSTOMER,
            referer: CONTROLLER,
        };
        let options = {
            url: BASE + 'Messages/getMessageList',
            type: 'GET',
            data: params,
            beforeSend: function () {
                $.LoadingOverlay('show');
            }
        };
        ajaxRequest(options, SESSION_TIMEOUT, function (data) {
            $('body').prepend(data);
            $('body').find('.modal-message').modal('show');
        });
    });

    $('.table td a, .table td select').click(function(e) {
        e.stopPropagation();
    });
});
