$(function(e) {
    var obj = [];

    $('.btn-save').click(function(e) {
        let form = $('form[name="invoice_form"]');
        let options = {
            type: 'POST',
            url: BASE + 'InvoiceDeliveryMethods/saveOrUpdate/',
            data: $(form).serialize(),
            beforeSend: function() {
                $.LoadingOverlay('show');
                $(form).find('.error-message').remove();
            }
        };
        ajaxRequest(options, SESSION_TIMEOUT, function (data) {
            if (data.message === MSG_ERROR) {
                displayError(form, data.data);
            } else {
                displayList(form, data.data.invoices);
            }
        });
    });

    $('body').on('click', '.btn-edit-payment', function(e) {
        let tr = $(this).parents('tr');
        var params = {
            name: $(tr).find('td:eq(1)').text(),
            name_en: $(tr).find('td:eq(2)').text(),
            id: $(tr).attr('data-id')
        };
        $('input[name="name"]').val(params.name);
        $('input[name="name_en"]').val(params.name_en);
        $('input[name="id"]').val(params.id);
        $('.btn-save').text(TXT_UPDATE);
    });

    $('body').on('click', '.btn-delete-payment', function(e) {
        let tr = $(this).parents('tr');
        let id = $(tr).attr('data-id');
        $('#modal_delete').modal('show');
        $('#modal_delete').find('input[name="id"]').val(id);
    });

    $('#btn_delete_yes').click(function(e) {
        let id = $('#modal_delete').find('.delete-item').val();
        let form = $('form[name="payment_form"]');
        var options = {
            type: 'POST',
            url: BASE + 'InvoiceDeliveryMethods/delete/',
            data: { id: id },
            beforeSend: function() {
                $.LoadingOverlay('show');
            }
        };
        ajaxRequest(options, SESSION_TIMEOUT, function (data) {
            if (data.message === MSG_SUCCESS) {
                $('#modal_delete').modal('hide');
                displayList(form, data.data.invoices);
            }
        });
    });

    $('.btn-close').click(function(e) {
        if (obj.length > 0) {
            var options = '';
            var selected = $('body').find('select[name="invoice_delivery_method_id"]');
            $(selected).find('option:not(:first)').remove();
            $.each(obj, function(i, v) {
                options += '<option value="' + v.id + '">' + v.name + ' ' + v.name_en + '</option>';
            });
            $(selected).append(options);
        }
    });

    function displayList(form, data)
    {
        if ((data === null) && (data !== 'undefined')) {
            return false;
        }
        obj = data;
        //RESET FORM
        $(form).find('input[name="id"]').remove();
        $(form).find('input[name="name_en"], input[name="name"]').val('');
        $(form).find('.btn-save').text(TXT_ADD);

        var table = $('.table > tbody');
        var element = '';
        $.each(data, function(i, v) {
            element += '<tr data-id="' + v.id + '">' +
                        '<td>' + (i+1) + '</td>' +
                        '<td>' + v.name + '</td>' +
                        '<td>' + v.name_en + '</td>' +
                        '<td>' +
                            '<button type="button" class="btn btn-primary btn-sm btn-edit-payment">' +
                                '<span><i class="fa fa-pencil-square-o" aria-hidden="true"></i></span>' +
                            '</button>&nbsp;&nbsp;' +
                            '<button type="button" class="btn btn-delete btn-sm btn-delete-payment">' +
                                '<span><i class="fa fa-trash" aria-hidden="true"></i></span>' +
                            '</button>' +
                        '</td>' +
                    '</tr>';
        });
        $(table).empty()
                .html(element);
    }

    $('.btn-close').click(function(e) {
        if (obj.length > 0) {
            let options = '';
            let selected = $('body').find('select[name="invoice_delivery_method_id"]');
            $(selected).find('option:not(:first)').remove();
            $.each(obj, function(i, v) {
                options += '<option value="' + v.id + '">' + v.name + ' ' + v.name_en + '</option>';
            });
            $(selected).append(options);
        }
    });
});