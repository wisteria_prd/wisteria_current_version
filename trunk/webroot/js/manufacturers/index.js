$(function (e) {
    /*
     * INDEX PAGE
     */
    var maxPage = parseInt($('#maxPage').val());
    updatePagination(maxPage);

    $('.table>tbody>tr>td a').click(function (e) {
        e.stopPropagation();
    });

    $('.data-tb-list .table>tbody>tr').click(function (e) {
        let options = {
            type: 'GET',
            url: BASE + 'manufacturers/view',
            data: { id:  $(this).closest('tr').data('id') },
            beforeSend: function () {
                $.LoadingOverlay('show');
            }
        };
        ajaxRequest(options, SESSION_TIMEOUT, function (data) {
            $('body').prepend(data);
            $('body').find('.modal-view').modal('show');
        });
    });

    $('.btn-restore').click(function (e) {
        e.stopPropagation();
        let params = {
            id: $(this).closest('tr').attr('data-id'),
            status: $(this).closest('td').attr('data-target')
        };
        let options = {
            url: BASE + 'manufacturers/get-status',
            type: 'GET',
            data: params,
            beforeSend: function () {
                $.LoadingOverlay('show');
            }
        };
        ajaxRequest(options, SESSION_TIMEOUT, function (data) {
            $('body').prepend(data);
            let content = $('body').find('.modal-status');
            $(content).modal('show');
            _iframe(content);
        });
    });

    $('.btn-delete').click(function (e) {
        e.stopPropagation();
        let params = {
            id: $(this).closest('tr').attr('data-id')
        };
        let options = {
            url: BASE + 'manufacturers/get-delete',
            type: 'GET',
            data: params,
            beforeSend: function () {
                $.LoadingOverlay('show');
            }
        };
        ajaxRequest(options, SESSION_TIMEOUT, function (data) {
            $('body').prepend(data);
            let content = $('body').find('.modal-delete');
            $(content).modal('show');
            _iframe(content);
        });
    });

    $('.btn_comment').click(function (e) {
        e.stopPropagation();
        let params = {
            id: $(this).closest('tr').attr('data-id'),
            type: MANUFACTURER_TYPE,
            referer: CONTROLLER,
        };
        let options = {
            url: BASE + 'messages/get-message-list',
            type: 'GET',
            data: params,
            beforeSend: function () {
                $.LoadingOverlay('show');
            }
        };
        ajaxRequest(options, SESSION_TIMEOUT, function (data) {
            $('body').prepend(data);
            $('body').find('.modal-message').modal('show');
        });
    });

    function _iframe(content)
    {
        /*
        * CHANGE OF STATUS MODAL
        */
        $(content).find('.change-status').click(function (e) {
            let params = {
                id: $(content).find('[name="id"]').attr('content'),
                status: $(content).find('[name="status"]').attr('content')
            };
            let options = {
                url: BASE + 'manufacturers/change-of-status',
                type: 'POST',
                data: params,
                beforeSend: function () {
                    $.LoadingOverlay('show');
                }
            };
            ajaxRequest(options, SESSION_TIMEOUT, function (data) {
                if (data.message === MSG_SUCCESS) {
                    location.reload();
                }
            });
        });

        /**
        * DELETE MODAL
        */
        $(content).find('.delete-mf').click(function (e) {
            let params = {
                id: $(content).find('[name="id"]').attr('content')
            };
            let options = {
                url: BASE + 'manufacturers/delete',
                type: 'POST',
                data: params,
                beforeSend: function () {
                    $.LoadingOverlay('show');
                }
            };
            ajaxRequest(options, SESSION_TIMEOUT, function (data) {
                if (data.message === MSG_SUCCESS) {
                    location.reload();
                }
            });
        });
    }
});
