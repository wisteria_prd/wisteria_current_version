$(function (e) {
    $('.select-countries').select2();
    initialTelInput();

    $('#btn-register').click(function (e) {
        let form = $('body').find('.form-manufacturer');
        // UPDATE DIAL CODE BEFORE SUBMIT
        updateDialCode(form);
        let options = {
            type: 'POST',
            url: BASE + 'Manufacturers/saveAndUpdate',
            data: $(form).serialize(),
            beforeSend: function () {
                $.LoadingOverlay('show');
                $(form).find('.error-message').remove();
            }
        };
        ajaxRequest(options, SESSION_TIMEOUT, function (data) {
            if (data.message === MSG_ERROR) {
                showMessageError(form, data.data);
                if ('info_details' in data.data) {
                    showAssociateMessageError(form, data.data.info_details, 'info_details');
                }
                return false;
            }
            location.href = BASE + 'manufacturers';
        });
    });

     // ADD MULTIPLE TELS
     $('body').on('click', '.btn-tel-add', function(e) {
         var index = $('body').find('.tel-box-wrap .form-group').length + 1;
         var content = '<div class="form-group">' +
                 '<label class="col-sm-2 control-label"></label>' +
                 '<div class="col-md-4 col-lg-4 col-sm-4">' +
                     '<input type="tel" placeholder="' + TXT_ENTER_TEL + '" class="form-control" name="info_details[' + index + '][tel]"/>' +
                     '<input type="hidden" name="info_details[' + index + '][type]" value="' + MANUFACTURER_TYPE + '"/>' +
                 '</div>' +
                 '<div class="col-md-4 col-lg-4 col-sm-4">' +
                     '<input type="text" placeholder="' + STR_TEL_EXT + '" class="form-control" name="info_details[' + index + '][tel_extension]"/>' +
                 '</div>' +
                 '<div class="col-sm-1">' +
                     '<button type="button" class=" btn btn-delete btn-sm form-btn-add btn-remove-field">' +
                         '<i class="fa fa-trash" aria-hidden="true"></i>' +
                     '</button>' +
                 '</div>' +
             '</div>';
         $('body').find('.tel-box-wrap').append(content);
         initialTelInput(null, true);
     });


    // ADD MULTIPLE EMAIL
    $('body').on('click', '.btn-email-add', function(e) {
        var index = $('body').find('.email-box-wrap .form-group').length + 1;
        var content = '<div class="form-group">' +
                '<label class="col-sm-2 control-label"></label>' +
                '<div class="c-lg col-lg-3 col-md-3 col-sm-3">' +
                    '<input type="email" name="info_details[' + index + '][email]" placeholder="' + TXT_ENTER_EMAIL + '" class="form-control"/>' +
                    '<input type="hidden" name="info_details[' + index + '][type]" value="' + MANUFACTURER_TYPE + '"/>' +
                '</div>' +
                '<div class="col-lg-1">' +
                    '<button type="button" class=" btn btn-delete btn-sm form-btn-add btn-remove-field"><i class="fa fa-trash" aria-hidden="true"></i></button>' +
                '</div>' +
            '</div>';
        $('body').find('.email-box-wrap').append(content);
//        createValidation();
    });

    // REMOVE TEL, EMAIL
    $('body').on('click', '.btn-remove-field, .btn-rm-position', function(e) {
        $(this).closest('.form-group').remove();
    });
});
