$(function(e) {
    //REGISTER SALE RECORD
    $('body').on('click', '.btn-register-po', function() {
        let options = {
            type: 'GET',
            url: BASE + 'Sales/getData',
            data: { kind_of: '<?= TYPE_MP ?>'},
            beforeSend: function() {
                $.LoadingOverlay('show');
            }
        };
        ajaxRequest(options, SESSION_TIMEOUT, function(data) {
            $('body').prepend(data);
            let content = $('body').find('#sale-modal');
            $(content).modal('show');
            _registerOrEditFrame(content);
        });
    });

    function _registerOrEditFrame(content)
    {
        $(content).find('#currency-id').val(1);
        $(content).find('#seller-id, #currency-id').select2({ width: '100%' });

        $(content).find('#datepicker, .order-date').datetimepicker({
            format: 'YYYY-MM-DD',
            minDate: '2007-01-01'
        });

        $(content).find('.customer-id').select2({
            width: '100%',
            minLength: 0,
            ajax: {
                url: BASE + 'Customers/getCustomerBySearch',
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    let query = { keyword: params.term };
                    return query;
                },
                processResults: function (data) {
                    let dataSource = [];
                    if (data.data === null && data.data === 'undefined') {
                        return false;
                    }
                    $.each(data.data, function(i, v) {
                        dataSource.push({
                            id: v.id,
                            text: v.full_name
                        });
                    });
                    return { results: dataSource };
                }
            }
        });

        $(content).find('.btn-register').click(function(e) {
            let form = $('form[name="purchase_order"]');
            let options = {
                type: 'POST',
                url: BASE + 'Sales/create',
                data: $(form).serialize(),
                beforeSend: function() {
                    $.LoadingOverlay('show');
                    $(form).find('.error-message').remove();
                }
            };
            ajaxRequest(options, SESSION_TIMEOUT, function(data) {
                if (data.message === MSG_ERROR) {
                    displayError(form, data.data);
                    return false;
                }
                location.href = BASE + 'Sales/rop/' + data.data;
            });
        });
    }
});