$(document).ready(function() {
    $('body').find('select').select2({ width: '100%' });

    /*
     * MODAL CLOSED AND REMOVE ELEMENT FORM HTML
     */
    $('body').on('click', '.modal-close', function (e) {
        $(this).closest('.modal').on('hidden.bs.modal', function (e) {
            $(this).closest('.modal').closest('.modal-wrapper').remove();
        });
    });

    /*
     * GET RESET PASSWORD FORM
     */
    $('body').on('click', '.change-password', function (e) {
        let options = {
            type: 'GET',
            url: BASE + 'users/reset-password-form',
            beforeSend: function () {
                $.LoadingOverlay('show');
            }
        };
        ajaxRequest(options, SESSION_TIMEOUT, function (data) {
            $('body').prepend(data);
            $('body').find('#reset-password').modal('show');
        });
    });

    /**
     * UPDATE DIAL CODE WHEN CHANGE COUNTRY FOR PHONE NUMBER
     */
    $('body').on('countrychange', 'input[type="tel"]', function(e, data) {
        $(this).val($(this).intlTelInput('getNumber'));
    });
});

/**
 * GO TO PREVIOUS HISTORY
 */
function goBack() {
    window.history.back(1);
}

/**
 * AJAX REQUESTED WITH CALLBACK FUNCTION
 */
function ajaxRequest(params, message, callback)
{
    $.ajax(params)
        .done(function (data) {
            if ((data === null) && (data === 'undefined')) {
                return false;
            }
            if (typeof callback === 'function') {
                callback(data);
            }
        })
        .fail(function (jqXHR, textStatus, errorThrown) {
            if (errorThrown === 'Forbidden') {
                if (confirm(message)) {
                    location.reload();
                }
            }
        })
        .always(function (data) {
            $.LoadingOverlay('hide');
        });
}

/**
 * SHOW MESSAGE ERROR
 */
function showMessageError(form, data)
{
    if (data === null && data === 'undefined') {
        return false;
    }
    $.each(data, function (i, v) {
        var text = '';

        // check rule validation
        if (VALIDATE_IS_EMPTY in v) {
            text = v._empty;
        } else if (VALIDATE_IS_VALID in v) {
            text = v.valid;
        } else if (VALIDATE_IS_URL in v) {
            text = v.urlWithProtocol;
        } else if (VALIDATE_IS_REQUIRED in v) {
            text = v._required;
        }

        var message = '<label class="error-message">' + text + '</label>';
        var content = $(form).find('[name="' + i + '"]');
        $(content).closest('div').append(message);
    });
}


/**
 * SHOW MESSAGE ERROR
 */
function showAssociateMessageError(form, data, associateName)
{
    if (data === null && data === 'undefined') {
        return false;
    }
    $.each(data, function (i, v) {
        var text = '';
        var field = '';

        if ('email' in v) {
            field = 'email';
            if (VALIDATE_IS_EMPTY in v.email) {
                text = v.email._empty;
            } else if (VALIDATE_IS_VALID in v.email) {
                text = v.email.valid;
            }
        }

        var name = associateName + '[' + i + '][' + field + ']';
        var message = '<label class="error-message">' + text + '</label>';
        var content = $(form).find('[name="' + name + '"]');
        $(content).closest('div').append(message);
    });
}

// CLEARN UP CODE

function displayError(form, data)
{
    if (data !== null && data !== 'undefined') {
        $.each(data, function (i, v) {
            var message = '<label class="error-message">' + v + '</label>';
            var content = $(form).find('[name="' + i + '"]');
            $(content).closest('div').append(message);
        });
    }
}

function ajax_request_get(url, params, callback, type, msg)
{
    $.get(url, params, function(data) {
        if (data !== null && data !== undefined) {
            callback(data);
        }
    }, type)
    .fail(function(jqXHR, textStatus, errorThrown) {
        if (errorThrown === 'Forbidden') {
            if (confirm(msg)) {
                location.reload();
            }
        }
    })
    .always(function() {
        $.LoadingOverlay('hide');
    });
}

function ajax_request_post(url, params, callback, msg)
{
    $.post(url, params, function(data) {
        if (data !== null && data !== undefined) {
            callback(data);
        }
    }, 'json')
    .fail(function(jqXHR, textStatus, errorThrown) {
        if (errorThrown === 'Forbidden') {
            if (confirm(msg)) {
                location.reload();
            }
        }
    })
    .always(function() {
        $.LoadingOverlay('hide');
    });
}

function ajax_post_type_file(url, data, callback, msg)
{
    $.ajax({
        type: 'post',
        url: url,
        contentType: false,
        processData: false,
        data: data,
    }).done(function(data) {
        if (data !== null && data !== undefined) {
            callback(data);
        }
    }).fail(function(jqXHR, textStatus, errorThrown) {
        if (errorThrown === 'Forbidden') {
            if (confirm(msg)) {
                location.reload();
            }
        }
    }).always(function(data) {
        $.LoadingOverlay('hide');
    });
}

/**
 * INITIALIZE TEL INPUT WITH DIAL CODES OF COUNTRY
 */
function initialTelInput(content = null, allowDropdown = true)
{
    $('body').find('input[type="tel"]')
            .intlTelInput({
                preferredCountries: ['jp'],
                allowDropdown: allowDropdown
            });
}

/**
 * UPDATE ALL PHONE NUMBERS BEFORE AJAX FORM SUBMIT
 */
function updateDialCode(content, type = null)
{
    $.each (content.find('input[type="tel"]'), function (index, value) {
        $(this).val($(this).intlTelInput('getNumber'));
    });
}

/**
 * NEED TO CLEANUP
 * TO DO
 */

/**
 * Function currencyFormat
 * @param int num
 * @returns sring
 */
function currencyFormat(num)
{
    return num.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,');
}

// url helper
function getUrlParameter(sParam)
{
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
}

function updateURLParameter(url, param, paramVal)
{
    var newAdditionalURL = '';
    var tempArray = url.split('?');
    var baseURL = tempArray[0];
    var additionalURL = tempArray[1];
    var temp = '';
    if (typeof additionalURL != 'undefined') {
        tempArray = additionalURL.split('&');
        for (var i=0; i<tempArray.length; i++){
            if(tempArray[i].split('=')[0] != param){
                newAdditionalURL += temp + tempArray[i];
                temp = '&';
            }
        }
    }
    var rows_txt = temp + '' + param + '=' + paramVal;
    return baseURL + '?' + newAdditionalURL + rows_txt;
}

//replace double quote in array of autocomplete typehead.js
var substringMatcher = function (strs) {
    return function findMatches(q, cb) {
        var matches, substringRegex;
        matches = [];
        substrRegex = new RegExp(q, 'i');
        $.each(strs, function (i, str) {
            if (substrRegex.test(str)) {
                matches.push(str);
            }
        });
        cb(matches);
    };
};

// substring from url
function getQueryString(name)
{
    var regexS = "[\\?&]"+name+"=([^&#]*)",
    regex = new RegExp( regexS ),
    results = regex.exec( window.location.search );
    if( results == null ){
        return "";
    } else{
        return decodeURIComponent(results[1].replace(/\+/g, " "));
    }
}

// create validate when append email field
function create_validate()
{
    $('input[type="email"]').each(function() {
        $(this).rules('remove');
        $(this).rules('add', {
            email: true,
            required: false,
            messages: {
                required: '<?php echo MESSAGE_REQUIRED_EN; ?>'
            }
        });
    });
}

/**
 * Confirm YES/NO function
 */
function confirm_yesno(btnClass, msg, placement, title) {
    var options = {
        placement: placement,
        title: title,
        html: 'true',
        content: '<p>' + msg + '</p>' + '<div class="text-center"><button class="btn btn-default confirm-yes" type="button">Yes</button><button class="btn btn-default confirm-no" type="button">No</button></div>'
    };

    // Delete button popover confirmation
    $('.' + btnClass).popover(options).on('shown.bs.popover', function() {
        var $delete = $(this);
        var $form = $delete.closest('form');
        var $pop = $form.find('.popover');
        $pop.find('button').click(function() {
            if ($(this).is('.confirm-yes')) {
                $form.submit();
            }
            $delete.popover('destroy').popover(options);
        });
    });
}

function confirmYesNo(btnClass, msg, placement, title, btnYes, btnNo) {
    var options = {
        placement: placement,
        title: title,
        html: 'true',
        trigger: 'manual',
        content: '<p>' + msg + '</p>' + '<div class="text-center"><button class="btn btn-default confirm-no" type="button">' + btnNo + '</button><button class="btn btn-default confirm-yes" type="button">' + btnYes + '</button></div>'
    };

    // Delete button popover confirmation
    $('.' + btnClass).popover(options).on('shown.bs.popover', function() {
        var $delete = $(this);
        var $form = $delete.closest('form');
        var $pop = $form.find('.popover');
        $pop.find('button').click(function() {
            if ($(this).is('.confirm-yes')) {
                $form.submit();
            }
            $delete.popover('destroy').popover(options);
        });
    });
}

//dynamically positioning the modal bootstrap.
function dynamicModalPosition(objPos, changePos, modalId, clsFocus) {
    $('#' + modalId).on('shown.bs.modal', function() {
        $('.' + clsFocus).focus();
    }).css({
        top: objPos - changePos,
        outline: 'none'
    }).modal({
        backdrop: 'static',
        keyboard: false
    });
}


//Next or previous 5 pages
function updatePagination(maxPage) {
    var current = $('#current').val();

    var nextFive = parseInt(current) + 5;
    var prevFive = parseInt(current) - 5;
    if (nextFive > maxPage) {
        $('#nextMore').attr('href', '#');
        $('#nextMore').closest('li').addClass('disabled');
    }
    if (prevFive < 1) {
        $('#prevMore').attr('href', '#');
        $('#prevMore').closest('li').addClass('disabled');
    }
}

$(function() {
    var maxPage = parseInt($('#maxPage').val());
    $('#nextMore').click(function(e) {
        e.preventDefault();
        var arrayUrl = $(this).attr('href').split('page=');
        var arrayUrlTrail = arrayUrl.pop().split('&');
        var length = arrayUrlTrail.length;
        var trail = '';
        if (length > 0) {
            var p = parseInt(arrayUrlTrail[0]);
            if (p === 1) {
                var page = p + 4;
            } else {
                var page = p + 5;
            }
        }
        //skip first index = 0 for page
        for (var i = 1 ; i < length ; i++) {
            trail = trail + '&' + arrayUrlTrail[i];
        }

        if (page <= maxPage){
            var url = arrayUrl.shift() + 'page=' + page;
            if (trail !== '') {
                url = url + trail;
            }
            window.location.replace(url);
        }
    });

    $('#prevMore').click(function(e) {
        e.preventDefault();
        var arrayUrl = $(this).attr('href').split('page=');
        var arrayUrlTrail = arrayUrl.pop().split('&');
        var length = arrayUrlTrail.length;
        var trail = '';
        if (length > 0) {
            var page = parseInt(arrayUrlTrail[0]) - 5;
        }
        //skip first index = 0 for page
        for (var i = 1 ; i < length ; i++) {
            trail = trail + '&' + arrayUrlTrail[i];
        }
        if (page <= maxPage){
            var url = arrayUrl.shift() + 'page=' + page;
            if (trail !== '') {
                url = url + trail;
            }
            window.location.replace(url);
        }
    });

    // pagination display perpage
    $('body').on('change', '.display-data', function (e) {
        var that = this;
        //console.log(getUrlParameter('displays'));
        //var param = getUrlParameter('displays');
        //console.log(updateURLParameter(document.location.href, 'displays', $(that).val()));
        window.location.href = updateURLParameter(document.location.href, 'displays', $(that).val());
        //window.location.href = document.location.href+'displays=' + $(that).val();
    });
});


//messages list
function messageList(url, type) {
    $('body').on('click', '.btn_comment', function (e) {
        e.stopPropagation();
        var id = $(this).data('id');
        $.ajax({
            url: url,
            type: 'GET',
            dataType: 'html',
            data: { id : id, type : type },
            beforeSend: function() {
                $.LoadingOverlay('show');
            },
            success: function(response) {
                $.LoadingOverlay('hide');
                if (response) {
                    $('#message-receive').empty().html(response);
                }
            },
            complete: function() {
                var number_of_msg = 0;
                if ($('.personal-message').length) {
                    number_of_msg = $('.personal-message').val();
                }
                $('.personal-badge').empty().text(number_of_msg);
                $('#modal_comment').modal('show');
            },
            error: function(jqXHR, textStatus, errorThrown) {
                $.LoadingOverlay('hide');
                if (errorThrown === 'Forbidden') {
                    if (confirm('Session timeout. Please login again.')) {
                        location.reload();
                    }
                }
            }
        });
    });
}

//suspend the messages
function suspendMessage(id, suspendUrl, data) {
    $.ajax({
        url: suspendUrl,
        type: 'POST',
        dataType: 'JSON',
        data: data,
        cache: false,
        beforeSend: function() {
            $.LoadingOverlay('show');
        },
        success: function(response) {
            $.LoadingOverlay('hide');
            if (response.status === 'OK') {
                $('.row-id-' + id).remove();
            } else {
                alert(response.msg);
            }
        },
        error: function(jqXHR, textStatus, errorThrown) {
            $.LoadingOverlay('hide');
            if (errorThrown === 'Forbidden') {
                if (confirm('Session timeout. Please login again.')) {
                    location.reload();
                }
            }
        }
    });
}

/**
 * Add multiple email
 */
function addMultipleEmail(type, placeHolder) {
    var element = '<div class="form-group">'
        + '<label class="col-sm-2 control-label"></label>'
        + '<div class="col-lg-3 col-sm-6 col-md-3 c-lg">'
        + '<input type="email" class="form-control multiple-mail" placeholder="'+placeHolder+'" name="info_details[][email]" />'
        + '<input type="hidden" class="mail-type" value="'+type+'" name="info_details[][type]" />'
        + '</div>'
        + '<div class="col-sm-1">'
        + '<button type="button" class=" btn btn-delete btn-sm form-btn-add btn-remove-mail"><i class="fa fa-trash" aria-hidden="true"></i></button>'
        + '</div>'
        + '</div>';
    $('.email-box-wrap').append(element);
    updateEmailIndex();
}

/**
 * multiple email
 */
function updateEmailIndex() {
    $('.multiple-mail').each(function(i, v) {
        $(this).attr('name', 'info_details[' + i + '][email]');
    });

    $('.mail-type').each(function(i, v) {
        $(this).attr('name', 'info_details[' + i + '][type]');
    });
}

function submitFormSearch(inputId) {
    jQuery(document).on('keydown', 'input#' + inputId, function(ev) {
        if (ev.which === 13) {
            $('form[name="form_search"]').submit();
        }
    });
}

function productAndBrandName(data) {
    var name = '';
    if (data.product_brand) {
        if (data.product_brand.name) {
            name += data.product_brand.name + ' ';
        } else {
            name += data.product_brand.name_en + ' ';
        }
    }

    if (data.name) {
        name += data.name + ' ';
    } else {
        name += data.name_en + ' ';
    }
    return name;
}
/**
 * follow pattern:
 * * if have both single_unit_size, pack_size, n description:
     * P1. Brand Product (1ml x 1Syringe) (description)
     * Other patterns r:
     * P2. Brand Product (1Syringe) (description)
     * P3. Brand Product (description)
     * b. No product_detail record is also ok
     * Then, display is
     * P4. Brand Product
 * @returns {undefined}
 */

function productDetails(data, en) {
    var details = '';
    if (data.single_unit && data.tb_pack_sizes) {
        details += '(';
        details += data.single_unit_size;
        details += (en == '_en' ? data.single_unit.name_en : data.single_unit.name);
        details += ' x ';
        details += data.pack_size;
        details += (en == '_en' ? data.tb_pack_sizes.name_en : data.tb_pack_sizes.name);
        details += ')';
    } else if (!data.single_unit && data.tb_pack_sizes) {
        details += '(';
        details += data.pack_size;
        details += (en == '_en' ? data.tb_pack_sizes.name_en : data.tb_pack_sizes.name);
        details += ')';
    } else if (data.single_unit && !data.tb_pack_sizes) {
        details += '(';
        details += data.single_unit_size;
        details += (en == '_en' ? data.single_unit.name_en : data.single_unit.name);
        details += ')';
    }

    if (data.description) {
        details += ' (' +data.description+ ')';
    }
    return details;
}

function nameEnOrJp(en, v) {
    var name = '';
    if (en == '_en') {
        name = v.name_en;
        if (name == '') {
            name = v.name;
        }
    } else {
        name = v.name;
        if (name == '') {
            name = v.name_en;
        }
    }
    return name;
}

function dateFormat(date)
{
    var d = new Date(date);
    return d.getFullYear() + '年' + ('0' + (d.getMonth() + 1)).slice(-2) + '月' + ('0' + d.getDate()).slice(-2) + '日';
}

/**
 * update country code
 */
function updateContryCode(action, frmType) {
    if (action === 'edit') {
        $('.multiple-tel').each(function(i, v) {
            phone_auto_complete('#tel-' + i, frmType);
        });

        $('.multiple-phone').each(function(i, v) {
            phone_auto_complete('#phone-' + i, frmType);
        });
    } else {
        phone_auto_complete('#tel-0', frmType);
        phone_auto_complete('#phone-0', frmType);
    }
}

/**
 * get name by local:
 * * if name_en is null or empty, we get value from name
    * else get value from name_en
 * @param {object} data
 * @param {string} local
 * @returns {string} field name like name or name_en
 */
function getFieldNameByLocal(data, local)
{
    var field = 'name' + local;
    var name = data[field];

    if (name === '')
    {
        switch (local) {
            case '_en' :
                name = data.name;
                break;
            default :
                name = data.name_en;
        }
    }

    return name;
}

function pageOutOfRange(pCount, page) {
    if (page !== '') {
        if (parseInt(pCount) === 0) {
            window.history.back();
            return false;
        }
    }
}

/**
 * convert string to capitalize the first char of word
 * @param {string} symbol separate string with symbol
 * @param {string} string string for convert
 * @returns {string}
 **/
function textHumanize(symbol, string)
{
    var str = string.replace(symbol, ' ');
    return str.replace(/\w\S*/g, function(txt){
        return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
    });
}

/**
 * format normal datetime in vue
 * @param {string} datetime for format
 * @returns {String}
 */
function getFormattedDate(date) {
    var date = new Date(date);
    var year = date.getFullYear();

    var month = (1 + date.getMonth()).toString();
    month = month.length > 1 ? month : '0' + month;

    var day = date.getDate().toString();
    day = day.length > 1 ? day : '0' + day;

    return year + '-' + month + '-' + day;
}

/**
 * concate first_name and last_name with parthern base on local
 * @param {string} local
 * @param {object} data such as firstname, firstname_en, lastname, lastname_en
 * @returns {string}
 */
function concatFirstAndLastName(local, data)
{
    var full_name = null;
    switch (local) {
        case '_en' :
            full_name = data.firstname_en + ' ' + data.lastname_en;
            break;
        default :
            full_name = data.lastname + ' ' + data.firstname;
            break;
    }

    return full_name;
}

/**
 * get name or name_en by local
 * @param {string} local
 * @param {object} data
 * @returns {string}
 */
function getNameByLocal(locale, data)
{
    var name = '';
    if (data !== null && data !== 'undefined') {
        if ((data.name_en !== '') && (locale === LOCALE_EN)) {
            name = data.name_en;
        } else {
            name = data.name;
        }
        if (name === '') {
            name = data.name_en;
        }
    }

    return name;
}

/**
 * get currency code base on local
 * @param {string} local
 * @param {object} data
 * @returns {string}
 */
function getCurrencyByLocal(local, data)
{
    var currency = '';
    switch (local) {
        case '_en' :
            currency = data.code_en;
            break;
        default :
            currency = data.code;
            break;
    }

    return currency;
}

/**
 * Using for format address base on country
 * @param {string} type of address or country such as:
 * - Japan Address
 * - Singapore Address
 * - English Address or other (Check on boostrap.php)
 * @param {object} data retrieved form database
 * @returns {string}
 */
function addressFormat(addressType, data) {
    var address = '';
    switch (addressType) {
        case 'english_address':
            break;
        case 'singapore_address':
            break;
        default:
            address = data.postal_code + ' ' + data.prefecture + ' ' + data.city;
    }
    return address;
}
