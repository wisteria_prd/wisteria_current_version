$(function (e) {
    // $('body').on('change', 'input:file', function (e) {
    //     let content = $(this).closest('.file-wrap');
    //     $(content).find('.img-preview')
    //             .show();
    //     readURL(this, content);
    // });

    $('body').find('select').select2('destroy');
    $('select#status').select2();
     $('select#message_category_id').select2();
    $('select#message-receivers').select2({
        placeholder: COMMENT_TXT_SELECT_USER,
        allowClear: true
    });

    // toggle recievers and manage by is_sent_to_all
    $('.is_sent_to_all').on('click', function() {
        var that = this;
        if ($(that).is(':checked')) {
            $('.message-receivers-id').hide();
        } else {
            $('.message-receivers-id').show();
        }
    });

    // if is_sent_to_all is selcted (when request error or edit form)
    if ($('.is_sent_to_all').is(':checked')) {
        $('.message-receivers-id').hide();
    } else {
        $('.message-receivers-id').show();
    }

    // display selected values for receivers
    if (selected_receivers) {
        $('select#message-receivers').select2({
            placeholder: COMMENT_TXT_SELECT_USER,
            allowClear: true
        }).val(selected_receivers).trigger('change');
    }

    $('body').on('click', '.btn-add-comment', function (e) {
        let form = $(this).closest('form');
        let formData = new FormData();
        formData.append('file', $(form).find('input:file')[0].files[0]);
        formData.append('message_id', $(form).find('input[name="message_id"]').val());
        formData.append('description', $(form).find('textarea').val());
        formData.append('parent_id', $(form).find('input[name="parent_id"]').val());
        let options = {
            type: 'POST',
            url: BASE + 'replies/create/',
            data: formData,
            processData: false,
            contentType: false,
            beforeSend: function () {
                $.LoadingOverlay('show');
                $(form).find('.error-message').remove();
                $(form).find('.form-group').removeClass('has-error');
            }
        };
        ajaxRequest(options, SESSION_TIMEOUT, function (data) {
            if (data.message === MSG_ERROR) {
                let content = $(form).find('textarea');
                $(content).closest('.form-group')
                        .addClass('has-error');
                $(content).closest('div').append('<label class="error-message">' + data.data.description + '</label>');
                return;
            }
            location.reload();
        });
    });

    $('body').on('click', '.btn-reply', function (e) {
        let content = $(this).closest('.comments-item');
        let params = {
            parent_id: $(this).attr('data-id'),
            message_id: $(this).attr('data-msg-id')
        };
        let options = {
            type: 'GET',
            url: BASE + 'replies/getReplyForm/',
            data: params,
            beforeSend: function () {
                $.LoadingOverlay('show');
            }
        };
        ajaxRequest(options, SESSION_TIMEOUT, function (data) {
            $(content).append(data);
        });
    });

    $('body').on('click', '.btn-delete-comment', function (e) {
        let options = {
            type: 'POST',
            url: BASE + 'replies/delete/',
            data: { id: $(this).attr('data-id') },
            beforeSend: function () {
                $.LoadingOverlay('show');
            }
        };
        ajaxRequest(options, SESSION_TIMEOUT, function (data) {
            if (data.message === MSG_SUCCESS) {
                location.reload();
            }
        });
    });

    // $('body').on('click', '.btn-add-category', function (e) {
    //     let options = {
    //         type: 'GET',
    //         url: BASE + 'MessageCategories/getCreateForm',
    //         beforeSend: function () {
    //             $.LoadingOverlay('show');
    //         }
    //     };
    //     ajaxRequest(options, SESSION_TIMEOUT, function (data) {
    //         $('body').prepend(data);
    //         let content = $('body').find('.msg-category');
    //         $(content).modal('show');
    //         _messageCategoryFrame(content);
    //     });
    // });

    function readURL(input, content)
    {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
                $(content).find('#imgPreview').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
            $('.comment-no-file-selected').hide();
        }
    }

    function _messageCategoryFrame(content)
    {
        $(content).find('.btn-msg-category').click(function (e) {
            let form = $(content).find('form');
            let options = {
                type: 'POST',
                url: BASE + 'MessageCategories/create',
                data: $(form).serialize(),
                beforeSend: function () {
                    $.LoadingOverlay('show');
                    $(form).find('.error-message').remove();
                }
            };
            ajaxRequest(options, SESSION_TIMEOUT, function (data) {
                if (data.message === MSG_ERROR) {
                    displayError(form, data.data);
                    return false;
                }
                location.reload();
            });
        });
    }
});
