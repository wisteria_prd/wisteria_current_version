$(function (e) {
    $('select').select2();

    $('.message-tab, .receiver-tab').click(function (e) {
        // ADD ACTIVE TAB
        $('.receiver-tab, .message-tab')
                .removeClass('btn-primary')
                .addClass('btn-default');
        $(this).addClass('btn-primary');
        let params = {
            tab: $(this).attr('data-tab'),
            id: $('meta[name="id"]').attr('content'),
            type: $('meta[name="type"]').attr('content')
        };
        let options = {
            type: 'GET',
            url: BASE + 'messages/getTabs',
            data: params,
            beforeSend: function () {
                $.LoadingOverlay('show');
            }
        };
        ajaxRequest(options, SESSION_TIMEOUT, function (data) {
            let tbody = $('.messages').find('tbody').empty();
            $(tbody).html(data);
        });
    });

    $('.btn-search').click(function (e) {
        let params = {
            tab: $('.tab-content').find('.btn-primary').attr('data-tab'),
            id: $('meta[name="id"]').attr('content'),
            type: $('meta[name="type"]').attr('content'),
            keyword: $('input[name="keyword"]').val(),
            status: $('select[name="status"]').val(),
            referer: CONTROLLER
        };
        let options = {
            type: 'GET',
            url: BASE + 'messages/filterMessage',
            data: params,
            beforeSend: function () {
                $.LoadingOverlay('show');
            }
        };
        ajaxRequest(options, SESSION_TIMEOUT, function (data) {
            let tbody = $('.messages').find('tbody').empty();
            $(tbody).html(data);
        });
    });

    $('body').on('click', '.btn-suspend-msg', function (e) {
        let options = {
            type: 'GET',
            url: BASE + 'Messages/getStatus',
            data: { id: $(this).closest('tr').attr('data-id') },
            beforeSend: function () {
                $.LoadingOverlay('show');
            }
        };
        ajaxRequest(options, SESSION_TIMEOUT, function (data) {
            $('body').prepend(data);
            let content = $('body').find('.modal-status');
            $(content).modal('show');
            _iframe(content);
        });
    });

    function _iframe(content)
    {
        $(content).find('.change-status').click(function (e) {
            let options = {
                type: 'POST',
                url: BASE + 'Messages/changeOfStatus',
                data: {
                    id: $(content).find('meta[name="id"]').attr('content'),
                    status: 0
                },
                beforeSend: function () {
                    $.LoadingOverlay('show');
                }
            };
            ajaxRequest(options, SESSION_TIMEOUT, function (data) {
                if (data.message === MSG_SUCCESS) {
                    location.reload();
                }
            });
        });
    }
});
