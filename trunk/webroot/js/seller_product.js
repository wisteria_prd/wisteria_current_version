$(function(e) {
    initialTreeView(true);

    $('#seller-id, #brand-id').select2();

    $('body').on('change', 'select#seller-id', function(e) {
        $('body').find('#jstree').empty();
        var id = $(this).val();
        var params = {
            type: 'get',
            url: BASE + 'SellerProducts/getListBrand',
            data: { id: id },
            beforeSend: function () {
                $.LoadingOverlay('show');
            }
        };
        ajaxRequest(params, SESSION_TIMEOUT, function(data) {
            var options = '<option value="">' + SELECT_PRODUCT_BRAND + '</option>';
            $.each(data.data, function(index, value) {
                options += '<option value="' + index + '">' + switchNameBylocale(value) + '</option>';
            });
            $('select#brand-id').html(options);
        });
    });

    $('body').on('change', 'select#brand-id', function(e) {
        var id = $(this).val();
        var params = {
            type: 'get',
            url: BASE + 'SellerProducts/getProductByBrandId',
            data: { id: id },
            beforeSend: function () {
                $.LoadingOverlay('show');
            }
        };
        ajaxRequest(params, SESSION_TIMEOUT, function(data) {
            getProductList(data.data);
        });
    });

    $('body').on('click', '.btn-register', function(e) {
        var form = $('form#seller-product');
        var tree_obj = $('#jstree').jstree('get_selected', true);
        var ids = [];
        if (tree_obj.length == 0) {
            return false;
        }
        $.each(tree_obj, function(index, value) {
            ids.push(value.data.id);
        });
        var params = {
            type: 'POST',
            url: BASE + 'SellerProducts/saveOrUpdate',
            data: {
                ids: ids,
                seller_id: $('#seller-id').val(),
                brand_id: $('#brand-id').val()
            },
            beforeSend: function() {
                $.LoadingOverlay('show');
            }
        };
        ajaxRequest(params, SESSION_TIMEOUT, function(data) {
            if (data.message === MSG_SUCCESS) {
                // location.href = BASE + 'SellerProducts';
            }
        });
    });

    function switchNameBylocale(data)
    {
        var name = '';
        if (LOCALE === '_en') {
            name = data.name_en;
        }
        if (LOCALE === '') {
            name = data.name;
        }
        if (name === '') {
            name = data.name;
        }
        return name;
    }

    function getProductList(data = null)
    {
        if ((data === null) && (data === 'undefined')) {
            return false;
        }
        var element = '<ul>';
        var content = $('body').find('#jstree');
        $.jstree.destroy ();
        $.each(data, function(index, value) {
            element += getProductDetail(value.product_details);
        });
        element += '</ul>';
        $(content).append(element);
        initialTreeView();
    }

    function getProductDetail(data = null)
    {
        if ((data === '') && (data === 'undefined')) {
            return false;
        }
        var element = '';
        $.each(data, function(index, value) {
            console.log(value);
            element += '<li data-id="' + value.id + '">' + value.single_unit_size + ' x ' + value.single_unit + '</li>';
        });
        return element;
    }

    function initialTreeView(checked = false)
    {
        $('#jstree').jstree({
            'core': {
                'themes': {'icons': false}
            },
            'plugins': ['checkbox']
        }).jstree('open_all');
        if (checked == false) {
            return false;
        }
        var li = $('body').find('.jstree-container-ul li');
        console.log(li);
        $.each(li, function(index, value) {
            $(this).find('.jstree-anchor').addClass('jstree-clicked');
        });
    }
});
