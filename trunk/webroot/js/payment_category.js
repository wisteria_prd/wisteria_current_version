$(function(e) {
    var datas = [];

    $('.btn-save').click(function(e) {
        let form = $('form[name="payment_form"]');
        let options = {
            type: 'POST',
            url: BASE + 'PaymentCategories/saveOrUpdate/',
            data: $(form).serialize(),
            beforeSend: function() {
                $.LoadingOverlay('show');
                $(form).find('.error-message').remove();
            }
        };
        ajaxRequest(options, SESSION_TIMEOUT, function (data) {
            if (data.message === MSG_ERROR) {
                displayError(form, data.data);
            } else {
                displayList(form, data.data.payments);
            }
        });
    });

    $('body').on('click', '.btn-edit-payment', function(e) {
        let tr = $(this).parents('tr');
        let obj = {
            'name': $(tr).find('td:eq(1)').text(),
            'name_en': $(tr).find('td:eq(2)').text(),
            'id': $(tr).attr('data-id')
        };
        $('input[name="name"]').val(obj.name);
        $('input[name="name_en"]').val(obj.name_en);
        $('input[name="id"]').val(obj.id);
        $('.btn-save').text(TXT_UPDATE);
    });

    $('body').on('click', '.btn-delete-payment', function(e) {
        let tr = $(this).parents('tr');
        let id = $(tr).attr('data-id');
        $('#modal_delete').modal('show');
        $('#modal_delete').find('input[name="id"]').val(id);
    });

    $('#btn_delete_yes').click(function(e) {
        var id = $('#modal_delete').find('.delete-item').val();
        let form = $('form[name="payment_form"]');
        var options = {
            type: 'POST',
            url: BASE + 'PaymentCategories/delete/',
            data: { id: id },
            beforeSend: function() {
                $.LoadingOverlay('show');
            }
        };
        ajaxRequest(options, SESSION_TIMEOUT, function (data) {
            if (data.message === MSG_SUCCESS) {
                $('#modal_delete').modal('hide');
                displayList(form, data.data.payments);
            }
        });
    });

    function displayList(form, data)
    {
        if ((data === null) && (data !== 'undefined')) {
            return false;
        }
        datas = data;
        //RESET FORM
        $(form).find('input[name="id"]').remove();
        $(form).find('input[name="name_en"], input[name="name"]').val('');
        $(form).find('.btn-save').text(TXT_ADD);

        var table = $('.table > tbody');
        var element = '';
        $.each(data, function(i, v) {
            element += '<tr data-id="' + v.id + '">' +
                        '<td>' + (i+1) + '</td>' +
                        '<td>' + v.name + '</td>' +
                        '<td>' + v.name_en + '</td>' +
                        '<td>' +
                            '<button type="button" class="btn btn-primary btn-sm btn-edit-payment">' +
                                '<span><i class="fa fa-pencil-square-o" aria-hidden="true"></i></span>' +
                            '</button>&nbsp;&nbsp;' +
                            '<button type="button" class="btn btn-delete btn-sm btn-delete-payment">' +
                                '<span><i class="fa fa-trash" aria-hidden="true"></i></span>' +
                            '</button>' +
                        '</td>' +
                    '</tr>';
        });
        $(table).empty()
                .html(element);
    }

    $('.btn-close').click(function(e) {
        if (datas.length > 0) {
            let options = '';
            let selected = $('body').find('select[name="payment_category_id"]');
            $(selected).find('option:not(:first)').remove();
            $.each(datas, function(i, v) {
                options += '<option value="' + v.id + '">' + v.name + ' ' + v.name_en + '</option>';
            });
            $(selected).append(options);
        }
    });
});