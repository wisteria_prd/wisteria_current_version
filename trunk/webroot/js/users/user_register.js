$(function(e) {
    $('select').select2({ width: '100%' });

    $('.btn-save').click(function(e) {
        var form = $('#register-modal').find('form');
        var options = {
            type: 'POST',
            url: BASE + 'Users/register',
            data: $(form).serialize(),
            beforeSend: function() {
                $.LoadingOverlay('show');
                $(form).find('.error-message').remove();
            }
        };
        ajaxRequest(options, SESSION_TIMEOUT, function(data) {
            if (data.message === MSG_ERROR) {
                displayError(form, data.data);
            } else {
                location.reload();
            }
        });
    });

    $('#affiliation-class').change(function(e) {
        var affiliation_class = $(this).val();
        if (affiliation_class === '') {
            return false;
        }
        var options = {
            type: 'GET',
            url: BASE + 'Users/getUserRole',
            data: { 'affiliation_class': affiliation_class },
            beforeSend: function() {
                $.LoadingOverlay('show');
                $('#role').find('option:not(:first)').remove();
            }
        };
        ajaxRequest(options, SESSION_TIMEOUT, function(data) {
            $('body').find('.select-user-role').html(data);
            $('body').find('.select-user-role > select').select2();
        });
    });
});