$(function () {
    $('.create-user').click(function (e) {
        let params = {
            target: $(this).attr('data-target'),
            id: null
        };
        if (params.target === TARGET_EDIT) {
            params.id = $(this).closest('tr').attr('data-id');
        }
        let options = {
            url: BASE + 'Users/getRegisterForm',
            type: 'GET',
            data: params,
            beforeSend: function () {
                $.LoadingOverlay('show');
            }
        };
        ajaxRequest(options, SESSION_TIMEOUT, function (data) {
            $('body').prepend(data);
            $('body').find('#register-modal').modal('show');
        });
    });

    $('.btn-restore').click(function (e) {
        let params = {
            id: $(this).closest('tr').attr('data-id'),
            status: $(this).attr('data-status')
        };
        let options = {
            url: BASE + 'Users/getStatus',
            type: 'GET',
            data: params,
            catche: false,
            beforeSend: function () {
                $.LoadingOverlay('show');
            }
        };
        ajaxRequest(options, SESSION_TIMEOUT, function (data) {
            $('body').prepend(data);
            let content = $('body').find('.modal-status');
            $(content).modal('show');
            _buttonActionFrame(content);
        });
    });

    $('.delete-user').click(function (e) {
        let params = {
            id: $(this).closest('tr').attr('data-id'),
            status: $(this).attr('data-status')
        };
        let options = {
            url: BASE + 'Users/getDelete',
            type: 'GET',
            data: params,
            catche: false,
            beforeSend: function () {
                $.LoadingOverlay('show');
            }
        };
        ajaxRequest(options, SESSION_TIMEOUT, function (data) {
            $('body').prepend(data);
            let content = $('body').find('.modal-delete');
            $(content).modal('show');
            _buttonActionFrame(content);
        });
    });

    $('.data-tb-list tr a, .data-tb-list tr button').click(function (e) {
        e.stopPropagation();
    });

    $('.data-tb-list .table>tbody>tr').click(function (e) {
        e.preventDefault();
        var params = {
            id: $(this).closest('tr').attr('data-id')
        };
        var options = {
            url: BASE + 'Users/view',
            type: 'GET',
            data: params,
            beforeSend: function () {
                $.LoadingOverlay('show');
            }
        };
        ajaxRequest(options, SESSION_TIMEOUT, function (data) {
            $('body').prepend(data);
            $('body').find('.modal-view').modal('show');
        });
    });

    $('#keyword-search').autocomplete({
        minLength: 0,
        delay: 500,
        source: function (request, response) {
            $.ajax({
                type: 'GET',
                url: BASE + 'users/getAutocomplete',
                dataType: 'json',
                data: { search: request.term },
                success: function (data) {
                    response($.map(data.data.users, function (el) {
                        return {
                            value: el.firstname
                        };
                    }));
                }
            });
        }
    });

    $('select[name="role"]').change(function (e) {
        $('form[name="form_search"]').submit();
    });

    var maxPage = parseInt($('#maxPage').val());
    updatePagination(maxPage);

    /**
     * USER RESET PASSWORD
     */
    var modalReset = $('body').find('#reset-password');
    $(modalReset).find('.btn-reset-pw').click(function (e) {
        let form = $(modalReset).find('form');
        let options = {
            url: BASE + 'users/change-password',
            type: 'POST',
            data: $(form).serialize(),
            beforeSend: function () {
                $.LoadingOverlay('show');
                $(form).find('.error-message, .error-message-wrap').remove();
            }
        };
        ajaxRequest(options, SESSION_TIMEOUT, function (data) {
            if (data.message === MSG_ERROR) {
                $.each (data.data, function (i, v) {
                    let message = '<ul class="error-message-wrap">';
                    let content = $(form).find('[name="' + i + '"]');
                    $.each (v, function (i1, v1) {
                        message += '<li><label class="error-message">' + v1 + '</label></li>';
                    });
                    message += '</ul>';
                    $(content).closest('.form-group').append(message);
                });
            } else {
                location.href = BASE + 'users/logout';
            }
        });
    });

    /*
     * INITIAL ALL ACTIONS IN MODAL SUSPEND, DELETE, RECOVER
     */
    function _buttonActionFrame(content)
    {
        /*
        * CHANGE OF STATUS MODAL
        */
       $(content).find('.change-status').click(function (e) {
           var params = {
               id: $(content).find('[name="id"]').attr('content'),
               status: $(content).find('[name="status"]').attr('content')
           };
           var options = {
               url: BASE + 'users/change-of-status',
               type: 'POST',
               data: params,
               beforeSend: function () {
                   $.LoadingOverlay('show');
               }
           };
           ajaxRequest(options, SESSION_TIMEOUT, function (data) {
               if (data.message === MSG_SUCCESS) {
                   location.reload();
               }
           });
       });

        /**
        * DELETE MODAL
        */
        $(content).find('.delete-user').click(function (e) {
            var params = {
                id: $(content).find('[name="id"]').attr('content')
            };
            var options = {
                url: BASE + 'users/delete',
                type: 'POST',
                data: params,
                beforeSend: function () {
                    $.LoadingOverlay('show');
                }
            };
            ajaxRequest(options, SESSION_TIMEOUT, function (data) {
                if (data.message === MSG_SUCCESS) {
                    location.reload();
                }
            });
        });
    }
});
