$(function(e) {
    var params = {
        tc_agreement: $('meta[name="tc_agreement"').attr('content'),
        pp_agreement: $('meta[name="pp_agreement"').attr('content'),
        activated: $('meta[name="activated"').attr('content')
    };
    activateAcount(params);

    $('.termNext').click(function(e) {
        var form = $('#form-user-tc-agreement');
        var options = {
            type: 'POST',
            url: BASE + 'users/tcAgreement',
            data: $(form).serialize(),
            beforeSend: function() {
                $.LoadingOverlay('show');
                $(form).find('.error-message').remove();
            }
        };
        ajaxRequest(options, SESSION_TIMEOUT, function(data) {
            if (data.message === MSG_ERROR) {
                displayError(form, data.data);
            } else {
                $('#cancelTerm').trigger('click');
                $('#modalPrivacy').modal({
                    backdrop: 'static',
                    keyboard: false
                });
            }
        });
    });

    $('#policyNext, .btn-next-step').click(function(e) {
        var form = $('#form-user-pp-agreement');
        var options = {
            type: 'POST',
            url: BASE + 'users/ppAgreement',
            data: $(form).serialize(),
            beforeSend: function() {
                $.LoadingOverlay('show');
                $(form).find('.error-message').remove();
            }
        };
        ajaxRequest(options, SESSION_TIMEOUT, function(data) {
            if (data.message === MSG_ERROR) {
                displayError(form, data.data);
            } else {
                $('#cancelTerm').trigger('click');
                $('#cancelPolicy').trigger('click');
                $('#modalConfirmation').modal({
                    backdrop: 'static',
                    keyboard: false
                });
            }
        });
    });

    $('#backPolicy, .btn-step-back').click(function(e) {
        $('#cancelPolicy').trigger('click');
        $('#modalTerm').modal({
            backdrop: 'static',
            keyboard: false
        });
        $('input:checkbox').prop('checked', true);
    });

    $('.backAccount').click(function(e) {
        $('#cancelAccount').trigger('click');
        $('#modalPrivacy').modal({
            backdrop: 'static',
            keyboard: false
        });
        $('input:checkbox').prop('checked', true);
    });

    $('#accountNext').click(function(e) {
        var form = $('#form-user-final-agreement');
        var options = {
            type: 'POST',
            url: BASE + 'users/register',
            data: $(form).serialize(),
            beforeSend: function() {
                $.LoadingOverlay('show');
                $(form).find('.error-message').remove();
            }
        };
        ajaxRequest(options, SESSION_TIMEOUT, function(data) {
            if (data.message === MSG_ERROR) {
                displayError(form, data.data);
            } else {
                location.href = '/';
            }
        });
    });

    function activateAcount(params)
    {
        var agreement = '';
        if ((params.tc_agreement === '') && (params.pp_agreement === '')) {
            agreement = 'modalTerm';
        } else if ((params.tc_agreement !== '') && (params.pp_agreement === '')) {
            agreement = 'modalPrivacy';
        } else {
            agreement = 'modalConfirmation';
        }
        $('#' + agreement).modal({
            backdrop: 'static',
            keyboard: false
        });
    }
});