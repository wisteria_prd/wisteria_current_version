$(function(e) {
    var objPosition;
    var triggerReset = 0;
    if ($.fn.popover.Constructor.VERSION === '3.3.7') {
        $('[data-toggle="popover"]').on('hidden.bs.popover', function() {
            $(this).data('bs.popover').inState.click = false;
        });
    }

    $('.show-forget-password').click(function(e) {
        e.preventDefault();
        if (($('#submit-login').has(e.target).length === 0)) {
            $('#submit-login').popover('hide');
        }
        objPosition = $(this).offset();
        dynamicModalPosition(objPosition.top, 195, 'modalResetPassword', 'reset-mail');
        if (triggerReset === 0) {
            $('.submit-reset-password').trigger('click');
            triggerReset = 1;
        }
    });

    $('.submit-reset-password').click(function() {
        var data = '<p>' + $('meta[name="msg"]').attr('content') + '</p>';
        data += '<div class="text-center">';
        data += '<button class="btn btn-default confirm-no" type="button">' + $('meta[name="btn_cancel"]').attr('content') + '</button>';
        data += '<button class="btn btn-default confirm-yes" type="button">' + $('meta[name="btn_ok"]').attr('content') + '</button>';
        data += '</div>';
        var tmp = '<div class="popover" role="tooltip"><div class="arrow"></div><h3 class="popover-title"></h3>';
        tmp += '<div class="popover-content"></div></div>';
        var options = {
            placement: 'top',
            html: true,
            content: data,
            container: 'body',
            template: tmp
        };
        $('.submit-reset-password').popover(options);
    });

    $('body').on('click', '.confirm-yes', function(e) {
        e.preventDefault();
        var data = $('.form-user-reset-password').serialize();
        $('#error_email').removeClass('error-tips').addClass('help-tips');
        $('.load').addClass('hide-loading');
        $('.confirm-no').click();
        $.ajax({
            url: BASE + 'users/checkEmail',
            type: 'POST',
            cache: false,
            data: data,
            beforeSend: function() {
                $('.load').removeClass('hide-loading');
            },
            success: function(response) {
                $('.load').addClass('hide-loading');
                if (response.status === 'OK') {
                    $('#modalResetPassword').modal('hide');
                    location.reload();
                } else if (response.status === 'ERROR') {
                    $('#error_email')
                            .removeClass('help-tips')
                            .addClass('error-tips')
                            .empty().text(response.msg);
                }
            }
        });
    });

    $('body').on('click', '.confirm-no', function(e) {
        $('.submit-reset-password').trigger('click');
    });
});