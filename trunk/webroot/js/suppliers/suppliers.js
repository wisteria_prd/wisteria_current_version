$(function (e) {
    /*
     * INDEX PAGE
     */
    var maxPage = parseInt($('#maxPage').val());
    updatePagination(maxPage);
    if (ACTION !== 'index') {
        $('select[name="country_id"]').select2();
        initialTelInput();
    }

    $('.table>tbody>tr>td a').click(function (e) {
        e.stopPropagation();
    });

    $('.data-tb-list .table>tbody>tr').click(function (e) {
        let options = {
            type: 'GET',
            url: BASE + 'Suppliers/view',
            data: { id:  $(this).closest('tr').data('id') },
            beforeSend: function () {
                $.LoadingOverlay('show');
            }
        };
        ajaxRequest(options, SESSION_TIMEOUT, function (data) {
            $('body').prepend(data);
            $('body').find('.modal-view').modal('show');
        });
    });

    $('.btn-restore').click(function (e) {
        e.stopPropagation();
        let params = {
            id: $(this).closest('tr').attr('data-id'),
            status: $(this).closest('td').attr('data-target')
        };
        let options = {
            url: BASE + 'Suppliers/getStatus',
            type: 'GET',
            data: params,
            beforeSend: function () {
                $.LoadingOverlay('show');
            }
        };
        ajaxRequest(options, SESSION_TIMEOUT, function (data) {
            $('body').prepend(data);
            let content = $('body').find('.modal-status');
            $(content).modal('show');
            _iframe(content);
        });
    });

    $('.btn-rm-record').click(function (e) {
        e.stopPropagation();
        let options = {
            url: BASE + 'Suppliers/getDelete',
            type: 'GET',
            data: { id: $(this).closest('tr').attr('data-id') },
            beforeSend: function () {
                $.LoadingOverlay('show');
            }
        };
        ajaxRequest(options, SESSION_TIMEOUT, function (data) {
            $('body').prepend(data);
            let content = $('body').find('.modal-delete');
            $(content).modal('show');
            _iframe(content);
        });
    });

    $('.btn_comment').click(function (e) {
        e.stopPropagation();
        let params = {
            id: $(this).closest('tr').attr('data-id'),
            type: SUPPLIER,
            referer: CONTROLLER
        };
        let options = {
            url: BASE + 'Messages/getMessageList',
            type: 'GET',
            data: params,
            beforeSend: function () {
                $.LoadingOverlay('show');
            }
        };
        ajaxRequest(options, SESSION_TIMEOUT, function (data) {
            $('body').prepend(data);
            $('body').find('.modal-message').modal('show');
        });
    });

    $('#btn-register').click(function (e) {
        let form = $('body').find('form[name="supplier_form"]');
        updateDialCode(form);
        let options = {
            url: BASE + 'Suppliers/saveOrUpdate',
            type: 'POST',
            data: $(form).serialize(),
            beforeSend: function () {
                $(form).find('.error-message').remove();
                $.LoadingOverlay('show');
            }
        };
        ajaxRequest(options, SESSION_TIMEOUT, function (data) {
            if (data.message === MSG_ERROR) {
                showMessageError(form, data.data);
                if ('info_details' in data.data) {
                    showAssociateMessageError(form, data.data.info_details, 'info_details');
                }
                return false;
            }
            location.href = BASE + 'Suppliers';
        });
    });

    $('.btn-seller-brand').click(function (e) {
        e.stopPropagation();
        var that = this;
        var data_id = $(that).closest('tr').attr('data-id');
        if (data_id) {
            let options = {
                url: BASE + 'SellerBrands/getListOfSellerBrands',
                type: 'GET',
                data: {
                    type: SUPPLIER,
                    external_id: data_id
                },
                beforeSend: function () {
                    $.LoadingOverlay('show');
                }
            };
            ajaxRequest(options, SESSION_TIMEOUT, function (data) {
                $('body').prepend(data);
                $('body').find('.modal-seller-brand').modal('show');
            });
        }
    });

    // ADD MULTIPLE TELS
    $('body').on('click', '.btn-tel-add', function(e) {
        var index = $('body').find('.tel-box-wrap .form-group').length + 1;
        var content = '<div class="form-group">' +
                '<label class="col-sm-2 control-label"></label>' +
                '<div class="col-md-4 col-lg-4 col-sm-4">' +
                    '<input type="tel" placeholder="' + TXT_ENTER_TEL + '" class="form-control" name="info_details[' + index + '][tel]"/>' +
                    '<input type="hidden" name="info_details[' + index + '][type]" value="' + SUPPLIER + '"/>' +
                '</div>' +
                '<div class="col-md-4 col-lg-4 col-sm-4">' +
                    '<input type="text" placeholder="' + STR_TEL_EXT + '" class="form-control" name="info_details[' + index + '][tel_extension]"/>' +
                '</div>' +
                '<div class="col-sm-1">' +
                    '<button type="button" class=" btn btn-delete btn-sm form-btn-add btn-remove-field">' +
                        '<i class="fa fa-trash" aria-hidden="true"></i>' +
                    '</button>' +
                '</div>' +
            '</div>';
        $('body').find('.tel-box-wrap').append(content);
        initialTelInput(null, true);
    });

    // ADD MULTIPLE EMAILS
    $('body').on('click', '.btn-email', function(e) {
        var index = $('body').find('.email-box-wrap .form-group').length + 1;
        var content = '<div class="form-group">' +
                '<label class="col-sm-2 control-label"></label>' +
                '<div class="col-md-4 col-sm-4">' +
                    '<div class="col-sm-10 no-p-l">' +
                        '<input type="email" placeholder="' + TXT_ENTER_EMAIL + '" class="form-control" name="info_details[' + index + '][email]"/>' +
                        '<input type="hidden" name="info_details[' + index + '][type]" value="' + SUPPLIER + '"/>' +
                    '</div>' +
                    '<div class="col-sm-2">' +
                        '<button type="button" class=" btn btn-delete btn-sm form-btn-add btn-remove-field">' +
                            '<i class="fa fa-trash" aria-hidden="true"></i>' +
                        '</button>' +
                    '</div>' +
                '</div>' +
            '</div>';
        $('body').find('.email-box-wrap').append(content);
    });

    // REMOVE EMAIL, TEL
    $('body').on('click', '.btn-remove-field', function(e) {
        $(this).closest('.form-group').remove();
    });

    function _iframe(content)
    {
        /*
        * CHANGE OF STATUS MODAL
        */
        $(content).find('.change-status').click(function (e) {
            let params = {
                id: $(content).find('[name="id"]').attr('content'),
                status: $(content).find('[name="status"]').attr('content')
            };
            let options = {
                url: BASE + 'Suppliers/changeOfStatus',
                type: 'POST',
                data: params,
                beforeSend: function () {
                    $.LoadingOverlay('show');
                }
            };
            ajaxRequest(options, SESSION_TIMEOUT, function (data) {
                if (data.message === MSG_SUCCESS) {
                    location.reload();
                }
            });
        });

        /**
        * DELETE MODAL
        */
        $(content).find('.delete-mf').click(function (e) {
            let options = {
                url: BASE + 'Suppliers/delete',
                type: 'POST',
                data: { id: $(content).find('[name="id"]').attr('content') },
                beforeSend: function () {
                    $.LoadingOverlay('show');
                }
            };
            ajaxRequest(options, SESSION_TIMEOUT, function (data) {
                if (data.message === MSG_SUCCESS) {
                    location.reload();
                }
            });
        });
    }
});
