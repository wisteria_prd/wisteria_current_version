$(function(e) {
    var maxPage = parseInt($('#maxPage').val());
    updatePagination(maxPage);

    $('#seller-id').select2({
        width: '100%',
        minLength: 0,
        ajax: {
            url: BASE + 'SellerBrands/getListOfSellerBrands',
            dataType: 'json',
            delay: 250,
            data: function (params) {
                let query = {
                    keyword: params.term
                };
                return query;
            },
            processResults: function (data) {
                let dataSource = [];
                if (data.data === null && data.data === 'undefined') {
                    return false;
                }
                $.each(data.data, function(i, v) {
                    var name = '';
                    switch (v.seller.type) {
                        case MANUFACTURER_TYPE:
                            name = getNameByLocal(LOCALE, v.seller.manufacturer);
                            break;
                        case SUPPLIER:
                            name = getNameByLocal(LOCALE, v.seller.supplier);
                            break;
                    }
                    dataSource.push({
                        id: v.seller_id,
                        text: name
                    });
                });
                return { results: dataSource };
            }
        }
    });

    $('#product-brand-id').select2({
        width: '100%',
        minLength: 0,
        ajax: {
            url: BASE + 'SellerBrands/getListOfSellerBrandsBySellerId',
            dataType: 'json',
            delay: 250,
            data: function (params) {
                let query = {
                    keyword: params.term,
                    seller_id: $('body').find('#seller-id').val()
                };
                return query;
            },
            processResults: function (data) {
                let dataSource = [];
                if (data.data === null && data.data === 'undefined') {
                    return false;
                }
                $.each(data.data, function(i, v) {
                    dataSource.push({
                        id: v.product_brand_id,
                        text: getNameByLocal(LOCALE, v.product_brand)
                    });
                });
                return { results: dataSource };
            }
        }
    });

    $('#product-detail-id').select2({
        width: '100%',
        minLength: 0,
        ajax: {
            url: BASE + 'Products/getListOfProductsByProductBrandId',
            dataType: 'json',
            delay: 250,
            data: function (params) {
                let query = {
                    keyword: params.term,
                    product_brand_id: $('body').find('#product-brand-id').val()
                };
                return query;
            },
            processResults: function (data) {
                let dataSource = [];
                if (data.data === null && data.data === 'undefined') {
                    return false;
                }
                $.each(data.data, function(i, v) {
                    if ((v.product_details === null) && (v.product_details === 'undefined')) {
                        return false;
                    }
                    var prd_name = getNameByLocal(LOCALE, v);
                    $.each(v.product_details, function(index, value) {
                        var prd_detail_name = value.pack_size;
                        if ((value.tb_pack_sizes !== null) && (value.tb_pack_sizes !== 'undefined')) {
                            prd_detail_name += ' ' + getNameByLocal(LOCALE, value.tb_pack_sizes);
                        }
                        prd_detail_name += ' ' + value.single_unit_size;
                        if ((value.single_unit !== null) && (value.single_unit !== 'undefined')) {
                            prd_detail_name += ' ' + getNameByLocal(LOCALE, value.single_unit);
                        }
                        dataSource.push({
                            id: v.id,
                            text: prd_name + ' ' + prd_detail_name
                        });
                    });
                });
                return { results: dataSource };
            }
        }
    });

    $('.btn-register').click(function(e) {
        var form = $('form[name="product_form"]');
        var params = {
            type: 'POST',
            url: BASE + 'PurchaseConditions/saveOrUpdate',
            data: $(form).serialize(),
            beforeSend: function() {
                $.LoadingOverlay('show');
                $(form).find('.error-message').remove();
            }
        };
        ajaxRequest(params, SESSION_TIMEOUT, function(data) {
            if (data.message === MSG_ERROR) {
                showMessageError(form, data.data);
                return false;
            }
            location.href = BASE + 'PurchaseConditions/product';
        });
    });

    $('.btn-restore').click(function (e) {
        e.stopPropagation();
        let params = {
            id: $(this).closest('tr').attr('data-id'),
            status: $(this).closest('td').attr('data-target')
        };
        let options = {
            url: BASE + 'PurchaseConditions/getStatus',
            type: 'GET',
            data: params,
            beforeSend: function () {
                $.LoadingOverlay('show');
            }
        };
        ajaxRequest(options, SESSION_TIMEOUT, function (data) {
            $('body').prepend(data);
            let content = $('body').find('.modal-status');
            $(content).modal('show');
            _iframe(content);
        });
    });

    $('.btn-delete').click(function (e) {
        e.stopPropagation();
        let params = {
            id: $(this).closest('tr').attr('data-id')
        };
        let options = {
            url: BASE + 'PurchaseConditions/getDelete',
            type: 'GET',
            data: params,
            beforeSend: function () {
                $.LoadingOverlay('show');
            }
        };
        ajaxRequest(options, SESSION_TIMEOUT, function (data) {
            $('body').prepend(data);
            let content = $('body').find('.modal-delete');
            $(content).modal('show');
            _iframe(content);
        });
    });

    $('.btn_comment').click(function (e) {
        e.stopPropagation();
        let params = {
            id: $(this).closest('tr').attr('data-id'),
            type: $(this).attr('data-type')
        };
        let options = {
            url: BASE + 'messages/get-message-list',
            type: 'GET',
            data: params,
            beforeSend: function () {
                $.LoadingOverlay('show');
            }
        };
        ajaxRequest(options, SESSION_TIMEOUT, function (data) {
            $('body').prepend(data);
            $('body').find('.modal-message').modal('show');
        });
    });

    function _iframe(content)
    {
        $(content).find('.change-status').click(function (e) {
            let params = {
                id: $(content).find('[name="id"]').attr('content'),
                status: $(content).find('[name="status"]').attr('content')
            };
            let options = {
                url: BASE + 'PurchaseConditions/changeOfStatus',
                type: 'POST',
                data: params,
                beforeSend: function () {
                    $.LoadingOverlay('show');
                }
            };
            ajaxRequest(options, SESSION_TIMEOUT, function (data) {
                if (data.message === MSG_SUCCESS) {
                    location.reload();
                }
            });
        });

        $(content).find('.delete-mf').click(function (e) {
            let params = {
                id: $(content).find('[name="id"]').attr('content')
            };
            let options = {
                url: BASE + 'PurchaseConditions/delete',
                type: 'POST',
                data: params,
                beforeSend: function () {
                    $.LoadingOverlay('show');
                }
            };
            ajaxRequest(options, SESSION_TIMEOUT, function (data) {
                if (data.message === MSG_SUCCESS) {
                    location.reload();
                }
            });
        });
    }
});
