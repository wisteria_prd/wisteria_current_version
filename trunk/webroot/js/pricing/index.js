$(function() {
    $('body').on('change', '.filter-manufacturer, .filter-brand, .filter-product', function(e) {
        $('form[name="form_search"]').submit();
    });

    // comments
    $('.btn_comment').click(function (e) {
        e.stopPropagation();
        let params = {
            id: $(this).closest('tr').attr('data-id'),
            type: MESSAGE_TYPE_PRICING,
            referer: CONTROLLER,
        };
        let options = {
            url: BASE + 'messages/get-message-list',
            type: 'GET',
            data: params,
            beforeSend: function () {
                $.LoadingOverlay('show');
            }
        };
        ajaxRequest(options, SESSION_TIMEOUT, function (data) {
            $('body').prepend(data);
            $('body').find('.modal-message').modal('show');
        });
    });

    // status restore
    $('.btn-restore').click(function (e) {
        e.stopPropagation();
        let params = {
            id: $(this).closest('tr').attr('data-id'),
            status: $(this).closest('td').attr('data-target')
        };
        let options = {
            url: BASE + 'pricing/get-status',
            type: 'GET',
            data: params,
            beforeSend: function () {
                $.LoadingOverlay('show');
            }
        };
        ajaxRequest(options, SESSION_TIMEOUT, function (data) {
            $('body').prepend(data);
            let content = $('body').find('.modal-status');
            $(content).modal('show');
            _iframe(content);
        });
    });


    // status delete
    $('.btn-delete').click(function (e) {
        e.stopPropagation();
        let params = {
            id: $(this).closest('tr').attr('data-id')
        };
        let options = {
            url: BASE + 'pricing/get-delete',
            type: 'GET',
            data: params,
            beforeSend: function () {
                $.LoadingOverlay('show');
            }
        };
        ajaxRequest(options, SESSION_TIMEOUT, function (data) {
            $('body').prepend(data);
            let content = $('body').find('.modal-delete');
            $(content).modal('show');
            _iframe(content);
        });
    });

    function _iframe(content)
    {
        /*
        * CHANGE OF STATUS MODAL
        */
        $(content).find('.change-status').click(function (e) {
            let params = {
                id: $(content).find('[name="id"]').attr('content'),
                status: $(content).find('[name="status"]').attr('content')
            };
            let options = {
                url: BASE + 'pricing/change-of-status',
                type: 'POST',
                data: params,
                beforeSend: function () {
                    $.LoadingOverlay('show');
                }
            };
            ajaxRequest(options, SESSION_TIMEOUT, function (data) {
                if (data.message === MSG_SUCCESS) {
                    location.reload();
                }
            });
        });

        /**
        * DELETE MODAL
        */
        $(content).find('.delete-mf').click(function (e) {
            let params = {
                id: $(content).find('[name="id"]').attr('content')
            };
            let options = {
                url: BASE + 'pricing/delete',
                type: 'POST',
                data: params,
                beforeSend: function () {
                    $.LoadingOverlay('show');
                }
            };
            ajaxRequest(options, SESSION_TIMEOUT, function (data) {
                if (data.message === MSG_SUCCESS) {
                    location.reload();
                }
            });
        });
    }
});
